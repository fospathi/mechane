package about

import (
	_ "embed"
)

var (
	//go:embed license/mechane.txt
	mechaneLicense string

	//go:embed license/glfw.txt
	glfwLicense string

	//go:embed license/go.txt
	goLicense string

	//go:embed license/elm.txt
	elmLicense string

	//go:embed license/nodejs.txt
	nodejsLicense string

	//go:embed license/typescript.txt
	typescriptLicense string
)

var (
	//go:embed license/cascadiacode.txt
	cascadiaCodeLicense string

	//go:embed license/droidsansmono.txt
	droidSansMonoLicense string

	//go:embed license/materialicons.txt
	materialIconsLicense string

	//go:embed license/opensans.txt
	openSansLicense string

	//go:embed license/notoserif.txt
	notoSerifLicense string
)

var Fospathi = Author{
	Name: "Christian Stewart",
	URL:  "https://gitlab.com/fospathi",
}

// Mechane module information.
//
// Some of the fields are purposely left empty and auto completed at run time.
var mechane = Mechane{
	Author: []Author{
		Fospathi,
	},

	Blurb: []string{
		"Immerse yourself in a universe of 3D worlds built in a wire art style.",
	},

	Link: "https://gitlab.com/fospathi/mechane",

	Name: "mechane",

	License: License{
		Badge: CreativeCommons.PublicDomainDedication.Badge,
		ID:    "CC0-1.0",
		Name:  "Creative Commons Zero v1.0 Universal",
		URL:   "https://gitlab.com/fospathi/mechane/-/blob/master/LICENSE",
		Text:  mechaneLicense,
	},

	Alternative: MITNoAttribution(2023, Fospathi.Name),

	Fonts: []Dependency{
		{
			Name:     "Cascadia Code",
			URL:      "https://github.com/microsoft/cascadia-code",
			Licensor: []string{"Microsoft"},
			License: License{
				ID:   "OFL-1.0",
				Name: "SIL Open Font License 1.1",
				URL:  "https://github.com/microsoft/cascadia-code/blob/main/LICENSE",
				Text: cascadiaCodeLicense,
			},
		},
		{
			Name:     "Droid Sans Mono",
			URL:      "https://github.com/aosp-mirror/platform_frameworks_base/tree/master/data/fonts",
			Licensor: []string{"Open Handset Alliance"},
			License: License{
				ID:   "Apache-2.0",
				Name: "Apache License 2.0",
				URL:  "https://github.com/aosp-mirror/platform_frameworks_base/tree/master/data/fonts#readme",
				Text: droidSansMonoLicense,
			},
		},
		{
			Name:     "Material Icons",
			URL:      "https://github.com/google/material-design-icons",
			Licensor: []string{"Google"},
			License: License{
				ID:   "Apache-2.0",
				Name: "Apache License 2.0",
				URL:  "https://github.com/google/material-design-icons/blob/master/LICENSE",
				Text: materialIconsLicense,
			},
		},
		{
			Name:     "Noto Serif",
			URL:      "https://fonts.google.com/noto/specimen/Noto+Serif",
			Licensor: []string{"Google"},
			License: License{
				ID:   "OFL-1.0",
				Name: "SIL Open Font License 1.1",
				URL:  "https://fonts.google.com/noto/specimen/Noto+Serif/about",
				Text: notoSerifLicense,
			},
		},
		{
			Name:     "Open Sans",
			URL:      "https://fonts.google.com/specimen/Open+Sans",
			Licensor: []string{"Google"},
			License: License{
				ID:   "OFL-1.0",
				Name: "SIL Open Font License 1.1",
				URL:  "https://fonts.google.com/specimen/Open+Sans/about",
				Text: openSansLicense,
			},
		},
	},

	Binaries: []Dependency{
		{
			Name:     "GLFW",
			URL:      "https://github.com/glfw/glfw",
			Licensor: []string{"Marcus Geelnard", "Camilla Löwy"},
			License: License{
				ID:   "Zlib",
				Name: "zlib License",
				URL:  "https://github.com/glfw/glfw/blob/master/LICENSE.md",
				Text: glfwLicense,
			},
		},
		{
			Name:     "Go",
			URL:      "https://go.dev",
			Licensor: []string{"The Go Authors"},
			License: License{
				ID:   "BSD-3-Clause",
				Name: `BSD 3-Clause "New" or "Revised" License`,
				URL:  "https://go.dev/LICENSE",
				Text: goLicense,
			},
		},
	},

	DevBinaries: []Dependency{
		{
			Name:     "Elm",
			URL:      "https://github.com/elm/compiler",
			Licensor: []string{"Evan Czaplicki"},
			License: License{
				ID:   "BSD-3-Clause",
				Name: `BSD 3-Clause "New" or "Revised" License`,
				URL:  "https://github.com/elm/compiler/blob/master/LICENSE",
				Text: elmLicense,
			},
		},
		{
			Name:     "Node.js",
			URL:      "https://github.com/nodejs/node",
			Licensor: []string{"Ryan Dahl", "OpenJS Foundation"},
			License: License{
				ID:   "MIT",
				Name: "MIT License",
				URL:  "https://github.com/nodejs/node/blob/main/LICENSE",
				Text: nodejsLicense,
			},
		},
		{
			Name:     "Typescript",
			URL:      "https://github.com/microsoft/TypeScript",
			Licensor: []string{"Microsoft"},
			License: License{
				ID:   "Apache-2.0",
				Name: "Apache License 2.0",
				URL:  "https://github.com/microsoft/TypeScript/blob/main/LICENSE.txt",
				Text: typescriptLicense,
			},
		},
	},
}
