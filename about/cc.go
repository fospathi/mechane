package about

import (
	_ "embed"
	"encoding/base64"
)

// CC0

//go:embed license/cc0.txt
var publicDomainDedication string

//go:embed license/ccbadge/cc-zero.png
var publicDomainDedicationBadge []byte

// CC-BY

//go:embed license/cc_by.txt
var attribution string

//go:embed license/ccbadge/by.png
var attributionBadge []byte

// CC-BY-SA

//go:embed license/cc_by_sa.txt
var attributionShareAlike string

//go:embed license/ccbadge/by-sa.png
var attributionShareAlikeBadge []byte

// CC-BY-NC

//go:embed license/cc_by_nc.txt
var attributionNonCommercial string

//go:embed license/ccbadge/by-nc.png
var attributionNonCommercialBadge []byte

// CC-BY-NC-SA

//go:embed license/cc_by_nc_sa.txt
var attributionNonCommercialShareAlike string

//go:embed license/ccbadge/by-nc-sa.png
var attributionNonCommercialShareAlikeBadge []byte

// CC-BY-ND

//go:embed license/cc_by_nd.txt
var attributionNoDerivatives string

//go:embed license/ccbadge/by-nd.png
var attributionNoDerivativesBadge []byte

// CC-BY-NC-ND

//go:embed license/cc_by_nc_nd.txt
var attributionNonCommercialNoDerivatives string

//go:embed license/ccbadge/by-nc-nd.png
var attributionNonCommercialNoDerivativesBadge []byte

// CreativeCommons licenses.
//
// https://creativecommons.org/about/cclicenses/
var CreativeCommons = struct {
	PublicDomainDedication                License
	Attribution                           License
	AttributionShareAlike                 License
	AttributionNonCommercial              License
	AttributionNonCommercialShareAlike    License
	AttributionNoDerivatives              License
	AttributionNonCommercialNoDerivatives License
}{
	// PublicDomainDedication license allows creators to give up their copyright
	// and put their works into the worldwide public domain. CC0 allows reusers
	// to distribute, remix, adapt, and build upon the material in any medium or
	// format, with no conditions.
	//
	// https://creativecommons.org/publicdomain/zero/1.0/
	PublicDomainDedication: License{
		Badge: base64.StdEncoding.EncodeToString(
			publicDomainDedicationBadge,
		),
		ID:   "CC0-1.0",
		Name: "Creative Commons Zero v1.0 Universal",
		Text: publicDomainDedication,
		URL:  "https://creativecommons.org/publicdomain/zero/1.0/legalcode",
	},

	// Attribution license allows reusers to distribute, remix, adapt, and build
	// upon the material in any medium or format, so long as attribution is
	// given to the creator. The license allows for commercial use.
	//
	// https://creativecommons.org/licenses/by/4.0/
	Attribution: License{
		Badge: base64.StdEncoding.EncodeToString(
			attributionBadge,
		),
		ID:   "CC-BY-4.0",
		Name: "Creative Commons Attribution 4.0 International",
		Text: attribution,
		URL:  "https://creativecommons.org/licenses/by/4.0/legalcode",
	},

	// AttributionShareAlike license allows reusers to distribute, remix, adapt,
	// and build upon the material in any medium or format, so long as
	// attribution is given to the creator. The license allows for commercial
	// use. If you remix, adapt, or build upon the material, you must license
	// the modified material under identical terms.
	//
	// https://creativecommons.org/licenses/by-sa/4.0/
	AttributionShareAlike: License{
		Badge: base64.StdEncoding.EncodeToString(
			attributionShareAlikeBadge,
		),
		ID:   "CC-BY-SA-4.0",
		Name: "Creative Commons Attribution Share Alike 4.0 International",
		Text: attributionShareAlike,
		URL:  "https://creativecommons.org/licenses/by-sa/4.0/legalcode",
	},

	// AttributionNonCommercial license allows reusers to distribute, remix,
	// adapt, and build upon the material in any medium or format for
	// noncommercial purposes only, and only so long as attribution is given to
	// the creator.
	//
	// https://creativecommons.org/licenses/by-nc/4.0/
	AttributionNonCommercial: License{
		Badge: base64.StdEncoding.EncodeToString(
			attributionNonCommercialBadge,
		),
		ID:   "CC-BY-NC-4.0",
		Name: "Creative Commons Attribution Non Commercial 4.0 International",
		Text: attributionNonCommercial,
		URL:  "https://creativecommons.org/licenses/by-nc/4.0/legalcode",
	},

	// AttributionNonCommercialShareAlike license allows reusers to distribute,
	// remix, adapt, and build upon the material in any medium or format for
	// noncommercial purposes only, and only so long as attribution is given to
	// the creator. If you remix, adapt, or build upon the material, you must
	// license the modified material under identical terms.
	//
	// https://creativecommons.org/licenses/by-nc-sa/4.0/
	AttributionNonCommercialShareAlike: License{
		Badge: base64.StdEncoding.EncodeToString(
			attributionNonCommercialShareAlikeBadge,
		),
		ID:   "CC-BY-NC-SA-4.0",
		Name: "Creative Commons Attribution Non Commercial Share Alike 4.0 International",
		Text: attributionNonCommercialShareAlike,
		URL:  "https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode",
	},

	// AttributionNoDerivatives license allows reusers to copy and distribute
	// the material in any medium or format in unadapted form only, and only so
	// long as attribution is given to the creator. The license allows for
	// commercial use.
	//
	// https://creativecommons.org/licenses/by-nd/4.0/
	AttributionNoDerivatives: License{
		Badge: base64.StdEncoding.EncodeToString(
			attributionNoDerivativesBadge,
		),
		ID:   "CC-BY-ND-4.0",
		Name: "Creative Commons Attribution No Derivatives 4.0 International",
		Text: attributionNoDerivatives,
		URL:  "https://creativecommons.org/licenses/by-nd/4.0/legalcode",
	},

	// AttributionNonCommercialNoDerivatives license allows reusers to copy and
	// distribute the material in any medium or format in unadapted form only,
	// for noncommercial purposes only, and only so long as attribution is given
	// to the creator.
	//
	// https://creativecommons.org/licenses/by-nc-nd/4.0/
	AttributionNonCommercialNoDerivatives: License{
		Badge: base64.StdEncoding.EncodeToString(
			attributionNonCommercialNoDerivativesBadge,
		),
		ID:   "CC-BY-NC-ND-4.0",
		Name: "Creative Commons Attribution Non Commercial No Derivatives 4.0 International",
		Text: attributionNonCommercialNoDerivatives,
		URL:  "https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode",
	},
}
