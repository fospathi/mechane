/*
Package license reproduces the legal code of various licenses in a format
suitable for inclusion in Go packages.
*/
package about

import (
	_ "embed"
	"encoding/base64"
	"strconv"
	"strings"
)

//go:embed license/mit_na.txt
var mitNoAttribution string

//go:embed license/ccbadge/publicdomain.png
var publicDomainBadge []byte

var publicDomainBadgeBase64 = base64.StdEncoding.EncodeToString(
	publicDomainBadge,
)

// MITNoAttribution is a modified version of the common MIT license, with the
// attribution paragraph removed, making it a public-domain-equivalent license.
//
// https://opensource.org/license/mit-0/
func MITNoAttribution(year int, licensors ...string) License {

	return License{
		Badge: publicDomainBadgeBase64,
		ID:    "MIT-0",
		Name:  "MIT No Attribution",
		Text: strings.NewReplacer(
			"<YEAR>", strconv.Itoa(year),
			"<COPYRIGHT HOLDER>", strings.Join(licensors, ", "),
		).Replace(mitNoAttribution),
		URL: "https://opensource.org/license/mit-0/",
	}
}
