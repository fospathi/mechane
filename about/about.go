/*
Package about contains and generates meta information about Mechane including
dependencies and their licenses.
*/
package about

import (
	"slices"
	"strings"

	"golang.org/x/mod/modfile"

	"gitlab.com/fospathi/mechane/mechui"
)

// Author of a software package.
type Author struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

// License associated with a software package.
type License struct {
	// Badge is an optional PNG format graphic logo for the license.
	//
	// The string shall be a base64 encoding of the bytes.
	Badge string `json:"badge"`
	// ID is an SPDX ID.
	//
	// https://spdx.org/licenses/
	ID string `json:"id"`
	// Name given to the license.
	Name string `json:"name"`
	// Legal code of the license.
	Text string `json:"text"`
	// A location the license is reproduced online.
	URL string `json:"url"`
}

// A Dependency of a software package.
type Dependency struct {
	License  License  `json:"license"`
	Licensor []string `json:"licensor"`
	Name     string   `json:"name"`
	URL      string   `json:"url"`
}

// Copy the receiver using new slices in the copy.
func (dep Dependency) Copy() Dependency {
	cpy := dep
	cpy.Licensor = slices.Clone(dep.Licensor)
	return dep
}

// A DataSource used by or included in a software package.
type DataSource struct {
	License  License  `json:"license"`
	Licensor []string `json:"licensor"`
	Name     string   `json:"name"`
	URL      string   `json:"url"`
}

// Software details.
type Software struct {
	Mechane Mechane `json:"mechane"`
	World   World   `json:"world"`
}

// Mechane has information about the Mechane software.
type Mechane struct {
	// Authors of the software.
	Author []Author `json:"author"`
	// Abilities or purpose of the software.
	Blurb []string `json:"blurb"`
	// Link to the source code repository or home page of the software.
	Link string `json:"link"`
	// Name of the software.
	Name string `json:"name"`

	// License the software is published under.
	License License `json:"license"`
	// Alternative license that the software is published under, if any.
	Alternative License `json:"alternativeLicense"`

	// Binaries which need to be installed separately on the system to run the
	// software.
	Binaries []Dependency `json:"binaries"`
	// Binaries which need to be installed separately on the system to do
	// development on the software.
	DevBinaries []Dependency `json:"devBinaries"`
	// Open source fonts distributed with the software.
	Fonts []Dependency `json:"fonts"`

	// URLs of imported packages for the Elm programming language integrant.
	ElmImports []string `json:"elmImports"`
	// URLs of imported packages for the Go programming language integrant.
	GoImports []string `json:"goImports"`
	// URLs of imported packages for the TS programming language integrant.
	TSImports []string `json:"tsImports"`
	// URLs of imported packages for the development of the TS programming
	// language integrant.
	TSDevImports []string `json:"tsDevImports"`
}

// World has information about a Mechane world's software.
type World struct {
	// Authors of the world.
	Author []Author `json:"author"`
	// Abilities or purpose of the world.
	Blurb []string `json:"blurb"`
	// Link to the source code repository or home page of the world.
	Link string `json:"link"`
	// Name of the world.
	Name string `json:"name"`

	// The license the world is published under.
	License License `json:"license"`
	// Alternative license that the world is published under, if any.
	Alternative License `json:"alternativeLicense"`

	// Binaries which need to be installed separately on the system to run the
	// world.
	Binaries []Dependency `json:"binaries"`
	// Binaries which need to be installed separately on the system to do
	// development on the world.
	DevBinaries []Dependency `json:"devBinaries"`
}

// Copy the receiver using new slices in the copy.
func (abt Mechane) Copy() Mechane {
	cvd := func(vd []Dependency) []Dependency {
		res := make([]Dependency, len(vd))
		for i, d := range vd {
			res[i] = d.Copy()
		}
		return res
	}
	cpy := Mechane{
		Author: slices.Clone(abt.Author),
		Blurb:  slices.Clone(abt.Blurb),
		Link:   abt.Link,
		Name:   abt.Name,

		License:     abt.License,
		Alternative: abt.Alternative,

		Binaries:    cvd(abt.Binaries),
		DevBinaries: cvd(abt.DevBinaries),
		Fonts:       cvd(abt.Fonts),

		GoImports:    slices.Clone(abt.GoImports),
		ElmImports:   slices.Clone(abt.ElmImports),
		TSImports:    slices.Clone(abt.TSImports),
		TSDevImports: slices.Clone(abt.TSDevImports),
	}
	return cpy
}

// NewMechane returns a new About with information about the Mechane module.
//
// The given argument shall be the Mechane module's go.mod file.
func NewMechane(goMod []byte) (Mechane, error) {
	const (
		// Filter out imports belonging to Mechane and the standard libraries.
		mechaneNamespace = "gitlab.com/fospathi"
		goXNamespace     = "golang.org/x"
		elmNamespace     = "elm/"

		// Turn imports into full URLs.
		elmPackagePrefix = "https://package.elm-lang.org/packages/"
		goPackagePrefix  = "https://pkg.go.dev/"
		npmPackagePrefix = "https://www.npmjs.com/package/"
	)

	var (
		err error
		mf  *modfile.File
		ed  mechui.ElmDeps
		td  mechui.TypescriptDeps
	)

	abt := mechane.Copy()

	if mf, err = modfile.Parse("go.mod", goMod, nil); err != nil {
		return Mechane{}, err
	}
	if ed, td, err = mechui.NewUIDeps(); err != nil {
		return Mechane{}, err
	}

	for _, r := range mf.Require {
		if strings.Contains(r.Mod.Path, mechaneNamespace) ||
			strings.Contains(r.Mod.Path, goXNamespace) {
			continue
		}
		abt.GoImports = append(abt.GoImports, goPackagePrefix+r.Mod.Path)
	}
	slices.Sort(abt.GoImports)

	for name := range ed.Dependencies.Direct {
		if strings.HasPrefix(name, elmNamespace) {
			continue
		}
		abt.ElmImports = append(abt.ElmImports, elmPackagePrefix+name)
	}
	slices.Sort(abt.ElmImports)
	for name := range ed.Dependencies.Indirect {
		if strings.HasPrefix(name, elmNamespace) {
			continue
		}
		abt.ElmImports = append(abt.ElmImports, elmPackagePrefix+name)
	}
	slices.Sort(abt.ElmImports)

	for name := range td.Dependencies {
		abt.TSImports =
			append(abt.TSImports, npmPackagePrefix+name)
	}
	slices.Sort(abt.TSImports)
	for name := range td.DevDependencies {
		abt.TSDevImports =
			append(abt.TSDevImports, npmPackagePrefix+name)
	}
	slices.Sort(abt.TSDevImports)

	return abt, nil
}
