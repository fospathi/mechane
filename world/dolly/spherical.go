package dolly

import (
	"time"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/dryad/ordset"
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechserver/mechuievent"
	"gitlab.com/fospathi/universal/watchable"
)

var (
	SphericalInwardsSurface = NewSpherical(SphericalSpec{
		SphericalMode: SphericalSurfaceInnyMode,
		Up:            Y,
	})
	SphericalOutwardsSurface = NewSpherical(SphericalSpec{
		SphericalMode: SphericalSurfaceOutyMode,
		Up:            Y,
	})
)

// A SphericalSpec specifies the properties of a new Spherical dolly.
type SphericalSpec struct {
	SphericalMode SphericalMode
	Up            AxisPointingUp
}

// NewSpherical constructs a new Spherical dolly (maker).
func NewSpherical(spec SphericalSpec) DollyMaker {
	sphericalOrientorKeys := []string{
		"w", "a", "s", "d", " ",
		"ArrowDown", "ArrowUp", "ArrowLeft", "ArrowRight",
	}
	return func(camera Camera, source string) Dolly {
		return &spherical{
			dolly: dolly{
				camera:         camera,
				keyState:       map[string]time.Time{},
				keySet:         eqset.New(sphericalOrientorKeys...),
				mouseMoveState: &mouseMoveState{},
				src:            source,
			},
			mode: spec.SphericalMode.mode(spec.Up),
		}
	}
}

type spherical struct {
	dolly
	mode sphericalMode
}

// drive the dolly with UI events.
func (d spherical) drive(
	fresh []mechuievent.Event,
	delta watchable.ChronoShift,
) {

	d.addFresh(fresh)
	orientSpherical(
		d.camera,
		d.pressed(),
		*d.mouseMoveState,
		d.mode,
		delta,
	)
}

// orientSpherical is a basic handler that handles moving the camera according
// to WASD key inputs and mouse movements interpreted as directions.
func orientSpherical(
	camera Camera,
	activeKeys dryad.Set[string],
	mouse mouseMoveState,
	mode sphericalMode,
	delta watchable.ChronoShift,
) {
	if mouse.ActiveEditable || (activeKeys.IsEmpty() && mouse.isStill()) {
		return
	}

	// Angular velocities are radians per sec and speeds are units per sec.
	const (
		vNorm, vFast       = 1.0, 3.0
		wOriNorm, wOriFast = maf.QuartPi, 2 * maf.QuartPi
		wRoll              = maf.HalfPi

		vRat, wRat = vFast / vNorm, wOriFast / wOriNorm
	)

	var (
		fast bool
		v    = vNorm
		wOri = wOriNorm
	)
	if activeKeys.Contains(" ") {
		fast = true
		v = vFast
		wOri = wOriFast
	}

	var (
		t     = delta.MeasuredPeriod.Seconds()
		d     = v * t
		oriA  = wOri * t
		rollA = wRoll * t

		fr         = camera.Node.Get()
		originalFr = fr

		up = mode.Up()

		stillMouse   = mouse.isStill()
		grabbedMouse = mouse.primary()

		// Mouse positions in terms of Mechane screen coordinates:
		s2 = mouse.position()              // Current mouse position.
		s1 = s2.subtract(mouse.movement()) // Previous mouse position.
	)

	if !stillMouse && (grabbedMouse || mouse.PointerLock) {
		switch {
		case mode.roll == staticRoll && mode.pitch == constrainedPitch:
			yaw, pitch := camera.yawAndPitch(s1, s2)
			if fast {
				yaw, pitch = yaw*wRat, pitch*wRat
			}
			fr = applyYawAndPitch(fr, maf.Zero, up, yaw, pitch)
		}
	}

	var (
		dir              maf.Vec // Overall direction vector.
		pitch, roll, yaw float64
	)

	for _, key := range ordset.Sorted(activeKeys) {
		var dirCom maf.Vec // Unit length component direction vector.
		switch key {
		case "w":
			if mode.radius != staticRadius {
				dirCom = maf.NegZ
			}
		case "a":
			if mode.orientation == outwardsOrientation {
				yaw += oriA
			} else {
				yaw -= oriA
			}
		case "s":
			if mode.radius != staticRadius {
				dirCom = maf.PosZ
			}
		case "d":
			if mode.orientation == outwardsOrientation {
				yaw -= oriA
			} else {
				yaw += oriA
			}
		case "ArrowUp":
			if mode.orientation == outwardsOrientation {
				pitch += oriA
			} else {
				pitch -= oriA
			}
		case "ArrowDown":
			if mode.orientation == outwardsOrientation {
				pitch -= oriA
			} else {
				pitch += oriA
			}
		case "ArrowLeft":
			if mode.roll != staticRoll {
				roll += rollA
			}
		case "ArrowRight":
			if mode.roll != staticRoll {
				roll -= rollA
			}
		}
		dir = dirCom.Add(dir)
	}

	if dir != maf.Zero {
		fr = fr.LocalDis(dir.Unit().Scale(d))
	}
	if yaw != 0 || pitch != 0 {
		fr = applyYawAndPitch(fr, maf.Zero, up, yaw, pitch)
	}
	if roll != 0 {
		fr = fr.LocalRotPosZ(roll)
	}
	if originalFr != fr {
		camera.Node.Orient(fr)
	}
}

type sphericalMode struct {
	AxisPointingUp
	elevation
	orientation
	pitch
	radius
	roll
}

type SphericalMode uint

const (
	SphericalInnyMode = SphericalMode(iota)
	SphericalOutyMode
	SphericalSurfaceInnyMode
	SphericalSurfaceOutyMode
)

func (sm SphericalMode) mode(up AxisPointingUp) sphericalMode {
	switch sm {
	case SphericalInnyMode:
		return sphericalMode{
			AxisPointingUp: up,
			orientation:    inwardsOrientation,
			pitch:          constrainedPitch,
			roll:           staticRoll,
		}
	case SphericalOutyMode:
		return sphericalMode{
			AxisPointingUp: up,
			orientation:    outwardsOrientation,
			pitch:          constrainedPitch,
			roll:           staticRoll,
		}
	case SphericalSurfaceInnyMode:
		return sphericalMode{
			AxisPointingUp: up,
			orientation:    inwardsOrientation,
			pitch:          constrainedPitch,
			radius:         staticRadius,
			roll:           staticRoll,
		}
	case SphericalSurfaceOutyMode:
		return sphericalMode{
			AxisPointingUp: up,
			orientation:    outwardsOrientation,
			pitch:          constrainedPitch,
			radius:         staticRadius,
			roll:           staticRoll,
		}
	}
	return sphericalMode{}
}
