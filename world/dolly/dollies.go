package dolly

import (
	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/mechane/mechserver/mechuievent"
	"gitlab.com/fospathi/universal/watchable"
)

// NewDollies constructs a new Dollies.
func NewDollies() Dollies {
	return Dollies{dols: map[string]Dolly{}}
}

// A Dollies is a collection of dollies.
type Dollies struct {
	dols map[string]Dolly // Map keys are sources.
}

// Add the dolly to the dollies.
func (dols Dollies) Add(dol Dolly) {
	dols.dols[dol.source()] = dol
}

// DeleteCamera from the dollies by deleting any dollies with that camera name.
//
// Return the newly disconnected sources.
func (dols Dollies) DeleteCamera(name string) dryad.Set[string] {
	disconnected := eqset.New[string]()
	for src, dol := range dols.dols {
		if dol.getCamera().Name == name {
			disconnected.Add(src)
			delete(dols.dols, src)
		}
	}
	return disconnected
}

// DeleteSources from the dollies.
func (dols Dollies) DeleteSources(sources dryad.Set[string]) {
	for src := range sources {
		delete(dols.dols, src)
	}
}

// Drive all the dollies taking into account the given fresh UI events.
func (dols Dollies) Drive(
	fresh mechuievent.Collections, // Map keys are input sources.
	delta watchable.ChronoShift,
) {

	for src, dol := range dols.dols {
		// Drive all the dollies including those without fresh UI events.
		dol.drive(fresh[src].Events, delta)
	}
}

// KeepSourcesIn the dollies which are present in the argument.
func (dols Dollies) KeepSourcesIn(sources dryad.Set[string]) {
	for src := range dols.dols {
		if sources.Contains(src) {
			continue
		}
		delete(dols.dols, src)
	}
}

// UnconnectedSourcesIn the argument sources.
//
// This is sources which are present in the argument but not in the dollies.
func (dols Dollies) UnconnectedSourcesIn(
	sources dryad.Set[string],
) dryad.Set[string] {

	unconnected := sources.Copy()
	for src := range dols.dols {
		if sources.Contains(src) {
			unconnected.Delete(src)
			continue
		}
	}
	return unconnected
}

// UpdateCamera in the dollies.
func (dols Dollies) UpdateCamera(camera Camera) {
	for src, dol := range dols.dols {
		if dol.getCamera().Name == camera.Name {
			dol.setCamera(camera)
			dols.dols[src] = dol
		}
	}
}

type Dolly interface {
	drive(fresh []mechuievent.Event, delta watchable.ChronoShift)
	source() string

	getCamera() Camera
	setCamera(cam Camera)
}
