/*
Package dolly maps user input to camera movements.
*/
package dolly

import (
	"time"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/dryad/ordset"
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechserver/mechuievent"
	"gitlab.com/fospathi/universal/watchable"
)

var (
	SpatialGame = NewSpatial(SpatialSpec{
		SpatialMode: SpatialGameMode,
		Up:          Y,
	})
	SpatialJetPack = NewSpatial(SpatialSpec{
		SpatialMode: SpatialJetPackMode,
		Up:          Y,
	})
	SpatialMiasma = NewSpatial(SpatialSpec{
		SpatialMode: SpatialMiasmaMode,
		Up:          Y,
	})
)

// A SpatialSpec specifies the properties of a new Spatial dolly.
type SpatialSpec struct {
	SpatialMode SpatialMode
	Up          AxisPointingUp
}

// NewSpatial constructs a new Spatial dolly (maker).
func NewSpatial(spec SpatialSpec) DollyMaker {
	spatialOrientorKeys := []string{
		"w", "a", "s", "d", " ",
		"ArrowDown", "ArrowUp", "ArrowLeft", "ArrowRight",
	}
	return func(camera Camera, source string) Dolly {
		return &spatial{
			dolly: dolly{
				camera:         camera,
				keyState:       map[string]time.Time{},
				keySet:         eqset.New(spatialOrientorKeys...),
				mouseMoveState: &mouseMoveState{},
				src:            source,
			},
			mode: spec.SpatialMode.mode(spec.Up),
		}
	}
}

type spatial struct {
	dolly
	mode spatialMode
}

// drive the dolly with UI events.
func (d spatial) drive(
	fresh []mechuievent.Event,
	delta watchable.ChronoShift,
) {

	d.addFresh(fresh)
	orientSpatial(
		d.camera,
		d.pressed(),
		*d.mouseMoveState,
		d.mode,
		delta,
	)
}

// orientSpatial is a basic handler that handles moving the camera according to
// WASD key inputs and mouse movements interpreted as directions.
func orientSpatial(
	camera Camera,
	activeKeys dryad.Set[string],
	mouse mouseMoveState,
	mode spatialMode,
	delta watchable.ChronoShift,
) {
	if mouse.ActiveEditable || (activeKeys.IsEmpty() && mouse.isStill()) {
		return
	}

	const (
		ω     = maf.HalfPi // Radians per second.
		vNorm = 1.0        // Units per second.
		vFast = 3.0        // Units per second.
	)

	v := vNorm
	if activeKeys.Contains(" ") {
		v = vFast
	}

	var (
		t = delta.MeasuredPeriod.Seconds()
		d = v * t
		a = ω * t

		fr         = camera.Node.Get()
		originalFr = fr

		up = mode.Up()

		stillMouse   = mouse.isStill()
		grabbedMouse = mouse.primary()

		// Mouse positions in terms of Mechane screen coordinates:
		s2 = mouse.position()              // Current mouse position.
		s1 = s2.subtract(mouse.movement()) // Previous mouse position.
	)

	if !stillMouse && (grabbedMouse || mouse.PointerLock) {
		switch {

		case mode.roll == freeRoll && mode.pitch == freePitch:
			e1, e2 := camera.screenToEye(s1), camera.screenToEye(s2)
			positionDir, grabDir := e1.Unit(), e1.To(e2).Unit()
			pivotDir := grabDir.Cross(positionDir).Unit()
			fr = fr.LocalRotBasis(pivotDir, e1.Angle(e2))

		case mode.roll == indirectRoll && mode.pitch == freePitch:
			yaw, pitch := camera.yawAndPitch(s1, s2)
			fr = fr.LocalRotPosY(yaw)
			fr = fr.LocalRotPosX(pitch)

		case mode.roll == staticRoll && mode.pitch == freePitch:
			yaw, pitch := camera.yawAndPitch(s1, s2)
			fr = fr.Global(maf.NewRot(maf.Line{P: fr.O, Dir: up}, yaw))
			fr = fr.Global(maf.NewRot(maf.Line{P: fr.O, Dir: fr.Axes.X}, pitch))

		case mode.roll == staticRoll && mode.pitch == constrainedPitch:
			yaw, pitch := camera.yawAndPitch(s1, s2)
			fr = applyYawAndPitch(fr, fr.O, up, yaw, pitch)
		}
	}

	var (
		dir     maf.Vec                 // Overall direction vector.
		disFunc func(maf.Vec) maf.Frame // Applies the direction vector to the frame.
		roll    float64
	)

	for _, key := range ordset.Sorted(activeKeys) {
		var dirCom maf.Vec // Unit length component direction vector.
		switch key {
		case "w":
			dirCom = maf.NegZ
		case "a":
			dirCom = maf.NegX
		case "s":
			dirCom = maf.PosZ
		case "d":
			dirCom = maf.PosX
		case "ArrowUp":
			dirCom = maf.PosY
		case "ArrowDown":
			dirCom = maf.NegY
		case "ArrowLeft":
			if mode.roll != staticRoll {
				roll += a
			}
		case "ArrowRight":
			if mode.roll != staticRoll {
				roll -= a
			}
		}
		dir = dirCom.Add(dir)
	}

	if mode.elevation == staticElevation {
		disFunc = fr.GlobalDis
		dir = fr.Axes.Out().Transform(dir)
		if dir.IsParallel(up, maf.SmallAngle) {
			dir = maf.Zero
		} else if mode.AxisPointingUp.ordinate(dir) != 0 {
			dir = dir.PlaneResolution(maf.Plane{Normal: up})
			// Account for any small rounding errors.
			dir = mode.AxisPointingUp.setOrdinate(dir, 0)
		}
	} else {
		disFunc = fr.LocalDis
	}

	if dir != maf.Zero {
		fr = disFunc(dir.Unit().Scale(d))
	}
	if roll != 0 {
		fr = fr.LocalRotPosZ(roll)
	}
	if originalFr != fr {
		camera.Node.Orient(fr)
	}
}

type spatialMode struct {
	AxisPointingUp
	elevation
	pitch
	roll
}

type SpatialMode uint

const (
	SpatialMiasmaMode = SpatialMode(iota)
	SpatialGameMode
	SpatialJetPackMode
)

func (sm SpatialMode) mode(up AxisPointingUp) spatialMode {
	switch sm {
	case SpatialGameMode:
		return spatialMode{
			AxisPointingUp: up,
			elevation:      staticElevation,
			pitch:          constrainedPitch,
			roll:           staticRoll,
		}
	case SpatialJetPackMode:
		return spatialMode{
			AxisPointingUp: up,
			pitch:          constrainedPitch,
			roll:           staticRoll,
		}
	}
	return spatialMode{}
}
