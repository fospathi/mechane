package dolly

import "gitlab.com/fospathi/maf"

func applyYawAndPitch(
	fr maf.Frame, o maf.Vec, up maf.Vec, yaw float64, pitch float64,
) maf.Frame {
	if yaw != 0 {
		fr = fr.Global(maf.NewRot(maf.Line{P: o, Dir: up}, yaw))
	}
	// Assert that the pitch is a non-zero acute angle.
	if pitch == 0 || pitch >= maf.HalfPi {
		return fr
	}
	fr = fr.Global(
		maf.NewRot(maf.Line{P: o, Dir: fr.Axes.X}, pitch))
	if fr.Axes.Y.Dot(up) < 0 { // Constrain the pitch.
		p1 := maf.Plane{P: o, Normal: fr.Axes.X}
		p2 := maf.Plane{P: o, Normal: up}
		l := p1.PlaneIntersection(p2) // Line coincides with camera's up at max allowed pitch.
		antiPitch := l.Dir.AcuteAngle(fr.Axes.Y)
		if pitch > 0 { // Require the opposite sign to the pitch.
			antiPitch = -antiPitch
		}
		fr = fr.Global(
			maf.NewRot(maf.Line{P: o, Dir: fr.Axes.X}, antiPitch))
	}
	return fr
}
