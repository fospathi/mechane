package dolly

import (
	"time"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechserver/mechuievent"
	"gitlab.com/fospathi/mechane/mechtree"
)

// A DollyMaker links a camera and input source to a new Dolly.
type DollyMaker func(camera Camera, source string) Dolly

type dolly struct {
	camera Camera
	// In a contiguous press spanning multiple time slots the time is that of
	// the initial press.
	keyState       map[string]time.Time
	keySet         dryad.Set[string] // Monitoring these keyboard keys.
	mouseMoveState *mouseMoveState
	src            string
}

// AddFresh UI events which have not been reported previously and update the
// Dolly's state regarding for the monitored keyboard keys and the mouse.
//
// For multiple keyboard key events for a single key the most recent event for
// that key determines whether it is pressed or not.
//
// In the case where a key is pressed as determined by the key's most recent
// event, and it wasn't already registered, and multiple up/down events for that
// key occurred in the time slot, then the press time is the time of the key's
// first down event in the time slot.
//
// Mouse movements are a sum of all the mousemove events' individual movements
// landing in the time slot. For other mouse move values, such as the pixel
// offsets, only the last/most recent mousemove event determines their value.
func (d dolly) addFresh(ve []mechuievent.Event) {
	d.mouseMoveState.MovementX = 0
	d.mouseMoveState.MovementY = 0
	var l int
	if l = len(ve); l == 0 {
		return
	}

	{
		var i int
		for i = l - 1; i >= 0; i-- {
			_, ok := ve[i].E.(mechuievent.MouseMoveEvent)
			if ok {
				break
			}
		}
		if i >= 0 {
			// The authoritative values of these state variables are taken from
			// the most recent of this time slot's mousemove events.
			ev := ve[i].E.(mechuievent.MouseMoveEvent)
			d.mouseMoveState.Buttons = uint8(ev.Buttons)
			d.mouseMoveState.PixelsOffsetX = ev.PixelsOffsetX
			d.mouseMoveState.PixelsOffsetY = ev.PixelsOffsetY
			d.mouseMoveState.PixelsW = ev.PixelsW
			d.mouseMoveState.PixelsH = ev.PixelsH
			d.mouseMoveState.PointerLock = ev.PointerLock
			d.mouseMoveState.ActiveEditable = ev.ActiveEditable
		}
		for ; i >= 0; i-- {
			switch ev := ve[i].E.(type) {
			case mechuievent.MouseMoveEvent:
				d.mouseMoveState.MovementX += ev.MovementX
				d.mouseMoveState.MovementY += ev.MovementY
			}
		}
	}

	// The authoritative pressed state for all keyboard keys is taken from a
	// single keyboard event, that being the most recent of this time slot's
	// keyboard events.
	var pressed dryad.Set[string]
	{
		for i := l - 1; i >= 0; i-- {
			ev, ok := ve[i].E.(mechuievent.KeyboardEvent)
			if !ok {
				continue
			}
			// Initialise pressed even for an empty pressed keys slice.
			pressed = eqset.New(ev.Pressed...)
			break
		}
	}
	if pressed == nil {
		return
	}

	pressed = d.keySet.Intersection(pressed)
	for key := range d.keyState {
		if !pressed.Contains(key) {
			delete(d.keyState, key)
		}
	}

	newlyPressed := pressed.Difference(d.pressed())
	if newlyPressed.Len() == 0 {
		return
	}

	for _, ev := range ve {
		switch e := ev.E.(type) {
		case mechuievent.KeyboardEvent:
			if newlyPressed.Contains(e.Key) && e.KeyDown {
				d.keyState[e.Key] = ev.T
				newlyPressed.Delete(e.Key)
			}
		}
	}
	if newlyPressed.Len() == 0 {
		return
	}

	now := time.Now().UTC()
	for key := range newlyPressed {
		d.keyState[key] = now
	}
}

func (d dolly) getCamera() Camera {
	return d.camera
}

// pressed keys.
func (d dolly) pressed() dryad.Set[string] {
	pressed := eqset.New[string]()
	for key := range d.keyState {
		pressed.Add(key)
	}
	return pressed
}

func (d *dolly) setCamera(cam Camera) {
	d.camera = cam
}

// source ID of the input events.
func (d dolly) source() string {
	return d.src
}

// mouseMoveState summarizes mousemove events.
type mouseMoveState struct {
	Buttons                      uint8
	MovementX, MovementY         float64
	PixelsOffsetX, PixelsOffsetY float64 // Relative to top-left corner.
	PixelsW, PixelsH             float64

	ActiveEditable bool // True if the hover is on a focused text editing area or similar.
	PointerLock    bool
}

// auxillary reports whether the auxillary mouse button is pressed.
func (s mouseMoveState) auxillary() bool {
	return (s.Buttons & 4) != 0
}

// isStill reports whether the overall movement is zero.
func (s mouseMoveState) isStill() bool {
	return s.MovementX == 0 && s.MovementY == 0
}

// movement of the mouse from the previous to the current mouse position
// expressed as a vector in Mechane screen coordinate space (that is, as
// proportions of the pixels' display rectangle height).
//
// When the pointer lock is active the movements are inverted.
func (s mouseMoveState) movement() screenCoords {
	// Invert the Y-axis direction because downwards movement is reported as a
	// positive movement by browsers but is a negative movement expressed in
	// Mechane screen coordinates.
	x, y := s.MovementX/s.PixelsH, -s.MovementY/s.PixelsH
	if s.PointerLock {
		x, y = -x, -y
	}
	return screenCoords{X: x, Y: y}
}

// position of the mouse in Mechane screen coordinates.
func (s mouseMoveState) position() screenCoords {
	// Change of frame from a coordinate system with its origin at the top-left
	// corner and its Y-axis pointing downwards to a coordinate system at the
	// centre and its Y-axis pointing upwards.
	f := maf.Frame{
		O: maf.Vec{X: -s.PixelsW / 2, Y: s.PixelsH / 2, Z: 0},
		Axes: maf.Basis{
			X: maf.PosX,
			Y: maf.NegY,
			Z: maf.NegZ,
		},
	}
	m := f.Out()
	p := m.Transform(maf.Vec{X: s.PixelsOffsetX, Y: s.PixelsOffsetY})
	return screenCoords{X: p.X / s.PixelsH, Y: p.Y / s.PixelsH}
}

// primary reports whether the primary mouse button is pressed.
func (s mouseMoveState) primary() bool {
	return (s.Buttons & 1) != 0
}

// secondary reports whether the secondary mouse button is pressed.
func (s mouseMoveState) secondary() bool {
	return (s.Buttons & 2) != 0
}

// screenCoords (Mechane screen coordinates) are in the range:
//   - +/-0.5 for the Y-axis;
//   - +/-(0.5 * aspect ratio) for the X-axis.
//
// This gives the Mechane screen a height of 1.
//
// In this coordinate system:
//   - distances are proportions of the screen height;
//   - the origin is at the centre of the screen (pixels' display rectangle).
type screenCoords maf.Vec2

// subtract the given vector from the receiver vector.
func (p1 screenCoords) subtract(p2 screenCoords) screenCoords {
	return screenCoords(maf.Vec2(p1).Add(maf.Vec2(p2).Scale(-1)))
}

// A Camera attached to a tree.
type Camera struct {
	Aspect  float64
	Inverse maf.Mat4 // Inverse of the lens (eye-to-clip) transformation.
	Name    string
	Node    *mechtree.Node
}

// screenToEye maps the given Mechane screen coordinates on the screen of the
// receiver camera, and all the points in OpenGL NDC space which are behind
// those screen coordinates, to a single point in eye space.
func (c Camera) screenToEye(p screenCoords) maf.Vec {
	ndc := maf.Vec4{
		// Mechane screen coordinates are in the range +/-0.5 for the Y-axis and
		// +/-(0.5 * aspect ratio) for the X-axis.
		//
		// OpenGL NDC coordinates are in the range +/-1.
		//
		// To map from Mechane screen coordinates to OpenGL NDC coordinates
		// multiply both x and y by 2 but also divide x by the W/H aspect ratio.
		X: p.X * 2 / c.Aspect,
		Y: p.Y * 2,
		Z: 0,
		W: 1,
	}
	e := c.Inverse.Transform(ndc) // Homogeneous since no guarantee inverse transformation gives a w of 1.
	return maf.Vec{X: e.X, Y: e.Y, Z: e.Z}.Scale(1 / e.W)
}

// yawAndPitch angles, in units of radians, which can be applied as local
// rotations to the receiver camera to approximate the effect of making an
// entity originally visible at screen position 1 to appear to have moved to
// screen position 2.
func (c Camera) yawAndPitch(s1, s2 screenCoords) (yaw float64, pitch float64) {
	e1, e2 := c.screenToEye(s1), c.screenToEye(s2)
	grab := e1.To(e2)
	yawRes := grab.Resolution(maf.PosX)
	yaw = e1.Angle(e1.Add(yawRes))
	if yawRes.X < 0 {
		yaw = -yaw
	}
	pitchRes := grab.Resolution(maf.PosY)
	pitch = e1.Angle(e1.Add(pitchRes))
	if pitchRes.Y > 0 {
		pitch = -pitch
	}
	return
}

// AxisPointingUp defines which axis points in the direction of what is
// considered 'up' in the implied reference frame.
//
// Note that the camera's local up direction is always taken as the positive
// Y-axis direction.
//
// The choice of up direction is relevant, for example, when used in conjunction
// with a static roll mode and/or a static elevation mode.
type AxisPointingUp uint

// Up direction unit vector.
func (ax AxisPointingUp) Up() maf.Vec {
	switch ax {
	case X:
		return maf.PosX
	case Z:
		return maf.PosZ
	default:
		return maf.PosY
	}
}

// ordinate value for this axis in the given vector.
func (ax AxisPointingUp) ordinate(v maf.Vec) float64 {
	switch ax {
	case X:
		return v.X
	case Z:
		return v.Z
	default:
		return v.Y
	}
}

// setOrdinate value in the given vector corresponding to this axis.
func (ax AxisPointingUp) setOrdinate(v maf.Vec, o float64) maf.Vec {
	switch ax {
	case X:
		v.X = o
	case Z:
		v.Z = o
	default:
		v.Y = o
	}

	return v
}

const (
	// Let the direction of the positive Y-axis of the parent frame of the
	// camera frame be taken as pointing 'up'.
	Y = AxisPointingUp(iota)
	// Let the direction of the positive X-axis of the parent frame of the
	// camera frame be taken as pointing 'up'.
	X
	// Let the direction of the positive Z-axis of the parent frame of the
	// camera frame be taken as pointing 'up'.
	Z
)

// The elevation mode choice has the potential to constrain the elevation of the
// camera-frame origin relative to the 'up' direction in the camera's parent
// frame.
type elevation uint

const (
	freeElevation = elevation(iota)
	staticElevation
)

type orientation uint

const (
	inwardsOrientation = orientation(iota)
	outwardsOrientation
)

type pitch uint

const (
	freePitch = pitch(iota)

	// constrainedPitch can be combined with a static roll mode to constrain the
	// pitch of the camera-frame to mimic that of a human-like capability, that
	// is, to be able look down to your toes and up to the sky but not beyond
	// those limits.
	constrainedPitch
)

// The radius mode choice has the potential to constrain the distance of the
// camera from a focus.
type radius uint

const (
	freeRadius = radius(iota)
	staticRadius
)

// The roll mode choice has the potential to constrain the rotation of the
// camera around its local Z-axis.
type roll uint

const (
	freeRoll = roll(iota)
	indirectRoll
	staticRoll
)
