package world

import (
	"log"
	"os"
	"runtime"

	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/mechane/mechserver"
	"gitlab.com/fospathi/mechane/mechserver/mechoverlayer"
	"gitlab.com/fospathi/mechane/mechserver/mechuievent"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world/dolly"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/universal/watchable"
	"gitlab.com/fospathi/wire/wiregl"
)

// LoopSpec specifies the desired game loop characteristics of a new standard
// kind of Mechane world.
type LoopSpec struct {
	// The target number of world loops per second.
	PulseRate LoopsPerSec

	// The point in a world loop to start drawing views from cameras.
	RasteriseOclock uint

	// The point in a world loop to process low level UI events from clients.
	UIEventsOclock uint
}

// A Standard kind of Mechane world.
type Standard struct {
	LoopLog *log.Logger

	ButtonClicks  mechuievent.ButtonClicks
	TextfieldKeys mechuievent.TextfieldKeys
	WheelMoves    mechuievent.WheelMoves

	CameraAdjustments CameraAdjustments
	ChronoDial        watchable.ChronoDial
	Stitcher          *wiregl.CPUStitcher
	mechserver.Server

	rasteriseOclock uint
	uiEventsOclock  uint
}

// WatchOclock on the chronodial.
//
// For each chronoshift placed at that position supply it as an argument to the
// given callback function.
//
// To avoid skipping chronoshifts the callback needs to complete before the next
// chronoshift. The callback receives a bool value reporting whether the
// previous callback completed in time; if the bool value is false at least one
// previous callback event was skipped and the chronoshifts for those events
// were missed.
//
// To stop any further callbacks the callback should return a false value.
func (sw Standard) WatchOclock(
	oclock uint,
	f func(watchable.ChronoShift, bool) bool,
) {

	var (
		ok = true
		w  = sw.ChronoDial.WatchOclock(oclock)
	)

	go func() {
		for {
			<-w
			t, err := sw.ChronoDial.AtOclockFor(oclock, w)
			if err != nil {
				sw.LoopLog.Printf("error: world watch oclock: %v: %v",
					oclock, err)
			}
			if !f(t, ok) {
				break
			}
			w, ok = sw.ChronoDial.ReWatchOclock(oclock, w)
		}
	}()
}

// NewStandard constructs a new Standard world.
//
// The world has 12 positions on its chronodial.
func NewStandard(loopSpec LoopSpec, server mechserver.Server) Standard {
	const doz = watchable.Dozens(1)
	period := watchable.Microseconds(1000000 / loopSpec.PulseRate)
	chronoDial := watchable.NewChronoDial(watchable.ChronoDialSpec{
		Dozens:     doz,
		Resolution: period / watchable.Microseconds(doz*12),
	})

	return Standard{
		LoopLog: log.New(os.Stderr, "mechane ", log.LstdFlags),

		ButtonClicks:  mechuievent.NewButtonClicks(),
		TextfieldKeys: mechuievent.NewTextfieldKeys(),
		WheelMoves:    mechuievent.NewWheelMoves(),

		CameraAdjustments: NewCameraAdjustments(),
		ChronoDial:        chronoDial,
		Stitcher:          wiregl.NewCPUStitcher(runtime.NumCPU()),
		Server:            server,

		rasteriseOclock: loopSpec.RasteriseOclock,
		uiEventsOclock:  loopSpec.UIEventsOclock,
	}
}

// RunStandard runs the given standard world.
func RunStandard(wld Standard) {

	var (
		timingOk bool

		cw, _ = wld.CameraAdjustments.seq.Await(nil)
		rw    = wld.ChronoDial.WatchOclock(wld.rasteriseOclock)
		uiw   = wld.ChronoDial.WatchOclock(wld.uiEventsOclock)

		old                = wld.UIEventsCollector.Collections()
		dollies            = dolly.NewDollies()
		unconnectedSources = eqset.New[string]()

		cams = map[string]Camera{} // Keys are camera names.
	)

	for {
		select {

		case <-wld.CameraRegistry.Watch():
			newA, _ := wld.CameraRegistry.Pop()
			a, hasA := wld.CameraRegistry.Associations(newA.Shutterbug)
			if ok := wld.CameraRegistry.Associate(newA); !ok { // Approve the association request.
				continue
			}
			if hasA && newA.Cameras[0] == a.Cameras[0] {
				// Only the major cam is the subject of the UI events so if the
				// major cam is unchanged the dollies and UI events are still
				// valid.
				continue
			}
			// Clear out the UI events and dollies of the sources associated
			// with the shutterbug. The game loop attaches a new dolly when
			// necessary later.
			old.ClearShutterbug(newA.Shutterbug)
			sources := wld.UIEventsCollector.ClearShutterbug(newA.Shutterbug)
			dollies.DeleteSources(sources)
			unconnectedSources.AddSet(sources)

		case <-cw:
			ca, err := wld.CameraAdjustments.seq.Delete(cw)
			if err != nil {
				wld.LoopLog.Printf("loop: camera adjust: %v\n", err)
				continue
			}

			switch ca := ca.(type) {
			case createCamera:
				wld.CameraRegistry.Register(ca.cam.Name)
				cams[ca.cam.Name] = ca.cam
			case deleteCamera:
				wld.CameraRegistry.Deregister(ca.name)
				delete(cams, ca.name)
				unconnectedSources.AddSet(dollies.DeleteCamera(ca.name))
			case updateCamera:
				cams[ca.cam.Name] = ca.cam
				dollies.UpdateCamera(dolly.Camera{
					Aspect:  mech.WToHPixelsRatio,
					Inverse: ca.cam.Inverse,
					Name:    ca.cam.Name,
					Node:    ca.cam.Node,
				})
			}

			cw, _ = wld.CameraAdjustments.seq.Await(nil)

		case <-rw:
			rw, timingOk = wld.ChronoDial.ReWatchOclock(wld.rasteriseOclock, rw)
			if !timingOk {
				wld.LoopLog.Println("loop: rasterise: skipped at least one")
			}

			// Rasterise the views from the cameras.

			type camView struct {
				main   mechoverlayer.Elements
				veneer mechoverlayer.Elements
				pix    wiregl.Pixels
			}
			camViews := map[string]camView{}
			for _, camera := range wld.CameraRegistry.AllAssociatedCameras() {
				cam, ok := cams[camera]
				if !ok {
					continue
				}
				pic, mainEls := mechtree.Localise(cam.Camera)
				ven, venEls := mechtree.Localise(cam.Veneer)
				scene := wiregl.Scene{
					Depiction:       pic,
					Veneer:          ven,
					EyeToClip:       cam.Lens.ToOpenGLMat4(),
					VeneerEyeToClip: cam.Veneer.Lens.ToOpenGLMat4(),
					ClearAlpha:      cam.BackgroundAlpha,
					ClearColour:     cam.BackgroundColour,
					Small:           wld.CameraRegistry.IsPartial(cam.Name),

					Minutiae: wiregl.SceneMinutiae{
						Debug: 0,
					},
				}
				var (
					err error
					pix wiregl.Pixels
				)
				if pix, err = wld.SceneRasteriser.Rasterise(scene); err != nil {
					wld.LoopLog.Printf("loop: rasterise: %v\n", err)
				}
				camViews[camera] = camView{
					main:   mainEls,
					veneer: venEls,
					pix:    pix,
				}
			}
			if len(camViews) == 0 {
				break
			}

			// Stitch camera views into a single view for each shutterbug.

			var (
				rasterisedCams = eqset.FromKeys(camViews)
				stitchedPix    = map[string]wiregl.PixelData{}
			)
		shutterbug_loop:
			for _, sb := range wld.CameraRegistry.AllAssociatedShutterbugs() {
				a, ok := wld.CameraRegistry.Associations(sb)
				if !ok {
					continue
				}
				if !rasterisedCams.ContainsAll(a.Cameras...) {
					continue
				}

				main := camViews[a.Cameras[0]]

				// Reuse cached identical images.
				if pix, ok := stitchedPix[a.Key()]; ok {
					wld.PixelDispenser.Dispense(sb, pix.AsBinary())
					wld.PixelOverlayer.Synchronise(sb, main.main, main.veneer)
					continue
				}

				pixels := []wiregl.Pixels{camViews[a.Cameras[0]].pix}
				for i, l := 1, len(a.Cameras); i < l; i++ {
					camera := a.Cameras[i]
					pix := camViews[camera].pix
					if a.IsPartial(camera) && !pix.ContainsSmall() {
						continue shutterbug_loop
					}
					pixels = append(pixels, pix)
				}

				stitched := a.StitchWithCPU(wld.Stitcher, pixels...)
				stitchedPix[a.Key()] = stitched
				wld.PixelDispenser.Dispense(sb, stitched.AsBinary())
				wld.PixelOverlayer.Synchronise(sb, main.main, main.veneer)
			}

		case <-uiw:
			uiw, timingOk =
				wld.ChronoDial.ReWatchOclock(wld.uiEventsOclock, uiw)
			if !timingOk {
				wld.LoopLog.Println("loop: ui events: skipped at least one")
			}
			delta, _ := wld.ChronoDial.AtOclock(wld.uiEventsOclock)

			events := wld.UIEventsCollector.Collections()

			allSources := events.Sources()
			unconnectedSources = unconnectedSources.Intersection(allSources)
			dollies.KeepSourcesIn(allSources)
			newSources := dollies.UnconnectedSourcesIn(allSources)
			unconnectedSources.AddSet(newSources)
			for src := range unconnectedSources {
				ac := wld.CameraRegistry.Associated(events[src].Shutterbug)
				if len(ac) == 0 {
					continue
				}

				camName := ac[0]
				cam, ok := cams[camName]
				if !ok {
					continue
				}

				dly := cam.Dolly(dolly.Camera{
					Aspect:  mech.WToHPixelsRatio,
					Inverse: cam.Inverse,
					Name:    cam.Name,
					Node:    cam.Node,
				}, src)

				unconnectedSources.Delete(src)
				dollies.Add(dly)
			}

			fresh := events.Fresh(old)
			wld.ButtonClicks.Extract(fresh, events)
			wld.TextfieldKeys.Extract(fresh)
			wld.WheelMoves.Extract(fresh)
			old = events
			dollies.Drive(fresh, delta)
		}
	}
}
