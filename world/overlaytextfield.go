package world

import (
	"errors"
	"fmt"
	"html/template"
	"strings"
	"time"

	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/universal/mech"
)

type Textfield struct {
	// EffectName determines the meaning of the text.
	//
	// The name doesn't need to be unique among textfields, that is, multiple
	// textfields with the same textfield name should cause the same side effect
	// if any among them were to be given a particular text input.
	EffectName string
	// ElementName is unique on a Mechane tree.
	ElementName string

	// Icon1 is placed to the left of the textfield input.
	Icon1 mech.Icon
	// The colour of the icon.
	Icon1Colour string

	// Icon2 is placed to the right of the textfield input.
	Icon2 mech.Icon
	// The colour of the icon.
	Icon2Colour string

	// A button placed at the far right-hand side of the textfield.
	Button Button

	// The value of the textfield input.
	InputValue string
	// The uneditable text after the first icon, if any, and before the
	// textfield input.
	Label string
	// Mask the input if true.
	Mask bool
	// On-screen orientation of the element relative to its in-world
	// orientation.
	Orientation mechtree.OverlayOrientation
	// Position of the node origin on the textfield.
	//
	// Decides which part of the textfield will overlap the onscreen position of
	// its mechtree node's origin.
	OriginPosition mechtree.OverlayOrigin
	// Placeholder text.
	Placeholder string
	// The number of visible characters that the textfield input can show at
	// once.
	//
	// Special cases:
	//   - to use the browser's default leave the size uninitialised (0);
	//   - to actually specify a size of zero, use any negative number.
	Size int
	// Whether to enable the browser's spellcheck squiggle on the input.
	Spellcheck bool
	// Theme to use. Leave blank to use the default theme.
	Theme string
	// Title is the hover text that appears over the textfield.
	Title string

	Cloggable
}

// Overlay the node with a mechane-textfield element.
func (ovl *Textfield) Overlay(node *mechtree.Node) error {
	if ovl.EffectName == "" {
		return errors.New("a textfield name is required")
	}
	if ovl.ElementName == "" {
		return errors.New("a textfield element name is required")
	}

	type textfieldTemplateData struct {
		IsSubmitEffect   bool
		CooldownDuration string

		Button         template.HTML
		EffectName     string
		InputValue     string
		Label          string
		Mask           bool
		OriginPosition string
		Placeholder    string
		Size           int
		Spellcheck     bool
		Theme          string
		Title          string

		IsIcon1     bool
		Icon1Colour string
		Icon1Family string
		Icon1Name   string

		IsIcon2     bool
		Icon2Colour string
		Icon2Family string
		Icon2Name   string
	}

	const buttonSlotName = "button"

	// Apply defaults for optional values.
	if ovl.ClogDuration == 0 {
		ovl.ClogDuration = mech.ClogDuration
	}
	if !mech.IsIconStyle(string(ovl.Icon1.Style)) {
		ovl.Icon1.Style = DefaultIconStyle
	}
	if !mech.IsIconStyle(string(ovl.Icon2.Style)) {
		ovl.Icon2.Style = DefaultIconStyle
	}
	if ovl.OriginPosition == "" {
		ovl.OriginPosition = DefaultOriginPosition
	}
	if ovl.Theme == "" {
		ovl.Theme = DefaultTheme
	}

	// Populate the template's input data.
	var (
		emptyTime = time.Time{}
		tmplData  textfieldTemplateData
	)

	// Non-custom attributes for the custom element.
	tmplData.Title = ovl.Title

	// Custom attributes.
	tmplData.EffectName = ovl.EffectName
	tmplData.InputValue = ovl.InputValue
	if ovl.clogTime != emptyTime {
		tmplData.IsSubmitEffect = true
		tmplData.CooldownDuration =
			fmt.Sprintf("%vms", int(ovl.ClogDuration)/int(time.Millisecond))
	}
	tmplData.Label = ovl.Label
	tmplData.Mask = ovl.Mask
	tmplData.OriginPosition = string(ovl.OriginPosition)
	tmplData.Placeholder = ovl.Placeholder
	tmplData.Size = ovl.Size
	tmplData.Spellcheck = ovl.Spellcheck
	tmplData.Theme = ovl.Theme

	// Icon slots.
	if ovl.Icon1 != (mech.Icon{}) {
		// Custom attributes.
		tmplData.IsIcon1 = true
		tmplData.Icon1Colour = ovl.Icon1Colour
		tmplData.Icon1Family = string(ovl.Icon1.Style)
		// Slot content.
		tmplData.Icon1Name = ovl.Icon1.Ligature
	}
	if ovl.Icon2 != (mech.Icon{}) {
		// Custom attributes.
		tmplData.IsIcon2 = true
		tmplData.Icon2Colour = ovl.Icon2Colour
		tmplData.Icon2Family = string(ovl.Icon2.Style)
		// Slot content.
		tmplData.Icon2Name = ovl.Icon2.Ligature
	}

	// The button.
	if ovl.Button != (Button{}) {
		markup, err := ovl.Button.html(buttonSlotName)
		if err != nil {
			return err
		}
		tmplData.Button = markup
	}

	// Generate the HTML.
	sb := &strings.Builder{}
	if tmpl, err := template.ParseFS(templates, textfieldTemplate); err != nil {
		return err
	} else if err = tmpl.Execute(sb, tmplData); err != nil {
		return err
	}

	node.Overlay(mechtree.OverlayElement{
		HTML:           sb.String(),
		Name:           ovl.ElementName,
		Orientation:    ovl.Orientation,
		OriginPosition: ovl.OriginPosition,
	})

	return nil
}
