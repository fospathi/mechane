package world

import (
	"errors"
	"fmt"
	"html/template"
	"strings"
	"time"

	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mico"
	"gitlab.com/fospathi/universal/mech"
)

type Button struct {
	// EffectName determines the side effect of clicking the button.
	//
	// The name doesn't need to be unique among buttons, that is, multiple
	// buttons with the same button name should cause the same side effect.
	EffectName string
	// ElementName is unique on a Mechane tree.
	ElementName string

	// Icon shown on the button.
	Icon mech.Icon
	// The colour of the icon shown on the button.
	IconColour string
	// On-screen orientation of the element relative to its in-world
	// orientation.
	Orientation mechtree.OverlayOrientation
	// Position of the node origin on the button.
	//
	// Decides which part of the button will overlap the onscreen position of
	// its mechtree node's origin.
	OriginPosition mechtree.OverlayOrigin
	// Text shown on the button.
	Text string
	// Theme to use. Leave blank to use the default theme.
	Theme string
	// Title is the hover text that appears over the button.
	Title string

	Cloggable
}

// html of the button element. The given slot attribute is optional and is
// required when the button is nested in a slot.
func (ovl *Button) html(slot string) (template.HTML, error) {
	if ovl.EffectName == "" {
		return "", errors.New("a button name is required")
	}
	if slot == "" && ovl.ElementName == "" {
		return "", errors.New("a button element name is required")
	}

	type buttonTemplateData struct {
		IsClickEffect    bool
		CooldownDuration string

		IsTabbable bool

		EffectName     string
		OriginPosition string
		Slot           string
		Theme          string
		Title          string

		IsIcon     bool
		IconColour string
		IconFamily string
		IconName   string

		Text string
	}

	// Apply defaults for optional values.
	if ovl.ClogDuration == 0 {
		ovl.ClogDuration = mech.ClogDuration
	}
	if !mech.IsIconStyle(string(ovl.Icon.Style)) {
		ovl.Icon.Style = DefaultIconStyle
	}
	if ovl.OriginPosition == "" {
		ovl.OriginPosition = DefaultOriginPosition
	}
	if ovl.Theme == "" {
		ovl.Theme = DefaultTheme
	}

	// Populate the template's input data.
	var (
		emptyTime = time.Time{}
		tmplData  buttonTemplateData
	)

	// Non-custom attributes for the custom element.
	tmplData.Title = ovl.Title

	// Custom attributes.
	if ovl.clogTime != emptyTime {
		tmplData.IsClickEffect = true
		tmplData.CooldownDuration =
			fmt.Sprintf("%vms", int(ovl.ClogDuration)/int(time.Millisecond))
	}
	tmplData.EffectName = ovl.EffectName
	tmplData.OriginPosition = string(ovl.OriginPosition)
	tmplData.Slot = slot
	tmplData.Theme = ovl.Theme
	if ovl.Icon != (mech.Icon{}) {
		tmplData.IsIcon = true
		tmplData.IconColour = ovl.IconColour
		tmplData.IconFamily = string(ovl.Icon.Style)
		tmplData.IconName = ovl.Icon.Ligature
	}
	tmplData.Text = ovl.Text

	// Generate the HTML.
	sb := &strings.Builder{}
	if tmpl, err := template.ParseFS(templates, buttonTemplate); err != nil {
		return "", err
	} else if err = tmpl.Execute(sb, tmplData); err != nil {
		return "", err
	}

	return template.HTML(sb.String()), nil
}

// Overlay the node with a mechane-button element.
func (btn *Button) Overlay(node *mechtree.Node) error {
	var (
		err    error
		markup template.HTML
	)
	markup, err = btn.html("")
	if err != nil {
		return err
	}

	node.Overlay(mechtree.OverlayElement{
		HTML:           string(markup),
		Name:           btn.ElementName,
		Orientation:    btn.Orientation,
		OriginPosition: btn.OriginPosition,
	})

	return nil
}

type ToggleButton struct {
	Button

	Off           bool
	OffIcon       mech.Icon
	OffIconColour string
	OffTitle      string
}

// Overlay the node with a mechane-button element representing the toggle
// button.
func (btn *ToggleButton) Overlay(node *mechtree.Node) {

	var (
		icon       mech.Icon
		iconColour string
		title      string
	)

	if btn.Off {
		if btn.OffIcon != (mech.Icon{}) {
			icon = btn.OffIcon
		} else {
			icon = mico.Outlined.ToggleOff
		}

		if btn.OffIconColour != "" {
			iconColour = btn.OffIconColour
		} else {
			iconColour = DefaultOffIconColour
		}

		if btn.OffTitle != "" {
			title = btn.OffTitle
		} else {
			title = btn.Title
		}
	} else {
		if btn.Icon != (mech.Icon{}) {
			icon = btn.Icon
		} else {
			icon = mico.Outlined.ToggleOn
		}

		title = btn.Title
	}

	togBtn := btn.Button
	togBtn.Icon = icon
	togBtn.IconColour = iconColour
	togBtn.Title = title
	togBtn.Overlay(node)
}

// Toggle and clog the toggle button.
//
// The toggle button shall not still be clogged.
func (btn *ToggleButton) Toggle(now time.Time) {
	btn.Clog(now)
	btn.Off = !btn.Off
}

type RadioButtonGroupSpec struct {
	// Buttons accessible by button name.
	Buttons map[string]RadioButtonSpec

	// ClogDuration before any of the buttons can be clicked again.
	ClogDuration time.Duration
}

// RadioButtonSpec specifies the properties of a radio button.
type RadioButtonSpec struct {
	// ElementName is unique on a tree.
	ElementName string
	// Icon shown on the button when selected. Leave blank to use the default
	// icon.
	Icon mech.Icon
	// Place the button relative to the node origin.
	OriginPosition mechtree.OverlayOrigin
	// Text shown on the button.
	Text string
	// Theme to use. Leave blank to use the default theme.
	Theme string
	// Title is the hover text that appears over the radio button when selected.
	Title string

	// Icon shown on the button when not selected. Leave blank to use the
	// default icon.
	OffIcon mech.Icon
	// Fill colour of the icon shown on the button when not selected. Leave
	// blank to use the default colour.
	OffIconColour string
	// Title is the hover text that appears over the radio button when not
	// selected.
	OffTitle string
}

// NewRadioButtonGroup constructs a new RadioButtonGroup.
func NewRadioButtonGroup(spec RadioButtonGroupSpec) RadioButtonGroup {
	var (
		defaultOffIcon = mico.Outlined.RadioButtonUnchecked
		defaultOnIcon  = mico.Outlined.RadioButtonChecked

		rg = RadioButtonGroup{}
	)

	for name, bSpec := range spec.Buttons {
		if bSpec.Icon == (mech.Icon{}) {
			bSpec.Icon = defaultOnIcon
		}
		if bSpec.OffIcon == (mech.Icon{}) {
			bSpec.OffIcon = defaultOffIcon
		}

		rg[name] = &ToggleButton{
			Button: Button{
				ElementName: bSpec.ElementName,
				EffectName:  name,

				Cloggable:      Cloggable{ClogDuration: spec.ClogDuration},
				Icon:           bSpec.Icon,
				OriginPosition: bSpec.OriginPosition,
				Text:           bSpec.Text,
				Theme:          bSpec.Theme,
				Title:          bSpec.Title,
			},
			Off:           true,
			OffIcon:       bSpec.OffIcon,
			OffIconColour: bSpec.OffIconColour,
			OffTitle:      bSpec.OffTitle,
		}
	}
	return rg
}

// A RadioButtonGroup with no more than one button activated at a time.
type RadioButtonGroup map[string]*ToggleButton

// Click the button in the group with the given name and clog the group.
//
// The radio button group shall not still be clogged.
func (rg RadioButtonGroup) Click(name string, now time.Time) {
	btn, ok := rg[name]
	if !ok {
		return
	}

	btn.Toggle(now)

	if btn.Off {
		for n, b := range rg {
			if n == name {
				continue
			}
			b.Clog(now)
		}
		return
	}

	for n, b := range rg {
		if n == name {
			continue
		}
		if b.Off {
			b.Clog(now)
		} else {
			b.Toggle(now)
		}
	}
}

// Clogged reports whether the group is still clogged after a click.
func (rg RadioButtonGroup) Clogged() bool {
	for _, btn := range rg {
		return btn.Clogged()
	}
	return false
}

// Declog the button group.
func (rg RadioButtonGroup) Declog() {
	for _, btn := range rg {
		btn.Declog()
	}
}

// Decloggable reports whether the group is decloggable.
//
// Reports false if the cloggable is not currently clogged.
func (rg RadioButtonGroup) Decloggable(now time.Time) bool {
	for _, btn := range rg {
		return btn.Clogged() && now.After(btn.clogTime.Add(btn.ClogDuration))
	}
	return true
}

// Overlay each of the nodes in the argument map with a mechane-button element
// representing the radio group button whose button name is given by the map
// key.
func (rg RadioButtonGroup) Overlay(nodes map[string]*mechtree.Node) {
	for name, node := range nodes {
		btn, ok := rg[name]
		if !ok {
			continue
		}
		btn.Overlay(node)
	}
}

// SelectedName and a true value or, for the no selection case, an empty string
// and a false value.
func (rg RadioButtonGroup) SelectedName() (string, bool) {
	for name, btn := range rg {
		if !btn.Off {
			return name, true
		}
	}
	return "", false
}
