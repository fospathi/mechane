package world

import (
	"embed"
	"time"

	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/universal/mech"
)

var (
	//go:embed *.html
	templates embed.FS
)

const (
	asciidocTemplate  = "overlayasciidoc.html"
	buttonTemplate    = "overlaybutton.html"
	preTemplate       = "overlaypre.html"
	textfieldTemplate = "overlaytextfield.html"
)

const (
	DefaultIconStyle      = mech.RoundStyle
	DefaultOffIconColour  = "darkGray" // Used for button types with off states.
	DefaultOriginPosition = mechtree.TopLeft
	DefaultTheme          = MechaneOrange
)

type Theme = string

const (
	MechaneOrange = Theme("mechane-orange")
)

// A Cloggable can be clogged and then eventually declogged when it becomes
// decloggable.
type Cloggable struct {
	// ClogDuration after cloggage until the cloggable can be declogged.
	ClogDuration time.Duration

	clogTime time.Time
}

// Clog the cloggable.
//
// The cloggable shall not still be clogged.
func (c *Cloggable) Clog(now time.Time) {
	c.clogTime = now
}

// Clogged reports whether the cloggable is clogged and not yet declogged.
func (c *Cloggable) Clogged() bool {
	return c.clogTime != time.Time{}
}

// Declog the cloggable.
func (c *Cloggable) Declog() {
	c.clogTime = time.Time{}
}

// Decloggable reports whether the cloggable is able to be declogged as yet.
//
// Reports false if the cloggable is not currently clogged.
func (c *Cloggable) Decloggable(now time.Time) bool {
	return c.Clogged() &&
		now.After(c.clogTime.Add(c.ClogDuration))
}
