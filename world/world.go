/*
Package world helps developers implement a Mechane world.
*/
package world

import (
	"embed"
	"io/fs"
	"net/http"
	"strings"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/about"
	"gitlab.com/fospathi/mechane/mechserver"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/universal/watchable"
	"gitlab.com/fospathi/wire/colour/rgb"
	"gitlab.com/fospathi/wire/wiregl"
)

var (
	defaultPerspLens = wiregl.NewPerspectiveProjection(
		wiregl.ViewingFrustum{
			Aspect: mech.WToHPixelsRatio,
			FOV:    maf.DegToRad(60),
			F:      10000,
			N:      0.1,
		})

	defaultVeneerLens = wiregl.NewOrthographicProjection(
		wiregl.ViewingBox{
			H: 1,
			W: mech.WToHPixelsRatio,
			F: 1,
			N: 0,
		},
	)

	// DefaultPerspectiveLens for a camera.
	DefaultPerspectiveLens = defaultPerspLens.Matrix

	// DefaultPerspectiveLensInverse inverts the default perspective lens.
	DefaultPerspectiveLensInverse = defaultPerspLens.Inverse

	// DefaultVeneerLens for a camera that displays a veneer over the world view.
	DefaultVeneerLens = defaultVeneerLens.Matrix

	// DefaultVeneerLensInverse inverts the default veneer lens.
	DefaultVeneerLensInverse = defaultVeneerLens.Inverse
)

// DefaultBackgroundColour for the Cameras' pixels.
var DefaultBackgroundColour = rgb.Whitesmoke.Colour()

// LoopsPerSec is the target number of times a world should complete a world
// loop in one second.
type LoopsPerSec uint

// Typical frequencies of a world's loop.
const (
	Fast   = LoopsPerSec(60)
	Medium = LoopsPerSec(30)
	Slow   = LoopsPerSec(5)
)

// HexStr32 is a generator of fixed length strings which consist of 32
// pseudorandom hexadecimal digits with lowercase letters.
//
// Not suitable for security oriented applications requiring true randomness.
var HexStr32 = mech.NewRandHexStr(32)

// LaunchResources for a Mechane world launch.
type LaunchResources struct {
	AboutWorld func() (about.World, error)
	// Run accepts a new Mechane server and uses it to run the Mechane world.
	Run func(server mechserver.Server)
	// WorldWeb provides the static resources made available by the Mechane
	// world.
	WorldWeb http.FileSystem
}

// LaunchResourcesSpec collects the resources needed when launching a Mechane
// world.
type LaunchResourcesSpec[M any] struct {
	// About this world.
	About about.World
	// Init the model of the world.
	Init func(wld Standard) M
	// PulseRate is the number of game loop iterations per second. The model
	// update function is called once per game loop iteration.
	PulseRate LoopsPerSec
	// Update the model of the world on each pulse.
	Update func(
		wld Standard,
		t watchable.ChronoShift,
		arrhythmic bool,
		mdl M,
	) bool
	// WorldWeb provides static HTTP resources for the world.
	WorldWeb embed.FS
}

// NewLaunchResources makes a function which returns a world's launch resources.
//
// Shall be called only once, as shall the returned function.
func NewLaunchResources[M any](
	spec LaunchResourcesSpec[M],
) func() (LaunchResources, error) {

	run := NewStandardRunFunc(spec.Init, spec.Update, spec.PulseRate)
	return func() (LaunchResources, error) {
		root, err := fs.Sub(
			spec.WorldWeb,
			strings.TrimPrefix(mech.WorldWebPath, "/"),
		)
		if err != nil {
			return LaunchResources{}, err
		}
		return LaunchResources{
			AboutWorld: func() (about.World, error) { return spec.About, nil },
			Run:        run,
			WorldWeb:   http.FS(root),
		}, nil
	}
}

// NewStandardRunFunc which accepts a Mechane server and uses it to run a
// Mechane world.
func NewStandardRunFunc[M any](
	initWorld func(wld Standard) M,
	update func(
		wld Standard,
		t watchable.ChronoShift,
		arrhythmic bool,
		mdl M,
	) bool,
	pulse LoopsPerSec,
) func(server mechserver.Server) {

	return func(server mechserver.Server) {
		var (
			wld = NewStandard(LoopSpec{
				PulseRate:       pulse,
				RasteriseOclock: 12,
				UIEventsOclock:  4,
			}, server)
			model = initWorld(wld)
		)

		const pulseOclock = 8
		wld.WatchOclock(
			pulseOclock,
			func(cs watchable.ChronoShift, contiguous bool) bool {
				// Note that contiguous is inverted to get arrhythmic.
				return update(wld, cs, !contiguous, model)
			})

		RunStandard(wld)
	}
}
