package world

import (
	"errors"
	"fmt"
	"html/template"
	"math"
	"strings"

	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/universal/mech"
)

// Pre formatted text overlay element.
type Pre struct {
	// ElementName is unique on a tree.
	ElementName string

	// MaxHeight of the element in units of 'ch'.
	//
	// If used, Mechane constrains this value to be within a reasonable range.
	MaxHeight int
	// Width of the element in units of 'ch'.
	//
	// If used, Mechane constrains this value to be within a reasonable range.
	Width int

	// Buttons placed horizontally at the top of the element.
	Buttons []Button
	// For the case of multiple buttons, this is the number of characters
	// between the leftmost point of successive buttons.
	//
	// Clamped to a minimum value of characters which is just above the physical
	// dimensions of a button including spacing.
	ButtonChars int
	// Preformatted text which is set as the content of a pre element.
	PreText string
	// On-screen orientation of the element relative to its in-world
	// orientation.
	Orientation mechtree.OverlayOrientation
	// Position of the node origin on the pre text.
	//
	// Decides which part of the pre text will overlap the onscreen position of
	// its mechtree node's origin.
	OriginPosition mechtree.OverlayOrigin
	// Theme to use. Leave blank to use the default theme.
	Theme string
	// Title is the hover text that appears over the pre text.
	Title string
}

// Overlay the node with a mechane-pre element.
func (ovl Pre) Overlay(node *mechtree.Node) error {
	if ovl.ElementName == "" {
		return errors.New("a pre element name is required")
	}

	type preTemplateData struct {
		Buttons        []template.HTML
		ButtonChars    int
		MaxHeight      string
		OriginPosition string
		PreText        string
		Theme          string
		Title          string
		Width          string
	}

	const buttonsSlotName = "buttons"

	const (
		defaultWidth = 90
		minWidth     = 10
		maxWidth     = 120

		minButtonChars = 8
	)

	const (
		defaultMaxHeight = defaultWidth / mech.WToHPixelsRatio
		minMaxHeight     = 10
		maxMaxHeight     = maxWidth / mech.WToHPixelsRatio
	)

	// Apply defaults for optional values.
	if ovl.MaxHeight == 0 {
		ovl.MaxHeight = int(math.Round(defaultMaxHeight))
	} else if ovl.MaxHeight < minMaxHeight {
		ovl.MaxHeight = minMaxHeight
	} else if max := int(math.Round(maxMaxHeight)); ovl.MaxHeight > max {
		ovl.MaxHeight = max
	}
	if ovl.Width == 0 {
		ovl.Width = defaultWidth
	} else if ovl.Width < minWidth {
		ovl.Width = minWidth
	} else if ovl.Width > maxWidth {
		ovl.Width = maxWidth
	}
	if ovl.ButtonChars < minButtonChars {
		ovl.ButtonChars = minButtonChars
	}

	if ovl.OriginPosition == "" {
		ovl.OriginPosition = DefaultOriginPosition
	}
	if ovl.Theme == "" {
		ovl.Theme = DefaultTheme
	}

	// Populate the template's input data.
	var (
		tmplData preTemplateData
	)

	// Non-custom attributes for the custom element.
	tmplData.Title = ovl.Title

	// Custom attributes.
	tmplData.MaxHeight = fmt.Sprintf("%vch", ovl.MaxHeight)
	tmplData.OriginPosition = string(ovl.OriginPosition)
	tmplData.Theme = ovl.Theme
	tmplData.Width = fmt.Sprintf("%vch", ovl.Width)

	// A slot for preformatted text.
	tmplData.PreText = ovl.PreText

	// The buttons.
	tmplData.ButtonChars = ovl.ButtonChars
	for _, b := range ovl.Buttons {
		if b != (Button{}) {
			markup, err := b.html(buttonsSlotName)
			if err != nil {
				return err
			}
			tmplData.Buttons = append(tmplData.Buttons, markup)
		}

	}

	// Generate the HTML.
	sb := &strings.Builder{}
	if tmpl, err := template.ParseFS(templates, preTemplate); err != nil {
		return err
	} else if err = tmpl.Execute(sb, tmplData); err != nil {
		return err
	}

	node.Overlay(mechtree.OverlayElement{
		HTML:           sb.String(),
		Name:           ovl.ElementName,
		Orientation:    ovl.Orientation,
		OriginPosition: ovl.OriginPosition,
	})

	return nil
}
