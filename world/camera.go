package world

import (
	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world/dolly"
	"gitlab.com/fospathi/universal/mech"
)

// A Camera to view a Mechane world.
type Camera struct {
	mechtree.Camera
	Dolly            dolly.DollyMaker
	Veneer           mechtree.Camera // The veneer is the view from this camera.
	BackgroundColour mech.Colour     // The background colour.
	BackgroundAlpha  float64         // The background colour opacity.
}

// NewCameraAdjustments constructs a new CameraAdjustments.
func NewCameraAdjustments() CameraAdjustments {
	return CameraAdjustments{
		seq: dryad.NewSyncAwaitSeq[any](),
	}
}

// CameraAdjustments is a queue to which you can add requests for a Mechane
// world to create, update, and delete cameras.
//
// The Mechane world will process the camera requests when the world loop is not
// busy.
type CameraAdjustments struct {
	seq *dryad.SyncAwaitSeq[any]
}

// Create appends a create camera request.
func (adj CameraAdjustments) Create(cam Camera) {
	adj.seq.Append(createCamera{cam})
}

// Delete appends a delete camera request.
func (adj CameraAdjustments) Delete(name string) {
	adj.seq.Append(deleteCamera{name})
}

// Update appends an update camera request.
func (adj CameraAdjustments) Update(cam Camera) {
	adj.seq.Append(updateCamera{cam})
}

type createCamera struct {
	cam Camera
}

type deleteCamera struct {
	name string
}

type updateCamera struct {
	cam Camera
}
