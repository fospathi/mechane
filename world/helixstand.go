package world

import (
	"math"
	"slices"
	"sync"
	"time"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

const HelixStandAnimationFPS = 60

// HelixStandSpec specifies the characteristics of a HelixStand.
type HelixStandSpec struct {
	// A unique name.
	ElementName string

	// Angle in radians between consecutive spokes as seen in the XY-plane.
	A float64
	// Pitch of the helix.
	P float64
	// Phase angle is the anticlockwise angle in radians between the positive
	// X-axis and the spoke of the first stand position in the Z=0 XY-plane.
	Phase float64
	// Radius of the helix and the length of the spokes extending
	// perpendicularly from the axis to a stand position.
	R float64
	// Time taken to move the other items on the stand into their new position
	// when another item is added or removed.
	T time.Duration

	downward     bool
	spokePortion mech.Portion
	to           int           // Destination stand position.
	tick         time.Duration // Duration between animation frames.
	totalTicks   int           // Animation frames in an uninterrupted animation.
}

// coordsAt the given stand position index.
//
// The first stand position has index 0.
func (spec HelixStandSpec) coordsAt(pos int) maf.Vec {
	ang := spec.Phase + float64(pos)*spec.A
	s, c := math.Sincos(ang)
	return maf.Vec{
		X: spec.R * c,
		Y: spec.R * s,
		Z: float64(pos) * spec.unitPitch(),
	}
}

// unitPitch is the pitch between successive stand positions.
func (spec HelixStandSpec) unitPitch() float64 {
	return (spec.A / maf.TwoPi) * spec.P
}

// HelixStand organises items by placing them along the path of a helix curve.
//
// The helix curves around the positive Z-axis. The first stand position, which
// can be adjusted with the phase, is always in the Z=0 XY-plane.
type HelixStand[T any] struct {
	Node *mechtree.Node

	m    sync.Mutex
	spec HelixStandSpec
	vItm dryad.ModSlice[helixStandItem[T]]
}

type helixStandItem[T any] struct {
	c    chan HelixStandSpec
	item T
	line *wire.LineSeg
	node *mechtree.Node
}

// Add an item to the stand by occupying the first stand position in the Z=0
// plane and move the other items along the helix by one position.
func (stand *HelixStand[T]) Add(item T) *mechtree.Node {
	stand.m.Lock()
	defer stand.m.Unlock()

	for i, itm := range stand.vItm {
		spec := stand.spec
		spec.to = i + 1
		itm.c <- spec
	}

	// Depict a spoke going from the Helix axis to the stand position.
	name := stand.spec.ElementName + "Spoke" + "-" + HexStr32.String()
	spoke := wire.NewLineSeg(maf.LineSeg{
		P1: maf.Zero,
		P2: stand.spec.coordsAt(0).Scale(-1),
	})
	spoke.SetName(name)
	spoke.Depict(stand.spec.spokePortion)

	node := mechtree.NewNode()
	node.Orient(maf.NewFrame(stand.spec.coordsAt(0)))
	node.AddDepictioner(spoke)
	stand.Node.AddChild(node)

	c := make(chan HelixStandSpec)
	stand.vItm = slices.Insert(stand.vItm, 0, helixStandItem[T]{
		c: c, item: item, line: spoke, node: node,
	})
	go nodeMover(c, node, spoke)
	return node
}

// Delete the given node and therefore its stand position from the helix stand.
//
// The stand position of the given node can be any stand position on the helix
// stand.
//
// Stand positions that come after the deleted position are moved down by one
// place to fill the gap.
func (stand *HelixStand[T]) Delete(node *mechtree.Node) {
	stand.m.Lock()
	defer stand.m.Unlock()

	i := slices.IndexFunc(stand.vItm, func(itm helixStandItem[T]) bool {
		return itm.node == node
	})
	if i == -1 {
		return
	}
	stand.vItm[i].node.Detach()
	close(stand.vItm[i].c)
	stand.vItm = slices.Delete(stand.vItm, i, i+1)
	for l := len(stand.vItm); i < l; i++ {
		spec := stand.spec
		spec.to = i
		stand.vItm[i].c <- spec
	}
}

// ElementName assigned to the HelixStand at creation.
func (stand *HelixStand[T]) ElementName() string {
	stand.m.Lock()
	defer stand.m.Unlock()

	return stand.spec.ElementName
}

// Len is the number of items on the helix stand.
func (stand *HelixStand[T]) Len() int {
	stand.m.Lock()
	defer stand.m.Unlock()

	return len(stand.vItm)
}

// ShiftByOne position the items on the helix stand.
//
// A true value moves the items towards the base and fills the rear gap from the
// base position. A false value operates in reverse, pushing the items away from
// the base and filling the base gap from the rear position.
//
// If the helix stand is empty or contains only a single item this is a no-op.
func (stand *HelixStand[T]) ShiftByOne(downward bool) {
	stand.m.Lock()
	defer stand.m.Unlock()

	l := len(stand.vItm)
	if l <= 1 {
		return
	}
	n := l - 1
	if !downward {
		n = 1
	}

	for i, itm := range stand.vItm {
		spec := stand.spec
		spec.downward = downward
		spec.to = (i + n) % l
		itm.c <- spec
	}

	stand.vItm.ShiftByOne(!downward)
}

// NewHelixStand constructs a new HelixStand.
func NewHelixStand[T any](spec HelixStandSpec) *HelixStand[T] {
	// Defaults.
	if spec.T == 0 {
		spec.T = 1 * time.Second
	}

	// Private animation parameters.
	spec.tick = (1 * time.Second) / HelixStandAnimationFPS
	spec.totalTicks = int(spec.T / spec.tick)

	spec.spokePortion = mech.Portion{
		In:       interval.In{0, 1},
		Colourer: style.Black,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	}

	return &HelixStand[T]{
		Node: mechtree.NewNode(),

		m:    sync.Mutex{},
		spec: spec,
	}
}

func nodeMover(
	c chan HelixStandSpec,
	node *mechtree.Node,
	spoke *wire.LineSeg,
) {

	var (
		fromA, fromZ, deltaA, deltaZ float64
		ok                           bool
		spec                         HelixStandSpec
		ticks                        int
		ticker                       *time.Ticker
		tickerC                      <-chan time.Time
	)
	for {
		select {
		case spec, ok = <-c:
			if !ok {
				return
			}
			if ticker != nil {
				ticker.Stop()
			}
			ticks = 0
			ticker = time.NewTicker(spec.tick)
			tickerC = ticker.C
			from := node.Get().O
			fromA, fromZ = math.Atan2(from.Y, from.X), from.Z
			to := spec.coordsAt(spec.to)
			deltaZ = to.Z - fromZ
			if spec.downward { // By convention clockwise is the down direction.
				deltaA = -from.Lower().Clockwise(to.Lower())
			} else {
				deltaA = from.Lower().Anticlockwise(to.Lower())
			}

		case <-tickerC:
			if ticks >= spec.totalTicks-1 {
				// Use exact positions for the final frame.
				pos := spec.coordsAt(spec.to)
				node.Orient(maf.Frame{
					O:    pos,
					Axes: maf.StdBasis,
				})
				// Spokes use their local node's coordinate system.
				spoke.P2 = maf.Vec{X: -pos.X, Y: -pos.Y, Z: 0}
				spoke.Depict(spec.spokePortion)
				ticker.Stop()
				continue
			}
			rat := float64(ticks) / float64(spec.totalTicks)
			a, z := fromA+rat*deltaA, fromZ+rat*deltaZ
			s, c := math.Sincos(a)
			x, y := spec.R*c, spec.R*s
			node.Orient(maf.Frame{
				O:    maf.Vec{X: x, Y: y, Z: z},
				Axes: maf.StdBasis,
			})
			// Spokes use their local node's coordinate system.
			spoke.P2 = maf.Vec{X: -x, Y: -y, Z: 0}
			spoke.Depict(spec.spokePortion)
			ticks++
		}
	}
}
