package world

import (
	"errors"
	"fmt"
	"html/template"
	"math"
	"strings"

	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/universal/mech"
)

type Asciidoc struct {
	// ElementName is unique on a tree.
	ElementName string

	// MaxHeight of the element in units of 'ch'.
	//
	// If used, Mechane constrains this value to be within a reasonable range.
	MaxHeight int
	// Width of the element in units of 'ch'.
	//
	// If used, Mechane constrains this value to be within a reasonable range.
	Width int

	// AsciiDocHTML is AsciiDoc in HTML format.
	//
	// This should be known safe or purified HTML as it is inserted verbatim
	// into the document.
	AsciiDocHTML string
	// AsciidoctorDefaultStyle enables the official Asciidoctor default style
	// sheet if true.
	AsciidoctorDefaultStyle bool
	// On-screen orientation of the element relative to its in-world
	// orientation.
	Orientation mechtree.OverlayOrientation
	// Position of the node origin on the asciidoc.
	//
	// Decides which part of the asciidoc will overlap the onscreen position of
	// its mechtree node's origin.
	OriginPosition mechtree.OverlayOrigin
	// Theme to use. Leave blank to use the default theme.
	Theme string
	// Title is the hover text that appears over the asciidoc.
	Title string
}

// Overlay the node with a mechane-asciidoc element.
func (ovl Asciidoc) Overlay(node *mechtree.Node) error {
	if ovl.ElementName == "" {
		return errors.New("an asciidoc element name is required")
	}

	type asciidocTemplateData struct {
		AsciiDocHTML            template.HTML
		AsciidoctorDefaultStyle bool
		MaxHeight               string
		OriginPosition          string
		Theme                   string
		Title                   string
		Width                   string
	}

	const (
		defaultWidth = 90
		minWidth     = 10
		maxWidth     = 120
	)

	const (
		defaultMaxHeight = defaultWidth / mech.WToHPixelsRatio
		minMaxHeight     = 10
		maxMaxHeight     = maxWidth / mech.WToHPixelsRatio
	)

	// Apply defaults for optional values.
	if ovl.MaxHeight == 0 {
		ovl.MaxHeight = int(math.Round(defaultMaxHeight))
	} else if ovl.MaxHeight < minMaxHeight {
		ovl.MaxHeight = minMaxHeight
	} else if max := int(math.Round(maxMaxHeight)); ovl.MaxHeight > max {
		ovl.MaxHeight = max
	}
	if ovl.Width == 0 {
		ovl.Width = defaultWidth
	} else if ovl.Width < minWidth {
		ovl.Width = minWidth
	} else if ovl.Width > maxWidth {
		ovl.Width = maxWidth
	}

	if ovl.OriginPosition == "" {
		ovl.OriginPosition = DefaultOriginPosition
	}
	if ovl.Theme == "" {
		ovl.Theme = DefaultTheme
	}

	// Populate the template's input data.
	var (
		tmplData asciidocTemplateData
	)

	// Non-custom attributes for the custom element.
	tmplData.Title = ovl.Title

	// Custom attributes.
	tmplData.MaxHeight = fmt.Sprintf("%vch", ovl.MaxHeight)
	tmplData.OriginPosition = string(ovl.OriginPosition)
	tmplData.Theme = ovl.Theme
	tmplData.Width = fmt.Sprintf("%vch", ovl.Width)

	// A slot for AsciiDoc that has been converted to HTML.
	tmplData.AsciiDocHTML = template.HTML(ovl.AsciiDocHTML)
	tmplData.AsciidoctorDefaultStyle = ovl.AsciidoctorDefaultStyle

	// Generate the HTML.
	sb := &strings.Builder{}
	if tmpl, err := template.ParseFS(templates, asciidocTemplate); err != nil {
		return err
	} else if err = tmpl.Execute(sb, tmplData); err != nil {
		return err
	}

	node.Overlay(mechtree.OverlayElement{
		HTML:           sb.String(),
		Name:           ovl.ElementName,
		Orientation:    ovl.Orientation,
		OriginPosition: ovl.OriginPosition,
	})

	return nil
}
