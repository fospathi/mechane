# Package mechui

```go
import "gitlab.com/fospathi/mechane/mechui"
```

Go package mechui provides:

- the _web_ directory: the root directory of the HTTP server's filesystem with the various HTML, CSS, font, and JS files needed by a Mechane server to display the browser based UI;

- Go package asciidoc: an asciidoc to HTML converter which depends on [Asciidoctor.js](https://docs.asciidoctor.org/asciidoctor.js/latest/).


## The Elm and Typescript integrants

The _web_ directory has a few sibling directories among which are the _elm_ and _typescript_ siblings.

The Mechane browser based UI is composed of two interdependent integrants: an Elm language integrant and a Typescript language integrant.

The integrants are compiled to JS which is placed in the _script_ directory under _web_.

### Compilation

Assuming global installations of the Elm compiler (elm), the Typescript compiler (tsc), and the Node Package Manager (npm), then the commands to compile the integrants and replace the JS files in the _script_ directory are as follows:

From the (_web_ sibling) _elm_ directory:

    elm make src/Main.elm --output ../web/script/elm-integrant.js

Followed by, from the (_web_ sibling) _typescript_ directory:

    npm ci
    npm run build

## Go package asciidoc

Automate the process of converting asciidoc files to HTML files which can then be incorporated into a Mechane world.

Package asciidoc recognises files as asciidoc if they use the `.asciidoc` filename extension.

### Go generate

Package asciidoc can be combined with the `go generate` command to convert all of a world's asciidoc anywhere in its filesystem hierarchy to HTML. For example, add a _doc_ directory to the world's root directory which contains the following two files:

- _a2h.go_ 

    This file performs the asciidoc to HTML conversions:


    ```go
    //go:build ignore

    package main // Convert asciidoc files to HTML.

    import (
        "log"

        "gitlab.com/fospathi/mechane/mechui/asciidoc"
    )

    const rootDir = "../" // Convert asciidoc files in the filesystem rooted here.

    func main() {
        if cnv, err := asciidoc.NewConverter(log.Default()); err != nil {
            log.Fatalln(err)
        } else if err := asciidoc.Convert(cnv, rootDir); err != nil {
            log.Fatalln(err)
        }
    }
    ```

- _doc.go_

    This file enables the use of `go generate` to convert the asciidoc:
    ```go
    package doc

    import _ "gitlab.com/fospathi/mechane/mechui/asciidoc"

    //go:generate go run a2h.go
    ```
    Run it from either:
    - the world's root directory (or any directory above it) with `go generate ./...`;
    - the _doc_ directory with `go generate`.