/* 
eslint-disable
    no-undef,
    @typescript-eslint/no-var-requires,
*/

const path = require("path")

module.exports = {
    devtool: "source-map",
    entry: "./build/main.js",
    experiments: {
        futureDefaults: true,
    },
    mode: "development",
    output: {
        path: path.resolve(__dirname, "../web/script"),
        filename: "typescript-integrant.js",
    },
}
