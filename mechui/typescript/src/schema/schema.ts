import { z } from "zod"

// A base64url encoding of 256 random bits: 256 bits maps to 44 characters
// including the padding.
export type Shutterbug = z.infer<typeof shutterbug>
export const shutterbug = z
    .string()
    .length(44)
    .regex(/^[\w-]{43}=$/)

// An object with the dimensions of the binary image data in units of pixels and
// the fullscreen status of the tab.
export interface PixelsMeta {
    w: number
    h: number
    fullscreen: boolean
    pointerLock: boolean
}

export type Motif = z.infer<typeof motif>
export const motif = z.object({
    motifName: z.string(),

    accent: z.string(),
    boxShadow: z.string(),
    buttonBackground: z.string(),
    buttonOpaqueBackground: z.string(),
    error: z.string(),
    primary: z.string(),
    primaryGlow: z.string(),
    primaryLight: z.string(),
    ready: z.string(),
    textNormal: z.string(),
    textPassive: z.string(),
    warning: z.string(),

    boxShadowGlow: z.string(),
    buttonHoverBackground: z.string(),
    disabled: z.string(),
    inputBorderFocus: z.string(),
    inputBorderPrimary: z.string(),
    preBackground: z.string(),
    stitchingMajor: z.string(),
    stitchingMinor1: z.string(),
    stitchingMinor2: z.string(),
    stitchingMinor3: z.string(),

    buttonBorder: z.string(),
    buttonBorderRadius: z.string(),
    buttonIconFontSize: z.string(),
    buttonPadding: z.string(),
})

export type LifeCycle = Problem | Stage
interface Problem {
    problem:
        | "failed to get the Typescript motif"
        | "failed to create a canvas rendering context"
        | "failed to create a shadowRoot"
}
interface Stage {
    stage: "all WebSockets are open"
}

// An object summarising the devoir state.
export type Devoir = z.infer<typeof devoir>
export const devoir = z.object({
    isPaused: z.boolean(),
})

// An object describing a devoir related UI command.
export type UICommand = z.infer<typeof uiCommand>
export const uiCommand = z.object({
    uiCommand: z.enum(["pause", "play", "shutdown"]),
})

export const stitching = z.enum([
    "uni",
    "bi",
    "biPartial",
    "tri",
    "triPartial",
    "quad",
    "quadPartial",
] as const)

export type CameraAssociations = z.infer<typeof cameraAssociations>
export const cameraAssociations = z.object({
    cameras: z.string().array(),
    stitching: stitching,
})

export type RegisteredAssociatedCameras = z.infer<
    typeof registeredAssociatedCameras
>
export const registeredAssociatedCameras = z.object({
    associated: z.string().array(),
    registered: z.string().array(),
    stitching: stitching,
})

export type OverlayItemElementProps = z.infer<typeof overlayItemElementProps>
export const overlayItemElementProps = z.object({
    innerHTML: z.string(),
    innerHTMLHash: z.string(),
    // Style properties.
    left: z.string(),
    top: z.string(),
    transform: z.string(),
    transformOrigin: z.string(),
    zIndex: z.number(),
})

export type OverlayItemUpdate = z.infer<typeof overlayItemUpdate>
export const overlayItemUpdate = overlayItemElementProps.extend({
    name: z.string(),
})

// An update to the shutterbug-pixels' overlay.
export type OverlayUpdate = z.infer<typeof overlayUpdate>
export const overlayUpdate = z.union([
    z.object({ create: z.string().array() }),
    z.object({ delete: z.string().array() }),
    z.object({ hide: z.string().array() }),
    z.object({ update: z.array(overlayItemUpdate) }),
])

export type MechaneUIEvent =
    | KeyboardUIEvent
    | MouseButtonUIEvent
    | MouseMoveUIEvent
    | MouseWheelUIEvent
    | MechaneButtonUIEvent
    | MechaneTextfieldKeyUpOrDownUIEvent

export const normalCloseReasons = z.enum([
    "received client request",
    "intercepted terminal interrupt",
] as const)

export interface WebSocketCloseEvent {
    code: number
    reason: string
    sourceWebSocket: string
    wasClean: boolean
}

export interface KeyboardUIEvent {
    keyboard: {
        altKey: boolean
        ctrlKey: boolean
        isComposing: boolean
        key: string // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key
        keyDown: boolean
        metaKey: boolean
        pressed: Array<string>
        repeat: boolean
        shiftKey: boolean
    }
}

export interface MouseButtonUIEvent {
    mouseButton: {
        altKey: boolean
        button: number // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
        clientX: number // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/clientX
        clientY: number
        ctrlKey: boolean
        detail: number // https://developer.mozilla.org/en-US/docs/Web/API/UIEvent/detail
        metaKey: boolean
        mouseDown: boolean
        shiftKey: boolean
        windowH: number // https://developer.mozilla.org/en-US/docs/Web/API/Window/innerHeight
        windowW: number
    }
}

export interface MouseMoveUIEvent {
    mouseMove: {
        activeEditable: boolean // True if the hover is on a focused text editing area or similar.
        altKey: boolean
        buttons: number // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/buttons
        clientX: number // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/clientX
        clientY: number
        ctrlKey: boolean
        metaKey: boolean
        movementX: number // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/movementX
        movementY: number
        pixelsH: number
        pixelsW: number
        pixelsOffsetX: number // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/offsetX
        pixelsOffsetY: number
        pointerLock: boolean
        shiftKey: boolean
        windowH: number // https://developer.mozilla.org/en-US/docs/Web/API/Window/innerHeight
        windowW: number
    }
}

export interface MouseWheelUIEvent {
    mouseWheel: {
        altKey: boolean
        clientX: number // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/clientX
        clientY: number
        ctrlKey: boolean
        deltaX: number
        deltaY: number
        effectName: string
        metaKey: boolean
        shiftKey: boolean
        windowH: number // https://developer.mozilla.org/en-US/docs/Web/API/Window/innerHeight
        windowW: number
    }
}

export interface MechaneButtonUIEvent {
    mechaneButton: {
        altKey: boolean
        button: number // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
        clientX: number // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/clientX
        clientY: number
        ctrlKey: boolean
        detail: number // https://developer.mozilla.org/en-US/docs/Web/API/UIEvent/detail
        effectName: string
        metaKey: boolean
        shiftKey: boolean
        windowH: number // https://developer.mozilla.org/en-US/docs/Web/API/Window/innerHeight
        windowW: number
    }
}

export interface MechaneTextfieldKeyUpOrDownUIEvent {
    mechaneTextfieldKeyUpOrDown: {
        altKey: boolean
        ctrlKey: boolean
        effectName: string
        isComposing: boolean
        key: string // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key
        keyDown: boolean
        metaKey: boolean
        shiftKey: boolean
        value: string
    }
}
