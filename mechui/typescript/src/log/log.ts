export function log(...parts: string[]) {
    console.log(parts.join(": "))
}
