import type * as CSS from "csstype"
import { stringify } from "@stitches/stringify"

export type Sheet<T extends CSS.Properties = CSS.Properties> = Array<Rule<T>>

export interface Rule<T extends CSS.Properties = CSS.Properties> {
    selector: string
    properties: T
}

export interface KeyframesProperties extends CSS.Properties {
    from?: CSS.Properties
    to?: CSS.Properties
    "0.1%"?: CSS.Properties
    "1%"?: CSS.Properties
    "5%"?: CSS.Properties
    "10%"?: CSS.Properties
    "20%"?: CSS.Properties
    "30%"?: CSS.Properties
    "40%"?: CSS.Properties
    "50%"?: CSS.Properties
    "60%"?: CSS.Properties
    "70%"?: CSS.Properties
    "80%"?: CSS.Properties
    "90%"?: CSS.Properties
    "95%"?: CSS.Properties
    "99%"?: CSS.Properties
    "99.9%"?: CSS.Properties
}

export interface FontFaceProperties extends CSS.Properties {
    src: string
}

export function toString<T extends CSS.Properties = CSS.Properties>(
    sheet: Sheet<T>,
    prettify = false
): string {
    const s = sheet
        .map<string>((rule: Rule<T>) => {
            return rule.selector + " " + `{\n${stringify(rule.properties)}\n}\n`
        })
        .join("\n")
    if (prettify) {
        return s.replaceAll(";", ";\n")
    }
    return s
}
