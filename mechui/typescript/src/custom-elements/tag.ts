// Custom element tag names.
export const customTag = {
    // Custom tags used as overlay items.
    overlay: {
        asciidoc: "mechane-asciidoc",
        button: "mechane-button",
        pre: "mechane-pre",
        textfield: "mechane-textfield",
    },
    shutterbugPixels: "shutterbug-pixels",
} as const

// A common suffix for the per tag name attributes used by overlay items which
// have a side effect.
//
// The full attribute name is formed by appending this suffix to the tag name.
export const effectNameAttrSuffix = "-effect-name"
