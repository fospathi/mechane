import type * as CSS from "csstype"

import { type OriginPosition, borderRadiuses } from "../style/origin-position"
import { theme } from "../style/theme"

// Container element.

const containerID = "mechane-asciidoc-container"

function containerBase({
    maxHeight,
    originPosition,
    width,
}: {
    maxHeight: string
    originPosition: OriginPosition
    width: string
}): CSS.Properties {
    return {
        background: theme.buttonBackground,
        borderColor: theme.primary,
        borderRadius: borderRadiuses.get(originPosition),
        borderTopStyle: "none",
        borderRightStyle: "solid",
        borderBottomStyle: "none",
        borderLeftStyle: "solid",
        borderWidth: theme.buttonBorderRadius,
        boxShadow: theme.boxShadow,
        maxHeight: maxHeight,
        overflow: "auto",
        padding: theme.buttonPadding,
        pointerEvents: "auto",
        position: "relative",
        width: width,
    }
}

// All the CSS.

export const css = {
    container: {
        id: containerID,
        selector: `#${containerID}`,
        style: containerBase,
    },
    asciidocSlot: {
        slotName: "asciidoc",
    },
}
