import { type Rule, type Sheet, toString } from "../../../crule/crule"
import { materialIcons } from "../../font"
import { css } from "./mechane-asciidoc.css"
import { getOriginPosition } from "../style/origin-position"
import { theme } from "../style/theme"
import { customTag } from "../../tag"

// A MechaneAsciidoc custom element provides a slot for the HTML output of the
// Asciidoctor.js AsciiDoc processor.
export class MechaneAsciidoc extends HTMLElement {
    // The tag name of this custom element.
    static readonly tag = customTag.overlay.asciidoc

    // Attributes set and unset by the Mechane server.
    //
    // When such an attribute is changed a whole new element is sent to replace
    // the existing element. Thus the connectedCallback() is where the logic for
    // such changes takes place.

    static readonly attribute = {
        maxHeight: `${MechaneAsciidoc.tag}-max-height`,
        originPosition: `${MechaneAsciidoc.tag}-origin-position`,
        theme: `${MechaneAsciidoc.tag}-theme`,
        width: `${MechaneAsciidoc.tag}-width`,
    } as const

    // Descendants of the shadowRoot.

    readonly container = document.createElement("div")
    readonly asciidocSlot = document.createElement("slot")

    constructor() {
        // Mandatory super call.

        super()

        // Instance fields.

        this.asciidocSlot.setAttribute("name", css.asciidocSlot.slotName)
        this.container.append(this.asciidocSlot)
        this.container.id = css.container.id

        // Shadow root and style.

        const shadowRoot = this.attachShadow({ mode: "open" })
        const style = document.createElement("style")
        shadowRoot.append(style, this.container)
    }

    attributeChangedCallback() {
        this.updateStyle()
    }

    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return
        }

        this.updateStyle()
    }

    disconnectedCallback() {
        return
    }

    updateStyle() {
        const style = this.shadowRoot?.querySelector("style")
        if (!style) {
            return
        }

        // if (
        //     this.getAttribute(MechaneAsciidoc.attribute.theme) !=
        //     theme.motifName
        // ) {
        //     return
        // }

        const originPosition =
            getOriginPosition(
                this.getAttribute(MechaneAsciidoc.attribute.originPosition)
            ) ?? theme.defaultOriginPosition

        const maxHeight =
            this.getAttribute(MechaneAsciidoc.attribute.maxHeight) ?? "60ch"

        const width =
            this.getAttribute(MechaneAsciidoc.attribute.width) ?? "100ch"

        // Accumulate CSS rules in a style sheet.
        const sheet: Sheet = new Array<Rule>()

        // Include the font styles. Even though the font rules are defined
        // globally, inside a custom element the font rules need to be defined
        // again.
        sheet.push(...materialIcons)

        // Container element.
        sheet.push({
            selector: css.container.selector,
            properties: css.container.style({
                maxHeight: maxHeight,
                originPosition: originPosition,
                width: width,
            }),
        })

        // Apply the accumulated CSS rules.
        style.textContent = toString(sheet)
    }
}
