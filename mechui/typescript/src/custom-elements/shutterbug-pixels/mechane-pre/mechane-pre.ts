import { type Rule, type Sheet, toString } from "../../../crule/crule"
import { css } from "./mechane-pre.css"
import { getOriginPosition } from "../style/origin-position"
import { theme } from "../style/theme"
import { customTag } from "../../tag"

// A MechanePre custom element provides a slot for preformatted text.
export class MechanePre extends HTMLElement {
    // The tag name of this custom element.
    static readonly tag = customTag.overlay.pre

    // Attributes set and unset by the Mechane server.
    //
    // When such an attribute is changed a whole new element is sent to replace
    // the existing element. Thus the connectedCallback() is where the logic for
    // such changes takes place.

    static readonly attribute = {
        maxHeight: `${MechanePre.tag}-max-height`,
        originPosition: `${MechanePre.tag}-origin-position`,
        theme: `${MechanePre.tag}-theme`,
        width: `${MechanePre.tag}-width`,
    } as const

    // Descendants of the shadowRoot.

    readonly container = document.createElement("div")
    readonly preSlot = document.createElement("slot")
    readonly buttonsSlot = document.createElement("slot")

    constructor() {
        // Mandatory super call.

        super()

        // Instance fields.

        this.buttonsSlot.setAttribute("name", css.buttonsSlot.slotName)
        this.preSlot.setAttribute("name", css.preSlot.slotName)
        this.container.append(this.buttonsSlot, this.preSlot)
        this.container.id = css.container.id

        // Shadow root and style.

        const shadowRoot = this.attachShadow({ mode: "open" })
        const style = document.createElement("style")
        shadowRoot.append(style, this.container)
    }

    attributeChangedCallback() {
        this.updateStyle()
    }

    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return
        }

        this.updateStyle()
    }

    disconnectedCallback() {
        return
    }

    updateStyle() {
        const style = this.shadowRoot?.querySelector("style")
        if (!style) {
            return
        }

        // if (this.getAttribute(MechanePre.attribute.theme) != theme.motifName) {
        //     return
        // }

        const originPosition =
            getOriginPosition(
                this.getAttribute(MechanePre.attribute.originPosition)
            ) ?? theme.defaultOriginPosition

        const maxHeight =
            this.getAttribute(MechanePre.attribute.maxHeight) ?? "60ch"

        const width = this.getAttribute(MechanePre.attribute.width) ?? "100ch"

        // Accumulate CSS rules in a style sheet.
        const sheet: Sheet = new Array<Rule>()

        // Container element.
        sheet.push({
            selector: css.container.selector,
            properties: css.container.style({
                maxHeight: maxHeight,
                originPosition: originPosition,
                width: width,
            }),
        })

        // Apply the accumulated CSS rules.
        style.textContent = toString(sheet)
    }
}
