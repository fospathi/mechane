import { type Rule, type Sheet, toString } from "../../../crule/crule"
import { newMechaneButtonUIEvent } from "./event"
import {
    isMaterialIconsFamily,
    materialIcons,
    materialIconsClass,
    setMaterialIconsClass,
} from "../../font"
import type { MechaneButtonClickListener } from "./listen"
import { log } from "../../../log/log"
import { css } from "./mechane-button.css"
import { disabledAttribute } from "../overlay"
import { ShutterbugPixels } from "../shutterbug-pixels"
import { getOriginPosition } from "../style/origin-position"
import { theme } from "../style/theme"
import { customTag, effectNameAttrSuffix } from "../../tag"

// A MechaneButton custom element is a button which can be clicked.
//
// Upon being clicked it makes a mechaneButton object summarising the event
// which is sent to the Mechane server along with the name attribute of the
// button that generated it.
export class MechaneButton extends HTMLElement {
    // The tag name of this custom element.
    static readonly tag = customTag.overlay.button

    // Attributes set by the client.
    //
    // The logic for handling a change of these attributes happens in the
    // attributeChangedCallback().

    static get observedAttributes() {
        // Disabled is set by the client when the overlay is permanently
        // disabled. This happens for a Mechane server shutdown or any other
        // connection loss.
        return [disabledAttribute]
    }

    // Attributes set and unset by the Mechane server.
    //
    // When such an attribute is changed a whole new element is sent to replace
    // the existing element. Thus the connectedCallback() is where the logic for
    // such changes takes place.

    static readonly attribute = {
        clickEffect: `${MechaneButton.tag}-click-effect`,
        cooldownDuration: `${MechaneButton.tag}-cooldown-duration`,
        effectName: `${MechaneButton.tag}${effectNameAttrSuffix}`,
        iconColour: `${MechaneButton.tag}-icon-colour`,
        iconFamily: `${MechaneButton.tag}-icon-family`,
        originPosition: `${MechaneButton.tag}-origin-position`,
        theme: `${MechaneButton.tag}-theme`,
    } as const

    // Descendants of the shadowRoot.

    readonly container = document.createElement("div")
    readonly clickEffect = document.createElement("div")
    readonly iconSlot = document.createElement("slot")
    readonly textSlot = document.createElement("slot")

    // The click listener's callback/element. The callback is initially an
    // ineffectual dummy function. It is meant to be replaced by a function
    // which sends clicks to the Mechane server.

    readonly clickListener: MechaneButtonClickListener = {
        callback: (ev: MouseEvent) => {
            console.log(`${MechaneButton.tag}: click failed: dummy callback`)
            void ev
        },
        element: this,
        event: "click",
    }

    constructor() {
        // Mandatory super call.

        super()

        // Instance fields.

        this.iconSlot.name = css.slots.icon.slotName
        this.iconSlot.id = css.slots.icon.id
        this.textSlot.name = css.slots.text.slotName
        this.container.append(this.iconSlot, this.textSlot)
        this.container.id = css.container.id
        this.clickEffect.id = css.clickEffect.id

        // Shadow root and style.

        const shadowRoot = this.attachShadow({
            delegatesFocus: true,
            mode: "open",
        })
        const style = document.createElement("style")
        shadowRoot.append(style, this.container)
    }

    attributeChangedCallback(name: string) {
        if (name == disabledAttribute && this.hasAttribute(disabledAttribute)) {
            this.container.classList.add(css.disabledClass)
            if (this.hasAttribute(MechaneButton.attribute.clickEffect)) {
                // Undo an active click effect.
                this.removeAttribute(MechaneButton.attribute.clickEffect)
                this.clickEffect.remove()
            }
        }
        this.updateStyle()
    }

    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return
        }

        // Add and store the click listener.
        const effectName = this.getAttribute(MechaneButton.attribute.effectName)
        if (!effectName) {
            log(MechaneButton.tag, "no effect name attribute")
        }
        const dummyButton = document.createElement("button") // Absorbs focus.
        const click = (ev: MouseEvent) => {
            if (!effectName) {
                log(`${MechaneButton.tag}`, "click failed", "no name attribute")
                return
            }
            dummyButton.focus()
            ev.preventDefault()

            ShutterbugPixels.uiEvents?.send(
                newMechaneButtonUIEvent(effectName, ev)
            )
        }
        this.addEventListener("click", click)
        this.clickListener.callback = click
        ShutterbugPixels.uiEvents?.pushOverlayItemClickListener(
            this.clickListener
        )

        // Click effect.
        if (this.hasAttribute(MechaneButton.attribute.clickEffect)) {
            this.container.prepend(this.clickEffect)
        }

        // Styling.
        this.updateStyle()
    }

    disconnectedCallback() {
        // Remove the stored click listener.
        ShutterbugPixels.uiEvents?.popOverlayItemClickListener(
            this.clickListener
        )

        this.clickListener.element.removeEventListener(
            this.clickListener.event,
            this.clickListener.callback
        )
    }

    updateStyle() {
        const style = this.shadowRoot?.querySelector("style")
        if (!style) {
            return
        }

        // if (
        //     this.getAttribute(MechaneButton.attribute.theme) != theme.motifName
        // ) {
        //     return
        // }

        const iconFamily =
            this.getAttribute(MechaneButton.attribute.iconFamily) ??
            materialIconsClass.round
        if (isMaterialIconsFamily(iconFamily)) {
            setMaterialIconsClass(this.iconSlot, iconFamily)
        }

        const originPosition =
            getOriginPosition(
                this.getAttribute(MechaneButton.attribute.originPosition)
            ) ?? theme.defaultOriginPosition

        // Accumulate CSS rules in a style sheet.
        const sheet: Sheet = new Array<Rule>()

        // Include the font styles. Even though the font rules are defined
        // globally, inside a custom element the font rules need to be defined
        // again.
        sheet.push(...materialIcons)

        const clicked = this.hasAttribute(MechaneButton.attribute.clickEffect)
        const slotted = this.hasAttribute("slot")

        // Container element.
        if (clicked) {
            sheet.push({
                selector: css.container.selector.normal,
                properties: css.container.style.clicked(
                    originPosition,
                    slotted
                ),
            })
        } else {
            sheet.push(
                {
                    selector: css.container.selector.normal,
                    properties: css.container.style.normal(
                        originPosition,
                        slotted
                    ),
                },
                {
                    selector: css.container.selector.hover,
                    properties: css.container.style.hover(),
                },
                {
                    selector: css.container.selector.disabled,
                    properties: css.container.style.disabled(slotted),
                }
            )
        }

        // Click effect element.
        if (clicked) {
            const flashDuration =
                this.getAttribute(MechaneButton.attribute.cooldownDuration) ??
                theme.defaultFlashDuration
            sheet.push(
                {
                    selector: css.clickEffect.selector.normal,
                    properties: css.clickEffect.style.normal(
                        flashDuration,
                        slotted ? undefined : originPosition
                    ),
                },
                {
                    selector: css.clickEffect.selector.keyframes,
                    properties: css.clickEffect.style.keyframes(theme.accent),
                }
            )
        }

        // Slot elements.
        const iconColour =
            this.getAttribute(MechaneButton.attribute.iconColour) ??
            theme.primary
        sheet.push(
            {
                selector: css.slots.icon.selector.normal,
                properties: css.slots.icon.style.normal(iconColour),
            },
            {
                selector: css.slots.icon.selector.disabled,
                properties: css.slots.icon.style.disabled,
            },
            {
                selector: css.slots.text.selector,
                properties: css.slots.text.style,
            }
        )

        // Apply the accumulated CSS rules.
        style.textContent = toString(sheet)
    }
}
