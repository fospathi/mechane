import type * as schema from "../../../schema/schema"

export function newMechaneButtonUIEvent(
    effectName: string,
    ev: MouseEvent
): schema.MechaneButtonUIEvent {
    return {
        mechaneButton: {
            altKey: ev.altKey,
            button: ev.button, // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
            clientX: ev.clientX, // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/clientX
            clientY: ev.clientY,
            ctrlKey: ev.ctrlKey,
            detail: ev.detail, // https://developer.mozilla.org/en-US/docs/Web/API/UIEvent/detail
            metaKey: ev.metaKey,
            effectName: effectName,
            shiftKey: ev.shiftKey,
            windowH: ev.view ? ev.view.innerHeight : 0, // https://developer.mozilla.org/en-US/docs/Web/API/Window/innerHeight
            windowW: ev.view ? ev.view.innerWidth : 0,
        },
    }
}
