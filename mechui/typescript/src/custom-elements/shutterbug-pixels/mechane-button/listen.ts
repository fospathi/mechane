export interface MechaneButtonClickListener {
    callback: (ev: MouseEvent) => void
    readonly element: HTMLElement
    readonly event: "click"
}
