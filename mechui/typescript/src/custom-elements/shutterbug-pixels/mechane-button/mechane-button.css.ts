import type * as CSS from "csstype"

import {
    flashEffect,
    flashEffectAnimationName,
    flashEffectKeyframes,
} from "../style/flash"
import { type OriginPosition, borderRadiuses } from "../style/origin-position"
import { theme } from "../style/theme"
import { kaputFilter } from "../../style"

// CSS custom properties for the button background transition. These custom
// properties are defined by the Elm integrant in a global style element. They
// are used by both the Elm and Typescript integrants. Defining them here inside
// the Shadow DOM is not supported.

const customs = {
    pct1: "--button-background-stop-1-percentage",
    pct2: "--button-background-stop-2-percentage",
    col1: "--button-background-stop-1-color",
} as const

export interface ButtonCustomProperties extends CSS.Properties {
    [customs.pct1]?: string
    [customs.pct2]?: string
    [customs.col1]?: string
}

const transition = [
    "border-color 100ms",
    "box-shadow 300ms",
    `${customs.pct1} 300ms`,
    `${customs.pct2} 300ms`,
    `${customs.col1} 300ms`,
].join()

// Container element.

const containerID = "mechane-button-container"

const disabledClass = "mechane-button-disabled"

function containerBase(
    originPosition: OriginPosition,
    slotted: boolean
): ButtonCustomProperties {
    return {
        display: "flex",
        alignItems: "center",

        background:
            "radial-gradient(" +
            [
                [`var(${customs.col1})`, `var(${customs.pct1})`].join(" "),
                [`${theme.primaryLight}`, `var(${customs.pct2})`].join(" "),
            ].join(",") +
            ")",
        [customs.pct1]: "100%",
        [customs.pct2]: "100%",
        [customs.col1]: slotted
            ? "rgba(255, 255, 255, 0.0)"
            : theme.buttonBackground,
        borderColor: theme.primary,
        borderRadius: slotted
            ? theme.buttonBorderRadius
            : borderRadiuses.get(originPosition),
        borderStyle: "solid",
        borderWidth: theme.buttonBorder,
        boxShadow: slotted ? "none" : theme.boxShadow,
        color: theme.textNormal,
        fontFamily: "CascadiaCode",
        fontSize: "1em",
        outline: "none",
        padding: theme.buttonPadding,
        pointerEvents: "auto",
        position: "relative",
        whiteSpace: "nowrap",
    }
}

function normalContainer(
    originPosition: OriginPosition,
    slotted: boolean
): CSS.Properties {
    return {
        ...containerBase(originPosition, slotted),
        transition: transition,
    }
}

function clickedContainer(
    originPosition: OriginPosition,
    slotted: boolean
): CSS.Properties {
    return {
        ...containerBase(originPosition, slotted),
    }
}

function hoveredContainer(): ButtonCustomProperties {
    return {
        borderColor: theme.primaryLight,
        boxShadow: theme.boxShadowGlow,
        [customs.pct1]: "62%",
        [customs.pct2]: "72%",
        [customs.col1]: theme.buttonHoverBackground,
    }
}

function disabledContainer(slotted: boolean): CSS.Properties {
    return {
        background: slotted ? "none" : "rgba(255, 255, 255, 0.65)",
        borderColor: theme.disabled,
        borderWidth: theme.buttonBorder,
        boxShadow: "none",
        filter: kaputFilter,
        padding: theme.buttonPadding,
    }
}

// Click effect element.

const clickEffectID = "mechane-button-click-effect"

// Slot elements.

const iconSlotID = "iconSlotID"
const iconSlotName = "icon"
const textSlotName = "text"

const iconSlot = function (iconColour: string): CSS.Properties {
    return {
        color: `${iconColour}`,
        fontSize: theme.buttonIconFontSize,
        zIndex: 2,
    }
}

const disabledIconSlot: CSS.Properties = {
    color: theme.disabled,
}

const textSlot: CSS.Properties = {
    paddingLeft: "0.5em",
    zIndex: 2,
}

// All the CSS!

export const css = {
    disabledClass: disabledClass,
    paddingBorderTotalWidth: `calc(${theme.buttonPadding} + ${theme.buttonBorder})`,
    container: {
        id: containerID,
        selector: {
            hover: `#${containerID}:hover:not(.${disabledClass})`,
            normal: `#${containerID}`,
            disabled: `#${containerID}.${disabledClass}`,
        },
        style: {
            clicked: clickedContainer,
            hover: hoveredContainer,
            normal: normalContainer,
            disabled: disabledContainer,
        },
    },
    clickEffect: {
        id: clickEffectID,
        selector: {
            keyframes: `@keyframes ${flashEffectAnimationName}`,
            normal: `#${clickEffectID}`,
        },
        style: {
            keyframes: flashEffectKeyframes,
            normal: flashEffect,
        },
    },
    slots: {
        icon: {
            id: iconSlotID,
            slotName: iconSlotName,
            selector: {
                normal: `#${iconSlotID}`,
                disabled: `.${disabledClass} #${iconSlotID}`,
            },
            style: {
                normal: iconSlot,
                disabled: disabledIconSlot,
            },
        },
        text: {
            slotName: textSlotName,
            selector: `::slotted([slot=${textSlotName}])`,
            style: textSlot,
        },
    },
} as const
