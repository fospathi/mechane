import {
    type Overlay,
    type OverlayItem,
    hiddenClass,
    disableAble,
    disabledAttribute,
    overlayItemElementTag,
} from "./overlay"
import * as schema from "../../schema/schema"

export function updateOverlay(
    overlay: Overlay,
    overlayUpdate: schema.OverlayUpdate
) {
    if (overlay.disabled) {
        return
    }

    if ("create" in overlayUpdate) {
        for (const name of overlayUpdate.create) {
            const e = document.createElement(overlayItemElementTag)
            e.classList.add(hiddenClass)
            overlay.element.appendChild(e)
            overlay.items.set(name, {
                element: e,
                innerHTMLHash: "",
                left: "",
                top: "",
                transform: "",
                transformOrigin: "",
                zIndex: 0,
            })
        }
    } else if ("delete" in overlayUpdate) {
        for (const name of overlayUpdate.delete) {
            const item = overlay.items.get(name)
            if (item) {
                item.element.remove()
                overlay.items.delete(name)
            } else {
                console.log(`cannot delete missing overlay item: ${name}`)
            }
        }
    } else if ("hide" in overlayUpdate) {
        for (const name of overlayUpdate.hide) {
            const item = overlay.items.get(name)
            if (item) {
                item.element.classList.add(hiddenClass)
            } else {
                console.log(`cannot hide missing overlay item: ${name}`)
            }
        }
    } else if ("update" in overlayUpdate) {
        for (const itemUpdate of overlayUpdate.update) {
            const item = overlay.items.get(itemUpdate.name)
            if (item) {
                updateItem(item, itemUpdate)
                item.element.classList.remove(hiddenClass)
            } else {
                console.log(
                    `cannot update missing overlay item: ${itemUpdate.name}`
                )
            }
        }
    }
}

export function disableOverlay(
    overlay: Overlay,
    closeEvent: schema.WebSocketCloseEvent
) {
    overlay.disabled = true

    let title = "No connection.\n"
    const reasonResult = schema.normalCloseReasons.safeParse(closeEvent.reason)
    if (reasonResult.success) {
        switch (reasonResult.data) {
            case "intercepted terminal interrupt":
                title += "Mechane was shut down from a terminal.\n"
                break

            case "received client request":
                title += "Mechane was shut down by a client.\n"
                break
        }
    } else {
        if (closeEvent.reason.length > 0) {
            title += `Websocket close reason: ${closeEvent.reason}.\n`
        }
        title += [
            `Websocket close code: ${closeEvent.code}.`,
            `Websocket path: ${closeEvent.sourceWebSocket}.`,
            "",
        ].join("\n")
    }

    overlay.items.forEach((item) => {
        for (const tag of disableAble) {
            item.element.querySelectorAll(tag).forEach((e) => {
                e.setAttribute(disabledAttribute, "true")
                e.setAttribute("title", title)
            })
        }
    })
}

function updateItem(item: OverlayItem, update: schema.OverlayItemElementProps) {
    if (item.innerHTMLHash != update.innerHTMLHash) {
        item.element.innerHTML = update.innerHTML
        item.innerHTMLHash = update.innerHTMLHash
    }

    // Style properties.
    if (item.left != update.left) {
        item.element.style.left = update.left
        item.left = update.left
    }
    if (item.top != update.top) {
        item.element.style.top = update.top
        item.top = update.top
    }
    if (item.transform != update.transform) {
        item.element.style.transform = update.transform
        item.transform = update.transform
    }
    if (item.transformOrigin != update.transformOrigin) {
        item.element.style.transformOrigin = update.transformOrigin
        item.transformOrigin = update.transformOrigin
    }
    if (item.zIndex != update.zIndex) {
        item.element.style.zIndex = update.zIndex.toString()
        item.zIndex = update.zIndex
    }
}
