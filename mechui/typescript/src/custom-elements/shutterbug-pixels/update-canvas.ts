// Use the data in the arraybuffer to update the canvas element.
//
// The data in the arraybuffer is an image in binary form along with a suffix
// giving the image dimensions.
export function updateCanvas(
    context: CanvasRenderingContext2D,
    data: ArrayBuffer
) {
    const suffixLen = 8 // The data has a suffix occupying this number of bytes.
    const dataLen = data.byteLength
    const pixLen = dataLen - suffixLen

    if (dataLen < suffixLen) {
        console.log("pixel socket: message data is too short")
        return
    }

    // The suffix contains the image dimensions in units of pixels.
    //
    // The first four bytes of the suffix represent an integer, in order of most
    // significant byte first, which is the image's width in pixels. Similarly
    // the remaining four bytes represent the height.
    const dimData = new Uint8Array(data, dataLen - suffixLen, suffixLen)
    const w =
        (dimData[0] as number) * (1 << 24) +
        (dimData[1] as number) * (1 << 16) +
        (dimData[2] as number) * (1 << 8) +
        (dimData[3] as number)
    const h =
        (dimData[4] as number) * (1 << 24) +
        (dimData[5] as number) * (1 << 16) +
        (dimData[6] as number) * (1 << 8) +
        (dimData[7] as number)

    // Each pixel is composed of four channels (RGBA). All channels have the
    // same colour depth, either one or two bytes. Thus there are two possible
    // lengths of the image data for a known width and height.
    const pixStandardLen = w * h * 4
    const pixDeepLen = pixStandardLen * 2
    switch (pixLen) {
        case pixStandardLen:
            break
        case pixDeepLen:
            console.log("pixel socket: 16 bit RGBA channels not yet supported")
            return
        default:
            console.log("pixel socket: wrong sized image or w/h suffix")
            return
    }
    const pix = new Uint8ClampedArray(data, 0, pixLen)

    if (context.canvas.width != w || context.canvas.height != h) {
        context.canvas.width = w
        context.canvas.height = h
    }
    context.putImageData(new ImageData(pix, w, h), 0, 0)
}
