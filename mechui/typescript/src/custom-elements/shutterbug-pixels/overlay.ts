import type * as schema from "../../schema/schema"
import { customTag } from "../tag"

export interface Overlay {
    disabled: boolean
    items: Map<string, OverlayItem>
    element: HTMLDivElement
}
export interface OverlayItem
    extends Omit<schema.OverlayItemElementProps, "innerHTML"> {
    element: OverlayItemElementType
}

export function newOverlay(): Overlay {
    const e = document.createElement("div")
    e.id = overlayElementID
    return {
        disabled: false,
        items: new Map<string, OverlayItem>(),
        element: e,
    }
}

// The overlay element occupies the region above the shutterbug-pixels' canvas
// element.
export const overlayElementID = "canvas-overlay-container"

// Overlay items. They live in the overlay container.
export const overlayItemElementTag = "div"
export type OverlayItemElementType =
    HTMLElementTagNameMap[typeof overlayItemElementTag]

// An overlay item element with this class is a temporarily deactivated
// invisible element.
export const hiddenClass = "hidden-overlay-element"

// An overlay item element with this attribute is a permanently disabled
// partially visible element.
export const disabledAttribute = "disabled-overlay-element"

// Elements supporting the disabled overlay element attribute.
export const disableAble = [
    customTag.overlay.button,
    customTag.overlay.textfield,
]
