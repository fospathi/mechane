import type * as CSS from "csstype"

import type { KeyframesProperties } from "../../../crule/crule"
import { type OriginPosition, borderRadiuses } from "./origin-position"
import { theme } from "./theme"

export const flashEffectAnimationName = "flash"

export function flashEffect(
    animationDuration: string,
    originPosition?: OriginPosition
): CSS.Properties {
    return {
        animation: `${flashEffectAnimationName} ${animationDuration}`,
        background: "rgba(0, 0, 0, 0)",
        borderRadius: originPosition
            ? borderRadiuses.get(originPosition)
            : theme.buttonBorderRadius,

        // Offset the (padding + border) of the parent.
        // A top margin isn't necessary as the flex container centers the items.
        marginLeft: `calc(-1 * (${theme.buttonPadding} + ${theme.buttonBorder}))`,

        position: "absolute",
        zIndex: 1,

        width: `calc(100% + 2 * ${theme.buttonBorder})`,
        height: `calc(100% + 2 * ${theme.buttonBorder})`,
    }
}

export function flashEffectKeyframes(flashColour: string): KeyframesProperties {
    return {
        from: {
            background: "rgba(0, 0, 0, 0)",
        },
        "50%": {
            background: flashColour,
        },
        to: {
            background: "rgba(0, 0, 0, 0)",
        },
    }
}
