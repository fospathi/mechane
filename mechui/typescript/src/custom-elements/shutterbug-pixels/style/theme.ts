import { initial } from "../../../motif/motif"

export const theme = {
    ...initial,

    // Flash effect.
    defaultFlashDuration: "0.4s",
    // Placement of the on-screen position of the button's mechtree node origin
    // relative to the button.
    //
    // Without a transform applied to the button the origin is at the top left
    // corner.
    defaultOriginPosition: "top-left" as const,
}
