import { theme } from "./theme"

// Origin positioning.

const originPositions = [
    "top",
    "bottom",
    "left",
    "right",
    "center",
    "icon-center",
    "top-left",
    "top-right",
    "bottom-right",
    "bottom-left",
] as const

// Use a circular button corner except when that corner overlaps the origin.
export const borderRadiuses = new Map<OriginPosition, string>([
    ["top", theme.buttonBorderRadius],
    ["bottom", theme.buttonBorderRadius],
    ["left", theme.buttonBorderRadius],
    ["right", theme.buttonBorderRadius],
    ["center", theme.buttonBorderRadius],
    ["icon-center", theme.buttonBorderRadius],
    [
        "top-left",
        `0 ${theme.buttonBorderRadius} ${theme.buttonBorderRadius} ${theme.buttonBorderRadius}`,
    ],
    [
        "top-right",
        `${theme.buttonBorderRadius} 0 ${theme.buttonBorderRadius} ${theme.buttonBorderRadius}`,
    ],
    [
        "bottom-right",
        `${theme.buttonBorderRadius} ${theme.buttonBorderRadius} 0 ${theme.buttonBorderRadius}`,
    ],
    [
        "bottom-left",
        `${theme.buttonBorderRadius} ${theme.buttonBorderRadius} ${theme.buttonBorderRadius} 0`,
    ],
])

// Origin position type indicating which part of the button should overlap the
// onscreen origin position of the button's mechtree node.
export type OriginPosition = typeof originPositions[number]

function isOriginPosition(value: string): value is OriginPosition {
    return originPositions.includes(value as OriginPosition)
}

export function getOriginPosition(
    attributeValue: string | null
): OriginPosition | null {
    if (!attributeValue || !isOriginPosition(attributeValue)) {
        return null
    }
    return attributeValue
}

export const positioningTransforms = new Map<OriginPosition, string>([
    ["top", "translateX(-50%)"],
    ["bottom", "translate(-50%, -100%)"],
    ["left", "translateY(-50%)"],
    ["right", "translate(-100%, -50%)"],
    ["center", "translate(-50%, -50%)"],
    [
        "icon-center",
        `translate(calc(-${theme.buttonBorder} - ${theme.buttonPadding} - ${theme.buttonIconFontSize} / 2), -50%)`,
    ],
    ["top-left", "translate(0, 0)"], // Unadjusted origin position.
    ["top-right", "translateX(-100%)"],
    ["bottom-right", "translate(-100%, -100%)"],
    ["bottom-left", "translateY(-100%)"],
])
