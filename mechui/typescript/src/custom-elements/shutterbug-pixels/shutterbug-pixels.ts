import type { UIEventListenersController } from "./listen"
import {
    hiddenClass,
    newOverlay,
    overlayElementID,
    overlayItemElementTag,
} from "./overlay"
import { kaputFilter } from "../style"

// A ShutterbugPixels custom element displays the pixels of a shutterbug's view.
//
// The ShutterbugPixels element is a singleton class.
//
// The static context field for the canvas may be null, in which case do not use
// the ShutterbugPixels element.
export class ShutterbugPixels extends HTMLElement {
    static #instance?: ShutterbugPixels

    // The tag name of this custom element.
    static readonly tag = "shutterbug-pixels"

    // Custom attributes start with this prefix.
    static readonly attributePrefix = ShutterbugPixels.tag + "-"

    static readonly attributes = {
        // CSS rules for the container div. Can be used to make a border.
        border: ShutterbugPixels.attributePrefix + "b",
        // Background-colour of the canvas.
        colour: ShutterbugPixels.attributePrefix + "c",
        // Canvas height attribute.
        height: ShutterbugPixels.attributePrefix + "h",
        // Indicates if the client is kaput.
        kaput: ShutterbugPixels.attributePrefix + "k",
        // Indicates if text in the overlay can be selected.
        selectable: ShutterbugPixels.attributePrefix + "s",
        // Canvas width attribute.
        width: ShutterbugPixels.attributePrefix + "w",
    } as const

    static get observedAttributes() {
        return Object.values(ShutterbugPixels.attributes)
    }

    // The canvas element used to display the view associated with the
    // shutterbug.
    static readonly context = document.createElement("canvas").getContext("2d")

    // The element which overlays the canvas element.
    static readonly overlay = newOverlay()

    // The element containing the canvas and the canvas overlay. An immediate
    // child of the shadow root.
    static readonly container = document.createElement("div")
    static readonly containerID = "shutterbug-pixels-container"

    static uiEvents: UIEventListenersController | undefined = undefined

    constructor() {
        // As a singleton class only one ShutterbugPixels instance is
        // constructed.

        if (ShutterbugPixels.#instance) {
            return ShutterbugPixels.#instance
        }

        // Mandatory super call. Store the instance for subsequent constructor
        // calls.

        super()
        ShutterbugPixels.#instance = this

        // Static fields.

        if (!ShutterbugPixels.context) {
            return
        }

        ShutterbugPixels.container.id = ShutterbugPixels.containerID
        ShutterbugPixels.container.append(
            ShutterbugPixels.context.canvas,
            ShutterbugPixels.overlay.element
        )

        // Shadow root and style.

        const style = document.createElement("style")
        const shadowRoot = this.attachShadow({ mode: "open" })
        shadowRoot.append(style, ShutterbugPixels.container)
    }

    attributeChangedCallback(name: string, oldValue: string | null) {
        // Disable the mouse movement listener when text selection is enabled.
        if (name == ShutterbugPixels.attributes.selectable) {
            if (oldValue === null) {
                ShutterbugPixels.uiEvents?.removeMouseMoveListener()
            } else {
                ShutterbugPixels.uiEvents?.addMouseMoveListener()
            }
        }

        this.updateStyle()
    }

    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return
        }

        this.updateStyle()
    }

    updateStyle() {
        const style = this.shadowRoot?.querySelector("style")
        if (!style) {
            return
        }

        const att = ShutterbugPixels.attributes
        const b = this.getAttribute(att.border) ?? ""
        const c = this.getAttribute(att.colour) ?? "black"
        const h = this.getAttribute(att.height) ?? 0
        const k = this.getAttribute(att.kaput) !== null ? true : false
        const s = this.getAttribute(att.selectable) !== null ? true : false
        const w = this.getAttribute(att.width) ?? 0

        // Pixels are provided from the server in bottom-to-top row order but a
        // canvas expects them to be given in top-to-bottom order: mitigate this
        // with a Y-flip on the canvas element.
        style.textContent = `
        canvas {
            background-color: ${c};
            display: block;
            cursor: ${k ? "auto" : "grab"};
            transform: scale(1, -1);
            width: ${w}; height: ${h};
            transition: width 1s, height 1s;
        }

        canvas:active {
            cursor: ${k ? "auto" : "grabbing"};
        }

        .${hiddenClass} {
            visibility: hidden;
        }

        #${ShutterbugPixels.containerID} {
            overflow: hidden;
            /* Allow overlay items to be positioned absolutely to this. */
            position: relative;
            ${b}
        }

        #${overlayElementID} {
            pointer-events: none;
            /* Prevent text selection on overlay items when mouse dragging. */
            user-select: none;
            width: 100%; height: 100%;
        }
        
        #${overlayElementID} > ${overlayItemElementTag} {
            ${k ? "filter: " + kaputFilter + ";" : ""}
            position: absolute;
            user-select: ${s ? "text" : "none"};
        }
        `
    }
}
