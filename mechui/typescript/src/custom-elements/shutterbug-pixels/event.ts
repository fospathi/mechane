import type * as schema from "../../schema/schema"

export function newKeyboardUIEvent(
    keyDown: boolean,
    pressedKeys: Set<string>,
    ev: KeyboardEvent
): schema.KeyboardUIEvent {
    return {
        keyboard: {
            altKey: ev.altKey,
            ctrlKey: ev.ctrlKey,
            isComposing: ev.isComposing,
            key: ev.key, // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key
            keyDown: keyDown,
            metaKey: ev.metaKey,
            pressed: Array.from(pressedKeys),
            repeat: ev.repeat,
            shiftKey: ev.shiftKey,
        },
    }
}

export function newMouseButtonUIEvent(
    mouseDown: boolean,
    ev: MouseEvent
): schema.MouseButtonUIEvent {
    return {
        mouseButton: {
            altKey: ev.altKey,
            button: ev.button, // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
            clientX: ev.clientX, // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/clientX
            clientY: ev.clientY,
            ctrlKey: ev.ctrlKey,
            detail: ev.detail, // https://developer.mozilla.org/en-US/docs/Web/API/UIEvent/detail
            metaKey: ev.metaKey,
            mouseDown: mouseDown,
            shiftKey: ev.shiftKey,
            windowH: ev.view ? ev.view.innerHeight : 0, // https://developer.mozilla.org/en-US/docs/Web/API/Window/innerHeight
            windowW: ev.view ? ev.view.innerWidth : 0,
        },
    }
}

// The width and height are those of the listened to element.
//
// The invertY flag is to mitigate a possible Y-axis inverting transform which
// has been applied to the element, so that the offsetY is still relative to the
// element's top.
export function newMouseMoveUIEvent({
    w,
    h,
    activeEditable,
    invertY,
    ev,
}: {
    w: number
    h: number
    activeEditable: boolean
    invertY: boolean
    ev: MouseEvent
}): schema.MouseMoveUIEvent {
    return {
        mouseMove: {
            activeEditable: activeEditable,
            altKey: ev.altKey,
            buttons: ev.buttons, // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/buttons
            clientX: ev.clientX, // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/clientX
            clientY: ev.clientY,
            ctrlKey: ev.ctrlKey,
            metaKey: ev.metaKey,
            movementX: ev.movementX, // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/movementX
            movementY: ev.movementY,
            pixelsW: w,
            pixelsH: h,
            pixelsOffsetX: ev.offsetX, // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/offsetX
            pixelsOffsetY: invertY ? h - ev.offsetY : ev.offsetY,
            pointerLock: document.pointerLockElement !== null,
            shiftKey: ev.shiftKey,
            windowH: ev.view ? ev.view.innerHeight : 0, // https://developer.mozilla.org/en-US/docs/Web/API/Window/innerHeight
            windowW: ev.view ? ev.view.innerWidth : 0,
        },
    }
}

export function newMouseWheelUIEvent({
    effectName,
    deltaX,
    deltaY,
    ev,
}: {
    effectName: string
    deltaX: number
    deltaY: number
    ev: WheelEvent
}): schema.MouseWheelUIEvent {
    return {
        mouseWheel: {
            altKey: ev.altKey,
            clientX: ev.clientX, // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/clientX
            clientY: ev.clientY,
            ctrlKey: ev.ctrlKey,
            effectName: effectName,
            deltaX: deltaX,
            deltaY: deltaY,
            metaKey: ev.metaKey,
            shiftKey: ev.shiftKey,
            windowH: ev.view ? ev.view.innerHeight : 0, // https://developer.mozilla.org/en-US/docs/Web/API/Window/innerHeight
            windowW: ev.view ? ev.view.innerWidth : 0,
        },
    }
}
