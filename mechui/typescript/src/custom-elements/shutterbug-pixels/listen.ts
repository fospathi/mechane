import {
    newKeyboardUIEvent,
    newMouseButtonUIEvent,
    newMouseMoveUIEvent,
    newMouseWheelUIEvent,
} from "./event"
import type * as schema from "../../schema/schema"
import { ShutterbugPixels } from "./shutterbug-pixels"
import { customTag, effectNameAttrSuffix } from "../tag"

export interface UIEventListenersController {
    // The mouse move listener on the main container.
    readonly addMouseMoveListener: () => void
    readonly removeMouseMoveListener: () => void

    // Keep a record of all overlay item listeners to be able, for example, to
    // remove them later when the client is kaput.
    readonly pushOverlayItemClickListener: (
        listener: MouseClickListener & OverlayItemListener
    ) => void
    readonly popOverlayItemClickListener: (
        listener: MouseClickListener & OverlayItemListener
    ) => void

    readonly pushOverlayItemKeyUpOrDownListener: (
        listener: (KeyUpListener | KeyDownListener) & OverlayItemListener
    ) => void
    readonly popOverlayItemKeyUpOrDownListener: (
        listener: (KeyUpListener | KeyDownListener) & OverlayItemListener
    ) => void

    // Remove all listeners.
    //
    // Shall be called only once when the client is kaput.
    readonly removeAllListeners: () => void

    // Send the UI event to the Mechane server.
    readonly send: (event: schema.MechaneUIEvent) => void
}

interface KeyDownListener {
    event: "keydown"
    callback: (event: KeyboardEvent) => void
}

interface KeyUpListener {
    event: "keyup"
    callback: (event: KeyboardEvent) => void
}

interface MouseClickListener {
    event: "click"
    callback: (event: MouseEvent) => void
}

interface MouseDownListener {
    event: "mousedown"
    callback: (event: MouseEvent) => void
}

interface MouseMoveListener {
    event: "mousemove"
    callback: (event: MouseEvent) => void
}

interface MouseUpListener {
    event: "mouseup"
    callback: (event: MouseEvent) => void
}

interface MouseWheelListener {
    event: "wheel"
    callback: (event: WheelEvent) => void
}

interface OverlayItemListener {
    element: HTMLElement
}

interface EventListeners {
    // Send an event to the Mechane server over the websocket meant for UI
    // events associated with aspects of the Typescript integrant.
    send: (event: schema.MechaneUIEvent) => void
    mainContainer: HTMLDivElement
    keyboard: {
        overlayItems: Array<
            (KeyUpListener | KeyDownListener) & OverlayItemListener
        >
        window: Array<KeyDownListener | KeyUpListener>
    }
    mouse: {
        // The main container contains both the canvas and the overlay.
        mainContainer:
            | {
                  move: MouseMoveListener
                  wheel: MouseWheelListener
              }
            | undefined

        overlayItems: Array<MouseClickListener & OverlayItemListener>
        window: Array<MouseDownListener | MouseUpListener>
    }
}

// Figure out if the mouse is hovering over a focused area where responses to
// mouse input events may need to deviate from the norm. Perhaps, for example,
// mouse movements should not pan the camera in a text editing area.
function activeEditableAt(
    shadowRoot: ShadowRoot,
    clientX: number,
    clientY: number,
    editable: ReadonlyArray<string> = [customTag.overlay.textfield]
): boolean {
    const active = shadowRoot.activeElement
    const elements =
        shadowRoot.elementsFromPoint(clientX, clientY) ??
        ShutterbugPixels.container
    for (const element of elements) {
        const tag = element.tagName.toLowerCase()
        if (editable.includes(tag) && active == element) return true
    }
    return false
}

// For the deepest nameable overlay item at the given position, if any, that use
// a per tag custom effect name attribute get the value of the attribute.
function effectNameAt(
    shadowRoot: ShadowRoot,
    clientX: number,
    clientY: number,
    nameable: ReadonlyArray<string> = [
        customTag.overlay.button,
        customTag.overlay.textfield,
    ]
): string {
    const elements =
        shadowRoot.elementsFromPoint(clientX, clientY) ??
        ShutterbugPixels.container
    for (const element of elements) {
        const tag = element.tagName.toLowerCase()
        if (nameable.includes(tag)) {
            return element.getAttribute(tag + effectNameAttrSuffix) ?? ""
        }
    }
    return ""
}

function addMainContainerMouseListeners(
    context: CanvasRenderingContext2D,
    mainContainer: HTMLDivElement,
    send: (event: schema.MechaneUIEvent) => void,
    shadowRoot: ShadowRoot
): { move: MouseMoveListener; wheel: MouseWheelListener } {
    // The mousemove event listener is attached to the container of the canvas
    // and overlay instead of the canvas element directly so that mouse moves
    // that occur over overlay items are still registered.
    function move(ev: MouseEvent) {
        send(
            newMouseMoveUIEvent({
                w: context.canvas.clientWidth,
                h: context.canvas.clientHeight,
                activeEditable: activeEditableAt(
                    shadowRoot,
                    ev.clientX,
                    ev.clientY
                ),
                invertY: true,
                ev: ev,
            })
        )
    }

    function wheel(ev: WheelEvent) {
        send(
            newMouseWheelUIEvent({
                deltaX: ev.deltaX,
                deltaY: ev.deltaY,
                effectName: effectNameAt(shadowRoot, ev.clientX, ev.clientY),
                ev: ev,
            })
        )
    }

    mainContainer.addEventListener("mousemove", move)
    mainContainer.addEventListener("wheel", wheel)
    return {
        move: { event: "mousemove", callback: move },
        wheel: { event: "wheel", callback: wheel },
    }
}

function removeMainContainerMouseListeners(listeners: EventListeners) {
    const mouse = listeners.mouse.mainContainer
    if (!mouse) {
        return
    }
    listeners.mainContainer.removeEventListener(
        mouse.move.event,
        mouse.move.callback
    )
    listeners.mainContainer.removeEventListener(
        mouse.wheel.event,
        mouse.wheel.callback
    )
    listeners.mouse.mainContainer = undefined
}

// Add UI event listeners, on the appropriate elements, which forward events to
// the Mechane server.
//
// Store and return the event listeners so they can be removed later.
//
// Shall be called only once.
export function addListeners(
    context: CanvasRenderingContext2D,
    mainContainer: HTMLDivElement,
    send: (event: schema.MechaneUIEvent) => void,
    shadow: ShadowRoot
): UIEventListenersController {
    // A key normally becomes pressed on a keydown event and unpressed on a
    // keyup event.
    const pressedKeys = new Set<string>()

    const listeners = {
        keyboard: {
            window: Array<KeyDownListener | KeyUpListener>(),
            overlayItems: Array<
                (KeyUpListener | KeyDownListener) & OverlayItemListener
            >(),
        },
        mouse: {
            mainContainer: addMainContainerMouseListeners(
                context,
                mainContainer,
                send,
                shadow
            ),
            overlayItems: Array<MouseClickListener & OverlayItemListener>(),
            window: Array<MouseDownListener | MouseUpListener>(),
        },
        mainContainer: mainContainer,
        send: send,
    }

    function keyDown(ev: KeyboardEvent) {
        // These key events, that have a target of ShutterbugPixels, were made
        // somewhere inside the shadowRoot and are handled by the real target.
        const internal = ev.target instanceof ShutterbugPixels
        if (internal) {
            return
        }

        // Prevent the default action on the tab key due to the possibility of
        // the focus moving into the hidden popups and causing unintended
        // display behaviour.
        if (ev.key == "Tab") {
            ev.preventDefault()
        }

        pressedKeys.add(ev.key)
        send(newKeyboardUIEvent(true, pressedKeys, ev))
    }

    function keyUp(ev: KeyboardEvent) {
        pressedKeys.delete(ev.key)
        const internal = ev.target instanceof ShutterbugPixels
        if (internal) {
            return
        }
        send(newKeyboardUIEvent(false, pressedKeys, ev))
    }

    function mouseDown(ev: MouseEvent) {
        send(newMouseButtonUIEvent(true, ev))
    }

    function mouseUp(ev: MouseEvent) {
        send(newMouseButtonUIEvent(false, ev))
    }

    window.addEventListener("keydown", keyDown)
    window.addEventListener("keyup", keyUp)
    window.addEventListener("mousedown", mouseDown)
    window.addEventListener("mouseup", mouseUp)

    listeners.keyboard.window.push({ event: "keydown", callback: keyDown })
    listeners.keyboard.window.push({ event: "keyup", callback: keyUp })
    listeners.mouse.window.push({ event: "mousedown", callback: mouseDown })
    listeners.mouse.window.push({ event: "mouseup", callback: mouseUp })
    const kaputState = { kaput: false }

    return {
        addMouseMoveListener: function () {
            if (kaputState.kaput) {
                return
            }
            if (listeners.mouse.mainContainer) {
                removeMainContainerMouseListeners(listeners)
            }
            listeners.mouse.mainContainer = addMainContainerMouseListeners(
                context,
                mainContainer,
                send,
                shadow
            )
        },
        removeMouseMoveListener: function () {
            removeMainContainerMouseListeners(listeners)
        },

        pushOverlayItemClickListener: function (listener) {
            listeners.mouse.overlayItems.push(listener)
        },
        popOverlayItemClickListener: function (listener) {
            const i = listeners.mouse.overlayItems.indexOf(listener)
            if (i == -1) {
                return
            }
            listeners.mouse.overlayItems.splice(i, 1)
        },
        pushOverlayItemKeyUpOrDownListener: function (listener) {
            listeners.keyboard.overlayItems.push(listener)
        },
        popOverlayItemKeyUpOrDownListener: function (listener) {
            const i = listeners.keyboard.overlayItems.indexOf(listener)
            if (i == -1) {
                return
            }
            listeners.keyboard.overlayItems.splice(i, 1)
        },

        removeAllListeners: function removeListeners() {
            kaputState.kaput = true

            // Keyboard

            for (const listener of listeners.keyboard.window) {
                window.removeEventListener(listener.event, listener.callback)
            }
            listeners.keyboard.window.length = 0

            for (const listener of listeners.keyboard.overlayItems) {
                listener.element.removeEventListener(
                    listener.event,
                    listener.callback
                )
            }
            listeners.keyboard.overlayItems.length = 0

            // Mouse

            removeMainContainerMouseListeners(listeners)

            for (const listener of listeners.mouse.overlayItems) {
                listener.element.removeEventListener(
                    listener.event,
                    listener.callback
                )
            }
            listeners.mouse.overlayItems.length = 0

            for (const listener of listeners.mouse.window) {
                window.removeEventListener(listener.event, listener.callback)
            }
            listeners.mouse.window.length = 0
        },

        send: send,
    }
}
