import type * as schema from "../../../schema/schema"

export function newMechaneTextfieldKeyUpOrDownUIEvent(
    effectName: string,
    value: string,
    keyDown: boolean,
    ev: KeyboardEvent
): schema.MechaneTextfieldKeyUpOrDownUIEvent {
    return {
        mechaneTextfieldKeyUpOrDown: {
            altKey: ev.altKey,
            ctrlKey: ev.ctrlKey,
            isComposing: ev.isComposing,
            key: ev.key,
            keyDown: keyDown,
            metaKey: ev.metaKey,
            effectName: effectName,
            shiftKey: ev.shiftKey,
            value: value,
        },
    }
}
