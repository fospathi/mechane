import type * as CSS from "csstype"

import {
    flashEffect,
    flashEffectAnimationName,
    flashEffectKeyframes,
} from "../style/flash"
import { type OriginPosition, borderRadiuses } from "../style/origin-position"
import { theme } from "../style/theme"
import { kaputFilter } from "../../style"

const transition = ["border-color 100ms", "box-shadow 300ms"].join()

// Container element.

const containerID = "mechane-textfield-container"

function containerBase(originPosition: OriginPosition): CSS.Properties {
    return {
        display: "flex",
        alignItems: "center",

        background: theme.buttonBackground,
        borderColor: theme.primary,
        borderRadius: borderRadiuses.get(originPosition),
        borderStyle: "double",
        borderWidth: theme.buttonBorder,
        boxShadow: theme.boxShadow,
        padding: theme.buttonPadding,
        pointerEvents: "auto",
        position: "relative",
        whiteSpace: "nowrap",
    }
}

function normalContainer(originPosition: OriginPosition): CSS.Properties {
    return {
        ...containerBase(originPosition),
        transition: transition,
    }
}

function submittedContainer(originPosition: OriginPosition): CSS.Properties {
    return {
        ...containerBase(originPosition),
    }
}

function hoveredContainer(): CSS.Properties {
    return {
        borderColor: theme.primaryLight,
    }
}

export const disabledColour = "rgb(211, 211, 211)"
function disabledContainer(originPosition: OriginPosition): CSS.Properties {
    return {
        ...containerBase(originPosition),
        background: "rgba(255, 255, 255, 0.65)",
        borderColor: disabledColour,
        borderWidth: theme.buttonBorder,
        boxShadow: "none",
        filter: kaputFilter,
        padding: theme.buttonPadding,
    }
}

// All child elements except the submit effect.
const containerChildren: CSS.Properties = {
    zIndex: 2, // Place above submit effect.
}

// Submit effect element.

const submitEffectID = "mechane-textfield-submit-effect"

// Icon elements.

const icon1SlotID = "icon1SlotID"
const icon2SlotID = "icon2SlotID"
const icon1SlotName = "icon1"
const icon2SlotName = "icon2"

function bothIcons(iconColour: string): CSS.Properties {
    return {
        color: `${iconColour}`,
        fontSize: theme.buttonIconFontSize,
    }
}

// Input element.

function inputFieldBase(borderColor: string): CSS.Properties {
    return {
        borderColor: borderColor,
        borderRadius: theme.buttonBorderRadius,
        borderStyle: "double",
        borderWidth: theme.buttonBorder,
        font: "inherit",
        marginLeft: theme.buttonPadding,
        marginRight: theme.buttonPadding,
        paddingLeft: theme.buttonPadding,
        paddingRight: theme.buttonPadding,
        paddingTop: `calc(${theme.buttonPadding} / 2)`,
        paddingBottom: `calc(${theme.buttonPadding} / 2)`,
        transition: transition,
    }
}

function inputField(disabled: boolean, width: number): CSS.Properties {
    const borderColor = disabled ? theme.disabled : theme.inputBorderPrimary
    if (isNaN(width)) {
        return inputFieldBase(borderColor)
    }
    return {
        ...inputFieldBase(borderColor),
        width: `${width}ch`,
    }
}

const hoverInputField: CSS.Properties = {
    boxShadow: `0px 0px 0.5em 0.2em rgb(0 0 0 / 20%)`,
}

function activeInputField(disabled: boolean): CSS.Properties {
    const props: CSS.Properties = {}
    if (!disabled) {
        props.borderStyle = "dashed"
        props.boxShadow = `0px 0px 0.6em 0.4em rgb(0 0 0 / 20%)`
        props.outline = `${theme.inputBorderFocus} solid calc(${theme.buttonBorder} / 2)`
    } else {
        props.boxShadow = "none"
        props.outline = "none"
    }
    return props
}

// Label text element.

const labelText: CSS.Properties = {
    cursor: "auto",
    display: "inline-block",
    marginLeft: theme.buttonPadding,
}

const activeLabelText: CSS.Properties = {
    outline: "none", // Hide outline in text select mode with focused label.
}

// Button element.

const buttonSlotName = "button"

const button: CSS.Properties = {
    marginLeft: theme.buttonPadding,
}

// All the CSS.

export const css = {
    borderWidth: theme.buttonBorder,
    container: {
        id: containerID,
        selector: {
            hover: `#${containerID}:hover`,
            normal: `#${containerID}`,
        },
        style: {
            submitted: submittedContainer,
            disabled: disabledContainer,
            hover: hoveredContainer,
            normal: normalContainer,
        },
    },
    containerChildren: {
        selector: `#${containerID} > *:not(#${submitEffectID})`,
        style: containerChildren,
    },
    submitEffect: {
        id: submitEffectID,
        selector: {
            keyframes: `@keyframes ${flashEffectAnimationName}`,
            normal: `#${submitEffectID}`,
        },
        style: {
            keyframes: flashEffectKeyframes,
            normal: flashEffect,
        },
    },
    iconSlot1: {
        id: icon1SlotID,
        slotName: icon1SlotName,
        selector: `#${icon1SlotID}`,
        style: bothIcons,
    },
    iconSlot2: {
        id: icon2SlotID,
        slotName: icon2SlotName,
        selector: `#${icon2SlotID}`,
        style: bothIcons,
    },
    inputField: {
        selector: "input",
        style: inputField,
    },
    hoverInputField: {
        selector: `#${containerID}:hover input`,
        style: hoverInputField,
    },
    activeInputField: {
        selector: `#${containerID} input:focus, p:focus + input`,
        style: activeInputField,
    },
    labelText: {
        selector: "label p",
        style: labelText,
    },
    activeLabelText: {
        selector: "label p:focus",
        style: activeLabelText,
    },
    button: {
        slotName: buttonSlotName,
        selector: `::slotted([slot=${buttonSlotName}])`,
        style: button,
    },
} as const
