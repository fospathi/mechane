import { type Rule, type Sheet, toString } from "../../../crule/crule"
import {
    isMaterialIconsFamily,
    materialIcons,
    materialIconsClass,
    setMaterialIconsClass,
} from "../../font"
import type {
    MechaneTextfieldKeyupListener,
    MechaneTextfieldKeydownListener,
} from "./listen"
import { log } from "../../../log/log"
import { css } from "./mechane-textfield.css"
import { disabledAttribute } from "../overlay"
import { ShutterbugPixels } from "../shutterbug-pixels"
import { getOriginPosition } from "../style/origin-position"
import { customTag, effectNameAttrSuffix } from "../../tag"
import { theme } from "../style/theme"
import { newMechaneTextfieldKeyUpOrDownUIEvent } from "./event"

// A MechaneTextfield custom element is a one line text input area.
export class MechaneTextfield extends HTMLElement {
    // The tag name of this custom element.
    static readonly tag = customTag.overlay.textfield

    // Attributes set by the client.
    //
    // The logic for handling a change of these attributes happens in the
    // attributeChangedCallback().

    static get observedAttributes() {
        // Disabled is set by the client when the overlay is permanently
        // disabled. This happens for a Mechane server shutdown or any other
        // connection loss.
        return [disabledAttribute]
    }

    // Attributes set and unset by the Mechane server.
    //
    // When such an attribute is changed a whole new element is sent to replace
    // the existing element. Thus the connectedCallback() is where the logic for
    // such changes takes place.

    static readonly attribute = {
        cooldownDuration: `${MechaneTextfield.tag}-cooldown-duration`,
        effectName: `${MechaneTextfield.tag}${effectNameAttrSuffix}`,
        icon1Colour: `${MechaneTextfield.tag}-icon1-colour`,
        icon1Family: `${MechaneTextfield.tag}-icon1-family`,
        icon2Colour: `${MechaneTextfield.tag}-icon2-colour`,
        icon2Family: `${MechaneTextfield.tag}-icon2-family`,
        inputValue: `${MechaneTextfield.tag}-input-value`,
        label: `${MechaneTextfield.tag}-label`,
        mask: `${MechaneTextfield.tag}-mask`,
        originPosition: `${MechaneTextfield.tag}-origin-position`,
        placeholder: `${MechaneTextfield.tag}-placeholder`,
        size: `${MechaneTextfield.tag}-size`,
        spellcheck: `${MechaneTextfield.tag}-spellcheck`,
        submitEffect: `${MechaneTextfield.tag}-submit-effect`,
        theme: `${MechaneTextfield.tag}-theme`,
    } as const

    // Descendants of the shadowRoot.

    readonly container = document.createElement("label")
    readonly submitEffect = document.createElement("div")
    readonly icon1Slot = document.createElement("slot")
    readonly icon2Slot = document.createElement("slot")
    readonly input = document.createElement("input")
    readonly labelText = document.createElement("p")
    readonly buttonSlot = document.createElement("slot")

    // The keyup/down listener's callback/element. The callback is initially an
    // ineffectual dummy function. It is meant to be replaced by a function
    // which sends keyup events to the Mechane server.

    readonly listener: {
        keyup: MechaneTextfieldKeyupListener
        keydown: MechaneTextfieldKeydownListener
    } = {
        keyup: {
            callback: (ev: KeyboardEvent) => {
                console.log(`${MechaneTextfield.tag}: keyup failed`)
                void ev
            },
            element: this.input,
            event: "keyup",
        },
        keydown: {
            callback: (ev: KeyboardEvent) => {
                console.log(`${MechaneTextfield.tag}: keydown failed`)
                void ev
            },
            element: this.input,
            event: "keydown",
        },
    }

    constructor() {
        // Mandatory super call.

        super()

        // Instance fields.

        this.icon1Slot.setAttribute("name", css.iconSlot1.slotName)
        this.icon1Slot.id = css.iconSlot1.id
        this.icon2Slot.setAttribute("name", css.iconSlot2.slotName)
        this.icon2Slot.id = css.iconSlot2.id
        this.labelText.tabIndex = -1 // Permit focus in text select mode.
        this.buttonSlot.setAttribute("name", css.button.slotName)

        this.container.append(
            this.icon1Slot,
            this.labelText,
            this.input,
            this.icon2Slot,
            this.buttonSlot
        )
        this.container.id = css.container.id
        this.submitEffect.id = css.submitEffect.id

        // Shadow root and style.

        const shadowRoot = this.attachShadow({
            delegatesFocus: true,
            mode: "open",
        })
        const style = document.createElement("style")
        shadowRoot.append(style, this.container)
    }

    attributeChangedCallback(name: string) {
        if (name == disabledAttribute && this.hasAttribute(disabledAttribute)) {
            if (this.hasAttribute(MechaneTextfield.attribute.submitEffect)) {
                // Undo an active submit effect.
                this.removeAttribute(MechaneTextfield.attribute.submitEffect)
                this.submitEffect.remove()
            }
        }
        this.updateStyle()
    }

    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return
        }

        // Add and store the keyup/down listeners.
        const effectName = this.getAttribute(
            MechaneTextfield.attribute.effectName
        )
        if (!effectName) {
            log(MechaneTextfield.tag, "missing effect name attribute")
        }
        for (const event of ["keyup", "keydown"] as const) {
            const callback = (ev: KeyboardEvent) => {
                if (!effectName) {
                    log(
                        `${MechaneTextfield.tag}`,
                        "key failed",
                        "missing name attribute"
                    )
                    return
                }
                ShutterbugPixels.uiEvents?.send(
                    newMechaneTextfieldKeyUpOrDownUIEvent(
                        effectName,
                        this.input.value,
                        event === "keydown",
                        ev
                    )
                )
            }
            this.input.addEventListener(event, callback)
            this.listener[event].callback = callback
            ShutterbugPixels.uiEvents?.pushOverlayItemKeyUpOrDownListener(
                this.listener[event]
            )
        }

        // Submit effect.
        if (this.hasAttribute(MechaneTextfield.attribute.submitEffect)) {
            this.container.prepend(this.submitEffect)
        }

        // Input value.
        this.input.value =
            this.getAttribute(MechaneTextfield.attribute.inputValue) ?? ""

        // Styling.
        this.updateStyle()
    }

    disconnectedCallback() {
        // Remove the stored keyup/down listeners.
        for (const event of ["keyup", "keydown"] as const) {
            const listener = this.listener[event]
            ShutterbugPixels.uiEvents?.popOverlayItemKeyUpOrDownListener(
                listener
            )
            listener.element.removeEventListener(
                listener.event,
                listener.callback
            )
        }
    }

    updateStyle() {
        const style = this.shadowRoot?.querySelector("style")
        if (!style) {
            return
        }

        // if (
        //     this.getAttribute(MechaneTextfield.attribute.theme) !=
        //     theme.motifName
        // ) {
        //     return
        // }

        const slotFamilies: [HTMLSlotElement, "icon1Family" | "icon2Family"][] =
            [
                [this.icon1Slot, "icon1Family"],
                [this.icon2Slot, "icon2Family"],
            ]
        for (const [e, f] of slotFamilies) {
            const family =
                this.getAttribute(MechaneTextfield.attribute[f]) ??
                materialIconsClass.round
            if (isMaterialIconsFamily(family)) {
                setMaterialIconsClass(e, family)
            }
        }

        // Input element attributes.

        this.input.type = this.hasAttribute(MechaneTextfield.attribute.mask)
            ? "password"
            : "text"

        const placeholder = this.getAttribute(
            MechaneTextfield.attribute.placeholder
        )
        if (placeholder) {
            this.input.placeholder = placeholder
        }

        this.input.spellcheck = this.hasAttribute(
            MechaneTextfield.attribute.spellcheck
        )
            ? true
            : false

        // Input element style.

        const size = Number(
            this.getAttribute(MechaneTextfield.attribute.size) ?? NaN
        )

        // Label.

        const label = this.getAttribute(MechaneTextfield.attribute.label)
        if (label) {
            this.labelText.textContent = label
        }

        const originPosition =
            getOriginPosition(
                this.getAttribute(MechaneTextfield.attribute.originPosition)
            ) ?? theme.defaultOriginPosition

        // Accumulate CSS rules in a style sheet.
        const sheet: Sheet = new Array<Rule>()

        // Include the font styles. Even though the font rules are defined
        // globally, inside a custom element the font rules need to be defined
        // again.
        sheet.push(...materialIcons)

        // Attribute flags can modify which styling is applied.

        const disabled = this.hasAttribute(disabledAttribute)
        const submitted = this.hasAttribute(
            MechaneTextfield.attribute.submitEffect
        )

        // Container element.
        if (disabled) {
            sheet.push({
                selector: css.container.selector.normal,
                properties: css.container.style.disabled(originPosition),
            })
        } else if (submitted) {
            sheet.push({
                selector: css.container.selector.normal,
                properties: css.container.style.submitted(originPosition),
            })
        } else {
            sheet.push(
                {
                    selector: css.container.selector.normal,
                    properties: css.container.style.normal(originPosition),
                },
                {
                    selector: css.container.selector.hover,
                    properties: css.container.style.hover(),
                }
            )
        }
        sheet.push({
            selector: css.containerChildren.selector,
            properties: css.containerChildren.style,
        })

        // Submit effect element.
        if (submitted) {
            const flashDuration =
                this.getAttribute(
                    MechaneTextfield.attribute.cooldownDuration
                ) ?? theme.defaultFlashDuration
            sheet.push(
                {
                    selector: css.submitEffect.selector.normal,
                    properties: css.submitEffect.style.normal(
                        flashDuration,
                        originPosition
                    ),
                },
                {
                    selector: css.submitEffect.selector.keyframes,
                    properties: css.submitEffect.style.keyframes(theme.accent),
                }
            )
        }

        // Slot elements.
        const iconColour1 =
            this.getAttribute(MechaneTextfield.attribute.icon1Colour) ??
            theme.primary
        sheet.push({
            selector: css.iconSlot1.selector,
            properties: css.iconSlot1.style(
                disabled ? theme.disabled : iconColour1
            ),
        })
        const iconColour2 =
            this.getAttribute(MechaneTextfield.attribute.icon2Colour) ??
            theme.primary
        sheet.push({
            selector: css.iconSlot2.selector,
            properties: css.iconSlot2.style(
                disabled ? theme.disabled : iconColour2
            ),
        })
        sheet.push({
            selector: css.button.selector,
            properties: css.button.style,
        })

        // Input field.
        sheet.push({
            selector: css.inputField.selector,
            properties: css.inputField.style(disabled, size),
        })
        if (!disabled) {
            sheet.push({
                selector: css.hoverInputField.selector,
                properties: css.hoverInputField.style,
            })
        }
        sheet.push({
            selector: css.activeInputField.selector,
            properties: css.activeInputField.style(disabled),
        })

        // Label text.
        sheet.push({
            selector: css.labelText.selector,
            properties: css.labelText.style,
        })
        sheet.push({
            selector: css.activeLabelText.selector,
            properties: css.activeLabelText.style,
        })

        // Apply the accumulated CSS rules.
        style.textContent = toString(sheet)
    }
}
