export interface MechaneTextfieldKeyupListener {
    callback: (ev: KeyboardEvent) => void
    readonly element: HTMLElement
    readonly event: "keyup"
}

export interface MechaneTextfieldKeydownListener {
    callback: (ev: KeyboardEvent) => void
    readonly element: HTMLElement
    readonly event: "keydown"
}
