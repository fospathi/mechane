import type * as CSS from "csstype"
import type { RoughCanvas } from "roughjs/bin/canvas"
import rough from "roughjs/bin/rough"
import { stringify } from "@stitches/stringify"

import { type Rule, type Sheet, toString } from "../../crule/crule"
import { fonts } from "../font"
import { stitching as stitchingType } from "../../schema/schema"
import { theme } from "../shutterbug-pixels/style/theme"

// Allow a user to choose, from the restricted set of allowed options, the
// position and size of cameras on the ShutterbugPixels view screen.
export class StitchingChooser extends HTMLElement {
    static get observedAttributes() {
        return [StitchingChooser.associationsAttribute, "w"]
    }
    // Attribute providing the stitching method and camera names.
    static readonly associationsAttribute = "associations"

    // The tag name of this custom element.
    static readonly tag = "stitching-chooser"

    // The class used by buttons that only appear when the :hover is on for the
    // canvas element.
    static readonly vanishingButtonClass =
        StitchingChooser.tag + "-vanishing-button-class"

    // The camera swap buttons fire a custom event to the Elm integrant in
    // order to swap two camera positions.
    static readonly cameraSwapEvent = StitchingChooser.tag + "-switch-cameras"

    // The stitching size toggle button fires a custom event to the Elm
    // integrant in order to switch between partial and full size camera
    // stitching.
    static readonly toggleStitchingSizeEvent =
        StitchingChooser.tag + "-toggle-stitching-size"

    // The width to height ratio of the size of the canvas element.
    static readonly ratio = 1.6

    // The canvas dimensions in terms of pixels. To scale the canvas to occupy a
    // different sized area but still leaving the canvas pixel dimensions the
    // same use the "w" attribute.
    static readonly height = 240
    static readonly width = StitchingChooser.height * StitchingChooser.ratio

    static readonly canvasContainerID =
        StitchingChooser.tag + "-canvas-container"
    readonly canvasContainer = document.createElement("div")

    // The stitching chooser is visually implemented with a canvas. The canvas
    // is used to draw, for example, the outlines of screens showing the camera
    // stitching situation.
    readonly context = document.createElement("canvas").getContext("2d")

    // Camera swappers allow the user to swap two camera positions.
    readonly cameraSwapper1 = cameraSwapper()
    readonly cameraSwapper2 = cameraSwapper()
    readonly cameraSwapper3 = cameraSwapper()

    // Stitching size togglers.
    readonly fullSizeOn = stitchingSizeToggler(true)
    readonly fullSizeOff = stitchingSizeToggler(false)

    constructor() {
        super()

        // Instance fields.

        if (!this.context) {
            console.log(
                `${StitchingChooser.tag}: failed to create a canvas rendering context`
            )
            return
        }
        this.context.canvas.width = StitchingChooser.width
        this.context.canvas.height = StitchingChooser.height

        this.canvasContainer.id = StitchingChooser.canvasContainerID
        this.canvasContainer.style.position = "relative" // For absolutely positioned canvas overlay buttons.
        this.canvasContainer.append(
            this.context.canvas,
            this.cameraSwapper1,
            this.cameraSwapper2,
            this.cameraSwapper3,
            this.fullSizeOff,
            this.fullSizeOn
        )

        // Shadow root and style.

        const shadowRoot = this.attachShadow({ mode: "open" })
        const style = document.createElement("style")
        shadowRoot.append(style, this.canvasContainer)
    }

    attributeChangedCallback() {
        this.updateStyle()
    }

    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return
        }

        this.updateStyle()
    }

    updateStyle() {
        if (!this.shadowRoot) {
            return
        }
        const style = this.shadowRoot.querySelector("style")
        if (!style) {
            return
        }
        if (!this.context) {
            return
        }

        const associationsValue = this.getAttribute(
            StitchingChooser.associationsAttribute
        )
        if (!associationsValue) {
            return
        }
        const associations = associationsValue.split(",")
        const stitching = associations.at(0)
        const cameras = associations.slice(1)

        if (!stitching || !stitchingType.safeParse(stitching).success) {
            console.log(
                `${StitchingChooser.tag}: ` +
                    (stitching
                        ? `unknown stitching method: ${stitching}`
                        : "a stitching method is required")
            )
            return
        }

        // Reset camera switchers to hidden, then enable later as required.
        this.cameraSwapper1.style.display = "none"
        this.cameraSwapper2.style.display = "none"
        this.cameraSwapper3.style.display = "none"
        // Likewise with the camera size togglers.
        this.fullSizeOff.style.display = "none"
        this.fullSizeOn.style.display = "none"

        const w = StitchingChooser.width
        const h = StitchingChooser.height
        this.context.clearRect(0, 0, w, h)
        const roughCanvas = rough.canvas(this.context.canvas)
        const padding = 5
        const innerH = h - 2 * padding
        const innerW = w - 2 * padding
        const smallFontH = 15
        const fullFontH = 24

        // Accumulate CSS rules in a style sheet.
        const sheet: Sheet = new Array<Rule>()

        // Include the font styles. Even though the font rules are defined
        // globally, inside a custom element the font rules need to be defined
        // again.
        sheet.push(...fonts)

        // Apply part of the style for the camera switch buttons and the
        // stitching size toggle button.

        // They are only visible when the :hover is on for the container.
        interface VanishingButtonOpacity extends CSS.Properties {
            "--vanishing-button-opacity": number
        }
        sheet.push(
            {
                selector: `#${StitchingChooser.canvasContainerID}`,
                properties: {
                    "--vanishing-button-opacity": 0,
                } as VanishingButtonOpacity,
            },
            {
                selector: `#${StitchingChooser.canvasContainerID}:hover`,
                properties: {
                    "--vanishing-button-opacity": 1,
                } as VanishingButtonOpacity,
            }
        )

        // They are circular buttons with a variable border and box shadow.
        const buttonBorder = "0.1em"
        const buttonPadding = "0.3em"
        sheet.push(
            {
                selector: `.${StitchingChooser.vanishingButtonClass}`,
                properties: {
                    background: theme.buttonBackground,
                    borderRadius: `calc(0.5em + ${buttonBorder} + ${buttonPadding})`, // 0.5 (half text size)
                    color: theme.primary,
                    border: `${theme.primary} dotted ${buttonBorder}`,
                    opacity: "var(--vanishing-button-opacity)",
                    padding: buttonPadding,
                    transition: "box-shadow 300ms, opacity 300ms",
                    userSelect: "none",
                },
            },
            {
                selector: `.${StitchingChooser.vanishingButtonClass}:hover`,
                properties: {
                    borderStyle: "solid",
                    boxShadow: `0 0 0.3em 0.2em ${theme.primaryGlow}`,
                },
            }
        )

        // If required, scale the canvas element beyond its actual pixels unit
        // dimensions with CSS.
        const cssWidth = this.getAttribute("w")
        if (cssWidth) {
            this.canvasContainer.style.width = cssWidth
            sheet.push({
                selector: "canvas",
                properties: { width: `${cssWidth}` },
            })
        }

        // Apply the accumulated CSS rules.
        style.textContent = toString(sheet)

        if (cameras.length == 0 || !cameras[0]) {
            if (stitching == "uni") {
                // Deal with the no camera associations special case.
                roughCanvas.rectangle(padding, padding, innerW, innerH, {
                    stroke: theme.textPassive,
                })
                roughCanvas.circle(w / 2, h / 2, h / 4, { fill: theme.error })
            } else {
                console.log(
                    `${StitchingChooser.tag}: ` + "camera names are required"
                )
            }
            return
        }

        const drawContext = {
            bowing: 3,
            cameras: cameras,
            cameraSwappers: [
                this.cameraSwapper1,
                this.cameraSwapper2,
                this.cameraSwapper3,
            ],
            context: this.context,
            fullFillStyle: "hachure",
            fullFont: `${fullFontH}px CascadiaCode`,
            fullFontBold: `bold ${fullFontH}px CascadiaCode`,
            fullFontH: fullFontH,
            fullPadding: padding * 3,
            fullSizeOn: this.fullSizeOn,
            fullSizeOff: this.fullSizeOff,
            h: h,
            innerH: innerH,
            innerW: innerW,
            padding: padding,
            roughCanvas: roughCanvas,
            smallFill: theme.stitchingMinor1,
            smallFont: `${smallFontH}px CascadiaCode`,
            smallFontH: smallFontH,
            smallStroke: theme.stitchingMinor2,
            smallH: innerH / 3,
            smallW: innerW / 3,
            w: w,
        } as const

        // Draw the major screen. It always occupies the whole view and is the
        // farthest back.

        roughCanvas.rectangle(padding, padding, innerW, innerH, {
            fill: theme.stitchingMajor,
            hachureGap: 12,
            stroke: theme.textPassive,
        })

        // Draw the minor screen(s).

        switch (stitching) {
            case "uni":
                drawPartialModeMajorName(drawContext, cameras[0])
                break
            case "bi":
            case "tri":
            case "quad":
                drawFullModeMinorScreens(drawContext)
                break

            case "biPartial":
            case "triPartial":
            case "quadPartial":
                drawPartialModeMinorScreens(drawContext)
                break
        }
    }
}

function cameraSwapper(): HTMLElement {
    const textStyle: CSS.Properties = {
        // Hide by default. There are more camera swappers than required in some
        // cases.
        display: "none",
        position: "absolute",
    }
    const text = document.createElement("text")
    text.style.cssText = stringify(textStyle)

    const spanStyle: CSS.Properties = {
        transform: "translate(-50%, -50%)",
    }
    const span = document.createElement("span")
    span.style.cssText = stringify(spanStyle)
    span.classList.add("material-icons", StitchingChooser.vanishingButtonClass)
    span.innerHTML = "cameraswitch"

    text.append(span)
    return text
}

function stitchingSizeToggler(isPartial: boolean): HTMLElement {
    const textStyle: CSS.Properties = {
        display: "none",
        left: "1em",
        position: "absolute",
        top: "1em",
    }
    const text = document.createElement("text")
    text.title = isPartial ? "Go full size mode." : "Go mixed size mode."
    text.style.cssText = stringify(textStyle)
    text.onclick = () => {
        const event = new CustomEvent(
            StitchingChooser.toggleStitchingSizeEvent,
            {
                bubbles: true,
                cancelable: false,
                composed: true,
            }
        )
        text.dispatchEvent(event)
    }

    const span = document.createElement("span")
    span.classList.add("material-icons", StitchingChooser.vanishingButtonClass)
    if (isPartial) {
        span.innerHTML = "open_in_full"
    } else {
        span.innerHTML = "close_fullscreen"
    }

    text.append(span)
    return text
}

interface DrawContext {
    bowing: number
    cameras: string[]
    cameraSwappers: readonly [HTMLElement, HTMLElement, HTMLElement]
    context: CanvasRenderingContext2D
    fullFillStyle: string
    fullFont: string
    fullFontBold: string
    fullFontH: number
    fullPadding: number
    fullSizeOn: HTMLElement
    fullSizeOff: HTMLElement
    h: number
    innerW: number
    innerH: number
    padding: number
    roughCanvas: RoughCanvas
    smallFill: string
    smallFont: string
    smallFontH: number
    smallStroke: string
    smallW: number
    smallH: number
    w: number
}

function drawSmallScreen(
    x: number,
    y: number,
    major: string,
    minor: string,
    switcher: HTMLElement,
    {
        bowing,
        context,
        h,
        roughCanvas,
        smallFill,
        smallFont,
        smallFontH,
        smallStroke,
        smallH,
        smallW,
        w,
    }: DrawContext
) {
    // Fade the background.
    context.beginPath()
    context.rect(x, y, smallW, smallH)
    context.closePath()
    context.fillStyle = theme.buttonBackground
    context.fill()

    // Draw the small screen.
    roughCanvas.rectangle(x, y, smallW, smallH, {
        bowing: bowing,
        fill: smallFill,
        fillWeight: 0.5,
        hachureAngle: 41,
        hachureGap: 10,
        stroke: smallStroke,
        strokeWidth: 3,
    })

    // Name the small screen.
    drawTextWithShadow(
        minor,
        x + smallW / 2,
        y + smallH / 2 + smallFontH / 2,
        context,
        smallFill,
        smallFont,
        smallW
    )

    // Add a camera switcher.
    switcher.style.display = "inline" // Undo display none.
    switcher.style.left = `${(100 * (x + smallW / 2)) / w}%`
    switcher.style.top = `${(100 * y) / h}%`
    switcher.style.transform = "translate(0%, -20%)"
    switcher.title = `Swap ${major} with ${minor}.`
    switcher.onclick = () => {
        const event = new CustomEvent(StitchingChooser.cameraSwapEvent, {
            bubbles: true,
            cancelable: false,
            composed: true,
            detail: { camera1: major, camera2: minor },
        })
        switcher.dispatchEvent(event)
    }
}

function drawTextWithShadow(
    text: string,
    x: number,
    y: number,
    context: CanvasRenderingContext2D,
    fill: string,
    font?: string,
    maxWidth?: number
) {
    context.filter =
        `drop-shadow(0px 0px 5px ${theme.buttonOpaqueBackground}) `.repeat(3)
    context.fillStyle = fill
    if (font) {
        context.font = font
    }
    context.fillText(text, x, y, maxWidth)
    context.filter = "none"
}

function drawFullModeName(
    dc: DrawContext,
    name: string,
    y: number,
    fill: string
) {
    dc.context.font = dc.fullFontBold
    dc.context.textAlign = "end"
    drawTextWithShadow(name, dc.w - (2 * dc.smallW) / 3, y, dc.context, fill)
}

function drawFullModeMinorScreens(dc: DrawContext) {
    const l = dc.cameras.length
    if (l < 2 || l > 4) {
        return
    }
    const dest = document.createElement("canvas").getContext("2d")
    if (!dest) {
        return
    }
    const src = document.createElement("canvas").getContext("2d")
    if (!src) {
        return
    }

    dest.canvas.width = dc.w
    dest.canvas.height = dc.h
    src.canvas.width = dc.w
    src.canvas.height = dc.h
    const roughSrc = rough.canvas(src.canvas)

    const angle = -41 // A default angle in the roughjs package.
    const angles = [angle + 45, angle + 2 * 45, angle + 3 * 45]
    const fills = [
        theme.stitchingMinor1,
        theme.stitchingMinor2,
        theme.stitchingMinor3,
    ]
    const paddingRatios = [1, 0.75, 0.5] // Ratio of the default padding.
    const textEnd = dc.w - (2 * dc.smallW) / 3 // Text is right aligned to this.
    const switcherMiddle = dc.w - dc.smallW / 3

    // Ratios of the height.
    const fontRatio = dc.fullFontH / dc.h
    const gapRatio = (1 - 0.15 * 2) / 3 // Gap between baselines of text.
    const startRatio = 0.85 // Position the first text at this height.

    // Draw the name of the major screen.
    drawFullModeName(
        dc,
        dc.cameras[0] as string,
        dc.h * startRatio + dc.fullFontH / 2,
        theme.primaryLight
    )

    for (let i = 0; i < l - 1; i++) {
        // Fill dest with the fuzzed up text of previous screens.
        dest.clearRect(0, 0, dc.w, dc.h)
        dest.globalCompositeOperation = "source-over" // Restore the normal op.
        dest.filter = "blur(6px)" // Makes dest a partially transparent mask.
        dest.fillStyle = theme.buttonOpaqueBackground
        dest.font = dc.fullFontBold
        dest.textAlign = "end"
        for (let j = i; j >= 0; j--) {
            // Increase iterations to increase text visibility below subsequent
            // hachured rectangular screen areas.
            const fuzzIterations = 1
            for (let k = 0; k < fuzzIterations; k++) {
                dest.fillText(
                    dc.cameras[j] as string,
                    textEnd,
                    dc.h * (startRatio - gapRatio * j) + dc.fullFontH / 2
                )
            }
        }
        dest.filter = "none"

        // When there are four cams you want to start with the first (biggest)
        // padding for the innermost minor camera. Then each successive camera
        // uses less padding so the strokes of closer cameras are outwith the
        // strokes of farther cameras.
        //
        // When there are only two cams, the single minor camera should use the
        // smallest padding.
        const paddingStartIndex = 4 - l
        const paddingRatio = paddingRatios[i + paddingStartIndex] as number

        // Fill src with the hachured rectangular screen area.
        src.clearRect(0, 0, dc.w, dc.h)
        roughSrc.rectangle(
            dc.fullPadding * paddingRatio,
            dc.fullPadding * paddingRatio,
            dc.w - 2 * dc.fullPadding * paddingRatio,
            dc.h - 2 * dc.fullPadding * paddingRatio,
            {
                bowing: dc.bowing,
                fill: fills[i] as string,
                fillStyle: dc.fullFillStyle,
                fillWeight: 6,
                hachureAngle: angles[i] as number,
                hachureGap: 45,
                stroke: fills[i] as string,
                strokeWidth: 3,
            }
        )

        // Draw the hachured rectangular screen area but leave partial holes
        // above the text of previous rectangular screen areas.

        // The leave-holes-where-dest-exists compositing operation.
        dest.globalCompositeOperation = "source-out"
        dest.drawImage(src.canvas, 0, 0)
        dc.context.drawImage(dest.canvas, 0, 0)

        // Draw the text of the current screen.
        const y = dc.h * (startRatio - gapRatio * (i + 1)) + dc.fullFontH / 2
        drawFullModeName(dc, dc.cameras[i + 1] as string, y, fills[i] as string)

        // Add a camera switcher.
        const switcher = dc.cameraSwappers[i]
        const prevCamera = dc.cameras[i]
        const camera = dc.cameras[i + 1]
        if (!switcher || !prevCamera || !camera) {
            return
        }
        switcher.style.display = "inline" // Undo display none.
        switcher.style.left = `${100 * (switcherMiddle / dc.w)}%`
        switcher.style.top = `${100 * (y / dc.h + (gapRatio - fontRatio) / 2)}%`
        switcher.style.transform = "translate(0%, 0%)" // Clear transform from partial mode.
        switcher.title = `Swap ${prevCamera} with ${camera}.`
        switcher.onclick = () => {
            const event = new CustomEvent(StitchingChooser.cameraSwapEvent, {
                bubbles: true,
                cancelable: false,
                composed: true,
                detail: { camera1: prevCamera, camera2: camera },
            })
            switcher.dispatchEvent(event)
        }
    }

    // Show the stitching size toggler which enables mixed size mode.
    dc.fullSizeOff.style.display = "inline" // Undo display none.
}

function drawPartialModeMajorName(dc: DrawContext, name: string) {
    dc.context.font = dc.fullFont
    dc.context.textAlign = "center"
    drawTextWithShadow(
        name,
        dc.padding + dc.innerW / 2,
        dc.padding + dc.smallH,
        dc.context,
        theme.stitchingMajor
    )
}

function drawPartialModeMinorScreens(dc: DrawContext) {
    if (dc.cameras.length < 2 || dc.cameras.length > 4) {
        return
    }
    const major = dc.cameras[0] as string

    // Name the major screen.
    drawPartialModeMajorName(dc, major)

    // Show the stitching size toggler which enables full size mode.
    dc.fullSizeOn.style.display = "inline" // Undo display none.

    // Draw the minor screens left to right.
    for (let i = 0; i < dc.cameras.length - 1; i++) {
        let xPadding = 0
        switch (dc.cameras.length) {
            case 2:
                xPadding = dc.padding + 2 * dc.smallW
                break
            case 3:
                xPadding = dc.padding + 2 * i * dc.smallW
                break
            case 4:
                xPadding = dc.padding + i * dc.smallW
                break
        }
        drawSmallScreen(
            xPadding,
            dc.padding + 2 * dc.smallH,
            major,
            dc.cameras[i + 1] as string,
            dc.cameraSwappers[i] as HTMLElement,
            dc
        )
    }
}
