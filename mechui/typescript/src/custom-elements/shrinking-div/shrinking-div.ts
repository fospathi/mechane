import {
    type KeyframesProperties,
    type Rule,
    type Sheet,
    toString,
} from "../../crule/crule"
import { GrowingDiv } from "../growing-div/growing-div"

// A ShrinkingDiv scales its content to zero over a given duration then
// vanishes.
//
// As a custom element with time varying effects, for it to be used by the Elm
// language integrant, the start time is cached statically by the value of the
// name attribute. If Elm inserts a new instance with the same name the
// animation effect uses the cached start time.
//
// A ShrinkingDiv with a given name interacts with a GrowingDiv with the same
// name; when a ShrinkingDiv with a given name is created it deletes the cached
// start time for that name in the GrowingDiv's start time cache.
export class ShrinkingDiv extends HTMLElement {
    static get observedAttributes() {
        return [
            ShrinkingDiv.nameAttribute,
            ShrinkingDiv.namesAttribute,
            ShrinkingDiv.sessionAttribute,
        ]
    }
    // Attribute giving the instance name.
    static readonly nameAttribute = "name"
    // Attribute giving the instance names to keep cached for now.
    static readonly namesAttribute = "names"
    // Attribute indicating a session. Start dates are cleared by a change of
    // session. It is also added to animation names for uniqueness.
    static readonly sessionAttribute = "session"

    // The tag name of this custom element.
    static readonly tag = "shrinking-div"

    // Start date cache for persistence of animation effects in elements created
    // by the Elm integrant.
    //
    // The date is the number of milliseconds since the UNIX epoch.
    static readonly startDates: Map<string, number> = new Map()
    static session = "0"

    // Attribute controlling the duration of the shrink.
    static readonly durationAttribute = "duration"
    static readonly defaultDuration = "2s"

    static readonly animationNamePrefix = "shrinking-div"
    static readonly divID = "shrinking-div"
    static readonly slotName = "content"

    // Descendants of the shadowRoot.

    readonly contentSlot = document.createElement("slot")
    readonly div = document.createElement("div")

    constructor() {
        super()

        // Instance fields.

        this.div.id = ShrinkingDiv.divID
        this.contentSlot.name = ShrinkingDiv.slotName
        this.div.append(this.contentSlot)

        // Shadow root and style.

        const shadowRoot = this.attachShadow({ mode: "open" })
        const style = document.createElement("style")
        shadowRoot.append(style, this.div)
    }

    attributeChangedCallback(attrName: string, oldVal: string, newVal: string) {
        if (attrName == ShrinkingDiv.nameAttribute && oldVal && newVal) {
            // The element instance is being repurposed by the Elm integrant.
            ShrinkingDiv.startDates.set(newVal, Date.now())
            this.updateStyle(newVal)
        }
    }

    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return
        }

        const connectedDate = Date.now()

        // Detect a new session and if so, clear all start dates.
        const session =
            this.getAttribute(ShrinkingDiv.sessionAttribute) ??
            ShrinkingDiv.session
        if (session != ShrinkingDiv.session) {
            ShrinkingDiv.startDates.clear()
            ShrinkingDiv.session = session
        }

        // Record the start date for this name if a start date doesn't already
        // exist.
        const name = this.getAttribute(ShrinkingDiv.nameAttribute)
        if (name) {
            const date = ShrinkingDiv.startDates.get(name)
            if (!date) {
                ShrinkingDiv.startDates.set(name, connectedDate)
            }

            // Delete the equivalent growing-div start time, if any.
            GrowingDiv.startDates.delete(name)
        }

        // Prevent the start date cache accumulating dates for too many obsolete
        // names.
        const names = this.getAttribute(ShrinkingDiv.namesAttribute)
        if (names) {
            const keeps = names.split(",")
            for (const name of ShrinkingDiv.startDates.keys()) {
                if (!keeps.includes(name)) {
                    ShrinkingDiv.startDates.delete(name)
                }
            }
        }

        this.updateStyle()
    }

    updateStyle(newName?: string) {
        const style = this.shadowRoot?.querySelector("style")
        if (!style) {
            return
        }

        const name = newName ?? this.getAttribute(ShrinkingDiv.nameAttribute)
        if (!name) {
            return
        }

        const start = ShrinkingDiv.startDates.get(name)
        if (!start) {
            return
        }

        const duration =
            this.getAttribute(ShrinkingDiv.durationAttribute) ??
            ShrinkingDiv.defaultDuration

        // Get the duration in milliseconds as a number.

        let interval: number
        if (duration.endsWith("ms")) {
            interval = Number(duration.slice(0, -2))
        } else if (duration.endsWith("s")) {
            interval = Number(duration.slice(0, -1)) * 1000
        } else {
            return
        }
        if (isNaN(interval)) {
            return
        }

        // Determine how far through the animation we are.

        const elapsed = Date.now() - start
        let t = elapsed
        if (elapsed < 0) {
            t = 0
        } else if (elapsed > interval) {
            t = interval
        }

        // Accumulate CSS rules in a style sheet.
        const sheet: Sheet = new Array<Rule>()

        if (t === interval) {
            sheet.push({
                selector: `#${ShrinkingDiv.divID}`,
                properties: {
                    display: "none",
                },
            })
            return
        }

        const animationKeyframes: KeyframesProperties = {
            from: {
                transform: `scale(1, ${1 - t / interval})`,
            },
            "99.9%": {
                position: "static",
            },
            to: {
                display: "none",
                position: "absolute",
                transform: "scale(1, 0)",
            },
        }

        const animationName =
            ShrinkingDiv.animationNamePrefix + name + ShrinkingDiv.session

        sheet.push({
            selector: `@keyframes ${animationName}`,
            properties: animationKeyframes,
        })

        sheet.push({
            selector: `#${ShrinkingDiv.divID}`,
            properties: {
                animation: `
                ${animationName} 
                ${interval - t}ms
                `,
                animationFillMode: "forwards",
            },
        })

        // Apply the accumulated CSS rules.
        style.textContent = toString(sheet)
    }
}
