import type * as CSS from "csstype"

import type { FontFaceProperties, Rule } from "../crule/crule"

// Cascadia Code.

const cascadiaCodeFontFaceProperties: FontFaceProperties = {
    fontFamily: "CascadiaCode",
    src:
        "url('/font/CascadiaCode-2111.01/woff2/CascadiaCode.woff2') format('woff2')," +
        "url('/font/CascadiaCode-2111.01/ttf/CascadiaCode.ttf') format('truetype')",
}

const cascadiaCode: Rule[] = [
    {
        selector: "@font-face",
        properties: cascadiaCodeFontFaceProperties,
    },
]

// Material Icons.

export const materialIconsClass = {
    regular: "material-icons",
    outlined: "material-icons-outlined",
    round: "material-icons-round",
    sharp: "material-icons-sharp",
    twoTone: "material-icons-two-tone",
} as const

export type MaterialIconsFamily =
    typeof materialIconsClass[keyof typeof materialIconsClass]

export function isMaterialIconsFamily(
    value: string
): value is MaterialIconsFamily {
    return Object.values(materialIconsClass).includes(
        value as MaterialIconsFamily
    )
}

export function setMaterialIconsClass(
    e: HTMLElement,
    family: MaterialIconsFamily
) {
    for (const iconClass of Object.values(materialIconsClass)) {
        if (family == iconClass) {
            e.classList.add(iconClass)
        } else {
            e.classList.remove(iconClass)
        }
    }
}

const materialIconsFontFaceProperties: FontFaceProperties = {
    fontFamily: "Material Icons",
    fontStyle: "normal",
    fontWeight: 400,
    src: "url('/font/material-design-icons/MaterialIcons.woff2') format('woff2')",
}

const materialIconsOutlinedFontFaceProperties: FontFaceProperties = {
    fontFamily: "Material Icons Outlined",
    fontStyle: "normal",
    fontWeight: 400,
    src: "url('/font/material-design-icons/MaterialIconsOutlined.woff2') format('woff2')",
}

const materialIconsRoundFontFaceProperties: FontFaceProperties = {
    fontFamily: "Material Icons Round",
    fontStyle: "normal",
    fontWeight: 400,
    src: "url('/font/material-design-icons/MaterialIconsRound.woff2') format('woff2')",
}

const materialIconsSharpFontFaceProperties: FontFaceProperties = {
    fontFamily: "Material Icons Sharp",
    fontStyle: "normal",
    fontWeight: 400,
    src: "url('/font/material-design-icons/MaterialIconsSharp.woff2') format('woff2')",
}

const materialIconsTwoToneFontFaceProperties: FontFaceProperties = {
    fontFamily: "Material Icons Two Tone",
    fontStyle: "normal",
    fontWeight: 400,
    src: "url('/font/material-design-icons/MaterialIconsTwoTone.woff2') format('woff2')",
}

const materialIconsBase: CSS.Properties = {
    fontWeight: "normal",
    fontStyle: "normal",
    fontSize: "24px",
    lineHeight: 1,
    letterSpacing: "normal",
    textTransform: "none",
    display: "inline-block",
    whiteSpace: "nowrap",
    wordWrap: "normal",
    direction: "ltr",
    textRendering: "optimizeLegibility",
    fontFeatureSettings: '"liga"',
}

export const materialIcons: Rule[] = [
    {
        selector: "@font-face",
        properties: materialIconsFontFaceProperties,
    },
    {
        selector: `.${materialIconsClass.regular}`,
        properties: {
            fontFamily: "Material Icons",
            ...materialIconsBase,
        },
    },

    {
        selector: "@font-face",
        properties: materialIconsOutlinedFontFaceProperties,
    },
    {
        selector: `.${materialIconsClass.outlined}`,
        properties: {
            fontFamily: "Material Icons Outlined",
            ...materialIconsBase,
        },
    },

    {
        selector: "@font-face",
        properties: materialIconsRoundFontFaceProperties,
    },
    {
        selector: `.${materialIconsClass.round}`,
        properties: {
            fontFamily: "Material Icons Round",
            ...materialIconsBase,
        },
    },

    {
        selector: "@font-face",
        properties: materialIconsSharpFontFaceProperties,
    },
    {
        selector: `.${materialIconsClass.sharp}`,
        properties: {
            fontFamily: "Material Icons Sharp",
            ...materialIconsBase,
        },
    },

    {
        selector: "@font-face",
        properties: materialIconsTwoToneFontFaceProperties,
    },
    {
        selector: `.${materialIconsClass.twoTone}`,
        properties: {
            fontFamily: "Material Icons Two Tone",
            ...materialIconsBase,
        },
    },
]

// CSS style rules for the default fonts used by the Mechane UI. They exist for
// the sake of custom elements which need to specify the CSS style rules of
// fonts they use even if the fonts are already globally defined.
export const fonts: Rule[] = cascadiaCode.concat(materialIcons)
