import {
    type KeyframesProperties,
    type Rule,
    type Sheet,
    toString,
} from "../../crule/crule"
import { ShrinkingDiv } from "../shrinking-div/shrinking-div"

// A GrowingDiv scales its content from zero to one over a given duration.
//
// As a custom element with time varying effects, for it to be used by the Elm
// language integrant, the start time is cached statically by the value of the
// name attribute. If Elm inserts a new instance with the same name the
// animation effect uses the cached start time.
//
// A GrowingDiv with a given name interacts with a ShrinkingDiv with the same
// name; when a GrowingDiv with a given name is created it deletes the cached
// start time for that name in the ShrinkingDiv's start time cache.
export class GrowingDiv extends HTMLElement {
    static get observedAttributes() {
        return [
            GrowingDiv.nameAttribute,
            GrowingDiv.namesAttribute,
            GrowingDiv.sessionAttribute,
        ]
    }
    // Attribute giving the instance name.
    static readonly nameAttribute = "name"
    // Attribute giving the instance names to keep cached for now.
    static readonly namesAttribute = "names"
    // Attribute indicating a session. Start dates are cleared by a change of
    // session. It is also added to animation names for uniqueness.
    static readonly sessionAttribute = "session"

    // The tag name of this custom element.
    static readonly tag = "growing-div"

    // Start date cache for persistence of animation effects in elements created
    // by the Elm integrant.
    //
    // The date is the number of milliseconds since the UNIX epoch.
    static readonly startDates: Map<string, number> = new Map()
    static session = "0"

    // Attribute controlling the duration of the grow.
    static readonly durationAttribute = "duration"
    static readonly defaultDuration = "2s"

    static readonly animationNamePrefix = "growing-div"
    static readonly divID = "growing-div"
    static readonly slotName = "content"

    // Descendants of the shadowRoot.

    readonly contentSlot = document.createElement("slot")
    readonly div = document.createElement("div")

    constructor() {
        super()

        // Instance fields.

        this.div.id = GrowingDiv.divID
        this.contentSlot.name = GrowingDiv.slotName
        this.div.append(this.contentSlot)

        // Shadow root and style.

        const shadowRoot = this.attachShadow({ mode: "open" })
        const style = document.createElement("style")
        shadowRoot.append(style, this.div)
    }

    attributeChangedCallback(attrName: string, oldVal: string, newVal: string) {
        if (attrName == GrowingDiv.nameAttribute && oldVal && newVal) {
            // The element instance is being repurposed by the Elm integrant.
            GrowingDiv.startDates.set(newVal, Date.now())
            this.updateStyle(newVal)
        }
    }

    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return
        }

        // Detect a new session and if so, clear all start dates.
        const session =
            this.getAttribute(GrowingDiv.sessionAttribute) ?? GrowingDiv.session
        if (session != GrowingDiv.session) {
            GrowingDiv.startDates.clear()
            GrowingDiv.session = session
        }

        const connectedDate = Date.now()

        // Record the start date for this name if a start date doesn't already
        // exist.
        const name = this.getAttribute(GrowingDiv.nameAttribute)
        if (name) {
            const date = GrowingDiv.startDates.get(name)
            if (!date) {
                GrowingDiv.startDates.set(name, connectedDate)
            }

            // Delete the equivalent shrinking-div start time, if any.
            ShrinkingDiv.startDates.delete(name)
        }

        // Prevent the start date cache accumulating dates for too many obsolete
        // names.
        const names = this.getAttribute(GrowingDiv.namesAttribute)
        if (names) {
            const keeps = names.split(",")
            for (const name of GrowingDiv.startDates.keys()) {
                if (!keeps.includes(name)) {
                    GrowingDiv.startDates.delete(name)
                }
            }
        }
        this.updateStyle()
    }

    updateStyle(newName?: string) {
        const style = this.shadowRoot?.querySelector("style")
        if (!style) {
            return
        }

        const name = newName ?? this.getAttribute(GrowingDiv.nameAttribute)
        if (!name) {
            return
        }

        const start = GrowingDiv.startDates.get(name)
        if (!start) {
            return
        }

        const duration =
            this.getAttribute(GrowingDiv.durationAttribute) ??
            GrowingDiv.defaultDuration

        // Get the duration in milliseconds as a number.

        let interval: number
        if (duration.endsWith("ms")) {
            interval = Number(duration.slice(0, -2))
        } else if (duration.endsWith("s")) {
            interval = Number(duration.slice(0, -1)) * 1000
        } else {
            return
        }
        if (isNaN(interval)) {
            return
        }

        // Determine how far through the animation we are.

        const elapsed = Date.now() - start

        let t = elapsed
        if (elapsed < 0) {
            t = 0
        } else if (elapsed > interval) {
            t = interval
        }

        // Accumulate CSS rules in a style sheet.
        const sheet: Sheet = new Array<Rule>()

        if (t === interval) {
            return
        }

        const animationKeyframes: KeyframesProperties = {
            from: {
                transform: `scale(1, ${t / interval})`,
            },
            to: {
                transform: `scale(1, 1)`,
            },
        }

        const animationName =
            GrowingDiv.animationNamePrefix + name + GrowingDiv.session

        sheet.push({
            selector: `@keyframes ${animationName}`,
            properties: animationKeyframes,
        })

        sheet.push({
            selector: `#${GrowingDiv.divID}`,
            properties: {
                animation: `
                ${animationName} 
                ${interval - t}ms
                `,
                animationFillMode: "forwards",
            },
        })

        // Apply the accumulated CSS rules.
        style.textContent = toString(sheet)
    }
}
