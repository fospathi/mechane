import { GrowingDiv } from "./growing-div/growing-div"
import { ShrinkingDiv } from "./shrinking-div/shrinking-div"

import { MechaneAsciidoc } from "./shutterbug-pixels/mechane-asciidoc/mechane-asciidoc"
import { MechaneButton } from "./shutterbug-pixels/mechane-button/mechane-button"
import { MechanePre } from "./shutterbug-pixels/mechane-pre/mechane-pre"
import { MechaneTextfield } from "./shutterbug-pixels/mechane-textfield/mechane-textfield"
import { ShutterbugPixels } from "./shutterbug-pixels/shutterbug-pixels"

import { StitchingChooser } from "./stitching-chooser/stitching-chooser"

customElements.define(GrowingDiv.tag, GrowingDiv)
customElements.define(ShrinkingDiv.tag, ShrinkingDiv)

customElements.define(ShutterbugPixels.tag, ShutterbugPixels)
customElements.define(MechaneAsciidoc.tag, MechaneAsciidoc)
customElements.define(MechaneButton.tag, MechaneButton)
customElements.define(MechanePre.tag, MechanePre)
customElements.define(MechaneTextfield.tag, MechaneTextfield)

customElements.define(StitchingChooser.tag, StitchingChooser)
