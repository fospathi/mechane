import * as schema from "../schema/schema"

const glow = "rgb(255, 210, 0)"

export type GetResult =
    | { result: "success"; motif: schema.Motif }
    | { result: "failure" }

export async function getMotif(): Promise<GetResult> {
    const response = await fetch(path, {
        method: "GET",
        headers: { Accept: "application/json" },
    })
    if (!response.ok) {
        return { result: "failure" }
    }
    const motif = schema.motif.safeParse(await response.json())
    if (!motif.success) {
        return { result: "failure" }
    }

    return { motif: motif.data, result: "success" }
}

export const initial: schema.Motif = {
    // The initial theme before the world's motif is known.
    motifName: "InitialMotif",

    accent: "deepskyblue",
    buttonBackground: "rgba(255, 255, 255, 0.85)",
    buttonOpaqueBackground: "rgb(255, 255, 255)",
    error: "red",
    primary: "darkorange",
    primaryGlow: glow,
    primaryLight: "orange",
    ready: "seagreen",
    textNormal: "black",
    textPassive: "gray",
    warning: "orange",

    boxShadow: "0px 0.1em 0.5em 1px DimGray",
    boxShadowGlow: `0px 0px 0.7em 0.2em ${glow}`,
    buttonHoverBackground: "rgba(255, 255, 255, 0.95)",
    disabled: "rgb(211, 211, 211)",
    inputBorderFocus: "rgb(30, 144, 255)", // Dodger blue.
    inputBorderPrimary: "gold",
    preBackground: "rgba(255, 248, 231, 0.85)", // Cosmic latte.
    stitchingMajor: "orange",
    stitchingMinor1: "deepskyblue",
    stitchingMinor2: "gold",
    stitchingMinor3: "silver",

    buttonBorder: "0.4",
    buttonBorderRadius: "1.2em",
    buttonIconFontSize: "2em",
    buttonPadding: "0.6em",
} as const

const path = "/motif/typescript"
