// Define custom elements first.
import "./custom-elements/define"

// Initialise the Elm app.
import { ports } from "./init/init"

import { theme } from "./custom-elements/shutterbug-pixels/style/theme"
import { disableOverlay } from "./custom-elements/shutterbug-pixels/update-overlay"
import { ShutterbugPixels } from "./custom-elements/shutterbug-pixels/shutterbug-pixels"
import { getMotif } from "./motif/motif"
import * as schema from "./schema/schema"
import { closeOpenedWebSockets } from "./websockets/opened-websockets"
import { openCameraWebSocket } from "./websockets/websocket-to-camera"
import { openDevoirWebSocket } from "./websockets/websocket-to-devoir"
import { openOverlayerWebSocket } from "./websockets/websocket-to-overlayer"
import { openPixelWebSocket } from "./websockets/websocket-to-pixel"
import { openUIEventWebSocket } from "./websockets/websocket-to-uievent"

await main()

async function main() {
    // Obtain the motif.

    const motifGetResult = await getMotif()
    if (motifGetResult.result === "failure") {
        ports.receiveLifeCycle.send({
            problem: "failed to get the Typescript motif",
        })
        return
    }
    Object.assign(theme, motifGetResult.motif)

    // Assert the successful acquisition of a canvas context and shadowRoot on
    // the ShutterbugPixels singleton class.

    const context = ShutterbugPixels.context
    if (context === null) {
        ports.receiveLifeCycle.send({
            problem: "failed to create a canvas rendering context",
        })
        return
    }

    const shadowRoot = new ShutterbugPixels().shadowRoot
    if (shadowRoot === null) {
        ports.receiveLifeCycle.send({
            problem: "failed to create a shadowRoot",
        })
        return
    }

    // The Elm integrant acquires a shutterbug from manual user entry or the
    // Mechane server via a HTTP request and forwards it to here, the Typescript
    // integrant.
    //
    // An extant shutterbug is needed to successfully open websockets on the
    // Mechane server.
    const shutterbug = await new Promise<string>(function (resolve) {
        // Receive the 'shutterbug' message from the Elm integrant.
        ports.sendShutterbug.subscribe(function callback(message: string) {
            ports.sendShutterbug.unsubscribe(callback) // Only need first message.

            const result = schema.shutterbug.safeParse(message)
            if (!result.success) {
                console.error(result.error.issues)
            }

            // Listen to the Elm integrant for user requests to change the
            // fullscreen state.
            ports.sendFullscreen.subscribe(function (fullscreen: boolean) {
                if (fullscreen && document.fullscreenElement === null) {
                    void document.documentElement.requestFullscreen()
                } else if (!fullscreen && document.fullscreenElement !== null) {
                    void document.exitFullscreen()
                }
            })

            // Listen to the Elm integrant for user requests to change the
            // pointer lock state.
            ports.sendPointerLock.subscribe(function (pointerLock: boolean) {
                if (pointerLock && document.pointerLockElement === null) {
                    void context.canvas.requestPointerLock()
                } else if (
                    !pointerLock &&
                    document.pointerLockElement !== null
                ) {
                    // The pointer unlock button will generally be inaccessible
                    // by the mouse in pointer lock mode and therefore useless
                    // but listen for clicks anyway for completeness sake.
                    void document.exitPointerLock()
                }
            })

            // Listen to the Elm integrant for user requests to open a new tab.
            ports.sendOpenNewTab.subscribe(function (url: string) {
                window.open(url, "_blank")
            })

            // Listen to the Elm integrant for user requests to copy the
            // shutterbug to the clipboard.
            ports.sendWriteShutterbugToClipboard.subscribe(function () {
                void navigator.clipboard.writeText(shutterbug)
            })

            // Listen to the Elm integrant for user requests to copy some kind
            // of text to the clipboard.
            ports.sendWriteTextToClipboard.subscribe(function (text: string) {
                void navigator.clipboard.writeText(text)
            })

            resolve(message)
        })
    })

    const openUIEventResult = openUIEventWebSocket(
        shutterbug,
        ShutterbugPixels.container,
        context,
        shadowRoot
    )
    const webSocketLifeCycles = [
        openCameraWebSocket(shutterbug),
        openDevoirWebSocket(shutterbug),
        openPixelWebSocket(shutterbug, context),
        openOverlayerWebSocket(shutterbug, ShutterbugPixels.overlay),
        openUIEventResult,
    ]
    const opened = webSocketLifeCycles.map((lifeCycle) => lifeCycle.opened)
    const closed = webSocketLifeCycles.map((lifeCycle) => lifeCycle.closed)

    const openWebSocketsResult = await Promise.any<"success" | "failure">([
        Promise.all(opened).then(() => "success"),
        Promise.any(closed).then(() => "failure"),
    ])

    if (openWebSocketsResult === "success") {
        ShutterbugPixels.uiEvents = await openUIEventResult.opened
        ports.receiveLifeCycle.send({ stage: "all WebSockets are open" })
    }

    const closeEvent = await Promise.any(closed)
    ports.receiveKaput.send(closeEvent)
    disableOverlay(ShutterbugPixels.overlay, closeEvent)
    ShutterbugPixels.uiEvents?.removeAllListeners()
    closeOpenedWebSockets(1000, "client is kaput; closing all websockets")
    if (document.pointerLockElement !== null) {
        void document.exitPointerLock()
    }
}
