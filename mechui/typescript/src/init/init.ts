import type { Ports } from "../ports/ports"

// The Elm instance declared below is provided by the Elm language integrant.
declare const Elm: ElmInstance<Ports>

const elmApp = Elm.Main.init({ flags: JSON.stringify(window.location.href) })

export const ports = elmApp.ports
