import {
    type UIEventListenersController,
    addListeners,
} from "../custom-elements/shutterbug-pixels/listen"
import { openedWebSockets } from "./opened-websockets"
import type * as schema from "../schema/schema"
import { newOnCloseCallback } from "./websocket-event"

/*

Handling of UI events in the browser based UI is split between the Elm and
Typescript integrants.

Typescript handles the ShutterbugPixels' events such as keyboard controlled
(WASD style) camera movements, mouse camera panning, overlay button clicks and
so on. These events are sent straight to the Mechane server bypassing the Elm
integrant.

Elm handles the Devoir buttons and settings for the UI. Some of these UI
commands may be forwarded to the Mechane Server via the Typescript integrant.

*/

// Open a uievent websocket on the Mechane server.
//
// The Typescript integrant needs to be able to send 'UI event' messages to the
// Mechane server.
//
// Fill out the listeners object.
export function openUIEventWebSocket(
    shutterbug: string,
    mainContainer: HTMLDivElement,
    context: CanvasRenderingContext2D,
    shadowRoot: ShadowRoot
): {
    closed: Promise<schema.WebSocketCloseEvent>
    opened: Promise<UIEventListenersController>
} {
    const uieventPath = "/uievent"
    const ws = new WebSocket("ws://" + window.location.host + uieventPath)

    const closePromise = new Promise<schema.WebSocketCloseEvent>((resolve) => {
        ws.onclose = newOnCloseCallback(ws, uieventPath, resolve)
    })

    const openPromise = new Promise<UIEventListenersController>(function (
        resolve
    ) {
        ws.onopen = function () {
            ws.send(JSON.stringify(shutterbug))
            openedWebSockets.push(ws)

            // Add UI event listeners to the aspects of the UI handled by the
            // Typescript integrant. These events are forwarded to the Mechane
            // server.
            const listenersController = addListeners(
                context,
                mainContainer,
                function (event) {
                    ws.send(JSON.stringify(event))
                },
                shadowRoot
            )

            resolve(listenersController)
        }
    })

    return {
        closed: closePromise,
        opened: openPromise,
    }
}
