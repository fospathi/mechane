import { openedWebSockets } from "./opened-websockets"
import * as schema from "../schema/schema.js"
import type { Overlay } from "../custom-elements/shutterbug-pixels/overlay"
import { updateOverlay } from "../custom-elements/shutterbug-pixels/update-overlay"
import { type WebSocketLifeCycle, newOnCloseCallback } from "./websocket-event"

// Open an overlayer websocket on the Mechane server.
//
// A Mechane server needs to be able to send 'overlay update' messages to the
// Typescript integrant.
export function openOverlayerWebSocket(
    shutterbug: string,
    overlay: Overlay
): WebSocketLifeCycle {
    const overlayerPath = "/overlayer"
    const ws = new WebSocket("ws://" + window.location.host + overlayerPath)

    const closePromise = new Promise<schema.WebSocketCloseEvent>((resolve) => {
        ws.onclose = newOnCloseCallback(ws, overlayerPath, resolve)
    })

    const openPromise = new Promise<void>(function (resolve) {
        ws.onopen = function () {
            ws.send(JSON.stringify(shutterbug))
            openedWebSockets.push(ws)
            resolve()
        }
    })

    // Receive 'overlay update' messages from the Mechane server.
    ws.onmessage = function (message) {
        if (typeof message.data !== "string") {
            return
        }
        const data: unknown = JSON.parse(message.data)

        const result = schema.overlayUpdate.safeParse(data)
        if (!result.success) {
            console.log(result.error.issues)
            return
        }

        updateOverlay(overlay, result.data)
    }

    return {
        closed: closePromise,
        opened: openPromise,
    }
}
