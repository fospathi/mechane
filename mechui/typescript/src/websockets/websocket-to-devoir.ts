import { openedWebSockets } from "./opened-websockets"
import { ports } from "../init/init.js"
import * as schema from "../schema/schema.js"
import { type WebSocketLifeCycle, newOnCloseCallback } from "./websocket-event"

// Open a devoir websocket on the Mechane server.
//
// A Mechane server needs to be able to send 'devoir' messages to the Elm
// integrant.
//
// The Elm integrant needs to be able to send 'UI command' messages that may
// alter the devoir state to the Mechane server.
export function openDevoirWebSocket(shutterbug: string): WebSocketLifeCycle {
    const devoirPath = "/devoir"
    const ws = new WebSocket("ws://" + window.location.host + devoirPath)

    const closePromise = new Promise<schema.WebSocketCloseEvent>((resolve) => {
        ws.onclose = newOnCloseCallback(ws, devoirPath, resolve)
    })

    const openPromise = new Promise<void>(function (resolve) {
        ws.onopen = function () {
            ws.send(JSON.stringify(shutterbug))
            openedWebSockets.push(ws)

            // Receive 'UI command' messages from the Elm integrant.
            ports.sendUICommand.subscribe(function (message) {
                const result = schema.uiCommand.safeParse(message)
                if (!result.success) {
                    console.log(result.error.issues)
                }

                // Forward the 'UI command' message to the Mechane server.
                ws.send(JSON.stringify(message))
            })

            resolve()
        }
    })

    // Receive 'devoir' messages from the Mechane server.
    ws.onmessage = function (message) {
        if (typeof message.data !== "string") {
            return
        }
        const data: unknown = JSON.parse(message.data)

        const result = schema.devoir.safeParse(data)
        if (!result.success) {
            console.log(result.error.issues)
        }

        // Forward the 'devoir' message to the Elm integrant.
        //
        // @ts-expect-error: forward good and bad input alike.
        ports.receiveDevoir.send(data)
    }

    return {
        closed: closePromise,
        opened: openPromise,
    }
}
