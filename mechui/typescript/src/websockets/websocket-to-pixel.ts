import { openedWebSockets } from "./opened-websockets"
import { ports } from "../init/init"
import type * as schema from "../schema/schema.js"
import { updateCanvas } from "../custom-elements/shutterbug-pixels/update-canvas"
import { type WebSocketLifeCycle, newOnCloseCallback } from "./websocket-event"

// Open a pixel websocket on the Mechane server.
//
// A Mechane server needs to be able to send 'shutterbug pixels' messages to the
// Typescript integrant.
export function openPixelWebSocket(
    shutterbug: string,
    context: CanvasRenderingContext2D
): WebSocketLifeCycle {
    const pixelPath = "/pixel"
    const ws = new WebSocket("ws://" + window.location.host + pixelPath)
    ws.binaryType = "arraybuffer"

    const closePromise = new Promise<schema.WebSocketCloseEvent>((resolve) => {
        ws.onclose = newOnCloseCallback(ws, pixelPath, resolve)
    })

    const openPromise = new Promise<void>(function (resolve) {
        ws.onopen = function () {
            ws.send(JSON.stringify(shutterbug))
            openedWebSockets.push(ws)
            resolve()
        }
    })

    function pixelsMeta(): schema.PixelsMeta {
        return {
            w: context.canvas.width,
            h: context.canvas.height,
            fullscreen: document.fullscreenElement !== null,
            pointerLock: document.pointerLockElement !== null,
        }
    }

    // Receive 'shutterbug pixels' messages from the Mechane server.
    ws.onmessage = function (message) {
        if (!(message.data instanceof ArrayBuffer)) {
            return
        }

        const w = context.canvas.width
        const h = context.canvas.height
        updateCanvas(context, message.data)
        if (context.canvas.width != w || context.canvas.height != h) {
            // Let the Elm integrant know the new dimensions of the image.
            ports.receivePixelsMeta.send(pixelsMeta())
        }
    }

    document.addEventListener("fullscreenchange", function () {
        // Let the Elm integrant know the fullscreen status.
        ports.receivePixelsMeta.send(pixelsMeta())
    })

    document.addEventListener("pointerlockchange", function () {
        // Let the Elm integrant know the pointerlock status.
        ports.receivePixelsMeta.send(pixelsMeta())
    })

    return {
        closed: closePromise,
        opened: openPromise,
    }
}
