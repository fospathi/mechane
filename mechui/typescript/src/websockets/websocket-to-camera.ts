import { openedWebSockets } from "./opened-websockets"
import { ports } from "../init/init.js"
import * as schema from "../schema/schema.js"
import { type WebSocketLifeCycle, newOnCloseCallback } from "./websocket-event"

// Open a camera websocket on the Mechane server.
//
// A Mechane server needs to be able to send 'registered and associated cameras'
// messages to the Elm integrant.
//
// The Elm integrant needs to be able to send 'camera associations' messages to
// the Mechane server.
export function openCameraWebSocket(shutterbug: string): WebSocketLifeCycle {
    const cameraPath = "/camera"
    const ws = new WebSocket("ws://" + window.location.host + cameraPath)

    const closePromise = new Promise<schema.WebSocketCloseEvent>((resolve) => {
        ws.onclose = newOnCloseCallback(ws, cameraPath, resolve)
    })

    const openPromise = new Promise<void>(function (resolve) {
        ws.onopen = function () {
            ws.send(JSON.stringify(shutterbug))
            openedWebSockets.push(ws)

            // Receive 'camera associations' messages from the Elm integrant.
            ports.sendCameraAssociations.subscribe(function (message) {
                const result = schema.cameraAssociations.safeParse(message)
                if (!result.success) {
                    console.log(result.error.issues)
                }

                // Forward the 'camera associations' message to the Mechane
                // server.
                ws.send(JSON.stringify(message))
            })

            resolve()
        }
    })

    // Receive 'registered and associated cameras' messages from the Mechane
    // server.
    ws.onmessage = function (message) {
        if (typeof message.data !== "string") {
            return
        }
        const data: unknown = JSON.parse(message.data)

        const result = schema.registeredAssociatedCameras.safeParse(data)
        if (!result.success) {
            console.log(result.error.issues)
        }

        // Forward the 'registered and associated cameras' message to the Elm
        // integrant.
        //
        // @ts-expect-error: forward good and bad input alike.
        ports.receiveRegisteredAssociatedCameras.send(data)
    }

    return {
        closed: closePromise,
        opened: openPromise,
    }
}
