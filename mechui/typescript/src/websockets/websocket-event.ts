import type { UIEventListenersController } from "../custom-elements/shutterbug-pixels/listen"
import { removeWebSocket } from "./opened-websockets"
import type * as schema from "../schema/schema"

export interface WebSocketLifeCycle {
    closed: Promise<schema.WebSocketCloseEvent>
    opened: Promise<void | UIEventListenersController>
}

export function newOnCloseCallback(
    ws: WebSocket,
    path: string,
    resolve: (wsCloseEvent: schema.WebSocketCloseEvent) => void
) {
    return function (closeEvent: CloseEvent) {
        removeWebSocket(ws)
        resolve({
            code: closeEvent.code,
            reason: closeEvent.reason,
            sourceWebSocket: path,
            wasClean: closeEvent.wasClean,
        })
    }
}
