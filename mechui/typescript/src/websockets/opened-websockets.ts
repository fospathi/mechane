// Opened websockets are placed in this array. Closed websockets are removed
// from the array.
export const openedWebSockets: Array<WebSocket> = []

// Close any websockets remaining in the array of opened websockets.
export function closeOpenedWebSockets(code: number, reason: string) {
    while (openedWebSockets.length > 0) {
        const ws = openedWebSockets.pop()
        if (ws !== undefined) {
            ws.close(code, reason)
        }
    }
}

// Remove a previously closed websocket from the websockets array.
export function removeWebSocket(webSocket: WebSocket) {
    const i = openedWebSockets.indexOf(webSocket)
    if (i != -1) {
        openedWebSockets.splice(i, 1)
    }
}
