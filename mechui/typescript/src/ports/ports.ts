import type * as schema from "../schema/schema.js"

// Ports are internal channels of communication between the UI's two integrants,
// that is, the Elm language integrant and the Typescript language integrant.
//
// Here port name prefixes are Elm-centric:
// * a 'receive' port is an incoming port to the Elm integrant from the
//   Typescript integrant.
// * a 'send' port is an outgoing port from the Elm integrant to the Typescript
//   integrant.
export interface Ports {
    receiveRegisteredAssociatedCameras: PortToElm<schema.RegisteredAssociatedCameras>
    sendCameraAssociations: PortFromElm<schema.CameraAssociations>

    receiveDevoir: PortToElm<schema.Devoir>
    sendUICommand: PortFromElm<schema.UICommand>

    receiveLifeCycle: PortToElm<schema.LifeCycle>

    receivePixelsMeta: PortToElm<schema.PixelsMeta>

    receiveKaput: PortToElm<schema.WebSocketCloseEvent>

    sendFullscreen: PortFromElm<boolean>

    sendPointerLock: PortFromElm<boolean>

    sendOpenNewTab: PortFromElm<string>

    sendShutterbug: PortFromElm<schema.Shutterbug>

    sendWriteShutterbugToClipboard: PortFromElm<null>

    sendWriteTextToClipboard: PortFromElm<string>
}
