package mechui

import (
	_ "embed"
	"encoding/json"
	"slices"
	"strings"

	"golang.org/x/exp/constraints"
)

var (
	// Contains the dependencies of the UI Elm integrant.
	//
	//go:embed elm/elm.json
	elmIntegrantPackageJSON []byte

	// Contains the dependencies of the UI Typescript integrant.
	//
	//go:embed typescript/package.json
	typescriptIntegrantPackageJSON []byte

	// Contains the dependencies of the Node.js asciidoc converter process.
	//
	//go:embed asciidoc/typescript/package.json
	asciidocPackageJSON []byte
)

// ElmDeps contains the dependencies of the Elm integrant.
//
// Keys are package paths and values are versions.
type ElmDeps struct {
	Dependencies struct {
		Direct   map[string]string
		Indirect map[string]string
	}
}

// TypescriptDeps contains the dependencies of the Typescript integrant.
//
// Keys are package paths and values are versions.
type TypescriptDeps struct {
	Dependencies    map[string]string
	DevDependencies map[string]string
}

// NewUIDeps returns the imported packages of the Elm and Typescript integrants.
func NewUIDeps() (ElmDeps, TypescriptDeps, error) {
	var (
		err error

		e = &ElmDeps{}
		t = &TypescriptDeps{}
		a = &TypescriptDeps{}
	)

	if err = json.Unmarshal(elmIntegrantPackageJSON, e); err != nil {
		return ElmDeps{}, TypescriptDeps{}, err
	}
	if err = json.Unmarshal(typescriptIntegrantPackageJSON, t); err != nil {
		return ElmDeps{}, TypescriptDeps{}, err
	}
	if err = json.Unmarshal(asciidocPackageJSON, a); err != nil {
		return ElmDeps{}, TypescriptDeps{}, err
	}

	allTS := TypescriptDeps{
		Dependencies: joinMapValues(
			mergeMaps(t.Dependencies, a.Dependencies),
			func(vers []string) string { return strings.Join(vers, " / ") },
		),
		DevDependencies: joinMapValues(
			mergeMaps(t.DevDependencies, a.DevDependencies),
			func(vers []string) string { return strings.Join(vers, " / ") },
		),
	}

	return *e, allTS, nil
}

func mergeMaps[K comparable, T constraints.Ordered](vm ...map[K]T) map[K][]T {
	res := map[K][]T{}
	for _, m := range vm {
		for k, v := range m {
			if rv, ok := res[k]; !ok {
				res[k] = []T{v}
			} else {
				res[k] = append(rv, v)
			}
		}
	}
	for _, v := range res {
		slices.Sort(v)
	}
	return res
}

func joinMapValues[K comparable, T any](m map[K][]T, join func([]T) T) map[K]T {
	res := map[K]T{}
	for k, v := range m {
		res[k] = join(v)
	}
	return res
}
