/*
Package mechui provides the HTML, JS, and other such files needed by a Mechane
server to display the browser based UI.
*/
package mechui

import (
	"embed"
	"io/fs"
	"net/http"
)

const webPath = "web"

// webFS is a file system containing the files which implement Mechane's
// browser based UI.
//
//go:embed web
var webFS embed.FS

// Root file system of a Mechane server with the web directory as the root
// directory. Can be supplied to a Mechane server as the root file system.
func Root() (http.FileSystem, error) {
	fs, err := fs.Sub(webFS, webPath)
	return http.FS(fs), err
}
