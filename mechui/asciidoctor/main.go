//go:generate go run .

// Download the default fonts used by Asciidoctor and create a modified
// stylesheet with the fonts' local URLs.
//
// Changes the working directory to this file's directory.
package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/universal/mech"
)

// The source CSS stylesheet which contains the font-face rules for the default
// Asciidoctor fonts.
//
// Learn more at:
// https://docs.asciidoctor.org/asciidoctor/latest/html-backend/default-stylesheet/
const cssURL = "https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic%7CNoto+Serif:400,400italic,700,700italic%7CDroid+Sans+Mono:400,700"

// Induce the font API to produce woff2 fonts by pretending to be the Chrome
// browser on linux.
const chromeLinux = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36"

// Expected number of unique font URLs. Note that each URL may occur multiple
// times.
const uniqueURLs = 45

const (
	// The path of the asciidoctor font directory relative to the server's root
	// directory.
	localPath = "font/asciidoctor/"

	// The path of the asciidoctor font directory relative to this file's
	// directory.
	fontDir = "../web/font/asciidoctor/"

	// The path of the server's root directory relative to this file's
	// directory.
	serverRootDir = "../web/"

	// The filename for the modified version of the CSS stylesheet with the
	// font-face rules modified to use local URLs pointing to the downloaded
	// fonts.
	cssFilename = "asciidoctor-default-font.css"
)

func main() {

	// Get the CSS style sheet for the fonts which contains among other things
	// the URL of each font. Note that the Google font API tailors its output to
	// a user agent's value.
	var css string
	{
		vb, err := func() ([]byte, error) {
			client := http.Client{Timeout: 5 * time.Second}

			req, err := http.NewRequest("GET", cssURL, nil)
			if err != nil {
				return nil, err
			}

			req.Header.Add("User-Agent", chromeLinux)
			resp, err := client.Do(req)
			if err != nil {
				return nil, err
			}
			defer resp.Body.Close()

			vb, err := io.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}

			return vb, nil
		}()
		if err != nil {
			log.Fatalln(err)
		}
		css = string(vb)
	}

	// Collect the font URLs by exploiting the url(...) syntax as the base of
	// the regular expression.
	urlSet := eqset.New[string]() // URLs in url(...) format.
	{
		if re, err := regexp.Compile(`url\(.+?\)`); err != nil {
			log.Fatalln(err)
		} else if vu := re.FindAllString(string(css), -1); len(vu) == 0 {
			log.Fatalln(errors.New("no URLs found"))
		} else {
			urlSet.AddAll(vu...)
		}
	}

	// Assert that the number of unique URLs is the same as previous runs.
	if urlSet.Len() != uniqueURLs {
		log.Fatalln(fmt.Errorf("expected %v unique font URLs: got %v",
			uniqueURLs, urlSet.Len()))
	}

	// Download each font.
	fonts := map[string][]byte{} // Keys are URLs in url(...) format.
	for u := range urlSet {
		fnt, err := func(u string) ([]byte, error) {
			addr := strings.TrimSuffix(strings.TrimPrefix(u, "url("), ")")
			resp, err := http.Get(addr)
			if err != nil {
				return nil, err
			}
			defer resp.Body.Close()
			fnt, err := io.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}
			return fnt, nil
		}(string(u))
		if err != nil {
			log.Fatalln(err)
		}
		fonts[u] = fnt
		time.Sleep(50 * time.Millisecond)
	}

	// Make sure the relative path to the font directory is valid.
	mech.ChdirToHere()
	if _, err := os.Stat(fontDir); err != nil {
		log.Fatalln(err)
	}

	// Make the filename of a font on the local disk from the segments of its
	// URL.
	// url(../family/version/random.woff2)
	var extractName func(string) string
	{
		matchName, err :=
			regexp.Compile(`[^/][[:alnum:]]+/v\d+/[[:alnum:]-_]+\.woff2`)
		if err != nil {
			log.Fatalln(err)
		}
		extractName = func(u string) string {
			name := matchName.FindString(u)
			if name == "" {
				log.Fatalln(fmt.Errorf("failed to match font name: %v", u))
			}
			name = strings.Replace(name, "/v", "V", 1)
			name = strings.Replace(name, "/", "_", 1)
			return name
		}
	}

	// Populate the CSS style sheet with the local URLs and write it to disk.
	{
		for u := range urlSet {
			lu := fmt.Sprintf("url(%v%v)", localPath, extractName(u))
			css = strings.ReplaceAll(css, u, lu)
		}

		if f, err :=
			os.Create(filepath.Join(serverRootDir, cssFilename)); err != nil {
			log.Fatalln(err)
		} else if _, err = f.WriteString(css); err != nil {
			log.Fatalln(err)
		} else if err = f.Close(); err != nil {
			log.Fatalln(err)
		}
	}

	// Write the fonts to disk.
	{
		for u, vb := range fonts {
			name := extractName(u)
			if f, err :=
				os.Create(filepath.Join(fontDir, name)); err != nil {
				log.Fatalln(err)
			} else if _, err = f.Write(vb); err != nil {
				log.Fatalln(err)
			} else if err = f.Close(); err != nil {
				log.Fatalln(err)
			}
		}
	}
}
