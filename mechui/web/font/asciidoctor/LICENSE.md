# Default Asciidoctor fonts

This directory contains the default fonts required when displaying the HTML output of the [Asciidoctor.js](https://github.com/asciidoctor/asciidoctor.js/) AsciiDoc converter.

## License
The fonts are open source fonts that were obtained from the [Google Fonts](https://fonts.google.com/) initiative. 

Some of these fonts are deprecated and will not appear to be listed there anymore but they are still available for download from the Google Fonts API.

The fonts are released under different open source licenses:

- ### Droid Sans Mono

  [Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0)

- ### Open Sans
  
  [SIL Open Font License (OFL) ](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)

- ### Noto Serif

  [SIL Open Font License (OFL) ](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)
