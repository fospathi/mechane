(function(scope){
'use strict';

function F(arity, fun, wrapper) {
  wrapper.a = arity;
  wrapper.f = fun;
  return wrapper;
}

function F2(fun) {
  return F(2, fun, function(a) { return function(b) { return fun(a,b); }; })
}
function F3(fun) {
  return F(3, fun, function(a) {
    return function(b) { return function(c) { return fun(a, b, c); }; };
  });
}
function F4(fun) {
  return F(4, fun, function(a) { return function(b) { return function(c) {
    return function(d) { return fun(a, b, c, d); }; }; };
  });
}
function F5(fun) {
  return F(5, fun, function(a) { return function(b) { return function(c) {
    return function(d) { return function(e) { return fun(a, b, c, d, e); }; }; }; };
  });
}
function F6(fun) {
  return F(6, fun, function(a) { return function(b) { return function(c) {
    return function(d) { return function(e) { return function(f) {
    return fun(a, b, c, d, e, f); }; }; }; }; };
  });
}
function F7(fun) {
  return F(7, fun, function(a) { return function(b) { return function(c) {
    return function(d) { return function(e) { return function(f) {
    return function(g) { return fun(a, b, c, d, e, f, g); }; }; }; }; }; };
  });
}
function F8(fun) {
  return F(8, fun, function(a) { return function(b) { return function(c) {
    return function(d) { return function(e) { return function(f) {
    return function(g) { return function(h) {
    return fun(a, b, c, d, e, f, g, h); }; }; }; }; }; }; };
  });
}
function F9(fun) {
  return F(9, fun, function(a) { return function(b) { return function(c) {
    return function(d) { return function(e) { return function(f) {
    return function(g) { return function(h) { return function(i) {
    return fun(a, b, c, d, e, f, g, h, i); }; }; }; }; }; }; }; };
  });
}

function A2(fun, a, b) {
  return fun.a === 2 ? fun.f(a, b) : fun(a)(b);
}
function A3(fun, a, b, c) {
  return fun.a === 3 ? fun.f(a, b, c) : fun(a)(b)(c);
}
function A4(fun, a, b, c, d) {
  return fun.a === 4 ? fun.f(a, b, c, d) : fun(a)(b)(c)(d);
}
function A5(fun, a, b, c, d, e) {
  return fun.a === 5 ? fun.f(a, b, c, d, e) : fun(a)(b)(c)(d)(e);
}
function A6(fun, a, b, c, d, e, f) {
  return fun.a === 6 ? fun.f(a, b, c, d, e, f) : fun(a)(b)(c)(d)(e)(f);
}
function A7(fun, a, b, c, d, e, f, g) {
  return fun.a === 7 ? fun.f(a, b, c, d, e, f, g) : fun(a)(b)(c)(d)(e)(f)(g);
}
function A8(fun, a, b, c, d, e, f, g, h) {
  return fun.a === 8 ? fun.f(a, b, c, d, e, f, g, h) : fun(a)(b)(c)(d)(e)(f)(g)(h);
}
function A9(fun, a, b, c, d, e, f, g, h, i) {
  return fun.a === 9 ? fun.f(a, b, c, d, e, f, g, h, i) : fun(a)(b)(c)(d)(e)(f)(g)(h)(i);
}

console.warn('Compiled in DEV mode. Follow the advice at https://elm-lang.org/0.19.1/optimize for better performance and smaller assets.');


// EQUALITY

function _Utils_eq(x, y)
{
	for (
		var pair, stack = [], isEqual = _Utils_eqHelp(x, y, 0, stack);
		isEqual && (pair = stack.pop());
		isEqual = _Utils_eqHelp(pair.a, pair.b, 0, stack)
		)
	{}

	return isEqual;
}

function _Utils_eqHelp(x, y, depth, stack)
{
	if (x === y)
	{
		return true;
	}

	if (typeof x !== 'object' || x === null || y === null)
	{
		typeof x === 'function' && _Debug_crash(5);
		return false;
	}

	if (depth > 100)
	{
		stack.push(_Utils_Tuple2(x,y));
		return true;
	}

	/**/
	if (x.$ === 'Set_elm_builtin')
	{
		x = $elm$core$Set$toList(x);
		y = $elm$core$Set$toList(y);
	}
	if (x.$ === 'RBNode_elm_builtin' || x.$ === 'RBEmpty_elm_builtin')
	{
		x = $elm$core$Dict$toList(x);
		y = $elm$core$Dict$toList(y);
	}
	//*/

	/**_UNUSED/
	if (x.$ < 0)
	{
		x = $elm$core$Dict$toList(x);
		y = $elm$core$Dict$toList(y);
	}
	//*/

	for (var key in x)
	{
		if (!_Utils_eqHelp(x[key], y[key], depth + 1, stack))
		{
			return false;
		}
	}
	return true;
}

var _Utils_equal = F2(_Utils_eq);
var _Utils_notEqual = F2(function(a, b) { return !_Utils_eq(a,b); });



// COMPARISONS

// Code in Generate/JavaScript.hs, Basics.js, and List.js depends on
// the particular integer values assigned to LT, EQ, and GT.

function _Utils_cmp(x, y, ord)
{
	if (typeof x !== 'object')
	{
		return x === y ? /*EQ*/ 0 : x < y ? /*LT*/ -1 : /*GT*/ 1;
	}

	/**/
	if (x instanceof String)
	{
		var a = x.valueOf();
		var b = y.valueOf();
		return a === b ? 0 : a < b ? -1 : 1;
	}
	//*/

	/**_UNUSED/
	if (typeof x.$ === 'undefined')
	//*/
	/**/
	if (x.$[0] === '#')
	//*/
	{
		return (ord = _Utils_cmp(x.a, y.a))
			? ord
			: (ord = _Utils_cmp(x.b, y.b))
				? ord
				: _Utils_cmp(x.c, y.c);
	}

	// traverse conses until end of a list or a mismatch
	for (; x.b && y.b && !(ord = _Utils_cmp(x.a, y.a)); x = x.b, y = y.b) {} // WHILE_CONSES
	return ord || (x.b ? /*GT*/ 1 : y.b ? /*LT*/ -1 : /*EQ*/ 0);
}

var _Utils_lt = F2(function(a, b) { return _Utils_cmp(a, b) < 0; });
var _Utils_le = F2(function(a, b) { return _Utils_cmp(a, b) < 1; });
var _Utils_gt = F2(function(a, b) { return _Utils_cmp(a, b) > 0; });
var _Utils_ge = F2(function(a, b) { return _Utils_cmp(a, b) >= 0; });

var _Utils_compare = F2(function(x, y)
{
	var n = _Utils_cmp(x, y);
	return n < 0 ? $elm$core$Basics$LT : n ? $elm$core$Basics$GT : $elm$core$Basics$EQ;
});


// COMMON VALUES

var _Utils_Tuple0_UNUSED = 0;
var _Utils_Tuple0 = { $: '#0' };

function _Utils_Tuple2_UNUSED(a, b) { return { a: a, b: b }; }
function _Utils_Tuple2(a, b) { return { $: '#2', a: a, b: b }; }

function _Utils_Tuple3_UNUSED(a, b, c) { return { a: a, b: b, c: c }; }
function _Utils_Tuple3(a, b, c) { return { $: '#3', a: a, b: b, c: c }; }

function _Utils_chr_UNUSED(c) { return c; }
function _Utils_chr(c) { return new String(c); }


// RECORDS

function _Utils_update(oldRecord, updatedFields)
{
	var newRecord = {};

	for (var key in oldRecord)
	{
		newRecord[key] = oldRecord[key];
	}

	for (var key in updatedFields)
	{
		newRecord[key] = updatedFields[key];
	}

	return newRecord;
}


// APPEND

var _Utils_append = F2(_Utils_ap);

function _Utils_ap(xs, ys)
{
	// append Strings
	if (typeof xs === 'string')
	{
		return xs + ys;
	}

	// append Lists
	if (!xs.b)
	{
		return ys;
	}
	var root = _List_Cons(xs.a, ys);
	xs = xs.b
	for (var curr = root; xs.b; xs = xs.b) // WHILE_CONS
	{
		curr = curr.b = _List_Cons(xs.a, ys);
	}
	return root;
}



var _List_Nil_UNUSED = { $: 0 };
var _List_Nil = { $: '[]' };

function _List_Cons_UNUSED(hd, tl) { return { $: 1, a: hd, b: tl }; }
function _List_Cons(hd, tl) { return { $: '::', a: hd, b: tl }; }


var _List_cons = F2(_List_Cons);

function _List_fromArray(arr)
{
	var out = _List_Nil;
	for (var i = arr.length; i--; )
	{
		out = _List_Cons(arr[i], out);
	}
	return out;
}

function _List_toArray(xs)
{
	for (var out = []; xs.b; xs = xs.b) // WHILE_CONS
	{
		out.push(xs.a);
	}
	return out;
}

var _List_map2 = F3(function(f, xs, ys)
{
	for (var arr = []; xs.b && ys.b; xs = xs.b, ys = ys.b) // WHILE_CONSES
	{
		arr.push(A2(f, xs.a, ys.a));
	}
	return _List_fromArray(arr);
});

var _List_map3 = F4(function(f, xs, ys, zs)
{
	for (var arr = []; xs.b && ys.b && zs.b; xs = xs.b, ys = ys.b, zs = zs.b) // WHILE_CONSES
	{
		arr.push(A3(f, xs.a, ys.a, zs.a));
	}
	return _List_fromArray(arr);
});

var _List_map4 = F5(function(f, ws, xs, ys, zs)
{
	for (var arr = []; ws.b && xs.b && ys.b && zs.b; ws = ws.b, xs = xs.b, ys = ys.b, zs = zs.b) // WHILE_CONSES
	{
		arr.push(A4(f, ws.a, xs.a, ys.a, zs.a));
	}
	return _List_fromArray(arr);
});

var _List_map5 = F6(function(f, vs, ws, xs, ys, zs)
{
	for (var arr = []; vs.b && ws.b && xs.b && ys.b && zs.b; vs = vs.b, ws = ws.b, xs = xs.b, ys = ys.b, zs = zs.b) // WHILE_CONSES
	{
		arr.push(A5(f, vs.a, ws.a, xs.a, ys.a, zs.a));
	}
	return _List_fromArray(arr);
});

var _List_sortBy = F2(function(f, xs)
{
	return _List_fromArray(_List_toArray(xs).sort(function(a, b) {
		return _Utils_cmp(f(a), f(b));
	}));
});

var _List_sortWith = F2(function(f, xs)
{
	return _List_fromArray(_List_toArray(xs).sort(function(a, b) {
		var ord = A2(f, a, b);
		return ord === $elm$core$Basics$EQ ? 0 : ord === $elm$core$Basics$LT ? -1 : 1;
	}));
});



var _JsArray_empty = [];

function _JsArray_singleton(value)
{
    return [value];
}

function _JsArray_length(array)
{
    return array.length;
}

var _JsArray_initialize = F3(function(size, offset, func)
{
    var result = new Array(size);

    for (var i = 0; i < size; i++)
    {
        result[i] = func(offset + i);
    }

    return result;
});

var _JsArray_initializeFromList = F2(function (max, ls)
{
    var result = new Array(max);

    for (var i = 0; i < max && ls.b; i++)
    {
        result[i] = ls.a;
        ls = ls.b;
    }

    result.length = i;
    return _Utils_Tuple2(result, ls);
});

var _JsArray_unsafeGet = F2(function(index, array)
{
    return array[index];
});

var _JsArray_unsafeSet = F3(function(index, value, array)
{
    var length = array.length;
    var result = new Array(length);

    for (var i = 0; i < length; i++)
    {
        result[i] = array[i];
    }

    result[index] = value;
    return result;
});

var _JsArray_push = F2(function(value, array)
{
    var length = array.length;
    var result = new Array(length + 1);

    for (var i = 0; i < length; i++)
    {
        result[i] = array[i];
    }

    result[length] = value;
    return result;
});

var _JsArray_foldl = F3(function(func, acc, array)
{
    var length = array.length;

    for (var i = 0; i < length; i++)
    {
        acc = A2(func, array[i], acc);
    }

    return acc;
});

var _JsArray_foldr = F3(function(func, acc, array)
{
    for (var i = array.length - 1; i >= 0; i--)
    {
        acc = A2(func, array[i], acc);
    }

    return acc;
});

var _JsArray_map = F2(function(func, array)
{
    var length = array.length;
    var result = new Array(length);

    for (var i = 0; i < length; i++)
    {
        result[i] = func(array[i]);
    }

    return result;
});

var _JsArray_indexedMap = F3(function(func, offset, array)
{
    var length = array.length;
    var result = new Array(length);

    for (var i = 0; i < length; i++)
    {
        result[i] = A2(func, offset + i, array[i]);
    }

    return result;
});

var _JsArray_slice = F3(function(from, to, array)
{
    return array.slice(from, to);
});

var _JsArray_appendN = F3(function(n, dest, source)
{
    var destLen = dest.length;
    var itemsToCopy = n - destLen;

    if (itemsToCopy > source.length)
    {
        itemsToCopy = source.length;
    }

    var size = destLen + itemsToCopy;
    var result = new Array(size);

    for (var i = 0; i < destLen; i++)
    {
        result[i] = dest[i];
    }

    for (var i = 0; i < itemsToCopy; i++)
    {
        result[i + destLen] = source[i];
    }

    return result;
});



// LOG

var _Debug_log_UNUSED = F2(function(tag, value)
{
	return value;
});

var _Debug_log = F2(function(tag, value)
{
	console.log(tag + ': ' + _Debug_toString(value));
	return value;
});


// TODOS

function _Debug_todo(moduleName, region)
{
	return function(message) {
		_Debug_crash(8, moduleName, region, message);
	};
}

function _Debug_todoCase(moduleName, region, value)
{
	return function(message) {
		_Debug_crash(9, moduleName, region, value, message);
	};
}


// TO STRING

function _Debug_toString_UNUSED(value)
{
	return '<internals>';
}

function _Debug_toString(value)
{
	return _Debug_toAnsiString(false, value);
}

function _Debug_toAnsiString(ansi, value)
{
	if (typeof value === 'function')
	{
		return _Debug_internalColor(ansi, '<function>');
	}

	if (typeof value === 'boolean')
	{
		return _Debug_ctorColor(ansi, value ? 'True' : 'False');
	}

	if (typeof value === 'number')
	{
		return _Debug_numberColor(ansi, value + '');
	}

	if (value instanceof String)
	{
		return _Debug_charColor(ansi, "'" + _Debug_addSlashes(value, true) + "'");
	}

	if (typeof value === 'string')
	{
		return _Debug_stringColor(ansi, '"' + _Debug_addSlashes(value, false) + '"');
	}

	if (typeof value === 'object' && '$' in value)
	{
		var tag = value.$;

		if (typeof tag === 'number')
		{
			return _Debug_internalColor(ansi, '<internals>');
		}

		if (tag[0] === '#')
		{
			var output = [];
			for (var k in value)
			{
				if (k === '$') continue;
				output.push(_Debug_toAnsiString(ansi, value[k]));
			}
			return '(' + output.join(',') + ')';
		}

		if (tag === 'Set_elm_builtin')
		{
			return _Debug_ctorColor(ansi, 'Set')
				+ _Debug_fadeColor(ansi, '.fromList') + ' '
				+ _Debug_toAnsiString(ansi, $elm$core$Set$toList(value));
		}

		if (tag === 'RBNode_elm_builtin' || tag === 'RBEmpty_elm_builtin')
		{
			return _Debug_ctorColor(ansi, 'Dict')
				+ _Debug_fadeColor(ansi, '.fromList') + ' '
				+ _Debug_toAnsiString(ansi, $elm$core$Dict$toList(value));
		}

		if (tag === 'Array_elm_builtin')
		{
			return _Debug_ctorColor(ansi, 'Array')
				+ _Debug_fadeColor(ansi, '.fromList') + ' '
				+ _Debug_toAnsiString(ansi, $elm$core$Array$toList(value));
		}

		if (tag === '::' || tag === '[]')
		{
			var output = '[';

			value.b && (output += _Debug_toAnsiString(ansi, value.a), value = value.b)

			for (; value.b; value = value.b) // WHILE_CONS
			{
				output += ',' + _Debug_toAnsiString(ansi, value.a);
			}
			return output + ']';
		}

		var output = '';
		for (var i in value)
		{
			if (i === '$') continue;
			var str = _Debug_toAnsiString(ansi, value[i]);
			var c0 = str[0];
			var parenless = c0 === '{' || c0 === '(' || c0 === '[' || c0 === '<' || c0 === '"' || str.indexOf(' ') < 0;
			output += ' ' + (parenless ? str : '(' + str + ')');
		}
		return _Debug_ctorColor(ansi, tag) + output;
	}

	if (typeof DataView === 'function' && value instanceof DataView)
	{
		return _Debug_stringColor(ansi, '<' + value.byteLength + ' bytes>');
	}

	if (typeof File !== 'undefined' && value instanceof File)
	{
		return _Debug_internalColor(ansi, '<' + value.name + '>');
	}

	if (typeof value === 'object')
	{
		var output = [];
		for (var key in value)
		{
			var field = key[0] === '_' ? key.slice(1) : key;
			output.push(_Debug_fadeColor(ansi, field) + ' = ' + _Debug_toAnsiString(ansi, value[key]));
		}
		if (output.length === 0)
		{
			return '{}';
		}
		return '{ ' + output.join(', ') + ' }';
	}

	return _Debug_internalColor(ansi, '<internals>');
}

function _Debug_addSlashes(str, isChar)
{
	var s = str
		.replace(/\\/g, '\\\\')
		.replace(/\n/g, '\\n')
		.replace(/\t/g, '\\t')
		.replace(/\r/g, '\\r')
		.replace(/\v/g, '\\v')
		.replace(/\0/g, '\\0');

	if (isChar)
	{
		return s.replace(/\'/g, '\\\'');
	}
	else
	{
		return s.replace(/\"/g, '\\"');
	}
}

function _Debug_ctorColor(ansi, string)
{
	return ansi ? '\x1b[96m' + string + '\x1b[0m' : string;
}

function _Debug_numberColor(ansi, string)
{
	return ansi ? '\x1b[95m' + string + '\x1b[0m' : string;
}

function _Debug_stringColor(ansi, string)
{
	return ansi ? '\x1b[93m' + string + '\x1b[0m' : string;
}

function _Debug_charColor(ansi, string)
{
	return ansi ? '\x1b[92m' + string + '\x1b[0m' : string;
}

function _Debug_fadeColor(ansi, string)
{
	return ansi ? '\x1b[37m' + string + '\x1b[0m' : string;
}

function _Debug_internalColor(ansi, string)
{
	return ansi ? '\x1b[36m' + string + '\x1b[0m' : string;
}

function _Debug_toHexDigit(n)
{
	return String.fromCharCode(n < 10 ? 48 + n : 55 + n);
}


// CRASH


function _Debug_crash_UNUSED(identifier)
{
	throw new Error('https://github.com/elm/core/blob/1.0.0/hints/' + identifier + '.md');
}


function _Debug_crash(identifier, fact1, fact2, fact3, fact4)
{
	switch(identifier)
	{
		case 0:
			throw new Error('What node should I take over? In JavaScript I need something like:\n\n    Elm.Main.init({\n        node: document.getElementById("elm-node")\n    })\n\nYou need to do this with any Browser.sandbox or Browser.element program.');

		case 1:
			throw new Error('Browser.application programs cannot handle URLs like this:\n\n    ' + document.location.href + '\n\nWhat is the root? The root of your file system? Try looking at this program with `elm reactor` or some other server.');

		case 2:
			var jsonErrorString = fact1;
			throw new Error('Problem with the flags given to your Elm program on initialization.\n\n' + jsonErrorString);

		case 3:
			var portName = fact1;
			throw new Error('There can only be one port named `' + portName + '`, but your program has multiple.');

		case 4:
			var portName = fact1;
			var problem = fact2;
			throw new Error('Trying to send an unexpected type of value through port `' + portName + '`:\n' + problem);

		case 5:
			throw new Error('Trying to use `(==)` on functions.\nThere is no way to know if functions are "the same" in the Elm sense.\nRead more about this at https://package.elm-lang.org/packages/elm/core/latest/Basics#== which describes why it is this way and what the better version will look like.');

		case 6:
			var moduleName = fact1;
			throw new Error('Your page is loading multiple Elm scripts with a module named ' + moduleName + '. Maybe a duplicate script is getting loaded accidentally? If not, rename one of them so I know which is which!');

		case 8:
			var moduleName = fact1;
			var region = fact2;
			var message = fact3;
			throw new Error('TODO in module `' + moduleName + '` ' + _Debug_regionToString(region) + '\n\n' + message);

		case 9:
			var moduleName = fact1;
			var region = fact2;
			var value = fact3;
			var message = fact4;
			throw new Error(
				'TODO in module `' + moduleName + '` from the `case` expression '
				+ _Debug_regionToString(region) + '\n\nIt received the following value:\n\n    '
				+ _Debug_toString(value).replace('\n', '\n    ')
				+ '\n\nBut the branch that handles it says:\n\n    ' + message.replace('\n', '\n    ')
			);

		case 10:
			throw new Error('Bug in https://github.com/elm/virtual-dom/issues');

		case 11:
			throw new Error('Cannot perform mod 0. Division by zero error.');
	}
}

function _Debug_regionToString(region)
{
	if (region.start.line === region.end.line)
	{
		return 'on line ' + region.start.line;
	}
	return 'on lines ' + region.start.line + ' through ' + region.end.line;
}



// MATH

var _Basics_add = F2(function(a, b) { return a + b; });
var _Basics_sub = F2(function(a, b) { return a - b; });
var _Basics_mul = F2(function(a, b) { return a * b; });
var _Basics_fdiv = F2(function(a, b) { return a / b; });
var _Basics_idiv = F2(function(a, b) { return (a / b) | 0; });
var _Basics_pow = F2(Math.pow);

var _Basics_remainderBy = F2(function(b, a) { return a % b; });

// https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/divmodnote-letter.pdf
var _Basics_modBy = F2(function(modulus, x)
{
	var answer = x % modulus;
	return modulus === 0
		? _Debug_crash(11)
		:
	((answer > 0 && modulus < 0) || (answer < 0 && modulus > 0))
		? answer + modulus
		: answer;
});


// TRIGONOMETRY

var _Basics_pi = Math.PI;
var _Basics_e = Math.E;
var _Basics_cos = Math.cos;
var _Basics_sin = Math.sin;
var _Basics_tan = Math.tan;
var _Basics_acos = Math.acos;
var _Basics_asin = Math.asin;
var _Basics_atan = Math.atan;
var _Basics_atan2 = F2(Math.atan2);


// MORE MATH

function _Basics_toFloat(x) { return x; }
function _Basics_truncate(n) { return n | 0; }
function _Basics_isInfinite(n) { return n === Infinity || n === -Infinity; }

var _Basics_ceiling = Math.ceil;
var _Basics_floor = Math.floor;
var _Basics_round = Math.round;
var _Basics_sqrt = Math.sqrt;
var _Basics_log = Math.log;
var _Basics_isNaN = isNaN;


// BOOLEANS

function _Basics_not(bool) { return !bool; }
var _Basics_and = F2(function(a, b) { return a && b; });
var _Basics_or  = F2(function(a, b) { return a || b; });
var _Basics_xor = F2(function(a, b) { return a !== b; });



var _String_cons = F2(function(chr, str)
{
	return chr + str;
});

function _String_uncons(string)
{
	var word = string.charCodeAt(0);
	return !isNaN(word)
		? $elm$core$Maybe$Just(
			0xD800 <= word && word <= 0xDBFF
				? _Utils_Tuple2(_Utils_chr(string[0] + string[1]), string.slice(2))
				: _Utils_Tuple2(_Utils_chr(string[0]), string.slice(1))
		)
		: $elm$core$Maybe$Nothing;
}

var _String_append = F2(function(a, b)
{
	return a + b;
});

function _String_length(str)
{
	return str.length;
}

var _String_map = F2(function(func, string)
{
	var len = string.length;
	var array = new Array(len);
	var i = 0;
	while (i < len)
	{
		var word = string.charCodeAt(i);
		if (0xD800 <= word && word <= 0xDBFF)
		{
			array[i] = func(_Utils_chr(string[i] + string[i+1]));
			i += 2;
			continue;
		}
		array[i] = func(_Utils_chr(string[i]));
		i++;
	}
	return array.join('');
});

var _String_filter = F2(function(isGood, str)
{
	var arr = [];
	var len = str.length;
	var i = 0;
	while (i < len)
	{
		var char = str[i];
		var word = str.charCodeAt(i);
		i++;
		if (0xD800 <= word && word <= 0xDBFF)
		{
			char += str[i];
			i++;
		}

		if (isGood(_Utils_chr(char)))
		{
			arr.push(char);
		}
	}
	return arr.join('');
});

function _String_reverse(str)
{
	var len = str.length;
	var arr = new Array(len);
	var i = 0;
	while (i < len)
	{
		var word = str.charCodeAt(i);
		if (0xD800 <= word && word <= 0xDBFF)
		{
			arr[len - i] = str[i + 1];
			i++;
			arr[len - i] = str[i - 1];
			i++;
		}
		else
		{
			arr[len - i] = str[i];
			i++;
		}
	}
	return arr.join('');
}

var _String_foldl = F3(function(func, state, string)
{
	var len = string.length;
	var i = 0;
	while (i < len)
	{
		var char = string[i];
		var word = string.charCodeAt(i);
		i++;
		if (0xD800 <= word && word <= 0xDBFF)
		{
			char += string[i];
			i++;
		}
		state = A2(func, _Utils_chr(char), state);
	}
	return state;
});

var _String_foldr = F3(function(func, state, string)
{
	var i = string.length;
	while (i--)
	{
		var char = string[i];
		var word = string.charCodeAt(i);
		if (0xDC00 <= word && word <= 0xDFFF)
		{
			i--;
			char = string[i] + char;
		}
		state = A2(func, _Utils_chr(char), state);
	}
	return state;
});

var _String_split = F2(function(sep, str)
{
	return str.split(sep);
});

var _String_join = F2(function(sep, strs)
{
	return strs.join(sep);
});

var _String_slice = F3(function(start, end, str) {
	return str.slice(start, end);
});

function _String_trim(str)
{
	return str.trim();
}

function _String_trimLeft(str)
{
	return str.replace(/^\s+/, '');
}

function _String_trimRight(str)
{
	return str.replace(/\s+$/, '');
}

function _String_words(str)
{
	return _List_fromArray(str.trim().split(/\s+/g));
}

function _String_lines(str)
{
	return _List_fromArray(str.split(/\r\n|\r|\n/g));
}

function _String_toUpper(str)
{
	return str.toUpperCase();
}

function _String_toLower(str)
{
	return str.toLowerCase();
}

var _String_any = F2(function(isGood, string)
{
	var i = string.length;
	while (i--)
	{
		var char = string[i];
		var word = string.charCodeAt(i);
		if (0xDC00 <= word && word <= 0xDFFF)
		{
			i--;
			char = string[i] + char;
		}
		if (isGood(_Utils_chr(char)))
		{
			return true;
		}
	}
	return false;
});

var _String_all = F2(function(isGood, string)
{
	var i = string.length;
	while (i--)
	{
		var char = string[i];
		var word = string.charCodeAt(i);
		if (0xDC00 <= word && word <= 0xDFFF)
		{
			i--;
			char = string[i] + char;
		}
		if (!isGood(_Utils_chr(char)))
		{
			return false;
		}
	}
	return true;
});

var _String_contains = F2(function(sub, str)
{
	return str.indexOf(sub) > -1;
});

var _String_startsWith = F2(function(sub, str)
{
	return str.indexOf(sub) === 0;
});

var _String_endsWith = F2(function(sub, str)
{
	return str.length >= sub.length &&
		str.lastIndexOf(sub) === str.length - sub.length;
});

var _String_indexes = F2(function(sub, str)
{
	var subLen = sub.length;

	if (subLen < 1)
	{
		return _List_Nil;
	}

	var i = 0;
	var is = [];

	while ((i = str.indexOf(sub, i)) > -1)
	{
		is.push(i);
		i = i + subLen;
	}

	return _List_fromArray(is);
});


// TO STRING

function _String_fromNumber(number)
{
	return number + '';
}


// INT CONVERSIONS

function _String_toInt(str)
{
	var total = 0;
	var code0 = str.charCodeAt(0);
	var start = code0 == 0x2B /* + */ || code0 == 0x2D /* - */ ? 1 : 0;

	for (var i = start; i < str.length; ++i)
	{
		var code = str.charCodeAt(i);
		if (code < 0x30 || 0x39 < code)
		{
			return $elm$core$Maybe$Nothing;
		}
		total = 10 * total + code - 0x30;
	}

	return i == start
		? $elm$core$Maybe$Nothing
		: $elm$core$Maybe$Just(code0 == 0x2D ? -total : total);
}


// FLOAT CONVERSIONS

function _String_toFloat(s)
{
	// check if it is a hex, octal, or binary number
	if (s.length === 0 || /[\sxbo]/.test(s))
	{
		return $elm$core$Maybe$Nothing;
	}
	var n = +s;
	// faster isNaN check
	return n === n ? $elm$core$Maybe$Just(n) : $elm$core$Maybe$Nothing;
}

function _String_fromList(chars)
{
	return _List_toArray(chars).join('');
}




function _Char_toCode(char)
{
	var code = char.charCodeAt(0);
	if (0xD800 <= code && code <= 0xDBFF)
	{
		return (code - 0xD800) * 0x400 + char.charCodeAt(1) - 0xDC00 + 0x10000
	}
	return code;
}

function _Char_fromCode(code)
{
	return _Utils_chr(
		(code < 0 || 0x10FFFF < code)
			? '\uFFFD'
			:
		(code <= 0xFFFF)
			? String.fromCharCode(code)
			:
		(code -= 0x10000,
			String.fromCharCode(Math.floor(code / 0x400) + 0xD800, code % 0x400 + 0xDC00)
		)
	);
}

function _Char_toUpper(char)
{
	return _Utils_chr(char.toUpperCase());
}

function _Char_toLower(char)
{
	return _Utils_chr(char.toLowerCase());
}

function _Char_toLocaleUpper(char)
{
	return _Utils_chr(char.toLocaleUpperCase());
}

function _Char_toLocaleLower(char)
{
	return _Utils_chr(char.toLocaleLowerCase());
}



/**/
function _Json_errorToString(error)
{
	return $elm$json$Json$Decode$errorToString(error);
}
//*/


// CORE DECODERS

function _Json_succeed(msg)
{
	return {
		$: 0,
		a: msg
	};
}

function _Json_fail(msg)
{
	return {
		$: 1,
		a: msg
	};
}

function _Json_decodePrim(decoder)
{
	return { $: 2, b: decoder };
}

var _Json_decodeInt = _Json_decodePrim(function(value) {
	return (typeof value !== 'number')
		? _Json_expecting('an INT', value)
		:
	(-2147483647 < value && value < 2147483647 && (value | 0) === value)
		? $elm$core$Result$Ok(value)
		:
	(isFinite(value) && !(value % 1))
		? $elm$core$Result$Ok(value)
		: _Json_expecting('an INT', value);
});

var _Json_decodeBool = _Json_decodePrim(function(value) {
	return (typeof value === 'boolean')
		? $elm$core$Result$Ok(value)
		: _Json_expecting('a BOOL', value);
});

var _Json_decodeFloat = _Json_decodePrim(function(value) {
	return (typeof value === 'number')
		? $elm$core$Result$Ok(value)
		: _Json_expecting('a FLOAT', value);
});

var _Json_decodeValue = _Json_decodePrim(function(value) {
	return $elm$core$Result$Ok(_Json_wrap(value));
});

var _Json_decodeString = _Json_decodePrim(function(value) {
	return (typeof value === 'string')
		? $elm$core$Result$Ok(value)
		: (value instanceof String)
			? $elm$core$Result$Ok(value + '')
			: _Json_expecting('a STRING', value);
});

function _Json_decodeList(decoder) { return { $: 3, b: decoder }; }
function _Json_decodeArray(decoder) { return { $: 4, b: decoder }; }

function _Json_decodeNull(value) { return { $: 5, c: value }; }

var _Json_decodeField = F2(function(field, decoder)
{
	return {
		$: 6,
		d: field,
		b: decoder
	};
});

var _Json_decodeIndex = F2(function(index, decoder)
{
	return {
		$: 7,
		e: index,
		b: decoder
	};
});

function _Json_decodeKeyValuePairs(decoder)
{
	return {
		$: 8,
		b: decoder
	};
}

function _Json_mapMany(f, decoders)
{
	return {
		$: 9,
		f: f,
		g: decoders
	};
}

var _Json_andThen = F2(function(callback, decoder)
{
	return {
		$: 10,
		b: decoder,
		h: callback
	};
});

function _Json_oneOf(decoders)
{
	return {
		$: 11,
		g: decoders
	};
}


// DECODING OBJECTS

var _Json_map1 = F2(function(f, d1)
{
	return _Json_mapMany(f, [d1]);
});

var _Json_map2 = F3(function(f, d1, d2)
{
	return _Json_mapMany(f, [d1, d2]);
});

var _Json_map3 = F4(function(f, d1, d2, d3)
{
	return _Json_mapMany(f, [d1, d2, d3]);
});

var _Json_map4 = F5(function(f, d1, d2, d3, d4)
{
	return _Json_mapMany(f, [d1, d2, d3, d4]);
});

var _Json_map5 = F6(function(f, d1, d2, d3, d4, d5)
{
	return _Json_mapMany(f, [d1, d2, d3, d4, d5]);
});

var _Json_map6 = F7(function(f, d1, d2, d3, d4, d5, d6)
{
	return _Json_mapMany(f, [d1, d2, d3, d4, d5, d6]);
});

var _Json_map7 = F8(function(f, d1, d2, d3, d4, d5, d6, d7)
{
	return _Json_mapMany(f, [d1, d2, d3, d4, d5, d6, d7]);
});

var _Json_map8 = F9(function(f, d1, d2, d3, d4, d5, d6, d7, d8)
{
	return _Json_mapMany(f, [d1, d2, d3, d4, d5, d6, d7, d8]);
});


// DECODE

var _Json_runOnString = F2(function(decoder, string)
{
	try
	{
		var value = JSON.parse(string);
		return _Json_runHelp(decoder, value);
	}
	catch (e)
	{
		return $elm$core$Result$Err(A2($elm$json$Json$Decode$Failure, 'This is not valid JSON! ' + e.message, _Json_wrap(string)));
	}
});

var _Json_run = F2(function(decoder, value)
{
	return _Json_runHelp(decoder, _Json_unwrap(value));
});

function _Json_runHelp(decoder, value)
{
	switch (decoder.$)
	{
		case 2:
			return decoder.b(value);

		case 5:
			return (value === null)
				? $elm$core$Result$Ok(decoder.c)
				: _Json_expecting('null', value);

		case 3:
			if (!_Json_isArray(value))
			{
				return _Json_expecting('a LIST', value);
			}
			return _Json_runArrayDecoder(decoder.b, value, _List_fromArray);

		case 4:
			if (!_Json_isArray(value))
			{
				return _Json_expecting('an ARRAY', value);
			}
			return _Json_runArrayDecoder(decoder.b, value, _Json_toElmArray);

		case 6:
			var field = decoder.d;
			if (typeof value !== 'object' || value === null || !(field in value))
			{
				return _Json_expecting('an OBJECT with a field named `' + field + '`', value);
			}
			var result = _Json_runHelp(decoder.b, value[field]);
			return ($elm$core$Result$isOk(result)) ? result : $elm$core$Result$Err(A2($elm$json$Json$Decode$Field, field, result.a));

		case 7:
			var index = decoder.e;
			if (!_Json_isArray(value))
			{
				return _Json_expecting('an ARRAY', value);
			}
			if (index >= value.length)
			{
				return _Json_expecting('a LONGER array. Need index ' + index + ' but only see ' + value.length + ' entries', value);
			}
			var result = _Json_runHelp(decoder.b, value[index]);
			return ($elm$core$Result$isOk(result)) ? result : $elm$core$Result$Err(A2($elm$json$Json$Decode$Index, index, result.a));

		case 8:
			if (typeof value !== 'object' || value === null || _Json_isArray(value))
			{
				return _Json_expecting('an OBJECT', value);
			}

			var keyValuePairs = _List_Nil;
			// TODO test perf of Object.keys and switch when support is good enough
			for (var key in value)
			{
				if (value.hasOwnProperty(key))
				{
					var result = _Json_runHelp(decoder.b, value[key]);
					if (!$elm$core$Result$isOk(result))
					{
						return $elm$core$Result$Err(A2($elm$json$Json$Decode$Field, key, result.a));
					}
					keyValuePairs = _List_Cons(_Utils_Tuple2(key, result.a), keyValuePairs);
				}
			}
			return $elm$core$Result$Ok($elm$core$List$reverse(keyValuePairs));

		case 9:
			var answer = decoder.f;
			var decoders = decoder.g;
			for (var i = 0; i < decoders.length; i++)
			{
				var result = _Json_runHelp(decoders[i], value);
				if (!$elm$core$Result$isOk(result))
				{
					return result;
				}
				answer = answer(result.a);
			}
			return $elm$core$Result$Ok(answer);

		case 10:
			var result = _Json_runHelp(decoder.b, value);
			return (!$elm$core$Result$isOk(result))
				? result
				: _Json_runHelp(decoder.h(result.a), value);

		case 11:
			var errors = _List_Nil;
			for (var temp = decoder.g; temp.b; temp = temp.b) // WHILE_CONS
			{
				var result = _Json_runHelp(temp.a, value);
				if ($elm$core$Result$isOk(result))
				{
					return result;
				}
				errors = _List_Cons(result.a, errors);
			}
			return $elm$core$Result$Err($elm$json$Json$Decode$OneOf($elm$core$List$reverse(errors)));

		case 1:
			return $elm$core$Result$Err(A2($elm$json$Json$Decode$Failure, decoder.a, _Json_wrap(value)));

		case 0:
			return $elm$core$Result$Ok(decoder.a);
	}
}

function _Json_runArrayDecoder(decoder, value, toElmValue)
{
	var len = value.length;
	var array = new Array(len);
	for (var i = 0; i < len; i++)
	{
		var result = _Json_runHelp(decoder, value[i]);
		if (!$elm$core$Result$isOk(result))
		{
			return $elm$core$Result$Err(A2($elm$json$Json$Decode$Index, i, result.a));
		}
		array[i] = result.a;
	}
	return $elm$core$Result$Ok(toElmValue(array));
}

function _Json_isArray(value)
{
	return Array.isArray(value) || (typeof FileList !== 'undefined' && value instanceof FileList);
}

function _Json_toElmArray(array)
{
	return A2($elm$core$Array$initialize, array.length, function(i) { return array[i]; });
}

function _Json_expecting(type, value)
{
	return $elm$core$Result$Err(A2($elm$json$Json$Decode$Failure, 'Expecting ' + type, _Json_wrap(value)));
}


// EQUALITY

function _Json_equality(x, y)
{
	if (x === y)
	{
		return true;
	}

	if (x.$ !== y.$)
	{
		return false;
	}

	switch (x.$)
	{
		case 0:
		case 1:
			return x.a === y.a;

		case 2:
			return x.b === y.b;

		case 5:
			return x.c === y.c;

		case 3:
		case 4:
		case 8:
			return _Json_equality(x.b, y.b);

		case 6:
			return x.d === y.d && _Json_equality(x.b, y.b);

		case 7:
			return x.e === y.e && _Json_equality(x.b, y.b);

		case 9:
			return x.f === y.f && _Json_listEquality(x.g, y.g);

		case 10:
			return x.h === y.h && _Json_equality(x.b, y.b);

		case 11:
			return _Json_listEquality(x.g, y.g);
	}
}

function _Json_listEquality(aDecoders, bDecoders)
{
	var len = aDecoders.length;
	if (len !== bDecoders.length)
	{
		return false;
	}
	for (var i = 0; i < len; i++)
	{
		if (!_Json_equality(aDecoders[i], bDecoders[i]))
		{
			return false;
		}
	}
	return true;
}


// ENCODE

var _Json_encode = F2(function(indentLevel, value)
{
	return JSON.stringify(_Json_unwrap(value), null, indentLevel) + '';
});

function _Json_wrap(value) { return { $: 0, a: value }; }
function _Json_unwrap(value) { return value.a; }

function _Json_wrap_UNUSED(value) { return value; }
function _Json_unwrap_UNUSED(value) { return value; }

function _Json_emptyArray() { return []; }
function _Json_emptyObject() { return {}; }

var _Json_addField = F3(function(key, value, object)
{
	object[key] = _Json_unwrap(value);
	return object;
});

function _Json_addEntry(func)
{
	return F2(function(entry, array)
	{
		array.push(_Json_unwrap(func(entry)));
		return array;
	});
}

var _Json_encodeNull = _Json_wrap(null);



// TASKS

function _Scheduler_succeed(value)
{
	return {
		$: 0,
		a: value
	};
}

function _Scheduler_fail(error)
{
	return {
		$: 1,
		a: error
	};
}

function _Scheduler_binding(callback)
{
	return {
		$: 2,
		b: callback,
		c: null
	};
}

var _Scheduler_andThen = F2(function(callback, task)
{
	return {
		$: 3,
		b: callback,
		d: task
	};
});

var _Scheduler_onError = F2(function(callback, task)
{
	return {
		$: 4,
		b: callback,
		d: task
	};
});

function _Scheduler_receive(callback)
{
	return {
		$: 5,
		b: callback
	};
}


// PROCESSES

var _Scheduler_guid = 0;

function _Scheduler_rawSpawn(task)
{
	var proc = {
		$: 0,
		e: _Scheduler_guid++,
		f: task,
		g: null,
		h: []
	};

	_Scheduler_enqueue(proc);

	return proc;
}

function _Scheduler_spawn(task)
{
	return _Scheduler_binding(function(callback) {
		callback(_Scheduler_succeed(_Scheduler_rawSpawn(task)));
	});
}

function _Scheduler_rawSend(proc, msg)
{
	proc.h.push(msg);
	_Scheduler_enqueue(proc);
}

var _Scheduler_send = F2(function(proc, msg)
{
	return _Scheduler_binding(function(callback) {
		_Scheduler_rawSend(proc, msg);
		callback(_Scheduler_succeed(_Utils_Tuple0));
	});
});

function _Scheduler_kill(proc)
{
	return _Scheduler_binding(function(callback) {
		var task = proc.f;
		if (task.$ === 2 && task.c)
		{
			task.c();
		}

		proc.f = null;

		callback(_Scheduler_succeed(_Utils_Tuple0));
	});
}


/* STEP PROCESSES

type alias Process =
  { $ : tag
  , id : unique_id
  , root : Task
  , stack : null | { $: SUCCEED | FAIL, a: callback, b: stack }
  , mailbox : [msg]
  }

*/


var _Scheduler_working = false;
var _Scheduler_queue = [];


function _Scheduler_enqueue(proc)
{
	_Scheduler_queue.push(proc);
	if (_Scheduler_working)
	{
		return;
	}
	_Scheduler_working = true;
	while (proc = _Scheduler_queue.shift())
	{
		_Scheduler_step(proc);
	}
	_Scheduler_working = false;
}


function _Scheduler_step(proc)
{
	while (proc.f)
	{
		var rootTag = proc.f.$;
		if (rootTag === 0 || rootTag === 1)
		{
			while (proc.g && proc.g.$ !== rootTag)
			{
				proc.g = proc.g.i;
			}
			if (!proc.g)
			{
				return;
			}
			proc.f = proc.g.b(proc.f.a);
			proc.g = proc.g.i;
		}
		else if (rootTag === 2)
		{
			proc.f.c = proc.f.b(function(newRoot) {
				proc.f = newRoot;
				_Scheduler_enqueue(proc);
			});
			return;
		}
		else if (rootTag === 5)
		{
			if (proc.h.length === 0)
			{
				return;
			}
			proc.f = proc.f.b(proc.h.shift());
		}
		else // if (rootTag === 3 || rootTag === 4)
		{
			proc.g = {
				$: rootTag === 3 ? 0 : 1,
				b: proc.f.b,
				i: proc.g
			};
			proc.f = proc.f.d;
		}
	}
}



function _Process_sleep(time)
{
	return _Scheduler_binding(function(callback) {
		var id = setTimeout(function() {
			callback(_Scheduler_succeed(_Utils_Tuple0));
		}, time);

		return function() { clearTimeout(id); };
	});
}




// PROGRAMS


var _Platform_worker = F4(function(impl, flagDecoder, debugMetadata, args)
{
	return _Platform_initialize(
		flagDecoder,
		args,
		impl.init,
		impl.update,
		impl.subscriptions,
		function() { return function() {} }
	);
});



// INITIALIZE A PROGRAM


function _Platform_initialize(flagDecoder, args, init, update, subscriptions, stepperBuilder)
{
	var result = A2(_Json_run, flagDecoder, _Json_wrap(args ? args['flags'] : undefined));
	$elm$core$Result$isOk(result) || _Debug_crash(2 /**/, _Json_errorToString(result.a) /**/);
	var managers = {};
	var initPair = init(result.a);
	var model = initPair.a;
	var stepper = stepperBuilder(sendToApp, model);
	var ports = _Platform_setupEffects(managers, sendToApp);

	function sendToApp(msg, viewMetadata)
	{
		var pair = A2(update, msg, model);
		stepper(model = pair.a, viewMetadata);
		_Platform_enqueueEffects(managers, pair.b, subscriptions(model));
	}

	_Platform_enqueueEffects(managers, initPair.b, subscriptions(model));

	return ports ? { ports: ports } : {};
}



// TRACK PRELOADS
//
// This is used by code in elm/browser and elm/http
// to register any HTTP requests that are triggered by init.
//


var _Platform_preload;


function _Platform_registerPreload(url)
{
	_Platform_preload.add(url);
}



// EFFECT MANAGERS


var _Platform_effectManagers = {};


function _Platform_setupEffects(managers, sendToApp)
{
	var ports;

	// setup all necessary effect managers
	for (var key in _Platform_effectManagers)
	{
		var manager = _Platform_effectManagers[key];

		if (manager.a)
		{
			ports = ports || {};
			ports[key] = manager.a(key, sendToApp);
		}

		managers[key] = _Platform_instantiateManager(manager, sendToApp);
	}

	return ports;
}


function _Platform_createManager(init, onEffects, onSelfMsg, cmdMap, subMap)
{
	return {
		b: init,
		c: onEffects,
		d: onSelfMsg,
		e: cmdMap,
		f: subMap
	};
}


function _Platform_instantiateManager(info, sendToApp)
{
	var router = {
		g: sendToApp,
		h: undefined
	};

	var onEffects = info.c;
	var onSelfMsg = info.d;
	var cmdMap = info.e;
	var subMap = info.f;

	function loop(state)
	{
		return A2(_Scheduler_andThen, loop, _Scheduler_receive(function(msg)
		{
			var value = msg.a;

			if (msg.$ === 0)
			{
				return A3(onSelfMsg, router, value, state);
			}

			return cmdMap && subMap
				? A4(onEffects, router, value.i, value.j, state)
				: A3(onEffects, router, cmdMap ? value.i : value.j, state);
		}));
	}

	return router.h = _Scheduler_rawSpawn(A2(_Scheduler_andThen, loop, info.b));
}



// ROUTING


var _Platform_sendToApp = F2(function(router, msg)
{
	return _Scheduler_binding(function(callback)
	{
		router.g(msg);
		callback(_Scheduler_succeed(_Utils_Tuple0));
	});
});


var _Platform_sendToSelf = F2(function(router, msg)
{
	return A2(_Scheduler_send, router.h, {
		$: 0,
		a: msg
	});
});



// BAGS


function _Platform_leaf(home)
{
	return function(value)
	{
		return {
			$: 1,
			k: home,
			l: value
		};
	};
}


function _Platform_batch(list)
{
	return {
		$: 2,
		m: list
	};
}


var _Platform_map = F2(function(tagger, bag)
{
	return {
		$: 3,
		n: tagger,
		o: bag
	}
});



// PIPE BAGS INTO EFFECT MANAGERS
//
// Effects must be queued!
//
// Say your init contains a synchronous command, like Time.now or Time.here
//
//   - This will produce a batch of effects (FX_1)
//   - The synchronous task triggers the subsequent `update` call
//   - This will produce a batch of effects (FX_2)
//
// If we just start dispatching FX_2, subscriptions from FX_2 can be processed
// before subscriptions from FX_1. No good! Earlier versions of this code had
// this problem, leading to these reports:
//
//   https://github.com/elm/core/issues/980
//   https://github.com/elm/core/pull/981
//   https://github.com/elm/compiler/issues/1776
//
// The queue is necessary to avoid ordering issues for synchronous commands.


// Why use true/false here? Why not just check the length of the queue?
// The goal is to detect "are we currently dispatching effects?" If we
// are, we need to bail and let the ongoing while loop handle things.
//
// Now say the queue has 1 element. When we dequeue the final element,
// the queue will be empty, but we are still actively dispatching effects.
// So you could get queue jumping in a really tricky category of cases.
//
var _Platform_effectsQueue = [];
var _Platform_effectsActive = false;


function _Platform_enqueueEffects(managers, cmdBag, subBag)
{
	_Platform_effectsQueue.push({ p: managers, q: cmdBag, r: subBag });

	if (_Platform_effectsActive) return;

	_Platform_effectsActive = true;
	for (var fx; fx = _Platform_effectsQueue.shift(); )
	{
		_Platform_dispatchEffects(fx.p, fx.q, fx.r);
	}
	_Platform_effectsActive = false;
}


function _Platform_dispatchEffects(managers, cmdBag, subBag)
{
	var effectsDict = {};
	_Platform_gatherEffects(true, cmdBag, effectsDict, null);
	_Platform_gatherEffects(false, subBag, effectsDict, null);

	for (var home in managers)
	{
		_Scheduler_rawSend(managers[home], {
			$: 'fx',
			a: effectsDict[home] || { i: _List_Nil, j: _List_Nil }
		});
	}
}


function _Platform_gatherEffects(isCmd, bag, effectsDict, taggers)
{
	switch (bag.$)
	{
		case 1:
			var home = bag.k;
			var effect = _Platform_toEffect(isCmd, home, taggers, bag.l);
			effectsDict[home] = _Platform_insert(isCmd, effect, effectsDict[home]);
			return;

		case 2:
			for (var list = bag.m; list.b; list = list.b) // WHILE_CONS
			{
				_Platform_gatherEffects(isCmd, list.a, effectsDict, taggers);
			}
			return;

		case 3:
			_Platform_gatherEffects(isCmd, bag.o, effectsDict, {
				s: bag.n,
				t: taggers
			});
			return;
	}
}


function _Platform_toEffect(isCmd, home, taggers, value)
{
	function applyTaggers(x)
	{
		for (var temp = taggers; temp; temp = temp.t)
		{
			x = temp.s(x);
		}
		return x;
	}

	var map = isCmd
		? _Platform_effectManagers[home].e
		: _Platform_effectManagers[home].f;

	return A2(map, applyTaggers, value)
}


function _Platform_insert(isCmd, newEffect, effects)
{
	effects = effects || { i: _List_Nil, j: _List_Nil };

	isCmd
		? (effects.i = _List_Cons(newEffect, effects.i))
		: (effects.j = _List_Cons(newEffect, effects.j));

	return effects;
}



// PORTS


function _Platform_checkPortName(name)
{
	if (_Platform_effectManagers[name])
	{
		_Debug_crash(3, name)
	}
}



// OUTGOING PORTS


function _Platform_outgoingPort(name, converter)
{
	_Platform_checkPortName(name);
	_Platform_effectManagers[name] = {
		e: _Platform_outgoingPortMap,
		u: converter,
		a: _Platform_setupOutgoingPort
	};
	return _Platform_leaf(name);
}


var _Platform_outgoingPortMap = F2(function(tagger, value) { return value; });


function _Platform_setupOutgoingPort(name)
{
	var subs = [];
	var converter = _Platform_effectManagers[name].u;

	// CREATE MANAGER

	var init = _Process_sleep(0);

	_Platform_effectManagers[name].b = init;
	_Platform_effectManagers[name].c = F3(function(router, cmdList, state)
	{
		for ( ; cmdList.b; cmdList = cmdList.b) // WHILE_CONS
		{
			// grab a separate reference to subs in case unsubscribe is called
			var currentSubs = subs;
			var value = _Json_unwrap(converter(cmdList.a));
			for (var i = 0; i < currentSubs.length; i++)
			{
				currentSubs[i](value);
			}
		}
		return init;
	});

	// PUBLIC API

	function subscribe(callback)
	{
		subs.push(callback);
	}

	function unsubscribe(callback)
	{
		// copy subs into a new array in case unsubscribe is called within a
		// subscribed callback
		subs = subs.slice();
		var index = subs.indexOf(callback);
		if (index >= 0)
		{
			subs.splice(index, 1);
		}
	}

	return {
		subscribe: subscribe,
		unsubscribe: unsubscribe
	};
}



// INCOMING PORTS


function _Platform_incomingPort(name, converter)
{
	_Platform_checkPortName(name);
	_Platform_effectManagers[name] = {
		f: _Platform_incomingPortMap,
		u: converter,
		a: _Platform_setupIncomingPort
	};
	return _Platform_leaf(name);
}


var _Platform_incomingPortMap = F2(function(tagger, finalTagger)
{
	return function(value)
	{
		return tagger(finalTagger(value));
	};
});


function _Platform_setupIncomingPort(name, sendToApp)
{
	var subs = _List_Nil;
	var converter = _Platform_effectManagers[name].u;

	// CREATE MANAGER

	var init = _Scheduler_succeed(null);

	_Platform_effectManagers[name].b = init;
	_Platform_effectManagers[name].c = F3(function(router, subList, state)
	{
		subs = subList;
		return init;
	});

	// PUBLIC API

	function send(incomingValue)
	{
		var result = A2(_Json_run, converter, _Json_wrap(incomingValue));

		$elm$core$Result$isOk(result) || _Debug_crash(4, name, result.a);

		var value = result.a;
		for (var temp = subs; temp.b; temp = temp.b) // WHILE_CONS
		{
			sendToApp(temp.a(value));
		}
	}

	return { send: send };
}



// EXPORT ELM MODULES
//
// Have DEBUG and PROD versions so that we can (1) give nicer errors in
// debug mode and (2) not pay for the bits needed for that in prod mode.
//


function _Platform_export_UNUSED(exports)
{
	scope['Elm']
		? _Platform_mergeExportsProd(scope['Elm'], exports)
		: scope['Elm'] = exports;
}


function _Platform_mergeExportsProd(obj, exports)
{
	for (var name in exports)
	{
		(name in obj)
			? (name == 'init')
				? _Debug_crash(6)
				: _Platform_mergeExportsProd(obj[name], exports[name])
			: (obj[name] = exports[name]);
	}
}


function _Platform_export(exports)
{
	scope['Elm']
		? _Platform_mergeExportsDebug('Elm', scope['Elm'], exports)
		: scope['Elm'] = exports;
}


function _Platform_mergeExportsDebug(moduleName, obj, exports)
{
	for (var name in exports)
	{
		(name in obj)
			? (name == 'init')
				? _Debug_crash(6, moduleName)
				: _Platform_mergeExportsDebug(moduleName + '.' + name, obj[name], exports[name])
			: (obj[name] = exports[name]);
	}
}




// HELPERS


var _VirtualDom_divertHrefToApp;

var _VirtualDom_doc = typeof document !== 'undefined' ? document : {};


function _VirtualDom_appendChild(parent, child)
{
	parent.appendChild(child);
}

var _VirtualDom_init = F4(function(virtualNode, flagDecoder, debugMetadata, args)
{
	// NOTE: this function needs _Platform_export available to work

	/**_UNUSED/
	var node = args['node'];
	//*/
	/**/
	var node = args && args['node'] ? args['node'] : _Debug_crash(0);
	//*/

	node.parentNode.replaceChild(
		_VirtualDom_render(virtualNode, function() {}),
		node
	);

	return {};
});



// TEXT


function _VirtualDom_text(string)
{
	return {
		$: 0,
		a: string
	};
}



// NODE


var _VirtualDom_nodeNS = F2(function(namespace, tag)
{
	return F2(function(factList, kidList)
	{
		for (var kids = [], descendantsCount = 0; kidList.b; kidList = kidList.b) // WHILE_CONS
		{
			var kid = kidList.a;
			descendantsCount += (kid.b || 0);
			kids.push(kid);
		}
		descendantsCount += kids.length;

		return {
			$: 1,
			c: tag,
			d: _VirtualDom_organizeFacts(factList),
			e: kids,
			f: namespace,
			b: descendantsCount
		};
	});
});


var _VirtualDom_node = _VirtualDom_nodeNS(undefined);



// KEYED NODE


var _VirtualDom_keyedNodeNS = F2(function(namespace, tag)
{
	return F2(function(factList, kidList)
	{
		for (var kids = [], descendantsCount = 0; kidList.b; kidList = kidList.b) // WHILE_CONS
		{
			var kid = kidList.a;
			descendantsCount += (kid.b.b || 0);
			kids.push(kid);
		}
		descendantsCount += kids.length;

		return {
			$: 2,
			c: tag,
			d: _VirtualDom_organizeFacts(factList),
			e: kids,
			f: namespace,
			b: descendantsCount
		};
	});
});


var _VirtualDom_keyedNode = _VirtualDom_keyedNodeNS(undefined);



// CUSTOM


function _VirtualDom_custom(factList, model, render, diff)
{
	return {
		$: 3,
		d: _VirtualDom_organizeFacts(factList),
		g: model,
		h: render,
		i: diff
	};
}



// MAP


var _VirtualDom_map = F2(function(tagger, node)
{
	return {
		$: 4,
		j: tagger,
		k: node,
		b: 1 + (node.b || 0)
	};
});



// LAZY


function _VirtualDom_thunk(refs, thunk)
{
	return {
		$: 5,
		l: refs,
		m: thunk,
		k: undefined
	};
}

var _VirtualDom_lazy = F2(function(func, a)
{
	return _VirtualDom_thunk([func, a], function() {
		return func(a);
	});
});

var _VirtualDom_lazy2 = F3(function(func, a, b)
{
	return _VirtualDom_thunk([func, a, b], function() {
		return A2(func, a, b);
	});
});

var _VirtualDom_lazy3 = F4(function(func, a, b, c)
{
	return _VirtualDom_thunk([func, a, b, c], function() {
		return A3(func, a, b, c);
	});
});

var _VirtualDom_lazy4 = F5(function(func, a, b, c, d)
{
	return _VirtualDom_thunk([func, a, b, c, d], function() {
		return A4(func, a, b, c, d);
	});
});

var _VirtualDom_lazy5 = F6(function(func, a, b, c, d, e)
{
	return _VirtualDom_thunk([func, a, b, c, d, e], function() {
		return A5(func, a, b, c, d, e);
	});
});

var _VirtualDom_lazy6 = F7(function(func, a, b, c, d, e, f)
{
	return _VirtualDom_thunk([func, a, b, c, d, e, f], function() {
		return A6(func, a, b, c, d, e, f);
	});
});

var _VirtualDom_lazy7 = F8(function(func, a, b, c, d, e, f, g)
{
	return _VirtualDom_thunk([func, a, b, c, d, e, f, g], function() {
		return A7(func, a, b, c, d, e, f, g);
	});
});

var _VirtualDom_lazy8 = F9(function(func, a, b, c, d, e, f, g, h)
{
	return _VirtualDom_thunk([func, a, b, c, d, e, f, g, h], function() {
		return A8(func, a, b, c, d, e, f, g, h);
	});
});



// FACTS


var _VirtualDom_on = F2(function(key, handler)
{
	return {
		$: 'a0',
		n: key,
		o: handler
	};
});
var _VirtualDom_style = F2(function(key, value)
{
	return {
		$: 'a1',
		n: key,
		o: value
	};
});
var _VirtualDom_property = F2(function(key, value)
{
	return {
		$: 'a2',
		n: key,
		o: value
	};
});
var _VirtualDom_attribute = F2(function(key, value)
{
	return {
		$: 'a3',
		n: key,
		o: value
	};
});
var _VirtualDom_attributeNS = F3(function(namespace, key, value)
{
	return {
		$: 'a4',
		n: key,
		o: { f: namespace, o: value }
	};
});



// XSS ATTACK VECTOR CHECKS
//
// For some reason, tabs can appear in href protocols and it still works.
// So '\tjava\tSCRIPT:alert("!!!")' and 'javascript:alert("!!!")' are the same
// in practice. That is why _VirtualDom_RE_js and _VirtualDom_RE_js_html look
// so freaky.
//
// Pulling the regular expressions out to the top level gives a slight speed
// boost in small benchmarks (4-10%) but hoisting values to reduce allocation
// can be unpredictable in large programs where JIT may have a harder time with
// functions are not fully self-contained. The benefit is more that the js and
// js_html ones are so weird that I prefer to see them near each other.


var _VirtualDom_RE_script = /^script$/i;
var _VirtualDom_RE_on_formAction = /^(on|formAction$)/i;
var _VirtualDom_RE_js = /^\s*j\s*a\s*v\s*a\s*s\s*c\s*r\s*i\s*p\s*t\s*:/i;
var _VirtualDom_RE_js_html = /^\s*(j\s*a\s*v\s*a\s*s\s*c\s*r\s*i\s*p\s*t\s*:|d\s*a\s*t\s*a\s*:\s*t\s*e\s*x\s*t\s*\/\s*h\s*t\s*m\s*l\s*(,|;))/i;


function _VirtualDom_noScript(tag)
{
	return _VirtualDom_RE_script.test(tag) ? 'p' : tag;
}

function _VirtualDom_noOnOrFormAction(key)
{
	return _VirtualDom_RE_on_formAction.test(key) ? 'data-' + key : key;
}

function _VirtualDom_noInnerHtmlOrFormAction(key)
{
	return key == 'innerHTML' || key == 'formAction' ? 'data-' + key : key;
}

function _VirtualDom_noJavaScriptUri(value)
{
	return _VirtualDom_RE_js.test(value)
		? /**_UNUSED/''//*//**/'javascript:alert("This is an XSS vector. Please use ports or web components instead.")'//*/
		: value;
}

function _VirtualDom_noJavaScriptOrHtmlUri(value)
{
	return _VirtualDom_RE_js_html.test(value)
		? /**_UNUSED/''//*//**/'javascript:alert("This is an XSS vector. Please use ports or web components instead.")'//*/
		: value;
}

function _VirtualDom_noJavaScriptOrHtmlJson(value)
{
	return (typeof _Json_unwrap(value) === 'string' && _VirtualDom_RE_js_html.test(_Json_unwrap(value)))
		? _Json_wrap(
			/**_UNUSED/''//*//**/'javascript:alert("This is an XSS vector. Please use ports or web components instead.")'//*/
		) : value;
}



// MAP FACTS


var _VirtualDom_mapAttribute = F2(function(func, attr)
{
	return (attr.$ === 'a0')
		? A2(_VirtualDom_on, attr.n, _VirtualDom_mapHandler(func, attr.o))
		: attr;
});

function _VirtualDom_mapHandler(func, handler)
{
	var tag = $elm$virtual_dom$VirtualDom$toHandlerInt(handler);

	// 0 = Normal
	// 1 = MayStopPropagation
	// 2 = MayPreventDefault
	// 3 = Custom

	return {
		$: handler.$,
		a:
			!tag
				? A2($elm$json$Json$Decode$map, func, handler.a)
				:
			A3($elm$json$Json$Decode$map2,
				tag < 3
					? _VirtualDom_mapEventTuple
					: _VirtualDom_mapEventRecord,
				$elm$json$Json$Decode$succeed(func),
				handler.a
			)
	};
}

var _VirtualDom_mapEventTuple = F2(function(func, tuple)
{
	return _Utils_Tuple2(func(tuple.a), tuple.b);
});

var _VirtualDom_mapEventRecord = F2(function(func, record)
{
	return {
		message: func(record.message),
		stopPropagation: record.stopPropagation,
		preventDefault: record.preventDefault
	}
});



// ORGANIZE FACTS


function _VirtualDom_organizeFacts(factList)
{
	for (var facts = {}; factList.b; factList = factList.b) // WHILE_CONS
	{
		var entry = factList.a;

		var tag = entry.$;
		var key = entry.n;
		var value = entry.o;

		if (tag === 'a2')
		{
			(key === 'className')
				? _VirtualDom_addClass(facts, key, _Json_unwrap(value))
				: facts[key] = _Json_unwrap(value);

			continue;
		}

		var subFacts = facts[tag] || (facts[tag] = {});
		(tag === 'a3' && key === 'class')
			? _VirtualDom_addClass(subFacts, key, value)
			: subFacts[key] = value;
	}

	return facts;
}

function _VirtualDom_addClass(object, key, newClass)
{
	var classes = object[key];
	object[key] = classes ? classes + ' ' + newClass : newClass;
}



// RENDER


function _VirtualDom_render(vNode, eventNode)
{
	var tag = vNode.$;

	if (tag === 5)
	{
		return _VirtualDom_render(vNode.k || (vNode.k = vNode.m()), eventNode);
	}

	if (tag === 0)
	{
		return _VirtualDom_doc.createTextNode(vNode.a);
	}

	if (tag === 4)
	{
		var subNode = vNode.k;
		var tagger = vNode.j;

		while (subNode.$ === 4)
		{
			typeof tagger !== 'object'
				? tagger = [tagger, subNode.j]
				: tagger.push(subNode.j);

			subNode = subNode.k;
		}

		var subEventRoot = { j: tagger, p: eventNode };
		var domNode = _VirtualDom_render(subNode, subEventRoot);
		domNode.elm_event_node_ref = subEventRoot;
		return domNode;
	}

	if (tag === 3)
	{
		var domNode = vNode.h(vNode.g);
		_VirtualDom_applyFacts(domNode, eventNode, vNode.d);
		return domNode;
	}

	// at this point `tag` must be 1 or 2

	var domNode = vNode.f
		? _VirtualDom_doc.createElementNS(vNode.f, vNode.c)
		: _VirtualDom_doc.createElement(vNode.c);

	if (_VirtualDom_divertHrefToApp && vNode.c == 'a')
	{
		domNode.addEventListener('click', _VirtualDom_divertHrefToApp(domNode));
	}

	_VirtualDom_applyFacts(domNode, eventNode, vNode.d);

	for (var kids = vNode.e, i = 0; i < kids.length; i++)
	{
		_VirtualDom_appendChild(domNode, _VirtualDom_render(tag === 1 ? kids[i] : kids[i].b, eventNode));
	}

	return domNode;
}



// APPLY FACTS


function _VirtualDom_applyFacts(domNode, eventNode, facts)
{
	for (var key in facts)
	{
		var value = facts[key];

		key === 'a1'
			? _VirtualDom_applyStyles(domNode, value)
			:
		key === 'a0'
			? _VirtualDom_applyEvents(domNode, eventNode, value)
			:
		key === 'a3'
			? _VirtualDom_applyAttrs(domNode, value)
			:
		key === 'a4'
			? _VirtualDom_applyAttrsNS(domNode, value)
			:
		((key !== 'value' && key !== 'checked') || domNode[key] !== value) && (domNode[key] = value);
	}
}



// APPLY STYLES


function _VirtualDom_applyStyles(domNode, styles)
{
	var domNodeStyle = domNode.style;

	for (var key in styles)
	{
		domNodeStyle[key] = styles[key];
	}
}



// APPLY ATTRS


function _VirtualDom_applyAttrs(domNode, attrs)
{
	for (var key in attrs)
	{
		var value = attrs[key];
		typeof value !== 'undefined'
			? domNode.setAttribute(key, value)
			: domNode.removeAttribute(key);
	}
}



// APPLY NAMESPACED ATTRS


function _VirtualDom_applyAttrsNS(domNode, nsAttrs)
{
	for (var key in nsAttrs)
	{
		var pair = nsAttrs[key];
		var namespace = pair.f;
		var value = pair.o;

		typeof value !== 'undefined'
			? domNode.setAttributeNS(namespace, key, value)
			: domNode.removeAttributeNS(namespace, key);
	}
}



// APPLY EVENTS


function _VirtualDom_applyEvents(domNode, eventNode, events)
{
	var allCallbacks = domNode.elmFs || (domNode.elmFs = {});

	for (var key in events)
	{
		var newHandler = events[key];
		var oldCallback = allCallbacks[key];

		if (!newHandler)
		{
			domNode.removeEventListener(key, oldCallback);
			allCallbacks[key] = undefined;
			continue;
		}

		if (oldCallback)
		{
			var oldHandler = oldCallback.q;
			if (oldHandler.$ === newHandler.$)
			{
				oldCallback.q = newHandler;
				continue;
			}
			domNode.removeEventListener(key, oldCallback);
		}

		oldCallback = _VirtualDom_makeCallback(eventNode, newHandler);
		domNode.addEventListener(key, oldCallback,
			_VirtualDom_passiveSupported
			&& { passive: $elm$virtual_dom$VirtualDom$toHandlerInt(newHandler) < 2 }
		);
		allCallbacks[key] = oldCallback;
	}
}



// PASSIVE EVENTS


var _VirtualDom_passiveSupported;

try
{
	window.addEventListener('t', null, Object.defineProperty({}, 'passive', {
		get: function() { _VirtualDom_passiveSupported = true; }
	}));
}
catch(e) {}



// EVENT HANDLERS


function _VirtualDom_makeCallback(eventNode, initialHandler)
{
	function callback(event)
	{
		var handler = callback.q;
		var result = _Json_runHelp(handler.a, event);

		if (!$elm$core$Result$isOk(result))
		{
			return;
		}

		var tag = $elm$virtual_dom$VirtualDom$toHandlerInt(handler);

		// 0 = Normal
		// 1 = MayStopPropagation
		// 2 = MayPreventDefault
		// 3 = Custom

		var value = result.a;
		var message = !tag ? value : tag < 3 ? value.a : value.message;
		var stopPropagation = tag == 1 ? value.b : tag == 3 && value.stopPropagation;
		var currentEventNode = (
			stopPropagation && event.stopPropagation(),
			(tag == 2 ? value.b : tag == 3 && value.preventDefault) && event.preventDefault(),
			eventNode
		);
		var tagger;
		var i;
		while (tagger = currentEventNode.j)
		{
			if (typeof tagger == 'function')
			{
				message = tagger(message);
			}
			else
			{
				for (var i = tagger.length; i--; )
				{
					message = tagger[i](message);
				}
			}
			currentEventNode = currentEventNode.p;
		}
		currentEventNode(message, stopPropagation); // stopPropagation implies isSync
	}

	callback.q = initialHandler;

	return callback;
}

function _VirtualDom_equalEvents(x, y)
{
	return x.$ == y.$ && _Json_equality(x.a, y.a);
}



// DIFF


// TODO: Should we do patches like in iOS?
//
// type Patch
//   = At Int Patch
//   | Batch (List Patch)
//   | Change ...
//
// How could it not be better?
//
function _VirtualDom_diff(x, y)
{
	var patches = [];
	_VirtualDom_diffHelp(x, y, patches, 0);
	return patches;
}


function _VirtualDom_pushPatch(patches, type, index, data)
{
	var patch = {
		$: type,
		r: index,
		s: data,
		t: undefined,
		u: undefined
	};
	patches.push(patch);
	return patch;
}


function _VirtualDom_diffHelp(x, y, patches, index)
{
	if (x === y)
	{
		return;
	}

	var xType = x.$;
	var yType = y.$;

	// Bail if you run into different types of nodes. Implies that the
	// structure has changed significantly and it's not worth a diff.
	if (xType !== yType)
	{
		if (xType === 1 && yType === 2)
		{
			y = _VirtualDom_dekey(y);
			yType = 1;
		}
		else
		{
			_VirtualDom_pushPatch(patches, 0, index, y);
			return;
		}
	}

	// Now we know that both nodes are the same $.
	switch (yType)
	{
		case 5:
			var xRefs = x.l;
			var yRefs = y.l;
			var i = xRefs.length;
			var same = i === yRefs.length;
			while (same && i--)
			{
				same = xRefs[i] === yRefs[i];
			}
			if (same)
			{
				y.k = x.k;
				return;
			}
			y.k = y.m();
			var subPatches = [];
			_VirtualDom_diffHelp(x.k, y.k, subPatches, 0);
			subPatches.length > 0 && _VirtualDom_pushPatch(patches, 1, index, subPatches);
			return;

		case 4:
			// gather nested taggers
			var xTaggers = x.j;
			var yTaggers = y.j;
			var nesting = false;

			var xSubNode = x.k;
			while (xSubNode.$ === 4)
			{
				nesting = true;

				typeof xTaggers !== 'object'
					? xTaggers = [xTaggers, xSubNode.j]
					: xTaggers.push(xSubNode.j);

				xSubNode = xSubNode.k;
			}

			var ySubNode = y.k;
			while (ySubNode.$ === 4)
			{
				nesting = true;

				typeof yTaggers !== 'object'
					? yTaggers = [yTaggers, ySubNode.j]
					: yTaggers.push(ySubNode.j);

				ySubNode = ySubNode.k;
			}

			// Just bail if different numbers of taggers. This implies the
			// structure of the virtual DOM has changed.
			if (nesting && xTaggers.length !== yTaggers.length)
			{
				_VirtualDom_pushPatch(patches, 0, index, y);
				return;
			}

			// check if taggers are "the same"
			if (nesting ? !_VirtualDom_pairwiseRefEqual(xTaggers, yTaggers) : xTaggers !== yTaggers)
			{
				_VirtualDom_pushPatch(patches, 2, index, yTaggers);
			}

			// diff everything below the taggers
			_VirtualDom_diffHelp(xSubNode, ySubNode, patches, index + 1);
			return;

		case 0:
			if (x.a !== y.a)
			{
				_VirtualDom_pushPatch(patches, 3, index, y.a);
			}
			return;

		case 1:
			_VirtualDom_diffNodes(x, y, patches, index, _VirtualDom_diffKids);
			return;

		case 2:
			_VirtualDom_diffNodes(x, y, patches, index, _VirtualDom_diffKeyedKids);
			return;

		case 3:
			if (x.h !== y.h)
			{
				_VirtualDom_pushPatch(patches, 0, index, y);
				return;
			}

			var factsDiff = _VirtualDom_diffFacts(x.d, y.d);
			factsDiff && _VirtualDom_pushPatch(patches, 4, index, factsDiff);

			var patch = y.i(x.g, y.g);
			patch && _VirtualDom_pushPatch(patches, 5, index, patch);

			return;
	}
}

// assumes the incoming arrays are the same length
function _VirtualDom_pairwiseRefEqual(as, bs)
{
	for (var i = 0; i < as.length; i++)
	{
		if (as[i] !== bs[i])
		{
			return false;
		}
	}

	return true;
}

function _VirtualDom_diffNodes(x, y, patches, index, diffKids)
{
	// Bail if obvious indicators have changed. Implies more serious
	// structural changes such that it's not worth it to diff.
	if (x.c !== y.c || x.f !== y.f)
	{
		_VirtualDom_pushPatch(patches, 0, index, y);
		return;
	}

	var factsDiff = _VirtualDom_diffFacts(x.d, y.d);
	factsDiff && _VirtualDom_pushPatch(patches, 4, index, factsDiff);

	diffKids(x, y, patches, index);
}



// DIFF FACTS


// TODO Instead of creating a new diff object, it's possible to just test if
// there *is* a diff. During the actual patch, do the diff again and make the
// modifications directly. This way, there's no new allocations. Worth it?
function _VirtualDom_diffFacts(x, y, category)
{
	var diff;

	// look for changes and removals
	for (var xKey in x)
	{
		if (xKey === 'a1' || xKey === 'a0' || xKey === 'a3' || xKey === 'a4')
		{
			var subDiff = _VirtualDom_diffFacts(x[xKey], y[xKey] || {}, xKey);
			if (subDiff)
			{
				diff = diff || {};
				diff[xKey] = subDiff;
			}
			continue;
		}

		// remove if not in the new facts
		if (!(xKey in y))
		{
			diff = diff || {};
			diff[xKey] =
				!category
					? (typeof x[xKey] === 'string' ? '' : null)
					:
				(category === 'a1')
					? ''
					:
				(category === 'a0' || category === 'a3')
					? undefined
					:
				{ f: x[xKey].f, o: undefined };

			continue;
		}

		var xValue = x[xKey];
		var yValue = y[xKey];

		// reference equal, so don't worry about it
		if (xValue === yValue && xKey !== 'value' && xKey !== 'checked'
			|| category === 'a0' && _VirtualDom_equalEvents(xValue, yValue))
		{
			continue;
		}

		diff = diff || {};
		diff[xKey] = yValue;
	}

	// add new stuff
	for (var yKey in y)
	{
		if (!(yKey in x))
		{
			diff = diff || {};
			diff[yKey] = y[yKey];
		}
	}

	return diff;
}



// DIFF KIDS


function _VirtualDom_diffKids(xParent, yParent, patches, index)
{
	var xKids = xParent.e;
	var yKids = yParent.e;

	var xLen = xKids.length;
	var yLen = yKids.length;

	// FIGURE OUT IF THERE ARE INSERTS OR REMOVALS

	if (xLen > yLen)
	{
		_VirtualDom_pushPatch(patches, 6, index, {
			v: yLen,
			i: xLen - yLen
		});
	}
	else if (xLen < yLen)
	{
		_VirtualDom_pushPatch(patches, 7, index, {
			v: xLen,
			e: yKids
		});
	}

	// PAIRWISE DIFF EVERYTHING ELSE

	for (var minLen = xLen < yLen ? xLen : yLen, i = 0; i < minLen; i++)
	{
		var xKid = xKids[i];
		_VirtualDom_diffHelp(xKid, yKids[i], patches, ++index);
		index += xKid.b || 0;
	}
}



// KEYED DIFF


function _VirtualDom_diffKeyedKids(xParent, yParent, patches, rootIndex)
{
	var localPatches = [];

	var changes = {}; // Dict String Entry
	var inserts = []; // Array { index : Int, entry : Entry }
	// type Entry = { tag : String, vnode : VNode, index : Int, data : _ }

	var xKids = xParent.e;
	var yKids = yParent.e;
	var xLen = xKids.length;
	var yLen = yKids.length;
	var xIndex = 0;
	var yIndex = 0;

	var index = rootIndex;

	while (xIndex < xLen && yIndex < yLen)
	{
		var x = xKids[xIndex];
		var y = yKids[yIndex];

		var xKey = x.a;
		var yKey = y.a;
		var xNode = x.b;
		var yNode = y.b;

		var newMatch = undefined;
		var oldMatch = undefined;

		// check if keys match

		if (xKey === yKey)
		{
			index++;
			_VirtualDom_diffHelp(xNode, yNode, localPatches, index);
			index += xNode.b || 0;

			xIndex++;
			yIndex++;
			continue;
		}

		// look ahead 1 to detect insertions and removals.

		var xNext = xKids[xIndex + 1];
		var yNext = yKids[yIndex + 1];

		if (xNext)
		{
			var xNextKey = xNext.a;
			var xNextNode = xNext.b;
			oldMatch = yKey === xNextKey;
		}

		if (yNext)
		{
			var yNextKey = yNext.a;
			var yNextNode = yNext.b;
			newMatch = xKey === yNextKey;
		}


		// swap x and y
		if (newMatch && oldMatch)
		{
			index++;
			_VirtualDom_diffHelp(xNode, yNextNode, localPatches, index);
			_VirtualDom_insertNode(changes, localPatches, xKey, yNode, yIndex, inserts);
			index += xNode.b || 0;

			index++;
			_VirtualDom_removeNode(changes, localPatches, xKey, xNextNode, index);
			index += xNextNode.b || 0;

			xIndex += 2;
			yIndex += 2;
			continue;
		}

		// insert y
		if (newMatch)
		{
			index++;
			_VirtualDom_insertNode(changes, localPatches, yKey, yNode, yIndex, inserts);
			_VirtualDom_diffHelp(xNode, yNextNode, localPatches, index);
			index += xNode.b || 0;

			xIndex += 1;
			yIndex += 2;
			continue;
		}

		// remove x
		if (oldMatch)
		{
			index++;
			_VirtualDom_removeNode(changes, localPatches, xKey, xNode, index);
			index += xNode.b || 0;

			index++;
			_VirtualDom_diffHelp(xNextNode, yNode, localPatches, index);
			index += xNextNode.b || 0;

			xIndex += 2;
			yIndex += 1;
			continue;
		}

		// remove x, insert y
		if (xNext && xNextKey === yNextKey)
		{
			index++;
			_VirtualDom_removeNode(changes, localPatches, xKey, xNode, index);
			_VirtualDom_insertNode(changes, localPatches, yKey, yNode, yIndex, inserts);
			index += xNode.b || 0;

			index++;
			_VirtualDom_diffHelp(xNextNode, yNextNode, localPatches, index);
			index += xNextNode.b || 0;

			xIndex += 2;
			yIndex += 2;
			continue;
		}

		break;
	}

	// eat up any remaining nodes with removeNode and insertNode

	while (xIndex < xLen)
	{
		index++;
		var x = xKids[xIndex];
		var xNode = x.b;
		_VirtualDom_removeNode(changes, localPatches, x.a, xNode, index);
		index += xNode.b || 0;
		xIndex++;
	}

	while (yIndex < yLen)
	{
		var endInserts = endInserts || [];
		var y = yKids[yIndex];
		_VirtualDom_insertNode(changes, localPatches, y.a, y.b, undefined, endInserts);
		yIndex++;
	}

	if (localPatches.length > 0 || inserts.length > 0 || endInserts)
	{
		_VirtualDom_pushPatch(patches, 8, rootIndex, {
			w: localPatches,
			x: inserts,
			y: endInserts
		});
	}
}



// CHANGES FROM KEYED DIFF


var _VirtualDom_POSTFIX = '_elmW6BL';


function _VirtualDom_insertNode(changes, localPatches, key, vnode, yIndex, inserts)
{
	var entry = changes[key];

	// never seen this key before
	if (!entry)
	{
		entry = {
			c: 0,
			z: vnode,
			r: yIndex,
			s: undefined
		};

		inserts.push({ r: yIndex, A: entry });
		changes[key] = entry;

		return;
	}

	// this key was removed earlier, a match!
	if (entry.c === 1)
	{
		inserts.push({ r: yIndex, A: entry });

		entry.c = 2;
		var subPatches = [];
		_VirtualDom_diffHelp(entry.z, vnode, subPatches, entry.r);
		entry.r = yIndex;
		entry.s.s = {
			w: subPatches,
			A: entry
		};

		return;
	}

	// this key has already been inserted or moved, a duplicate!
	_VirtualDom_insertNode(changes, localPatches, key + _VirtualDom_POSTFIX, vnode, yIndex, inserts);
}


function _VirtualDom_removeNode(changes, localPatches, key, vnode, index)
{
	var entry = changes[key];

	// never seen this key before
	if (!entry)
	{
		var patch = _VirtualDom_pushPatch(localPatches, 9, index, undefined);

		changes[key] = {
			c: 1,
			z: vnode,
			r: index,
			s: patch
		};

		return;
	}

	// this key was inserted earlier, a match!
	if (entry.c === 0)
	{
		entry.c = 2;
		var subPatches = [];
		_VirtualDom_diffHelp(vnode, entry.z, subPatches, index);

		_VirtualDom_pushPatch(localPatches, 9, index, {
			w: subPatches,
			A: entry
		});

		return;
	}

	// this key has already been removed or moved, a duplicate!
	_VirtualDom_removeNode(changes, localPatches, key + _VirtualDom_POSTFIX, vnode, index);
}



// ADD DOM NODES
//
// Each DOM node has an "index" assigned in order of traversal. It is important
// to minimize our crawl over the actual DOM, so these indexes (along with the
// descendantsCount of virtual nodes) let us skip touching entire subtrees of
// the DOM if we know there are no patches there.


function _VirtualDom_addDomNodes(domNode, vNode, patches, eventNode)
{
	_VirtualDom_addDomNodesHelp(domNode, vNode, patches, 0, 0, vNode.b, eventNode);
}


// assumes `patches` is non-empty and indexes increase monotonically.
function _VirtualDom_addDomNodesHelp(domNode, vNode, patches, i, low, high, eventNode)
{
	var patch = patches[i];
	var index = patch.r;

	while (index === low)
	{
		var patchType = patch.$;

		if (patchType === 1)
		{
			_VirtualDom_addDomNodes(domNode, vNode.k, patch.s, eventNode);
		}
		else if (patchType === 8)
		{
			patch.t = domNode;
			patch.u = eventNode;

			var subPatches = patch.s.w;
			if (subPatches.length > 0)
			{
				_VirtualDom_addDomNodesHelp(domNode, vNode, subPatches, 0, low, high, eventNode);
			}
		}
		else if (patchType === 9)
		{
			patch.t = domNode;
			patch.u = eventNode;

			var data = patch.s;
			if (data)
			{
				data.A.s = domNode;
				var subPatches = data.w;
				if (subPatches.length > 0)
				{
					_VirtualDom_addDomNodesHelp(domNode, vNode, subPatches, 0, low, high, eventNode);
				}
			}
		}
		else
		{
			patch.t = domNode;
			patch.u = eventNode;
		}

		i++;

		if (!(patch = patches[i]) || (index = patch.r) > high)
		{
			return i;
		}
	}

	var tag = vNode.$;

	if (tag === 4)
	{
		var subNode = vNode.k;

		while (subNode.$ === 4)
		{
			subNode = subNode.k;
		}

		return _VirtualDom_addDomNodesHelp(domNode, subNode, patches, i, low + 1, high, domNode.elm_event_node_ref);
	}

	// tag must be 1 or 2 at this point

	var vKids = vNode.e;
	var childNodes = domNode.childNodes;
	for (var j = 0; j < vKids.length; j++)
	{
		low++;
		var vKid = tag === 1 ? vKids[j] : vKids[j].b;
		var nextLow = low + (vKid.b || 0);
		if (low <= index && index <= nextLow)
		{
			i = _VirtualDom_addDomNodesHelp(childNodes[j], vKid, patches, i, low, nextLow, eventNode);
			if (!(patch = patches[i]) || (index = patch.r) > high)
			{
				return i;
			}
		}
		low = nextLow;
	}
	return i;
}



// APPLY PATCHES


function _VirtualDom_applyPatches(rootDomNode, oldVirtualNode, patches, eventNode)
{
	if (patches.length === 0)
	{
		return rootDomNode;
	}

	_VirtualDom_addDomNodes(rootDomNode, oldVirtualNode, patches, eventNode);
	return _VirtualDom_applyPatchesHelp(rootDomNode, patches);
}

function _VirtualDom_applyPatchesHelp(rootDomNode, patches)
{
	for (var i = 0; i < patches.length; i++)
	{
		var patch = patches[i];
		var localDomNode = patch.t
		var newNode = _VirtualDom_applyPatch(localDomNode, patch);
		if (localDomNode === rootDomNode)
		{
			rootDomNode = newNode;
		}
	}
	return rootDomNode;
}

function _VirtualDom_applyPatch(domNode, patch)
{
	switch (patch.$)
	{
		case 0:
			return _VirtualDom_applyPatchRedraw(domNode, patch.s, patch.u);

		case 4:
			_VirtualDom_applyFacts(domNode, patch.u, patch.s);
			return domNode;

		case 3:
			domNode.replaceData(0, domNode.length, patch.s);
			return domNode;

		case 1:
			return _VirtualDom_applyPatchesHelp(domNode, patch.s);

		case 2:
			if (domNode.elm_event_node_ref)
			{
				domNode.elm_event_node_ref.j = patch.s;
			}
			else
			{
				domNode.elm_event_node_ref = { j: patch.s, p: patch.u };
			}
			return domNode;

		case 6:
			var data = patch.s;
			for (var i = 0; i < data.i; i++)
			{
				domNode.removeChild(domNode.childNodes[data.v]);
			}
			return domNode;

		case 7:
			var data = patch.s;
			var kids = data.e;
			var i = data.v;
			var theEnd = domNode.childNodes[i];
			for (; i < kids.length; i++)
			{
				domNode.insertBefore(_VirtualDom_render(kids[i], patch.u), theEnd);
			}
			return domNode;

		case 9:
			var data = patch.s;
			if (!data)
			{
				domNode.parentNode.removeChild(domNode);
				return domNode;
			}
			var entry = data.A;
			if (typeof entry.r !== 'undefined')
			{
				domNode.parentNode.removeChild(domNode);
			}
			entry.s = _VirtualDom_applyPatchesHelp(domNode, data.w);
			return domNode;

		case 8:
			return _VirtualDom_applyPatchReorder(domNode, patch);

		case 5:
			return patch.s(domNode);

		default:
			_Debug_crash(10); // 'Ran into an unknown patch!'
	}
}


function _VirtualDom_applyPatchRedraw(domNode, vNode, eventNode)
{
	var parentNode = domNode.parentNode;
	var newNode = _VirtualDom_render(vNode, eventNode);

	if (!newNode.elm_event_node_ref)
	{
		newNode.elm_event_node_ref = domNode.elm_event_node_ref;
	}

	if (parentNode && newNode !== domNode)
	{
		parentNode.replaceChild(newNode, domNode);
	}
	return newNode;
}


function _VirtualDom_applyPatchReorder(domNode, patch)
{
	var data = patch.s;

	// remove end inserts
	var frag = _VirtualDom_applyPatchReorderEndInsertsHelp(data.y, patch);

	// removals
	domNode = _VirtualDom_applyPatchesHelp(domNode, data.w);

	// inserts
	var inserts = data.x;
	for (var i = 0; i < inserts.length; i++)
	{
		var insert = inserts[i];
		var entry = insert.A;
		var node = entry.c === 2
			? entry.s
			: _VirtualDom_render(entry.z, patch.u);
		domNode.insertBefore(node, domNode.childNodes[insert.r]);
	}

	// add end inserts
	if (frag)
	{
		_VirtualDom_appendChild(domNode, frag);
	}

	return domNode;
}


function _VirtualDom_applyPatchReorderEndInsertsHelp(endInserts, patch)
{
	if (!endInserts)
	{
		return;
	}

	var frag = _VirtualDom_doc.createDocumentFragment();
	for (var i = 0; i < endInserts.length; i++)
	{
		var insert = endInserts[i];
		var entry = insert.A;
		_VirtualDom_appendChild(frag, entry.c === 2
			? entry.s
			: _VirtualDom_render(entry.z, patch.u)
		);
	}
	return frag;
}


function _VirtualDom_virtualize(node)
{
	// TEXT NODES

	if (node.nodeType === 3)
	{
		return _VirtualDom_text(node.textContent);
	}


	// WEIRD NODES

	if (node.nodeType !== 1)
	{
		return _VirtualDom_text('');
	}


	// ELEMENT NODES

	var attrList = _List_Nil;
	var attrs = node.attributes;
	for (var i = attrs.length; i--; )
	{
		var attr = attrs[i];
		var name = attr.name;
		var value = attr.value;
		attrList = _List_Cons( A2(_VirtualDom_attribute, name, value), attrList );
	}

	var tag = node.tagName.toLowerCase();
	var kidList = _List_Nil;
	var kids = node.childNodes;

	for (var i = kids.length; i--; )
	{
		kidList = _List_Cons(_VirtualDom_virtualize(kids[i]), kidList);
	}
	return A3(_VirtualDom_node, tag, attrList, kidList);
}

function _VirtualDom_dekey(keyedNode)
{
	var keyedKids = keyedNode.e;
	var len = keyedKids.length;
	var kids = new Array(len);
	for (var i = 0; i < len; i++)
	{
		kids[i] = keyedKids[i].b;
	}

	return {
		$: 1,
		c: keyedNode.c,
		d: keyedNode.d,
		e: kids,
		f: keyedNode.f,
		b: keyedNode.b
	};
}




// ELEMENT


var _Debugger_element;

var _Browser_element = _Debugger_element || F4(function(impl, flagDecoder, debugMetadata, args)
{
	return _Platform_initialize(
		flagDecoder,
		args,
		impl.init,
		impl.update,
		impl.subscriptions,
		function(sendToApp, initialModel) {
			var view = impl.view;
			/**_UNUSED/
			var domNode = args['node'];
			//*/
			/**/
			var domNode = args && args['node'] ? args['node'] : _Debug_crash(0);
			//*/
			var currNode = _VirtualDom_virtualize(domNode);

			return _Browser_makeAnimator(initialModel, function(model)
			{
				var nextNode = view(model);
				var patches = _VirtualDom_diff(currNode, nextNode);
				domNode = _VirtualDom_applyPatches(domNode, currNode, patches, sendToApp);
				currNode = nextNode;
			});
		}
	);
});



// DOCUMENT


var _Debugger_document;

var _Browser_document = _Debugger_document || F4(function(impl, flagDecoder, debugMetadata, args)
{
	return _Platform_initialize(
		flagDecoder,
		args,
		impl.init,
		impl.update,
		impl.subscriptions,
		function(sendToApp, initialModel) {
			var divertHrefToApp = impl.setup && impl.setup(sendToApp)
			var view = impl.view;
			var title = _VirtualDom_doc.title;
			var bodyNode = _VirtualDom_doc.body;
			var currNode = _VirtualDom_virtualize(bodyNode);
			return _Browser_makeAnimator(initialModel, function(model)
			{
				_VirtualDom_divertHrefToApp = divertHrefToApp;
				var doc = view(model);
				var nextNode = _VirtualDom_node('body')(_List_Nil)(doc.body);
				var patches = _VirtualDom_diff(currNode, nextNode);
				bodyNode = _VirtualDom_applyPatches(bodyNode, currNode, patches, sendToApp);
				currNode = nextNode;
				_VirtualDom_divertHrefToApp = 0;
				(title !== doc.title) && (_VirtualDom_doc.title = title = doc.title);
			});
		}
	);
});



// ANIMATION


var _Browser_cancelAnimationFrame =
	typeof cancelAnimationFrame !== 'undefined'
		? cancelAnimationFrame
		: function(id) { clearTimeout(id); };

var _Browser_requestAnimationFrame =
	typeof requestAnimationFrame !== 'undefined'
		? requestAnimationFrame
		: function(callback) { return setTimeout(callback, 1000 / 60); };


function _Browser_makeAnimator(model, draw)
{
	draw(model);

	var state = 0;

	function updateIfNeeded()
	{
		state = state === 1
			? 0
			: ( _Browser_requestAnimationFrame(updateIfNeeded), draw(model), 1 );
	}

	return function(nextModel, isSync)
	{
		model = nextModel;

		isSync
			? ( draw(model),
				state === 2 && (state = 1)
				)
			: ( state === 0 && _Browser_requestAnimationFrame(updateIfNeeded),
				state = 2
				);
	};
}



// APPLICATION


function _Browser_application(impl)
{
	var onUrlChange = impl.onUrlChange;
	var onUrlRequest = impl.onUrlRequest;
	var key = function() { key.a(onUrlChange(_Browser_getUrl())); };

	return _Browser_document({
		setup: function(sendToApp)
		{
			key.a = sendToApp;
			_Browser_window.addEventListener('popstate', key);
			_Browser_window.navigator.userAgent.indexOf('Trident') < 0 || _Browser_window.addEventListener('hashchange', key);

			return F2(function(domNode, event)
			{
				if (!event.ctrlKey && !event.metaKey && !event.shiftKey && event.button < 1 && !domNode.target && !domNode.hasAttribute('download'))
				{
					event.preventDefault();
					var href = domNode.href;
					var curr = _Browser_getUrl();
					var next = $elm$url$Url$fromString(href).a;
					sendToApp(onUrlRequest(
						(next
							&& curr.protocol === next.protocol
							&& curr.host === next.host
							&& curr.port_.a === next.port_.a
						)
							? $elm$browser$Browser$Internal(next)
							: $elm$browser$Browser$External(href)
					));
				}
			});
		},
		init: function(flags)
		{
			return A3(impl.init, flags, _Browser_getUrl(), key);
		},
		view: impl.view,
		update: impl.update,
		subscriptions: impl.subscriptions
	});
}

function _Browser_getUrl()
{
	return $elm$url$Url$fromString(_VirtualDom_doc.location.href).a || _Debug_crash(1);
}

var _Browser_go = F2(function(key, n)
{
	return A2($elm$core$Task$perform, $elm$core$Basics$never, _Scheduler_binding(function() {
		n && history.go(n);
		key();
	}));
});

var _Browser_pushUrl = F2(function(key, url)
{
	return A2($elm$core$Task$perform, $elm$core$Basics$never, _Scheduler_binding(function() {
		history.pushState({}, '', url);
		key();
	}));
});

var _Browser_replaceUrl = F2(function(key, url)
{
	return A2($elm$core$Task$perform, $elm$core$Basics$never, _Scheduler_binding(function() {
		history.replaceState({}, '', url);
		key();
	}));
});



// GLOBAL EVENTS


var _Browser_fakeNode = { addEventListener: function() {}, removeEventListener: function() {} };
var _Browser_doc = typeof document !== 'undefined' ? document : _Browser_fakeNode;
var _Browser_window = typeof window !== 'undefined' ? window : _Browser_fakeNode;

var _Browser_on = F3(function(node, eventName, sendToSelf)
{
	return _Scheduler_spawn(_Scheduler_binding(function(callback)
	{
		function handler(event)	{ _Scheduler_rawSpawn(sendToSelf(event)); }
		node.addEventListener(eventName, handler, _VirtualDom_passiveSupported && { passive: true });
		return function() { node.removeEventListener(eventName, handler); };
	}));
});

var _Browser_decodeEvent = F2(function(decoder, event)
{
	var result = _Json_runHelp(decoder, event);
	return $elm$core$Result$isOk(result) ? $elm$core$Maybe$Just(result.a) : $elm$core$Maybe$Nothing;
});



// PAGE VISIBILITY


function _Browser_visibilityInfo()
{
	return (typeof _VirtualDom_doc.hidden !== 'undefined')
		? { hidden: 'hidden', change: 'visibilitychange' }
		:
	(typeof _VirtualDom_doc.mozHidden !== 'undefined')
		? { hidden: 'mozHidden', change: 'mozvisibilitychange' }
		:
	(typeof _VirtualDom_doc.msHidden !== 'undefined')
		? { hidden: 'msHidden', change: 'msvisibilitychange' }
		:
	(typeof _VirtualDom_doc.webkitHidden !== 'undefined')
		? { hidden: 'webkitHidden', change: 'webkitvisibilitychange' }
		: { hidden: 'hidden', change: 'visibilitychange' };
}



// ANIMATION FRAMES


function _Browser_rAF()
{
	return _Scheduler_binding(function(callback)
	{
		var id = _Browser_requestAnimationFrame(function() {
			callback(_Scheduler_succeed(Date.now()));
		});

		return function() {
			_Browser_cancelAnimationFrame(id);
		};
	});
}


function _Browser_now()
{
	return _Scheduler_binding(function(callback)
	{
		callback(_Scheduler_succeed(Date.now()));
	});
}



// DOM STUFF


function _Browser_withNode(id, doStuff)
{
	return _Scheduler_binding(function(callback)
	{
		_Browser_requestAnimationFrame(function() {
			var node = document.getElementById(id);
			callback(node
				? _Scheduler_succeed(doStuff(node))
				: _Scheduler_fail($elm$browser$Browser$Dom$NotFound(id))
			);
		});
	});
}


function _Browser_withWindow(doStuff)
{
	return _Scheduler_binding(function(callback)
	{
		_Browser_requestAnimationFrame(function() {
			callback(_Scheduler_succeed(doStuff()));
		});
	});
}


// FOCUS and BLUR


var _Browser_call = F2(function(functionName, id)
{
	return _Browser_withNode(id, function(node) {
		node[functionName]();
		return _Utils_Tuple0;
	});
});



// WINDOW VIEWPORT


function _Browser_getViewport()
{
	return {
		scene: _Browser_getScene(),
		viewport: {
			x: _Browser_window.pageXOffset,
			y: _Browser_window.pageYOffset,
			width: _Browser_doc.documentElement.clientWidth,
			height: _Browser_doc.documentElement.clientHeight
		}
	};
}

function _Browser_getScene()
{
	var body = _Browser_doc.body;
	var elem = _Browser_doc.documentElement;
	return {
		width: Math.max(body.scrollWidth, body.offsetWidth, elem.scrollWidth, elem.offsetWidth, elem.clientWidth),
		height: Math.max(body.scrollHeight, body.offsetHeight, elem.scrollHeight, elem.offsetHeight, elem.clientHeight)
	};
}

var _Browser_setViewport = F2(function(x, y)
{
	return _Browser_withWindow(function()
	{
		_Browser_window.scroll(x, y);
		return _Utils_Tuple0;
	});
});



// ELEMENT VIEWPORT


function _Browser_getViewportOf(id)
{
	return _Browser_withNode(id, function(node)
	{
		return {
			scene: {
				width: node.scrollWidth,
				height: node.scrollHeight
			},
			viewport: {
				x: node.scrollLeft,
				y: node.scrollTop,
				width: node.clientWidth,
				height: node.clientHeight
			}
		};
	});
}


var _Browser_setViewportOf = F3(function(id, x, y)
{
	return _Browser_withNode(id, function(node)
	{
		node.scrollLeft = x;
		node.scrollTop = y;
		return _Utils_Tuple0;
	});
});



// ELEMENT


function _Browser_getElement(id)
{
	return _Browser_withNode(id, function(node)
	{
		var rect = node.getBoundingClientRect();
		var x = _Browser_window.pageXOffset;
		var y = _Browser_window.pageYOffset;
		return {
			scene: _Browser_getScene(),
			viewport: {
				x: x,
				y: y,
				width: _Browser_doc.documentElement.clientWidth,
				height: _Browser_doc.documentElement.clientHeight
			},
			element: {
				x: x + rect.left,
				y: y + rect.top,
				width: rect.width,
				height: rect.height
			}
		};
	});
}



// LOAD and RELOAD


function _Browser_reload(skipCache)
{
	return A2($elm$core$Task$perform, $elm$core$Basics$never, _Scheduler_binding(function(callback)
	{
		_VirtualDom_doc.location.reload(skipCache);
	}));
}

function _Browser_load(url)
{
	return A2($elm$core$Task$perform, $elm$core$Basics$never, _Scheduler_binding(function(callback)
	{
		try
		{
			_Browser_window.location = url;
		}
		catch(err)
		{
			// Only Firefox can throw a NS_ERROR_MALFORMED_URI exception here.
			// Other browsers reload the page, so let's be consistent about that.
			_VirtualDom_doc.location.reload(false);
		}
	}));
}



// SEND REQUEST

var _Http_toTask = F3(function(router, toTask, request)
{
	return _Scheduler_binding(function(callback)
	{
		function done(response) {
			callback(toTask(request.expect.a(response)));
		}

		var xhr = new XMLHttpRequest();
		xhr.addEventListener('error', function() { done($elm$http$Http$NetworkError_); });
		xhr.addEventListener('timeout', function() { done($elm$http$Http$Timeout_); });
		xhr.addEventListener('load', function() { done(_Http_toResponse(request.expect.b, xhr)); });
		$elm$core$Maybe$isJust(request.tracker) && _Http_track(router, xhr, request.tracker.a);

		try {
			xhr.open(request.method, request.url, true);
		} catch (e) {
			return done($elm$http$Http$BadUrl_(request.url));
		}

		_Http_configureRequest(xhr, request);

		request.body.a && xhr.setRequestHeader('Content-Type', request.body.a);
		xhr.send(request.body.b);

		return function() { xhr.c = true; xhr.abort(); };
	});
});


// CONFIGURE

function _Http_configureRequest(xhr, request)
{
	for (var headers = request.headers; headers.b; headers = headers.b) // WHILE_CONS
	{
		xhr.setRequestHeader(headers.a.a, headers.a.b);
	}
	xhr.timeout = request.timeout.a || 0;
	xhr.responseType = request.expect.d;
	xhr.withCredentials = request.allowCookiesFromOtherDomains;
}


// RESPONSES

function _Http_toResponse(toBody, xhr)
{
	return A2(
		200 <= xhr.status && xhr.status < 300 ? $elm$http$Http$GoodStatus_ : $elm$http$Http$BadStatus_,
		_Http_toMetadata(xhr),
		toBody(xhr.response)
	);
}


// METADATA

function _Http_toMetadata(xhr)
{
	return {
		url: xhr.responseURL,
		statusCode: xhr.status,
		statusText: xhr.statusText,
		headers: _Http_parseHeaders(xhr.getAllResponseHeaders())
	};
}


// HEADERS

function _Http_parseHeaders(rawHeaders)
{
	if (!rawHeaders)
	{
		return $elm$core$Dict$empty;
	}

	var headers = $elm$core$Dict$empty;
	var headerPairs = rawHeaders.split('\r\n');
	for (var i = headerPairs.length; i--; )
	{
		var headerPair = headerPairs[i];
		var index = headerPair.indexOf(': ');
		if (index > 0)
		{
			var key = headerPair.substring(0, index);
			var value = headerPair.substring(index + 2);

			headers = A3($elm$core$Dict$update, key, function(oldValue) {
				return $elm$core$Maybe$Just($elm$core$Maybe$isJust(oldValue)
					? value + ', ' + oldValue.a
					: value
				);
			}, headers);
		}
	}
	return headers;
}


// EXPECT

var _Http_expect = F3(function(type, toBody, toValue)
{
	return {
		$: 0,
		d: type,
		b: toBody,
		a: toValue
	};
});

var _Http_mapExpect = F2(function(func, expect)
{
	return {
		$: 0,
		d: expect.d,
		b: expect.b,
		a: function(x) { return func(expect.a(x)); }
	};
});

function _Http_toDataView(arrayBuffer)
{
	return new DataView(arrayBuffer);
}


// BODY and PARTS

var _Http_emptyBody = { $: 0 };
var _Http_pair = F2(function(a, b) { return { $: 0, a: a, b: b }; });

function _Http_toFormData(parts)
{
	for (var formData = new FormData(); parts.b; parts = parts.b) // WHILE_CONS
	{
		var part = parts.a;
		formData.append(part.a, part.b);
	}
	return formData;
}

var _Http_bytesToBlob = F2(function(mime, bytes)
{
	return new Blob([bytes], { type: mime });
});


// PROGRESS

function _Http_track(router, xhr, tracker)
{
	// TODO check out lengthComputable on loadstart event

	xhr.upload.addEventListener('progress', function(event) {
		if (xhr.c) { return; }
		_Scheduler_rawSpawn(A2($elm$core$Platform$sendToSelf, router, _Utils_Tuple2(tracker, $elm$http$Http$Sending({
			sent: event.loaded,
			size: event.total
		}))));
	});
	xhr.addEventListener('progress', function(event) {
		if (xhr.c) { return; }
		_Scheduler_rawSpawn(A2($elm$core$Platform$sendToSelf, router, _Utils_Tuple2(tracker, $elm$http$Http$Receiving({
			received: event.loaded,
			size: event.lengthComputable ? $elm$core$Maybe$Just(event.total) : $elm$core$Maybe$Nothing
		}))));
	});
}

function _Url_percentEncode(string)
{
	return encodeURIComponent(string);
}

function _Url_percentDecode(string)
{
	try
	{
		return $elm$core$Maybe$Just(decodeURIComponent(string));
	}
	catch (e)
	{
		return $elm$core$Maybe$Nothing;
	}
}


function _Time_now(millisToPosix)
{
	return _Scheduler_binding(function(callback)
	{
		callback(_Scheduler_succeed(millisToPosix(Date.now())));
	});
}

var _Time_setInterval = F2(function(interval, task)
{
	return _Scheduler_binding(function(callback)
	{
		var id = setInterval(function() { _Scheduler_rawSpawn(task); }, interval);
		return function() { clearInterval(id); };
	});
});

function _Time_here()
{
	return _Scheduler_binding(function(callback)
	{
		callback(_Scheduler_succeed(
			A2($elm$time$Time$customZone, -(new Date().getTimezoneOffset()), _List_Nil)
		));
	});
}


function _Time_getZoneName()
{
	return _Scheduler_binding(function(callback)
	{
		try
		{
			var name = $elm$time$Time$Name(Intl.DateTimeFormat().resolvedOptions().timeZone);
		}
		catch (e)
		{
			var name = $elm$time$Time$Offset(new Date().getTimezoneOffset());
		}
		callback(_Scheduler_succeed(name));
	});
}



var _Bitwise_and = F2(function(a, b)
{
	return a & b;
});

var _Bitwise_or = F2(function(a, b)
{
	return a | b;
});

var _Bitwise_xor = F2(function(a, b)
{
	return a ^ b;
});

function _Bitwise_complement(a)
{
	return ~a;
};

var _Bitwise_shiftLeftBy = F2(function(offset, a)
{
	return a << offset;
});

var _Bitwise_shiftRightBy = F2(function(offset, a)
{
	return a >> offset;
});

var _Bitwise_shiftRightZfBy = F2(function(offset, a)
{
	return a >>> offset;
});


// CREATE

var _Regex_never = /.^/;

var _Regex_fromStringWith = F2(function(options, string)
{
	var flags = 'g';
	if (options.multiline) { flags += 'm'; }
	if (options.caseInsensitive) { flags += 'i'; }

	try
	{
		return $elm$core$Maybe$Just(new RegExp(string, flags));
	}
	catch(error)
	{
		return $elm$core$Maybe$Nothing;
	}
});


// USE

var _Regex_contains = F2(function(re, string)
{
	return string.match(re) !== null;
});


var _Regex_findAtMost = F3(function(n, re, str)
{
	var out = [];
	var number = 0;
	var string = str;
	var lastIndex = re.lastIndex;
	var prevLastIndex = -1;
	var result;
	while (number++ < n && (result = re.exec(string)))
	{
		if (prevLastIndex == re.lastIndex) break;
		var i = result.length - 1;
		var subs = new Array(i);
		while (i > 0)
		{
			var submatch = result[i];
			subs[--i] = submatch
				? $elm$core$Maybe$Just(submatch)
				: $elm$core$Maybe$Nothing;
		}
		out.push(A4($elm$regex$Regex$Match, result[0], result.index, number, _List_fromArray(subs)));
		prevLastIndex = re.lastIndex;
	}
	re.lastIndex = lastIndex;
	return _List_fromArray(out);
});


var _Regex_replaceAtMost = F4(function(n, re, replacer, string)
{
	var count = 0;
	function jsReplacer(match)
	{
		if (count++ >= n)
		{
			return match;
		}
		var i = arguments.length - 3;
		var submatches = new Array(i);
		while (i > 0)
		{
			var submatch = arguments[i];
			submatches[--i] = submatch
				? $elm$core$Maybe$Just(submatch)
				: $elm$core$Maybe$Nothing;
		}
		return replacer(A4($elm$regex$Regex$Match, match, arguments[arguments.length - 2], count, _List_fromArray(submatches)));
	}
	return string.replace(re, jsReplacer);
});

var _Regex_splitAtMost = F3(function(n, re, str)
{
	var string = str;
	var out = [];
	var start = re.lastIndex;
	var restoreLastIndex = re.lastIndex;
	while (n--)
	{
		var result = re.exec(string);
		if (!result) break;
		out.push(string.slice(start, result.index));
		start = re.lastIndex;
	}
	out.push(string.slice(start));
	re.lastIndex = restoreLastIndex;
	return _List_fromArray(out);
});

var _Regex_infinity = Infinity;
var $elm$core$Basics$EQ = {$: 'EQ'};
var $elm$core$Basics$GT = {$: 'GT'};
var $elm$core$Basics$LT = {$: 'LT'};
var $elm$core$List$cons = _List_cons;
var $elm$core$Dict$foldr = F3(
	function (func, acc, t) {
		foldr:
		while (true) {
			if (t.$ === 'RBEmpty_elm_builtin') {
				return acc;
			} else {
				var key = t.b;
				var value = t.c;
				var left = t.d;
				var right = t.e;
				var $temp$func = func,
					$temp$acc = A3(
					func,
					key,
					value,
					A3($elm$core$Dict$foldr, func, acc, right)),
					$temp$t = left;
				func = $temp$func;
				acc = $temp$acc;
				t = $temp$t;
				continue foldr;
			}
		}
	});
var $elm$core$Dict$toList = function (dict) {
	return A3(
		$elm$core$Dict$foldr,
		F3(
			function (key, value, list) {
				return A2(
					$elm$core$List$cons,
					_Utils_Tuple2(key, value),
					list);
			}),
		_List_Nil,
		dict);
};
var $elm$core$Dict$keys = function (dict) {
	return A3(
		$elm$core$Dict$foldr,
		F3(
			function (key, value, keyList) {
				return A2($elm$core$List$cons, key, keyList);
			}),
		_List_Nil,
		dict);
};
var $elm$core$Set$toList = function (_v0) {
	var dict = _v0.a;
	return $elm$core$Dict$keys(dict);
};
var $elm$core$Elm$JsArray$foldr = _JsArray_foldr;
var $elm$core$Array$foldr = F3(
	function (func, baseCase, _v0) {
		var tree = _v0.c;
		var tail = _v0.d;
		var helper = F2(
			function (node, acc) {
				if (node.$ === 'SubTree') {
					var subTree = node.a;
					return A3($elm$core$Elm$JsArray$foldr, helper, acc, subTree);
				} else {
					var values = node.a;
					return A3($elm$core$Elm$JsArray$foldr, func, acc, values);
				}
			});
		return A3(
			$elm$core$Elm$JsArray$foldr,
			helper,
			A3($elm$core$Elm$JsArray$foldr, func, baseCase, tail),
			tree);
	});
var $elm$core$Array$toList = function (array) {
	return A3($elm$core$Array$foldr, $elm$core$List$cons, _List_Nil, array);
};
var $elm$core$Result$Err = function (a) {
	return {$: 'Err', a: a};
};
var $elm$json$Json$Decode$Failure = F2(
	function (a, b) {
		return {$: 'Failure', a: a, b: b};
	});
var $elm$json$Json$Decode$Field = F2(
	function (a, b) {
		return {$: 'Field', a: a, b: b};
	});
var $elm$json$Json$Decode$Index = F2(
	function (a, b) {
		return {$: 'Index', a: a, b: b};
	});
var $elm$core$Result$Ok = function (a) {
	return {$: 'Ok', a: a};
};
var $elm$json$Json$Decode$OneOf = function (a) {
	return {$: 'OneOf', a: a};
};
var $elm$core$Basics$False = {$: 'False'};
var $elm$core$Basics$add = _Basics_add;
var $elm$core$Maybe$Just = function (a) {
	return {$: 'Just', a: a};
};
var $elm$core$Maybe$Nothing = {$: 'Nothing'};
var $elm$core$String$all = _String_all;
var $elm$core$Basics$and = _Basics_and;
var $elm$core$Basics$append = _Utils_append;
var $elm$json$Json$Encode$encode = _Json_encode;
var $elm$core$String$fromInt = _String_fromNumber;
var $elm$core$String$join = F2(
	function (sep, chunks) {
		return A2(
			_String_join,
			sep,
			_List_toArray(chunks));
	});
var $elm$core$String$split = F2(
	function (sep, string) {
		return _List_fromArray(
			A2(_String_split, sep, string));
	});
var $elm$json$Json$Decode$indent = function (str) {
	return A2(
		$elm$core$String$join,
		'\n    ',
		A2($elm$core$String$split, '\n', str));
};
var $elm$core$List$foldl = F3(
	function (func, acc, list) {
		foldl:
		while (true) {
			if (!list.b) {
				return acc;
			} else {
				var x = list.a;
				var xs = list.b;
				var $temp$func = func,
					$temp$acc = A2(func, x, acc),
					$temp$list = xs;
				func = $temp$func;
				acc = $temp$acc;
				list = $temp$list;
				continue foldl;
			}
		}
	});
var $elm$core$List$length = function (xs) {
	return A3(
		$elm$core$List$foldl,
		F2(
			function (_v0, i) {
				return i + 1;
			}),
		0,
		xs);
};
var $elm$core$List$map2 = _List_map2;
var $elm$core$Basics$le = _Utils_le;
var $elm$core$Basics$sub = _Basics_sub;
var $elm$core$List$rangeHelp = F3(
	function (lo, hi, list) {
		rangeHelp:
		while (true) {
			if (_Utils_cmp(lo, hi) < 1) {
				var $temp$lo = lo,
					$temp$hi = hi - 1,
					$temp$list = A2($elm$core$List$cons, hi, list);
				lo = $temp$lo;
				hi = $temp$hi;
				list = $temp$list;
				continue rangeHelp;
			} else {
				return list;
			}
		}
	});
var $elm$core$List$range = F2(
	function (lo, hi) {
		return A3($elm$core$List$rangeHelp, lo, hi, _List_Nil);
	});
var $elm$core$List$indexedMap = F2(
	function (f, xs) {
		return A3(
			$elm$core$List$map2,
			f,
			A2(
				$elm$core$List$range,
				0,
				$elm$core$List$length(xs) - 1),
			xs);
	});
var $elm$core$Char$toCode = _Char_toCode;
var $elm$core$Char$isLower = function (_char) {
	var code = $elm$core$Char$toCode(_char);
	return (97 <= code) && (code <= 122);
};
var $elm$core$Char$isUpper = function (_char) {
	var code = $elm$core$Char$toCode(_char);
	return (code <= 90) && (65 <= code);
};
var $elm$core$Basics$or = _Basics_or;
var $elm$core$Char$isAlpha = function (_char) {
	return $elm$core$Char$isLower(_char) || $elm$core$Char$isUpper(_char);
};
var $elm$core$Char$isDigit = function (_char) {
	var code = $elm$core$Char$toCode(_char);
	return (code <= 57) && (48 <= code);
};
var $elm$core$Char$isAlphaNum = function (_char) {
	return $elm$core$Char$isLower(_char) || ($elm$core$Char$isUpper(_char) || $elm$core$Char$isDigit(_char));
};
var $elm$core$List$reverse = function (list) {
	return A3($elm$core$List$foldl, $elm$core$List$cons, _List_Nil, list);
};
var $elm$core$String$uncons = _String_uncons;
var $elm$json$Json$Decode$errorOneOf = F2(
	function (i, error) {
		return '\n\n(' + ($elm$core$String$fromInt(i + 1) + (') ' + $elm$json$Json$Decode$indent(
			$elm$json$Json$Decode$errorToString(error))));
	});
var $elm$json$Json$Decode$errorToString = function (error) {
	return A2($elm$json$Json$Decode$errorToStringHelp, error, _List_Nil);
};
var $elm$json$Json$Decode$errorToStringHelp = F2(
	function (error, context) {
		errorToStringHelp:
		while (true) {
			switch (error.$) {
				case 'Field':
					var f = error.a;
					var err = error.b;
					var isSimple = function () {
						var _v1 = $elm$core$String$uncons(f);
						if (_v1.$ === 'Nothing') {
							return false;
						} else {
							var _v2 = _v1.a;
							var _char = _v2.a;
							var rest = _v2.b;
							return $elm$core$Char$isAlpha(_char) && A2($elm$core$String$all, $elm$core$Char$isAlphaNum, rest);
						}
					}();
					var fieldName = isSimple ? ('.' + f) : ('[\'' + (f + '\']'));
					var $temp$error = err,
						$temp$context = A2($elm$core$List$cons, fieldName, context);
					error = $temp$error;
					context = $temp$context;
					continue errorToStringHelp;
				case 'Index':
					var i = error.a;
					var err = error.b;
					var indexName = '[' + ($elm$core$String$fromInt(i) + ']');
					var $temp$error = err,
						$temp$context = A2($elm$core$List$cons, indexName, context);
					error = $temp$error;
					context = $temp$context;
					continue errorToStringHelp;
				case 'OneOf':
					var errors = error.a;
					if (!errors.b) {
						return 'Ran into a Json.Decode.oneOf with no possibilities' + function () {
							if (!context.b) {
								return '!';
							} else {
								return ' at json' + A2(
									$elm$core$String$join,
									'',
									$elm$core$List$reverse(context));
							}
						}();
					} else {
						if (!errors.b.b) {
							var err = errors.a;
							var $temp$error = err,
								$temp$context = context;
							error = $temp$error;
							context = $temp$context;
							continue errorToStringHelp;
						} else {
							var starter = function () {
								if (!context.b) {
									return 'Json.Decode.oneOf';
								} else {
									return 'The Json.Decode.oneOf at json' + A2(
										$elm$core$String$join,
										'',
										$elm$core$List$reverse(context));
								}
							}();
							var introduction = starter + (' failed in the following ' + ($elm$core$String$fromInt(
								$elm$core$List$length(errors)) + ' ways:'));
							return A2(
								$elm$core$String$join,
								'\n\n',
								A2(
									$elm$core$List$cons,
									introduction,
									A2($elm$core$List$indexedMap, $elm$json$Json$Decode$errorOneOf, errors)));
						}
					}
				default:
					var msg = error.a;
					var json = error.b;
					var introduction = function () {
						if (!context.b) {
							return 'Problem with the given value:\n\n';
						} else {
							return 'Problem with the value at json' + (A2(
								$elm$core$String$join,
								'',
								$elm$core$List$reverse(context)) + ':\n\n    ');
						}
					}();
					return introduction + ($elm$json$Json$Decode$indent(
						A2($elm$json$Json$Encode$encode, 4, json)) + ('\n\n' + msg));
			}
		}
	});
var $elm$core$Array$branchFactor = 32;
var $elm$core$Array$Array_elm_builtin = F4(
	function (a, b, c, d) {
		return {$: 'Array_elm_builtin', a: a, b: b, c: c, d: d};
	});
var $elm$core$Elm$JsArray$empty = _JsArray_empty;
var $elm$core$Basics$ceiling = _Basics_ceiling;
var $elm$core$Basics$fdiv = _Basics_fdiv;
var $elm$core$Basics$logBase = F2(
	function (base, number) {
		return _Basics_log(number) / _Basics_log(base);
	});
var $elm$core$Basics$toFloat = _Basics_toFloat;
var $elm$core$Array$shiftStep = $elm$core$Basics$ceiling(
	A2($elm$core$Basics$logBase, 2, $elm$core$Array$branchFactor));
var $elm$core$Array$empty = A4($elm$core$Array$Array_elm_builtin, 0, $elm$core$Array$shiftStep, $elm$core$Elm$JsArray$empty, $elm$core$Elm$JsArray$empty);
var $elm$core$Elm$JsArray$initialize = _JsArray_initialize;
var $elm$core$Array$Leaf = function (a) {
	return {$: 'Leaf', a: a};
};
var $elm$core$Basics$apL = F2(
	function (f, x) {
		return f(x);
	});
var $elm$core$Basics$apR = F2(
	function (x, f) {
		return f(x);
	});
var $elm$core$Basics$eq = _Utils_equal;
var $elm$core$Basics$floor = _Basics_floor;
var $elm$core$Elm$JsArray$length = _JsArray_length;
var $elm$core$Basics$gt = _Utils_gt;
var $elm$core$Basics$max = F2(
	function (x, y) {
		return (_Utils_cmp(x, y) > 0) ? x : y;
	});
var $elm$core$Basics$mul = _Basics_mul;
var $elm$core$Array$SubTree = function (a) {
	return {$: 'SubTree', a: a};
};
var $elm$core$Elm$JsArray$initializeFromList = _JsArray_initializeFromList;
var $elm$core$Array$compressNodes = F2(
	function (nodes, acc) {
		compressNodes:
		while (true) {
			var _v0 = A2($elm$core$Elm$JsArray$initializeFromList, $elm$core$Array$branchFactor, nodes);
			var node = _v0.a;
			var remainingNodes = _v0.b;
			var newAcc = A2(
				$elm$core$List$cons,
				$elm$core$Array$SubTree(node),
				acc);
			if (!remainingNodes.b) {
				return $elm$core$List$reverse(newAcc);
			} else {
				var $temp$nodes = remainingNodes,
					$temp$acc = newAcc;
				nodes = $temp$nodes;
				acc = $temp$acc;
				continue compressNodes;
			}
		}
	});
var $elm$core$Tuple$first = function (_v0) {
	var x = _v0.a;
	return x;
};
var $elm$core$Array$treeFromBuilder = F2(
	function (nodeList, nodeListSize) {
		treeFromBuilder:
		while (true) {
			var newNodeSize = $elm$core$Basics$ceiling(nodeListSize / $elm$core$Array$branchFactor);
			if (newNodeSize === 1) {
				return A2($elm$core$Elm$JsArray$initializeFromList, $elm$core$Array$branchFactor, nodeList).a;
			} else {
				var $temp$nodeList = A2($elm$core$Array$compressNodes, nodeList, _List_Nil),
					$temp$nodeListSize = newNodeSize;
				nodeList = $temp$nodeList;
				nodeListSize = $temp$nodeListSize;
				continue treeFromBuilder;
			}
		}
	});
var $elm$core$Array$builderToArray = F2(
	function (reverseNodeList, builder) {
		if (!builder.nodeListSize) {
			return A4(
				$elm$core$Array$Array_elm_builtin,
				$elm$core$Elm$JsArray$length(builder.tail),
				$elm$core$Array$shiftStep,
				$elm$core$Elm$JsArray$empty,
				builder.tail);
		} else {
			var treeLen = builder.nodeListSize * $elm$core$Array$branchFactor;
			var depth = $elm$core$Basics$floor(
				A2($elm$core$Basics$logBase, $elm$core$Array$branchFactor, treeLen - 1));
			var correctNodeList = reverseNodeList ? $elm$core$List$reverse(builder.nodeList) : builder.nodeList;
			var tree = A2($elm$core$Array$treeFromBuilder, correctNodeList, builder.nodeListSize);
			return A4(
				$elm$core$Array$Array_elm_builtin,
				$elm$core$Elm$JsArray$length(builder.tail) + treeLen,
				A2($elm$core$Basics$max, 5, depth * $elm$core$Array$shiftStep),
				tree,
				builder.tail);
		}
	});
var $elm$core$Basics$idiv = _Basics_idiv;
var $elm$core$Basics$lt = _Utils_lt;
var $elm$core$Array$initializeHelp = F5(
	function (fn, fromIndex, len, nodeList, tail) {
		initializeHelp:
		while (true) {
			if (fromIndex < 0) {
				return A2(
					$elm$core$Array$builderToArray,
					false,
					{nodeList: nodeList, nodeListSize: (len / $elm$core$Array$branchFactor) | 0, tail: tail});
			} else {
				var leaf = $elm$core$Array$Leaf(
					A3($elm$core$Elm$JsArray$initialize, $elm$core$Array$branchFactor, fromIndex, fn));
				var $temp$fn = fn,
					$temp$fromIndex = fromIndex - $elm$core$Array$branchFactor,
					$temp$len = len,
					$temp$nodeList = A2($elm$core$List$cons, leaf, nodeList),
					$temp$tail = tail;
				fn = $temp$fn;
				fromIndex = $temp$fromIndex;
				len = $temp$len;
				nodeList = $temp$nodeList;
				tail = $temp$tail;
				continue initializeHelp;
			}
		}
	});
var $elm$core$Basics$remainderBy = _Basics_remainderBy;
var $elm$core$Array$initialize = F2(
	function (len, fn) {
		if (len <= 0) {
			return $elm$core$Array$empty;
		} else {
			var tailLen = len % $elm$core$Array$branchFactor;
			var tail = A3($elm$core$Elm$JsArray$initialize, tailLen, len - tailLen, fn);
			var initialFromIndex = (len - tailLen) - $elm$core$Array$branchFactor;
			return A5($elm$core$Array$initializeHelp, fn, initialFromIndex, len, _List_Nil, tail);
		}
	});
var $elm$core$Basics$True = {$: 'True'};
var $elm$core$Result$isOk = function (result) {
	if (result.$ === 'Ok') {
		return true;
	} else {
		return false;
	}
};
var $elm$json$Json$Decode$map = _Json_map1;
var $elm$json$Json$Decode$map2 = _Json_map2;
var $elm$json$Json$Decode$succeed = _Json_succeed;
var $elm$virtual_dom$VirtualDom$toHandlerInt = function (handler) {
	switch (handler.$) {
		case 'Normal':
			return 0;
		case 'MayStopPropagation':
			return 1;
		case 'MayPreventDefault':
			return 2;
		default:
			return 3;
	}
};
var $elm$browser$Browser$External = function (a) {
	return {$: 'External', a: a};
};
var $elm$browser$Browser$Internal = function (a) {
	return {$: 'Internal', a: a};
};
var $elm$core$Basics$identity = function (x) {
	return x;
};
var $elm$browser$Browser$Dom$NotFound = function (a) {
	return {$: 'NotFound', a: a};
};
var $elm$url$Url$Http = {$: 'Http'};
var $elm$url$Url$Https = {$: 'Https'};
var $elm$url$Url$Url = F6(
	function (protocol, host, port_, path, query, fragment) {
		return {fragment: fragment, host: host, path: path, port_: port_, protocol: protocol, query: query};
	});
var $elm$core$String$contains = _String_contains;
var $elm$core$String$length = _String_length;
var $elm$core$String$slice = _String_slice;
var $elm$core$String$dropLeft = F2(
	function (n, string) {
		return (n < 1) ? string : A3(
			$elm$core$String$slice,
			n,
			$elm$core$String$length(string),
			string);
	});
var $elm$core$String$indexes = _String_indexes;
var $elm$core$String$isEmpty = function (string) {
	return string === '';
};
var $elm$core$String$left = F2(
	function (n, string) {
		return (n < 1) ? '' : A3($elm$core$String$slice, 0, n, string);
	});
var $elm$core$String$toInt = _String_toInt;
var $elm$url$Url$chompBeforePath = F5(
	function (protocol, path, params, frag, str) {
		if ($elm$core$String$isEmpty(str) || A2($elm$core$String$contains, '@', str)) {
			return $elm$core$Maybe$Nothing;
		} else {
			var _v0 = A2($elm$core$String$indexes, ':', str);
			if (!_v0.b) {
				return $elm$core$Maybe$Just(
					A6($elm$url$Url$Url, protocol, str, $elm$core$Maybe$Nothing, path, params, frag));
			} else {
				if (!_v0.b.b) {
					var i = _v0.a;
					var _v1 = $elm$core$String$toInt(
						A2($elm$core$String$dropLeft, i + 1, str));
					if (_v1.$ === 'Nothing') {
						return $elm$core$Maybe$Nothing;
					} else {
						var port_ = _v1;
						return $elm$core$Maybe$Just(
							A6(
								$elm$url$Url$Url,
								protocol,
								A2($elm$core$String$left, i, str),
								port_,
								path,
								params,
								frag));
					}
				} else {
					return $elm$core$Maybe$Nothing;
				}
			}
		}
	});
var $elm$url$Url$chompBeforeQuery = F4(
	function (protocol, params, frag, str) {
		if ($elm$core$String$isEmpty(str)) {
			return $elm$core$Maybe$Nothing;
		} else {
			var _v0 = A2($elm$core$String$indexes, '/', str);
			if (!_v0.b) {
				return A5($elm$url$Url$chompBeforePath, protocol, '/', params, frag, str);
			} else {
				var i = _v0.a;
				return A5(
					$elm$url$Url$chompBeforePath,
					protocol,
					A2($elm$core$String$dropLeft, i, str),
					params,
					frag,
					A2($elm$core$String$left, i, str));
			}
		}
	});
var $elm$url$Url$chompBeforeFragment = F3(
	function (protocol, frag, str) {
		if ($elm$core$String$isEmpty(str)) {
			return $elm$core$Maybe$Nothing;
		} else {
			var _v0 = A2($elm$core$String$indexes, '?', str);
			if (!_v0.b) {
				return A4($elm$url$Url$chompBeforeQuery, protocol, $elm$core$Maybe$Nothing, frag, str);
			} else {
				var i = _v0.a;
				return A4(
					$elm$url$Url$chompBeforeQuery,
					protocol,
					$elm$core$Maybe$Just(
						A2($elm$core$String$dropLeft, i + 1, str)),
					frag,
					A2($elm$core$String$left, i, str));
			}
		}
	});
var $elm$url$Url$chompAfterProtocol = F2(
	function (protocol, str) {
		if ($elm$core$String$isEmpty(str)) {
			return $elm$core$Maybe$Nothing;
		} else {
			var _v0 = A2($elm$core$String$indexes, '#', str);
			if (!_v0.b) {
				return A3($elm$url$Url$chompBeforeFragment, protocol, $elm$core$Maybe$Nothing, str);
			} else {
				var i = _v0.a;
				return A3(
					$elm$url$Url$chompBeforeFragment,
					protocol,
					$elm$core$Maybe$Just(
						A2($elm$core$String$dropLeft, i + 1, str)),
					A2($elm$core$String$left, i, str));
			}
		}
	});
var $elm$core$String$startsWith = _String_startsWith;
var $elm$url$Url$fromString = function (str) {
	return A2($elm$core$String$startsWith, 'http://', str) ? A2(
		$elm$url$Url$chompAfterProtocol,
		$elm$url$Url$Http,
		A2($elm$core$String$dropLeft, 7, str)) : (A2($elm$core$String$startsWith, 'https://', str) ? A2(
		$elm$url$Url$chompAfterProtocol,
		$elm$url$Url$Https,
		A2($elm$core$String$dropLeft, 8, str)) : $elm$core$Maybe$Nothing);
};
var $elm$core$Basics$never = function (_v0) {
	never:
	while (true) {
		var nvr = _v0.a;
		var $temp$_v0 = nvr;
		_v0 = $temp$_v0;
		continue never;
	}
};
var $elm$core$Task$Perform = function (a) {
	return {$: 'Perform', a: a};
};
var $elm$core$Task$succeed = _Scheduler_succeed;
var $elm$core$Task$init = $elm$core$Task$succeed(_Utils_Tuple0);
var $elm$core$List$foldrHelper = F4(
	function (fn, acc, ctr, ls) {
		if (!ls.b) {
			return acc;
		} else {
			var a = ls.a;
			var r1 = ls.b;
			if (!r1.b) {
				return A2(fn, a, acc);
			} else {
				var b = r1.a;
				var r2 = r1.b;
				if (!r2.b) {
					return A2(
						fn,
						a,
						A2(fn, b, acc));
				} else {
					var c = r2.a;
					var r3 = r2.b;
					if (!r3.b) {
						return A2(
							fn,
							a,
							A2(
								fn,
								b,
								A2(fn, c, acc)));
					} else {
						var d = r3.a;
						var r4 = r3.b;
						var res = (ctr > 500) ? A3(
							$elm$core$List$foldl,
							fn,
							acc,
							$elm$core$List$reverse(r4)) : A4($elm$core$List$foldrHelper, fn, acc, ctr + 1, r4);
						return A2(
							fn,
							a,
							A2(
								fn,
								b,
								A2(
									fn,
									c,
									A2(fn, d, res))));
					}
				}
			}
		}
	});
var $elm$core$List$foldr = F3(
	function (fn, acc, ls) {
		return A4($elm$core$List$foldrHelper, fn, acc, 0, ls);
	});
var $elm$core$List$map = F2(
	function (f, xs) {
		return A3(
			$elm$core$List$foldr,
			F2(
				function (x, acc) {
					return A2(
						$elm$core$List$cons,
						f(x),
						acc);
				}),
			_List_Nil,
			xs);
	});
var $elm$core$Task$andThen = _Scheduler_andThen;
var $elm$core$Task$map = F2(
	function (func, taskA) {
		return A2(
			$elm$core$Task$andThen,
			function (a) {
				return $elm$core$Task$succeed(
					func(a));
			},
			taskA);
	});
var $elm$core$Task$map2 = F3(
	function (func, taskA, taskB) {
		return A2(
			$elm$core$Task$andThen,
			function (a) {
				return A2(
					$elm$core$Task$andThen,
					function (b) {
						return $elm$core$Task$succeed(
							A2(func, a, b));
					},
					taskB);
			},
			taskA);
	});
var $elm$core$Task$sequence = function (tasks) {
	return A3(
		$elm$core$List$foldr,
		$elm$core$Task$map2($elm$core$List$cons),
		$elm$core$Task$succeed(_List_Nil),
		tasks);
};
var $elm$core$Platform$sendToApp = _Platform_sendToApp;
var $elm$core$Task$spawnCmd = F2(
	function (router, _v0) {
		var task = _v0.a;
		return _Scheduler_spawn(
			A2(
				$elm$core$Task$andThen,
				$elm$core$Platform$sendToApp(router),
				task));
	});
var $elm$core$Task$onEffects = F3(
	function (router, commands, state) {
		return A2(
			$elm$core$Task$map,
			function (_v0) {
				return _Utils_Tuple0;
			},
			$elm$core$Task$sequence(
				A2(
					$elm$core$List$map,
					$elm$core$Task$spawnCmd(router),
					commands)));
	});
var $elm$core$Task$onSelfMsg = F3(
	function (_v0, _v1, _v2) {
		return $elm$core$Task$succeed(_Utils_Tuple0);
	});
var $elm$core$Task$cmdMap = F2(
	function (tagger, _v0) {
		var task = _v0.a;
		return $elm$core$Task$Perform(
			A2($elm$core$Task$map, tagger, task));
	});
_Platform_effectManagers['Task'] = _Platform_createManager($elm$core$Task$init, $elm$core$Task$onEffects, $elm$core$Task$onSelfMsg, $elm$core$Task$cmdMap);
var $elm$core$Task$command = _Platform_leaf('Task');
var $elm$core$Task$perform = F2(
	function (toMessage, task) {
		return $elm$core$Task$command(
			$elm$core$Task$Perform(
				A2($elm$core$Task$map, toMessage, task)));
	});
var $elm$browser$Browser$document = _Browser_document;
var $author$project$Main$Connecting = {$: 'Connecting'};
var $author$project$Main$Errored = F2(
	function (a, b) {
		return {$: 'Errored', a: a, b: b};
	});
var $author$project$Main$MakeNew = {$: 'MakeNew'};
var $author$project$Main$ReceivedAbout = function (a) {
	return {$: 'ReceivedAbout', a: a};
};
var $author$project$Main$ReceivedMotif = function (a) {
	return {$: 'ReceivedMotif', a: a};
};
var $author$project$Main$Starting = function (a) {
	return {$: 'Starting', a: a};
};
var $author$project$Main$UseExisting = function (a) {
	return {$: 'UseExisting', a: a};
};
var $elm$core$Platform$Cmd$batch = _Platform_batch;
var $author$project$Static$About$Model = function (a) {
	return {$: 'Model', a: a};
};
var $author$project$Static$About$Msg = function (a) {
	return {$: 'Msg', a: a};
};
var $author$project$Static$About$About = F2(
	function (mechane, world) {
		return {mechane: mechane, world: world};
	});
var $author$project$Static$About$Mechane = function (author) {
	return function (blurb) {
		return function (link) {
			return function (name) {
				return function (license) {
					return function (alternativeLicense) {
						return function (binaries) {
							return function (devBinaries) {
								return function (fonts) {
									return function (elmImports) {
										return function (goImports) {
											return function (tsImports) {
												return function (tsDevImports) {
													return {alternativeLicense: alternativeLicense, author: author, binaries: binaries, blurb: blurb, devBinaries: devBinaries, elmImports: elmImports, fonts: fonts, goImports: goImports, license: license, link: link, name: name, tsDevImports: tsDevImports, tsImports: tsImports};
												};
											};
										};
									};
								};
							};
						};
					};
				};
			};
		};
	};
};
var $elm$json$Json$Decode$andThen = _Json_andThen;
var $author$project$Static$About$Author = F2(
	function (name, url) {
		return {name: name, url: url};
	});
var $elm$json$Json$Decode$field = _Json_decodeField;
var $elm$json$Json$Decode$list = _Json_decodeList;
var $elm$json$Json$Decode$string = _Json_decodeString;
var $author$project$Static$About$authorDecoder = $elm$json$Json$Decode$list(
	A3(
		$elm$json$Json$Decode$map2,
		$author$project$Static$About$Author,
		A2($elm$json$Json$Decode$field, 'name', $elm$json$Json$Decode$string),
		A2($elm$json$Json$Decode$field, 'url', $elm$json$Json$Decode$string)));
var $author$project$Static$About$Dependency = F4(
	function (license, licensor, name, url) {
		return {license: license, licensor: licensor, name: name, url: url};
	});
var $author$project$Static$About$License = F5(
	function (badge, id, name, text, url) {
		return {badge: badge, id: id, name: name, text: text, url: url};
	});
var $elm$json$Json$Decode$map5 = _Json_map5;
var $author$project$Static$About$licenseDecoder = A6(
	$elm$json$Json$Decode$map5,
	$author$project$Static$About$License,
	A2($elm$json$Json$Decode$field, 'badge', $elm$json$Json$Decode$string),
	A2($elm$json$Json$Decode$field, 'id', $elm$json$Json$Decode$string),
	A2($elm$json$Json$Decode$field, 'name', $elm$json$Json$Decode$string),
	A2($elm$json$Json$Decode$field, 'text', $elm$json$Json$Decode$string),
	A2($elm$json$Json$Decode$field, 'url', $elm$json$Json$Decode$string));
var $elm$json$Json$Decode$map4 = _Json_map4;
var $author$project$Static$About$depsDecoder = $elm$json$Json$Decode$list(
	A5(
		$elm$json$Json$Decode$map4,
		$author$project$Static$About$Dependency,
		A2($elm$json$Json$Decode$field, 'license', $author$project$Static$About$licenseDecoder),
		A2(
			$elm$json$Json$Decode$field,
			'licensor',
			$elm$json$Json$Decode$list($elm$json$Json$Decode$string)),
		A2($elm$json$Json$Decode$field, 'name', $elm$json$Json$Decode$string),
		A2($elm$json$Json$Decode$field, 'url', $elm$json$Json$Decode$string)));
var $NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$custom = $elm$json$Json$Decode$map2($elm$core$Basics$apR);
var $NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required = F3(
	function (key, valDecoder, decoder) {
		return A2(
			$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$custom,
			A2($elm$json$Json$Decode$field, key, valDecoder),
			decoder);
	});
var $elm$core$Result$map2 = F3(
	function (func, ra, rb) {
		if (ra.$ === 'Err') {
			var x = ra.a;
			return $elm$core$Result$Err(x);
		} else {
			var a = ra.a;
			if (rb.$ === 'Err') {
				var x = rb.a;
				return $elm$core$Result$Err(x);
			} else {
				var b = rb.a;
				return $elm$core$Result$Ok(
					A2(func, a, b));
			}
		}
	});
var $elm_community$result_extra$Result$Extra$combine = A2(
	$elm$core$List$foldr,
	$elm$core$Result$map2($elm$core$List$cons),
	$elm$core$Result$Ok(_List_Nil));
var $elm$core$List$append = F2(
	function (xs, ys) {
		if (!ys.b) {
			return xs;
		} else {
			return A3($elm$core$List$foldr, $elm$core$List$cons, ys, xs);
		}
	});
var $elm$core$List$concat = function (lists) {
	return A3($elm$core$List$foldr, $elm$core$List$append, _List_Nil, lists);
};
var $author$project$Static$About$depURLs = function (deps) {
	return $elm$core$List$concat(
		A2(
			$elm$core$List$map,
			function (dep) {
				return _List_fromArray(
					[dep.url, dep.license.url]);
			},
			deps));
};
var $elm$json$Json$Decode$fail = _Json_fail;
var $elm$core$List$any = F2(
	function (isOkay, list) {
		any:
		while (true) {
			if (!list.b) {
				return false;
			} else {
				var x = list.a;
				var xs = list.b;
				if (isOkay(x)) {
					return true;
				} else {
					var $temp$isOkay = isOkay,
						$temp$list = xs;
					isOkay = $temp$isOkay;
					list = $temp$list;
					continue any;
				}
			}
		}
	});
var $elm_community$result_extra$Result$Extra$error = function (result) {
	if (result.$ === 'Ok') {
		return $elm$core$Maybe$Nothing;
	} else {
		var err = result.a;
		return $elm$core$Maybe$Just(err);
	}
};
var $elm$core$List$maybeCons = F3(
	function (f, mx, xs) {
		var _v0 = f(mx);
		if (_v0.$ === 'Just') {
			var x = _v0.a;
			return A2($elm$core$List$cons, x, xs);
		} else {
			return xs;
		}
	});
var $elm$core$List$filterMap = F2(
	function (f, xs) {
		return A3(
			$elm$core$List$foldr,
			$elm$core$List$maybeCons(f),
			_List_Nil,
			xs);
	});
var $elm_community$result_extra$Result$Extra$isErr = function (x) {
	if (x.$ === 'Ok') {
		return false;
	} else {
		return true;
	}
};
var $elm$core$Result$toMaybe = function (result) {
	if (result.$ === 'Ok') {
		var v = result.a;
		return $elm$core$Maybe$Just(v);
	} else {
		return $elm$core$Maybe$Nothing;
	}
};
var $author$project$My$List$validate = F2(
	function (test, strings) {
		var results = A2($elm$core$List$map, test, strings);
		return A2($elm$core$List$any, $elm_community$result_extra$Result$Extra$isErr, results) ? $elm$core$Result$Err(
			A2($elm$core$List$filterMap, $elm_community$result_extra$Result$Extra$error, results)) : $elm$core$Result$Ok(
			A2($elm$core$List$filterMap, $elm$core$Result$toMaybe, results));
	});
var $author$project$Static$About$elmPackagePrefix = 'https://package.elm-lang.org/packages';
var $author$project$Static$About$validateElmURL = function (url) {
	if ($elm$core$String$isEmpty(url)) {
		return $elm$core$Result$Err('cannot parse empty Elm package URL');
	} else {
		var _v0 = $elm$url$Url$fromString(url);
		if (_v0.$ === 'Just') {
			var parsedURL = _v0.a;
			return A2($elm$core$String$startsWith, $author$project$Static$About$elmPackagePrefix, url) ? (($elm$core$List$length(
				A2($elm$core$String$split, '/', parsedURL.path)) === 4) ? $elm$core$Result$Ok(parsedURL) : $elm$core$Result$Err('unexpected Elm package URL path: ' + url)) : $elm$core$Result$Err('unexpected Elm package URL prefix: ' + url);
		} else {
			return $elm$core$Result$Err('cannot parse Elm package URL: ' + url);
		}
	}
};
var $elm$core$Basics$ge = _Utils_ge;
var $author$project$Static$About$goPackagePrefix = 'https://pkg.go.dev';
var $author$project$Static$About$validateGoURL = function (url) {
	if ($elm$core$String$isEmpty(url)) {
		return $elm$core$Result$Err('cannot parse empty Go package URL');
	} else {
		var minPathSegments = 3;
		var _v0 = $elm$url$Url$fromString(url);
		if (_v0.$ === 'Just') {
			var parsedURL = _v0.a;
			return A2($elm$core$String$startsWith, $author$project$Static$About$goPackagePrefix, url) ? ((_Utils_cmp(
				$elm$core$List$length(
					A2($elm$core$String$split, '/', parsedURL.path)),
				minPathSegments) > -1) ? $elm$core$Result$Ok(parsedURL) : $elm$core$Result$Err('unexpected Go package URL path: ' + url)) : $elm$core$Result$Err('unexpected Go package URL prefix: ' + url);
		} else {
			return $elm$core$Result$Err('cannot parse Go package URL: ' + url);
		}
	}
};
var $elm$core$Basics$neq = _Utils_notEqual;
var $author$project$Static$About$validateURL = function (url) {
	if ($elm$core$String$isEmpty(url)) {
		return $elm$core$Result$Err('cannot parse empty URL');
	} else {
		var _v0 = $elm$url$Url$fromString(url);
		if (_v0.$ === 'Just') {
			var parsedURL = _v0.a;
			return (!_Utils_eq(parsedURL.query, $elm$core$Maybe$Nothing)) ? $elm$core$Result$Err('unexpected query: ' + url) : $elm$core$Result$Ok(parsedURL);
		} else {
			return $elm$core$Result$Err('cannot parse URL: ' + url);
		}
	}
};
var $author$project$Static$About$validateMechane = function (about) {
	var importURLs = _Utils_ap(
		about.elmImports,
		_Utils_ap(
			about.goImports,
			_Utils_ap(about.tsImports, about.tsDevImports)));
	var depsURLs = $elm$core$List$concat(
		A2(
			$elm$core$List$map,
			$author$project$Static$About$depURLs,
			_List_fromArray(
				[about.binaries, about.devBinaries, about.fonts])));
	var authorURLs = A2(
		$elm$core$List$map,
		function (_v2) {
			var url = _v2.url;
			return url;
		},
		about.author);
	var _v0 = A2(
		$author$project$My$List$validate,
		$author$project$Static$About$validateURL,
		$elm$core$List$concat(
			_List_fromArray(
				[
					_List_fromArray(
					[about.link]),
					authorURLs,
					_List_fromArray(
					[about.license.url]),
					depsURLs,
					importURLs
				])));
	if (_v0.$ === 'Ok') {
		var _v1 = $elm_community$result_extra$Result$Extra$combine(
			_List_fromArray(
				[
					A2($author$project$My$List$validate, $author$project$Static$About$validateElmURL, about.elmImports),
					A2($author$project$My$List$validate, $author$project$Static$About$validateGoURL, about.goImports)
				]));
		if (_v1.$ === 'Ok') {
			return $elm$json$Json$Decode$succeed(about);
		} else {
			var errors = _v1.a;
			return $elm$json$Json$Decode$fail(
				A2($elm$core$String$join, '\n', errors));
		}
	} else {
		var errors = _v0.a;
		return $elm$json$Json$Decode$fail(
			A2($elm$core$String$join, '\n', errors));
	}
};
var $author$project$Static$About$aboutMechaneDecoder = A2(
	$elm$json$Json$Decode$andThen,
	$author$project$Static$About$validateMechane,
	A3(
		$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
		'tsDevImports',
		$elm$json$Json$Decode$list($elm$json$Json$Decode$string),
		A3(
			$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
			'tsImports',
			$elm$json$Json$Decode$list($elm$json$Json$Decode$string),
			A3(
				$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
				'goImports',
				$elm$json$Json$Decode$list($elm$json$Json$Decode$string),
				A3(
					$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
					'elmImports',
					$elm$json$Json$Decode$list($elm$json$Json$Decode$string),
					A3(
						$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
						'fonts',
						$author$project$Static$About$depsDecoder,
						A3(
							$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
							'devBinaries',
							$author$project$Static$About$depsDecoder,
							A3(
								$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
								'binaries',
								$author$project$Static$About$depsDecoder,
								A3(
									$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
									'alternativeLicense',
									$author$project$Static$About$licenseDecoder,
									A3(
										$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
										'license',
										$author$project$Static$About$licenseDecoder,
										A3(
											$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
											'name',
											$elm$json$Json$Decode$string,
											A3(
												$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
												'link',
												$elm$json$Json$Decode$string,
												A3(
													$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
													'blurb',
													$elm$json$Json$Decode$list($elm$json$Json$Decode$string),
													A3(
														$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
														'author',
														$author$project$Static$About$authorDecoder,
														$elm$json$Json$Decode$succeed($author$project$Static$About$Mechane)))))))))))))));
var $author$project$Static$About$World = F8(
	function (author, blurb, link, name, license, alternativeLicense, binaries, devBinaries) {
		return {alternativeLicense: alternativeLicense, author: author, binaries: binaries, blurb: blurb, devBinaries: devBinaries, license: license, link: link, name: name};
	});
var $elm$json$Json$Decode$at = F2(
	function (fields, decoder) {
		return A3($elm$core$List$foldr, $elm$json$Json$Decode$field, decoder, fields);
	});
var $elm$json$Json$Decode$decodeValue = _Json_run;
var $elm$json$Json$Decode$null = _Json_decodeNull;
var $elm$json$Json$Decode$oneOf = _Json_oneOf;
var $elm$json$Json$Decode$value = _Json_decodeValue;
var $NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$optionalDecoder = F3(
	function (path, valDecoder, fallback) {
		var nullOr = function (decoder) {
			return $elm$json$Json$Decode$oneOf(
				_List_fromArray(
					[
						decoder,
						$elm$json$Json$Decode$null(fallback)
					]));
		};
		var handleResult = function (input) {
			var _v0 = A2(
				$elm$json$Json$Decode$decodeValue,
				A2($elm$json$Json$Decode$at, path, $elm$json$Json$Decode$value),
				input);
			if (_v0.$ === 'Ok') {
				var rawValue = _v0.a;
				var _v1 = A2(
					$elm$json$Json$Decode$decodeValue,
					nullOr(valDecoder),
					rawValue);
				if (_v1.$ === 'Ok') {
					var finalResult = _v1.a;
					return $elm$json$Json$Decode$succeed(finalResult);
				} else {
					return A2(
						$elm$json$Json$Decode$at,
						path,
						nullOr(valDecoder));
				}
			} else {
				return $elm$json$Json$Decode$succeed(fallback);
			}
		};
		return A2($elm$json$Json$Decode$andThen, handleResult, $elm$json$Json$Decode$value);
	});
var $NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$optional = F4(
	function (key, valDecoder, fallback, decoder) {
		return A2(
			$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$custom,
			A3(
				$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$optionalDecoder,
				_List_fromArray(
					[key]),
				valDecoder,
				fallback),
			decoder);
	});
var $author$project$My$Extra$appendWhen = F2(
	function (prefix, potentialSuffixes) {
		return A3(
			$elm$core$List$foldl,
			F2(
				function (_v0, acc) {
					var ok = _v0.a;
					var suffix = _v0.b;
					return ok ? _Utils_ap(acc, suffix) : acc;
				}),
			prefix,
			potentialSuffixes);
	});
var $elm$core$List$isEmpty = function (xs) {
	if (!xs.b) {
		return true;
	} else {
		return false;
	}
};
var $elm$core$Basics$not = _Basics_not;
var $author$project$My$Extra$ternary = F3(
	function (ok, okValue, notOkValue) {
		return ok ? okValue : notOkValue;
	});
var $author$project$Static$About$validateAuthor = function (author) {
	if (A2(
		$elm$core$List$any,
		$elm$core$String$isEmpty,
		_List_fromArray(
			[author.name, author.url]))) {
		return $elm$core$Result$Err('empty field in author');
	} else {
		var _v0 = $author$project$Static$About$validateURL(author.url);
		if (_v0.$ === 'Ok') {
			return $elm$core$Result$Ok(_Utils_Tuple0);
		} else {
			var err = _v0.a;
			return $elm$core$Result$Err(err);
		}
	}
};
var $elm$core$Result$map = F2(
	function (func, ra) {
		if (ra.$ === 'Ok') {
			var a = ra.a;
			return $elm$core$Result$Ok(
				func(a));
		} else {
			var e = ra.a;
			return $elm$core$Result$Err(e);
		}
	});
var $author$project$Static$About$validateLicense = function (license) {
	if (A2(
		$elm$core$List$any,
		$elm$core$String$isEmpty,
		_List_fromArray(
			[license.id, license.name, license.text, license.url]))) {
		return $elm$core$Result$Err('empty field in license');
	} else {
		var _v0 = $author$project$Static$About$validateURL(license.url);
		if (_v0.$ === 'Ok') {
			return $elm$core$Result$Ok(_Utils_Tuple0);
		} else {
			var err = _v0.a;
			return $elm$core$Result$Err(err);
		}
	}
};
var $author$project$Static$About$validateDependency = function (dep) {
	var _v0 = $elm_community$result_extra$Result$Extra$combine(
		_List_fromArray(
			[
				$author$project$Static$About$validateLicense(dep.license),
				A3(
				$author$project$My$Extra$ternary,
				A2(
					$elm$core$List$any,
					function (s) {
						return !$elm$core$String$isEmpty(s);
					},
					dep.licensor),
				$elm$core$Result$Ok(_Utils_Tuple0),
				$elm$core$Result$Err('empty dependency licensor field')),
				A3(
				$author$project$My$Extra$ternary,
				!$elm$core$String$isEmpty(dep.name),
				$elm$core$Result$Ok(_Utils_Tuple0),
				$elm$core$Result$Err('empty dependency name field ')),
				A2(
				$elm$core$Result$map,
				function (_v1) {
					return _Utils_Tuple0;
				},
				$author$project$Static$About$validateURL(dep.url))
			]));
	if (_v0.$ === 'Ok') {
		return $elm$core$Result$Ok(_Utils_Tuple0);
	} else {
		var error = _v0.a;
		return $elm$core$Result$Err(error);
	}
};
var $elm$core$Result$mapError = F2(
	function (f, result) {
		if (result.$ === 'Ok') {
			var v = result.a;
			return $elm$core$Result$Ok(v);
		} else {
			var e = result.a;
			return $elm$core$Result$Err(
				f(e));
		}
	});
var $author$project$Static$About$validateList = F2(
	function (test, list) {
		return A2(
			$elm$core$Result$mapError,
			$elm$core$String$join('\n'),
			A2(
				$elm$core$Result$map,
				function (_v0) {
					return _Utils_Tuple0;
				},
				A2($author$project$My$List$validate, test, list)));
	});
var $author$project$Static$About$validateWorld = function (about) {
	var _v0 = $elm_community$result_extra$Result$Extra$combine(
		A2(
			$author$project$My$Extra$appendWhen,
			_List_fromArray(
				[
					A3(
					$author$project$My$Extra$ternary,
					!$elm$core$List$isEmpty(about.author),
					$elm$core$Result$Ok(_Utils_Tuple0),
					$elm$core$Result$Err('empty world author field')),
					A3(
					$author$project$My$Extra$ternary,
					A2(
						$elm$core$List$any,
						function (s) {
							return !$elm$core$String$isEmpty(s);
						},
						about.blurb),
					$elm$core$Result$Ok(_Utils_Tuple0),
					$elm$core$Result$Err('empty world blurb field')),
					A3(
					$author$project$My$Extra$ternary,
					!$elm$core$String$isEmpty(about.link),
					$elm$core$Result$Ok(_Utils_Tuple0),
					$elm$core$Result$Err('empty world link field ')),
					A3(
					$author$project$My$Extra$ternary,
					!$elm$core$String$isEmpty(about.name),
					$elm$core$Result$Ok(_Utils_Tuple0),
					$elm$core$Result$Err('empty world name field ')),
					A2($author$project$Static$About$validateList, $author$project$Static$About$validateAuthor, about.author),
					$author$project$Static$About$validateLicense(about.license)
				]),
			_List_fromArray(
				[
					_Utils_Tuple2(
					$elm$core$List$length(about.binaries) > 0,
					_List_fromArray(
						[
							A2($author$project$Static$About$validateList, $author$project$Static$About$validateDependency, about.binaries)
						])),
					_Utils_Tuple2(
					$elm$core$List$length(about.devBinaries) > 0,
					_List_fromArray(
						[
							A2($author$project$Static$About$validateList, $author$project$Static$About$validateDependency, about.devBinaries)
						]))
				])));
	if (_v0.$ === 'Ok') {
		var _v1 = $author$project$Static$About$validateURL(about.link);
		if (_v1.$ === 'Ok') {
			return $elm$json$Json$Decode$succeed(about);
		} else {
			var err = _v1.a;
			return $elm$json$Json$Decode$fail(err);
		}
	} else {
		var error = _v0.a;
		return $elm$json$Json$Decode$fail(error);
	}
};
var $author$project$Static$About$aboutWorldDecoder = A2(
	$elm$json$Json$Decode$andThen,
	$author$project$Static$About$validateWorld,
	A4(
		$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$optional,
		'devBinaries',
		$author$project$Static$About$depsDecoder,
		_List_Nil,
		A4(
			$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$optional,
			'binaries',
			$author$project$Static$About$depsDecoder,
			_List_Nil,
			A3(
				$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
				'alternativeLicense',
				$author$project$Static$About$licenseDecoder,
				A3(
					$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
					'license',
					$author$project$Static$About$licenseDecoder,
					A3(
						$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
						'name',
						$elm$json$Json$Decode$string,
						A3(
							$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
							'link',
							$elm$json$Json$Decode$string,
							A3(
								$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
								'blurb',
								$elm$json$Json$Decode$list($elm$json$Json$Decode$string),
								A3(
									$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
									'author',
									$author$project$Static$About$authorDecoder,
									$elm$json$Json$Decode$succeed($author$project$Static$About$World))))))))));
var $author$project$Static$About$aboutDecoder = A3(
	$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
	'world',
	$author$project$Static$About$aboutWorldDecoder,
	A3(
		$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
		'mechane',
		$author$project$Static$About$aboutMechaneDecoder,
		$elm$json$Json$Decode$succeed($author$project$Static$About$About)));
var $elm$json$Json$Decode$decodeString = _Json_runOnString;
var $elm$http$Http$BadStatus_ = F2(
	function (a, b) {
		return {$: 'BadStatus_', a: a, b: b};
	});
var $elm$http$Http$BadUrl_ = function (a) {
	return {$: 'BadUrl_', a: a};
};
var $elm$http$Http$GoodStatus_ = F2(
	function (a, b) {
		return {$: 'GoodStatus_', a: a, b: b};
	});
var $elm$http$Http$NetworkError_ = {$: 'NetworkError_'};
var $elm$http$Http$Receiving = function (a) {
	return {$: 'Receiving', a: a};
};
var $elm$http$Http$Sending = function (a) {
	return {$: 'Sending', a: a};
};
var $elm$http$Http$Timeout_ = {$: 'Timeout_'};
var $elm$core$Dict$RBEmpty_elm_builtin = {$: 'RBEmpty_elm_builtin'};
var $elm$core$Dict$empty = $elm$core$Dict$RBEmpty_elm_builtin;
var $elm$core$Maybe$isJust = function (maybe) {
	if (maybe.$ === 'Just') {
		return true;
	} else {
		return false;
	}
};
var $elm$core$Platform$sendToSelf = _Platform_sendToSelf;
var $elm$core$Basics$compare = _Utils_compare;
var $elm$core$Dict$get = F2(
	function (targetKey, dict) {
		get:
		while (true) {
			if (dict.$ === 'RBEmpty_elm_builtin') {
				return $elm$core$Maybe$Nothing;
			} else {
				var key = dict.b;
				var value = dict.c;
				var left = dict.d;
				var right = dict.e;
				var _v1 = A2($elm$core$Basics$compare, targetKey, key);
				switch (_v1.$) {
					case 'LT':
						var $temp$targetKey = targetKey,
							$temp$dict = left;
						targetKey = $temp$targetKey;
						dict = $temp$dict;
						continue get;
					case 'EQ':
						return $elm$core$Maybe$Just(value);
					default:
						var $temp$targetKey = targetKey,
							$temp$dict = right;
						targetKey = $temp$targetKey;
						dict = $temp$dict;
						continue get;
				}
			}
		}
	});
var $elm$core$Dict$Black = {$: 'Black'};
var $elm$core$Dict$RBNode_elm_builtin = F5(
	function (a, b, c, d, e) {
		return {$: 'RBNode_elm_builtin', a: a, b: b, c: c, d: d, e: e};
	});
var $elm$core$Dict$Red = {$: 'Red'};
var $elm$core$Dict$balance = F5(
	function (color, key, value, left, right) {
		if ((right.$ === 'RBNode_elm_builtin') && (right.a.$ === 'Red')) {
			var _v1 = right.a;
			var rK = right.b;
			var rV = right.c;
			var rLeft = right.d;
			var rRight = right.e;
			if ((left.$ === 'RBNode_elm_builtin') && (left.a.$ === 'Red')) {
				var _v3 = left.a;
				var lK = left.b;
				var lV = left.c;
				var lLeft = left.d;
				var lRight = left.e;
				return A5(
					$elm$core$Dict$RBNode_elm_builtin,
					$elm$core$Dict$Red,
					key,
					value,
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Black, lK, lV, lLeft, lRight),
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Black, rK, rV, rLeft, rRight));
			} else {
				return A5(
					$elm$core$Dict$RBNode_elm_builtin,
					color,
					rK,
					rV,
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, key, value, left, rLeft),
					rRight);
			}
		} else {
			if ((((left.$ === 'RBNode_elm_builtin') && (left.a.$ === 'Red')) && (left.d.$ === 'RBNode_elm_builtin')) && (left.d.a.$ === 'Red')) {
				var _v5 = left.a;
				var lK = left.b;
				var lV = left.c;
				var _v6 = left.d;
				var _v7 = _v6.a;
				var llK = _v6.b;
				var llV = _v6.c;
				var llLeft = _v6.d;
				var llRight = _v6.e;
				var lRight = left.e;
				return A5(
					$elm$core$Dict$RBNode_elm_builtin,
					$elm$core$Dict$Red,
					lK,
					lV,
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Black, llK, llV, llLeft, llRight),
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Black, key, value, lRight, right));
			} else {
				return A5($elm$core$Dict$RBNode_elm_builtin, color, key, value, left, right);
			}
		}
	});
var $elm$core$Dict$insertHelp = F3(
	function (key, value, dict) {
		if (dict.$ === 'RBEmpty_elm_builtin') {
			return A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, key, value, $elm$core$Dict$RBEmpty_elm_builtin, $elm$core$Dict$RBEmpty_elm_builtin);
		} else {
			var nColor = dict.a;
			var nKey = dict.b;
			var nValue = dict.c;
			var nLeft = dict.d;
			var nRight = dict.e;
			var _v1 = A2($elm$core$Basics$compare, key, nKey);
			switch (_v1.$) {
				case 'LT':
					return A5(
						$elm$core$Dict$balance,
						nColor,
						nKey,
						nValue,
						A3($elm$core$Dict$insertHelp, key, value, nLeft),
						nRight);
				case 'EQ':
					return A5($elm$core$Dict$RBNode_elm_builtin, nColor, nKey, value, nLeft, nRight);
				default:
					return A5(
						$elm$core$Dict$balance,
						nColor,
						nKey,
						nValue,
						nLeft,
						A3($elm$core$Dict$insertHelp, key, value, nRight));
			}
		}
	});
var $elm$core$Dict$insert = F3(
	function (key, value, dict) {
		var _v0 = A3($elm$core$Dict$insertHelp, key, value, dict);
		if ((_v0.$ === 'RBNode_elm_builtin') && (_v0.a.$ === 'Red')) {
			var _v1 = _v0.a;
			var k = _v0.b;
			var v = _v0.c;
			var l = _v0.d;
			var r = _v0.e;
			return A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Black, k, v, l, r);
		} else {
			var x = _v0;
			return x;
		}
	});
var $elm$core$Dict$getMin = function (dict) {
	getMin:
	while (true) {
		if ((dict.$ === 'RBNode_elm_builtin') && (dict.d.$ === 'RBNode_elm_builtin')) {
			var left = dict.d;
			var $temp$dict = left;
			dict = $temp$dict;
			continue getMin;
		} else {
			return dict;
		}
	}
};
var $elm$core$Dict$moveRedLeft = function (dict) {
	if (((dict.$ === 'RBNode_elm_builtin') && (dict.d.$ === 'RBNode_elm_builtin')) && (dict.e.$ === 'RBNode_elm_builtin')) {
		if ((dict.e.d.$ === 'RBNode_elm_builtin') && (dict.e.d.a.$ === 'Red')) {
			var clr = dict.a;
			var k = dict.b;
			var v = dict.c;
			var _v1 = dict.d;
			var lClr = _v1.a;
			var lK = _v1.b;
			var lV = _v1.c;
			var lLeft = _v1.d;
			var lRight = _v1.e;
			var _v2 = dict.e;
			var rClr = _v2.a;
			var rK = _v2.b;
			var rV = _v2.c;
			var rLeft = _v2.d;
			var _v3 = rLeft.a;
			var rlK = rLeft.b;
			var rlV = rLeft.c;
			var rlL = rLeft.d;
			var rlR = rLeft.e;
			var rRight = _v2.e;
			return A5(
				$elm$core$Dict$RBNode_elm_builtin,
				$elm$core$Dict$Red,
				rlK,
				rlV,
				A5(
					$elm$core$Dict$RBNode_elm_builtin,
					$elm$core$Dict$Black,
					k,
					v,
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, lK, lV, lLeft, lRight),
					rlL),
				A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Black, rK, rV, rlR, rRight));
		} else {
			var clr = dict.a;
			var k = dict.b;
			var v = dict.c;
			var _v4 = dict.d;
			var lClr = _v4.a;
			var lK = _v4.b;
			var lV = _v4.c;
			var lLeft = _v4.d;
			var lRight = _v4.e;
			var _v5 = dict.e;
			var rClr = _v5.a;
			var rK = _v5.b;
			var rV = _v5.c;
			var rLeft = _v5.d;
			var rRight = _v5.e;
			if (clr.$ === 'Black') {
				return A5(
					$elm$core$Dict$RBNode_elm_builtin,
					$elm$core$Dict$Black,
					k,
					v,
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, lK, lV, lLeft, lRight),
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, rK, rV, rLeft, rRight));
			} else {
				return A5(
					$elm$core$Dict$RBNode_elm_builtin,
					$elm$core$Dict$Black,
					k,
					v,
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, lK, lV, lLeft, lRight),
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, rK, rV, rLeft, rRight));
			}
		}
	} else {
		return dict;
	}
};
var $elm$core$Dict$moveRedRight = function (dict) {
	if (((dict.$ === 'RBNode_elm_builtin') && (dict.d.$ === 'RBNode_elm_builtin')) && (dict.e.$ === 'RBNode_elm_builtin')) {
		if ((dict.d.d.$ === 'RBNode_elm_builtin') && (dict.d.d.a.$ === 'Red')) {
			var clr = dict.a;
			var k = dict.b;
			var v = dict.c;
			var _v1 = dict.d;
			var lClr = _v1.a;
			var lK = _v1.b;
			var lV = _v1.c;
			var _v2 = _v1.d;
			var _v3 = _v2.a;
			var llK = _v2.b;
			var llV = _v2.c;
			var llLeft = _v2.d;
			var llRight = _v2.e;
			var lRight = _v1.e;
			var _v4 = dict.e;
			var rClr = _v4.a;
			var rK = _v4.b;
			var rV = _v4.c;
			var rLeft = _v4.d;
			var rRight = _v4.e;
			return A5(
				$elm$core$Dict$RBNode_elm_builtin,
				$elm$core$Dict$Red,
				lK,
				lV,
				A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Black, llK, llV, llLeft, llRight),
				A5(
					$elm$core$Dict$RBNode_elm_builtin,
					$elm$core$Dict$Black,
					k,
					v,
					lRight,
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, rK, rV, rLeft, rRight)));
		} else {
			var clr = dict.a;
			var k = dict.b;
			var v = dict.c;
			var _v5 = dict.d;
			var lClr = _v5.a;
			var lK = _v5.b;
			var lV = _v5.c;
			var lLeft = _v5.d;
			var lRight = _v5.e;
			var _v6 = dict.e;
			var rClr = _v6.a;
			var rK = _v6.b;
			var rV = _v6.c;
			var rLeft = _v6.d;
			var rRight = _v6.e;
			if (clr.$ === 'Black') {
				return A5(
					$elm$core$Dict$RBNode_elm_builtin,
					$elm$core$Dict$Black,
					k,
					v,
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, lK, lV, lLeft, lRight),
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, rK, rV, rLeft, rRight));
			} else {
				return A5(
					$elm$core$Dict$RBNode_elm_builtin,
					$elm$core$Dict$Black,
					k,
					v,
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, lK, lV, lLeft, lRight),
					A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, rK, rV, rLeft, rRight));
			}
		}
	} else {
		return dict;
	}
};
var $elm$core$Dict$removeHelpPrepEQGT = F7(
	function (targetKey, dict, color, key, value, left, right) {
		if ((left.$ === 'RBNode_elm_builtin') && (left.a.$ === 'Red')) {
			var _v1 = left.a;
			var lK = left.b;
			var lV = left.c;
			var lLeft = left.d;
			var lRight = left.e;
			return A5(
				$elm$core$Dict$RBNode_elm_builtin,
				color,
				lK,
				lV,
				lLeft,
				A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Red, key, value, lRight, right));
		} else {
			_v2$2:
			while (true) {
				if ((right.$ === 'RBNode_elm_builtin') && (right.a.$ === 'Black')) {
					if (right.d.$ === 'RBNode_elm_builtin') {
						if (right.d.a.$ === 'Black') {
							var _v3 = right.a;
							var _v4 = right.d;
							var _v5 = _v4.a;
							return $elm$core$Dict$moveRedRight(dict);
						} else {
							break _v2$2;
						}
					} else {
						var _v6 = right.a;
						var _v7 = right.d;
						return $elm$core$Dict$moveRedRight(dict);
					}
				} else {
					break _v2$2;
				}
			}
			return dict;
		}
	});
var $elm$core$Dict$removeMin = function (dict) {
	if ((dict.$ === 'RBNode_elm_builtin') && (dict.d.$ === 'RBNode_elm_builtin')) {
		var color = dict.a;
		var key = dict.b;
		var value = dict.c;
		var left = dict.d;
		var lColor = left.a;
		var lLeft = left.d;
		var right = dict.e;
		if (lColor.$ === 'Black') {
			if ((lLeft.$ === 'RBNode_elm_builtin') && (lLeft.a.$ === 'Red')) {
				var _v3 = lLeft.a;
				return A5(
					$elm$core$Dict$RBNode_elm_builtin,
					color,
					key,
					value,
					$elm$core$Dict$removeMin(left),
					right);
			} else {
				var _v4 = $elm$core$Dict$moveRedLeft(dict);
				if (_v4.$ === 'RBNode_elm_builtin') {
					var nColor = _v4.a;
					var nKey = _v4.b;
					var nValue = _v4.c;
					var nLeft = _v4.d;
					var nRight = _v4.e;
					return A5(
						$elm$core$Dict$balance,
						nColor,
						nKey,
						nValue,
						$elm$core$Dict$removeMin(nLeft),
						nRight);
				} else {
					return $elm$core$Dict$RBEmpty_elm_builtin;
				}
			}
		} else {
			return A5(
				$elm$core$Dict$RBNode_elm_builtin,
				color,
				key,
				value,
				$elm$core$Dict$removeMin(left),
				right);
		}
	} else {
		return $elm$core$Dict$RBEmpty_elm_builtin;
	}
};
var $elm$core$Dict$removeHelp = F2(
	function (targetKey, dict) {
		if (dict.$ === 'RBEmpty_elm_builtin') {
			return $elm$core$Dict$RBEmpty_elm_builtin;
		} else {
			var color = dict.a;
			var key = dict.b;
			var value = dict.c;
			var left = dict.d;
			var right = dict.e;
			if (_Utils_cmp(targetKey, key) < 0) {
				if ((left.$ === 'RBNode_elm_builtin') && (left.a.$ === 'Black')) {
					var _v4 = left.a;
					var lLeft = left.d;
					if ((lLeft.$ === 'RBNode_elm_builtin') && (lLeft.a.$ === 'Red')) {
						var _v6 = lLeft.a;
						return A5(
							$elm$core$Dict$RBNode_elm_builtin,
							color,
							key,
							value,
							A2($elm$core$Dict$removeHelp, targetKey, left),
							right);
					} else {
						var _v7 = $elm$core$Dict$moveRedLeft(dict);
						if (_v7.$ === 'RBNode_elm_builtin') {
							var nColor = _v7.a;
							var nKey = _v7.b;
							var nValue = _v7.c;
							var nLeft = _v7.d;
							var nRight = _v7.e;
							return A5(
								$elm$core$Dict$balance,
								nColor,
								nKey,
								nValue,
								A2($elm$core$Dict$removeHelp, targetKey, nLeft),
								nRight);
						} else {
							return $elm$core$Dict$RBEmpty_elm_builtin;
						}
					}
				} else {
					return A5(
						$elm$core$Dict$RBNode_elm_builtin,
						color,
						key,
						value,
						A2($elm$core$Dict$removeHelp, targetKey, left),
						right);
				}
			} else {
				return A2(
					$elm$core$Dict$removeHelpEQGT,
					targetKey,
					A7($elm$core$Dict$removeHelpPrepEQGT, targetKey, dict, color, key, value, left, right));
			}
		}
	});
var $elm$core$Dict$removeHelpEQGT = F2(
	function (targetKey, dict) {
		if (dict.$ === 'RBNode_elm_builtin') {
			var color = dict.a;
			var key = dict.b;
			var value = dict.c;
			var left = dict.d;
			var right = dict.e;
			if (_Utils_eq(targetKey, key)) {
				var _v1 = $elm$core$Dict$getMin(right);
				if (_v1.$ === 'RBNode_elm_builtin') {
					var minKey = _v1.b;
					var minValue = _v1.c;
					return A5(
						$elm$core$Dict$balance,
						color,
						minKey,
						minValue,
						left,
						$elm$core$Dict$removeMin(right));
				} else {
					return $elm$core$Dict$RBEmpty_elm_builtin;
				}
			} else {
				return A5(
					$elm$core$Dict$balance,
					color,
					key,
					value,
					left,
					A2($elm$core$Dict$removeHelp, targetKey, right));
			}
		} else {
			return $elm$core$Dict$RBEmpty_elm_builtin;
		}
	});
var $elm$core$Dict$remove = F2(
	function (key, dict) {
		var _v0 = A2($elm$core$Dict$removeHelp, key, dict);
		if ((_v0.$ === 'RBNode_elm_builtin') && (_v0.a.$ === 'Red')) {
			var _v1 = _v0.a;
			var k = _v0.b;
			var v = _v0.c;
			var l = _v0.d;
			var r = _v0.e;
			return A5($elm$core$Dict$RBNode_elm_builtin, $elm$core$Dict$Black, k, v, l, r);
		} else {
			var x = _v0;
			return x;
		}
	});
var $elm$core$Dict$update = F3(
	function (targetKey, alter, dictionary) {
		var _v0 = alter(
			A2($elm$core$Dict$get, targetKey, dictionary));
		if (_v0.$ === 'Just') {
			var value = _v0.a;
			return A3($elm$core$Dict$insert, targetKey, value, dictionary);
		} else {
			return A2($elm$core$Dict$remove, targetKey, dictionary);
		}
	});
var $elm$core$Basics$composeR = F3(
	function (f, g, x) {
		return g(
			f(x));
	});
var $elm$http$Http$expectStringResponse = F2(
	function (toMsg, toResult) {
		return A3(
			_Http_expect,
			'',
			$elm$core$Basics$identity,
			A2($elm$core$Basics$composeR, toResult, toMsg));
	});
var $elm$http$Http$BadBody = function (a) {
	return {$: 'BadBody', a: a};
};
var $elm$http$Http$BadStatus = function (a) {
	return {$: 'BadStatus', a: a};
};
var $elm$http$Http$BadUrl = function (a) {
	return {$: 'BadUrl', a: a};
};
var $elm$http$Http$NetworkError = {$: 'NetworkError'};
var $elm$http$Http$Timeout = {$: 'Timeout'};
var $elm$http$Http$resolve = F2(
	function (toResult, response) {
		switch (response.$) {
			case 'BadUrl_':
				var url = response.a;
				return $elm$core$Result$Err(
					$elm$http$Http$BadUrl(url));
			case 'Timeout_':
				return $elm$core$Result$Err($elm$http$Http$Timeout);
			case 'NetworkError_':
				return $elm$core$Result$Err($elm$http$Http$NetworkError);
			case 'BadStatus_':
				var metadata = response.a;
				return $elm$core$Result$Err(
					$elm$http$Http$BadStatus(metadata.statusCode));
			default:
				var body = response.b;
				return A2(
					$elm$core$Result$mapError,
					$elm$http$Http$BadBody,
					toResult(body));
		}
	});
var $elm$http$Http$expectJson = F2(
	function (toMsg, decoder) {
		return A2(
			$elm$http$Http$expectStringResponse,
			toMsg,
			$elm$http$Http$resolve(
				function (string) {
					return A2(
						$elm$core$Result$mapError,
						$elm$json$Json$Decode$errorToString,
						A2($elm$json$Json$Decode$decodeString, decoder, string));
				}));
	});
var $elm$http$Http$emptyBody = _Http_emptyBody;
var $elm$http$Http$Request = function (a) {
	return {$: 'Request', a: a};
};
var $elm$http$Http$State = F2(
	function (reqs, subs) {
		return {reqs: reqs, subs: subs};
	});
var $elm$http$Http$init = $elm$core$Task$succeed(
	A2($elm$http$Http$State, $elm$core$Dict$empty, _List_Nil));
var $elm$core$Process$kill = _Scheduler_kill;
var $elm$core$Process$spawn = _Scheduler_spawn;
var $elm$http$Http$updateReqs = F3(
	function (router, cmds, reqs) {
		updateReqs:
		while (true) {
			if (!cmds.b) {
				return $elm$core$Task$succeed(reqs);
			} else {
				var cmd = cmds.a;
				var otherCmds = cmds.b;
				if (cmd.$ === 'Cancel') {
					var tracker = cmd.a;
					var _v2 = A2($elm$core$Dict$get, tracker, reqs);
					if (_v2.$ === 'Nothing') {
						var $temp$router = router,
							$temp$cmds = otherCmds,
							$temp$reqs = reqs;
						router = $temp$router;
						cmds = $temp$cmds;
						reqs = $temp$reqs;
						continue updateReqs;
					} else {
						var pid = _v2.a;
						return A2(
							$elm$core$Task$andThen,
							function (_v3) {
								return A3(
									$elm$http$Http$updateReqs,
									router,
									otherCmds,
									A2($elm$core$Dict$remove, tracker, reqs));
							},
							$elm$core$Process$kill(pid));
					}
				} else {
					var req = cmd.a;
					return A2(
						$elm$core$Task$andThen,
						function (pid) {
							var _v4 = req.tracker;
							if (_v4.$ === 'Nothing') {
								return A3($elm$http$Http$updateReqs, router, otherCmds, reqs);
							} else {
								var tracker = _v4.a;
								return A3(
									$elm$http$Http$updateReqs,
									router,
									otherCmds,
									A3($elm$core$Dict$insert, tracker, pid, reqs));
							}
						},
						$elm$core$Process$spawn(
							A3(
								_Http_toTask,
								router,
								$elm$core$Platform$sendToApp(router),
								req)));
				}
			}
		}
	});
var $elm$http$Http$onEffects = F4(
	function (router, cmds, subs, state) {
		return A2(
			$elm$core$Task$andThen,
			function (reqs) {
				return $elm$core$Task$succeed(
					A2($elm$http$Http$State, reqs, subs));
			},
			A3($elm$http$Http$updateReqs, router, cmds, state.reqs));
	});
var $elm$http$Http$maybeSend = F4(
	function (router, desiredTracker, progress, _v0) {
		var actualTracker = _v0.a;
		var toMsg = _v0.b;
		return _Utils_eq(desiredTracker, actualTracker) ? $elm$core$Maybe$Just(
			A2(
				$elm$core$Platform$sendToApp,
				router,
				toMsg(progress))) : $elm$core$Maybe$Nothing;
	});
var $elm$http$Http$onSelfMsg = F3(
	function (router, _v0, state) {
		var tracker = _v0.a;
		var progress = _v0.b;
		return A2(
			$elm$core$Task$andThen,
			function (_v1) {
				return $elm$core$Task$succeed(state);
			},
			$elm$core$Task$sequence(
				A2(
					$elm$core$List$filterMap,
					A3($elm$http$Http$maybeSend, router, tracker, progress),
					state.subs)));
	});
var $elm$http$Http$Cancel = function (a) {
	return {$: 'Cancel', a: a};
};
var $elm$http$Http$cmdMap = F2(
	function (func, cmd) {
		if (cmd.$ === 'Cancel') {
			var tracker = cmd.a;
			return $elm$http$Http$Cancel(tracker);
		} else {
			var r = cmd.a;
			return $elm$http$Http$Request(
				{
					allowCookiesFromOtherDomains: r.allowCookiesFromOtherDomains,
					body: r.body,
					expect: A2(_Http_mapExpect, func, r.expect),
					headers: r.headers,
					method: r.method,
					timeout: r.timeout,
					tracker: r.tracker,
					url: r.url
				});
		}
	});
var $elm$http$Http$MySub = F2(
	function (a, b) {
		return {$: 'MySub', a: a, b: b};
	});
var $elm$http$Http$subMap = F2(
	function (func, _v0) {
		var tracker = _v0.a;
		var toMsg = _v0.b;
		return A2(
			$elm$http$Http$MySub,
			tracker,
			A2($elm$core$Basics$composeR, toMsg, func));
	});
_Platform_effectManagers['Http'] = _Platform_createManager($elm$http$Http$init, $elm$http$Http$onEffects, $elm$http$Http$onSelfMsg, $elm$http$Http$cmdMap, $elm$http$Http$subMap);
var $elm$http$Http$command = _Platform_leaf('Http');
var $elm$http$Http$subscription = _Platform_leaf('Http');
var $elm$http$Http$request = function (r) {
	return $elm$http$Http$command(
		$elm$http$Http$Request(
			{allowCookiesFromOtherDomains: false, body: r.body, expect: r.expect, headers: r.headers, method: r.method, timeout: r.timeout, tracker: r.tracker, url: r.url}));
};
var $elm$http$Http$get = function (r) {
	return $elm$http$Http$request(
		{body: $elm$http$Http$emptyBody, expect: r.expect, headers: _List_Nil, method: 'GET', timeout: $elm$core$Maybe$Nothing, tracker: $elm$core$Maybe$Nothing, url: r.url});
};
var $author$project$Static$About$path = '/about';
var $author$project$Static$About$create = $elm$http$Http$get(
	{
		expect: A2(
			$elm$http$Http$expectJson,
			$author$project$Static$About$Msg,
			A2($elm$json$Json$Decode$map, $author$project$Static$About$Model, $author$project$Static$About$aboutDecoder)),
		url: $author$project$Static$About$path
	});
var $author$project$Static$Motif$Msg = function (a) {
	return {$: 'Msg', a: a};
};
var $author$project$Static$Motif$Model = function (a) {
	return {$: 'Model', a: a};
};
var $author$project$Static$Motif$Motif = function (motifName) {
	return function (accent) {
		return function (boxShadow) {
			return function (buttonBackground) {
				return function (buttonOpaqueBackground) {
					return function (error) {
						return function (primaryGlow) {
							return function (primaryLight) {
								return function (primary) {
									return function (ready) {
										return function (textNormal) {
											return function (textPassive) {
												return function (warning) {
													return function (textBigPassive) {
														return function (buttonBorder) {
															return function (buttonBorderRadius) {
																return function (buttonIconFontSize) {
																	return function (buttonPadding) {
																		return function (buttonHeight) {
																			return {accent: accent, boxShadow: boxShadow, buttonBackground: buttonBackground, buttonBorder: buttonBorder, buttonBorderRadius: buttonBorderRadius, buttonHeight: buttonHeight, buttonIconFontSize: buttonIconFontSize, buttonOpaqueBackground: buttonOpaqueBackground, buttonPadding: buttonPadding, error: error, motifName: motifName, primary: primary, primaryGlow: primaryGlow, primaryLight: primaryLight, ready: ready, textBigPassive: textBigPassive, textNormal: textNormal, textPassive: textPassive, warning: warning};
																		};
																	};
																};
															};
														};
													};
												};
											};
										};
									};
								};
							};
						};
					};
				};
			};
		};
	};
};
var $author$project$Static$Motif$Colour = F4(
	function (r, g, b, a) {
		return {a: a, b: b, g: g, r: r};
	});
var $elm$json$Json$Decode$float = _Json_decodeFloat;
var $elm$json$Json$Decode$int = _Json_decodeInt;
var $author$project$Static$Motif$colorDecoder = A3(
	$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
	'a',
	$elm$json$Json$Decode$float,
	A3(
		$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
		'b',
		$elm$json$Json$Decode$int,
		A3(
			$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
			'g',
			$elm$json$Json$Decode$int,
			A3(
				$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
				'r',
				$elm$json$Json$Decode$int,
				$elm$json$Json$Decode$succeed($author$project$Static$Motif$Colour)))));
var $rtfeldman$elm_css$Css$Structure$Compatible = {$: 'Compatible'};
var $rtfeldman$elm_css$Css$cssFunction = F2(
	function (funcName, args) {
		return funcName + ('(' + (A2($elm$core$String$join, ',', args) + ')'));
	});
var $elm$core$String$fromFloat = _String_fromNumber;
var $rtfeldman$elm_css$Css$rgba = F4(
	function (r, g, b, alpha) {
		return {
			alpha: alpha,
			blue: b,
			color: $rtfeldman$elm_css$Css$Structure$Compatible,
			green: g,
			red: r,
			value: A2(
				$rtfeldman$elm_css$Css$cssFunction,
				'rgba',
				_Utils_ap(
					A2(
						$elm$core$List$map,
						$elm$core$String$fromInt,
						_List_fromArray(
							[r, g, b])),
					_List_fromArray(
						[
							$elm$core$String$fromFloat(alpha)
						])))
		};
	});
var $author$project$Static$Motif$colourToCSSColor = function (col) {
	return A4($rtfeldman$elm_css$Css$rgba, col.r, col.g, col.b, col.a);
};
var $author$project$Static$Motif$cssColorDecoder = A2($elm$json$Json$Decode$map, $author$project$Static$Motif$colourToCSSColor, $author$project$Static$Motif$colorDecoder);
var $elm$core$String$toFloat = _String_toFloat;
var $author$project$Static$Motif$floatStringDecoder = A2(
	$elm$json$Json$Decode$andThen,
	function (s) {
		var _v0 = $elm$core$String$toFloat(s);
		if (_v0.$ === 'Just') {
			var f = _v0.a;
			return $elm$json$Json$Decode$succeed(f);
		} else {
			return $elm$json$Json$Decode$fail('could not convert string to float');
		}
	},
	$elm$json$Json$Decode$string);
var $author$project$Static$Motif$motifDecoder = A2(
	$elm$json$Json$Decode$map,
	$author$project$Static$Motif$Model,
	A3(
		$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
		'buttonHeight',
		$elm$json$Json$Decode$float,
		A3(
			$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
			'buttonPadding',
			$author$project$Static$Motif$floatStringDecoder,
			A3(
				$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
				'buttonIconFontSize',
				$author$project$Static$Motif$floatStringDecoder,
				A3(
					$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
					'buttonBorderRadius',
					$author$project$Static$Motif$floatStringDecoder,
					A3(
						$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
						'buttonBorder',
						$author$project$Static$Motif$floatStringDecoder,
						A3(
							$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
							'textBigPassive',
							$author$project$Static$Motif$cssColorDecoder,
							A3(
								$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
								'warning',
								$author$project$Static$Motif$cssColorDecoder,
								A3(
									$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
									'textPassive',
									$author$project$Static$Motif$cssColorDecoder,
									A3(
										$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
										'textNormal',
										$author$project$Static$Motif$cssColorDecoder,
										A3(
											$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
											'ready',
											$author$project$Static$Motif$cssColorDecoder,
											A3(
												$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
												'primary',
												$author$project$Static$Motif$cssColorDecoder,
												A3(
													$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
													'primaryLight',
													$author$project$Static$Motif$cssColorDecoder,
													A3(
														$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
														'primaryGlow',
														$author$project$Static$Motif$cssColorDecoder,
														A3(
															$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
															'error',
															$author$project$Static$Motif$cssColorDecoder,
															A3(
																$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
																'buttonOpaqueBackground',
																$author$project$Static$Motif$cssColorDecoder,
																A3(
																	$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
																	'buttonBackground',
																	$author$project$Static$Motif$cssColorDecoder,
																	A3(
																		$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
																		'boxShadow',
																		$elm$json$Json$Decode$string,
																		A3(
																			$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
																			'accent',
																			$author$project$Static$Motif$cssColorDecoder,
																			A3(
																				$NoRedInk$elm_json_decode_pipeline$Json$Decode$Pipeline$required,
																				'motifName',
																				$elm$json$Json$Decode$string,
																				$elm$json$Json$Decode$succeed($author$project$Static$Motif$Motif)))))))))))))))))))));
var $author$project$Static$Motif$path = '/motif/elm';
var $author$project$Static$Motif$create = $elm$http$Http$get(
	{
		expect: A2($elm$http$Http$expectJson, $author$project$Static$Motif$Msg, $author$project$Static$Motif$motifDecoder),
		url: $author$project$Static$Motif$path
	});
var $rtfeldman$elm_css$Css$rgb = F3(
	function (r, g, b) {
		return {
			alpha: 1,
			blue: b,
			color: $rtfeldman$elm_css$Css$Structure$Compatible,
			green: g,
			red: r,
			value: A2(
				$rtfeldman$elm_css$Css$cssFunction,
				'rgb',
				A2(
					$elm$core$List$map,
					$elm$core$String$fromInt,
					_List_fromArray(
						[r, g, b])))
		};
	});
var $author$project$Theme$black = A3($rtfeldman$elm_css$Css$rgb, 0, 0, 0);
var $author$project$Theme$darkGray = A3($rtfeldman$elm_css$Css$rgb, 169, 169, 169);
var $author$project$Theme$darkOrange = A3($rtfeldman$elm_css$Css$rgb, 255, 140, 0);
var $author$project$Theme$deepSkyBlue = A3($rtfeldman$elm_css$Css$rgb, 0, 191, 255);
var $author$project$Theme$green = A3($rtfeldman$elm_css$Css$rgb, 0, 255, 0);
var $author$project$Theme$lightGray = A3($rtfeldman$elm_css$Css$rgb, 211, 211, 211);
var $author$project$Theme$orange = A3($rtfeldman$elm_css$Css$rgb, 255, 165, 0);
var $author$project$Theme$initial = {
	accent: $author$project$Theme$deepSkyBlue,
	boxShadow: '0px 0.1em 0.5em 1px DimGray',
	buttonBackground: A4($rtfeldman$elm_css$Css$rgba, 255, 255, 255, 0.85),
	buttonBorder: 0.4,
	buttonBorderRadius: 1.2,
	buttonHeight: 4,
	buttonIconFontSize: 2.0,
	buttonOpaqueBackground: A3($rtfeldman$elm_css$Css$rgb, 255, 255, 255),
	buttonPadding: 0.6,
	error: A3($rtfeldman$elm_css$Css$rgb, 255, 0, 0),
	motifName: 'InitialMotif',
	primary: $author$project$Theme$darkOrange,
	primaryGlow: A4($rtfeldman$elm_css$Css$rgba, 255, 210, 0, 1.0),
	primaryLight: $author$project$Theme$orange,
	ready: $author$project$Theme$green,
	textBigPassive: $author$project$Theme$lightGray,
	textNormal: $author$project$Theme$black,
	textPassive: $author$project$Theme$darkGray,
	warning: A3($rtfeldman$elm_css$Css$rgb, 255, 100, 100)
};
var $elm$core$Platform$Cmd$map = _Platform_map;
var $elm$core$Platform$Cmd$none = $elm$core$Platform$Cmd$batch(_List_Nil);
var $author$project$Shutterbug$MakeNew = {$: 'MakeNew'};
var $author$project$Shutterbug$defaultSource = $author$project$Shutterbug$MakeNew;
var $elm$core$String$concat = function (strings) {
	return A2($elm$core$String$join, '', strings);
};
var $author$project$Shutterbug$queryKey = 'shutterbug';
var $author$project$Shutterbug$queryValues = {extant: 'extant', _new: 'new'};
var $author$project$Shutterbug$manualEntryURL = function (location) {
	return $elm$core$String$concat(
		_List_fromArray(
			[
				function () {
				var _v0 = location.protocol;
				if (_v0.$ === 'Http') {
					return 'http://';
				} else {
					return 'https://';
				}
			}(),
				location.host,
				function () {
				var _v1 = location.port_;
				if (_v1.$ === 'Just') {
					var urlPort = _v1.a;
					return ':' + $elm$core$String$fromInt(urlPort);
				} else {
					return '';
				}
			}(),
				'/?' + ($author$project$Shutterbug$queryKey + ('=' + $author$project$Shutterbug$queryValues.extant))
			]));
};
var $author$project$Route$Home = function (a) {
	return {$: 'Home', a: a};
};
var $elm$url$Url$Parser$Parser = function (a) {
	return {$: 'Parser', a: a};
};
var $elm$url$Url$Parser$State = F5(
	function (visited, unvisited, params, frag, value) {
		return {frag: frag, params: params, unvisited: unvisited, value: value, visited: visited};
	});
var $elm$url$Url$Parser$mapState = F2(
	function (func, _v0) {
		var visited = _v0.visited;
		var unvisited = _v0.unvisited;
		var params = _v0.params;
		var frag = _v0.frag;
		var value = _v0.value;
		return A5(
			$elm$url$Url$Parser$State,
			visited,
			unvisited,
			params,
			frag,
			func(value));
	});
var $elm$url$Url$Parser$map = F2(
	function (subValue, _v0) {
		var parseArg = _v0.a;
		return $elm$url$Url$Parser$Parser(
			function (_v1) {
				var visited = _v1.visited;
				var unvisited = _v1.unvisited;
				var params = _v1.params;
				var frag = _v1.frag;
				var value = _v1.value;
				return A2(
					$elm$core$List$map,
					$elm$url$Url$Parser$mapState(value),
					parseArg(
						A5($elm$url$Url$Parser$State, visited, unvisited, params, frag, subValue)));
			});
	});
var $elm$core$List$concatMap = F2(
	function (f, list) {
		return $elm$core$List$concat(
			A2($elm$core$List$map, f, list));
	});
var $elm$url$Url$Parser$oneOf = function (parsers) {
	return $elm$url$Url$Parser$Parser(
		function (state) {
			return A2(
				$elm$core$List$concatMap,
				function (_v0) {
					var parser = _v0.a;
					return parser(state);
				},
				parsers);
		});
};
var $elm$url$Url$Parser$getFirstMatch = function (states) {
	getFirstMatch:
	while (true) {
		if (!states.b) {
			return $elm$core$Maybe$Nothing;
		} else {
			var state = states.a;
			var rest = states.b;
			var _v1 = state.unvisited;
			if (!_v1.b) {
				return $elm$core$Maybe$Just(state.value);
			} else {
				if ((_v1.a === '') && (!_v1.b.b)) {
					return $elm$core$Maybe$Just(state.value);
				} else {
					var $temp$states = rest;
					states = $temp$states;
					continue getFirstMatch;
				}
			}
		}
	}
};
var $elm$url$Url$Parser$removeFinalEmpty = function (segments) {
	if (!segments.b) {
		return _List_Nil;
	} else {
		if ((segments.a === '') && (!segments.b.b)) {
			return _List_Nil;
		} else {
			var segment = segments.a;
			var rest = segments.b;
			return A2(
				$elm$core$List$cons,
				segment,
				$elm$url$Url$Parser$removeFinalEmpty(rest));
		}
	}
};
var $elm$url$Url$Parser$preparePath = function (path) {
	var _v0 = A2($elm$core$String$split, '/', path);
	if (_v0.b && (_v0.a === '')) {
		var segments = _v0.b;
		return $elm$url$Url$Parser$removeFinalEmpty(segments);
	} else {
		var segments = _v0;
		return $elm$url$Url$Parser$removeFinalEmpty(segments);
	}
};
var $elm$url$Url$Parser$addToParametersHelp = F2(
	function (value, maybeList) {
		if (maybeList.$ === 'Nothing') {
			return $elm$core$Maybe$Just(
				_List_fromArray(
					[value]));
		} else {
			var list = maybeList.a;
			return $elm$core$Maybe$Just(
				A2($elm$core$List$cons, value, list));
		}
	});
var $elm$url$Url$percentDecode = _Url_percentDecode;
var $elm$url$Url$Parser$addParam = F2(
	function (segment, dict) {
		var _v0 = A2($elm$core$String$split, '=', segment);
		if ((_v0.b && _v0.b.b) && (!_v0.b.b.b)) {
			var rawKey = _v0.a;
			var _v1 = _v0.b;
			var rawValue = _v1.a;
			var _v2 = $elm$url$Url$percentDecode(rawKey);
			if (_v2.$ === 'Nothing') {
				return dict;
			} else {
				var key = _v2.a;
				var _v3 = $elm$url$Url$percentDecode(rawValue);
				if (_v3.$ === 'Nothing') {
					return dict;
				} else {
					var value = _v3.a;
					return A3(
						$elm$core$Dict$update,
						key,
						$elm$url$Url$Parser$addToParametersHelp(value),
						dict);
				}
			}
		} else {
			return dict;
		}
	});
var $elm$url$Url$Parser$prepareQuery = function (maybeQuery) {
	if (maybeQuery.$ === 'Nothing') {
		return $elm$core$Dict$empty;
	} else {
		var qry = maybeQuery.a;
		return A3(
			$elm$core$List$foldr,
			$elm$url$Url$Parser$addParam,
			$elm$core$Dict$empty,
			A2($elm$core$String$split, '&', qry));
	}
};
var $elm$url$Url$Parser$parse = F2(
	function (_v0, url) {
		var parser = _v0.a;
		return $elm$url$Url$Parser$getFirstMatch(
			parser(
				A5(
					$elm$url$Url$Parser$State,
					_List_Nil,
					$elm$url$Url$Parser$preparePath(url.path),
					$elm$url$Url$Parser$prepareQuery(url.query),
					url.fragment,
					$elm$core$Basics$identity)));
	});
var $author$project$Shutterbug$UseExisting = {$: 'UseExisting'};
var $elm$url$Url$Parser$Internal$Parser = function (a) {
	return {$: 'Parser', a: a};
};
var $elm$core$Maybe$withDefault = F2(
	function (_default, maybe) {
		if (maybe.$ === 'Just') {
			var value = maybe.a;
			return value;
		} else {
			return _default;
		}
	});
var $elm$url$Url$Parser$Query$custom = F2(
	function (key, func) {
		return $elm$url$Url$Parser$Internal$Parser(
			function (dict) {
				return func(
					A2(
						$elm$core$Maybe$withDefault,
						_List_Nil,
						A2($elm$core$Dict$get, key, dict)));
			});
	});
var $elm$url$Url$Parser$Query$enum = F2(
	function (key, dict) {
		return A2(
			$elm$url$Url$Parser$Query$custom,
			key,
			function (stringList) {
				if (stringList.b && (!stringList.b.b)) {
					var str = stringList.a;
					return A2($elm$core$Dict$get, str, dict);
				} else {
					return $elm$core$Maybe$Nothing;
				}
			});
	});
var $elm$core$Dict$fromList = function (assocs) {
	return A3(
		$elm$core$List$foldl,
		F2(
			function (_v0, dict) {
				var key = _v0.a;
				var value = _v0.b;
				return A3($elm$core$Dict$insert, key, value, dict);
			}),
		$elm$core$Dict$empty,
		assocs);
};
var $author$project$Route$parseQuery = A2(
	$elm$url$Url$Parser$Query$enum,
	$author$project$Shutterbug$queryKey,
	$elm$core$Dict$fromList(
		_List_fromArray(
			[
				_Utils_Tuple2($author$project$Shutterbug$queryValues.extant, $author$project$Shutterbug$UseExisting),
				_Utils_Tuple2($author$project$Shutterbug$queryValues._new, $author$project$Shutterbug$MakeNew)
			])));
var $elm$url$Url$Parser$query = function (_v0) {
	var queryParser = _v0.a;
	return $elm$url$Url$Parser$Parser(
		function (_v1) {
			var visited = _v1.visited;
			var unvisited = _v1.unvisited;
			var params = _v1.params;
			var frag = _v1.frag;
			var value = _v1.value;
			return _List_fromArray(
				[
					A5(
					$elm$url$Url$Parser$State,
					visited,
					unvisited,
					params,
					frag,
					value(
						queryParser(params)))
				]);
		});
};
var $elm$url$Url$Parser$slash = F2(
	function (_v0, _v1) {
		var parseBefore = _v0.a;
		var parseAfter = _v1.a;
		return $elm$url$Url$Parser$Parser(
			function (state) {
				return A2(
					$elm$core$List$concatMap,
					parseAfter,
					parseBefore(state));
			});
	});
var $elm$url$Url$Parser$top = $elm$url$Url$Parser$Parser(
	function (state) {
		return _List_fromArray(
			[state]);
	});
var $author$project$Route$parseRoute = function (url) {
	return A2(
		$elm$url$Url$Parser$parse,
		$elm$url$Url$Parser$oneOf(
			_List_fromArray(
				[
					A2(
					$elm$url$Url$Parser$map,
					$author$project$Route$Home,
					A2(
						$elm$url$Url$Parser$slash,
						$elm$url$Url$Parser$top,
						$elm$url$Url$Parser$query($author$project$Route$parseQuery)))
				])),
		url);
};
var $elm$core$Basics$negate = function (n) {
	return -n;
};
var $elm$core$String$dropRight = F2(
	function (n, string) {
		return (n < 1) ? string : A3($elm$core$String$slice, 0, -n, string);
	});
var $elm$core$String$endsWith = _String_endsWith;
var $elm_community$string_extra$String$Extra$unsurround = F2(
	function (wrapper, string) {
		if (A2($elm$core$String$startsWith, wrapper, string) && A2($elm$core$String$endsWith, wrapper, string)) {
			var length = $elm$core$String$length(wrapper);
			return A2(
				$elm$core$String$dropRight,
				length,
				A2($elm$core$String$dropLeft, length, string));
		} else {
			return string;
		}
	});
var $elm_community$string_extra$String$Extra$unquote = function (string) {
	return A2($elm_community$string_extra$String$Extra$unsurround, '\"', string);
};
var $author$project$Route$parseURL = function (maybeURL) {
	var _v0 = A2($elm$json$Json$Decode$decodeValue, $elm$json$Json$Decode$string, maybeURL);
	if (_v0.$ === 'Ok') {
		var urlString = _v0.a;
		var _v1 = $elm$url$Url$fromString(
			$elm_community$string_extra$String$Extra$unquote(urlString));
		if (_v1.$ === 'Just') {
			var url = _v1.a;
			var _v2 = $author$project$Route$parseRoute(url);
			if (_v2.$ === 'Just') {
				if (_v2.a.a.$ === 'Nothing') {
					var _v3 = _v2.a.a;
					var _v4 = url.query;
					if (_v4.$ === 'Just') {
						return $elm$core$Result$Err('the URL query is not as expected');
					} else {
						return $elm$core$Result$Ok(
							{
								manualEntryURL: $author$project$Shutterbug$manualEntryURL(url),
								source: $author$project$Shutterbug$defaultSource
							});
					}
				} else {
					var source = _v2.a.a.a;
					return $elm$core$Result$Ok(
						{
							manualEntryURL: $author$project$Shutterbug$manualEntryURL(url),
							source: source
						});
				}
			} else {
				return $elm$core$Result$Err('URL processing error');
			}
		} else {
			return $elm$core$Result$Err('the URL is invalid');
		}
	} else {
		var error = _v0.a;
		return $elm$core$Result$Err(
			$elm$json$Json$Decode$errorToString(error));
	}
};
var $author$project$Main$init = function (url) {
	var _v0 = $author$project$Route$parseURL(url);
	if (_v0.$ === 'Ok') {
		var shutterbugMeta = _v0.a;
		return _Utils_Tuple2(
			$author$project$Main$Starting(
				{
					awaitingShutterbug: true,
					extantUrl: shutterbugMeta.manualEntryURL,
					maybeAbout: $elm$core$Maybe$Nothing,
					maybeCameras: $elm$core$Maybe$Nothing,
					maybeDevoir: $elm$core$Maybe$Nothing,
					maybePix: $elm$core$Maybe$Nothing,
					maybeTheme: $elm$core$Maybe$Nothing,
					sentCameraAssociations: false,
					shutterbugActionsPopUp: false,
					shutterbugSource: function () {
						var _v1 = shutterbugMeta.source;
						if (_v1.$ === 'MakeNew') {
							return $author$project$Main$MakeNew;
						} else {
							return $author$project$Main$UseExisting(
								{shutterbugInput: '', shutterbugUnmasked: false});
						}
					}(),
					websockets: $author$project$Main$Connecting
				}),
			$elm$core$Platform$Cmd$batch(
				_List_fromArray(
					[
						A2($elm$core$Platform$Cmd$map, $author$project$Main$ReceivedAbout, $author$project$Static$About$create),
						A2($elm$core$Platform$Cmd$map, $author$project$Main$ReceivedMotif, $author$project$Static$Motif$create)
					])));
	} else {
		var error = _v0.a;
		return _Utils_Tuple2(
			A2(
				$author$project$Main$Errored,
				$author$project$Theme$initial,
				_List_fromArray(
					[
						_List_fromArray(
						[error])
					])),
			$elm$core$Platform$Cmd$none);
	}
};
var $author$project$Main$ReceivedDevoir = function (a) {
	return {$: 'ReceivedDevoir', a: a};
};
var $author$project$Main$ReceivedKaput = function (a) {
	return {$: 'ReceivedKaput', a: a};
};
var $author$project$Main$ReceivedLifeCycle = function (a) {
	return {$: 'ReceivedLifeCycle', a: a};
};
var $author$project$Main$ReceivedPixelsMeta = function (a) {
	return {$: 'ReceivedPixelsMeta', a: a};
};
var $author$project$Main$TryToUnblockCameraToggleButtons = function (a) {
	return {$: 'TryToUnblockCameraToggleButtons', a: a};
};
var $elm$core$Platform$Sub$batch = _Platform_batch;
var $elm$time$Time$Every = F2(
	function (a, b) {
		return {$: 'Every', a: a, b: b};
	});
var $elm$time$Time$State = F2(
	function (taggers, processes) {
		return {processes: processes, taggers: taggers};
	});
var $elm$time$Time$init = $elm$core$Task$succeed(
	A2($elm$time$Time$State, $elm$core$Dict$empty, $elm$core$Dict$empty));
var $elm$time$Time$addMySub = F2(
	function (_v0, state) {
		var interval = _v0.a;
		var tagger = _v0.b;
		var _v1 = A2($elm$core$Dict$get, interval, state);
		if (_v1.$ === 'Nothing') {
			return A3(
				$elm$core$Dict$insert,
				interval,
				_List_fromArray(
					[tagger]),
				state);
		} else {
			var taggers = _v1.a;
			return A3(
				$elm$core$Dict$insert,
				interval,
				A2($elm$core$List$cons, tagger, taggers),
				state);
		}
	});
var $elm$core$Dict$foldl = F3(
	function (func, acc, dict) {
		foldl:
		while (true) {
			if (dict.$ === 'RBEmpty_elm_builtin') {
				return acc;
			} else {
				var key = dict.b;
				var value = dict.c;
				var left = dict.d;
				var right = dict.e;
				var $temp$func = func,
					$temp$acc = A3(
					func,
					key,
					value,
					A3($elm$core$Dict$foldl, func, acc, left)),
					$temp$dict = right;
				func = $temp$func;
				acc = $temp$acc;
				dict = $temp$dict;
				continue foldl;
			}
		}
	});
var $elm$core$Dict$merge = F6(
	function (leftStep, bothStep, rightStep, leftDict, rightDict, initialResult) {
		var stepState = F3(
			function (rKey, rValue, _v0) {
				stepState:
				while (true) {
					var list = _v0.a;
					var result = _v0.b;
					if (!list.b) {
						return _Utils_Tuple2(
							list,
							A3(rightStep, rKey, rValue, result));
					} else {
						var _v2 = list.a;
						var lKey = _v2.a;
						var lValue = _v2.b;
						var rest = list.b;
						if (_Utils_cmp(lKey, rKey) < 0) {
							var $temp$rKey = rKey,
								$temp$rValue = rValue,
								$temp$_v0 = _Utils_Tuple2(
								rest,
								A3(leftStep, lKey, lValue, result));
							rKey = $temp$rKey;
							rValue = $temp$rValue;
							_v0 = $temp$_v0;
							continue stepState;
						} else {
							if (_Utils_cmp(lKey, rKey) > 0) {
								return _Utils_Tuple2(
									list,
									A3(rightStep, rKey, rValue, result));
							} else {
								return _Utils_Tuple2(
									rest,
									A4(bothStep, lKey, lValue, rValue, result));
							}
						}
					}
				}
			});
		var _v3 = A3(
			$elm$core$Dict$foldl,
			stepState,
			_Utils_Tuple2(
				$elm$core$Dict$toList(leftDict),
				initialResult),
			rightDict);
		var leftovers = _v3.a;
		var intermediateResult = _v3.b;
		return A3(
			$elm$core$List$foldl,
			F2(
				function (_v4, result) {
					var k = _v4.a;
					var v = _v4.b;
					return A3(leftStep, k, v, result);
				}),
			intermediateResult,
			leftovers);
	});
var $elm$time$Time$Name = function (a) {
	return {$: 'Name', a: a};
};
var $elm$time$Time$Offset = function (a) {
	return {$: 'Offset', a: a};
};
var $elm$time$Time$Zone = F2(
	function (a, b) {
		return {$: 'Zone', a: a, b: b};
	});
var $elm$time$Time$customZone = $elm$time$Time$Zone;
var $elm$time$Time$setInterval = _Time_setInterval;
var $elm$time$Time$spawnHelp = F3(
	function (router, intervals, processes) {
		if (!intervals.b) {
			return $elm$core$Task$succeed(processes);
		} else {
			var interval = intervals.a;
			var rest = intervals.b;
			var spawnTimer = $elm$core$Process$spawn(
				A2(
					$elm$time$Time$setInterval,
					interval,
					A2($elm$core$Platform$sendToSelf, router, interval)));
			var spawnRest = function (id) {
				return A3(
					$elm$time$Time$spawnHelp,
					router,
					rest,
					A3($elm$core$Dict$insert, interval, id, processes));
			};
			return A2($elm$core$Task$andThen, spawnRest, spawnTimer);
		}
	});
var $elm$time$Time$onEffects = F3(
	function (router, subs, _v0) {
		var processes = _v0.processes;
		var rightStep = F3(
			function (_v6, id, _v7) {
				var spawns = _v7.a;
				var existing = _v7.b;
				var kills = _v7.c;
				return _Utils_Tuple3(
					spawns,
					existing,
					A2(
						$elm$core$Task$andThen,
						function (_v5) {
							return kills;
						},
						$elm$core$Process$kill(id)));
			});
		var newTaggers = A3($elm$core$List$foldl, $elm$time$Time$addMySub, $elm$core$Dict$empty, subs);
		var leftStep = F3(
			function (interval, taggers, _v4) {
				var spawns = _v4.a;
				var existing = _v4.b;
				var kills = _v4.c;
				return _Utils_Tuple3(
					A2($elm$core$List$cons, interval, spawns),
					existing,
					kills);
			});
		var bothStep = F4(
			function (interval, taggers, id, _v3) {
				var spawns = _v3.a;
				var existing = _v3.b;
				var kills = _v3.c;
				return _Utils_Tuple3(
					spawns,
					A3($elm$core$Dict$insert, interval, id, existing),
					kills);
			});
		var _v1 = A6(
			$elm$core$Dict$merge,
			leftStep,
			bothStep,
			rightStep,
			newTaggers,
			processes,
			_Utils_Tuple3(
				_List_Nil,
				$elm$core$Dict$empty,
				$elm$core$Task$succeed(_Utils_Tuple0)));
		var spawnList = _v1.a;
		var existingDict = _v1.b;
		var killTask = _v1.c;
		return A2(
			$elm$core$Task$andThen,
			function (newProcesses) {
				return $elm$core$Task$succeed(
					A2($elm$time$Time$State, newTaggers, newProcesses));
			},
			A2(
				$elm$core$Task$andThen,
				function (_v2) {
					return A3($elm$time$Time$spawnHelp, router, spawnList, existingDict);
				},
				killTask));
	});
var $elm$time$Time$Posix = function (a) {
	return {$: 'Posix', a: a};
};
var $elm$time$Time$millisToPosix = $elm$time$Time$Posix;
var $elm$time$Time$now = _Time_now($elm$time$Time$millisToPosix);
var $elm$time$Time$onSelfMsg = F3(
	function (router, interval, state) {
		var _v0 = A2($elm$core$Dict$get, interval, state.taggers);
		if (_v0.$ === 'Nothing') {
			return $elm$core$Task$succeed(state);
		} else {
			var taggers = _v0.a;
			var tellTaggers = function (time) {
				return $elm$core$Task$sequence(
					A2(
						$elm$core$List$map,
						function (tagger) {
							return A2(
								$elm$core$Platform$sendToApp,
								router,
								tagger(time));
						},
						taggers));
			};
			return A2(
				$elm$core$Task$andThen,
				function (_v1) {
					return $elm$core$Task$succeed(state);
				},
				A2($elm$core$Task$andThen, tellTaggers, $elm$time$Time$now));
		}
	});
var $elm$core$Basics$composeL = F3(
	function (g, f, x) {
		return g(
			f(x));
	});
var $elm$time$Time$subMap = F2(
	function (f, _v0) {
		var interval = _v0.a;
		var tagger = _v0.b;
		return A2(
			$elm$time$Time$Every,
			interval,
			A2($elm$core$Basics$composeL, f, tagger));
	});
_Platform_effectManagers['Time'] = _Platform_createManager($elm$time$Time$init, $elm$time$Time$onEffects, $elm$time$Time$onSelfMsg, 0, $elm$time$Time$subMap);
var $elm$time$Time$subscription = _Platform_leaf('Time');
var $elm$time$Time$every = F2(
	function (interval, tagger) {
		return $elm$time$Time$subscription(
			A2($elm$time$Time$Every, interval, tagger));
	});
var $author$project$Main$receiveDevoir = _Platform_incomingPort('receiveDevoir', $elm$json$Json$Decode$value);
var $author$project$Main$receiveKaput = _Platform_incomingPort('receiveKaput', $elm$json$Json$Decode$value);
var $author$project$Main$receiveLifeCycle = _Platform_incomingPort('receiveLifeCycle', $elm$json$Json$Decode$value);
var $author$project$Main$receivePixelsMeta = _Platform_incomingPort('receivePixelsMeta', $elm$json$Json$Decode$value);
var $author$project$Main$ReceivedRegAssocCameras = function (a) {
	return {$: 'ReceivedRegAssocCameras', a: a};
};
var $elm$core$Platform$Sub$map = _Platform_map;
var $author$project$Cameras$RegisteredAssociatedMsg = function (a) {
	return {$: 'RegisteredAssociatedMsg', a: a};
};
var $author$project$Cameras$receiveRegisteredAssociatedCameras = _Platform_incomingPort('receiveRegisteredAssociatedCameras', $elm$json$Json$Decode$value);
var $author$project$Cameras$registeredAssociatedSub = $author$project$Cameras$receiveRegisteredAssociatedCameras($author$project$Cameras$RegisteredAssociatedMsg);
var $author$project$Main$receiveRegisteredAssociatedCameras = A2($elm$core$Platform$Sub$map, $author$project$Main$ReceivedRegAssocCameras, $author$project$Cameras$registeredAssociatedSub);
var $author$project$Cameras$recentDifference = function (_v0) {
	var registry = _v0.b;
	return registry.recentChanges;
};
var $author$project$Main$registeredCamerasChanged = function (model) {
	var getCameras = function (mdl) {
		switch (mdl.$) {
			case 'Errored':
				return $elm$core$Maybe$Nothing;
			case 'Kaput':
				return $elm$core$Maybe$Nothing;
			case 'Starting':
				var maybeCameras = mdl.a.maybeCameras;
				return maybeCameras;
			default:
				var cameras = mdl.a;
				return $elm$core$Maybe$Just(cameras);
		}
	};
	var _v0 = getCameras(model);
	if (_v0.$ === 'Just') {
		var cameras = _v0.a;
		var _v1 = $author$project$Cameras$recentDifference(cameras);
		if (_v1.$ === 'Just') {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
};
var $author$project$Main$subscriptions = function (model) {
	return $elm$core$Platform$Sub$batch(
		_Utils_ap(
			$author$project$Main$registeredCamerasChanged(model) ? _List_fromArray(
				[
					A2($elm$time$Time$every, 100, $author$project$Main$TryToUnblockCameraToggleButtons)
				]) : _List_Nil,
			_List_fromArray(
				[
					$author$project$Main$receiveLifeCycle($author$project$Main$ReceivedLifeCycle),
					$author$project$Main$receiveRegisteredAssociatedCameras,
					$author$project$Main$receiveDevoir($author$project$Main$ReceivedDevoir),
					$author$project$Main$receivePixelsMeta($author$project$Main$ReceivedPixelsMeta),
					$author$project$Main$receiveKaput($author$project$Main$ReceivedKaput)
				])));
};
var $author$project$Kaput$AfterRunning = F4(
	function (a, b, c, d) {
		return {$: 'AfterRunning', a: a, b: b, c: c, d: d};
	});
var $author$project$Kaput$BeforeRunning = F3(
	function (a, b, c) {
		return {$: 'BeforeRunning', a: a, b: b, c: c};
	});
var $author$project$PopUp$CamerasPopUp = {$: 'CamerasPopUp'};
var $author$project$Main$DelayedLifeCycle = function (a) {
	return {$: 'DelayedLifeCycle', a: a};
};
var $author$project$Main$DelayedPixelsMeta = function (a) {
	return {$: 'DelayedPixelsMeta', a: a};
};
var $author$project$Main$DelayedRegAssocCameras = function (a) {
	return {$: 'DelayedRegAssocCameras', a: a};
};
var $author$project$Main$DelayedShutterbug = function (a) {
	return {$: 'DelayedShutterbug', a: a};
};
var $author$project$PopUp$InfoPopUp = {$: 'InfoPopUp'};
var $author$project$Main$Kaput = function (a) {
	return {$: 'Kaput', a: a};
};
var $author$project$Main$Running = F4(
	function (a, b, c, d) {
		return {$: 'Running', a: a, b: b, c: c, d: d};
	});
var $author$project$PopUp$SettingsPopUp = {$: 'SettingsPopUp'};
var $author$project$Main$ToggledCamerasPopUp = {$: 'ToggledCamerasPopUp'};
var $author$project$PopUp$cameras = function (model) {
	var _v0 = model.pixelsArea;
	if ((_v0.$ === 'Just') && (_v0.a.$ === 'CamerasPopUp')) {
		var _v1 = _v0.a;
		return true;
	} else {
		return false;
	}
};
var $author$project$PopUp$changeTab = F2(
	function (model, _v0) {
		var tab = _v0.a;
		return _Utils_update(
			model,
			{infoTab: tab});
	});
var $author$project$Cameras$Model = F2(
	function (a, b) {
		return {$: 'Model', a: a, b: b};
	});
var $author$project$Stitch$Uni = function (a) {
	return {$: 'Uni', a: a};
};
var $author$project$Cameras$clearSelection = function (_v0) {
	var mode = _v0.a;
	var registry = _v0.b;
	return A2(
		$author$project$Cameras$Model,
		mode,
		_Utils_update(
			registry,
			{
				selection: $author$project$Stitch$Uni($elm$core$Maybe$Nothing)
			}));
};
var $author$project$LifeCycle$decodeProblem = A2($elm$json$Json$Decode$field, 'problem', $elm$json$Json$Decode$string);
var $author$project$LifeCycle$decodeStage = A2($elm$json$Json$Decode$field, 'stage', $elm$json$Json$Decode$string);
var $author$project$LifeCycle$CanvasContextCreationFailure = {$: 'CanvasContextCreationFailure'};
var $author$project$LifeCycle$GetTypescriptMotifFailure = {$: 'GetTypescriptMotifFailure'};
var $author$project$LifeCycle$ShadowRootCreationFailure = {$: 'ShadowRootCreationFailure'};
var $author$project$LifeCycle$problemToEvent = function (problem) {
	switch (problem) {
		case 'failed to create a canvas rendering context':
			return $elm$core$Result$Ok($author$project$LifeCycle$CanvasContextCreationFailure);
		case 'failed to get the Typescript motif':
			return $elm$core$Result$Ok($author$project$LifeCycle$GetTypescriptMotifFailure);
		case 'failed to create a shadowRoot':
			return $elm$core$Result$Ok($author$project$LifeCycle$ShadowRootCreationFailure);
		default:
			return $elm$core$Result$Err('this problem is not accounted for: ' + problem);
	}
};
var $author$project$LifeCycle$AllWebSocketsAreOpen = {$: 'AllWebSocketsAreOpen'};
var $author$project$LifeCycle$stageToEvent = function (stage) {
	if (stage === 'all WebSockets are open') {
		return $elm$core$Result$Ok($author$project$LifeCycle$AllWebSocketsAreOpen);
	} else {
		return $elm$core$Result$Err('this stage is not accounted for: ' + stage);
	}
};
var $author$project$LifeCycle$decodeLifeCycleMessage = $elm$json$Json$Decode$oneOf(
	_List_fromArray(
		[
			A2($elm$json$Json$Decode$map, $author$project$LifeCycle$problemToEvent, $author$project$LifeCycle$decodeProblem),
			A2($elm$json$Json$Decode$map, $author$project$LifeCycle$stageToEvent, $author$project$LifeCycle$decodeStage)
		]));
var $author$project$Cameras$RegisteredAssociated = F2(
	function (associated, registered) {
		return {associated: associated, registered: registered};
	});
var $author$project$Cameras$RegisteredAssociatedSchema = F3(
	function (associated, registered, stitching) {
		return {associated: associated, registered: registered, stitching: stitching};
	});
var $author$project$Stitch$BiPartialStitching = {$: 'BiPartialStitching'};
var $author$project$Stitch$BiStitching = {$: 'BiStitching'};
var $author$project$Stitch$QuadPartialStitching = {$: 'QuadPartialStitching'};
var $author$project$Stitch$QuadStitching = {$: 'QuadStitching'};
var $author$project$Stitch$TriPartialStitching = {$: 'TriPartialStitching'};
var $author$project$Stitch$TriStitching = {$: 'TriStitching'};
var $author$project$Stitch$UniStitching = {$: 'UniStitching'};
var $author$project$Stitch$bi = 'bi';
var $author$project$Stitch$biPartial = 'biPartial';
var $author$project$Stitch$quad = 'quad';
var $author$project$Stitch$quadPartial = 'quadPartial';
var $author$project$Stitch$tri = 'tri';
var $author$project$Stitch$triPartial = 'triPartial';
var $author$project$Stitch$uni = 'uni';
var $author$project$Stitch$decode = A2(
	$elm$json$Json$Decode$andThen,
	function (str) {
		return _Utils_eq(str, $author$project$Stitch$uni) ? $elm$json$Json$Decode$succeed($author$project$Stitch$UniStitching) : (_Utils_eq(str, $author$project$Stitch$bi) ? $elm$json$Json$Decode$succeed($author$project$Stitch$BiStitching) : (_Utils_eq(str, $author$project$Stitch$biPartial) ? $elm$json$Json$Decode$succeed($author$project$Stitch$BiPartialStitching) : (_Utils_eq(str, $author$project$Stitch$tri) ? $elm$json$Json$Decode$succeed($author$project$Stitch$TriStitching) : (_Utils_eq(str, $author$project$Stitch$triPartial) ? $elm$json$Json$Decode$succeed($author$project$Stitch$TriPartialStitching) : (_Utils_eq(str, $author$project$Stitch$quad) ? $elm$json$Json$Decode$succeed($author$project$Stitch$QuadStitching) : (_Utils_eq(str, $author$project$Stitch$quadPartial) ? $elm$json$Json$Decode$succeed($author$project$Stitch$QuadPartialStitching) : $elm$json$Json$Decode$fail('unknown stitching: ' + str)))))));
	},
	$elm$json$Json$Decode$string);
var $author$project$Camera$Model = function (a) {
	return {$: 'Model', a: a};
};
var $author$project$Camera$init = function (name) {
	return $author$project$Camera$Model(name);
};
var $author$project$Cameras$decodeListCameras = A2(
	$elm$json$Json$Decode$andThen,
	function (ss) {
		return $elm$json$Json$Decode$succeed(
			A2($elm$core$List$map, $author$project$Camera$init, ss));
	},
	$elm$json$Json$Decode$list($elm$json$Json$Decode$string));
var $author$project$Stitch$Bi = F2(
	function (a, b) {
		return {$: 'Bi', a: a, b: b};
	});
var $author$project$Stitch$BiPartial = F2(
	function (a, b) {
		return {$: 'BiPartial', a: a, b: b};
	});
var $author$project$Stitch$Quad = F4(
	function (a, b, c, d) {
		return {$: 'Quad', a: a, b: b, c: c, d: d};
	});
var $author$project$Stitch$QuadPartial = F4(
	function (a, b, c, d) {
		return {$: 'QuadPartial', a: a, b: b, c: c, d: d};
	});
var $author$project$Stitch$Tri = F3(
	function (a, b, c) {
		return {$: 'Tri', a: a, b: b, c: c};
	});
var $author$project$Stitch$TriPartial = F3(
	function (a, b, c) {
		return {$: 'TriPartial', a: a, b: b, c: c};
	});
var $author$project$Stitch$checkAssociatedCamerasLength = F2(
	function (stitching, length) {
		switch (stitching.$) {
			case 'UniStitching':
				return ((!length) || (length === 1)) ? $elm$core$Result$Ok(_Utils_Tuple0) : $elm$core$Result$Err('uni stitching expects 0 or 1 associated cameras');
			case 'BiStitching':
				if (length === 2) {
					return $elm$core$Result$Ok(_Utils_Tuple0);
				} else {
					return $elm$core$Result$Err('bi stitching expects 2 associated cameras');
				}
			case 'BiPartialStitching':
				if (length === 2) {
					return $elm$core$Result$Ok(_Utils_Tuple0);
				} else {
					return $elm$core$Result$Err('biPartial stitching expects 2 associated cameras');
				}
			case 'TriStitching':
				if (length === 3) {
					return $elm$core$Result$Ok(_Utils_Tuple0);
				} else {
					return $elm$core$Result$Err('tri stitching expects 3 associated cameras');
				}
			case 'TriPartialStitching':
				if (length === 3) {
					return $elm$core$Result$Ok(_Utils_Tuple0);
				} else {
					return $elm$core$Result$Err('triPartial stitching expects 3 associated cameras');
				}
			case 'QuadStitching':
				if (length === 4) {
					return $elm$core$Result$Ok(_Utils_Tuple0);
				} else {
					return $elm$core$Result$Err('quad stitching expects 4 associated cameras');
				}
			default:
				if (length === 4) {
					return $elm$core$Result$Ok(_Utils_Tuple0);
				} else {
					return $elm$core$Result$Err('quadPartial stitching expects 4 associated cameras');
				}
		}
	});
var $author$project$Stitch$initAssociations = F2(
	function (stitching, associations) {
		var _v0 = A2(
			$author$project$Stitch$checkAssociatedCamerasLength,
			stitching,
			$elm$core$List$length(associations));
		if (_v0.$ === 'Ok') {
			_v1$4:
			while (true) {
				if (associations.b) {
					if (!associations.b.b) {
						var major = associations.a;
						return $elm$core$Result$Ok(
							$author$project$Stitch$Uni(
								$elm$core$Maybe$Just(major)));
					} else {
						if (!associations.b.b.b) {
							var major = associations.a;
							var _v2 = associations.b;
							var minor = _v2.a;
							if (stitching.$ === 'BiStitching') {
								return $elm$core$Result$Ok(
									A2($author$project$Stitch$Bi, major, minor));
							} else {
								return $elm$core$Result$Ok(
									A2($author$project$Stitch$BiPartial, major, minor));
							}
						} else {
							if (!associations.b.b.b.b) {
								var major = associations.a;
								var _v4 = associations.b;
								var minor1 = _v4.a;
								var _v5 = _v4.b;
								var minor2 = _v5.a;
								if (stitching.$ === 'TriStitching') {
									return $elm$core$Result$Ok(
										A3($author$project$Stitch$Tri, major, minor1, minor2));
								} else {
									return $elm$core$Result$Ok(
										A3($author$project$Stitch$TriPartial, major, minor1, minor2));
								}
							} else {
								if (!associations.b.b.b.b.b) {
									var major = associations.a;
									var _v7 = associations.b;
									var minor1 = _v7.a;
									var _v8 = _v7.b;
									var minor2 = _v8.a;
									var _v9 = _v8.b;
									var major3 = _v9.a;
									if (stitching.$ === 'QuadStitching') {
										return $elm$core$Result$Ok(
											A4($author$project$Stitch$Quad, major, minor1, minor2, major3));
									} else {
										return $elm$core$Result$Ok(
											A4($author$project$Stitch$QuadPartial, major, minor1, minor2, major3));
									}
								} else {
									break _v1$4;
								}
							}
						}
					}
				} else {
					break _v1$4;
				}
			}
			return $elm$core$Result$Ok(
				$author$project$Stitch$Uni($elm$core$Maybe$Nothing));
		} else {
			var err = _v0.a;
			return $elm$core$Result$Err(err);
		}
	});
var $elm$json$Json$Decode$map3 = _Json_map3;
var $author$project$Camera$nameOf = function (_v0) {
	var name = _v0.a;
	return name;
};
var $author$project$Camera$namesOf = function (cams) {
	return A2(
		$elm$core$List$map,
		function (cam) {
			return $author$project$Camera$nameOf(cam);
		},
		cams);
};
var $author$project$Cameras$decodeRegisteredAssociatedMessage = function (_v0) {
	var value = _v0.a;
	var _v1 = A2(
		$elm$json$Json$Decode$decodeValue,
		A4(
			$elm$json$Json$Decode$map3,
			$author$project$Cameras$RegisteredAssociatedSchema,
			A2(
				$elm$json$Json$Decode$at,
				_List_fromArray(
					['associated']),
				$author$project$Cameras$decodeListCameras),
			A2(
				$elm$json$Json$Decode$at,
				_List_fromArray(
					['registered']),
				$author$project$Cameras$decodeListCameras),
			A2(
				$elm$json$Json$Decode$at,
				_List_fromArray(
					['stitching']),
				$author$project$Stitch$decode)),
		value);
	if (_v1.$ === 'Ok') {
		var registeredCameras = _v1.a;
		var _v2 = A2(
			$author$project$Stitch$initAssociations,
			registeredCameras.stitching,
			$author$project$Camera$namesOf(registeredCameras.associated));
		if (_v2.$ === 'Ok') {
			var associations = _v2.a;
			return $elm$core$Result$Ok(
				A2($author$project$Cameras$RegisteredAssociated, associations, registeredCameras.registered));
		} else {
			var err = _v2.a;
			return $elm$core$Result$Err(err);
		}
	} else {
		var err = _v1.a;
		return $elm$core$Result$Err(
			$elm$json$Json$Decode$errorToString(err));
	}
};
var $author$project$LifeCycle$WSCloseEvent = F4(
	function (code, reason, sourceSocket, wasClean) {
		return {code: code, reason: reason, sourceSocket: sourceSocket, wasClean: wasClean};
	});
var $elm$json$Json$Decode$bool = _Json_decodeBool;
var $author$project$LifeCycle$decodeWSCloseEvent = A5(
	$elm$json$Json$Decode$map4,
	$author$project$LifeCycle$WSCloseEvent,
	A2($elm$json$Json$Decode$field, 'code', $elm$json$Json$Decode$int),
	A2($elm$json$Json$Decode$field, 'reason', $elm$json$Json$Decode$string),
	A2($elm$json$Json$Decode$field, 'sourceWebSocket', $elm$json$Json$Decode$string),
	A2($elm$json$Json$Decode$field, 'wasClean', $elm$json$Json$Decode$bool));
var $elm$core$Basics$always = F2(
	function (a, _v0) {
		return a;
	});
var $elm$core$Process$sleep = _Process_sleep;
var $author$project$Main$delayMsg = F2(
	function (delay, msg) {
		return A2(
			$elm$core$Task$perform,
			$elm$core$Basics$always(msg),
			$elm$core$Process$sleep(delay));
	});
var $author$project$Devoir$devoirDecoder = A2($elm$json$Json$Decode$field, 'isPaused', $elm$json$Json$Decode$bool);
var $author$project$PixScreen$Model = function (a) {
	return {$: 'Model', a: a};
};
var $author$project$PixScreen$disableTextSelect = function (_v0) {
	var data = _v0.a;
	return $author$project$PixScreen$Model(
		_Utils_update(
			data,
			{textSelect: false}));
};
var $elm$core$String$foldl = _String_foldl;
var $elm$core$String$cons = _String_cons;
var $elm$core$String$fromChar = function (_char) {
	return A2($elm$core$String$cons, _char, '');
};
var $elm$core$String$lines = _String_lines;
var $elm$core$String$right = F2(
	function (n, string) {
		return (n < 1) ? '' : A3(
			$elm$core$String$slice,
			-n,
			$elm$core$String$length(string),
			string);
	});
var $author$project$My$Http$formatLongError = function (err) {
	var _v0 = A3(
		$elm$core$String$foldl,
		F2(
			function (_char, _v1) {
				var s = _v1.a;
				var n = _v1.b;
				return _Utils_eq(
					_char,
					_Utils_chr(' ')) ? ((!n) ? _Utils_Tuple2(s + ' ', 1) : _Utils_Tuple2(s, n + 1)) : _Utils_Tuple2(
					_Utils_ap(
						s,
						$elm$core$String$fromChar(_char)),
					0);
			}),
		_Utils_Tuple2('', 0),
		err);
	var substantial = _v0.a;
	return ($elm$core$String$length(substantial) > 300) ? A2(
		$elm$core$List$map,
		function (line) {
			return ($elm$core$String$length(line) > 150) ? (A2($elm$core$String$left, 30, line) + (' ... ' + A2($elm$core$String$right, 30, line))) : line;
		},
		$elm$core$String$lines(substantial)) : _List_fromArray(
		[err]);
};
var $author$project$Shutterbug$Model = function (a) {
	return {$: 'Model', a: a};
};
var $author$project$Shutterbug$fromString = function (shutterbug) {
	return $author$project$Shutterbug$Model(shutterbug);
};
var $author$project$Theme$withInitialDefault = function (maybeTheme) {
	return A2($elm$core$Maybe$withDefault, $author$project$Theme$initial, maybeTheme);
};
var $author$project$Main$handleError = F2(
	function (model, err) {
		switch (model.$) {
			case 'Errored':
				var theme = model.a;
				var errors = model.b;
				return _Utils_Tuple2(
					A2(
						$author$project$Main$Errored,
						theme,
						_Utils_ap(
							errors,
							_List_fromArray(
								[err]))),
					$elm$core$Platform$Cmd$none);
			case 'Kaput':
				if (model.a.$ === 'BeforeRunning') {
					var _v1 = model.a;
					var reason = _v1.a;
					var errors = _v1.b;
					var popups = _v1.c;
					return _Utils_Tuple2(
						$author$project$Main$Kaput(
							function (errs) {
								return A3($author$project$Kaput$BeforeRunning, reason, errs, popups);
							}(
								_Utils_ap(
									errors,
									_List_fromArray(
										[err])))),
						$elm$core$Platform$Cmd$none);
				} else {
					var _v2 = model.a;
					var reason = _v2.a;
					var errors = _v2.b;
					var screen = _v2.c;
					var popups = _v2.d;
					return _Utils_Tuple2(
						$author$project$Main$Kaput(
							function (errs) {
								return A4($author$project$Kaput$AfterRunning, reason, errs, screen, popups);
							}(
								_Utils_ap(
									errors,
									_List_fromArray(
										[err])))),
						$elm$core$Platform$Cmd$none);
				}
			case 'Starting':
				var data = model.a;
				return _Utils_Tuple2(
					A2(
						$author$project$Main$Errored,
						$author$project$Theme$withInitialDefault(data.maybeTheme),
						_List_fromArray(
							[err])),
					$elm$core$Platform$Cmd$none);
			default:
				var data = model.d;
				return _Utils_Tuple2(
					A2(
						$author$project$Main$Errored,
						data.theme,
						_List_fromArray(
							[err])),
					$elm$core$Platform$Cmd$none);
		}
	});
var $author$project$Main$handleMsgError = F2(
	function (model, msgDescription) {
		return A2(
			$author$project$Main$handleError,
			model,
			_List_fromArray(
				['received ' + (msgDescription + ' message from an invalid state')]));
	});
var $author$project$Cameras$incrementSelectionSession = function (_v0) {
	var mode = _v0.a;
	var registry = _v0.b;
	return A2(
		$author$project$Cameras$Model,
		mode,
		_Utils_update(
			registry,
			{selection: registry.associated, selectionSession: registry.selectionSession + 1}));
};
var $author$project$My$Http$errorToString = function (error) {
	switch (error.$) {
		case 'BadBody':
			var message = error.a;
			return 'bad body: ' + message;
		case 'BadStatus':
			var code = error.a;
			return 'bad status: ' + $elm$core$String$fromInt(code);
		case 'BadUrl':
			var url = error.a;
			return 'bad URL: ' + url;
		case 'NetworkError':
			return 'network error';
		default:
			return 'timeout';
	}
};
var $ianmackenzie$elm_interval$Interval$contains = F2(
	function (value, _v0) {
		var _v1 = _v0.a;
		var a = _v1.a;
		var b = _v1.b;
		return (_Utils_cmp(a, value) < 1) && (_Utils_cmp(value, b) < 1);
	});
var $ianmackenzie$elm_interval$Interval$Interval = function (a) {
	return {$: 'Interval', a: a};
};
var $ianmackenzie$elm_interval$Interval$from = F2(
	function (firstValue, secondValue) {
		return (_Utils_cmp(firstValue, secondValue) < 1) ? $ianmackenzie$elm_interval$Interval$Interval(
			_Utils_Tuple2(firstValue, secondValue)) : $ianmackenzie$elm_interval$Interval$Interval(
			_Utils_Tuple2(secondValue, firstValue));
	});
var $author$project$Shutterbug$input = 32;
var $author$project$Shutterbug$maxLength = ((($author$project$Shutterbug$input + 2) / 3) | 0) * 4;
var $author$project$Shutterbug$minLength = ((($author$project$Shutterbug$input * 4) + 2) / 3) | 0;
var $author$project$Shutterbug$validateLength = function (shutterbug) {
	return A2(
		$ianmackenzie$elm_interval$Interval$contains,
		$elm$core$String$length(shutterbug),
		A2($ianmackenzie$elm_interval$Interval$from, $author$project$Shutterbug$minLength, $author$project$Shutterbug$maxLength)) ? $elm$core$Result$Ok(shutterbug) : $elm$core$Result$Err('the shutterbug\'s length is invalid');
};
var $author$project$Shutterbug$init = function (msg) {
	if (msg.a.$ === 'Ok') {
		var shutterbug = msg.a.a;
		var _v1 = $author$project$Shutterbug$validateLength(shutterbug);
		if (_v1.$ === 'Ok') {
			return $elm$core$Result$Ok(
				$author$project$Shutterbug$Model(shutterbug));
		} else {
			var err = _v1.a;
			return $elm$core$Result$Err(err);
		}
	} else {
		var err = msg.a.a;
		return $elm$core$Result$Err(
			$author$project$My$Http$errorToString(err));
	}
};
var $author$project$Static$About$init = function (_v0) {
	var msg = _v0.a;
	if (msg.$ === 'Ok') {
		var model = msg.a;
		return $elm$core$Result$Ok(model);
	} else {
		var err = msg.a;
		return $elm$core$Result$Err(
			$author$project$My$Http$errorToString(err));
	}
};
var $author$project$Static$Motif$init = function (_v0) {
	var msg = _v0.a;
	if (msg.$ === 'Ok') {
		var model = msg.a;
		return $elm$core$Result$Ok(model);
	} else {
		var err = msg.a;
		return $elm$core$Result$Err(
			$author$project$My$Http$errorToString(err));
	}
};
var $author$project$PixScreen$Meta = F4(
	function (w, h, fullscreen, pointerLock) {
		return {fullscreen: fullscreen, h: h, pointerLock: pointerLock, w: w};
	});
var $author$project$PixScreen$metaDecoder = A5(
	$elm$json$Json$Decode$map4,
	$author$project$PixScreen$Meta,
	A2($elm$json$Json$Decode$field, 'w', $elm$json$Json$Decode$int),
	A2($elm$json$Json$Decode$field, 'h', $elm$json$Json$Decode$int),
	A2($elm$json$Json$Decode$field, 'fullscreen', $elm$json$Json$Decode$bool),
	A2($elm$json$Json$Decode$field, 'pointerLock', $elm$json$Json$Decode$bool));
var $elm$json$Json$Encode$list = F2(
	function (func, entries) {
		return _Json_wrap(
			A3(
				$elm$core$List$foldl,
				_Json_addEntry(func),
				_Json_emptyArray(_Utils_Tuple0),
				entries));
	});
var $elm$json$Json$Encode$object = function (pairs) {
	return _Json_wrap(
		A3(
			$elm$core$List$foldl,
			F2(
				function (_v0, obj) {
					var k = _v0.a;
					var v = _v0.b;
					return A3(_Json_addField, k, v, obj);
				}),
			_Json_emptyObject(_Utils_Tuple0),
			pairs));
};
var $elm$json$Json$Encode$string = _Json_wrap;
var $author$project$Main$sendCameraAssociations = _Platform_outgoingPort(
	'sendCameraAssociations',
	function ($) {
		return $elm$json$Json$Encode$object(
			_List_fromArray(
				[
					_Utils_Tuple2(
					'cameras',
					$elm$json$Json$Encode$list($elm$json$Json$Encode$string)($.cameras)),
					_Utils_Tuple2(
					'stitching',
					$elm$json$Json$Encode$string($.stitching))
				]));
	});
var $elm$json$Json$Encode$bool = _Json_wrap;
var $author$project$Main$sendFullscreen = _Platform_outgoingPort('sendFullscreen', $elm$json$Json$Encode$bool);
var $author$project$Main$sendOpenNewTab = _Platform_outgoingPort('sendOpenNewTab', $elm$json$Json$Encode$string);
var $author$project$Main$sendPointerLock = _Platform_outgoingPort('sendPointerLock', $elm$json$Json$Encode$bool);
var $author$project$Main$sendUICommand = _Platform_outgoingPort(
	'sendUICommand',
	function ($) {
		return $elm$json$Json$Encode$object(
			_List_fromArray(
				[
					_Utils_Tuple2(
					'uiCommand',
					$elm$json$Json$Encode$string($.uiCommand))
				]));
	});
var $elm$json$Json$Encode$null = _Json_encodeNull;
var $author$project$Main$sendWriteShutterbugToClipboard = _Platform_outgoingPort(
	'sendWriteShutterbugToClipboard',
	function ($) {
		return $elm$json$Json$Encode$null;
	});
var $author$project$Main$sendWriteTextToClipboard = _Platform_outgoingPort('sendWriteTextToClipboard', $elm$json$Json$Encode$string);
var $elm$core$Set$Set_elm_builtin = function (a) {
	return {$: 'Set_elm_builtin', a: a};
};
var $elm$core$Set$empty = $elm$core$Set$Set_elm_builtin($elm$core$Dict$empty);
var $elm$core$Set$insert = F2(
	function (key, _v0) {
		var dict = _v0.a;
		return $elm$core$Set$Set_elm_builtin(
			A3($elm$core$Dict$insert, key, _Utils_Tuple0, dict));
	});
var $elm$core$Set$fromList = function (list) {
	return A3($elm$core$List$foldl, $elm$core$Set$insert, $elm$core$Set$empty, list);
};
var $elm$core$Dict$filter = F2(
	function (isGood, dict) {
		return A3(
			$elm$core$Dict$foldl,
			F3(
				function (k, v, d) {
					return A2(isGood, k, v) ? A3($elm$core$Dict$insert, k, v, d) : d;
				}),
			$elm$core$Dict$empty,
			dict);
	});
var $elm$core$Dict$member = F2(
	function (key, dict) {
		var _v0 = A2($elm$core$Dict$get, key, dict);
		if (_v0.$ === 'Just') {
			return true;
		} else {
			return false;
		}
	});
var $elm$core$Dict$intersect = F2(
	function (t1, t2) {
		return A2(
			$elm$core$Dict$filter,
			F2(
				function (k, _v0) {
					return A2($elm$core$Dict$member, k, t2);
				}),
			t1);
	});
var $elm$core$Set$intersect = F2(
	function (_v0, _v1) {
		var dict1 = _v0.a;
		var dict2 = _v1.a;
		return $elm$core$Set$Set_elm_builtin(
			A2($elm$core$Dict$intersect, dict1, dict2));
	});
var $author$project$Stitch$names = function (associations) {
	switch (associations.$) {
		case 'Uni':
			var maybeMajor = associations.a;
			if (maybeMajor.$ === 'Just') {
				var major = maybeMajor.a;
				return _List_fromArray(
					[major]);
			} else {
				return _List_Nil;
			}
		case 'Bi':
			var major = associations.a;
			var minor = associations.b;
			return _List_fromArray(
				[major, minor]);
		case 'BiPartial':
			var major = associations.a;
			var minor = associations.b;
			return _List_fromArray(
				[major, minor]);
		case 'Tri':
			var major = associations.a;
			var minor1 = associations.b;
			var minor2 = associations.c;
			return _List_fromArray(
				[major, minor1, minor2]);
		case 'TriPartial':
			var major = associations.a;
			var minor1 = associations.b;
			var minor2 = associations.c;
			return _List_fromArray(
				[major, minor1, minor2]);
		case 'Quad':
			var major = associations.a;
			var minor1 = associations.b;
			var minor2 = associations.c;
			var minor3 = associations.d;
			return _List_fromArray(
				[major, minor1, minor2, minor3]);
		default:
			var major = associations.a;
			var minor1 = associations.b;
			var minor2 = associations.c;
			var minor3 = associations.d;
			return _List_fromArray(
				[major, minor1, minor2, minor3]);
	}
};
var $author$project$Stitch$setNames = F2(
	function (associations, cameras) {
		if (_Utils_eq(
			$elm$core$List$length(
				$author$project$Stitch$names(associations)),
			$elm$core$List$length(cameras))) {
			if (cameras.b) {
				if (cameras.b.b) {
					if (cameras.b.b.b) {
						if (cameras.b.b.b.b) {
							if (!cameras.b.b.b.b.b) {
								var major = cameras.a;
								var _v1 = cameras.b;
								var minor1 = _v1.a;
								var _v2 = _v1.b;
								var minor2 = _v2.a;
								var _v3 = _v2.b;
								var minor3 = _v3.a;
								if (associations.$ === 'Quad') {
									return A4($author$project$Stitch$Quad, major, minor1, minor2, minor3);
								} else {
									return A4($author$project$Stitch$QuadPartial, major, minor1, minor2, minor3);
								}
							} else {
								return associations;
							}
						} else {
							var major = cameras.a;
							var _v5 = cameras.b;
							var minor1 = _v5.a;
							var _v6 = _v5.b;
							var minor2 = _v6.a;
							if (associations.$ === 'Tri') {
								return A3($author$project$Stitch$Tri, major, minor1, minor2);
							} else {
								return A3($author$project$Stitch$TriPartial, major, minor1, minor2);
							}
						}
					} else {
						var major = cameras.a;
						var _v8 = cameras.b;
						var minor = _v8.a;
						if (associations.$ === 'Bi') {
							return A2($author$project$Stitch$Bi, major, minor);
						} else {
							return A2($author$project$Stitch$BiPartial, major, minor);
						}
					}
				} else {
					var major = cameras.a;
					return $author$project$Stitch$Uni(
						$elm$core$Maybe$Just(major));
				}
			} else {
				return $author$project$Stitch$Uni($elm$core$Maybe$Nothing);
			}
		} else {
			return associations;
		}
	});
var $elm$core$Dict$sizeHelp = F2(
	function (n, dict) {
		sizeHelp:
		while (true) {
			if (dict.$ === 'RBEmpty_elm_builtin') {
				return n;
			} else {
				var left = dict.d;
				var right = dict.e;
				var $temp$n = A2($elm$core$Dict$sizeHelp, n + 1, right),
					$temp$dict = left;
				n = $temp$n;
				dict = $temp$dict;
				continue sizeHelp;
			}
		}
	});
var $elm$core$Dict$size = function (dict) {
	return A2($elm$core$Dict$sizeHelp, 0, dict);
};
var $elm$core$Set$size = function (_v0) {
	var dict = _v0.a;
	return $elm$core$Dict$size(dict);
};
var $author$project$Stitch$switch = F3(
	function (associations, camera1, camera2) {
		return ($elm$core$Set$size(
			A2(
				$elm$core$Set$intersect,
				$elm$core$Set$fromList(
					$author$project$Stitch$names(associations)),
				$elm$core$Set$fromList(
					_List_fromArray(
						[camera1, camera2])))) === 2) ? A2(
			$author$project$Stitch$setNames,
			associations,
			A2(
				$elm$core$List$map,
				function (camera) {
					return _Utils_eq(camera, camera1) ? camera2 : (_Utils_eq(camera, camera2) ? camera1 : camera);
				},
				$author$project$Stitch$names(associations))) : associations;
	});
var $author$project$Cameras$switchCameras = F2(
	function (_v0, _v1) {
		var mode = _v0.a;
		var registry = _v0.b;
		var cam1 = _v1.a;
		var cam2 = _v1.b;
		return A2(
			$author$project$Cameras$Model,
			mode,
			_Utils_update(
				registry,
				{
					selection: A3($author$project$Stitch$switch, registry.selection, cam1, cam2)
				}));
	});
var $author$project$Devoir$toRecord = function (command) {
	switch (command.$) {
		case 'Pause':
			return {uiCommand: 'pause'};
		case 'Play':
			return {uiCommand: 'play'};
		default:
			return {uiCommand: 'shutdown'};
	}
};
var $author$project$PixScreen$toggleBorder = function (_v0) {
	var data = _v0.a;
	return $author$project$PixScreen$Model(
		_Utils_update(
			data,
			{borderScreen: !data.borderScreen}));
};
var $author$project$PixScreen$toggleFit = function (_v0) {
	var data = _v0.a;
	return $author$project$PixScreen$Model(
		_Utils_update(
			data,
			{fitScreen: !data.fitScreen}));
};
var $author$project$PopUp$togglePixelsAreaPopUp = F2(
	function (model, popUp) {
		var _v0 = model.pixelsArea;
		if (_v0.$ === 'Just') {
			var currentPopUp = _v0.a;
			return _Utils_eq(currentPopUp, popUp) ? _Utils_update(
				model,
				{pixelsArea: $elm$core$Maybe$Nothing}) : _Utils_update(
				model,
				{
					pixelsArea: $elm$core$Maybe$Just(popUp)
				});
		} else {
			return _Utils_update(
				model,
				{
					pixelsArea: $elm$core$Maybe$Just(popUp)
				});
		}
	});
var $author$project$Cameras$Multiple = {$: 'Multiple'};
var $author$project$Cameras$Single = {$: 'Single'};
var $elm$core$List$member = F2(
	function (x, xs) {
		return A2(
			$elm$core$List$any,
			function (a) {
				return _Utils_eq(a, x);
			},
			xs);
	});
var $author$project$Stitch$toggle = F2(
	function (associations, camera) {
		switch (associations.$) {
			case 'Uni':
				var maybeMajor = associations.a;
				if (maybeMajor.$ === 'Just') {
					var major = maybeMajor.a;
					return _Utils_eq(camera, major) ? $author$project$Stitch$Uni($elm$core$Maybe$Nothing) : A2($author$project$Stitch$BiPartial, major, camera);
				} else {
					return $author$project$Stitch$Uni(
						$elm$core$Maybe$Just(camera));
				}
			case 'Bi':
				var major = associations.a;
				var minor = associations.b;
				return A2(
					$elm$core$List$member,
					camera,
					_List_fromArray(
						[major, minor])) ? (_Utils_eq(camera, major) ? $author$project$Stitch$Uni(
					$elm$core$Maybe$Just(minor)) : $author$project$Stitch$Uni(
					$elm$core$Maybe$Just(major))) : A3($author$project$Stitch$Tri, major, minor, camera);
			case 'BiPartial':
				var major = associations.a;
				var minor = associations.b;
				return A2(
					$elm$core$List$member,
					camera,
					_List_fromArray(
						[major, minor])) ? (_Utils_eq(camera, major) ? $author$project$Stitch$Uni(
					$elm$core$Maybe$Just(minor)) : $author$project$Stitch$Uni(
					$elm$core$Maybe$Just(major))) : A3($author$project$Stitch$TriPartial, major, camera, minor);
			case 'Tri':
				var major = associations.a;
				var minor1 = associations.b;
				var minor2 = associations.c;
				return A2(
					$elm$core$List$member,
					camera,
					_List_fromArray(
						[major, minor1, minor2])) ? (_Utils_eq(camera, major) ? A2($author$project$Stitch$Bi, minor1, minor2) : (_Utils_eq(camera, minor1) ? A2($author$project$Stitch$Bi, major, minor2) : A2($author$project$Stitch$Bi, major, minor1))) : A4($author$project$Stitch$Quad, major, minor1, minor2, camera);
			case 'TriPartial':
				var major = associations.a;
				var minor1 = associations.b;
				var minor2 = associations.c;
				return A2(
					$elm$core$List$member,
					camera,
					_List_fromArray(
						[major, minor1, minor2])) ? (_Utils_eq(camera, major) ? A2($author$project$Stitch$BiPartial, minor1, minor2) : (_Utils_eq(camera, minor1) ? A2($author$project$Stitch$BiPartial, major, minor2) : A2($author$project$Stitch$BiPartial, major, minor1))) : A4($author$project$Stitch$QuadPartial, major, minor1, camera, minor2);
			case 'Quad':
				var major = associations.a;
				var minor1 = associations.b;
				var minor2 = associations.c;
				var minor3 = associations.d;
				return A2(
					$elm$core$List$member,
					camera,
					_List_fromArray(
						[major, minor1, minor2, minor3])) ? (_Utils_eq(camera, major) ? A3($author$project$Stitch$Tri, minor1, minor2, minor3) : (_Utils_eq(camera, minor1) ? A3($author$project$Stitch$Tri, major, minor2, minor3) : (_Utils_eq(camera, minor2) ? A3($author$project$Stitch$Tri, major, minor1, minor3) : A3($author$project$Stitch$Tri, major, minor1, minor2)))) : A4($author$project$Stitch$Quad, camera, major, minor1, minor2);
			default:
				var major = associations.a;
				var minor1 = associations.b;
				var minor2 = associations.c;
				var minor3 = associations.d;
				return A2(
					$elm$core$List$member,
					camera,
					_List_fromArray(
						[major, minor1, minor2, minor3])) ? (_Utils_eq(camera, major) ? A3($author$project$Stitch$TriPartial, minor1, minor2, minor3) : (_Utils_eq(camera, minor1) ? A3($author$project$Stitch$TriPartial, major, minor2, minor3) : (_Utils_eq(camera, minor2) ? A3($author$project$Stitch$TriPartial, major, minor1, minor3) : A3($author$project$Stitch$TriPartial, major, minor1, minor2)))) : A4($author$project$Stitch$QuadPartial, camera, major, minor1, minor2);
		}
	});
var $author$project$Cameras$toggleSelectionMode = function (_v0) {
	var mode = _v0.a;
	var registry = _v0.b;
	if (mode.$ === 'Single') {
		return A2($author$project$Cameras$Model, $author$project$Cameras$Multiple, registry);
	} else {
		return A2(
			$author$project$Cameras$Model,
			$author$project$Cameras$Single,
			_Utils_update(
				registry,
				{
					selection: function () {
						var _v2 = $author$project$Stitch$names(registry.selection);
						if (_v2.b) {
							var minors = _v2.b;
							return A3(
								$elm$core$List$foldl,
								F2(
									function (cam, sel) {
										return A2($author$project$Stitch$toggle, sel, cam);
									}),
								registry.selection,
								minors);
						} else {
							return registry.selection;
						}
					}()
				}));
	}
};
var $author$project$PopUp$toggleShutterbugActionsPopUp = function (model) {
	return _Utils_update(
		model,
		{shutterbugActions: !model.shutterbugActions});
};
var $author$project$Stitch$togglePartial = function (associations) {
	switch (associations.$) {
		case 'Uni':
			return associations;
		case 'Bi':
			var major = associations.a;
			var minor = associations.b;
			return A2($author$project$Stitch$BiPartial, major, minor);
		case 'BiPartial':
			var major = associations.a;
			var minor = associations.b;
			return A2($author$project$Stitch$Bi, major, minor);
		case 'Tri':
			var major = associations.a;
			var minor1 = associations.b;
			var minor2 = associations.c;
			return A3($author$project$Stitch$TriPartial, major, minor1, minor2);
		case 'TriPartial':
			var major = associations.a;
			var minor1 = associations.b;
			var minor2 = associations.c;
			return A3($author$project$Stitch$Tri, major, minor1, minor2);
		case 'Quad':
			var major = associations.a;
			var minor1 = associations.b;
			var minor2 = associations.c;
			var minor3 = associations.d;
			return A4($author$project$Stitch$QuadPartial, major, minor1, minor2, minor3);
		default:
			var major = associations.a;
			var minor1 = associations.b;
			var minor2 = associations.c;
			var minor3 = associations.d;
			return A4($author$project$Stitch$Quad, major, minor1, minor2, minor3);
	}
};
var $author$project$Cameras$toggleStitchingSize = function (_v0) {
	var mode = _v0.a;
	var registry = _v0.b;
	return A2(
		$author$project$Cameras$Model,
		mode,
		_Utils_update(
			registry,
			{
				selection: $author$project$Stitch$togglePartial(registry.selection)
			}));
};
var $author$project$PixScreen$toggleTextSelect = function (_v0) {
	var data = _v0.a;
	return $author$project$PixScreen$Model(
		_Utils_update(
			data,
			{textSelect: !data.textSelect}));
};
var $author$project$Main$UpdateCamerasTime = function (a) {
	return {$: 'UpdateCamerasTime', a: a};
};
var $author$project$Main$Connected = {$: 'Connected'};
var $elm$core$Maybe$map2 = F3(
	function (func, ma, mb) {
		if (ma.$ === 'Nothing') {
			return $elm$core$Maybe$Nothing;
		} else {
			var a = ma.a;
			if (mb.$ === 'Nothing') {
				return $elm$core$Maybe$Nothing;
			} else {
				var b = mb.a;
				return $elm$core$Maybe$Just(
					A2(func, a, b));
			}
		}
	});
var $elm_community$maybe_extra$Maybe$Extra$andMap = $elm$core$Maybe$map2($elm$core$Basics$apR);
var $author$project$PopUp$WorldTab = {$: 'WorldTab'};
var $author$project$PopUp$init = function (about) {
	return {about: about, autoCloseCameras: true, infoTab: $author$project$PopUp$WorldTab, pixelsArea: $elm$core$Maybe$Nothing, shutterbugActions: false};
};
var $author$project$Main$attemptToRun = function (data) {
	var _v0 = _Utils_Tuple2(
		A2(
			$elm_community$maybe_extra$Maybe$Extra$andMap,
			data.maybePix,
			A2(
				$elm_community$maybe_extra$Maybe$Extra$andMap,
				data.maybeDevoir,
				A2(
					$elm_community$maybe_extra$Maybe$Extra$andMap,
					data.maybeCameras,
					A2(
						$elm_community$maybe_extra$Maybe$Extra$andMap,
						data.maybeTheme,
						A2(
							$elm_community$maybe_extra$Maybe$Extra$andMap,
							data.maybeAbout,
							$elm$core$Maybe$Just(
								F5(
									function (about, motif, cameras, devoir, pix) {
										return {about: about, cameras: cameras, devoir: devoir, motif: motif, pix: pix};
									}))))))),
		_Utils_eq(data.websockets, $author$project$Main$Connected));
	if ((_v0.a.$ === 'Just') && _v0.b) {
		var justData = _v0.a.a;
		return A4(
			$author$project$Main$Running,
			justData.cameras,
			justData.pix,
			justData.devoir,
			{
				extantUrl: data.extantUrl,
				popUps: data.shutterbugActionsPopUp ? $author$project$PopUp$toggleShutterbugActionsPopUp(
					$author$project$PopUp$init(justData.about)) : $author$project$PopUp$init(justData.about),
				theme: justData.motif
			});
	} else {
		return $author$project$Main$Starting(data);
	}
};
var $author$project$Stitch$unassociated = $author$project$Stitch$Uni($elm$core$Maybe$Nothing);
var $author$project$Cameras$init = A2(
	$author$project$Cameras$Model,
	$author$project$Cameras$Single,
	{allRegistered: _List_Nil, associated: $author$project$Stitch$unassociated, recentChanges: $elm$core$Maybe$Nothing, selection: $author$project$Stitch$unassociated, selectionSession: 0});
var $author$project$Cameras$timeNeedsUpdating = function (_v0) {
	var recentChanges = _v0.b.recentChanges;
	if (recentChanges.$ === 'Just') {
		var changes = recentChanges.a;
		return changes.needToUpdateTime;
	} else {
		return false;
	}
};
var $author$project$Stitch$deselect = F2(
	function (associations, camera) {
		return A2(
			$elm$core$List$member,
			camera,
			$author$project$Stitch$names(associations)) ? A2($author$project$Stitch$toggle, associations, camera) : associations;
	});
var $elm$core$Dict$diff = F2(
	function (t1, t2) {
		return A3(
			$elm$core$Dict$foldl,
			F3(
				function (k, v, t) {
					return A2($elm$core$Dict$remove, k, t);
				}),
			t1,
			t2);
	});
var $elm$core$Set$diff = F2(
	function (_v0, _v1) {
		var dict1 = _v0.a;
		var dict2 = _v1.a;
		return $elm$core$Set$Set_elm_builtin(
			A2($elm$core$Dict$diff, dict1, dict2));
	});
var $elm$core$List$filter = F2(
	function (isGood, list) {
		return A3(
			$elm$core$List$foldr,
			F2(
				function (x, xs) {
					return isGood(x) ? A2($elm$core$List$cons, x, xs) : xs;
				}),
			_List_Nil,
			list);
	});
var $author$project$Cameras$timeZeroValue = $elm$time$Time$millisToPosix(0);
var $elm$core$Dict$union = F2(
	function (t1, t2) {
		return A3($elm$core$Dict$foldl, $elm$core$Dict$insert, t2, t1);
	});
var $elm$core$Set$union = F2(
	function (_v0, _v1) {
		var dict1 = _v0.a;
		var dict2 = _v1.a;
		return $elm$core$Set$Set_elm_builtin(
			A2($elm$core$Dict$union, dict1, dict2));
	});
var $author$project$Cameras$update = F2(
	function (regAssoc, _v0) {
		var mode = _v0.a;
		var registry = _v0.b;
		var trueRegistered = $author$project$Camera$namesOf(regAssoc.registered);
		var filterTrueRegistered = $elm$core$List$filter(
			function (cam) {
				return !A2($elm$core$List$member, cam, trueRegistered);
			});
		var removed = filterTrueRegistered(
			$author$project$Camera$namesOf(registry.allRegistered));
		var autoDisassociated = filterTrueRegistered(
			$author$project$Stitch$names(registry.associated));
		var autoDeselected = filterTrueRegistered(
			$author$project$Stitch$names(registry.selection));
		var added = A2(
			$elm$core$List$filter,
			function (cam) {
				return !A2(
					$elm$core$List$member,
					cam,
					$author$project$Camera$namesOf(registry.allRegistered));
			},
			trueRegistered);
		return A2(
			$author$project$Cameras$Model,
			function () {
				if (mode.$ === 'Single') {
					return ($elm$core$List$length(
						$author$project$Stitch$names(regAssoc.associated)) > 1) ? $author$project$Cameras$Multiple : $author$project$Cameras$Single;
				} else {
					return $author$project$Cameras$Multiple;
				}
			}(),
			{
				allRegistered: regAssoc.registered,
				associated: regAssoc.associated,
				recentChanges: function () {
					var _v2 = registry.recentChanges;
					if (_v2.$ === 'Just') {
						var changes = _v2.a;
						return $elm$core$Maybe$Just(
							{
								added: A2(
									$elm$core$List$filter,
									function (cam) {
										return !A2($elm$core$List$member, cam, removed);
									},
									_Utils_ap(added, changes.added)),
								autoDeselected: A2(
									$elm$core$Set$union,
									changes.autoDeselected,
									$elm$core$Set$fromList(autoDeselected)),
								autoDisassociated: A2(
									$elm$core$Set$diff,
									A2(
										$elm$core$Set$union,
										changes.autoDisassociated,
										$elm$core$Set$fromList(autoDisassociated)),
									$elm$core$Set$fromList(
										$author$project$Stitch$names(regAssoc.associated))),
								latestUpdate: changes.latestUpdate,
								needToUpdateTime: true,
								removed: A2(
									$elm$core$List$filter,
									function (cam) {
										return !A2($elm$core$List$member, cam, added);
									},
									_Utils_ap(removed, changes.removed))
							});
					} else {
						return (($elm$core$List$length(added) > 0) || ($elm$core$List$length(removed) > 0)) ? $elm$core$Maybe$Just(
							{
								added: added,
								autoDeselected: $elm$core$Set$fromList(autoDeselected),
								autoDisassociated: $elm$core$Set$fromList(autoDisassociated),
								latestUpdate: $author$project$Cameras$timeZeroValue,
								needToUpdateTime: true,
								removed: removed
							}) : $elm$core$Maybe$Nothing;
					}
				}(),
				selection: A3(
					$elm$core$List$foldl,
					F2(
						function (camera, associations) {
							return A2($author$project$Stitch$deselect, associations, camera);
						}),
					registry.selection,
					removed),
				selectionSession: registry.selectionSession
			});
	});
var $author$project$Main$updateCameras = F2(
	function (registeredAssociated, model) {
		var makeCmd = function (cameras) {
			return $author$project$Cameras$timeNeedsUpdating(cameras) ? A2($elm$core$Task$perform, $author$project$Main$UpdateCamerasTime, $elm$time$Time$now) : $elm$core$Platform$Cmd$none;
		};
		switch (model.$) {
			case 'Errored':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received the cameras while errored']));
			case 'Kaput':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received the cameras while kaput']));
			case 'Starting':
				var data = model.a;
				return function (cams) {
					return _Utils_Tuple2(
						$author$project$Main$attemptToRun(
							_Utils_update(
								data,
								{
									maybeCameras: $elm$core$Maybe$Just(cams)
								})),
						makeCmd(cams));
				}(
					A2(
						$author$project$Cameras$update,
						registeredAssociated,
						A2($elm$core$Maybe$withDefault, $author$project$Cameras$init, data.maybeCameras)));
			default:
				var cameras = model.a;
				var screen = model.b;
				var devoir = model.c;
				var popups = model.d;
				return function (cams) {
					return _Utils_Tuple2(
						A4($author$project$Main$Running, cams, screen, devoir, popups),
						makeCmd(cams));
				}(
					A2($author$project$Cameras$update, registeredAssociated, cameras));
		}
	});
var $author$project$Cameras$updateTime = F2(
	function (_v0, time) {
		var mode = _v0.a;
		var registry = _v0.b;
		var _v1 = registry.recentChanges;
		if (_v1.$ === 'Just') {
			var changes = _v1.a;
			return A2(
				$author$project$Cameras$Model,
				mode,
				_Utils_update(
					registry,
					{
						recentChanges: $elm$core$Maybe$Just(
							_Utils_update(
								changes,
								{latestUpdate: time, needToUpdateTime: false}))
					}));
		} else {
			return A2($author$project$Cameras$Model, mode, registry);
		}
	});
var $author$project$Main$updateCamerasTime = F2(
	function (model, now) {
		switch (model.$) {
			case 'Errored':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received the cameras time while errored']));
			case 'Kaput':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received the cameras time while kaput']));
			case 'Starting':
				var data = model.a;
				var _v1 = data.maybeCameras;
				if (_v1.$ === 'Just') {
					var cameras = _v1.a;
					return _Utils_Tuple2(
						$author$project$Main$Starting(
							_Utils_update(
								data,
								{
									maybeCameras: $elm$core$Maybe$Just(
										A2($author$project$Cameras$updateTime, cameras, now))
								})),
						$elm$core$Platform$Cmd$none);
				} else {
					return A2($author$project$Main$handleMsgError, model, 'received the cameras time early');
				}
			default:
				var cameras = model.a;
				var screen = model.b;
				var devoir = model.c;
				var popups = model.d;
				return _Utils_Tuple2(
					function (cams) {
						return A4($author$project$Main$Running, cams, screen, devoir, popups);
					}(
						A2($author$project$Cameras$updateTime, cameras, now)),
					$elm$core$Platform$Cmd$none);
		}
	});
var $author$project$Main$Paused = function (a) {
	return {$: 'Paused', a: a};
};
var $author$project$Main$updateDevoir = F2(
	function (paused, model) {
		switch (model.$) {
			case 'Errored':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received the devoir while errored']));
			case 'Kaput':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received the devoir while kaput']));
			case 'Starting':
				var data = model.a;
				return _Utils_Tuple2(
					$author$project$Main$attemptToRun(
						_Utils_update(
							data,
							{
								maybeDevoir: $elm$core$Maybe$Just(
									$author$project$Main$Paused(paused))
							})),
					$elm$core$Platform$Cmd$none);
			default:
				var cameras = model.a;
				var screen = model.b;
				var popups = model.d;
				return _Utils_Tuple2(
					function (devoir) {
						return A4($author$project$Main$Running, cameras, screen, devoir, popups);
					}(
						$author$project$Main$Paused(paused)),
					$elm$core$Platform$Cmd$none);
		}
	});
var $author$project$PopUp$hidePixelsAreaPopUp = F2(
	function (model, popUp) {
		var _v0 = model.pixelsArea;
		if (_v0.$ === 'Just') {
			var currentPopUp = _v0.a;
			return _Utils_eq(currentPopUp, popUp) ? _Utils_update(
				model,
				{pixelsArea: $elm$core$Maybe$Nothing}) : model;
		} else {
			return model;
		}
	});
var $author$project$Main$updateKaput = F2(
	function (notification, model) {
		switch (model.$) {
			case 'Errored':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received a kaput while errored']));
			case 'Kaput':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received a kaput while already kaput']));
			case 'Starting':
				var data = model.a;
				return _Utils_Tuple2(
					$author$project$Main$Kaput(
						A3(
							$author$project$Kaput$BeforeRunning,
							notification,
							_List_Nil,
							{
								extantUrl: data.extantUrl,
								shutterbugActionsPopUp: data.shutterbugActionsPopUp,
								theme: $author$project$Theme$withInitialDefault(data.maybeTheme)
							})),
					$elm$core$Platform$Cmd$none);
			default:
				var screen = model.b;
				var data = model.d;
				return _Utils_Tuple2(
					$author$project$Main$Kaput(
						A4(
							$author$project$Kaput$AfterRunning,
							notification,
							_List_Nil,
							screen,
							function (popUps) {
								return _Utils_update(
									data,
									{popUps: popUps});
							}(
								A2($author$project$PopUp$hidePixelsAreaPopUp, data.popUps, $author$project$PopUp$CamerasPopUp)))),
					$elm$core$Platform$Cmd$none);
		}
	});
var $author$project$LifeCycle$eventMessages = {
	problem: {context: 'failed to create a canvas rendering context', motif: 'failed to get the motif', shadowRoot: 'failed to create a shadowRoot'},
	stage: {webSocketsOpen: 'all WebSockets are open'}
};
var $author$project$LifeCycle$toString = function (event) {
	switch (event.$) {
		case 'CanvasContextCreationFailure':
			return $author$project$LifeCycle$eventMessages.problem.context;
		case 'GetTypescriptMotifFailure':
			return $author$project$LifeCycle$eventMessages.problem.motif;
		case 'ShadowRootCreationFailure':
			return $author$project$LifeCycle$eventMessages.problem.shadowRoot;
		default:
			return $author$project$LifeCycle$eventMessages.stage.webSocketsOpen;
	}
};
var $author$project$Main$updateLifeCycle = F2(
	function (event, model) {
		if (model.$ === 'Starting') {
			var data = model.a;
			switch (event.$) {
				case 'AllWebSocketsAreOpen':
					return _Utils_Tuple2(
						$author$project$Main$attemptToRun(
							_Utils_update(
								data,
								{websockets: $author$project$Main$Connected})),
						$elm$core$Platform$Cmd$none);
				case 'CanvasContextCreationFailure':
					return A2(
						$author$project$Main$handleError,
						model,
						_List_fromArray(
							['could not create a canvas rendering context']));
				case 'GetTypescriptMotifFailure':
					return A2(
						$author$project$Main$handleError,
						model,
						_List_fromArray(
							['could not obtain the world\'s motif from the server']));
				default:
					return A2(
						$author$project$Main$handleError,
						model,
						_List_fromArray(
							['could not create a shadowRoot']));
			}
		} else {
			return A2(
				$author$project$Main$handleError,
				model,
				_List_fromArray(
					[
						'received an unhandled life cycle event: ' + $author$project$LifeCycle$toString(event)
					]));
		}
	});
var $author$project$PixScreen$init = function (meta) {
	return $author$project$PixScreen$Model(
		{borderScreen: false, fitScreen: true, fullscreen: meta.fullscreen, h: meta.h, pointerLock: meta.pointerLock, textSelect: false, w: meta.w});
};
var $author$project$PixScreen$locked = function (_v0) {
	var pointerLock = _v0.a.pointerLock;
	return pointerLock;
};
var $author$project$PopUp$settings = function (model) {
	var _v0 = model.pixelsArea;
	if ((_v0.$ === 'Just') && (_v0.a.$ === 'SettingsPopUp')) {
		var _v1 = _v0.a;
		return true;
	} else {
		return false;
	}
};
var $author$project$PixScreen$updateMeta = F2(
	function (meta, _v0) {
		var data = _v0.a;
		return $author$project$PixScreen$Model(
			_Utils_update(
				data,
				{fullscreen: meta.fullscreen, h: meta.h, pointerLock: meta.pointerLock, w: meta.w}));
	});
var $author$project$Main$updatePixScreen = F2(
	function (meta, model) {
		switch (model.$) {
			case 'Errored':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received the pixels-meta while errored']));
			case 'Kaput':
				if (model.a.$ === 'AfterRunning') {
					var _v1 = model.a;
					var notification = _v1.a;
					var errors = _v1.b;
					var pix = _v1.c;
					var data = _v1.d;
					return _Utils_Tuple2(
						$author$project$Main$Kaput(
							A4(
								$author$project$Kaput$AfterRunning,
								notification,
								errors,
								A2($author$project$PixScreen$updateMeta, meta, pix),
								data)),
						$elm$core$Platform$Cmd$none);
				} else {
					return A2(
						$author$project$Main$handleError,
						model,
						_List_fromArray(
							['received the pixels-meta while kaput']));
				}
			case 'Starting':
				var data = model.a;
				return _Utils_Tuple2(
					$author$project$Main$attemptToRun(
						_Utils_update(
							data,
							{
								maybePix: $elm$core$Maybe$Just(
									$author$project$PixScreen$init(meta))
							})),
					$elm$core$Platform$Cmd$none);
			default:
				var cameras = model.a;
				var screen = model.b;
				var devoir = model.c;
				var popups = model.d;
				return _Utils_Tuple2(
					function (_v2) {
						var pixels = _v2.a;
						var pops = _v2.b;
						return A4($author$project$Main$Running, cameras, pixels, devoir, pops);
					}(
						_Utils_Tuple2(
							A2($author$project$PixScreen$updateMeta, meta, screen),
							(meta.pointerLock && ((!$author$project$PixScreen$locked(screen)) && $author$project$PopUp$settings(popups.popUps))) ? function (pops) {
								return _Utils_update(
									popups,
									{popUps: pops});
							}(
								A2($author$project$PopUp$togglePixelsAreaPopUp, popups.popUps, $author$project$PopUp$SettingsPopUp)) : popups)),
					$elm$core$Platform$Cmd$none);
		}
	});
var $author$project$Main$sendShutterbug = _Platform_outgoingPort('sendShutterbug', $elm$json$Json$Encode$string);
var $author$project$Shutterbug$toString = function (model) {
	var shutterbug = model.a;
	return shutterbug;
};
var $author$project$Main$updateShutterbug = F2(
	function (shutterbug, model) {
		switch (model.$) {
			case 'Errored':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received a shutterbug while errored']));
			case 'Kaput':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received a shutterbug while kaput']));
			case 'Starting':
				var data = model.a;
				return _Utils_Tuple2(
					$author$project$Main$Starting(
						_Utils_update(
							data,
							{awaitingShutterbug: false})),
					$author$project$Main$sendShutterbug(
						$author$project$Shutterbug$toString(shutterbug)));
			default:
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received another shutterbug']));
		}
	});
var $author$project$Main$ReceivedShutterbug = function (a) {
	return {$: 'ReceivedShutterbug', a: a};
};
var $author$project$Shutterbug$Msg = function (a) {
	return {$: 'Msg', a: a};
};
var $author$project$Shutterbug$path = '/shutterbug';
var $elm$http$Http$post = function (r) {
	return $elm$http$Http$request(
		{body: r.body, expect: r.expect, headers: _List_Nil, method: 'POST', timeout: $elm$core$Maybe$Nothing, tracker: $elm$core$Maybe$Nothing, url: r.url});
};
var $author$project$Shutterbug$create = $elm$http$Http$post(
	{
		body: $elm$http$Http$emptyBody,
		expect: A2($elm$http$Http$expectJson, $author$project$Shutterbug$Msg, $elm$json$Json$Decode$string),
		url: $author$project$Shutterbug$path
	});
var $elm$core$List$all = F2(
	function (isOkay, list) {
		return !A2(
			$elm$core$List$any,
			A2($elm$core$Basics$composeL, $elm$core$Basics$not, isOkay),
			list);
	});
var $elm_community$maybe_extra$Maybe$Extra$isJust = function (m) {
	if (m.$ === 'Nothing') {
		return false;
	} else {
		return true;
	}
};
var $author$project$Static$isComplete = function (data) {
	return A2(
		$elm$core$List$all,
		function (bool) {
			return bool;
		},
		_List_fromArray(
			[
				$elm_community$maybe_extra$Maybe$Extra$isJust(data.maybeAbout),
				$elm_community$maybe_extra$Maybe$Extra$isJust(data.maybeTheme)
			]));
};
var $author$project$Static$Motif$motif = function (_v0) {
	var theme = _v0.a;
	return theme;
};
var $author$project$Main$updateStatic = F3(
	function (maybeAbout, maybeMotif, model) {
		var updateTheMotif = F2(
			function (maybe, data) {
				if (maybe.$ === 'Just') {
					var motif = maybe.a;
					return _Utils_update(
						data,
						{
							maybeTheme: $elm$core$Maybe$Just(
								$author$project$Static$Motif$motif(motif))
						});
				} else {
					return data;
				}
			});
		var updateTheAbout = F2(
			function (maybe, data) {
				if (maybe.$ === 'Just') {
					var about = maybe.a;
					return _Utils_update(
						data,
						{
							maybeAbout: $elm$core$Maybe$Just(about)
						});
				} else {
					return data;
				}
			});
		switch (model.$) {
			case 'Errored':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received static data while errored']));
			case 'Kaput':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received static data while kaput']));
			case 'Starting':
				var startingData = model.a;
				return function (updatedStartingData) {
					return _Utils_Tuple2(
						$author$project$Main$Starting(updatedStartingData),
						function () {
							if ($author$project$Static$isComplete(updatedStartingData)) {
								var _v1 = startingData.shutterbugSource;
								if (_v1.$ === 'MakeNew') {
									return A2($elm$core$Platform$Cmd$map, $author$project$Main$ReceivedShutterbug, $author$project$Shutterbug$create);
								} else {
									return $elm$core$Platform$Cmd$none;
								}
							} else {
								return $elm$core$Platform$Cmd$none;
							}
						}());
				}(
					A2(
						updateTheMotif,
						maybeMotif,
						A2(updateTheAbout, maybeAbout, startingData)));
			default:
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received more static data']));
		}
	});
var $author$project$My$Extra$appendIf = F3(
	function (ok, prefix, potentialSuffix) {
		return ok ? _Utils_ap(prefix, potentialSuffix) : prefix;
	});
var $elm_community$list_extra$List$Extra$notMember = function (x) {
	return A2(
		$elm$core$Basics$composeL,
		$elm$core$Basics$not,
		$elm$core$List$member(x));
};
var $author$project$Cameras$toggleExclusive = F2(
	function (name, _v0) {
		var mode = _v0.a;
		var registry = _v0.b;
		var names = $author$project$Stitch$names(registry.selection);
		var toToggle = A3(
			$author$project$My$Extra$appendIf,
			A2($elm_community$list_extra$List$Extra$notMember, name, names),
			names,
			_List_fromArray(
				[name]));
		return A2(
			$author$project$Cameras$Model,
			mode,
			_Utils_update(
				registry,
				{
					selection: A3(
						$elm$core$List$foldl,
						F2(
							function (cam, sel) {
								return A2($author$project$Stitch$toggle, sel, cam);
							}),
						registry.selection,
						toToggle)
				}));
	});
var $author$project$Cameras$toggleIndependent = F2(
	function (name, _v0) {
		var mode = _v0.a;
		var registry = _v0.b;
		return A2(
			$author$project$Cameras$Model,
			mode,
			_Utils_update(
				registry,
				{
					selection: A2($author$project$Stitch$toggle, registry.selection, name)
				}));
	});
var $author$project$Cameras$toggle = F2(
	function (_v0, model) {
		var name = _v0.a;
		var _v1 = $author$project$Cameras$recentDifference(model);
		if (_v1.$ === 'Just') {
			return model;
		} else {
			var _v2 = model;
			var mode = _v2.a;
			if (mode.$ === 'Single') {
				return A2($author$project$Cameras$toggleExclusive, name, model);
			} else {
				return A2($author$project$Cameras$toggleIndependent, name, model);
			}
		}
	});
var $author$project$Main$updateToggledCamera = F2(
	function (model, toggleMsg) {
		switch (model.$) {
			case 'Errored':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received a camera toggle while errored']));
			case 'Kaput':
				return A2(
					$author$project$Main$handleError,
					model,
					_List_fromArray(
						['received a camera toggle while kaput']));
			case 'Starting':
				var data = model.a;
				var _v1 = data.maybeCameras;
				if (_v1.$ === 'Just') {
					var cameras = _v1.a;
					return _Utils_Tuple2(
						$author$project$Main$Starting(
							_Utils_update(
								data,
								{
									maybeCameras: $elm$core$Maybe$Just(
										A2($author$project$Cameras$toggle, toggleMsg, cameras))
								})),
						$elm$core$Platform$Cmd$none);
				} else {
					return A2($author$project$Main$handleMsgError, model, 'received a camera toggle early');
				}
			default:
				var cameras = model.a;
				var screen = model.b;
				var devoir = model.c;
				var popups = model.d;
				return _Utils_Tuple2(
					A4(
						$author$project$Main$Running,
						A2($author$project$Cameras$toggle, toggleMsg, cameras),
						screen,
						devoir,
						popups),
					$elm$core$Platform$Cmd$none);
		}
	});
var $author$project$Cameras$blockedInterval = 1750;
var $elm$time$Time$posixToMillis = function (_v0) {
	var millis = _v0.a;
	return millis;
};
var $author$project$Cameras$tryToUnblock = F2(
	function (_v0, now) {
		var mode = _v0.a;
		var registry = _v0.b;
		var _v1 = registry.recentChanges;
		if (_v1.$ === 'Just') {
			var changes = _v1.a;
			return (_Utils_cmp(
				$elm$time$Time$posixToMillis(now),
				$elm$time$Time$posixToMillis(changes.latestUpdate) + $author$project$Cameras$blockedInterval) > 0) ? A2(
				$author$project$Cameras$Model,
				mode,
				_Utils_update(
					registry,
					{recentChanges: $elm$core$Maybe$Nothing})) : A2($author$project$Cameras$Model, mode, registry);
		} else {
			return A2($author$project$Cameras$Model, mode, registry);
		}
	});
var $author$project$Main$updateTryToUnblockCameraToggleButtons = F2(
	function (model, now) {
		switch (model.$) {
			case 'Errored':
				return _Utils_Tuple2(model, $elm$core$Platform$Cmd$none);
			case 'Kaput':
				return _Utils_Tuple2(model, $elm$core$Platform$Cmd$none);
			case 'Starting':
				var data = model.a;
				var _v1 = data.maybeCameras;
				if (_v1.$ === 'Just') {
					var cameras = _v1.a;
					return _Utils_Tuple2(
						$author$project$Main$Starting(
							_Utils_update(
								data,
								{
									maybeCameras: $elm$core$Maybe$Just(
										A2($author$project$Cameras$tryToUnblock, cameras, now))
								})),
						$elm$core$Platform$Cmd$none);
				} else {
					return A2($author$project$Main$handleMsgError, model, 'unblock selectors');
				}
			default:
				var cameras = model.a;
				var screen = model.b;
				var devoir = model.c;
				var popups = model.d;
				return _Utils_Tuple2(
					A4(
						$author$project$Main$Running,
						A2($author$project$Cameras$tryToUnblock, cameras, now),
						screen,
						devoir,
						popups),
					$elm$core$Platform$Cmd$none);
		}
	});
var $author$project$Main$update = F2(
	function (msg, model) {
		var delay = $author$project$Main$delayMsg(0);
		switch (msg.$) {
			case 'ReceivedAbout':
				var aboutMsg = msg.a;
				var _v1 = $author$project$Static$About$init(aboutMsg);
				if (_v1.$ === 'Ok') {
					var about = _v1.a;
					return A3(
						$author$project$Main$updateStatic,
						$elm$core$Maybe$Just(about),
						$elm$core$Maybe$Nothing,
						model);
				} else {
					var err = _v1.a;
					return A2(
						$author$project$Main$handleError,
						model,
						$author$project$My$Http$formatLongError(err));
				}
			case 'ReceivedMotif':
				var motifMsg = msg.a;
				var _v2 = $author$project$Static$Motif$init(motifMsg);
				if (_v2.$ === 'Ok') {
					var motif = _v2.a;
					return A3(
						$author$project$Main$updateStatic,
						$elm$core$Maybe$Nothing,
						$elm$core$Maybe$Just(motif),
						model);
				} else {
					var err = _v2.a;
					return A2(
						$author$project$Main$handleError,
						model,
						$author$project$My$Http$formatLongError(err));
				}
			case 'ReceivedShutterbug':
				var shutterbugMsg = msg.a;
				return _Utils_Tuple2(
					model,
					delay(
						$author$project$Main$DelayedShutterbug(shutterbugMsg)));
			case 'DelayedShutterbug':
				var shutterbugMsg = msg.a;
				var _v3 = $author$project$Shutterbug$init(shutterbugMsg);
				if (_v3.$ === 'Ok') {
					var shutterbug = _v3.a;
					return A2($author$project$Main$updateShutterbug, shutterbug, model);
				} else {
					var err = _v3.a;
					return A2(
						$author$project$Main$handleError,
						model,
						_List_fromArray(
							[err]));
				}
			case 'ReceivedLifeCycle':
				var lifeCycleMsg = msg.a;
				return _Utils_Tuple2(
					model,
					delay(
						$author$project$Main$DelayedLifeCycle(lifeCycleMsg)));
			case 'DelayedLifeCycle':
				var lifeCycleMsg = msg.a;
				var _v4 = A2($elm$json$Json$Decode$decodeValue, $author$project$LifeCycle$decodeLifeCycleMessage, lifeCycleMsg);
				if (_v4.$ === 'Ok') {
					var lifeCycle = _v4.a;
					if (lifeCycle.$ === 'Ok') {
						var event = lifeCycle.a;
						return A2($author$project$Main$updateLifeCycle, event, model);
					} else {
						var err = lifeCycle.a;
						return A2(
							$author$project$Main$handleError,
							model,
							_List_fromArray(
								[err]));
					}
				} else {
					var err = _v4.a;
					return A2(
						$author$project$Main$handleError,
						model,
						_List_fromArray(
							[
								$elm$json$Json$Decode$errorToString(err)
							]));
				}
			case 'ReceivedRegAssocCameras':
				var camerasMsg = msg.a;
				return _Utils_Tuple2(
					model,
					delay(
						$author$project$Main$DelayedRegAssocCameras(camerasMsg)));
			case 'DelayedRegAssocCameras':
				var camerasMsg = msg.a;
				var _v6 = $author$project$Cameras$decodeRegisteredAssociatedMessage(camerasMsg);
				if (_v6.$ === 'Ok') {
					var allRegistered = _v6.a;
					return A2($author$project$Main$updateCameras, allRegistered, model);
				} else {
					var err = _v6.a;
					return A2(
						$author$project$Main$handleError,
						model,
						_List_fromArray(
							[err]));
				}
			case 'ReceivedPixelsMeta':
				var value = msg.a;
				return _Utils_Tuple2(
					model,
					delay(
						$author$project$Main$DelayedPixelsMeta(value)));
			case 'DelayedPixelsMeta':
				var value = msg.a;
				var _v7 = A2($elm$json$Json$Decode$decodeValue, $author$project$PixScreen$metaDecoder, value);
				if (_v7.$ === 'Ok') {
					var meta = _v7.a;
					return A2($author$project$Main$updatePixScreen, meta, model);
				} else {
					var err = _v7.a;
					return A2(
						$author$project$Main$handleError,
						model,
						_List_fromArray(
							[
								$elm$json$Json$Decode$errorToString(err)
							]));
				}
			case 'ReceivedDevoir':
				var value = msg.a;
				var _v8 = A2($elm$json$Json$Decode$decodeValue, $author$project$Devoir$devoirDecoder, value);
				if (_v8.$ === 'Ok') {
					var paused = _v8.a;
					return A2($author$project$Main$updateDevoir, paused, model);
				} else {
					var err = _v8.a;
					return A2(
						$author$project$Main$handleError,
						model,
						_List_fromArray(
							[
								$elm$json$Json$Decode$errorToString(err)
							]));
				}
			case 'ReceivedKaput':
				var value = msg.a;
				var _v9 = A2($elm$json$Json$Decode$decodeValue, $author$project$LifeCycle$decodeWSCloseEvent, value);
				if (_v9.$ === 'Ok') {
					var notification = _v9.a;
					return A2($author$project$Main$updateKaput, notification, model);
				} else {
					var err = _v9.a;
					return A2(
						$author$project$Main$handleError,
						model,
						_List_fromArray(
							[
								$elm$json$Json$Decode$errorToString(err)
							]));
				}
			case 'TryToUnblockCameraToggleButtons':
				var now = msg.a;
				return A2($author$project$Main$updateTryToUnblockCameraToggleButtons, model, now);
			case 'UpdateCamerasTime':
				var now = msg.a;
				return A2($author$project$Main$updateCamerasTime, model, now);
			case 'IssuedCameraAssociations':
				var associations = msg.a.a;
				switch (model.$) {
					case 'Starting':
						var data = model.a;
						return _Utils_Tuple2(
							$author$project$Main$Starting(
								_Utils_update(
									data,
									{sentCameraAssociations: true})),
							$author$project$Main$sendCameraAssociations(associations));
					case 'Running':
						var data = model.d;
						return _Utils_Tuple2(
							function () {
								if (data.popUps.autoCloseCameras && $author$project$PopUp$cameras(data.popUps)) {
									var _v11 = A2($author$project$Main$update, $author$project$Main$ToggledCamerasPopUp, model);
									var newModel = _v11.a;
									return newModel;
								} else {
									return model;
								}
							}(),
							$author$project$Main$sendCameraAssociations(associations));
					default:
						return A2($author$project$Main$handleMsgError, model, 'camera association');
				}
			case 'ClearedCameraSelections':
				if (model.$ === 'Running') {
					var cameras = model.a;
					var screen = model.b;
					var devoir = model.c;
					var data = model.d;
					return _Utils_Tuple2(
						A4(
							$author$project$Main$Running,
							$author$project$Cameras$clearSelection(cameras),
							screen,
							devoir,
							data),
						$elm$core$Platform$Cmd$none);
				} else {
					return A2($author$project$Main$handleMsgError, model, 'cleared camera selections');
				}
			case 'StitchingMsg':
				var stitchingMsg = msg.a;
				if (stitchingMsg.$ === 'SwitchedCameras') {
					var switchCamerasMsg = stitchingMsg.a;
					switch (model.$) {
						case 'Starting':
							var data = model.a;
							var _v15 = data.maybeCameras;
							if (_v15.$ === 'Just') {
								var cameras = _v15.a;
								return _Utils_Tuple2(
									$author$project$Main$Starting(
										_Utils_update(
											data,
											{
												maybeCameras: $elm$core$Maybe$Just(
													A2($author$project$Cameras$switchCameras, cameras, switchCamerasMsg))
											})),
									$elm$core$Platform$Cmd$none);
							} else {
								return A2($author$project$Main$handleMsgError, model, 'switched cameras');
							}
						case 'Running':
							var cameras = model.a;
							var screen = model.b;
							var devoir = model.c;
							var data = model.d;
							return _Utils_Tuple2(
								A4(
									$author$project$Main$Running,
									A2($author$project$Cameras$switchCameras, cameras, switchCamerasMsg),
									screen,
									devoir,
									data),
								$elm$core$Platform$Cmd$none);
						default:
							return A2($author$project$Main$handleMsgError, model, 'switched cameras');
					}
				} else {
					switch (model.$) {
						case 'Starting':
							var data = model.a;
							var _v17 = data.maybeCameras;
							if (_v17.$ === 'Just') {
								var cameras = _v17.a;
								return _Utils_Tuple2(
									$author$project$Main$Starting(
										_Utils_update(
											data,
											{
												maybeCameras: $elm$core$Maybe$Just(
													$author$project$Cameras$toggleStitchingSize(cameras))
											})),
									$elm$core$Platform$Cmd$none);
							} else {
								return A2($author$project$Main$handleMsgError, model, 'toggled stitching size');
							}
						case 'Running':
							var cameras = model.a;
							var screen = model.b;
							var devoir = model.c;
							var data = model.d;
							return _Utils_Tuple2(
								A4(
									$author$project$Main$Running,
									$author$project$Cameras$toggleStitchingSize(cameras),
									screen,
									devoir,
									data),
								$elm$core$Platform$Cmd$none);
						default:
							return A2($author$project$Main$handleMsgError, model, 'toggled stitching size');
					}
				}
			case 'ToggledCamera':
				var toggleMsg = msg.a;
				return A2($author$project$Main$updateToggledCamera, model, toggleMsg);
			case 'ToggledCameraSelectionMode':
				if (model.$ === 'Running') {
					var cameras = model.a;
					var screen = model.b;
					var devoir = model.c;
					var data = model.d;
					return _Utils_Tuple2(
						A4(
							$author$project$Main$Running,
							$author$project$Cameras$toggleSelectionMode(cameras),
							screen,
							devoir,
							data),
						$elm$core$Platform$Cmd$none);
				} else {
					return A2($author$project$Main$handleMsgError, model, 'camera selection mode toggle');
				}
			case 'ToggledOutputSize':
				_v19$2:
				while (true) {
					switch (model.$) {
						case 'Running':
							var cameras = model.a;
							var screen = model.b;
							var devoir = model.c;
							var data = model.d;
							return _Utils_Tuple2(
								A4(
									$author$project$Main$Running,
									cameras,
									$author$project$PixScreen$toggleFit(screen),
									devoir,
									data),
								$elm$core$Platform$Cmd$none);
						case 'Kaput':
							if (model.a.$ === 'AfterRunning') {
								var _v20 = model.a;
								var notification = _v20.a;
								var errors = _v20.b;
								var screen = _v20.c;
								var data = _v20.d;
								return _Utils_Tuple2(
									$author$project$Main$Kaput(
										A4(
											$author$project$Kaput$AfterRunning,
											notification,
											errors,
											$author$project$PixScreen$toggleFit(screen),
											data)),
									$elm$core$Platform$Cmd$none);
							} else {
								break _v19$2;
							}
						default:
							break _v19$2;
					}
				}
				return A2($author$project$Main$handleMsgError, model, 'screen size toggle');
			case 'ToggledOutputBorder':
				_v21$2:
				while (true) {
					switch (model.$) {
						case 'Running':
							var cameras = model.a;
							var screen = model.b;
							var devoir = model.c;
							var data = model.d;
							return _Utils_Tuple2(
								A4(
									$author$project$Main$Running,
									cameras,
									$author$project$PixScreen$toggleBorder(screen),
									devoir,
									data),
								$elm$core$Platform$Cmd$none);
						case 'Kaput':
							if (model.a.$ === 'AfterRunning') {
								var _v22 = model.a;
								var notification = _v22.a;
								var errors = _v22.b;
								var screen = _v22.c;
								var data = _v22.d;
								return _Utils_Tuple2(
									$author$project$Main$Kaput(
										A4(
											$author$project$Kaput$AfterRunning,
											notification,
											errors,
											$author$project$PixScreen$toggleBorder(screen),
											data)),
									$elm$core$Platform$Cmd$none);
							} else {
								break _v21$2;
							}
						default:
							break _v21$2;
					}
				}
				return A2($author$project$Main$handleMsgError, model, 'screen border toggle');
			case 'ToggledTextSelect':
				_v23$2:
				while (true) {
					switch (model.$) {
						case 'Running':
							var cameras = model.a;
							var screen = model.b;
							var devoir = model.c;
							var data = model.d;
							return _Utils_Tuple2(
								A4(
									$author$project$Main$Running,
									cameras,
									$author$project$PixScreen$toggleTextSelect(screen),
									devoir,
									data),
								$elm$core$Platform$Cmd$none);
						case 'Kaput':
							if (model.a.$ === 'AfterRunning') {
								var _v24 = model.a;
								var notification = _v24.a;
								var errors = _v24.b;
								var screen = _v24.c;
								var data = _v24.d;
								return _Utils_Tuple2(
									$author$project$Main$Kaput(
										A4(
											$author$project$Kaput$AfterRunning,
											notification,
											errors,
											$author$project$PixScreen$toggleTextSelect(screen),
											data)),
									$elm$core$Platform$Cmd$none);
							} else {
								break _v23$2;
							}
						default:
							break _v23$2;
					}
				}
				return A2($author$project$Main$handleMsgError, model, 'screen text select');
			case 'ToggledCamerasPopUp':
				if (model.$ === 'Running') {
					var cameras = model.a;
					var screen = model.b;
					var devoir = model.c;
					var data = model.d;
					return _Utils_Tuple2(
						A4(
							$author$project$Main$Running,
							$author$project$PopUp$cameras(data.popUps) ? cameras : $author$project$Cameras$incrementSelectionSession(cameras),
							screen,
							devoir,
							function (popups) {
								return _Utils_update(
									data,
									{popUps: popups});
							}(
								A2($author$project$PopUp$togglePixelsAreaPopUp, data.popUps, $author$project$PopUp$CamerasPopUp))),
						$elm$core$Platform$Cmd$none);
				} else {
					return A2($author$project$Main$handleMsgError, model, 'cameras pop-up toggle');
				}
			case 'ToggledAutoCloseCameras':
				if (model.$ === 'Running') {
					var cameras = model.a;
					var screen = model.b;
					var devoir = model.c;
					var data = model.d;
					return _Utils_Tuple2(
						A4(
							$author$project$Main$Running,
							cameras,
							screen,
							devoir,
							function (popups) {
								return _Utils_update(
									data,
									{popUps: popups});
							}(
								function (_v27) {
									var autoCloseCameras = _v27.a;
									var popups = _v27.b;
									return _Utils_update(
										popups,
										{autoCloseCameras: autoCloseCameras});
								}(
									_Utils_Tuple2(!data.popUps.autoCloseCameras, data.popUps)))),
						$elm$core$Platform$Cmd$none);
				} else {
					return A2($author$project$Main$handleMsgError, model, 'cameras auto close toggle');
				}
			case 'ToggledInfoPopUp':
				_v28$2:
				while (true) {
					switch (model.$) {
						case 'Running':
							var cameras = model.a;
							var screen = model.b;
							var devoir = model.c;
							var data = model.d;
							return _Utils_Tuple2(
								A4(
									$author$project$Main$Running,
									cameras,
									screen,
									devoir,
									function (popups) {
										return _Utils_update(
											data,
											{popUps: popups});
									}(
										A2($author$project$PopUp$togglePixelsAreaPopUp, data.popUps, $author$project$PopUp$InfoPopUp))),
								$elm$core$Platform$Cmd$none);
						case 'Kaput':
							if (model.a.$ === 'AfterRunning') {
								var _v29 = model.a;
								var reason = _v29.a;
								var errors = _v29.b;
								var screen = _v29.c;
								var data = _v29.d;
								return _Utils_Tuple2(
									$author$project$Main$Kaput(
										A4(
											$author$project$Kaput$AfterRunning,
											reason,
											errors,
											screen,
											function (popups) {
												return _Utils_update(
													data,
													{popUps: popups});
											}(
												A2($author$project$PopUp$togglePixelsAreaPopUp, data.popUps, $author$project$PopUp$InfoPopUp)))),
									$elm$core$Platform$Cmd$none);
							} else {
								break _v28$2;
							}
						default:
							break _v28$2;
					}
				}
				return A2($author$project$Main$handleMsgError, model, 'info pop-up toggle');
			case 'ToggledSettingsPopUp':
				_v30$2:
				while (true) {
					switch (model.$) {
						case 'Running':
							var cameras = model.a;
							var screen = model.b;
							var devoir = model.c;
							var data = model.d;
							return _Utils_Tuple2(
								A4(
									$author$project$Main$Running,
									cameras,
									screen,
									devoir,
									function (popups) {
										return _Utils_update(
											data,
											{popUps: popups});
									}(
										A2($author$project$PopUp$togglePixelsAreaPopUp, data.popUps, $author$project$PopUp$SettingsPopUp))),
								$elm$core$Platform$Cmd$none);
						case 'Kaput':
							if (model.a.$ === 'AfterRunning') {
								var _v31 = model.a;
								var reason = _v31.a;
								var errors = _v31.b;
								var screen = _v31.c;
								var data = _v31.d;
								return _Utils_Tuple2(
									$author$project$Main$Kaput(
										A4(
											$author$project$Kaput$AfterRunning,
											reason,
											errors,
											screen,
											function (popups) {
												return _Utils_update(
													data,
													{popUps: popups});
											}(
												A2($author$project$PopUp$togglePixelsAreaPopUp, data.popUps, $author$project$PopUp$SettingsPopUp)))),
									$elm$core$Platform$Cmd$none);
							} else {
								break _v30$2;
							}
						default:
							break _v30$2;
					}
				}
				return A2($author$project$Main$handleMsgError, model, 'settings pop-up toggle');
			case 'ToggledShutterbugPopUp':
				switch (model.$) {
					case 'Starting':
						var data = model.a;
						return _Utils_Tuple2(
							$author$project$Main$Starting(
								_Utils_update(
									data,
									{shutterbugActionsPopUp: !data.shutterbugActionsPopUp})),
							$elm$core$Platform$Cmd$none);
					case 'Running':
						var cameras = model.a;
						var screen = model.b;
						var devoir = model.c;
						var data = model.d;
						return _Utils_Tuple2(
							A4(
								$author$project$Main$Running,
								cameras,
								screen,
								devoir,
								function (popups) {
									return _Utils_update(
										data,
										{popUps: popups});
								}(
									$author$project$PopUp$toggleShutterbugActionsPopUp(data.popUps))),
							$elm$core$Platform$Cmd$none);
					case 'Kaput':
						if (model.a.$ === 'BeforeRunning') {
							var _v33 = model.a;
							var reason = _v33.a;
							var errors = _v33.b;
							var data = _v33.c;
							return _Utils_Tuple2(
								$author$project$Main$Kaput(
									A3(
										$author$project$Kaput$BeforeRunning,
										reason,
										errors,
										_Utils_update(
											data,
											{shutterbugActionsPopUp: !data.shutterbugActionsPopUp}))),
								$elm$core$Platform$Cmd$none);
						} else {
							var _v34 = model.a;
							var reason = _v34.a;
							var errors = _v34.b;
							var screen = _v34.c;
							var data = _v34.d;
							return _Utils_Tuple2(
								$author$project$Main$Kaput(
									A4(
										$author$project$Kaput$AfterRunning,
										reason,
										errors,
										screen,
										function (popups) {
											return _Utils_update(
												data,
												{popUps: popups});
										}(
											$author$project$PopUp$toggleShutterbugActionsPopUp(data.popUps)))),
								$elm$core$Platform$Cmd$none);
						}
					default:
						return A2($author$project$Main$handleMsgError, model, 'shutterbug pop-up toggle');
				}
			case 'ChangedInfoTab':
				var tab = msg.a;
				_v35$2:
				while (true) {
					switch (model.$) {
						case 'Running':
							var cameras = model.a;
							var screen = model.b;
							var devoir = model.c;
							var data = model.d;
							return _Utils_Tuple2(
								A4(
									$author$project$Main$Running,
									cameras,
									screen,
									devoir,
									_Utils_update(
										data,
										{
											popUps: A2($author$project$PopUp$changeTab, data.popUps, tab)
										})),
								$elm$core$Platform$Cmd$none);
						case 'Kaput':
							if (model.a.$ === 'AfterRunning') {
								var _v36 = model.a;
								var reason = _v36.a;
								var errors = _v36.b;
								var screen = _v36.c;
								var data = _v36.d;
								return _Utils_Tuple2(
									$author$project$Main$Kaput(
										A4(
											$author$project$Kaput$AfterRunning,
											reason,
											errors,
											screen,
											_Utils_update(
												data,
												{
													popUps: A2($author$project$PopUp$changeTab, data.popUps, tab)
												}))),
									$elm$core$Platform$Cmd$none);
							} else {
								break _v35$2;
							}
						default:
							break _v35$2;
					}
				}
				return A2($author$project$Main$handleMsgError, model, 'info tab change');
			case 'OpenedNewTab':
				var url = msg.a;
				return _Utils_Tuple2(
					model,
					$author$project$Main$sendOpenNewTab(url));
			case 'ToggledFullscreen':
				var full = msg.a;
				return _Utils_Tuple2(
					model,
					$author$project$Main$sendFullscreen(full));
			case 'ToggledPointerLock':
				var locked = msg.a;
				return _Utils_Tuple2(
					function () {
						if (model.$ === 'Running') {
							var cameras = model.a;
							var screen = model.b;
							var devoir = model.c;
							var data = model.d;
							return A4(
								$author$project$Main$Running,
								cameras,
								A3(
									$author$project$My$Extra$ternary,
									locked,
									$author$project$PixScreen$disableTextSelect(screen),
									screen),
								devoir,
								data);
						} else {
							return model;
						}
					}(),
					$author$project$Main$sendPointerLock(locked));
			case 'WroteShutterbugToClipboard':
				return _Utils_Tuple2(
					model,
					$author$project$Main$sendWriteShutterbugToClipboard(_Utils_Tuple0));
			case 'WroteTextToClipboard':
				var text = msg.a;
				return _Utils_Tuple2(
					model,
					$author$project$Main$sendWriteTextToClipboard(text));
			case 'IssuedUICommand':
				var command = msg.a;
				return _Utils_Tuple2(
					model,
					$author$project$Main$sendUICommand(
						$author$project$Devoir$toRecord(command)));
			case 'ChangedShutterbugInputValue':
				var shutterbugInput = msg.a;
				if (model.$ === 'Starting') {
					var data = model.a;
					var _v39 = data.shutterbugSource;
					if (_v39.$ === 'UseExisting') {
						return _Utils_Tuple2(
							$author$project$Main$Starting(
								_Utils_update(
									data,
									{
										shutterbugSource: $author$project$Main$UseExisting(shutterbugInput)
									})),
							$elm$core$Platform$Cmd$none);
					} else {
						return A2(
							$author$project$Main$handleError,
							model,
							_List_fromArray(
								['shutterbug input value changed']));
					}
				} else {
					return A2(
						$author$project$Main$handleError,
						model,
						_List_fromArray(
							['shutterbug input value changed']));
				}
			case 'SubmittedShutterbug':
				var shutterbug = msg.a;
				if (shutterbug === '') {
					return _Utils_Tuple2(model, $elm$core$Platform$Cmd$none);
				} else {
					return A2(
						$author$project$Main$updateShutterbug,
						$author$project$Shutterbug$fromString(shutterbug),
						model);
				}
			default:
				return _Utils_Tuple2(model, $elm$core$Platform$Cmd$none);
		}
	});
var $rtfeldman$elm_css$Css$Structure$Selector = F3(
	function (a, b, c) {
		return {$: 'Selector', a: a, b: b, c: c};
	});
var $rtfeldman$elm_css$Css$Preprocess$Snippet = function (a) {
	return {$: 'Snippet', a: a};
};
var $rtfeldman$elm_css$Css$Preprocess$StyleBlock = F3(
	function (a, b, c) {
		return {$: 'StyleBlock', a: a, b: b, c: c};
	});
var $rtfeldman$elm_css$Css$Preprocess$StyleBlockDeclaration = function (a) {
	return {$: 'StyleBlockDeclaration', a: a};
};
var $rtfeldman$elm_css$Css$Structure$TypeSelector = function (a) {
	return {$: 'TypeSelector', a: a};
};
var $rtfeldman$elm_css$Css$Structure$TypeSelectorSequence = F2(
	function (a, b) {
		return {$: 'TypeSelectorSequence', a: a, b: b};
	});
var $rtfeldman$elm_css$Css$Global$typeSelector = F2(
	function (selectorStr, styles) {
		var sequence = A2(
			$rtfeldman$elm_css$Css$Structure$TypeSelectorSequence,
			$rtfeldman$elm_css$Css$Structure$TypeSelector(selectorStr),
			_List_Nil);
		var sel = A3($rtfeldman$elm_css$Css$Structure$Selector, sequence, _List_Nil, $elm$core$Maybe$Nothing);
		return $rtfeldman$elm_css$Css$Preprocess$Snippet(
			_List_fromArray(
				[
					$rtfeldman$elm_css$Css$Preprocess$StyleBlockDeclaration(
					A3($rtfeldman$elm_css$Css$Preprocess$StyleBlock, sel, _List_Nil, styles))
				]));
	});
var $rtfeldman$elm_css$Css$Global$body = $rtfeldman$elm_css$Css$Global$typeSelector('body');
var $author$project$Theme$buttonBackgroundStop1Color = '--button-background-stop-1-color';
var $author$project$Theme$buttonBackgroundStop1Percentage = '--button-background-stop-1-percentage';
var $author$project$Theme$buttonBackgroundStop2Percentage = '--button-background-stop-2-percentage';
var $author$project$Theme$colToString = function (col) {
	return A2(
		$elm$core$String$join,
		',',
		_List_fromArray(
			[
				'rgba(' + $elm$core$String$fromInt(col.red),
				$elm$core$String$fromInt(col.green),
				$elm$core$String$fromInt(col.blue),
				$elm$core$String$fromFloat(col.alpha) + ')'
			]));
};
var $rtfeldman$elm_css$Css$Structure$CustomSelector = F2(
	function (a, b) {
		return {$: 'CustomSelector', a: a, b: b};
	});
var $rtfeldman$elm_css$Css$Structure$UniversalSelectorSequence = function (a) {
	return {$: 'UniversalSelectorSequence', a: a};
};
var $rtfeldman$elm_css$Css$Structure$appendRepeatable = F2(
	function (selector, sequence) {
		switch (sequence.$) {
			case 'TypeSelectorSequence':
				var typeSelector = sequence.a;
				var list = sequence.b;
				return A2(
					$rtfeldman$elm_css$Css$Structure$TypeSelectorSequence,
					typeSelector,
					_Utils_ap(
						list,
						_List_fromArray(
							[selector])));
			case 'UniversalSelectorSequence':
				var list = sequence.a;
				return $rtfeldman$elm_css$Css$Structure$UniversalSelectorSequence(
					_Utils_ap(
						list,
						_List_fromArray(
							[selector])));
			default:
				var str = sequence.a;
				var list = sequence.b;
				return A2(
					$rtfeldman$elm_css$Css$Structure$CustomSelector,
					str,
					_Utils_ap(
						list,
						_List_fromArray(
							[selector])));
		}
	});
var $rtfeldman$elm_css$Css$Preprocess$unwrapSnippet = function (_v0) {
	var declarations = _v0.a;
	return declarations;
};
var $rtfeldman$elm_css$Css$Global$collectSelectors = function (declarations) {
	collectSelectors:
	while (true) {
		if (!declarations.b) {
			return _List_Nil;
		} else {
			if (declarations.a.$ === 'StyleBlockDeclaration') {
				var _v5 = declarations.a.a;
				var firstSelector = _v5.a;
				var otherSelectors = _v5.b;
				var styles = _v5.c;
				var rest = declarations.b;
				return _Utils_ap(
					A2(
						$elm$core$List$cons,
						A2($rtfeldman$elm_css$Css$Global$unwrapSelector, firstSelector, styles),
						otherSelectors),
					$rtfeldman$elm_css$Css$Global$collectSelectors(rest));
			} else {
				var rest = declarations.b;
				var $temp$declarations = rest;
				declarations = $temp$declarations;
				continue collectSelectors;
			}
		}
	}
};
var $rtfeldman$elm_css$Css$Global$unwrapSelector = F2(
	function (_v0, styles) {
		var sequence = _v0.a;
		var combinators = _v0.b;
		var mPseudo = _v0.c;
		var unwrapSequenceSelector = F2(
			function (style, s) {
				if (style.$ === 'ExtendSelector') {
					var nestedSelector = style.a;
					var evenMoreNestedStyles = style.b;
					return A3(
						$elm$core$List$foldr,
						unwrapSequenceSelector,
						A2($rtfeldman$elm_css$Css$Structure$appendRepeatable, nestedSelector, s),
						evenMoreNestedStyles);
				} else {
					return s;
				}
			});
		var unwrapCombinatorSelector = F2(
			function (style, cs) {
				if (style.$ === 'NestSnippet') {
					var combinator = style.a;
					var snippets = style.b;
					return A2(
						$elm$core$List$append,
						cs,
						A2(
							$elm$core$List$map,
							function (_v3) {
								var s = _v3.a;
								return _Utils_Tuple2(combinator, s);
							},
							A2(
								$elm$core$List$concatMap,
								A2($elm$core$Basics$composeR, $rtfeldman$elm_css$Css$Preprocess$unwrapSnippet, $rtfeldman$elm_css$Css$Global$collectSelectors),
								snippets)));
				} else {
					return cs;
				}
			});
		return A3(
			$rtfeldman$elm_css$Css$Structure$Selector,
			A3($elm$core$List$foldr, unwrapSequenceSelector, sequence, styles),
			A3($elm$core$List$foldr, unwrapCombinatorSelector, combinators, styles),
			mPseudo);
	});
var $rtfeldman$elm_css$Css$Global$each = F2(
	function (snippetCreators, styles) {
		var selectorsToSnippet = function (selectors) {
			if (!selectors.b) {
				return $rtfeldman$elm_css$Css$Preprocess$Snippet(_List_Nil);
			} else {
				var first = selectors.a;
				var rest = selectors.b;
				return $rtfeldman$elm_css$Css$Preprocess$Snippet(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$Preprocess$StyleBlockDeclaration(
							A3($rtfeldman$elm_css$Css$Preprocess$StyleBlock, first, rest, styles))
						]));
			}
		};
		return selectorsToSnippet(
			$rtfeldman$elm_css$Css$Global$collectSelectors(
				A2(
					$elm$core$List$concatMap,
					$rtfeldman$elm_css$Css$Preprocess$unwrapSnippet,
					A2(
						$elm$core$List$map,
						$elm$core$Basics$apR(_List_Nil),
						snippetCreators))));
	});
var $elm$virtual_dom$VirtualDom$attribute = F2(
	function (key, value) {
		return A2(
			_VirtualDom_attribute,
			_VirtualDom_noOnOrFormAction(key),
			_VirtualDom_noJavaScriptOrHtmlUri(value));
	});
var $rtfeldman$elm_css$Css$Structure$compactHelp = F2(
	function (declaration, _v0) {
		var keyframesByName = _v0.a;
		var declarations = _v0.b;
		switch (declaration.$) {
			case 'StyleBlockDeclaration':
				var _v2 = declaration.a;
				var properties = _v2.c;
				return $elm$core$List$isEmpty(properties) ? _Utils_Tuple2(keyframesByName, declarations) : _Utils_Tuple2(
					keyframesByName,
					A2($elm$core$List$cons, declaration, declarations));
			case 'MediaRule':
				var styleBlocks = declaration.b;
				return A2(
					$elm$core$List$all,
					function (_v3) {
						var properties = _v3.c;
						return $elm$core$List$isEmpty(properties);
					},
					styleBlocks) ? _Utils_Tuple2(keyframesByName, declarations) : _Utils_Tuple2(
					keyframesByName,
					A2($elm$core$List$cons, declaration, declarations));
			case 'SupportsRule':
				var otherDeclarations = declaration.b;
				return $elm$core$List$isEmpty(otherDeclarations) ? _Utils_Tuple2(keyframesByName, declarations) : _Utils_Tuple2(
					keyframesByName,
					A2($elm$core$List$cons, declaration, declarations));
			case 'DocumentRule':
				return _Utils_Tuple2(
					keyframesByName,
					A2($elm$core$List$cons, declaration, declarations));
			case 'PageRule':
				var properties = declaration.a;
				return $elm$core$List$isEmpty(properties) ? _Utils_Tuple2(keyframesByName, declarations) : _Utils_Tuple2(
					keyframesByName,
					A2($elm$core$List$cons, declaration, declarations));
			case 'FontFace':
				var properties = declaration.a;
				return $elm$core$List$isEmpty(properties) ? _Utils_Tuple2(keyframesByName, declarations) : _Utils_Tuple2(
					keyframesByName,
					A2($elm$core$List$cons, declaration, declarations));
			case 'Keyframes':
				var record = declaration.a;
				return $elm$core$String$isEmpty(record.declaration) ? _Utils_Tuple2(keyframesByName, declarations) : _Utils_Tuple2(
					A3($elm$core$Dict$insert, record.name, record.declaration, keyframesByName),
					declarations);
			case 'Viewport':
				var properties = declaration.a;
				return $elm$core$List$isEmpty(properties) ? _Utils_Tuple2(keyframesByName, declarations) : _Utils_Tuple2(
					keyframesByName,
					A2($elm$core$List$cons, declaration, declarations));
			case 'CounterStyle':
				var properties = declaration.a;
				return $elm$core$List$isEmpty(properties) ? _Utils_Tuple2(keyframesByName, declarations) : _Utils_Tuple2(
					keyframesByName,
					A2($elm$core$List$cons, declaration, declarations));
			default:
				var tuples = declaration.a;
				return A2(
					$elm$core$List$all,
					function (_v4) {
						var properties = _v4.b;
						return $elm$core$List$isEmpty(properties);
					},
					tuples) ? _Utils_Tuple2(keyframesByName, declarations) : _Utils_Tuple2(
					keyframesByName,
					A2($elm$core$List$cons, declaration, declarations));
		}
	});
var $rtfeldman$elm_css$Css$Structure$Keyframes = function (a) {
	return {$: 'Keyframes', a: a};
};
var $rtfeldman$elm_css$Css$Structure$withKeyframeDeclarations = F2(
	function (keyframesByName, compactedDeclarations) {
		return A2(
			$elm$core$List$append,
			A2(
				$elm$core$List$map,
				function (_v0) {
					var name = _v0.a;
					var decl = _v0.b;
					return $rtfeldman$elm_css$Css$Structure$Keyframes(
						{declaration: decl, name: name});
				},
				$elm$core$Dict$toList(keyframesByName)),
			compactedDeclarations);
	});
var $rtfeldman$elm_css$Css$Structure$compactDeclarations = function (declarations) {
	var _v0 = A3(
		$elm$core$List$foldr,
		$rtfeldman$elm_css$Css$Structure$compactHelp,
		_Utils_Tuple2($elm$core$Dict$empty, _List_Nil),
		declarations);
	var keyframesByName = _v0.a;
	var compactedDeclarations = _v0.b;
	return A2($rtfeldman$elm_css$Css$Structure$withKeyframeDeclarations, keyframesByName, compactedDeclarations);
};
var $rtfeldman$elm_css$Css$Structure$compactStylesheet = function (_v0) {
	var charset = _v0.charset;
	var imports = _v0.imports;
	var namespaces = _v0.namespaces;
	var declarations = _v0.declarations;
	return {
		charset: charset,
		declarations: $rtfeldman$elm_css$Css$Structure$compactDeclarations(declarations),
		imports: imports,
		namespaces: namespaces
	};
};
var $elm$core$Maybe$map = F2(
	function (f, maybe) {
		if (maybe.$ === 'Just') {
			var value = maybe.a;
			return $elm$core$Maybe$Just(
				f(value));
		} else {
			return $elm$core$Maybe$Nothing;
		}
	});
var $rtfeldman$elm_css$Css$Structure$Output$charsetToString = function (charset) {
	return A2(
		$elm$core$Maybe$withDefault,
		'',
		A2(
			$elm$core$Maybe$map,
			function (str) {
				return '@charset \"' + (str + '\"');
			},
			charset));
};
var $rtfeldman$elm_css$Css$String$mapJoinHelp = F4(
	function (map, sep, strs, result) {
		mapJoinHelp:
		while (true) {
			if (!strs.b) {
				return result;
			} else {
				if (!strs.b.b) {
					var first = strs.a;
					return result + (map(first) + '');
				} else {
					var first = strs.a;
					var rest = strs.b;
					var $temp$map = map,
						$temp$sep = sep,
						$temp$strs = rest,
						$temp$result = result + (map(first) + (sep + ''));
					map = $temp$map;
					sep = $temp$sep;
					strs = $temp$strs;
					result = $temp$result;
					continue mapJoinHelp;
				}
			}
		}
	});
var $rtfeldman$elm_css$Css$String$mapJoin = F3(
	function (map, sep, strs) {
		return A4($rtfeldman$elm_css$Css$String$mapJoinHelp, map, sep, strs, '');
	});
var $rtfeldman$elm_css$Css$Structure$Output$mediaExpressionToString = function (expression) {
	return '(' + (expression.feature + (A2(
		$elm$core$Maybe$withDefault,
		'',
		A2(
			$elm$core$Maybe$map,
			$elm$core$Basics$append(': '),
			expression.value)) + ')'));
};
var $rtfeldman$elm_css$Css$Structure$Output$mediaTypeToString = function (mediaType) {
	switch (mediaType.$) {
		case 'Print':
			return 'print';
		case 'Screen':
			return 'screen';
		default:
			return 'speech';
	}
};
var $rtfeldman$elm_css$Css$Structure$Output$mediaQueryToString = function (mediaQuery) {
	var prefixWith = F3(
		function (str, mediaType, expressions) {
			return str + (' ' + A2(
				$elm$core$String$join,
				' and ',
				A2(
					$elm$core$List$cons,
					$rtfeldman$elm_css$Css$Structure$Output$mediaTypeToString(mediaType),
					A2($elm$core$List$map, $rtfeldman$elm_css$Css$Structure$Output$mediaExpressionToString, expressions))));
		});
	switch (mediaQuery.$) {
		case 'AllQuery':
			var expressions = mediaQuery.a;
			return A3($rtfeldman$elm_css$Css$String$mapJoin, $rtfeldman$elm_css$Css$Structure$Output$mediaExpressionToString, ' and ', expressions);
		case 'OnlyQuery':
			var mediaType = mediaQuery.a;
			var expressions = mediaQuery.b;
			return A3(prefixWith, 'only', mediaType, expressions);
		case 'NotQuery':
			var mediaType = mediaQuery.a;
			var expressions = mediaQuery.b;
			return A3(prefixWith, 'not', mediaType, expressions);
		default:
			var str = mediaQuery.a;
			return str;
	}
};
var $rtfeldman$elm_css$Css$Structure$Output$importMediaQueryToString = F2(
	function (name, mediaQuery) {
		return '@import \"' + (name + ($rtfeldman$elm_css$Css$Structure$Output$mediaQueryToString(mediaQuery) + '\"'));
	});
var $rtfeldman$elm_css$Css$Structure$Output$importToString = function (_v0) {
	var name = _v0.a;
	var mediaQueries = _v0.b;
	return A3(
		$rtfeldman$elm_css$Css$String$mapJoin,
		$rtfeldman$elm_css$Css$Structure$Output$importMediaQueryToString(name),
		'\n',
		mediaQueries);
};
var $rtfeldman$elm_css$Css$Structure$Output$namespaceToString = function (_v0) {
	var prefix = _v0.a;
	var str = _v0.b;
	return '@namespace ' + (prefix + ('\"' + (str + '\"')));
};
var $rtfeldman$elm_css$Css$Structure$Output$emitProperties = function (properties) {
	return A3(
		$rtfeldman$elm_css$Css$String$mapJoin,
		function (_v0) {
			var prop = _v0.a;
			return prop + ';';
		},
		'',
		properties);
};
var $elm$core$String$append = _String_append;
var $rtfeldman$elm_css$Css$Structure$Output$pseudoElementToString = function (_v0) {
	var str = _v0.a;
	return '::' + str;
};
var $rtfeldman$elm_css$Css$Structure$Output$combinatorToString = function (combinator) {
	switch (combinator.$) {
		case 'AdjacentSibling':
			return '+';
		case 'GeneralSibling':
			return '~';
		case 'Child':
			return '>';
		default:
			return '';
	}
};
var $rtfeldman$elm_css$Css$Structure$Output$repeatableSimpleSelectorToString = function (repeatableSimpleSelector) {
	switch (repeatableSimpleSelector.$) {
		case 'ClassSelector':
			var str = repeatableSimpleSelector.a;
			return '.' + str;
		case 'IdSelector':
			var str = repeatableSimpleSelector.a;
			return '#' + str;
		case 'PseudoClassSelector':
			var str = repeatableSimpleSelector.a;
			return ':' + str;
		default:
			var str = repeatableSimpleSelector.a;
			return '[' + (str + ']');
	}
};
var $rtfeldman$elm_css$Css$Structure$Output$simpleSelectorSequenceToString = function (simpleSelectorSequence) {
	switch (simpleSelectorSequence.$) {
		case 'TypeSelectorSequence':
			var str = simpleSelectorSequence.a.a;
			var repeatableSimpleSelectors = simpleSelectorSequence.b;
			return _Utils_ap(
				str,
				A3($rtfeldman$elm_css$Css$String$mapJoin, $rtfeldman$elm_css$Css$Structure$Output$repeatableSimpleSelectorToString, '', repeatableSimpleSelectors));
		case 'UniversalSelectorSequence':
			var repeatableSimpleSelectors = simpleSelectorSequence.a;
			return $elm$core$List$isEmpty(repeatableSimpleSelectors) ? '*' : A3($rtfeldman$elm_css$Css$String$mapJoin, $rtfeldman$elm_css$Css$Structure$Output$repeatableSimpleSelectorToString, '', repeatableSimpleSelectors);
		default:
			var str = simpleSelectorSequence.a;
			var repeatableSimpleSelectors = simpleSelectorSequence.b;
			return _Utils_ap(
				str,
				A3($rtfeldman$elm_css$Css$String$mapJoin, $rtfeldman$elm_css$Css$Structure$Output$repeatableSimpleSelectorToString, '', repeatableSimpleSelectors));
	}
};
var $rtfeldman$elm_css$Css$Structure$Output$selectorChainToString = function (_v0) {
	var combinator = _v0.a;
	var sequence = _v0.b;
	return $rtfeldman$elm_css$Css$Structure$Output$combinatorToString(combinator) + (' ' + $rtfeldman$elm_css$Css$Structure$Output$simpleSelectorSequenceToString(sequence));
};
var $rtfeldman$elm_css$Css$Structure$Output$selectorToString = function (_v0) {
	var simpleSelectorSequence = _v0.a;
	var chain = _v0.b;
	var pseudoElement = _v0.c;
	var segments = A2(
		$elm$core$List$cons,
		$rtfeldman$elm_css$Css$Structure$Output$simpleSelectorSequenceToString(simpleSelectorSequence),
		A2($elm$core$List$map, $rtfeldman$elm_css$Css$Structure$Output$selectorChainToString, chain));
	var pseudoElementsString = A2(
		$elm$core$Maybe$withDefault,
		'',
		A2($elm$core$Maybe$map, $rtfeldman$elm_css$Css$Structure$Output$pseudoElementToString, pseudoElement));
	return A2(
		$elm$core$String$append,
		A2($elm$core$String$join, ' ', segments),
		pseudoElementsString);
};
var $rtfeldman$elm_css$Css$Structure$Output$prettyPrintStyleBlock = function (_v0) {
	var firstSelector = _v0.a;
	var otherSelectors = _v0.b;
	var properties = _v0.c;
	var selectorStr = A3(
		$rtfeldman$elm_css$Css$String$mapJoin,
		$rtfeldman$elm_css$Css$Structure$Output$selectorToString,
		',',
		A2($elm$core$List$cons, firstSelector, otherSelectors));
	return selectorStr + ('{' + ($rtfeldman$elm_css$Css$Structure$Output$emitProperties(properties) + '}'));
};
var $rtfeldman$elm_css$Css$Structure$Output$prettyPrintDeclaration = function (decl) {
	switch (decl.$) {
		case 'StyleBlockDeclaration':
			var styleBlock = decl.a;
			return $rtfeldman$elm_css$Css$Structure$Output$prettyPrintStyleBlock(styleBlock);
		case 'MediaRule':
			var mediaQueries = decl.a;
			var styleBlocks = decl.b;
			var query = A3($rtfeldman$elm_css$Css$String$mapJoin, $rtfeldman$elm_css$Css$Structure$Output$mediaQueryToString, ', ', mediaQueries);
			var blocks = A3($rtfeldman$elm_css$Css$String$mapJoin, $rtfeldman$elm_css$Css$Structure$Output$prettyPrintStyleBlock, '\n', styleBlocks);
			return '@media ' + (query + ('{' + (blocks + '}')));
		case 'SupportsRule':
			return 'TODO';
		case 'DocumentRule':
			return 'TODO';
		case 'PageRule':
			return 'TODO';
		case 'FontFace':
			return 'TODO';
		case 'Keyframes':
			var name = decl.a.name;
			var declaration = decl.a.declaration;
			return '@keyframes ' + (name + ('{' + (declaration + '}')));
		case 'Viewport':
			return 'TODO';
		case 'CounterStyle':
			return 'TODO';
		default:
			return 'TODO';
	}
};
var $rtfeldman$elm_css$Css$Structure$Output$prettyPrint = function (_v0) {
	var charset = _v0.charset;
	var imports = _v0.imports;
	var namespaces = _v0.namespaces;
	var declarations = _v0.declarations;
	return $rtfeldman$elm_css$Css$Structure$Output$charsetToString(charset) + (A3($rtfeldman$elm_css$Css$String$mapJoin, $rtfeldman$elm_css$Css$Structure$Output$importToString, '\n', imports) + (A3($rtfeldman$elm_css$Css$String$mapJoin, $rtfeldman$elm_css$Css$Structure$Output$namespaceToString, '\n', namespaces) + (A3($rtfeldman$elm_css$Css$String$mapJoin, $rtfeldman$elm_css$Css$Structure$Output$prettyPrintDeclaration, '\n', declarations) + '')));
};
var $rtfeldman$elm_css$Css$Structure$CounterStyle = function (a) {
	return {$: 'CounterStyle', a: a};
};
var $rtfeldman$elm_css$Css$Structure$FontFace = function (a) {
	return {$: 'FontFace', a: a};
};
var $rtfeldman$elm_css$Css$Structure$PageRule = function (a) {
	return {$: 'PageRule', a: a};
};
var $rtfeldman$elm_css$Css$Structure$Property = function (a) {
	return {$: 'Property', a: a};
};
var $rtfeldman$elm_css$Css$Structure$StyleBlock = F3(
	function (a, b, c) {
		return {$: 'StyleBlock', a: a, b: b, c: c};
	});
var $rtfeldman$elm_css$Css$Structure$StyleBlockDeclaration = function (a) {
	return {$: 'StyleBlockDeclaration', a: a};
};
var $rtfeldman$elm_css$Css$Structure$SupportsRule = F2(
	function (a, b) {
		return {$: 'SupportsRule', a: a, b: b};
	});
var $rtfeldman$elm_css$Css$Structure$Viewport = function (a) {
	return {$: 'Viewport', a: a};
};
var $rtfeldman$elm_css$Css$Structure$MediaRule = F2(
	function (a, b) {
		return {$: 'MediaRule', a: a, b: b};
	});
var $rtfeldman$elm_css$Css$Structure$mapLast = F2(
	function (update, list) {
		if (!list.b) {
			return list;
		} else {
			if (!list.b.b) {
				var only = list.a;
				return _List_fromArray(
					[
						update(only)
					]);
			} else {
				var first = list.a;
				var rest = list.b;
				return A2(
					$elm$core$List$cons,
					first,
					A2($rtfeldman$elm_css$Css$Structure$mapLast, update, rest));
			}
		}
	});
var $rtfeldman$elm_css$Css$Structure$withPropertyAppended = F2(
	function (property, _v0) {
		var firstSelector = _v0.a;
		var otherSelectors = _v0.b;
		var properties = _v0.c;
		return A3(
			$rtfeldman$elm_css$Css$Structure$StyleBlock,
			firstSelector,
			otherSelectors,
			_Utils_ap(
				properties,
				_List_fromArray(
					[property])));
	});
var $rtfeldman$elm_css$Css$Structure$appendProperty = F2(
	function (property, declarations) {
		if (!declarations.b) {
			return declarations;
		} else {
			if (!declarations.b.b) {
				switch (declarations.a.$) {
					case 'StyleBlockDeclaration':
						var styleBlock = declarations.a.a;
						return _List_fromArray(
							[
								$rtfeldman$elm_css$Css$Structure$StyleBlockDeclaration(
								A2($rtfeldman$elm_css$Css$Structure$withPropertyAppended, property, styleBlock))
							]);
					case 'MediaRule':
						var _v1 = declarations.a;
						var mediaQueries = _v1.a;
						var styleBlocks = _v1.b;
						return _List_fromArray(
							[
								A2(
								$rtfeldman$elm_css$Css$Structure$MediaRule,
								mediaQueries,
								A2(
									$rtfeldman$elm_css$Css$Structure$mapLast,
									$rtfeldman$elm_css$Css$Structure$withPropertyAppended(property),
									styleBlocks))
							]);
					default:
						return declarations;
				}
			} else {
				var first = declarations.a;
				var rest = declarations.b;
				return A2(
					$elm$core$List$cons,
					first,
					A2($rtfeldman$elm_css$Css$Structure$appendProperty, property, rest));
			}
		}
	});
var $rtfeldman$elm_css$Css$Structure$appendToLastSelector = F2(
	function (f, styleBlock) {
		if (!styleBlock.b.b) {
			var only = styleBlock.a;
			var properties = styleBlock.c;
			return _List_fromArray(
				[
					A3($rtfeldman$elm_css$Css$Structure$StyleBlock, only, _List_Nil, properties),
					A3(
					$rtfeldman$elm_css$Css$Structure$StyleBlock,
					f(only),
					_List_Nil,
					_List_Nil)
				]);
		} else {
			var first = styleBlock.a;
			var rest = styleBlock.b;
			var properties = styleBlock.c;
			var newRest = A2($elm$core$List$map, f, rest);
			var newFirst = f(first);
			return _List_fromArray(
				[
					A3($rtfeldman$elm_css$Css$Structure$StyleBlock, first, rest, properties),
					A3($rtfeldman$elm_css$Css$Structure$StyleBlock, newFirst, newRest, _List_Nil)
				]);
		}
	});
var $rtfeldman$elm_css$Css$Structure$applyPseudoElement = F2(
	function (pseudo, _v0) {
		var sequence = _v0.a;
		var selectors = _v0.b;
		return A3(
			$rtfeldman$elm_css$Css$Structure$Selector,
			sequence,
			selectors,
			$elm$core$Maybe$Just(pseudo));
	});
var $rtfeldman$elm_css$Css$Structure$appendPseudoElementToLastSelector = F2(
	function (pseudo, styleBlock) {
		return A2(
			$rtfeldman$elm_css$Css$Structure$appendToLastSelector,
			$rtfeldman$elm_css$Css$Structure$applyPseudoElement(pseudo),
			styleBlock);
	});
var $rtfeldman$elm_css$Css$Structure$appendRepeatableWithCombinator = F2(
	function (selector, list) {
		if (!list.b) {
			return _List_Nil;
		} else {
			if (!list.b.b) {
				var _v1 = list.a;
				var combinator = _v1.a;
				var sequence = _v1.b;
				return _List_fromArray(
					[
						_Utils_Tuple2(
						combinator,
						A2($rtfeldman$elm_css$Css$Structure$appendRepeatable, selector, sequence))
					]);
			} else {
				var first = list.a;
				var rest = list.b;
				return A2(
					$elm$core$List$cons,
					first,
					A2($rtfeldman$elm_css$Css$Structure$appendRepeatableWithCombinator, selector, rest));
			}
		}
	});
var $rtfeldman$elm_css$Css$Structure$appendRepeatableSelector = F2(
	function (repeatableSimpleSelector, selector) {
		if (!selector.b.b) {
			var sequence = selector.a;
			var pseudoElement = selector.c;
			return A3(
				$rtfeldman$elm_css$Css$Structure$Selector,
				A2($rtfeldman$elm_css$Css$Structure$appendRepeatable, repeatableSimpleSelector, sequence),
				_List_Nil,
				pseudoElement);
		} else {
			var firstSelector = selector.a;
			var tuples = selector.b;
			var pseudoElement = selector.c;
			return A3(
				$rtfeldman$elm_css$Css$Structure$Selector,
				firstSelector,
				A2($rtfeldman$elm_css$Css$Structure$appendRepeatableWithCombinator, repeatableSimpleSelector, tuples),
				pseudoElement);
		}
	});
var $rtfeldman$elm_css$Css$Structure$appendRepeatableToLastSelector = F2(
	function (selector, styleBlock) {
		return A2(
			$rtfeldman$elm_css$Css$Structure$appendToLastSelector,
			$rtfeldman$elm_css$Css$Structure$appendRepeatableSelector(selector),
			styleBlock);
	});
var $rtfeldman$elm_css$Css$Preprocess$Resolve$collectSelectors = function (declarations) {
	collectSelectors:
	while (true) {
		if (!declarations.b) {
			return _List_Nil;
		} else {
			if (declarations.a.$ === 'StyleBlockDeclaration') {
				var _v1 = declarations.a.a;
				var firstSelector = _v1.a;
				var otherSelectors = _v1.b;
				var rest = declarations.b;
				return _Utils_ap(
					A2($elm$core$List$cons, firstSelector, otherSelectors),
					$rtfeldman$elm_css$Css$Preprocess$Resolve$collectSelectors(rest));
			} else {
				var rest = declarations.b;
				var $temp$declarations = rest;
				declarations = $temp$declarations;
				continue collectSelectors;
			}
		}
	}
};
var $rtfeldman$elm_css$Css$Structure$DocumentRule = F5(
	function (a, b, c, d, e) {
		return {$: 'DocumentRule', a: a, b: b, c: c, d: d, e: e};
	});
var $rtfeldman$elm_css$Css$Structure$concatMapLastStyleBlock = F2(
	function (update, declarations) {
		_v0$12:
		while (true) {
			if (!declarations.b) {
				return declarations;
			} else {
				if (!declarations.b.b) {
					switch (declarations.a.$) {
						case 'StyleBlockDeclaration':
							var styleBlock = declarations.a.a;
							return A2(
								$elm$core$List$map,
								$rtfeldman$elm_css$Css$Structure$StyleBlockDeclaration,
								update(styleBlock));
						case 'MediaRule':
							if (declarations.a.b.b) {
								if (!declarations.a.b.b.b) {
									var _v1 = declarations.a;
									var mediaQueries = _v1.a;
									var _v2 = _v1.b;
									var styleBlock = _v2.a;
									return _List_fromArray(
										[
											A2(
											$rtfeldman$elm_css$Css$Structure$MediaRule,
											mediaQueries,
											update(styleBlock))
										]);
								} else {
									var _v3 = declarations.a;
									var mediaQueries = _v3.a;
									var _v4 = _v3.b;
									var first = _v4.a;
									var rest = _v4.b;
									var _v5 = A2(
										$rtfeldman$elm_css$Css$Structure$concatMapLastStyleBlock,
										update,
										_List_fromArray(
											[
												A2($rtfeldman$elm_css$Css$Structure$MediaRule, mediaQueries, rest)
											]));
									if ((_v5.b && (_v5.a.$ === 'MediaRule')) && (!_v5.b.b)) {
										var _v6 = _v5.a;
										var newMediaQueries = _v6.a;
										var newStyleBlocks = _v6.b;
										return _List_fromArray(
											[
												A2(
												$rtfeldman$elm_css$Css$Structure$MediaRule,
												newMediaQueries,
												A2($elm$core$List$cons, first, newStyleBlocks))
											]);
									} else {
										var newDeclarations = _v5;
										return newDeclarations;
									}
								}
							} else {
								break _v0$12;
							}
						case 'SupportsRule':
							var _v7 = declarations.a;
							var str = _v7.a;
							var nestedDeclarations = _v7.b;
							return _List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Css$Structure$SupportsRule,
									str,
									A2($rtfeldman$elm_css$Css$Structure$concatMapLastStyleBlock, update, nestedDeclarations))
								]);
						case 'DocumentRule':
							var _v8 = declarations.a;
							var str1 = _v8.a;
							var str2 = _v8.b;
							var str3 = _v8.c;
							var str4 = _v8.d;
							var styleBlock = _v8.e;
							return A2(
								$elm$core$List$map,
								A4($rtfeldman$elm_css$Css$Structure$DocumentRule, str1, str2, str3, str4),
								update(styleBlock));
						case 'PageRule':
							return declarations;
						case 'FontFace':
							return declarations;
						case 'Keyframes':
							return declarations;
						case 'Viewport':
							return declarations;
						case 'CounterStyle':
							return declarations;
						default:
							return declarations;
					}
				} else {
					break _v0$12;
				}
			}
		}
		var first = declarations.a;
		var rest = declarations.b;
		return A2(
			$elm$core$List$cons,
			first,
			A2($rtfeldman$elm_css$Css$Structure$concatMapLastStyleBlock, update, rest));
	});
var $robinheghan$murmur3$Murmur3$HashData = F4(
	function (shift, seed, hash, charsProcessed) {
		return {charsProcessed: charsProcessed, hash: hash, seed: seed, shift: shift};
	});
var $robinheghan$murmur3$Murmur3$c1 = 3432918353;
var $robinheghan$murmur3$Murmur3$c2 = 461845907;
var $elm$core$Bitwise$and = _Bitwise_and;
var $elm$core$Bitwise$shiftLeftBy = _Bitwise_shiftLeftBy;
var $elm$core$Bitwise$shiftRightZfBy = _Bitwise_shiftRightZfBy;
var $robinheghan$murmur3$Murmur3$multiplyBy = F2(
	function (b, a) {
		return ((a & 65535) * b) + ((((a >>> 16) * b) & 65535) << 16);
	});
var $elm$core$Bitwise$or = _Bitwise_or;
var $robinheghan$murmur3$Murmur3$rotlBy = F2(
	function (b, a) {
		return (a << b) | (a >>> (32 - b));
	});
var $elm$core$Bitwise$xor = _Bitwise_xor;
var $robinheghan$murmur3$Murmur3$finalize = function (data) {
	var acc = (!(!data.hash)) ? (data.seed ^ A2(
		$robinheghan$murmur3$Murmur3$multiplyBy,
		$robinheghan$murmur3$Murmur3$c2,
		A2(
			$robinheghan$murmur3$Murmur3$rotlBy,
			15,
			A2($robinheghan$murmur3$Murmur3$multiplyBy, $robinheghan$murmur3$Murmur3$c1, data.hash)))) : data.seed;
	var h0 = acc ^ data.charsProcessed;
	var h1 = A2($robinheghan$murmur3$Murmur3$multiplyBy, 2246822507, h0 ^ (h0 >>> 16));
	var h2 = A2($robinheghan$murmur3$Murmur3$multiplyBy, 3266489909, h1 ^ (h1 >>> 13));
	return (h2 ^ (h2 >>> 16)) >>> 0;
};
var $robinheghan$murmur3$Murmur3$mix = F2(
	function (h1, k1) {
		return A2(
			$robinheghan$murmur3$Murmur3$multiplyBy,
			5,
			A2(
				$robinheghan$murmur3$Murmur3$rotlBy,
				13,
				h1 ^ A2(
					$robinheghan$murmur3$Murmur3$multiplyBy,
					$robinheghan$murmur3$Murmur3$c2,
					A2(
						$robinheghan$murmur3$Murmur3$rotlBy,
						15,
						A2($robinheghan$murmur3$Murmur3$multiplyBy, $robinheghan$murmur3$Murmur3$c1, k1))))) + 3864292196;
	});
var $robinheghan$murmur3$Murmur3$hashFold = F2(
	function (c, data) {
		var res = data.hash | ((255 & $elm$core$Char$toCode(c)) << data.shift);
		var _v0 = data.shift;
		if (_v0 === 24) {
			return {
				charsProcessed: data.charsProcessed + 1,
				hash: 0,
				seed: A2($robinheghan$murmur3$Murmur3$mix, data.seed, res),
				shift: 0
			};
		} else {
			return {charsProcessed: data.charsProcessed + 1, hash: res, seed: data.seed, shift: data.shift + 8};
		}
	});
var $robinheghan$murmur3$Murmur3$hashString = F2(
	function (seed, str) {
		return $robinheghan$murmur3$Murmur3$finalize(
			A3(
				$elm$core$String$foldl,
				$robinheghan$murmur3$Murmur3$hashFold,
				A4($robinheghan$murmur3$Murmur3$HashData, 0, seed, 0, 0),
				str));
	});
var $rtfeldman$elm_css$Hash$initialSeed = 15739;
var $elm$core$String$fromList = _String_fromList;
var $elm$core$Basics$modBy = _Basics_modBy;
var $rtfeldman$elm_hex$Hex$unsafeToDigit = function (num) {
	unsafeToDigit:
	while (true) {
		switch (num) {
			case 0:
				return _Utils_chr('0');
			case 1:
				return _Utils_chr('1');
			case 2:
				return _Utils_chr('2');
			case 3:
				return _Utils_chr('3');
			case 4:
				return _Utils_chr('4');
			case 5:
				return _Utils_chr('5');
			case 6:
				return _Utils_chr('6');
			case 7:
				return _Utils_chr('7');
			case 8:
				return _Utils_chr('8');
			case 9:
				return _Utils_chr('9');
			case 10:
				return _Utils_chr('a');
			case 11:
				return _Utils_chr('b');
			case 12:
				return _Utils_chr('c');
			case 13:
				return _Utils_chr('d');
			case 14:
				return _Utils_chr('e');
			case 15:
				return _Utils_chr('f');
			default:
				var $temp$num = num;
				num = $temp$num;
				continue unsafeToDigit;
		}
	}
};
var $rtfeldman$elm_hex$Hex$unsafePositiveToDigits = F2(
	function (digits, num) {
		unsafePositiveToDigits:
		while (true) {
			if (num < 16) {
				return A2(
					$elm$core$List$cons,
					$rtfeldman$elm_hex$Hex$unsafeToDigit(num),
					digits);
			} else {
				var $temp$digits = A2(
					$elm$core$List$cons,
					$rtfeldman$elm_hex$Hex$unsafeToDigit(
						A2($elm$core$Basics$modBy, 16, num)),
					digits),
					$temp$num = (num / 16) | 0;
				digits = $temp$digits;
				num = $temp$num;
				continue unsafePositiveToDigits;
			}
		}
	});
var $rtfeldman$elm_hex$Hex$toString = function (num) {
	return $elm$core$String$fromList(
		(num < 0) ? A2(
			$elm$core$List$cons,
			_Utils_chr('-'),
			A2($rtfeldman$elm_hex$Hex$unsafePositiveToDigits, _List_Nil, -num)) : A2($rtfeldman$elm_hex$Hex$unsafePositiveToDigits, _List_Nil, num));
};
var $rtfeldman$elm_css$Hash$fromString = function (str) {
	return A2(
		$elm$core$String$cons,
		_Utils_chr('_'),
		$rtfeldman$elm_hex$Hex$toString(
			A2($robinheghan$murmur3$Murmur3$hashString, $rtfeldman$elm_css$Hash$initialSeed, str)));
};
var $elm$core$List$head = function (list) {
	if (list.b) {
		var x = list.a;
		var xs = list.b;
		return $elm$core$Maybe$Just(x);
	} else {
		return $elm$core$Maybe$Nothing;
	}
};
var $rtfeldman$elm_css$Css$Preprocess$Resolve$last = function (list) {
	last:
	while (true) {
		if (!list.b) {
			return $elm$core$Maybe$Nothing;
		} else {
			if (!list.b.b) {
				var singleton = list.a;
				return $elm$core$Maybe$Just(singleton);
			} else {
				var rest = list.b;
				var $temp$list = rest;
				list = $temp$list;
				continue last;
			}
		}
	}
};
var $rtfeldman$elm_css$Css$Preprocess$Resolve$lastDeclaration = function (declarations) {
	lastDeclaration:
	while (true) {
		if (!declarations.b) {
			return $elm$core$Maybe$Nothing;
		} else {
			if (!declarations.b.b) {
				var x = declarations.a;
				return $elm$core$Maybe$Just(
					_List_fromArray(
						[x]));
			} else {
				var xs = declarations.b;
				var $temp$declarations = xs;
				declarations = $temp$declarations;
				continue lastDeclaration;
			}
		}
	}
};
var $rtfeldman$elm_css$Css$Preprocess$Resolve$oneOf = function (maybes) {
	oneOf:
	while (true) {
		if (!maybes.b) {
			return $elm$core$Maybe$Nothing;
		} else {
			var maybe = maybes.a;
			var rest = maybes.b;
			if (maybe.$ === 'Nothing') {
				var $temp$maybes = rest;
				maybes = $temp$maybes;
				continue oneOf;
			} else {
				return maybe;
			}
		}
	}
};
var $rtfeldman$elm_css$Css$Structure$FontFeatureValues = function (a) {
	return {$: 'FontFeatureValues', a: a};
};
var $rtfeldman$elm_css$Css$Preprocess$Resolve$resolveFontFeatureValues = function (tuples) {
	var expandTuples = function (tuplesToExpand) {
		if (!tuplesToExpand.b) {
			return _List_Nil;
		} else {
			var properties = tuplesToExpand.a;
			var rest = tuplesToExpand.b;
			return A2(
				$elm$core$List$cons,
				properties,
				expandTuples(rest));
		}
	};
	var newTuples = expandTuples(tuples);
	return _List_fromArray(
		[
			$rtfeldman$elm_css$Css$Structure$FontFeatureValues(newTuples)
		]);
};
var $elm$core$List$singleton = function (value) {
	return _List_fromArray(
		[value]);
};
var $rtfeldman$elm_css$Css$Structure$styleBlockToMediaRule = F2(
	function (mediaQueries, declaration) {
		if (declaration.$ === 'StyleBlockDeclaration') {
			var styleBlock = declaration.a;
			return A2(
				$rtfeldman$elm_css$Css$Structure$MediaRule,
				mediaQueries,
				_List_fromArray(
					[styleBlock]));
		} else {
			return declaration;
		}
	});
var $elm$core$List$tail = function (list) {
	if (list.b) {
		var x = list.a;
		var xs = list.b;
		return $elm$core$Maybe$Just(xs);
	} else {
		return $elm$core$Maybe$Nothing;
	}
};
var $elm$core$List$takeReverse = F3(
	function (n, list, kept) {
		takeReverse:
		while (true) {
			if (n <= 0) {
				return kept;
			} else {
				if (!list.b) {
					return kept;
				} else {
					var x = list.a;
					var xs = list.b;
					var $temp$n = n - 1,
						$temp$list = xs,
						$temp$kept = A2($elm$core$List$cons, x, kept);
					n = $temp$n;
					list = $temp$list;
					kept = $temp$kept;
					continue takeReverse;
				}
			}
		}
	});
var $elm$core$List$takeTailRec = F2(
	function (n, list) {
		return $elm$core$List$reverse(
			A3($elm$core$List$takeReverse, n, list, _List_Nil));
	});
var $elm$core$List$takeFast = F3(
	function (ctr, n, list) {
		if (n <= 0) {
			return _List_Nil;
		} else {
			var _v0 = _Utils_Tuple2(n, list);
			_v0$1:
			while (true) {
				_v0$5:
				while (true) {
					if (!_v0.b.b) {
						return list;
					} else {
						if (_v0.b.b.b) {
							switch (_v0.a) {
								case 1:
									break _v0$1;
								case 2:
									var _v2 = _v0.b;
									var x = _v2.a;
									var _v3 = _v2.b;
									var y = _v3.a;
									return _List_fromArray(
										[x, y]);
								case 3:
									if (_v0.b.b.b.b) {
										var _v4 = _v0.b;
										var x = _v4.a;
										var _v5 = _v4.b;
										var y = _v5.a;
										var _v6 = _v5.b;
										var z = _v6.a;
										return _List_fromArray(
											[x, y, z]);
									} else {
										break _v0$5;
									}
								default:
									if (_v0.b.b.b.b && _v0.b.b.b.b.b) {
										var _v7 = _v0.b;
										var x = _v7.a;
										var _v8 = _v7.b;
										var y = _v8.a;
										var _v9 = _v8.b;
										var z = _v9.a;
										var _v10 = _v9.b;
										var w = _v10.a;
										var tl = _v10.b;
										return (ctr > 1000) ? A2(
											$elm$core$List$cons,
											x,
											A2(
												$elm$core$List$cons,
												y,
												A2(
													$elm$core$List$cons,
													z,
													A2(
														$elm$core$List$cons,
														w,
														A2($elm$core$List$takeTailRec, n - 4, tl))))) : A2(
											$elm$core$List$cons,
											x,
											A2(
												$elm$core$List$cons,
												y,
												A2(
													$elm$core$List$cons,
													z,
													A2(
														$elm$core$List$cons,
														w,
														A3($elm$core$List$takeFast, ctr + 1, n - 4, tl)))));
									} else {
										break _v0$5;
									}
							}
						} else {
							if (_v0.a === 1) {
								break _v0$1;
							} else {
								break _v0$5;
							}
						}
					}
				}
				return list;
			}
			var _v1 = _v0.b;
			var x = _v1.a;
			return _List_fromArray(
				[x]);
		}
	});
var $elm$core$List$take = F2(
	function (n, list) {
		return A3($elm$core$List$takeFast, 0, n, list);
	});
var $rtfeldman$elm_css$Css$Preprocess$Resolve$toDocumentRule = F5(
	function (str1, str2, str3, str4, declaration) {
		if (declaration.$ === 'StyleBlockDeclaration') {
			var structureStyleBlock = declaration.a;
			return A5($rtfeldman$elm_css$Css$Structure$DocumentRule, str1, str2, str3, str4, structureStyleBlock);
		} else {
			return declaration;
		}
	});
var $rtfeldman$elm_css$Css$Preprocess$Resolve$toMediaRule = F2(
	function (mediaQueries, declaration) {
		switch (declaration.$) {
			case 'StyleBlockDeclaration':
				var structureStyleBlock = declaration.a;
				return A2(
					$rtfeldman$elm_css$Css$Structure$MediaRule,
					mediaQueries,
					_List_fromArray(
						[structureStyleBlock]));
			case 'MediaRule':
				var newMediaQueries = declaration.a;
				var structureStyleBlocks = declaration.b;
				return A2(
					$rtfeldman$elm_css$Css$Structure$MediaRule,
					_Utils_ap(mediaQueries, newMediaQueries),
					structureStyleBlocks);
			case 'SupportsRule':
				var str = declaration.a;
				var declarations = declaration.b;
				return A2(
					$rtfeldman$elm_css$Css$Structure$SupportsRule,
					str,
					A2(
						$elm$core$List$map,
						$rtfeldman$elm_css$Css$Preprocess$Resolve$toMediaRule(mediaQueries),
						declarations));
			case 'DocumentRule':
				var str1 = declaration.a;
				var str2 = declaration.b;
				var str3 = declaration.c;
				var str4 = declaration.d;
				var structureStyleBlock = declaration.e;
				return A5($rtfeldman$elm_css$Css$Structure$DocumentRule, str1, str2, str3, str4, structureStyleBlock);
			case 'PageRule':
				return declaration;
			case 'FontFace':
				return declaration;
			case 'Keyframes':
				return declaration;
			case 'Viewport':
				return declaration;
			case 'CounterStyle':
				return declaration;
			default:
				return declaration;
		}
	});
var $rtfeldman$elm_css$Css$Preprocess$Resolve$applyNestedStylesToLast = F4(
	function (nestedStyles, rest, f, declarations) {
		var withoutParent = function (decls) {
			return A2(
				$elm$core$Maybe$withDefault,
				_List_Nil,
				$elm$core$List$tail(decls));
		};
		var nextResult = A2(
			$rtfeldman$elm_css$Css$Preprocess$Resolve$applyStyles,
			rest,
			A2(
				$elm$core$Maybe$withDefault,
				_List_Nil,
				$rtfeldman$elm_css$Css$Preprocess$Resolve$lastDeclaration(declarations)));
		var newDeclarations = function () {
			var _v14 = _Utils_Tuple2(
				$elm$core$List$head(nextResult),
				$rtfeldman$elm_css$Css$Preprocess$Resolve$last(declarations));
			if ((_v14.a.$ === 'Just') && (_v14.b.$ === 'Just')) {
				var nextResultParent = _v14.a.a;
				var originalParent = _v14.b.a;
				return _Utils_ap(
					A2(
						$elm$core$List$take,
						$elm$core$List$length(declarations) - 1,
						declarations),
					_List_fromArray(
						[
							(!_Utils_eq(originalParent, nextResultParent)) ? nextResultParent : originalParent
						]));
			} else {
				return declarations;
			}
		}();
		var insertStylesToNestedDecl = function (lastDecl) {
			return $elm$core$List$concat(
				A2(
					$rtfeldman$elm_css$Css$Structure$mapLast,
					$rtfeldman$elm_css$Css$Preprocess$Resolve$applyStyles(nestedStyles),
					A2(
						$elm$core$List$map,
						$elm$core$List$singleton,
						A2($rtfeldman$elm_css$Css$Structure$concatMapLastStyleBlock, f, lastDecl))));
		};
		var initialResult = A2(
			$elm$core$Maybe$withDefault,
			_List_Nil,
			A2(
				$elm$core$Maybe$map,
				insertStylesToNestedDecl,
				$rtfeldman$elm_css$Css$Preprocess$Resolve$lastDeclaration(declarations)));
		return _Utils_ap(
			newDeclarations,
			_Utils_ap(
				withoutParent(initialResult),
				withoutParent(nextResult)));
	});
var $rtfeldman$elm_css$Css$Preprocess$Resolve$applyStyles = F2(
	function (styles, declarations) {
		if (!styles.b) {
			return declarations;
		} else {
			switch (styles.a.$) {
				case 'AppendProperty':
					var property = styles.a.a;
					var rest = styles.b;
					return A2(
						$rtfeldman$elm_css$Css$Preprocess$Resolve$applyStyles,
						rest,
						A2($rtfeldman$elm_css$Css$Structure$appendProperty, property, declarations));
				case 'ExtendSelector':
					var _v4 = styles.a;
					var selector = _v4.a;
					var nestedStyles = _v4.b;
					var rest = styles.b;
					return A4(
						$rtfeldman$elm_css$Css$Preprocess$Resolve$applyNestedStylesToLast,
						nestedStyles,
						rest,
						$rtfeldman$elm_css$Css$Structure$appendRepeatableToLastSelector(selector),
						declarations);
				case 'NestSnippet':
					var _v5 = styles.a;
					var selectorCombinator = _v5.a;
					var snippets = _v5.b;
					var rest = styles.b;
					var chain = F2(
						function (_v9, _v10) {
							var originalSequence = _v9.a;
							var originalTuples = _v9.b;
							var originalPseudoElement = _v9.c;
							var newSequence = _v10.a;
							var newTuples = _v10.b;
							var newPseudoElement = _v10.c;
							return A3(
								$rtfeldman$elm_css$Css$Structure$Selector,
								originalSequence,
								_Utils_ap(
									originalTuples,
									A2(
										$elm$core$List$cons,
										_Utils_Tuple2(selectorCombinator, newSequence),
										newTuples)),
								$rtfeldman$elm_css$Css$Preprocess$Resolve$oneOf(
									_List_fromArray(
										[newPseudoElement, originalPseudoElement])));
						});
					var expandDeclaration = function (declaration) {
						switch (declaration.$) {
							case 'StyleBlockDeclaration':
								var _v7 = declaration.a;
								var firstSelector = _v7.a;
								var otherSelectors = _v7.b;
								var nestedStyles = _v7.c;
								var newSelectors = A2(
									$elm$core$List$concatMap,
									function (originalSelector) {
										return A2(
											$elm$core$List$map,
											chain(originalSelector),
											A2($elm$core$List$cons, firstSelector, otherSelectors));
									},
									$rtfeldman$elm_css$Css$Preprocess$Resolve$collectSelectors(declarations));
								var newDeclarations = function () {
									if (!newSelectors.b) {
										return _List_Nil;
									} else {
										var first = newSelectors.a;
										var remainder = newSelectors.b;
										return _List_fromArray(
											[
												$rtfeldman$elm_css$Css$Structure$StyleBlockDeclaration(
												A3($rtfeldman$elm_css$Css$Structure$StyleBlock, first, remainder, _List_Nil))
											]);
									}
								}();
								return A2($rtfeldman$elm_css$Css$Preprocess$Resolve$applyStyles, nestedStyles, newDeclarations);
							case 'MediaRule':
								var mediaQueries = declaration.a;
								var styleBlocks = declaration.b;
								return A2($rtfeldman$elm_css$Css$Preprocess$Resolve$resolveMediaRule, mediaQueries, styleBlocks);
							case 'SupportsRule':
								var str = declaration.a;
								var otherSnippets = declaration.b;
								return A2($rtfeldman$elm_css$Css$Preprocess$Resolve$resolveSupportsRule, str, otherSnippets);
							case 'DocumentRule':
								var str1 = declaration.a;
								var str2 = declaration.b;
								var str3 = declaration.c;
								var str4 = declaration.d;
								var styleBlock = declaration.e;
								return A2(
									$elm$core$List$map,
									A4($rtfeldman$elm_css$Css$Preprocess$Resolve$toDocumentRule, str1, str2, str3, str4),
									$rtfeldman$elm_css$Css$Preprocess$Resolve$expandStyleBlock(styleBlock));
							case 'PageRule':
								var properties = declaration.a;
								return _List_fromArray(
									[
										$rtfeldman$elm_css$Css$Structure$PageRule(properties)
									]);
							case 'FontFace':
								var properties = declaration.a;
								return _List_fromArray(
									[
										$rtfeldman$elm_css$Css$Structure$FontFace(properties)
									]);
							case 'Viewport':
								var properties = declaration.a;
								return _List_fromArray(
									[
										$rtfeldman$elm_css$Css$Structure$Viewport(properties)
									]);
							case 'CounterStyle':
								var properties = declaration.a;
								return _List_fromArray(
									[
										$rtfeldman$elm_css$Css$Structure$CounterStyle(properties)
									]);
							default:
								var tuples = declaration.a;
								return $rtfeldman$elm_css$Css$Preprocess$Resolve$resolveFontFeatureValues(tuples);
						}
					};
					return $elm$core$List$concat(
						_Utils_ap(
							_List_fromArray(
								[
									A2($rtfeldman$elm_css$Css$Preprocess$Resolve$applyStyles, rest, declarations)
								]),
							A2(
								$elm$core$List$map,
								expandDeclaration,
								A2($elm$core$List$concatMap, $rtfeldman$elm_css$Css$Preprocess$unwrapSnippet, snippets))));
				case 'WithPseudoElement':
					var _v11 = styles.a;
					var pseudoElement = _v11.a;
					var nestedStyles = _v11.b;
					var rest = styles.b;
					return A4(
						$rtfeldman$elm_css$Css$Preprocess$Resolve$applyNestedStylesToLast,
						nestedStyles,
						rest,
						$rtfeldman$elm_css$Css$Structure$appendPseudoElementToLastSelector(pseudoElement),
						declarations);
				case 'WithKeyframes':
					var str = styles.a.a;
					var rest = styles.b;
					var name = $rtfeldman$elm_css$Hash$fromString(str);
					var newProperty = $rtfeldman$elm_css$Css$Structure$Property('animation-name:' + name);
					var newDeclarations = A2(
						$rtfeldman$elm_css$Css$Preprocess$Resolve$applyStyles,
						rest,
						A2($rtfeldman$elm_css$Css$Structure$appendProperty, newProperty, declarations));
					return A2(
						$elm$core$List$append,
						newDeclarations,
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$Structure$Keyframes(
								{declaration: str, name: name})
							]));
				case 'WithMedia':
					var _v12 = styles.a;
					var mediaQueries = _v12.a;
					var nestedStyles = _v12.b;
					var rest = styles.b;
					var extraDeclarations = function () {
						var _v13 = $rtfeldman$elm_css$Css$Preprocess$Resolve$collectSelectors(declarations);
						if (!_v13.b) {
							return _List_Nil;
						} else {
							var firstSelector = _v13.a;
							var otherSelectors = _v13.b;
							return A2(
								$elm$core$List$map,
								$rtfeldman$elm_css$Css$Structure$styleBlockToMediaRule(mediaQueries),
								A2(
									$rtfeldman$elm_css$Css$Preprocess$Resolve$applyStyles,
									nestedStyles,
									$elm$core$List$singleton(
										$rtfeldman$elm_css$Css$Structure$StyleBlockDeclaration(
											A3($rtfeldman$elm_css$Css$Structure$StyleBlock, firstSelector, otherSelectors, _List_Nil)))));
						}
					}();
					return _Utils_ap(
						A2($rtfeldman$elm_css$Css$Preprocess$Resolve$applyStyles, rest, declarations),
						extraDeclarations);
				default:
					var otherStyles = styles.a.a;
					var rest = styles.b;
					return A2(
						$rtfeldman$elm_css$Css$Preprocess$Resolve$applyStyles,
						_Utils_ap(otherStyles, rest),
						declarations);
			}
		}
	});
var $rtfeldman$elm_css$Css$Preprocess$Resolve$expandStyleBlock = function (_v2) {
	var firstSelector = _v2.a;
	var otherSelectors = _v2.b;
	var styles = _v2.c;
	return A2(
		$rtfeldman$elm_css$Css$Preprocess$Resolve$applyStyles,
		styles,
		_List_fromArray(
			[
				$rtfeldman$elm_css$Css$Structure$StyleBlockDeclaration(
				A3($rtfeldman$elm_css$Css$Structure$StyleBlock, firstSelector, otherSelectors, _List_Nil))
			]));
};
var $rtfeldman$elm_css$Css$Preprocess$Resolve$extract = function (snippetDeclarations) {
	if (!snippetDeclarations.b) {
		return _List_Nil;
	} else {
		var first = snippetDeclarations.a;
		var rest = snippetDeclarations.b;
		return _Utils_ap(
			$rtfeldman$elm_css$Css$Preprocess$Resolve$toDeclarations(first),
			$rtfeldman$elm_css$Css$Preprocess$Resolve$extract(rest));
	}
};
var $rtfeldman$elm_css$Css$Preprocess$Resolve$resolveMediaRule = F2(
	function (mediaQueries, styleBlocks) {
		var handleStyleBlock = function (styleBlock) {
			return A2(
				$elm$core$List$map,
				$rtfeldman$elm_css$Css$Preprocess$Resolve$toMediaRule(mediaQueries),
				$rtfeldman$elm_css$Css$Preprocess$Resolve$expandStyleBlock(styleBlock));
		};
		return A2($elm$core$List$concatMap, handleStyleBlock, styleBlocks);
	});
var $rtfeldman$elm_css$Css$Preprocess$Resolve$resolveSupportsRule = F2(
	function (str, snippets) {
		var declarations = $rtfeldman$elm_css$Css$Preprocess$Resolve$extract(
			A2($elm$core$List$concatMap, $rtfeldman$elm_css$Css$Preprocess$unwrapSnippet, snippets));
		return _List_fromArray(
			[
				A2($rtfeldman$elm_css$Css$Structure$SupportsRule, str, declarations)
			]);
	});
var $rtfeldman$elm_css$Css$Preprocess$Resolve$toDeclarations = function (snippetDeclaration) {
	switch (snippetDeclaration.$) {
		case 'StyleBlockDeclaration':
			var styleBlock = snippetDeclaration.a;
			return $rtfeldman$elm_css$Css$Preprocess$Resolve$expandStyleBlock(styleBlock);
		case 'MediaRule':
			var mediaQueries = snippetDeclaration.a;
			var styleBlocks = snippetDeclaration.b;
			return A2($rtfeldman$elm_css$Css$Preprocess$Resolve$resolveMediaRule, mediaQueries, styleBlocks);
		case 'SupportsRule':
			var str = snippetDeclaration.a;
			var snippets = snippetDeclaration.b;
			return A2($rtfeldman$elm_css$Css$Preprocess$Resolve$resolveSupportsRule, str, snippets);
		case 'DocumentRule':
			var str1 = snippetDeclaration.a;
			var str2 = snippetDeclaration.b;
			var str3 = snippetDeclaration.c;
			var str4 = snippetDeclaration.d;
			var styleBlock = snippetDeclaration.e;
			return A2(
				$elm$core$List$map,
				A4($rtfeldman$elm_css$Css$Preprocess$Resolve$toDocumentRule, str1, str2, str3, str4),
				$rtfeldman$elm_css$Css$Preprocess$Resolve$expandStyleBlock(styleBlock));
		case 'PageRule':
			var properties = snippetDeclaration.a;
			return _List_fromArray(
				[
					$rtfeldman$elm_css$Css$Structure$PageRule(properties)
				]);
		case 'FontFace':
			var properties = snippetDeclaration.a;
			return _List_fromArray(
				[
					$rtfeldman$elm_css$Css$Structure$FontFace(properties)
				]);
		case 'Viewport':
			var properties = snippetDeclaration.a;
			return _List_fromArray(
				[
					$rtfeldman$elm_css$Css$Structure$Viewport(properties)
				]);
		case 'CounterStyle':
			var properties = snippetDeclaration.a;
			return _List_fromArray(
				[
					$rtfeldman$elm_css$Css$Structure$CounterStyle(properties)
				]);
		default:
			var tuples = snippetDeclaration.a;
			return $rtfeldman$elm_css$Css$Preprocess$Resolve$resolveFontFeatureValues(tuples);
	}
};
var $rtfeldman$elm_css$Css$Preprocess$Resolve$toStructure = function (_v0) {
	var charset = _v0.charset;
	var imports = _v0.imports;
	var namespaces = _v0.namespaces;
	var snippets = _v0.snippets;
	var declarations = $rtfeldman$elm_css$Css$Preprocess$Resolve$extract(
		A2($elm$core$List$concatMap, $rtfeldman$elm_css$Css$Preprocess$unwrapSnippet, snippets));
	return {charset: charset, declarations: declarations, imports: imports, namespaces: namespaces};
};
var $rtfeldman$elm_css$Css$Preprocess$Resolve$compile = function (sheet) {
	return $rtfeldman$elm_css$Css$Structure$Output$prettyPrint(
		$rtfeldman$elm_css$Css$Structure$compactStylesheet(
			$rtfeldman$elm_css$Css$Preprocess$Resolve$toStructure(sheet)));
};
var $elm$virtual_dom$VirtualDom$node = function (tag) {
	return _VirtualDom_node(
		_VirtualDom_noScript(tag));
};
var $rtfeldman$elm_css$Css$Preprocess$stylesheet = function (snippets) {
	return {charset: $elm$core$Maybe$Nothing, imports: _List_Nil, namespaces: _List_Nil, snippets: snippets};
};
var $elm$virtual_dom$VirtualDom$text = _VirtualDom_text;
var $rtfeldman$elm_css$VirtualDom$Styled$Unstyled = function (a) {
	return {$: 'Unstyled', a: a};
};
var $rtfeldman$elm_css$VirtualDom$Styled$unstyledNode = $rtfeldman$elm_css$VirtualDom$Styled$Unstyled;
var $rtfeldman$elm_css$Css$Global$global = function (snippets) {
	return $rtfeldman$elm_css$VirtualDom$Styled$unstyledNode(
		A3(
			$elm$virtual_dom$VirtualDom$node,
			'span',
			_List_fromArray(
				[
					A2($elm$virtual_dom$VirtualDom$attribute, 'style', 'display: none;'),
					A2($elm$virtual_dom$VirtualDom$attribute, 'class', 'elm-css-style-wrapper')
				]),
			$elm$core$List$singleton(
				A3(
					$elm$virtual_dom$VirtualDom$node,
					'style',
					_List_Nil,
					$elm$core$List$singleton(
						$elm$virtual_dom$VirtualDom$text(
							$rtfeldman$elm_css$Css$Preprocess$Resolve$compile(
								$rtfeldman$elm_css$Css$Preprocess$stylesheet(snippets))))))));
};
var $rtfeldman$elm_css$Css$Preprocess$AppendProperty = function (a) {
	return {$: 'AppendProperty', a: a};
};
var $rtfeldman$elm_css$Css$property = F2(
	function (key, value) {
		return $rtfeldman$elm_css$Css$Preprocess$AppendProperty(
			$rtfeldman$elm_css$Css$Structure$Property(key + (':' + value)));
	});
var $rtfeldman$elm_css$Css$prop1 = F2(
	function (key, arg) {
		return A2($rtfeldman$elm_css$Css$property, key, arg.value);
	});
var $rtfeldman$elm_css$Css$height = $rtfeldman$elm_css$Css$prop1('height');
var $rtfeldman$elm_css$Css$Global$html = $rtfeldman$elm_css$Css$Global$typeSelector('html');
var $rtfeldman$elm_css$Css$prop4 = F5(
	function (key, argA, argB, argC, argD) {
		return A2($rtfeldman$elm_css$Css$property, key, argA.value + (' ' + (argB.value + (' ' + (argC.value + (' ' + argD.value))))));
	});
var $rtfeldman$elm_css$Css$margin4 = $rtfeldman$elm_css$Css$prop4('margin');
var $rtfeldman$elm_css$Css$PercentageUnits = {$: 'PercentageUnits'};
var $rtfeldman$elm_css$Css$Internal$lengthConverter = F3(
	function (units, unitLabel, numericValue) {
		return {
			absoluteLength: $rtfeldman$elm_css$Css$Structure$Compatible,
			calc: $rtfeldman$elm_css$Css$Structure$Compatible,
			flexBasis: $rtfeldman$elm_css$Css$Structure$Compatible,
			fontSize: $rtfeldman$elm_css$Css$Structure$Compatible,
			length: $rtfeldman$elm_css$Css$Structure$Compatible,
			lengthOrAuto: $rtfeldman$elm_css$Css$Structure$Compatible,
			lengthOrAutoOrCoverOrContain: $rtfeldman$elm_css$Css$Structure$Compatible,
			lengthOrMinMaxDimension: $rtfeldman$elm_css$Css$Structure$Compatible,
			lengthOrNone: $rtfeldman$elm_css$Css$Structure$Compatible,
			lengthOrNoneOrMinMaxDimension: $rtfeldman$elm_css$Css$Structure$Compatible,
			lengthOrNumber: $rtfeldman$elm_css$Css$Structure$Compatible,
			lengthOrNumberOrAutoOrNoneOrContent: $rtfeldman$elm_css$Css$Structure$Compatible,
			numericValue: numericValue,
			textIndent: $rtfeldman$elm_css$Css$Structure$Compatible,
			unitLabel: unitLabel,
			units: units,
			value: _Utils_ap(
				$elm$core$String$fromFloat(numericValue),
				unitLabel)
		};
	});
var $rtfeldman$elm_css$Css$pct = A2($rtfeldman$elm_css$Css$Internal$lengthConverter, $rtfeldman$elm_css$Css$PercentageUnits, '%');
var $rtfeldman$elm_css$Css$PxUnits = {$: 'PxUnits'};
var $rtfeldman$elm_css$Css$px = A2($rtfeldman$elm_css$Css$Internal$lengthConverter, $rtfeldman$elm_css$Css$PxUnits, 'px');
var $rtfeldman$elm_css$VirtualDom$Styled$makeSnippet = F2(
	function (styles, sequence) {
		var selector = A3($rtfeldman$elm_css$Css$Structure$Selector, sequence, _List_Nil, $elm$core$Maybe$Nothing);
		return $rtfeldman$elm_css$Css$Preprocess$Snippet(
			_List_fromArray(
				[
					$rtfeldman$elm_css$Css$Preprocess$StyleBlockDeclaration(
					A3($rtfeldman$elm_css$Css$Preprocess$StyleBlock, selector, _List_Nil, styles))
				]));
	});
var $rtfeldman$elm_css$Css$Global$selector = F2(
	function (selectorStr, styles) {
		return A2(
			$rtfeldman$elm_css$VirtualDom$Styled$makeSnippet,
			styles,
			A2($rtfeldman$elm_css$Css$Structure$CustomSelector, selectorStr, _List_Nil));
	});
var $author$project$View$globalStyleNode = function (theme) {
	var fullHeight = _List_fromArray(
		[
			$rtfeldman$elm_css$Css$Global$body(
			_List_fromArray(
				[
					A4(
					$rtfeldman$elm_css$Css$margin4,
					$rtfeldman$elm_css$Css$px(0),
					$rtfeldman$elm_css$Css$px(0),
					$rtfeldman$elm_css$Css$px(0),
					$rtfeldman$elm_css$Css$px(0))
				])),
			A2(
			$rtfeldman$elm_css$Css$Global$each,
			_List_fromArray(
				[$rtfeldman$elm_css$Css$Global$body, $rtfeldman$elm_css$Css$Global$html]),
			_List_fromArray(
				[
					$rtfeldman$elm_css$Css$height(
					$rtfeldman$elm_css$Css$pct(100))
				]))
		]);
	var buttonGradient = _List_fromArray(
		[
			A2(
			$rtfeldman$elm_css$Css$Global$selector,
			'@property ' + $author$project$Theme$buttonBackgroundStop1Percentage,
			_List_fromArray(
				[
					A2($rtfeldman$elm_css$Css$property, 'syntax', '\'<percentage>\''),
					A2($rtfeldman$elm_css$Css$property, 'inherits', 'false'),
					A2($rtfeldman$elm_css$Css$property, 'initial-value', '100%')
				])),
			A2(
			$rtfeldman$elm_css$Css$Global$selector,
			'@property ' + $author$project$Theme$buttonBackgroundStop2Percentage,
			_List_fromArray(
				[
					A2($rtfeldman$elm_css$Css$property, 'syntax', '\'<percentage>\''),
					A2($rtfeldman$elm_css$Css$property, 'inherits', 'false'),
					A2($rtfeldman$elm_css$Css$property, 'initial-value', '100%')
				])),
			A2(
			$rtfeldman$elm_css$Css$Global$selector,
			'@property ' + $author$project$Theme$buttonBackgroundStop1Color,
			_List_fromArray(
				[
					A2($rtfeldman$elm_css$Css$property, 'syntax', '\'<color>\''),
					A2($rtfeldman$elm_css$Css$property, 'inherits', 'false'),
					A2(
					$rtfeldman$elm_css$Css$property,
					'initial-value',
					$author$project$Theme$colToString(theme.buttonBackground))
				]))
		]);
	return $rtfeldman$elm_css$Css$Global$global(
		_Utils_ap(fullHeight, buttonGradient));
};
var $author$project$Kaput$theme = function (model) {
	if (model.$ === 'BeforeRunning') {
		var data = model.c;
		return data.theme;
	} else {
		var data = model.d;
		return data.theme;
	}
};
var $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles = F2(
	function (_v0, styles) {
		var isCssStyles = _v0.b;
		var cssTemplate = _v0.c;
		if (isCssStyles) {
			var _v1 = A2($elm$core$Dict$get, cssTemplate, styles);
			if (_v1.$ === 'Just') {
				return styles;
			} else {
				return A3(
					$elm$core$Dict$insert,
					cssTemplate,
					$rtfeldman$elm_css$Hash$fromString(cssTemplate),
					styles);
			}
		} else {
			return styles;
		}
	});
var $elm$virtual_dom$VirtualDom$property = F2(
	function (key, value) {
		return A2(
			_VirtualDom_property,
			_VirtualDom_noInnerHtmlOrFormAction(key),
			_VirtualDom_noJavaScriptOrHtmlJson(value));
	});
var $rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttribute = F2(
	function (styles, _v0) {
		var val = _v0.a;
		var isCssStyles = _v0.b;
		var cssTemplate = _v0.c;
		if (isCssStyles) {
			var _v1 = A2($elm$core$Dict$get, cssTemplate, styles);
			if (_v1.$ === 'Just') {
				var classname = _v1.a;
				return A2(
					$elm$virtual_dom$VirtualDom$property,
					'className',
					$elm$json$Json$Encode$string(classname));
			} else {
				return A2(
					$elm$virtual_dom$VirtualDom$property,
					'className',
					$elm$json$Json$Encode$string('_unstyled'));
			}
		} else {
			return val;
		}
	});
var $rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttributeNS = F2(
	function (styles, _v0) {
		var val = _v0.a;
		var isCssStyles = _v0.b;
		var cssTemplate = _v0.c;
		if (isCssStyles) {
			var _v1 = A2($elm$core$Dict$get, cssTemplate, styles);
			if (_v1.$ === 'Just') {
				var classname = _v1.a;
				return A2($elm$virtual_dom$VirtualDom$attribute, 'class', classname);
			} else {
				return A2($elm$virtual_dom$VirtualDom$attribute, 'class', '_unstyled');
			}
		} else {
			return val;
		}
	});
var $elm$virtual_dom$VirtualDom$keyedNode = function (tag) {
	return _VirtualDom_keyedNode(
		_VirtualDom_noScript(tag));
};
var $elm$virtual_dom$VirtualDom$keyedNodeNS = F2(
	function (namespace, tag) {
		return A2(
			_VirtualDom_keyedNodeNS,
			namespace,
			_VirtualDom_noScript(tag));
	});
var $elm$virtual_dom$VirtualDom$nodeNS = F2(
	function (namespace, tag) {
		return A2(
			_VirtualDom_nodeNS,
			namespace,
			_VirtualDom_noScript(tag));
	});
var $rtfeldman$elm_css$VirtualDom$Styled$accumulateKeyedStyledHtml = F2(
	function (_v6, _v7) {
		var key = _v6.a;
		var html = _v6.b;
		var pairs = _v7.a;
		var styles = _v7.b;
		switch (html.$) {
			case 'Unstyled':
				var vdom = html.a;
				return _Utils_Tuple2(
					A2(
						$elm$core$List$cons,
						_Utils_Tuple2(key, vdom),
						pairs),
					styles);
			case 'Node':
				var elemType = html.a;
				var properties = html.b;
				var children = html.c;
				var combinedStyles = A3($elm$core$List$foldl, $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles, styles, properties);
				var _v9 = A3(
					$elm$core$List$foldl,
					$rtfeldman$elm_css$VirtualDom$Styled$accumulateStyledHtml,
					_Utils_Tuple2(_List_Nil, combinedStyles),
					children);
				var childNodes = _v9.a;
				var finalStyles = _v9.b;
				var vdom = A3(
					$elm$virtual_dom$VirtualDom$node,
					elemType,
					A2(
						$elm$core$List$map,
						$rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttribute(finalStyles),
						properties),
					$elm$core$List$reverse(childNodes));
				return _Utils_Tuple2(
					A2(
						$elm$core$List$cons,
						_Utils_Tuple2(key, vdom),
						pairs),
					finalStyles);
			case 'NodeNS':
				var ns = html.a;
				var elemType = html.b;
				var properties = html.c;
				var children = html.d;
				var combinedStyles = A3($elm$core$List$foldl, $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles, styles, properties);
				var _v10 = A3(
					$elm$core$List$foldl,
					$rtfeldman$elm_css$VirtualDom$Styled$accumulateStyledHtml,
					_Utils_Tuple2(_List_Nil, combinedStyles),
					children);
				var childNodes = _v10.a;
				var finalStyles = _v10.b;
				var vdom = A4(
					$elm$virtual_dom$VirtualDom$nodeNS,
					ns,
					elemType,
					A2(
						$elm$core$List$map,
						$rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttribute(finalStyles),
						properties),
					$elm$core$List$reverse(childNodes));
				return _Utils_Tuple2(
					A2(
						$elm$core$List$cons,
						_Utils_Tuple2(key, vdom),
						pairs),
					finalStyles);
			case 'KeyedNode':
				var elemType = html.a;
				var properties = html.b;
				var children = html.c;
				var combinedStyles = A3($elm$core$List$foldl, $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles, styles, properties);
				var _v11 = A3(
					$elm$core$List$foldl,
					$rtfeldman$elm_css$VirtualDom$Styled$accumulateKeyedStyledHtml,
					_Utils_Tuple2(_List_Nil, combinedStyles),
					children);
				var childNodes = _v11.a;
				var finalStyles = _v11.b;
				var vdom = A3(
					$elm$virtual_dom$VirtualDom$keyedNode,
					elemType,
					A2(
						$elm$core$List$map,
						$rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttribute(finalStyles),
						properties),
					$elm$core$List$reverse(childNodes));
				return _Utils_Tuple2(
					A2(
						$elm$core$List$cons,
						_Utils_Tuple2(key, vdom),
						pairs),
					finalStyles);
			default:
				var ns = html.a;
				var elemType = html.b;
				var properties = html.c;
				var children = html.d;
				var combinedStyles = A3($elm$core$List$foldl, $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles, styles, properties);
				var _v12 = A3(
					$elm$core$List$foldl,
					$rtfeldman$elm_css$VirtualDom$Styled$accumulateKeyedStyledHtml,
					_Utils_Tuple2(_List_Nil, combinedStyles),
					children);
				var childNodes = _v12.a;
				var finalStyles = _v12.b;
				var vdom = A4(
					$elm$virtual_dom$VirtualDom$keyedNodeNS,
					ns,
					elemType,
					A2(
						$elm$core$List$map,
						$rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttribute(finalStyles),
						properties),
					$elm$core$List$reverse(childNodes));
				return _Utils_Tuple2(
					A2(
						$elm$core$List$cons,
						_Utils_Tuple2(key, vdom),
						pairs),
					finalStyles);
		}
	});
var $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyledHtml = F2(
	function (html, _v0) {
		var nodes = _v0.a;
		var styles = _v0.b;
		switch (html.$) {
			case 'Unstyled':
				var vdomNode = html.a;
				return _Utils_Tuple2(
					A2($elm$core$List$cons, vdomNode, nodes),
					styles);
			case 'Node':
				var elemType = html.a;
				var properties = html.b;
				var children = html.c;
				var combinedStyles = A3($elm$core$List$foldl, $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles, styles, properties);
				var _v2 = A3(
					$elm$core$List$foldl,
					$rtfeldman$elm_css$VirtualDom$Styled$accumulateStyledHtml,
					_Utils_Tuple2(_List_Nil, combinedStyles),
					children);
				var childNodes = _v2.a;
				var finalStyles = _v2.b;
				var vdomNode = A3(
					$elm$virtual_dom$VirtualDom$node,
					elemType,
					A2(
						$elm$core$List$map,
						$rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttribute(finalStyles),
						properties),
					$elm$core$List$reverse(childNodes));
				return _Utils_Tuple2(
					A2($elm$core$List$cons, vdomNode, nodes),
					finalStyles);
			case 'NodeNS':
				var ns = html.a;
				var elemType = html.b;
				var properties = html.c;
				var children = html.d;
				var combinedStyles = A3($elm$core$List$foldl, $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles, styles, properties);
				var _v3 = A3(
					$elm$core$List$foldl,
					$rtfeldman$elm_css$VirtualDom$Styled$accumulateStyledHtml,
					_Utils_Tuple2(_List_Nil, combinedStyles),
					children);
				var childNodes = _v3.a;
				var finalStyles = _v3.b;
				var vdomNode = A4(
					$elm$virtual_dom$VirtualDom$nodeNS,
					ns,
					elemType,
					A2(
						$elm$core$List$map,
						$rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttributeNS(finalStyles),
						properties),
					$elm$core$List$reverse(childNodes));
				return _Utils_Tuple2(
					A2($elm$core$List$cons, vdomNode, nodes),
					finalStyles);
			case 'KeyedNode':
				var elemType = html.a;
				var properties = html.b;
				var children = html.c;
				var combinedStyles = A3($elm$core$List$foldl, $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles, styles, properties);
				var _v4 = A3(
					$elm$core$List$foldl,
					$rtfeldman$elm_css$VirtualDom$Styled$accumulateKeyedStyledHtml,
					_Utils_Tuple2(_List_Nil, combinedStyles),
					children);
				var childNodes = _v4.a;
				var finalStyles = _v4.b;
				var vdomNode = A3(
					$elm$virtual_dom$VirtualDom$keyedNode,
					elemType,
					A2(
						$elm$core$List$map,
						$rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttribute(finalStyles),
						properties),
					$elm$core$List$reverse(childNodes));
				return _Utils_Tuple2(
					A2($elm$core$List$cons, vdomNode, nodes),
					finalStyles);
			default:
				var ns = html.a;
				var elemType = html.b;
				var properties = html.c;
				var children = html.d;
				var combinedStyles = A3($elm$core$List$foldl, $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles, styles, properties);
				var _v5 = A3(
					$elm$core$List$foldl,
					$rtfeldman$elm_css$VirtualDom$Styled$accumulateKeyedStyledHtml,
					_Utils_Tuple2(_List_Nil, combinedStyles),
					children);
				var childNodes = _v5.a;
				var finalStyles = _v5.b;
				var vdomNode = A4(
					$elm$virtual_dom$VirtualDom$keyedNodeNS,
					ns,
					elemType,
					A2(
						$elm$core$List$map,
						$rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttributeNS(finalStyles),
						properties),
					$elm$core$List$reverse(childNodes));
				return _Utils_Tuple2(
					A2($elm$core$List$cons, vdomNode, nodes),
					finalStyles);
		}
	});
var $rtfeldman$elm_css$VirtualDom$Styled$classnameStandin = '\u0007';
var $elm$core$String$replace = F3(
	function (before, after, string) {
		return A2(
			$elm$core$String$join,
			after,
			A2($elm$core$String$split, before, string));
	});
var $rtfeldman$elm_css$VirtualDom$Styled$styleToDeclaration = F3(
	function (template, classname, declaration) {
		return declaration + ('\n' + A3($elm$core$String$replace, $rtfeldman$elm_css$VirtualDom$Styled$classnameStandin, classname, template));
	});
var $rtfeldman$elm_css$VirtualDom$Styled$toDeclaration = function (dict) {
	return A3($elm$core$Dict$foldl, $rtfeldman$elm_css$VirtualDom$Styled$styleToDeclaration, '', dict);
};
var $rtfeldman$elm_css$VirtualDom$Styled$toStyleNode = F2(
	function (maybeNonce, styles) {
		return A3(
			$elm$virtual_dom$VirtualDom$node,
			'span',
			_List_fromArray(
				[
					A2($elm$virtual_dom$VirtualDom$attribute, 'style', 'display: none;'),
					A2($elm$virtual_dom$VirtualDom$attribute, 'class', 'elm-css-style-wrapper')
				]),
			_List_fromArray(
				[
					A3(
					$elm$virtual_dom$VirtualDom$node,
					'style',
					function () {
						if (maybeNonce.$ === 'Just') {
							var nonce = maybeNonce.a.a;
							return _List_fromArray(
								[
									A2($elm$virtual_dom$VirtualDom$attribute, 'nonce', nonce)
								]);
						} else {
							return _List_Nil;
						}
					}(),
					$elm$core$List$singleton(
						$elm$virtual_dom$VirtualDom$text(
							$rtfeldman$elm_css$VirtualDom$Styled$toDeclaration(styles))))
				]));
	});
var $rtfeldman$elm_css$VirtualDom$Styled$unstyle = F4(
	function (maybeNonce, elemType, properties, children) {
		var initialStyles = A3($elm$core$List$foldl, $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles, $elm$core$Dict$empty, properties);
		var _v0 = A3(
			$elm$core$List$foldl,
			$rtfeldman$elm_css$VirtualDom$Styled$accumulateStyledHtml,
			_Utils_Tuple2(_List_Nil, initialStyles),
			children);
		var childNodes = _v0.a;
		var styles = _v0.b;
		var styleNode = A2($rtfeldman$elm_css$VirtualDom$Styled$toStyleNode, maybeNonce, styles);
		var unstyledProperties = A2(
			$elm$core$List$map,
			$rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttribute(styles),
			properties);
		return A3(
			$elm$virtual_dom$VirtualDom$node,
			elemType,
			unstyledProperties,
			A2(
				$elm$core$List$cons,
				styleNode,
				$elm$core$List$reverse(childNodes)));
	});
var $rtfeldman$elm_css$VirtualDom$Styled$containsKey = F2(
	function (key, pairs) {
		containsKey:
		while (true) {
			if (!pairs.b) {
				return false;
			} else {
				var _v1 = pairs.a;
				var str = _v1.a;
				var rest = pairs.b;
				if (_Utils_eq(key, str)) {
					return true;
				} else {
					var $temp$key = key,
						$temp$pairs = rest;
					key = $temp$key;
					pairs = $temp$pairs;
					continue containsKey;
				}
			}
		}
	});
var $rtfeldman$elm_css$VirtualDom$Styled$getUnusedKey = F2(
	function (_default, pairs) {
		getUnusedKey:
		while (true) {
			if (!pairs.b) {
				return _default;
			} else {
				var _v1 = pairs.a;
				var firstKey = _v1.a;
				var rest = pairs.b;
				var newKey = '_' + firstKey;
				if (A2($rtfeldman$elm_css$VirtualDom$Styled$containsKey, newKey, rest)) {
					var $temp$default = newKey,
						$temp$pairs = rest;
					_default = $temp$default;
					pairs = $temp$pairs;
					continue getUnusedKey;
				} else {
					return newKey;
				}
			}
		}
	});
var $rtfeldman$elm_css$VirtualDom$Styled$toKeyedStyleNode = F3(
	function (maybeNonce, allStyles, keyedChildNodes) {
		var styleNodeKey = A2($rtfeldman$elm_css$VirtualDom$Styled$getUnusedKey, '_', keyedChildNodes);
		var finalNode = A2($rtfeldman$elm_css$VirtualDom$Styled$toStyleNode, maybeNonce, allStyles);
		return _Utils_Tuple2(styleNodeKey, finalNode);
	});
var $rtfeldman$elm_css$VirtualDom$Styled$unstyleKeyed = F4(
	function (maybeNonce, elemType, properties, keyedChildren) {
		var initialStyles = A3($elm$core$List$foldl, $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles, $elm$core$Dict$empty, properties);
		var _v0 = A3(
			$elm$core$List$foldl,
			$rtfeldman$elm_css$VirtualDom$Styled$accumulateKeyedStyledHtml,
			_Utils_Tuple2(_List_Nil, initialStyles),
			keyedChildren);
		var keyedChildNodes = _v0.a;
		var styles = _v0.b;
		var keyedStyleNode = A3($rtfeldman$elm_css$VirtualDom$Styled$toKeyedStyleNode, maybeNonce, styles, keyedChildNodes);
		var unstyledProperties = A2(
			$elm$core$List$map,
			$rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttribute(styles),
			properties);
		return A3(
			$elm$virtual_dom$VirtualDom$keyedNode,
			elemType,
			unstyledProperties,
			A2(
				$elm$core$List$cons,
				keyedStyleNode,
				$elm$core$List$reverse(keyedChildNodes)));
	});
var $rtfeldman$elm_css$VirtualDom$Styled$unstyleKeyedNS = F5(
	function (maybeNonce, ns, elemType, properties, keyedChildren) {
		var initialStyles = A3($elm$core$List$foldl, $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles, $elm$core$Dict$empty, properties);
		var _v0 = A3(
			$elm$core$List$foldl,
			$rtfeldman$elm_css$VirtualDom$Styled$accumulateKeyedStyledHtml,
			_Utils_Tuple2(_List_Nil, initialStyles),
			keyedChildren);
		var keyedChildNodes = _v0.a;
		var styles = _v0.b;
		var keyedStyleNode = A3($rtfeldman$elm_css$VirtualDom$Styled$toKeyedStyleNode, maybeNonce, styles, keyedChildNodes);
		var unstyledProperties = A2(
			$elm$core$List$map,
			$rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttributeNS(styles),
			properties);
		return A4(
			$elm$virtual_dom$VirtualDom$keyedNodeNS,
			ns,
			elemType,
			unstyledProperties,
			A2(
				$elm$core$List$cons,
				keyedStyleNode,
				$elm$core$List$reverse(keyedChildNodes)));
	});
var $rtfeldman$elm_css$VirtualDom$Styled$unstyleNS = F5(
	function (maybeNonce, ns, elemType, properties, children) {
		var initialStyles = A3($elm$core$List$foldl, $rtfeldman$elm_css$VirtualDom$Styled$accumulateStyles, $elm$core$Dict$empty, properties);
		var _v0 = A3(
			$elm$core$List$foldl,
			$rtfeldman$elm_css$VirtualDom$Styled$accumulateStyledHtml,
			_Utils_Tuple2(_List_Nil, initialStyles),
			children);
		var childNodes = _v0.a;
		var styles = _v0.b;
		var styleNode = A2($rtfeldman$elm_css$VirtualDom$Styled$toStyleNode, maybeNonce, styles);
		var unstyledProperties = A2(
			$elm$core$List$map,
			$rtfeldman$elm_css$VirtualDom$Styled$extractUnstyledAttributeNS(styles),
			properties);
		return A4(
			$elm$virtual_dom$VirtualDom$nodeNS,
			ns,
			elemType,
			unstyledProperties,
			A2(
				$elm$core$List$cons,
				styleNode,
				$elm$core$List$reverse(childNodes)));
	});
var $rtfeldman$elm_css$VirtualDom$Styled$toUnstyled = function (vdom) {
	switch (vdom.$) {
		case 'Unstyled':
			var plainNode = vdom.a;
			return plainNode;
		case 'Node':
			var elemType = vdom.a;
			var properties = vdom.b;
			var children = vdom.c;
			return A4($rtfeldman$elm_css$VirtualDom$Styled$unstyle, $elm$core$Maybe$Nothing, elemType, properties, children);
		case 'NodeNS':
			var ns = vdom.a;
			var elemType = vdom.b;
			var properties = vdom.c;
			var children = vdom.d;
			return A5($rtfeldman$elm_css$VirtualDom$Styled$unstyleNS, $elm$core$Maybe$Nothing, ns, elemType, properties, children);
		case 'KeyedNode':
			var elemType = vdom.a;
			var properties = vdom.b;
			var children = vdom.c;
			return A4($rtfeldman$elm_css$VirtualDom$Styled$unstyleKeyed, $elm$core$Maybe$Nothing, elemType, properties, children);
		default:
			var ns = vdom.a;
			var elemType = vdom.b;
			var properties = vdom.c;
			var children = vdom.d;
			return A5($rtfeldman$elm_css$VirtualDom$Styled$unstyleKeyedNS, $elm$core$Maybe$Nothing, ns, elemType, properties, children);
	}
};
var $rtfeldman$elm_css$Html$Styled$toUnstyled = $rtfeldman$elm_css$VirtualDom$Styled$toUnstyled;
var $author$project$Main$ChangedShutterbugInputValue = function (a) {
	return {$: 'ChangedShutterbugInputValue', a: a};
};
var $author$project$Main$IssuedCameraAssociations = function (a) {
	return {$: 'IssuedCameraAssociations', a: a};
};
var $author$project$Main$StitchingMsg = function (a) {
	return {$: 'StitchingMsg', a: a};
};
var $author$project$Main$SubmittedShutterbug = function (a) {
	return {$: 'SubmittedShutterbug', a: a};
};
var $author$project$Main$ToggledCamera = function (a) {
	return {$: 'ToggledCamera', a: a};
};
var $author$project$Cameras$associated = function (_v0) {
	var registry = _v0.b;
	return $elm$core$List$length(
		$author$project$Stitch$names(registry.associated)) > 0;
};
var $rtfeldman$elm_css$VirtualDom$Styled$Node = F3(
	function (a, b, c) {
		return {$: 'Node', a: a, b: b, c: c};
	});
var $rtfeldman$elm_css$VirtualDom$Styled$node = $rtfeldman$elm_css$VirtualDom$Styled$Node;
var $rtfeldman$elm_css$Html$Styled$node = $rtfeldman$elm_css$VirtualDom$Styled$node;
var $rtfeldman$elm_css$Html$Styled$a = $rtfeldman$elm_css$Html$Styled$node('a');
var $rtfeldman$elm_css$VirtualDom$Styled$Attribute = F3(
	function (a, b, c) {
		return {$: 'Attribute', a: a, b: b, c: c};
	});
var $rtfeldman$elm_css$VirtualDom$Styled$property = F2(
	function (key, value) {
		return A3(
			$rtfeldman$elm_css$VirtualDom$Styled$Attribute,
			A2($elm$virtual_dom$VirtualDom$property, key, value),
			false,
			'');
	});
var $rtfeldman$elm_css$Html$Styled$Attributes$stringProperty = F2(
	function (key, string) {
		return A2(
			$rtfeldman$elm_css$VirtualDom$Styled$property,
			key,
			$elm$json$Json$Encode$string(string));
	});
var $rtfeldman$elm_css$Html$Styled$Attributes$href = function (url) {
	return A2($rtfeldman$elm_css$Html$Styled$Attributes$stringProperty, 'href', url);
};
var $rtfeldman$elm_css$VirtualDom$Styled$attribute = F2(
	function (key, value) {
		return A3(
			$rtfeldman$elm_css$VirtualDom$Styled$Attribute,
			A2($elm$virtual_dom$VirtualDom$attribute, key, value),
			false,
			'');
	});
var $rtfeldman$elm_css$Html$Styled$Attributes$rel = $rtfeldman$elm_css$VirtualDom$Styled$attribute('rel');
var $rtfeldman$elm_css$Html$Styled$Attributes$target = $rtfeldman$elm_css$Html$Styled$Attributes$stringProperty('target');
var $rtfeldman$elm_css$VirtualDom$Styled$text = function (str) {
	return $rtfeldman$elm_css$VirtualDom$Styled$Unstyled(
		$elm$virtual_dom$VirtualDom$text(str));
};
var $rtfeldman$elm_css$Html$Styled$text = $rtfeldman$elm_css$VirtualDom$Styled$text;
var $author$project$My$Html$a = F2(
	function (href, text) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$a,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$href(href),
					$rtfeldman$elm_css$Html$Styled$Attributes$rel('noopener noreferrer'),
					$rtfeldman$elm_css$Html$Styled$Attributes$target('_blank')
				]),
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$text(text)
				]));
	});
var $rtfeldman$elm_css$Css$Preprocess$ApplyStyles = function (a) {
	return {$: 'ApplyStyles', a: a};
};
var $rtfeldman$elm_css$Css$Internal$property = F2(
	function (key, value) {
		return $rtfeldman$elm_css$Css$Preprocess$AppendProperty(
			$rtfeldman$elm_css$Css$Structure$Property(key + (':' + value)));
	});
var $rtfeldman$elm_css$Css$Internal$getOverloadedProperty = F3(
	function (functionName, desiredKey, style) {
		getOverloadedProperty:
		while (true) {
			switch (style.$) {
				case 'AppendProperty':
					var str = style.a.a;
					var key = A2(
						$elm$core$Maybe$withDefault,
						'',
						$elm$core$List$head(
							A2($elm$core$String$split, ':', str)));
					return A2($rtfeldman$elm_css$Css$Internal$property, desiredKey, key);
				case 'ExtendSelector':
					var selector = style.a;
					return A2($rtfeldman$elm_css$Css$Internal$property, desiredKey, 'elm-css-error-cannot-apply-' + (functionName + '-with-inapplicable-Style-for-selector'));
				case 'NestSnippet':
					var combinator = style.a;
					return A2($rtfeldman$elm_css$Css$Internal$property, desiredKey, 'elm-css-error-cannot-apply-' + (functionName + '-with-inapplicable-Style-for-combinator'));
				case 'WithPseudoElement':
					var pseudoElement = style.a;
					return A2($rtfeldman$elm_css$Css$Internal$property, desiredKey, 'elm-css-error-cannot-apply-' + (functionName + '-with-inapplicable-Style-for-pseudo-element setter'));
				case 'WithMedia':
					return A2($rtfeldman$elm_css$Css$Internal$property, desiredKey, 'elm-css-error-cannot-apply-' + (functionName + '-with-inapplicable-Style-for-media-query'));
				case 'WithKeyframes':
					return A2($rtfeldman$elm_css$Css$Internal$property, desiredKey, 'elm-css-error-cannot-apply-' + (functionName + '-with-inapplicable-Style-for-keyframes'));
				default:
					if (!style.a.b) {
						return A2($rtfeldman$elm_css$Css$Internal$property, desiredKey, 'elm-css-error-cannot-apply-' + (functionName + '-with-empty-Style'));
					} else {
						if (!style.a.b.b) {
							var _v1 = style.a;
							var only = _v1.a;
							var $temp$functionName = functionName,
								$temp$desiredKey = desiredKey,
								$temp$style = only;
							functionName = $temp$functionName;
							desiredKey = $temp$desiredKey;
							style = $temp$style;
							continue getOverloadedProperty;
						} else {
							var _v2 = style.a;
							var first = _v2.a;
							var rest = _v2.b;
							var $temp$functionName = functionName,
								$temp$desiredKey = desiredKey,
								$temp$style = $rtfeldman$elm_css$Css$Preprocess$ApplyStyles(rest);
							functionName = $temp$functionName;
							desiredKey = $temp$desiredKey;
							style = $temp$style;
							continue getOverloadedProperty;
						}
					}
			}
		}
	});
var $rtfeldman$elm_css$Css$Internal$IncompatibleUnits = {$: 'IncompatibleUnits'};
var $rtfeldman$elm_css$Css$Internal$lengthForOverloadedProperty = A3($rtfeldman$elm_css$Css$Internal$lengthConverter, $rtfeldman$elm_css$Css$Internal$IncompatibleUnits, '', 0);
var $rtfeldman$elm_css$Css$alignItems = function (fn) {
	return A3(
		$rtfeldman$elm_css$Css$Internal$getOverloadedProperty,
		'alignItems',
		'align-items',
		fn($rtfeldman$elm_css$Css$Internal$lengthForOverloadedProperty));
};
var $rtfeldman$elm_css$Html$Styled$Attributes$autocomplete = function (bool) {
	return A2(
		$rtfeldman$elm_css$Html$Styled$Attributes$stringProperty,
		'autocomplete',
		bool ? 'on' : 'off');
};
var $rtfeldman$elm_css$Css$backgroundColor = function (c) {
	return A2($rtfeldman$elm_css$Css$property, 'background-color', c.value);
};
var $rtfeldman$elm_css$Css$bold = {fontWeight: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'bold'};
var $rtfeldman$elm_css$Css$prop3 = F4(
	function (key, argA, argB, argC) {
		return A2($rtfeldman$elm_css$Css$property, key, argA.value + (' ' + (argB.value + (' ' + argC.value))));
	});
var $rtfeldman$elm_css$Css$border3 = $rtfeldman$elm_css$Css$prop3('border');
var $rtfeldman$elm_css$Css$borderColor = function (c) {
	return A2($rtfeldman$elm_css$Css$property, 'border-color', c.value);
};
var $rtfeldman$elm_css$Css$borderRadius = $rtfeldman$elm_css$Css$prop1('border-radius');
var $rtfeldman$elm_css$Css$borderStyle = $rtfeldman$elm_css$Css$prop1('border-style');
var $rtfeldman$elm_css$Css$borderWidth = $rtfeldman$elm_css$Css$prop1('border-width');
var $author$project$Theme$Colour = function (a) {
	return {$: 'Colour', a: a};
};
var $author$project$Theme$CustomProperty = function (a) {
	return {$: 'CustomProperty', a: a};
};
var $rtfeldman$elm_css$Css$prop5 = F6(
	function (key, argA, argB, argC, argD, argE) {
		return A2($rtfeldman$elm_css$Css$property, key, argA.value + (' ' + (argB.value + (' ' + (argC.value + (' ' + (argD.value + (' ' + argE.value))))))));
	});
var $rtfeldman$elm_css$Css$boxShadow5 = $rtfeldman$elm_css$Css$prop5('box-shadow');
var $rtfeldman$elm_css$Css$color = function (c) {
	return A2($rtfeldman$elm_css$Css$property, 'color', c.value);
};
var $rtfeldman$elm_css$Css$Structure$Descendant = {$: 'Descendant'};
var $rtfeldman$elm_css$Css$Preprocess$NestSnippet = F2(
	function (a, b) {
		return {$: 'NestSnippet', a: a, b: b};
	});
var $rtfeldman$elm_css$Css$Global$descendants = $rtfeldman$elm_css$Css$Preprocess$NestSnippet($rtfeldman$elm_css$Css$Structure$Descendant);
var $rtfeldman$elm_css$Css$EmUnits = {$: 'EmUnits'};
var $rtfeldman$elm_css$Css$em = A2($rtfeldman$elm_css$Css$Internal$lengthConverter, $rtfeldman$elm_css$Css$EmUnits, 'em');
var $author$project$Theme$expanderIconClass = 'mechane-elm-expander-icon';
var $rtfeldman$elm_css$Css$Preprocess$ExtendSelector = F2(
	function (a, b) {
		return {$: 'ExtendSelector', a: a, b: b};
	});
var $rtfeldman$elm_css$Css$Structure$PseudoClassSelector = function (a) {
	return {$: 'PseudoClassSelector', a: a};
};
var $rtfeldman$elm_css$Css$pseudoClass = function (_class) {
	return $rtfeldman$elm_css$Css$Preprocess$ExtendSelector(
		$rtfeldman$elm_css$Css$Structure$PseudoClassSelector(_class));
};
var $rtfeldman$elm_css$Css$hover = $rtfeldman$elm_css$Css$pseudoClass('hover');
var $rtfeldman$elm_css$Css$none = {backgroundImage: $rtfeldman$elm_css$Css$Structure$Compatible, blockAxisOverflow: $rtfeldman$elm_css$Css$Structure$Compatible, borderStyle: $rtfeldman$elm_css$Css$Structure$Compatible, cursor: $rtfeldman$elm_css$Css$Structure$Compatible, display: $rtfeldman$elm_css$Css$Structure$Compatible, hoverCapability: $rtfeldman$elm_css$Css$Structure$Compatible, inlineAxisOverflow: $rtfeldman$elm_css$Css$Structure$Compatible, keyframes: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrNone: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrNoneOrMinMaxDimension: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrNumberOrAutoOrNoneOrContent: $rtfeldman$elm_css$Css$Structure$Compatible, listStyleType: $rtfeldman$elm_css$Css$Structure$Compatible, listStyleTypeOrPositionOrImage: $rtfeldman$elm_css$Css$Structure$Compatible, none: $rtfeldman$elm_css$Css$Structure$Compatible, outline: $rtfeldman$elm_css$Css$Structure$Compatible, pointerDevice: $rtfeldman$elm_css$Css$Structure$Compatible, pointerEvents: $rtfeldman$elm_css$Css$Structure$Compatible, resize: $rtfeldman$elm_css$Css$Structure$Compatible, scriptingSupport: $rtfeldman$elm_css$Css$Structure$Compatible, textDecorationLine: $rtfeldman$elm_css$Css$Structure$Compatible, textTransform: $rtfeldman$elm_css$Css$Structure$Compatible, touchAction: $rtfeldman$elm_css$Css$Structure$Compatible, transform: $rtfeldman$elm_css$Css$Structure$Compatible, updateFrequency: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'none'};
var $rtfeldman$elm_css$Css$UnitlessFloat = {$: 'UnitlessFloat'};
var $rtfeldman$elm_css$Css$num = function (val) {
	return {
		lengthOrNumber: $rtfeldman$elm_css$Css$Structure$Compatible,
		lengthOrNumberOrAutoOrNoneOrContent: $rtfeldman$elm_css$Css$Structure$Compatible,
		number: $rtfeldman$elm_css$Css$Structure$Compatible,
		numberOrInfinite: $rtfeldman$elm_css$Css$Structure$Compatible,
		numericValue: val,
		unitLabel: '',
		units: $rtfeldman$elm_css$Css$UnitlessFloat,
		value: $elm$core$String$fromFloat(val)
	};
};
var $rtfeldman$elm_css$Css$opacity = $rtfeldman$elm_css$Css$prop1('opacity');
var $rtfeldman$elm_css$Css$Transitions$Opacity = {$: 'Opacity'};
var $rtfeldman$elm_css$Css$Transitions$Transition = function (a) {
	return {$: 'Transition', a: a};
};
var $rtfeldman$elm_css$Css$Transitions$durationTransition = F2(
	function (animation, duration) {
		return $rtfeldman$elm_css$Css$Transitions$Transition(
			{animation: animation, delay: $elm$core$Maybe$Nothing, duration: duration, timing: $elm$core$Maybe$Nothing});
	});
var $rtfeldman$elm_css$Css$Transitions$opacity = $rtfeldman$elm_css$Css$Transitions$durationTransition($rtfeldman$elm_css$Css$Transitions$Opacity);
var $rtfeldman$elm_css$Css$outline = $rtfeldman$elm_css$Css$prop1('outline');
var $rtfeldman$elm_css$Css$padding = $rtfeldman$elm_css$Css$prop1('padding');
var $author$project$Theme$popUpDuration = 750;
var $author$project$Theme$var = function (name) {
	return 'var(' + (name + ')');
};
var $author$project$Theme$radialGradient = function (colStops) {
	return A2(
		$rtfeldman$elm_css$Css$property,
		'background',
		function (stops) {
			return 'radial-gradient(' + (stops + ')');
		}(
			A2(
				$elm$core$String$join,
				',',
				A2(
					$elm$core$List$map,
					function (_v0) {
						var c = _v0.a;
						var s = _v0.b;
						return function () {
							if (c.$ === 'Colour') {
								var col = c.a;
								return $author$project$Theme$colToString(col);
							} else {
								var col = c.a;
								return $author$project$Theme$var(col);
							}
						}() + (' ' + $author$project$Theme$var(s));
					},
					colStops))));
};
var $rtfeldman$elm_css$Css$solid = {borderStyle: $rtfeldman$elm_css$Css$Structure$Compatible, textDecorationStyle: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'solid'};
var $rtfeldman$elm_css$Css$Transitions$Transform = {$: 'Transform'};
var $rtfeldman$elm_css$Css$Transitions$transform = $rtfeldman$elm_css$Css$Transitions$durationTransition($rtfeldman$elm_css$Css$Transitions$Transform);
var $rtfeldman$elm_css$Css$Transitions$propToString = function (prop) {
	switch (prop.$) {
		case 'Background':
			return 'background';
		case 'BackgroundColor':
			return 'background-color';
		case 'BackgroundPosition':
			return 'background-position';
		case 'BackgroundSize':
			return 'background-size';
		case 'Border':
			return 'border';
		case 'BorderBottom':
			return 'border-bottom';
		case 'BorderBottomColor':
			return 'border-bottom-color';
		case 'BorderBottomLeftRadius':
			return 'border-bottom-left-radius';
		case 'BorderBottomRightRadius':
			return 'border-bottom-right-radius';
		case 'BorderBottomWidth':
			return 'border-bottom-width';
		case 'BorderColor':
			return 'border-color';
		case 'BorderLeft':
			return 'border-left';
		case 'BorderLeftColor':
			return 'border-left-color';
		case 'BorderLeftWidth':
			return 'border-left-width';
		case 'BorderRadius':
			return 'border-radius';
		case 'BorderRight':
			return 'border-right';
		case 'BorderRightColor':
			return 'border-right-color';
		case 'BorderRightWidth':
			return 'border-right-width';
		case 'BorderTop':
			return 'border-top';
		case 'BorderTopColor':
			return 'border-top-color';
		case 'BorderTopLeftRadius':
			return 'border-top-left-radius';
		case 'BorderTopRightRadius':
			return 'border-top-right-radius';
		case 'BorderTopWidth':
			return 'border-top-width';
		case 'BorderWidth':
			return 'border-width';
		case 'Bottom':
			return 'bottom';
		case 'BoxShadow':
			return 'box-shadow';
		case 'CaretColor':
			return 'caret-color';
		case 'Clip':
			return 'clip';
		case 'ClipPath':
			return 'clip-path';
		case 'Color':
			return 'color';
		case 'ColumnCount':
			return 'column-count';
		case 'ColumnGap':
			return 'column-gap';
		case 'ColumnRule':
			return 'column-rule';
		case 'ColumnRuleColor':
			return 'column-rule-color';
		case 'ColumnRuleWidth':
			return 'column-rule-width';
		case 'ColumnWidth':
			return 'column-width';
		case 'Columns':
			return 'columns';
		case 'Filter':
			return 'filter';
		case 'Flex':
			return 'flex';
		case 'FlexBasis':
			return 'flex-basis';
		case 'FlexGrow':
			return 'flex-grow';
		case 'FlexShrink':
			return 'flex-shrink';
		case 'Font':
			return 'font';
		case 'FontSize':
			return 'font-size';
		case 'FontSizeAdjust':
			return 'font-size-adjust';
		case 'FontStretch':
			return 'font-stretch';
		case 'FontVariationSettings':
			return 'font-variation-settings';
		case 'FontWeight':
			return 'font-weight';
		case 'GridColumnGap':
			return 'grid-column-gap';
		case 'GridGap':
			return 'grid-gap';
		case 'GridRowGap':
			return 'grid-row-gap';
		case 'Height':
			return 'height';
		case 'Left':
			return 'left';
		case 'LetterSpacing':
			return 'letter-spacing';
		case 'LineHeight':
			return 'line-height';
		case 'Margin':
			return 'margin';
		case 'MarginBottom':
			return 'margin-bottom';
		case 'MarginLeft':
			return 'margin-left';
		case 'MarginRight':
			return 'margin-right';
		case 'MarginTop':
			return 'margin-top';
		case 'Mask':
			return 'mask';
		case 'MaskPosition':
			return 'mask-position';
		case 'MaskSize':
			return 'mask-size';
		case 'MaxHeight':
			return 'max-height';
		case 'MaxWidth':
			return 'max-width';
		case 'MinHeight':
			return 'min-height';
		case 'MinWidth':
			return 'min-width';
		case 'ObjectPosition':
			return 'object-position';
		case 'Offset':
			return 'offset';
		case 'OffsetAnchor':
			return 'offset-anchor';
		case 'OffsetDistance':
			return 'offset-distance';
		case 'OffsetPath':
			return 'offset-path';
		case 'OffsetRotate':
			return 'offset-rotate';
		case 'Opacity':
			return 'opacity';
		case 'Order':
			return 'order';
		case 'Outline':
			return 'outline';
		case 'OutlineColor':
			return 'outline-color';
		case 'OutlineOffset':
			return 'outline-offset';
		case 'OutlineWidth':
			return 'outline-width';
		case 'Padding':
			return 'padding';
		case 'PaddingBottom':
			return 'padding-bottom';
		case 'PaddingLeft':
			return 'padding-left';
		case 'PaddingRight':
			return 'padding-right';
		case 'PaddingTop':
			return 'padding-top';
		case 'Right':
			return 'right';
		case 'TabSize':
			return 'tab-size';
		case 'TextIndent':
			return 'text-indent';
		case 'TextShadow':
			return 'text-shadow';
		case 'Top':
			return 'top';
		case 'Transform':
			return 'transform';
		case 'TransformOrigin':
			return 'transform-origin';
		case 'VerticalAlign':
			return 'vertical-align';
		case 'Visibility':
			return 'visibility';
		case 'Width':
			return 'width';
		case 'WordSpacing':
			return 'word-spacing';
		default:
			return 'z-index';
	}
};
var $rtfeldman$elm_css$Css$Transitions$timeToString = function (time) {
	return $elm$core$String$fromFloat(time) + 'ms';
};
var $rtfeldman$elm_css$Css$Transitions$timingFunctionToString = function (tf) {
	switch (tf.$) {
		case 'Ease':
			return 'ease';
		case 'Linear':
			return 'linear';
		case 'EaseIn':
			return 'ease-in';
		case 'EaseOut':
			return 'ease-out';
		case 'EaseInOut':
			return 'ease-in-out';
		case 'StepStart':
			return 'step-start';
		case 'StepEnd':
			return 'step-end';
		default:
			var _float = tf.a;
			var float2 = tf.b;
			var float3 = tf.c;
			var float4 = tf.d;
			return 'cubic-bezier(' + ($elm$core$String$fromFloat(_float) + (' , ' + ($elm$core$String$fromFloat(float2) + (' , ' + ($elm$core$String$fromFloat(float3) + (' , ' + ($elm$core$String$fromFloat(float4) + ')')))))));
	}
};
var $rtfeldman$elm_css$Css$Transitions$transition = function (options) {
	var v = A3(
		$elm$core$String$slice,
		0,
		-1,
		A3(
			$elm$core$List$foldl,
			F2(
				function (_v0, s) {
					var animation = _v0.a.animation;
					var duration = _v0.a.duration;
					var delay = _v0.a.delay;
					var timing = _v0.a.timing;
					return s + ($rtfeldman$elm_css$Css$Transitions$propToString(animation) + (' ' + ($rtfeldman$elm_css$Css$Transitions$timeToString(duration) + (' ' + (A2(
						$elm$core$Maybe$withDefault,
						'',
						A2($elm$core$Maybe$map, $rtfeldman$elm_css$Css$Transitions$timeToString, delay)) + (' ' + (A2(
						$elm$core$Maybe$withDefault,
						'',
						A2($elm$core$Maybe$map, $rtfeldman$elm_css$Css$Transitions$timingFunctionToString, timing)) + ',')))))));
				}),
			'',
			options));
	return A2($rtfeldman$elm_css$Css$property, 'transition', v);
};
var $author$project$Theme$transition = function (transitions) {
	return A2(
		$rtfeldman$elm_css$Css$property,
		'transition',
		A2(
			$elm$core$String$join,
			',',
			A2(
				$elm$core$List$map,
				function (_v0) {
					var p = _v0.a;
					var d = _v0.b;
					return p + (' ' + ($elm$core$String$fromInt(d) + 'ms'));
				},
				transitions)));
};
var $author$project$Theme$button = function (theme) {
	var transitions = $author$project$Theme$transition(
		_List_fromArray(
			[
				_Utils_Tuple2('border', 100),
				_Utils_Tuple2('box-shadow', 300),
				_Utils_Tuple2($author$project$Theme$buttonBackgroundStop1Percentage, 300),
				_Utils_Tuple2($author$project$Theme$buttonBackgroundStop2Percentage, 300),
				_Utils_Tuple2($author$project$Theme$buttonBackgroundStop1Color, 300)
			]));
	var hover = _List_fromArray(
		[
			A3(
			$rtfeldman$elm_css$Css$border3,
			$rtfeldman$elm_css$Css$em(theme.buttonBorder),
			$rtfeldman$elm_css$Css$solid,
			theme.primaryLight),
			A5(
			$rtfeldman$elm_css$Css$boxShadow5,
			$rtfeldman$elm_css$Css$em(0),
			$rtfeldman$elm_css$Css$em(0),
			$rtfeldman$elm_css$Css$em(0.7),
			$rtfeldman$elm_css$Css$em(0.2),
			theme.primaryGlow),
			A2($rtfeldman$elm_css$Css$property, $author$project$Theme$buttonBackgroundStop1Percentage, '65%'),
			A2($rtfeldman$elm_css$Css$property, $author$project$Theme$buttonBackgroundStop2Percentage, '75%')
		]);
	var backgroundGradient = $author$project$Theme$radialGradient(
		_List_fromArray(
			[
				_Utils_Tuple2(
				$author$project$Theme$CustomProperty($author$project$Theme$buttonBackgroundStop1Color),
				$author$project$Theme$buttonBackgroundStop1Percentage),
				_Utils_Tuple2(
				$author$project$Theme$Colour(theme.primaryLight),
				$author$project$Theme$buttonBackgroundStop2Percentage)
			]));
	return {
		inactive: _List_fromArray(
			[
				$rtfeldman$elm_css$Css$backgroundColor(theme.buttonBackground),
				A3(
				$rtfeldman$elm_css$Css$border3,
				$rtfeldman$elm_css$Css$em(theme.buttonBorder),
				$rtfeldman$elm_css$Css$solid,
				theme.textBigPassive),
				$rtfeldman$elm_css$Css$borderRadius(
				$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
				$rtfeldman$elm_css$Css$color(theme.textBigPassive),
				$rtfeldman$elm_css$Css$padding(
				$rtfeldman$elm_css$Css$em(theme.buttonPadding))
			]),
		standard: _List_fromArray(
			[
				backgroundGradient,
				A3(
				$rtfeldman$elm_css$Css$border3,
				$rtfeldman$elm_css$Css$em(theme.buttonBorder),
				$rtfeldman$elm_css$Css$solid,
				theme.primary),
				$rtfeldman$elm_css$Css$borderRadius(
				$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
				$rtfeldman$elm_css$Css$color(theme.primary),
				$rtfeldman$elm_css$Css$outline($rtfeldman$elm_css$Css$none),
				$rtfeldman$elm_css$Css$padding(
				$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
				transitions,
				$rtfeldman$elm_css$Css$hover(
				A2(
					$elm$core$List$cons,
					$rtfeldman$elm_css$Css$Global$descendants(
						_List_fromArray(
							[
								A2(
								$rtfeldman$elm_css$Css$Global$typeSelector,
								'.' + $author$project$Theme$expanderIconClass,
								_List_fromArray(
									[
										$rtfeldman$elm_css$Css$opacity(
										$rtfeldman$elm_css$Css$num(1)),
										$rtfeldman$elm_css$Css$Transitions$transition(
										_List_fromArray(
											[
												$rtfeldman$elm_css$Css$Transitions$transform($author$project$Theme$popUpDuration),
												$rtfeldman$elm_css$Css$Transitions$opacity(400)
											]))
									]))
							])),
					hover))
			]),
		unselected: _List_fromArray(
			[
				backgroundGradient,
				A3(
				$rtfeldman$elm_css$Css$border3,
				$rtfeldman$elm_css$Css$em(theme.buttonBorder),
				$rtfeldman$elm_css$Css$solid,
				theme.primary),
				$rtfeldman$elm_css$Css$borderRadius(
				$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
				$rtfeldman$elm_css$Css$color(theme.textBigPassive),
				$rtfeldman$elm_css$Css$padding(
				$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
				transitions,
				$rtfeldman$elm_css$Css$hover(hover)
			])
	};
};
var $rtfeldman$elm_css$Html$Styled$Attributes$class = $rtfeldman$elm_css$Html$Styled$Attributes$stringProperty('className');
var $rtfeldman$elm_css$Css$Structure$ClassSelector = function (a) {
	return {$: 'ClassSelector', a: a};
};
var $rtfeldman$elm_css$VirtualDom$Styled$templateSelector = $rtfeldman$elm_css$Css$Structure$UniversalSelectorSequence(
	_List_fromArray(
		[
			$rtfeldman$elm_css$Css$Structure$ClassSelector($rtfeldman$elm_css$VirtualDom$Styled$classnameStandin)
		]));
var $rtfeldman$elm_css$VirtualDom$Styled$getCssTemplate = function (styles) {
	if (!styles.b) {
		return '';
	} else {
		var otherwise = styles;
		return $rtfeldman$elm_css$Css$Preprocess$Resolve$compile(
			$rtfeldman$elm_css$Css$Preprocess$stylesheet(
				_List_fromArray(
					[
						A2($rtfeldman$elm_css$VirtualDom$Styled$makeSnippet, styles, $rtfeldman$elm_css$VirtualDom$Styled$templateSelector)
					])));
	}
};
var $rtfeldman$elm_css$Html$Styled$Internal$css = function (styles) {
	var cssTemplate = $rtfeldman$elm_css$VirtualDom$Styled$getCssTemplate(styles);
	var classProperty = A2($elm$virtual_dom$VirtualDom$attribute, '', '');
	return A3($rtfeldman$elm_css$VirtualDom$Styled$Attribute, classProperty, true, cssTemplate);
};
var $rtfeldman$elm_css$Html$Styled$Attributes$css = $rtfeldman$elm_css$Html$Styled$Internal$css;
var $author$project$View$defaultIconStyle = 'material-icons-round';
var $rtfeldman$elm_css$Css$fontSize = $rtfeldman$elm_css$Css$prop1('font-size');
var $rtfeldman$elm_css$Css$middle = $rtfeldman$elm_css$Css$prop1('middle');
var $rtfeldman$elm_css$Html$Styled$span = $rtfeldman$elm_css$Html$Styled$node('span');
var $rtfeldman$elm_css$Css$verticalAlign = function (fn) {
	return A3(
		$rtfeldman$elm_css$Css$Internal$getOverloadedProperty,
		'verticalAlign',
		'vertical-align',
		fn($rtfeldman$elm_css$Css$Internal$lengthForOverloadedProperty));
};
var $author$project$View$buttonIcon = F2(
	function (theme, name) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$span,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$class($author$project$View$defaultIconStyle),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$fontSize(
							$rtfeldman$elm_css$Css$em(theme.buttonIconFontSize)),
							A2($rtfeldman$elm_css$Css$property, 'user-select', 'none'),
							$rtfeldman$elm_css$Css$verticalAlign($rtfeldman$elm_css$Css$middle)
						]))
				]),
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$text(name)
				]));
	});
var $rtfeldman$elm_css$Css$center = $rtfeldman$elm_css$Css$prop1('center');
var $rtfeldman$elm_css$Css$ChUnits = {$: 'ChUnits'};
var $rtfeldman$elm_css$Css$ch = A2($rtfeldman$elm_css$Css$Internal$lengthConverter, $rtfeldman$elm_css$Css$ChUnits, 'ch');
var $rtfeldman$elm_css$Css$row = {flexDirection: $rtfeldman$elm_css$Css$Structure$Compatible, flexDirectionOrWrap: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'row'};
var $rtfeldman$elm_css$Css$column = _Utils_update(
	$rtfeldman$elm_css$Css$row,
	{value: 'column'});
var $author$project$Theme$consoleGreen = A3($rtfeldman$elm_css$Css$rgb, 74, 246, 38);
var $elm$regex$Regex$Match = F4(
	function (match, index, number, submatches) {
		return {index: index, match: match, number: number, submatches: submatches};
	});
var $elm$regex$Regex$contains = _Regex_contains;
var $rtfeldman$elm_css$Css$display = $rtfeldman$elm_css$Css$prop1('display');
var $rtfeldman$elm_css$Css$displayFlex = A2($rtfeldman$elm_css$Css$property, 'display', 'flex');
var $rtfeldman$elm_css$Html$Styled$div = $rtfeldman$elm_css$Html$Styled$node('div');
var $author$project$Theme$dodgerBlue = A3($rtfeldman$elm_css$Css$rgb, 30, 144, 255);
var $rtfeldman$elm_css$Css$double = {borderStyle: $rtfeldman$elm_css$Css$Structure$Compatible, textDecorationStyle: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'double'};
var $rtfeldman$elm_css$Css$flexDirection = $rtfeldman$elm_css$Css$prop1('flex-direction');
var $rtfeldman$elm_css$Css$fontWeight = function (_v0) {
	var value = _v0.value;
	return A2($rtfeldman$elm_css$Css$property, 'font-weight', value);
};
var $elm$regex$Regex$fromStringWith = _Regex_fromStringWith;
var $elm$regex$Regex$fromString = function (string) {
	return A2(
		$elm$regex$Regex$fromStringWith,
		{caseInsensitive: false, multiline: false},
		string);
};
var $author$project$Theme$gold = A3($rtfeldman$elm_css$Css$rgb, 255, 215, 0);
var $rtfeldman$elm_css$Css$inlineBlock = {display: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'inline-block'};
var $rtfeldman$elm_css$Html$Styled$input = $rtfeldman$elm_css$Html$Styled$node('input');
var $rtfeldman$elm_css$Css$inset = {borderStyle: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'inset'};
var $rtfeldman$elm_css$Html$Styled$li = $rtfeldman$elm_css$Html$Styled$node('li');
var $rtfeldman$elm_css$Css$margin = $rtfeldman$elm_css$Css$prop1('margin');
var $rtfeldman$elm_css$Css$marginBottom = $rtfeldman$elm_css$Css$prop1('margin-bottom');
var $rtfeldman$elm_css$Css$marginLeft = $rtfeldman$elm_css$Css$prop1('margin-left');
var $rtfeldman$elm_css$Css$marginRight = $rtfeldman$elm_css$Css$prop1('margin-right');
var $rtfeldman$elm_css$Css$marginTop = $rtfeldman$elm_css$Css$prop1('margin-top');
var $rtfeldman$elm_css$Html$Styled$Attributes$maxlength = function (n) {
	return A2(
		$rtfeldman$elm_css$VirtualDom$Styled$attribute,
		'maxlength',
		$elm$core$String$fromInt(n));
};
var $rtfeldman$elm_css$Html$Styled$Attributes$minlength = function (n) {
	return A2(
		$rtfeldman$elm_css$VirtualDom$Styled$attribute,
		'minLength',
		$elm$core$String$fromInt(n));
};
var $elm$regex$Regex$never = _Regex_never;
var $elm$virtual_dom$VirtualDom$Normal = function (a) {
	return {$: 'Normal', a: a};
};
var $elm$virtual_dom$VirtualDom$on = _VirtualDom_on;
var $rtfeldman$elm_css$VirtualDom$Styled$on = F2(
	function (eventName, handler) {
		return A3(
			$rtfeldman$elm_css$VirtualDom$Styled$Attribute,
			A2($elm$virtual_dom$VirtualDom$on, eventName, handler),
			false,
			'');
	});
var $rtfeldman$elm_css$Html$Styled$Events$on = F2(
	function (event, decoder) {
		return A2(
			$rtfeldman$elm_css$VirtualDom$Styled$on,
			event,
			$elm$virtual_dom$VirtualDom$Normal(decoder));
	});
var $elm$virtual_dom$VirtualDom$Custom = function (a) {
	return {$: 'Custom', a: a};
};
var $rtfeldman$elm_css$Html$Styled$Events$custom = F2(
	function (event, decoder) {
		return A2(
			$rtfeldman$elm_css$VirtualDom$Styled$on,
			event,
			$elm$virtual_dom$VirtualDom$Custom(decoder));
	});
var $author$project$My$Html$onEventPreventDefault = F2(
	function (event, msg) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$Events$custom,
			event,
			$elm$json$Json$Decode$succeed(
				{message: msg, preventDefault: true, stopPropagation: false}));
	});
var $author$project$My$Html$onClickPreventDefault = function (msg) {
	return A2($author$project$My$Html$onEventPreventDefault, 'click', msg);
};
var $rtfeldman$elm_css$Html$Styled$Events$alwaysStop = function (x) {
	return _Utils_Tuple2(x, true);
};
var $elm$virtual_dom$VirtualDom$MayStopPropagation = function (a) {
	return {$: 'MayStopPropagation', a: a};
};
var $rtfeldman$elm_css$Html$Styled$Events$stopPropagationOn = F2(
	function (event, decoder) {
		return A2(
			$rtfeldman$elm_css$VirtualDom$Styled$on,
			event,
			$elm$virtual_dom$VirtualDom$MayStopPropagation(decoder));
	});
var $rtfeldman$elm_css$Html$Styled$Events$targetValue = A2(
	$elm$json$Json$Decode$at,
	_List_fromArray(
		['target', 'value']),
	$elm$json$Json$Decode$string);
var $rtfeldman$elm_css$Html$Styled$Events$onInput = function (tagger) {
	return A2(
		$rtfeldman$elm_css$Html$Styled$Events$stopPropagationOn,
		'input',
		A2(
			$elm$json$Json$Decode$map,
			$rtfeldman$elm_css$Html$Styled$Events$alwaysStop,
			A2($elm$json$Json$Decode$map, tagger, $rtfeldman$elm_css$Html$Styled$Events$targetValue)));
};
var $rtfeldman$elm_css$Css$outlineColor = function (c) {
	return A2($rtfeldman$elm_css$Css$property, 'outline-color', c.value);
};
var $rtfeldman$elm_css$Css$outlineStyle = $rtfeldman$elm_css$Css$prop1('outline-style');
var $rtfeldman$elm_css$Css$outlineWidth = $rtfeldman$elm_css$Css$prop1('outline-width');
var $rtfeldman$elm_css$Html$Styled$p = $rtfeldman$elm_css$Html$Styled$node('p');
var $rtfeldman$elm_css$Html$Styled$Attributes$pattern = $rtfeldman$elm_css$Html$Styled$Attributes$stringProperty('pattern');
var $rtfeldman$elm_css$Html$Styled$Attributes$placeholder = $rtfeldman$elm_css$Html$Styled$Attributes$stringProperty('placeholder');
var $author$project$Theme$red = A3($rtfeldman$elm_css$Css$rgb, 255, 0, 0);
var $author$project$Shutterbug$regex = function () {
	var _v0 = $elm$regex$Regex$fromString('^[\\w-]{43}=$');
	if (_v0.$ === 'Just') {
		var r = _v0.a;
		return r;
	} else {
		return $elm$regex$Regex$never;
	}
}();
var $rtfeldman$elm_css$Html$Styled$Attributes$boolProperty = F2(
	function (key, bool) {
		return A2(
			$rtfeldman$elm_css$VirtualDom$Styled$property,
			key,
			$elm$json$Json$Encode$bool(bool));
	});
var $rtfeldman$elm_css$Html$Styled$Attributes$spellcheck = $rtfeldman$elm_css$Html$Styled$Attributes$boolProperty('spellcheck');
var $rtfeldman$elm_css$Css$animationDuration = function (arg) {
	return A2($rtfeldman$elm_css$Css$prop1, 'animation-duration', arg);
};
var $rtfeldman$elm_css$Css$animationIterationCount = function (arg) {
	return A2($rtfeldman$elm_css$Css$prop1, 'animation-iteration-count', arg);
};
var $rtfeldman$elm_css$Css$Preprocess$WithKeyframes = function (a) {
	return {$: 'WithKeyframes', a: a};
};
var $rtfeldman$elm_css$Css$animationName = function (arg) {
	return ((arg.value === 'none') || ((arg.value === 'inherit') || ((arg.value === 'unset') || (arg.value === 'initial')))) ? A2($rtfeldman$elm_css$Css$prop1, 'animation-name', arg) : $rtfeldman$elm_css$Css$Preprocess$WithKeyframes(arg.value);
};
var $rtfeldman$elm_css$Css$infinite = {numberOrInfinite: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'infinite'};
var $rtfeldman$elm_css$Css$Internal$printKeyframeSelector = function (_v0) {
	var percentage = _v0.a;
	var properties = _v0.b;
	var propertiesStr = A3(
		$rtfeldman$elm_css$Css$String$mapJoin,
		function (_v1) {
			var prop = _v1.a;
			return prop + ';';
		},
		'',
		properties);
	var percentageStr = $elm$core$String$fromInt(percentage) + '%';
	return percentageStr + ('{' + (propertiesStr + '}'));
};
var $rtfeldman$elm_css$Css$Internal$compileKeyframes = function (tuples) {
	return A3($rtfeldman$elm_css$Css$String$mapJoin, $rtfeldman$elm_css$Css$Internal$printKeyframeSelector, '', tuples);
};
var $rtfeldman$elm_css$Css$Animations$keyframes = function (tuples) {
	return $elm$core$List$isEmpty(tuples) ? {keyframes: $rtfeldman$elm_css$Css$Structure$Compatible, none: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'none'} : {
		keyframes: $rtfeldman$elm_css$Css$Structure$Compatible,
		none: $rtfeldman$elm_css$Css$Structure$Compatible,
		value: $rtfeldman$elm_css$Css$Internal$compileKeyframes(tuples)
	};
};
var $rtfeldman$elm_css$Css$ms = function (amount) {
	return {
		duration: $rtfeldman$elm_css$Css$Structure$Compatible,
		value: $elm$core$String$fromFloat(amount) + 'ms'
	};
};
var $rtfeldman$elm_css$Css$Internal$Property = function (a) {
	return {$: 'Property', a: a};
};
var $rtfeldman$elm_css$Css$Animations$property = F2(
	function (key, value) {
		return $rtfeldman$elm_css$Css$Internal$Property(key + (':' + value));
	});
var $author$project$Theme$flashingReadyToSubmitStyles = function (theme) {
	var property = $author$project$Theme$buttonBackgroundStop1Color;
	var buttonBackground = $author$project$Theme$colToString(theme.buttonBackground);
	return _List_fromArray(
		[
			$rtfeldman$elm_css$Css$animationName(
			$rtfeldman$elm_css$Css$Animations$keyframes(
				_List_fromArray(
					[
						_Utils_Tuple2(
						0,
						_List_fromArray(
							[
								A2($rtfeldman$elm_css$Css$Animations$property, property, buttonBackground)
							])),
						_Utils_Tuple2(
						10,
						_List_fromArray(
							[
								A2(
								$rtfeldman$elm_css$Css$Animations$property,
								property,
								$author$project$Theme$colToString(theme.ready))
							])),
						_Utils_Tuple2(
						20,
						_List_fromArray(
							[
								A2(
								$rtfeldman$elm_css$Css$Animations$property,
								property,
								$author$project$Theme$colToString(theme.ready))
							])),
						_Utils_Tuple2(
						30,
						_List_fromArray(
							[
								A2($rtfeldman$elm_css$Css$Animations$property, property, buttonBackground)
							])),
						_Utils_Tuple2(
						100,
						_List_fromArray(
							[
								A2($rtfeldman$elm_css$Css$Animations$property, property, buttonBackground)
							]))
					]))),
			$rtfeldman$elm_css$Css$animationDuration(
			$rtfeldman$elm_css$Css$ms(2000)),
			$rtfeldman$elm_css$Css$animationIterationCount($rtfeldman$elm_css$Css$infinite)
		]);
};
var $author$project$View$inactiveSubmitButton = function (theme) {
	return A2(
		$rtfeldman$elm_css$Html$Styled$span,
		_List_fromArray(
			[
				$rtfeldman$elm_css$Html$Styled$Attributes$css(
				_Utils_ap(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$display($rtfeldman$elm_css$Css$inlineBlock),
							$rtfeldman$elm_css$Css$margin(
							$rtfeldman$elm_css$Css$em(0))
						]),
					function ($) {
						return $.inactive;
					}(
						$author$project$Theme$button(theme))))
			]),
		_List_fromArray(
			[
				A2($author$project$View$buttonIcon, theme, 'double_arrow')
			]));
};
var $rtfeldman$elm_css$Html$Styled$Attributes$title = $rtfeldman$elm_css$Html$Styled$Attributes$stringProperty('title');
var $author$project$View$Shutterbug$submitButton = F3(
	function (theme, msg, shutterbugInput) {
		var valid = A2($elm$regex$Regex$contains, $author$project$Shutterbug$regex, shutterbugInput);
		var summary = (shutterbugInput !== '') ? (valid ? 'Attempt to connect using the provided shutterbug.' : 'Awaiting a valid shutterbug.') : 'Awaiting a shutterbug.';
		var flashingStyle = valid ? $author$project$Theme$flashingReadyToSubmitStyles(theme) : _List_Nil;
		return valid ? A2(
			$rtfeldman$elm_css$Html$Styled$span,
			_List_fromArray(
				[
					$author$project$My$Html$onClickPreventDefault(msg),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_Utils_ap(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$display($rtfeldman$elm_css$Css$inlineBlock),
								$rtfeldman$elm_css$Css$margin(
								$rtfeldman$elm_css$Css$em(0))
							]),
						_Utils_ap(
							$author$project$Theme$button(theme).standard,
							flashingStyle))),
					$rtfeldman$elm_css$Html$Styled$Attributes$title(summary)
				]),
			_List_fromArray(
				[
					A2($author$project$View$buttonIcon, theme, 'double_arrow')
				])) : $author$project$View$inactiveSubmitButton(theme);
	});
var $rtfeldman$elm_css$Css$textBottom = $rtfeldman$elm_css$Css$prop1('text-bottom');
var $rtfeldman$elm_css$Html$Styled$Attributes$type_ = $rtfeldman$elm_css$Html$Styled$Attributes$stringProperty('type');
var $rtfeldman$elm_css$Html$Styled$ul = $rtfeldman$elm_css$Html$Styled$node('ul');
var $rtfeldman$elm_css$Html$Styled$Attributes$value = $rtfeldman$elm_css$Html$Styled$Attributes$stringProperty('value');
var $rtfeldman$elm_css$Css$width = $rtfeldman$elm_css$Css$prop1('width');
var $author$project$View$Shutterbug$inputBox = F5(
	function (theme, submitMsg, changeInputMsg, changeMaskMsg, shutterbug) {
		var tickOrError = function (ok) {
			return ok ? A2(
				$rtfeldman$elm_css$Html$Styled$span,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$class($author$project$View$defaultIconStyle),
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$color($author$project$Theme$gold),
								$rtfeldman$elm_css$Css$verticalAlign($rtfeldman$elm_css$Css$textBottom)
							]))
					]),
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$text('done_outline')
					])) : A2(
				$rtfeldman$elm_css$Html$Styled$span,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$class($author$project$View$defaultIconStyle),
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$color($author$project$Theme$red),
								$rtfeldman$elm_css$Css$verticalAlign($rtfeldman$elm_css$Css$textBottom)
							]))
					]),
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$text('error')
					]));
		};
		var paddingOk = A2($elm$core$String$endsWith, '=', shutterbug.input);
		var inputOk = ($elm$core$String$length(shutterbug.input) > 0) ? (A2($elm$regex$Regex$contains, $author$project$Shutterbug$regex, shutterbug.input) ? $elm$core$Maybe$Just(true) : $elm$core$Maybe$Just(false)) : $elm$core$Maybe$Nothing;
		var b64regex = function () {
			var _v4 = $elm$regex$Regex$fromString('^[\\w-]+=?$');
			if (_v4.$ === 'Just') {
				var r = _v4.a;
				return r;
			} else {
				return $elm$regex$Regex$never;
			}
		}();
		var charactersOk = A2($elm$regex$Regex$contains, b64regex, shutterbug.input);
		var inputHelp = _List_fromArray(
			[
				A2(
				$rtfeldman$elm_css$Html$Styled$p,
				_List_Nil,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$text('The shutterbug should be:')
					])),
				A2(
				$rtfeldman$elm_css$Html$Styled$ul,
				_List_Nil,
				_List_fromArray(
					[
						A2(
						$rtfeldman$elm_css$Html$Styled$li,
						_List_Nil,
						_List_fromArray(
							[
								tickOrError(
								$elm$core$String$length(shutterbug.input) === 44),
								$rtfeldman$elm_css$Html$Styled$text(' 44 characters long;')
							])),
						A2(
						$rtfeldman$elm_css$Html$Styled$li,
						_List_Nil,
						_List_fromArray(
							[
								tickOrError(charactersOk && paddingOk),
								$rtfeldman$elm_css$Html$Styled$text(' a '),
								A2($author$project$My$Html$a, 'https://en.wikipedia.org/wiki/Base64', 'Base64URL'),
								$rtfeldman$elm_css$Html$Styled$text(' encoding which:'),
								A2(
								$rtfeldman$elm_css$Html$Styled$ul,
								_List_Nil,
								_List_fromArray(
									[
										A2(
										$rtfeldman$elm_css$Html$Styled$li,
										_List_Nil,
										_List_fromArray(
											[
												tickOrError(paddingOk),
												$rtfeldman$elm_css$Html$Styled$text(' ends with a \''),
												A2(
												$rtfeldman$elm_css$Html$Styled$span,
												_List_fromArray(
													[
														$rtfeldman$elm_css$Html$Styled$Attributes$css(
														_List_fromArray(
															[
																$rtfeldman$elm_css$Css$fontWeight($rtfeldman$elm_css$Css$bold)
															]))
													]),
												_List_fromArray(
													[
														$rtfeldman$elm_css$Html$Styled$text('=')
													])),
												$rtfeldman$elm_css$Html$Styled$text('\' (the padding character);')
											])),
										A2(
										$rtfeldman$elm_css$Html$Styled$li,
										_List_Nil,
										_List_fromArray(
											[
												tickOrError(charactersOk),
												$rtfeldman$elm_css$Html$Styled$text(' contains only (apart from the padding character):'),
												A2(
												$rtfeldman$elm_css$Html$Styled$ul,
												_List_Nil,
												_List_fromArray(
													[
														A2(
														$rtfeldman$elm_css$Html$Styled$li,
														_List_Nil,
														_List_fromArray(
															[
																$rtfeldman$elm_css$Html$Styled$text('a-z')
															])),
														A2(
														$rtfeldman$elm_css$Html$Styled$li,
														_List_Nil,
														_List_fromArray(
															[
																$rtfeldman$elm_css$Html$Styled$text('A-Z')
															])),
														A2(
														$rtfeldman$elm_css$Html$Styled$li,
														_List_Nil,
														_List_fromArray(
															[
																$rtfeldman$elm_css$Html$Styled$text('0-9')
															])),
														A2(
														$rtfeldman$elm_css$Html$Styled$li,
														_List_Nil,
														_List_fromArray(
															[
																$rtfeldman$elm_css$Html$Styled$text('_-')
															]))
													]))
											]))
									]))
							]))
					]))
			]);
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$alignItems($rtfeldman$elm_css$Css$center),
							$rtfeldman$elm_css$Css$displayFlex,
							$rtfeldman$elm_css$Css$flexDirection($rtfeldman$elm_css$Css$column)
						]))
				]),
			_List_fromArray(
				[
					A2(
					$rtfeldman$elm_css$Html$Styled$p,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									$rtfeldman$elm_css$Css$marginBottom(
									$rtfeldman$elm_css$Css$em(3)),
									$rtfeldman$elm_css$Css$marginTop(
									$rtfeldman$elm_css$Css$em(3))
								]))
						]),
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text('Connect to Mechane using an already existing shutterbug.')
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									$rtfeldman$elm_css$Css$alignItems($rtfeldman$elm_css$Css$center),
									A3(
									$rtfeldman$elm_css$Css$border3,
									$rtfeldman$elm_css$Css$em(theme.buttonBorder),
									$rtfeldman$elm_css$Css$double,
									theme.primary),
									$rtfeldman$elm_css$Css$borderRadius(
									$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
									$rtfeldman$elm_css$Css$displayFlex,
									$rtfeldman$elm_css$Css$padding(
									$rtfeldman$elm_css$Css$em(theme.buttonPadding))
								]))
						]),
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Html$Styled$input,
							_Utils_ap(
								_List_fromArray(
									[
										$rtfeldman$elm_css$Html$Styled$Attributes$css(
										_List_fromArray(
											[
												$rtfeldman$elm_css$Css$backgroundColor($author$project$Theme$black),
												$rtfeldman$elm_css$Css$borderColor($author$project$Theme$gold),
												$rtfeldman$elm_css$Css$borderRadius(
												$rtfeldman$elm_css$Css$em(0.5)),
												$rtfeldman$elm_css$Css$borderStyle($rtfeldman$elm_css$Css$inset),
												$rtfeldman$elm_css$Css$borderWidth(
												$rtfeldman$elm_css$Css$em(0.5)),
												$rtfeldman$elm_css$Css$color($author$project$Theme$consoleGreen),
												A2(
												$rtfeldman$elm_css$Css$property,
												'caret-color',
												$author$project$Theme$colToString($author$project$Theme$consoleGreen)),
												$rtfeldman$elm_css$Css$fontSize(
												$rtfeldman$elm_css$Css$em(1)),
												$rtfeldman$elm_css$Css$height(
												$rtfeldman$elm_css$Css$em(2)),
												$rtfeldman$elm_css$Css$marginRight(
												$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
												A2($rtfeldman$elm_css$Css$property, 'user-select', 'none'),
												$rtfeldman$elm_css$Css$width(
												$rtfeldman$elm_css$Css$ch(44)),
												A2(
												$rtfeldman$elm_css$Css$pseudoClass,
												'focus',
												_List_fromArray(
													[
														$rtfeldman$elm_css$Css$outlineColor($author$project$Theme$dodgerBlue),
														$rtfeldman$elm_css$Css$outlineStyle($rtfeldman$elm_css$Css$solid),
														$rtfeldman$elm_css$Css$outlineWidth(
														$rtfeldman$elm_css$Css$em(0.3))
													])),
												A2(
												$rtfeldman$elm_css$Css$pseudoClass,
												':placeholder',
												_List_fromArray(
													[
														$rtfeldman$elm_css$Css$color($author$project$Theme$consoleGreen)
													]))
											])),
										$rtfeldman$elm_css$Html$Styled$Attributes$autocomplete(false),
										$rtfeldman$elm_css$Html$Styled$Attributes$minlength($author$project$Shutterbug$maxLength),
										$rtfeldman$elm_css$Html$Styled$Attributes$maxlength($author$project$Shutterbug$maxLength),
										$rtfeldman$elm_css$Html$Styled$Attributes$pattern('[\\w-]{43}='),
										$rtfeldman$elm_css$Html$Styled$Attributes$placeholder('Paste the shutterbug here...'),
										$rtfeldman$elm_css$Html$Styled$Attributes$spellcheck(false),
										$rtfeldman$elm_css$Html$Styled$Attributes$title('Enter a Base64URL encoding with a length of 44 sextets' + ' including the padding character.'),
										$rtfeldman$elm_css$Html$Styled$Attributes$type_(
										shutterbug.unmasked ? 'text' : 'password'),
										$rtfeldman$elm_css$Html$Styled$Attributes$value(shutterbug.input),
										$rtfeldman$elm_css$Html$Styled$Events$onInput(changeInputMsg)
									]),
								function () {
									if ((inputOk.$ === 'Just') && inputOk.a) {
										return _List_fromArray(
											[
												A2(
												$rtfeldman$elm_css$Html$Styled$Events$on,
												'keydown',
												A2(
													$elm$json$Json$Decode$map,
													function (key) {
														if (key === 'Enter') {
															return submitMsg(shutterbug.input);
														} else {
															return submitMsg('');
														}
													},
													A2($elm$json$Json$Decode$field, 'key', $elm$json$Json$Decode$string)))
											]);
									} else {
										return _List_Nil;
									}
								}()),
							_List_Nil),
							A3(
							$author$project$View$Shutterbug$submitButton,
							theme,
							submitMsg(shutterbug.input),
							shutterbug.input),
							A2(
							$rtfeldman$elm_css$Html$Styled$p,
							_List_Nil,
							_List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Html$Styled$span,
									_List_fromArray(
										[
											$author$project$My$Html$onClickPreventDefault(changeMaskMsg),
											$rtfeldman$elm_css$Html$Styled$Attributes$css(
											_Utils_ap(
												_List_fromArray(
													[
														$rtfeldman$elm_css$Css$display($rtfeldman$elm_css$Css$inlineBlock),
														$rtfeldman$elm_css$Css$margin(
														$rtfeldman$elm_css$Css$em(0)),
														$rtfeldman$elm_css$Css$marginLeft(
														$rtfeldman$elm_css$Css$em(theme.buttonPadding))
													]),
												$author$project$Theme$button(theme).standard)),
											$rtfeldman$elm_css$Html$Styled$Attributes$title(
											shutterbug.unmasked ? 'Mask the shutterbug.' : ('Unmask the shutterbug.\n' + '( ⚠ Only unmask in private.)'))
										]),
									_List_fromArray(
										[
											A2(
											$author$project$View$buttonIcon,
											theme,
											shutterbug.unmasked ? 'visibility' : 'visibility_off')
										]))
								]))
						])),
					function () {
					if (inputOk.$ === 'Nothing') {
						return $rtfeldman$elm_css$Html$Styled$text('');
					} else {
						if (inputOk.a) {
							return A2(
								$rtfeldman$elm_css$Html$Styled$p,
								_List_fromArray(
									[
										$rtfeldman$elm_css$Html$Styled$Attributes$css(
										_List_fromArray(
											[
												$rtfeldman$elm_css$Css$color(theme.primary)
											])),
										$rtfeldman$elm_css$Html$Styled$Attributes$title('Looks good!')
									]),
								_List_fromArray(
									[
										A2($author$project$View$buttonIcon, theme, 'thumb_up')
									]));
						} else {
							return A2(
								$rtfeldman$elm_css$Html$Styled$p,
								_List_fromArray(
									[
										$rtfeldman$elm_css$Html$Styled$Attributes$css(
										_List_fromArray(
											[
												$rtfeldman$elm_css$Css$color(theme.primary)
											])),
										$rtfeldman$elm_css$Html$Styled$Attributes$title('Oh no, this doesn\'t look right!')
									]),
								_List_fromArray(
									[
										A2($author$project$View$buttonIcon, theme, 'thumb_down')
									]));
						}
					}
				}(),
					function () {
					if (inputOk.$ === 'Nothing') {
						return $rtfeldman$elm_css$Html$Styled$text('');
					} else {
						if (inputOk.a) {
							return $rtfeldman$elm_css$Html$Styled$text('');
						} else {
							return A2($rtfeldman$elm_css$Html$Styled$div, _List_Nil, inputHelp);
						}
					}
				}()
				]));
	});
var $rtfeldman$elm_css$Css$flexStart = $rtfeldman$elm_css$Css$prop1('flex-start');
var $rtfeldman$elm_css$Css$justifyContent = function (fn) {
	return A3(
		$rtfeldman$elm_css$Css$Internal$getOverloadedProperty,
		'justifyContent',
		'justify-content',
		fn($rtfeldman$elm_css$Css$Internal$lengthForOverloadedProperty));
};
var $rtfeldman$elm_css$Css$minWidth = $rtfeldman$elm_css$Css$prop1('min-width');
var $rtfeldman$elm_css$Css$paddingLeft = $rtfeldman$elm_css$Css$prop1('padding-left');
var $rtfeldman$elm_css$Css$textAlign = function (fn) {
	return A3(
		$rtfeldman$elm_css$Css$Internal$getOverloadedProperty,
		'textAlign',
		'text-align',
		fn($rtfeldman$elm_css$Css$Internal$lengthForOverloadedProperty));
};
var $rtfeldman$elm_css$Css$auto = {alignItemsOrAuto: $rtfeldman$elm_css$Css$Structure$Compatible, cursor: $rtfeldman$elm_css$Css$Structure$Compatible, flexBasis: $rtfeldman$elm_css$Css$Structure$Compatible, intOrAuto: $rtfeldman$elm_css$Css$Structure$Compatible, justifyContentOrAuto: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrAuto: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrAutoOrCoverOrContain: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrNumberOrAutoOrNoneOrContent: $rtfeldman$elm_css$Css$Structure$Compatible, overflow: $rtfeldman$elm_css$Css$Structure$Compatible, pointerEvents: $rtfeldman$elm_css$Css$Structure$Compatible, tableLayout: $rtfeldman$elm_css$Css$Structure$Compatible, textRendering: $rtfeldman$elm_css$Css$Structure$Compatible, touchAction: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'auto'};
var $rtfeldman$elm_css$Html$Styled$form = $rtfeldman$elm_css$Html$Styled$node('form');
var $rtfeldman$elm_css$Css$left = $rtfeldman$elm_css$Css$prop1('left');
var $rtfeldman$elm_css$Css$maxHeight = $rtfeldman$elm_css$Css$prop1('max-height');
var $rtfeldman$elm_css$Css$overflow = $rtfeldman$elm_css$Css$prop1('overflow');
var $rtfeldman$elm_css$Css$position = $rtfeldman$elm_css$Css$prop1('position');
var $rtfeldman$elm_css$Css$relative = {position: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'relative'};
var $author$project$Cameras$AssociateMsg = function (a) {
	return {$: 'AssociateMsg', a: a};
};
var $author$project$Stitch$AssociateSchema = F2(
	function (cameras, stitching) {
		return {cameras: cameras, stitching: stitching};
	});
var $author$project$Stitch$toString = function (stitching) {
	switch (stitching.$) {
		case 'UniStitching':
			return $author$project$Stitch$uni;
		case 'BiStitching':
			return $author$project$Stitch$bi;
		case 'BiPartialStitching':
			return $author$project$Stitch$biPartial;
		case 'TriStitching':
			return $author$project$Stitch$tri;
		case 'TriPartialStitching':
			return $author$project$Stitch$triPartial;
		case 'QuadStitching':
			return $author$project$Stitch$quad;
		default:
			return $author$project$Stitch$quadPartial;
	}
};
var $author$project$Stitch$associate = function (associations) {
	switch (associations.$) {
		case 'Uni':
			var maybeMajor = associations.a;
			if (maybeMajor.$ === 'Just') {
				var major = maybeMajor.a;
				return A2(
					$author$project$Stitch$AssociateSchema,
					_List_fromArray(
						[major]),
					$author$project$Stitch$toString($author$project$Stitch$UniStitching));
			} else {
				return A2(
					$author$project$Stitch$AssociateSchema,
					_List_Nil,
					$author$project$Stitch$toString($author$project$Stitch$UniStitching));
			}
		case 'Bi':
			var major = associations.a;
			var minor = associations.b;
			return A2(
				$author$project$Stitch$AssociateSchema,
				_List_fromArray(
					[major, minor]),
				$author$project$Stitch$toString($author$project$Stitch$BiStitching));
		case 'BiPartial':
			var major = associations.a;
			var minor = associations.b;
			return A2(
				$author$project$Stitch$AssociateSchema,
				_List_fromArray(
					[major, minor]),
				$author$project$Stitch$toString($author$project$Stitch$BiPartialStitching));
		case 'Tri':
			var major = associations.a;
			var minor1 = associations.b;
			var minor2 = associations.c;
			return A2(
				$author$project$Stitch$AssociateSchema,
				_List_fromArray(
					[major, minor1, minor2]),
				$author$project$Stitch$toString($author$project$Stitch$TriStitching));
		case 'TriPartial':
			var major = associations.a;
			var minor1 = associations.b;
			var minor2 = associations.c;
			return A2(
				$author$project$Stitch$AssociateSchema,
				_List_fromArray(
					[major, minor1, minor2]),
				$author$project$Stitch$toString($author$project$Stitch$TriPartialStitching));
		case 'Quad':
			var major = associations.a;
			var minor1 = associations.b;
			var minor2 = associations.c;
			var minor3 = associations.d;
			return A2(
				$author$project$Stitch$AssociateSchema,
				_List_fromArray(
					[major, minor1, minor2, minor3]),
				$author$project$Stitch$toString($author$project$Stitch$QuadStitching));
		default:
			var major = associations.a;
			var minor1 = associations.b;
			var minor2 = associations.c;
			var minor3 = associations.d;
			return A2(
				$author$project$Stitch$AssociateSchema,
				_List_fromArray(
					[major, minor1, minor2, minor3]),
				$author$project$Stitch$toString($author$project$Stitch$QuadPartialStitching));
	}
};
var $rtfeldman$elm_css$Html$Styled$button = $rtfeldman$elm_css$Html$Styled$node('button');
var $author$project$Stitch$FullMode = {$: 'FullMode'};
var $author$project$Stitch$PartialMode = {$: 'PartialMode'};
var $author$project$Stitch$UniMode = {$: 'UniMode'};
var $author$project$Stitch$mode = function (stitching) {
	switch (stitching.$) {
		case 'UniStitching':
			return $author$project$Stitch$UniMode;
		case 'BiStitching':
			return $author$project$Stitch$FullMode;
		case 'BiPartialStitching':
			return $author$project$Stitch$PartialMode;
		case 'TriStitching':
			return $author$project$Stitch$FullMode;
		case 'TriPartialStitching':
			return $author$project$Stitch$PartialMode;
		case 'QuadStitching':
			return $author$project$Stitch$FullMode;
		default:
			return $author$project$Stitch$PartialMode;
	}
};
var $elm$core$List$sortBy = _List_sortBy;
var $elm$core$List$sort = function (xs) {
	return A2($elm$core$List$sortBy, $elm$core$Basics$identity, xs);
};
var $author$project$Stitch$stitchingOf = function (associations) {
	switch (associations.$) {
		case 'Uni':
			return $author$project$Stitch$UniStitching;
		case 'Bi':
			return $author$project$Stitch$BiStitching;
		case 'BiPartial':
			return $author$project$Stitch$BiPartialStitching;
		case 'Tri':
			return $author$project$Stitch$TriStitching;
		case 'TriPartial':
			return $author$project$Stitch$TriPartialStitching;
		case 'Quad':
			return $author$project$Stitch$QuadStitching;
		default:
			return $author$project$Stitch$QuadPartialStitching;
	}
};
var $author$project$Stitch$summariseChanges = F2(
	function (from, to) {
		var toNames = $author$project$Stitch$names(to);
		var toMode = $author$project$Stitch$mode(
			$author$project$Stitch$stitchingOf(to));
		var fromNames = $author$project$Stitch$names(from);
		var removing = $elm$core$List$sort(
			$elm$core$Set$toList(
				A2(
					$elm$core$Set$diff,
					$elm$core$Set$fromList(fromNames),
					$elm$core$Set$fromList(toNames))));
		var toRemove = $elm$core$List$length(removing);
		var fromMode = $author$project$Stitch$mode(
			$author$project$Stitch$stitchingOf(from));
		var adding = $elm$core$List$sort(
			$elm$core$Set$toList(
				A2(
					$elm$core$Set$diff,
					$elm$core$Set$fromList(toNames),
					$elm$core$Set$fromList(fromNames))));
		var toAdd = $elm$core$List$length(adding);
		return (!(toAdd + toRemove)) ? (_Utils_eq(fromMode, toMode) ? (_Utils_eq(toNames, fromNames) ? '' : '\nAdjust camera positioning.') : (_Utils_eq(toMode, $author$project$Stitch$FullMode) ? '\nSwitch to full size mode.' : '\nSwitch to mixed size mode.')) : _Utils_ap(
			function () {
				var _v0 = $elm$core$List$length(adding);
				switch (_v0) {
					case 0:
						return '';
					case 1:
						return '\nAssociate 1 new camera: ' + (A2($elm$core$String$join, ', ', adding) + '.');
					default:
						var n = _v0;
						return '\nAssociate ' + ($elm$core$String$fromInt(n) + (' new cameras: ' + (A2($elm$core$String$join, ', ', adding) + '.')));
				}
			}(),
			_Utils_ap(
				function () {
					var _v1 = $elm$core$List$length(removing);
					switch (_v1) {
						case 0:
							return '';
						case 1:
							return '\nDisassociate 1 camera: ' + (A2($elm$core$String$join, ', ', removing) + '.');
						default:
							var n = _v1;
							return '\nDisassociate ' + ($elm$core$String$fromInt(n) + (' cameras: ' + (A2($elm$core$String$join, ', ', removing) + '.')));
					}
				}(),
				function () {
					if (_Utils_eq(toMode, fromMode)) {
						return '';
					} else {
						switch (toMode.$) {
							case 'UniMode':
								return '\nSwitch to single camera mode.';
							case 'FullMode':
								return '\nSwitch to full size mode.';
							default:
								return '\nSwitch to mixed size mode.';
						}
					}
				}()));
	});
var $author$project$Cameras$difference = function (_v0) {
	var registry = _v0.b;
	return A2($author$project$Stitch$summariseChanges, registry.associated, registry.selection);
};
var $author$project$Cameras$selected = function (_v0) {
	var registry = _v0.b;
	return registry.selection;
};
var $author$project$Cameras$selectedNames = function (_v0) {
	var registry = _v0.b;
	return $author$project$Stitch$names(registry.selection);
};
var $author$project$Cameras$registered = function (_v0) {
	var registry = _v0.b;
	return registry.allRegistered;
};
var $author$project$View$Cameras$viewSelectionSummary = F2(
	function (theme, cameras) {
		return function (_v3) {
			var text = _v3.a;
			var color = _v3.b;
			return A2(
				$rtfeldman$elm_css$Html$Styled$span,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$color(color),
								$rtfeldman$elm_css$Css$padding(
								$rtfeldman$elm_css$Css$em(theme.buttonPadding))
							]))
					]),
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$text(text)
					]));
		}(
			function () {
				var _v0 = $elm$core$List$length(
					$author$project$Cameras$registered(cameras));
				switch (_v0) {
					case 0:
						return _Utils_Tuple2('No cameras to choose from', theme.textPassive);
					case 1:
						var _v1 = $elm$core$List$length(
							$author$project$Cameras$selectedNames(cameras));
						if (!_v1) {
							return _Utils_Tuple2('Camera not selected', theme.textPassive);
						} else {
							return _Utils_Tuple2('Camera selected', theme.textNormal);
						}
					default:
						var _v2 = $elm$core$List$length(
							$author$project$Cameras$selectedNames(cameras));
						switch (_v2) {
							case 0:
								return _Utils_Tuple2('No cameras selected', theme.textPassive);
							case 1:
								return _Utils_Tuple2('Selected 1 camera', theme.textNormal);
							default:
								var n = _v2;
								return _Utils_Tuple2(
									'Selected ' + ($elm$core$String$fromInt(n) + ' cameras'),
									theme.textNormal);
						}
				}
			}());
	});
var $author$project$View$Cameras$viewSelectionSubmitButton = F3(
	function (theme, msg, cameras) {
		var noChanges = '\n(There are no changes.)';
		var changes = $author$project$Cameras$difference(cameras);
		var flashingStyle = (changes !== '') ? $author$project$Theme$flashingReadyToSubmitStyles(theme) : _List_Nil;
		var summary = (changes !== '') ? changes : noChanges;
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$height(
							$rtfeldman$elm_css$Css$em(theme.buttonHeight))
						]))
				]),
			_List_fromArray(
				[
					function () {
					var _v0 = $author$project$Cameras$selectedNames(cameras);
					if (!_v0.b) {
						return $author$project$View$inactiveSubmitButton(theme);
					} else {
						return A2(
							$rtfeldman$elm_css$Html$Styled$button,
							_List_fromArray(
								[
									$author$project$My$Html$onClickPreventDefault(
									msg(
										$author$project$Cameras$AssociateMsg(
											$author$project$Stitch$associate(
												$author$project$Cameras$selected(cameras))))),
									$rtfeldman$elm_css$Html$Styled$Attributes$css(
									_Utils_ap(
										$author$project$Theme$button(theme).standard,
										flashingStyle)),
									function () {
									var _v1 = $elm$core$List$length(
										$author$project$Cameras$selectedNames(cameras));
									if (_v1 === 1) {
										return $rtfeldman$elm_css$Html$Styled$Attributes$title('Submit camera selection.' + summary);
									} else {
										return $rtfeldman$elm_css$Html$Styled$Attributes$title('Submit camera selections.' + summary);
									}
								}()
								]),
							_List_fromArray(
								[
									A2($author$project$View$buttonIcon, theme, 'double_arrow')
								]));
					}
				}(),
					A2($author$project$View$Cameras$viewSelectionSummary, theme, cameras)
				]));
	});
var $rtfeldman$elm_css$Css$border = $rtfeldman$elm_css$Css$prop1('border');
var $author$project$Cameras$displayedNames = function (_v0) {
	var registry = _v0.b;
	var _v1 = registry.recentChanges;
	if (_v1.$ === 'Just') {
		var changes = _v1.a;
		return _Utils_ap(
			A2($elm$core$List$map, $author$project$Camera$nameOf, registry.allRegistered),
			changes.removed);
	} else {
		return A2($elm$core$List$map, $author$project$Camera$nameOf, registry.allRegistered);
	}
};
var $rtfeldman$elm_css$Css$minHeight = $rtfeldman$elm_css$Css$prop1('min-height');
var $rtfeldman$elm_css$Html$Styled$Attributes$attribute = $rtfeldman$elm_css$VirtualDom$Styled$attribute;
var $author$project$Cameras$buttonAnimationDuration = '1.4s';
var $rtfeldman$elm_css$Html$Styled$label = $rtfeldman$elm_css$Html$Styled$node('label');
var $rtfeldman$elm_css$Css$lineThrough = {textDecorationLine: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'line-through'};
var $author$project$Cameras$recentAddedNames = function (_v0) {
	var registry = _v0.b;
	var _v1 = registry.recentChanges;
	if (_v1.$ === 'Just') {
		var changes = _v1.a;
		return changes.added;
	} else {
		return _List_Nil;
	}
};
var $author$project$Cameras$recentRemovedNames = function (_v0) {
	var registry = _v0.b;
	var _v1 = registry.recentChanges;
	if (_v1.$ === 'Just') {
		var changes = _v1.a;
		return changes.removed;
	} else {
		return _List_Nil;
	}
};
var $rtfeldman$elm_css$Css$textDecoration3 = $rtfeldman$elm_css$Css$prop3('text-decoration');
var $author$project$Cameras$ToggleMsg = function (a) {
	return {$: 'ToggleMsg', a: a};
};
var $author$project$Cameras$radio = function (_v0) {
	var mode = _v0.a;
	if (mode.$ === 'Single') {
		return true;
	} else {
		return false;
	}
};
var $elm$core$Set$member = F2(
	function (key, _v0) {
		var dict = _v0.a;
		return A2($elm$core$Dict$member, key, dict);
	});
var $author$project$Cameras$recentAutoDeselected = F2(
	function (name, _v0) {
		var registry = _v0.b;
		var _v1 = registry.recentChanges;
		if (_v1.$ === 'Just') {
			var changes = _v1.a;
			return A2($elm$core$Set$member, name, changes.autoDeselected);
		} else {
			return false;
		}
	});
var $author$project$Cameras$recentAutoDisassociated = F2(
	function (name, _v0) {
		var registry = _v0.b;
		var _v1 = registry.recentChanges;
		if (_v1.$ === 'Just') {
			var changes = _v1.a;
			return A2($elm$core$Set$member, name, changes.autoDisassociated);
		} else {
			return false;
		}
	});
var $author$project$View$Cameras$viewTogglerButton = F4(
	function (theme, msg, cameras, cam) {
		var selected = A2(
			$elm$core$List$member,
			cam,
			$author$project$Cameras$selectedNames(cameras));
		var inactive = $elm_community$maybe_extra$Maybe$Extra$isJust(
			$author$project$Cameras$recentDifference(cameras));
		var buttonStyle = A3(
			$author$project$My$Extra$ternary,
			inactive,
			function ($) {
				return $.inactive;
			},
			A3(
				$author$project$My$Extra$ternary,
				selected,
				function ($) {
					return $.standard;
				},
				function ($) {
					return $.unselected;
				}));
		var autoDisassociated = A2($author$project$Cameras$recentAutoDisassociated, cam, cameras);
		var autoDeselected = A2($author$project$Cameras$recentAutoDeselected, cam, cameras);
		return A2(
			$rtfeldman$elm_css$Html$Styled$button,
			_List_fromArray(
				[
					$author$project$My$Html$onClickPreventDefault(
					msg(
						$author$project$Cameras$ToggleMsg(cam))),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					A2(
						$author$project$My$Extra$appendWhen,
						buttonStyle(
							$author$project$Theme$button(theme)),
						_List_fromArray(
							[
								_Utils_Tuple2(
								autoDeselected || autoDisassociated,
								_List_fromArray(
									[
										A3(
										$author$project$My$Extra$ternary,
										autoDisassociated,
										$rtfeldman$elm_css$Css$backgroundColor(theme.error),
										$rtfeldman$elm_css$Css$backgroundColor(theme.warning))
									]))
							])))
				]),
			_List_fromArray(
				[
					A2(
					$author$project$View$buttonIcon,
					theme,
					$author$project$Cameras$radio(cameras) ? A3($author$project$My$Extra$ternary, selected, 'radio_button_checked', 'radio_button_unchecked') : A3($author$project$My$Extra$ternary, selected, 'check_box', 'check_box_outline_blank'))
				]));
	});
var $rtfeldman$elm_css$Css$wavy = {textDecorationStyle: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'wavy'};
var $author$project$View$Cameras$viewTogglerField = F4(
	function (theme, msg, cameras, cam) {
		var viewRecentAddOrRemove = function (removed) {
			return A3(
				$rtfeldman$elm_css$Html$Styled$node,
				removed ? 'shrinking-div' : 'growing-div',
				_List_fromArray(
					[
						A2($rtfeldman$elm_css$Html$Styled$Attributes$attribute, 'duration', $author$project$Cameras$buttonAnimationDuration),
						A2($rtfeldman$elm_css$Html$Styled$Attributes$attribute, 'name', cam),
						A2(
						$rtfeldman$elm_css$Html$Styled$Attributes$attribute,
						'names',
						A2(
							$elm$core$String$join,
							',',
							$author$project$Cameras$displayedNames(cameras)))
					]),
				_List_fromArray(
					[
						A2(
						$rtfeldman$elm_css$Html$Styled$div,
						_List_fromArray(
							[
								$rtfeldman$elm_css$Html$Styled$Attributes$css(
								_List_fromArray(
									[
										$rtfeldman$elm_css$Css$height(
										$rtfeldman$elm_css$Css$em(theme.buttonHeight)),
										$rtfeldman$elm_css$Css$marginTop(
										$rtfeldman$elm_css$Css$em(theme.buttonPadding))
									])),
								A2($rtfeldman$elm_css$Html$Styled$Attributes$attribute, 'slot', 'content')
							]),
						_List_fromArray(
							[
								A4($author$project$View$Cameras$viewTogglerButton, theme, msg, cameras, cam),
								A2(
								$rtfeldman$elm_css$Html$Styled$label,
								_List_fromArray(
									[
										$rtfeldman$elm_css$Html$Styled$Attributes$css(
										A2(
											$author$project$My$Extra$appendWhen,
											_List_fromArray(
												[
													$rtfeldman$elm_css$Css$paddingLeft(
													$rtfeldman$elm_css$Css$em(theme.buttonPadding))
												]),
											_List_fromArray(
												[
													_Utils_Tuple2(
													removed,
													_List_fromArray(
														[
															A3(
															$rtfeldman$elm_css$Css$textDecoration3,
															$rtfeldman$elm_css$Css$lineThrough,
															$rtfeldman$elm_css$Css$wavy,
															A3($rtfeldman$elm_css$Css$rgb, 255, 0, 0))
														]))
												])))
									]),
								_List_fromArray(
									[
										$rtfeldman$elm_css$Html$Styled$text(cam)
									]))
							]))
					]));
		};
		return A2(
			$elm$core$List$member,
			cam,
			$author$project$Cameras$recentRemovedNames(cameras)) ? viewRecentAddOrRemove(true) : (A2(
			$elm$core$List$member,
			cam,
			$author$project$Cameras$recentAddedNames(cameras)) ? viewRecentAddOrRemove(false) : A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$height(
							$rtfeldman$elm_css$Css$em(theme.buttonHeight)),
							$rtfeldman$elm_css$Css$marginTop(
							$rtfeldman$elm_css$Css$em(theme.buttonPadding))
						]))
				]),
			_List_fromArray(
				[
					A4($author$project$View$Cameras$viewTogglerButton, theme, msg, cameras, cam),
					A2(
					$rtfeldman$elm_css$Html$Styled$label,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									$rtfeldman$elm_css$Css$paddingLeft(
									$rtfeldman$elm_css$Css$em(theme.buttonPadding))
								]))
						]),
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text(cam)
						]))
				])));
	});
var $author$project$View$Cameras$viewSelectors = F3(
	function (theme, msg, cameras) {
		var margin = theme.buttonPadding;
		var minHeight = (4 * theme.buttonHeight) + (3 * margin);
		var _v0 = $author$project$Cameras$displayedNames(cameras);
		if (!_v0.b) {
			return A2(
				$rtfeldman$elm_css$Html$Styled$div,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$alignItems($rtfeldman$elm_css$Css$center),
								$rtfeldman$elm_css$Css$color(theme.textPassive),
								$rtfeldman$elm_css$Css$displayFlex,
								$rtfeldman$elm_css$Css$flexDirection($rtfeldman$elm_css$Css$column),
								$rtfeldman$elm_css$Css$height(
								$rtfeldman$elm_css$Css$em(minHeight)),
								$rtfeldman$elm_css$Css$justifyContent($rtfeldman$elm_css$Css$center),
								$rtfeldman$elm_css$Css$marginTop(
								$rtfeldman$elm_css$Css$em(margin))
							]))
					]),
				_List_fromArray(
					[
						A2(
						$rtfeldman$elm_css$Html$Styled$span,
						_List_Nil,
						_List_fromArray(
							[
								$rtfeldman$elm_css$Html$Styled$text('Available cameras appear here')
							]))
					]));
		} else {
			var displayed = _v0;
			return A2(
				$rtfeldman$elm_css$Html$Styled$div,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$border(
								$rtfeldman$elm_css$Css$em(0)),
								$rtfeldman$elm_css$Css$minHeight(
								$rtfeldman$elm_css$Css$em(minHeight))
							]))
					]),
				A2(
					$elm$core$List$map,
					function (cam) {
						return A4($author$project$View$Cameras$viewTogglerField, theme, msg, cameras, cam);
					},
					$elm$core$List$sort(displayed)));
		}
	});
var $author$project$View$Cameras$viewSelectorForm = F5(
	function (theme, toggleMsg, assocMsg, cameras, maybeMaxVisibleCams) {
		var maxVisibleCams = function () {
			if (maybeMaxVisibleCams.$ === 'Just') {
				var max = maybeMaxVisibleCams.a;
				return max;
			} else {
				return 6;
			}
		}();
		return A2(
			$rtfeldman$elm_css$Html$Styled$form,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							A3(
							$rtfeldman$elm_css$Css$border3,
							$rtfeldman$elm_css$Css$em(theme.buttonBorder),
							$rtfeldman$elm_css$Css$double,
							theme.primary),
							$rtfeldman$elm_css$Css$borderRadius(
							$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
							$rtfeldman$elm_css$Css$display($rtfeldman$elm_css$Css$inlineBlock),
							$rtfeldman$elm_css$Css$maxHeight(
							$rtfeldman$elm_css$Css$em(((maxVisibleCams + 1) * theme.buttonHeight) + (maxVisibleCams * theme.buttonPadding))),
							$rtfeldman$elm_css$Css$overflow($rtfeldman$elm_css$Css$auto),
							$rtfeldman$elm_css$Css$padding(
							$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
							$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$relative),
							$rtfeldman$elm_css$Css$textAlign($rtfeldman$elm_css$Css$left),
							$rtfeldman$elm_css$Css$width(
							$rtfeldman$elm_css$Css$em(20))
						]))
				]),
			_List_fromArray(
				[
					A3($author$project$View$Cameras$viewSelectionSubmitButton, theme, assocMsg, cameras),
					A3($author$project$View$Cameras$viewSelectors, theme, toggleMsg, cameras)
				]));
	});
var $author$project$Stitch$SwitchedCameras = function (a) {
	return {$: 'SwitchedCameras', a: a};
};
var $author$project$Stitch$ToggledStitchingSize = function (a) {
	return {$: 'ToggledStitchingSize', a: a};
};
var $author$project$Stitch$associationsAttribute = 'associations';
var $author$project$Stitch$attribute = function (associations) {
	var schema = $author$project$Stitch$associate(associations);
	return A2(
		$elm$core$String$join,
		',',
		A2($elm$core$List$cons, schema.stitching, schema.cameras));
};
var $author$project$Stitch$SwitchCamerasMsg = F2(
	function (a, b) {
		return {$: 'SwitchCamerasMsg', a: a, b: b};
	});
var $author$project$Stitch$decodeSwitchCameras = A2(
	$elm$json$Json$Decode$field,
	'detail',
	A3(
		$elm$json$Json$Decode$map2,
		$author$project$Stitch$SwitchCamerasMsg,
		A2($elm$json$Json$Decode$field, 'camera1', $elm$json$Json$Decode$string),
		A2($elm$json$Json$Decode$field, 'camera2', $elm$json$Json$Decode$string)));
var $author$project$Stitch$ToggleStitchingSizeMsg = {$: 'ToggleStitchingSizeMsg'};
var $author$project$Stitch$decodeToggleStitchingSize = $elm$json$Json$Decode$succeed($author$project$Stitch$ToggleStitchingSizeMsg);
var $author$project$Cameras$selectionSession = function (_v0) {
	var registry = _v0.b;
	return registry.selectionSession;
};
var $author$project$Stitch$sessionAttribute = 'session';
var $author$project$Stitch$tag = 'stitching-chooser';
var $author$project$Stitch$switchCamerasEvent = $author$project$Stitch$tag + '-switch-cameras';
var $author$project$Stitch$toggleStitchingSizeEvent = $author$project$Stitch$tag + '-toggle-stitching-size';
var $author$project$Stitch$widthAttribute = 'w';
var $author$project$View$Cameras$viewStitchingChooser = F2(
	function (msg, cameras) {
		return A3(
			$rtfeldman$elm_css$Html$Styled$node,
			$author$project$Stitch$tag,
			_List_fromArray(
				[
					A2(
					$rtfeldman$elm_css$Html$Styled$Attributes$attribute,
					$author$project$Stitch$associationsAttribute,
					$author$project$Stitch$attribute(
						$author$project$Cameras$selected(cameras))),
					A2(
					$rtfeldman$elm_css$Html$Styled$Attributes$attribute,
					$author$project$Stitch$sessionAttribute,
					$elm$core$String$fromInt(
						$author$project$Cameras$selectionSession(cameras))),
					A2($rtfeldman$elm_css$Html$Styled$Attributes$attribute, $author$project$Stitch$widthAttribute, '25em'),
					A2(
					$rtfeldman$elm_css$Html$Styled$Events$on,
					$author$project$Stitch$switchCamerasEvent,
					A2(
						$elm$json$Json$Decode$map,
						msg,
						A2($elm$json$Json$Decode$map, $author$project$Stitch$SwitchedCameras, $author$project$Stitch$decodeSwitchCameras))),
					A2(
					$rtfeldman$elm_css$Html$Styled$Events$on,
					$author$project$Stitch$toggleStitchingSizeEvent,
					A2(
						$elm$json$Json$Decode$map,
						msg,
						A2($elm$json$Json$Decode$map, $author$project$Stitch$ToggledStitchingSize, $author$project$Stitch$decodeToggleStitchingSize)))
				]),
			_List_Nil);
	});
var $author$project$View$Cameras$starting = function (data) {
	var fadeIn = _List_fromArray(
		[
			$rtfeldman$elm_css$Css$animationName(
			$rtfeldman$elm_css$Css$Animations$keyframes(
				_List_fromArray(
					[
						_Utils_Tuple2(
						0,
						_List_fromArray(
							[
								A2($rtfeldman$elm_css$Css$Animations$property, 'opacity', '0')
							])),
						_Utils_Tuple2(
						50,
						_List_fromArray(
							[
								A2($rtfeldman$elm_css$Css$Animations$property, 'opacity', '0')
							])),
						_Utils_Tuple2(
						100,
						_List_fromArray(
							[
								A2($rtfeldman$elm_css$Css$Animations$property, 'opacity', '1')
							]))
					]))),
			$rtfeldman$elm_css$Css$animationDuration(
			$rtfeldman$elm_css$Css$ms(1000)),
			$rtfeldman$elm_css$Css$animationIterationCount(
			$rtfeldman$elm_css$Css$num(1))
		]);
	return A2(
		$rtfeldman$elm_css$Html$Styled$div,
		_List_fromArray(
			[
				$rtfeldman$elm_css$Html$Styled$Attributes$css(
				_Utils_ap(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$textAlign($rtfeldman$elm_css$Css$center),
							$rtfeldman$elm_css$Css$minWidth(
							$rtfeldman$elm_css$Css$em(50))
						]),
					function () {
						var _v0 = data.source;
						if (_v0.$ === 'MakeNew') {
							return _List_Nil;
						} else {
							return fadeIn;
						}
					}()))
			]),
		_List_fromArray(
			[
				A2(
				$rtfeldman$elm_css$Html$Styled$p,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$marginBottom(
								$rtfeldman$elm_css$Css$em(3)),
								$rtfeldman$elm_css$Css$marginTop(
								$rtfeldman$elm_css$Css$em(3))
							]))
					]),
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$text('To continue select a camera(s):')
					])),
				A2(
				$rtfeldman$elm_css$Html$Styled$div,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$displayFlex,
								$rtfeldman$elm_css$Css$alignItems($rtfeldman$elm_css$Css$flexStart),
								$rtfeldman$elm_css$Css$justifyContent($rtfeldman$elm_css$Css$center)
							]))
					]),
				_List_fromArray(
					[
						A5($author$project$View$Cameras$viewSelectorForm, data.theme, data.toggleMsg, data.assocMsg, data.cameras, $elm$core$Maybe$Nothing),
						A2(
						$rtfeldman$elm_css$Html$Styled$div,
						_List_fromArray(
							[
								$rtfeldman$elm_css$Html$Styled$Attributes$css(
								_List_fromArray(
									[
										$rtfeldman$elm_css$Css$paddingLeft(
										$rtfeldman$elm_css$Css$em(data.theme.buttonPadding))
									]))
							]),
						_List_fromArray(
							[
								A2($author$project$View$Cameras$viewStitchingChooser, data.stitchMsg, data.cameras)
							]))
					]))
			]));
};
var $rtfeldman$elm_css$Css$flexEnd = $rtfeldman$elm_css$Css$prop1('flex-end');
var $rtfeldman$elm_css$Html$Styled$h1 = $rtfeldman$elm_css$Html$Styled$node('h1');
var $rtfeldman$elm_css$Css$hidden = {borderStyle: $rtfeldman$elm_css$Css$Structure$Compatible, overflow: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'hidden', visibility: $rtfeldman$elm_css$Css$Structure$Compatible};
var $rtfeldman$elm_css$Css$paddingRight = $rtfeldman$elm_css$Css$prop1('padding-right');
var $rtfeldman$elm_css$Css$paddingTop = $rtfeldman$elm_css$Css$prop1('padding-top');
var $elm$core$String$trim = _String_trim;
var $rtfeldman$elm_css$Css$VhUnits = {$: 'VhUnits'};
var $rtfeldman$elm_css$Css$vh = A2($rtfeldman$elm_css$Css$Internal$lengthConverter, $rtfeldman$elm_css$Css$VhUnits, 'vh');
var $author$project$View$startingGrid = F5(
	function (theme, prefix, leftTrack, middleCell, rightTrack) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							A2($rtfeldman$elm_css$Css$property, 'display', 'grid'),
							A2($rtfeldman$elm_css$Css$property, 'grid-template-columns', '20em minmax(50em, auto) minmax(10em, 20em)'),
							A2($rtfeldman$elm_css$Css$property, 'grid-template-rows', '4em auto'),
							$rtfeldman$elm_css$Css$height(
							$rtfeldman$elm_css$Css$vh(100)),
							$rtfeldman$elm_css$Css$minHeight(
							$rtfeldman$elm_css$Css$em(40))
						]))
				]),
			_List_fromArray(
				[
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_Nil,
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Html$Styled$h1,
							_List_fromArray(
								[
									$rtfeldman$elm_css$Html$Styled$Attributes$css(
									_List_fromArray(
										[
											A2($rtfeldman$elm_css$Css$property, 'grid-column', '2'),
											A2($rtfeldman$elm_css$Css$property, 'grid-row', '1'),
											$rtfeldman$elm_css$Css$marginTop(
											$rtfeldman$elm_css$Css$em(0)),
											$rtfeldman$elm_css$Css$paddingTop(
											$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
											$rtfeldman$elm_css$Css$textAlign($rtfeldman$elm_css$Css$center)
										]))
								]),
							_List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Html$Styled$span,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$Attributes$css(
											_List_fromArray(
												[
													$rtfeldman$elm_css$Css$color(theme.textBigPassive)
												]))
										]),
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$text(
											$elm$core$String$trim(prefix) + ' ')
										])),
									A2(
									$rtfeldman$elm_css$Html$Styled$span,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$Attributes$css(
											_List_fromArray(
												[
													$rtfeldman$elm_css$Css$color(theme.primaryLight)
												]))
										]),
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$text('mechane')
										]))
								]))
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									A2($rtfeldman$elm_css$Css$property, 'grid-column', '1'),
									A2($rtfeldman$elm_css$Css$property, 'grid-row', '1 / 3')
								]))
						]),
					_List_fromArray(
						[leftTrack])),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									A2($rtfeldman$elm_css$Css$property, 'grid-column', '2'),
									A2($rtfeldman$elm_css$Css$property, 'grid-row', '2')
								]))
						]),
					_List_fromArray(
						[middleCell])),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									A2($rtfeldman$elm_css$Css$property, 'grid-column', '3'),
									A2($rtfeldman$elm_css$Css$property, 'grid-row', '1 / 3'),
									$rtfeldman$elm_css$Css$alignItems($rtfeldman$elm_css$Css$flexEnd),
									$rtfeldman$elm_css$Css$displayFlex,
									$rtfeldman$elm_css$Css$justifyContent($rtfeldman$elm_css$Css$flexEnd),
									$rtfeldman$elm_css$Css$paddingRight(
									$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
									$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$relative),
									$rtfeldman$elm_css$Css$overflow($rtfeldman$elm_css$Css$hidden)
								]))
						]),
					_List_fromArray(
						[rightTrack]))
				]));
	});
var $author$project$View$startingTasks = _List_fromArray(
	['Awaiting static data', 'Awaiting shutterbug', 'Opening WebSockets', 'Awaiting dynamic data', 'Selecting cameras', 'Awaiting shutterbug pixels']);
var $rtfeldman$elm_css$Css$lineHeight = $rtfeldman$elm_css$Css$prop1('line-height');
var $rtfeldman$elm_css$Css$listStyleType = $rtfeldman$elm_css$Css$prop1('list-style-type');
var $rtfeldman$elm_css$Html$Styled$ol = $rtfeldman$elm_css$Html$Styled$node('ol');
var $author$project$View$tickedList = F2(
	function (theme, items) {
		var height = 2.5;
		var tickedItem = function (item) {
			return A2(
				$rtfeldman$elm_css$Html$Styled$li,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$color(theme.textPassive),
								$rtfeldman$elm_css$Css$height(
								$rtfeldman$elm_css$Css$em(height)),
								$rtfeldman$elm_css$Css$lineHeight(
								$rtfeldman$elm_css$Css$em(height))
							]))
					]),
				_List_fromArray(
					[
						A2($author$project$View$buttonIcon, theme, 'check_circle'),
						A2(
						$rtfeldman$elm_css$Html$Styled$span,
						_List_fromArray(
							[
								$rtfeldman$elm_css$Html$Styled$Attributes$css(
								_List_fromArray(
									[
										$rtfeldman$elm_css$Css$paddingLeft(
										$rtfeldman$elm_css$Css$em(theme.buttonPadding))
									]))
							]),
						_List_fromArray(
							[
								$rtfeldman$elm_css$Html$Styled$text(item)
							]))
					]));
		};
		var unTickedItem = function (item) {
			return A2(
				$rtfeldman$elm_css$Html$Styled$li,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$height(
								$rtfeldman$elm_css$Css$em(height)),
								$rtfeldman$elm_css$Css$lineHeight(
								$rtfeldman$elm_css$Css$em(height)),
								$rtfeldman$elm_css$Css$animationName(
								$rtfeldman$elm_css$Css$Animations$keyframes(
									_List_fromArray(
										[
											_Utils_Tuple2(
											0,
											_List_fromArray(
												[
													A2($rtfeldman$elm_css$Css$Animations$property, 'color', 'black')
												])),
											_Utils_Tuple2(
											50,
											_List_fromArray(
												[
													A2($rtfeldman$elm_css$Css$Animations$property, 'color', 'gray')
												])),
											_Utils_Tuple2(
											100,
											_List_fromArray(
												[
													A2($rtfeldman$elm_css$Css$Animations$property, 'color', 'black')
												]))
										]))),
								$rtfeldman$elm_css$Css$animationDuration(
								$rtfeldman$elm_css$Css$ms(1000)),
								$rtfeldman$elm_css$Css$animationIterationCount($rtfeldman$elm_css$Css$infinite)
							]))
					]),
				_List_fromArray(
					[
						A2($author$project$View$buttonIcon, theme, 'pending'),
						A2(
						$rtfeldman$elm_css$Html$Styled$span,
						_List_fromArray(
							[
								$rtfeldman$elm_css$Html$Styled$Attributes$css(
								_List_fromArray(
									[
										$rtfeldman$elm_css$Css$paddingLeft(
										$rtfeldman$elm_css$Css$em(theme.buttonPadding))
									]))
							]),
						_List_fromArray(
							[
								$rtfeldman$elm_css$Html$Styled$text(item)
							]))
					]));
		};
		return A2(
			$rtfeldman$elm_css$Html$Styled$ol,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$listStyleType($rtfeldman$elm_css$Css$none),
							$rtfeldman$elm_css$Css$minWidth(
							$rtfeldman$elm_css$Css$em(20)),
							$rtfeldman$elm_css$Css$marginLeft(
							$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
							$rtfeldman$elm_css$Css$marginRight(
							$rtfeldman$elm_css$Css$em(theme.buttonPadding))
						]))
				]),
			function () {
				if (!items.b) {
					return _List_Nil;
				} else {
					if (!items.b.b) {
						var x = items.a;
						return _List_fromArray(
							[
								unTickedItem(x)
							]);
					} else {
						var unTicked = A2(
							$elm$core$List$take,
							1,
							$elm$core$List$reverse(items));
						var ticked = A2(
							$elm$core$List$take,
							$elm$core$List$length(items) - 1,
							items);
						return _Utils_ap(
							A2($elm$core$List$map, tickedItem, ticked),
							A2($elm$core$List$map, unTickedItem, unTicked));
					}
				}
			}());
	});
var $rtfeldman$elm_css$Html$Styled$br = $rtfeldman$elm_css$Html$Styled$node('br');
var $rtfeldman$elm_css$Css$dotted = {borderStyle: $rtfeldman$elm_css$Css$Structure$Compatible, textDecorationStyle: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'dotted'};
var $rtfeldman$elm_css$Css$maxWidth = $rtfeldman$elm_css$Css$prop1('max-width');
var $author$project$Theme$errorContainerStyles = function (theme) {
	return _List_fromArray(
		[
			A3(
			$rtfeldman$elm_css$Css$border3,
			$rtfeldman$elm_css$Css$em(0.3),
			$rtfeldman$elm_css$Css$dotted,
			theme.primaryLight),
			$rtfeldman$elm_css$Css$maxWidth(
			$rtfeldman$elm_css$Css$em(70)),
			$rtfeldman$elm_css$Css$marginLeft($rtfeldman$elm_css$Css$auto),
			$rtfeldman$elm_css$Css$marginRight($rtfeldman$elm_css$Css$auto)
		]);
};
var $elm$core$List$intersperse = F2(
	function (sep, xs) {
		if (!xs.b) {
			return _List_Nil;
		} else {
			var hd = xs.a;
			var tl = xs.b;
			var step = F2(
				function (x, rest) {
					return A2(
						$elm$core$List$cons,
						sep,
						A2($elm$core$List$cons, x, rest));
				});
			var spersed = A3($elm$core$List$foldr, step, _List_Nil, tl);
			return A2($elm$core$List$cons, hd, spersed);
		}
	});
var $author$project$View$errorsList = F2(
	function (theme, errors) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$ul,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_Utils_ap(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$marginTop(
								$rtfeldman$elm_css$Css$em(2 * theme.buttonPadding)),
								$rtfeldman$elm_css$Css$overflow($rtfeldman$elm_css$Css$auto),
								$rtfeldman$elm_css$Css$textAlign($rtfeldman$elm_css$Css$left)
							]),
						$author$project$Theme$errorContainerStyles(theme)))
				]),
			A2(
				$elm$core$List$map,
				function (err) {
					return A2(
						$rtfeldman$elm_css$Html$Styled$li,
						_List_Nil,
						_List_fromArray(
							[
								A2(
								$rtfeldman$elm_css$Html$Styled$p,
								_List_Nil,
								A2(
									$elm$core$List$intersperse,
									A2($rtfeldman$elm_css$Html$Styled$br, _List_Nil, _List_Nil),
									A2(
										$elm$core$List$map,
										function (line) {
											return A2(
												$rtfeldman$elm_css$Html$Styled$span,
												_List_Nil,
												_List_fromArray(
													[
														$rtfeldman$elm_css$Html$Styled$text(line)
													]));
										},
										err)))
							]));
				},
				errors));
	});
var $rtfeldman$elm_css$Html$Styled$h2 = $rtfeldman$elm_css$Html$Styled$node('h2');
var $rtfeldman$elm_css$Html$Styled$h3 = $rtfeldman$elm_css$Html$Styled$node('h3');
var $author$project$Theme$startingGridMiddleDivStyle = _List_fromArray(
	[
		$rtfeldman$elm_css$Css$height(
		$rtfeldman$elm_css$Css$pct(100)),
		$rtfeldman$elm_css$Css$marginTop(
		$rtfeldman$elm_css$Css$em(0)),
		$rtfeldman$elm_css$Css$overflow($rtfeldman$elm_css$Css$auto),
		$rtfeldman$elm_css$Css$paddingTop(
		$rtfeldman$elm_css$Css$em(0)),
		$rtfeldman$elm_css$Css$textAlign($rtfeldman$elm_css$Css$center)
	]);
var $author$project$Main$viewErrored = F2(
	function (theme, errors) {
		return function (middle) {
			return A5(
				$author$project$View$startingGrid,
				theme,
				'toodle-oo from',
				A2($rtfeldman$elm_css$Html$Styled$div, _List_Nil, _List_Nil),
				middle,
				A2($rtfeldman$elm_css$Html$Styled$div, _List_Nil, _List_Nil));
		}(
			A2(
				$rtfeldman$elm_css$Html$Styled$div,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css($author$project$Theme$startingGridMiddleDivStyle)
					]),
				function () {
					if (!errors.b) {
						return _List_fromArray(
							[
								A2(
								$rtfeldman$elm_css$Html$Styled$h2,
								_List_Nil,
								_List_fromArray(
									[
										$rtfeldman$elm_css$Html$Styled$text('Error')
									])),
								A2(
								$author$project$View$errorsList,
								theme,
								_List_fromArray(
									[
										_List_fromArray(
										['empty errors list'])
									]))
							]);
					} else {
						if (!errors.b.b) {
							var x = errors.a;
							return _List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Html$Styled$h2,
									_List_Nil,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$text('Error')
										])),
									A2(
									$author$project$View$errorsList,
									theme,
									_List_fromArray(
										[x]))
								]);
						} else {
							var x = errors.a;
							var xs = errors.b;
							return _List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Html$Styled$h2,
									_List_Nil,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$text('Error')
										])),
									A2(
									$rtfeldman$elm_css$Html$Styled$h3,
									_List_Nil,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$text('Main error')
										])),
									A2(
									$author$project$View$errorsList,
									theme,
									_List_fromArray(
										[x])),
									A2(
									$rtfeldman$elm_css$Html$Styled$h3,
									_List_Nil,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$text('Subsequent errors')
										])),
									A2($author$project$View$errorsList, theme, xs)
								]);
						}
					}
				}()));
	});
var $author$project$PixScreen$backgroundColour = 'white';
var $rtfeldman$elm_css$Css$alignSelf = function (fn) {
	return A3(
		$rtfeldman$elm_css$Css$Internal$getOverloadedProperty,
		'alignSelf',
		'align-self',
		fn($rtfeldman$elm_css$Css$Internal$lengthForOverloadedProperty));
};
var $rtfeldman$elm_css$Css$flex = $rtfeldman$elm_css$Css$prop1('flex');
var $rtfeldman$elm_css$Css$UnitlessInteger = {$: 'UnitlessInteger'};
var $rtfeldman$elm_css$Css$int = function (val) {
	return {
		fontWeight: $rtfeldman$elm_css$Css$Structure$Compatible,
		intOrAuto: $rtfeldman$elm_css$Css$Structure$Compatible,
		lengthOrNumber: $rtfeldman$elm_css$Css$Structure$Compatible,
		lengthOrNumberOrAutoOrNoneOrContent: $rtfeldman$elm_css$Css$Structure$Compatible,
		number: $rtfeldman$elm_css$Css$Structure$Compatible,
		numberOrInfinite: $rtfeldman$elm_css$Css$Structure$Compatible,
		numericValue: val,
		unitLabel: '',
		units: $rtfeldman$elm_css$Css$UnitlessInteger,
		value: $elm$core$String$fromInt(val)
	};
};
var $rtfeldman$elm_css$Css$zIndex = $rtfeldman$elm_css$Css$prop1('z-index');
var $author$project$View$mainColumns = F3(
	function (col1, col2, col3) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$height(
							$rtfeldman$elm_css$Css$vh(100)),
							$rtfeldman$elm_css$Css$displayFlex,
							$rtfeldman$elm_css$Css$alignItems($rtfeldman$elm_css$Css$center),
							$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$relative)
						]))
				]),
			_List_fromArray(
				[
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									$rtfeldman$elm_css$Css$flex(
									$rtfeldman$elm_css$Css$int(1))
								]))
						]),
					_List_fromArray(
						[col1])),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									$rtfeldman$elm_css$Css$flex(
									$rtfeldman$elm_css$Css$int(0))
								]))
						]),
					_List_fromArray(
						[col2])),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									$rtfeldman$elm_css$Css$alignSelf($rtfeldman$elm_css$Css$flexEnd),
									$rtfeldman$elm_css$Css$flex(
									$rtfeldman$elm_css$Css$int(1)),
									$rtfeldman$elm_css$Css$height(
									$rtfeldman$elm_css$Css$pct(100)),
									$rtfeldman$elm_css$Css$zIndex(
									$rtfeldman$elm_css$Css$int(1))
								]))
						]),
					_List_fromArray(
						[col3]))
				]));
	});
var $author$project$Theme$bottomMargin = function (theme) {
	return $rtfeldman$elm_css$Css$marginBottom(
		$rtfeldman$elm_css$Css$em(theme.buttonPadding));
};
var $author$project$Theme$controlColumnDivStyle = function (theme) {
	return _List_fromArray(
		[
			$rtfeldman$elm_css$Css$alignItems($rtfeldman$elm_css$Css$flexEnd),
			$rtfeldman$elm_css$Css$displayFlex,
			$rtfeldman$elm_css$Css$flexDirection($rtfeldman$elm_css$Css$column),
			$rtfeldman$elm_css$Css$justifyContent($rtfeldman$elm_css$Css$center),
			$rtfeldman$elm_css$Css$minWidth(
			$rtfeldman$elm_css$Css$em(theme.buttonHeight)),
			$rtfeldman$elm_css$Css$paddingLeft(
			$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
			$rtfeldman$elm_css$Css$paddingRight(
			$rtfeldman$elm_css$Css$em(theme.buttonPadding))
		]);
};
var $author$project$LifeCycle$normalReasons = _List_fromArray(
	[
		_Utils_Tuple2('received client request', 'Mechane was shut down by a client.\n'),
		_Utils_Tuple2('intercepted terminal interrupt', 'Mechane was shut down from a terminal.\n')
	]);
var $author$project$LifeCycle$friendlyReason = function (terseReason) {
	var _v0 = A3(
		$elm$core$List$foldl,
		F2(
			function (_v1, acc) {
				var terse = _v1.a;
				var friendly = _v1.b;
				return _Utils_eq(terse, terseReason) ? friendly : acc;
			}),
		'',
		$author$project$LifeCycle$normalReasons);
	if (_v0 === '') {
		return $elm$core$Maybe$Nothing;
	} else {
		var friendly = _v0;
		return $elm$core$Maybe$Just(friendly);
	}
};
var $author$project$LifeCycle$explainWSCloseEvent = function (notification) {
	var _v0 = $author$project$LifeCycle$friendlyReason(notification.reason);
	if (_v0.$ === 'Just') {
		var explanation = _v0.a;
		return 'No connection.\n' + explanation;
	} else {
		return 'No connection.\n' + ((($elm$core$String$length(notification.reason) > 0) ? ('Websocket close reason: ' + (notification.reason + '.\n')) : '') + ('Websocket close code: ' + ($elm$core$String$fromInt(notification.code) + ('.\n' + ('Websocket path: ' + (notification.sourceSocket + '.\n'))))));
	}
};
var $author$project$Theme$kaputButtonStyles = function (theme) {
	return A2(
		$elm$core$List$cons,
		A2($rtfeldman$elm_css$Css$property, 'filter', 'blur(0.08em)'),
		function ($) {
			return $.inactive;
		}(
			$author$project$Theme$button(theme)));
};
var $author$project$Main$viewKaputControlColumn = F2(
	function (theme, notification) {
		var kaputButtonStyles = A2(
			$elm$core$List$cons,
			$author$project$Theme$bottomMargin(theme),
			$author$project$Theme$kaputButtonStyles(theme));
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					$author$project$Theme$controlColumnDivStyle(theme))
				]),
			_List_fromArray(
				[
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(kaputButtonStyles),
							$rtfeldman$elm_css$Html$Styled$Attributes$title(
							$author$project$LifeCycle$explainWSCloseEvent(notification))
						]),
					_List_fromArray(
						[
							A2($author$project$View$buttonIcon, theme, 'videocam_off')
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$button,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(kaputButtonStyles),
							$rtfeldman$elm_css$Html$Styled$Attributes$title(
							$author$project$LifeCycle$explainWSCloseEvent(notification))
						]),
					_List_fromArray(
						[
							A2($author$project$View$buttonIcon, theme, 'play_arrow')
						]))
				]));
	});
var $author$project$Main$Identity = {$: 'Identity'};
var $author$project$Main$IssuedUICommand = function (a) {
	return {$: 'IssuedUICommand', a: a};
};
var $author$project$Devoir$Shutdown = {$: 'Shutdown'};
var $author$project$Main$ToggledInfoPopUp = {$: 'ToggledInfoPopUp'};
var $author$project$Main$ToggledSettingsPopUp = {$: 'ToggledSettingsPopUp'};
var $author$project$PopUp$anyPixelsArea = function (model) {
	var _v0 = model.pixelsArea;
	if (_v0.$ === 'Just') {
		return true;
	} else {
		return false;
	}
};
var $author$project$Theme$buttonWithMargin = function (theme) {
	return {
		inactive: A2(
			$elm$core$List$cons,
			$author$project$Theme$bottomMargin(theme),
			function ($) {
				return $.inactive;
			}(
				$author$project$Theme$button(theme))),
		standard: A2(
			$elm$core$List$cons,
			$author$project$Theme$bottomMargin(theme),
			function ($) {
				return $.standard;
			}(
				$author$project$Theme$button(theme))),
		unselected: A2(
			$elm$core$List$cons,
			$author$project$Theme$bottomMargin(theme),
			function ($) {
				return $.unselected;
			}(
				$author$project$Theme$button(theme)))
	};
};
var $rtfeldman$elm_css$Css$absolute = {position: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'absolute'};
var $rtfeldman$elm_css$Css$angleConverter = F2(
	function (suffix, angleVal) {
		return {
			angle: $rtfeldman$elm_css$Css$Structure$Compatible,
			angleOrDirection: $rtfeldman$elm_css$Css$Structure$Compatible,
			value: _Utils_ap(
				$elm$core$String$fromFloat(angleVal),
				suffix)
		};
	});
var $rtfeldman$elm_css$Css$deg = $rtfeldman$elm_css$Css$angleConverter('deg');
var $rtfeldman$elm_css$Css$rotate = function (_v0) {
	var value = _v0.value;
	return {
		transform: $rtfeldman$elm_css$Css$Structure$Compatible,
		value: A2(
			$rtfeldman$elm_css$Css$cssFunction,
			'rotate',
			_List_fromArray(
				[value]))
	};
};
var $rtfeldman$elm_css$Css$valuesOrNone = function (list) {
	return $elm$core$List$isEmpty(list) ? {value: 'none'} : {
		value: A3(
			$rtfeldman$elm_css$Css$String$mapJoin,
			function ($) {
				return $.value;
			},
			' ',
			list)
	};
};
var $rtfeldman$elm_css$Css$transforms = A2(
	$elm$core$Basics$composeL,
	$rtfeldman$elm_css$Css$prop1('transform'),
	$rtfeldman$elm_css$Css$valuesOrNone);
var $rtfeldman$elm_css$Css$translate2 = F2(
	function (tx, ty) {
		return {
			transform: $rtfeldman$elm_css$Css$Structure$Compatible,
			value: A2(
				$rtfeldman$elm_css$Css$cssFunction,
				'translate',
				_List_fromArray(
					[tx.value, ty.value]))
		};
	});
var $author$project$View$expanderButtonIcon = F4(
	function (theme, name, popUp, any) {
		var rotationAngle = $rtfeldman$elm_css$Css$deg(
			A3($author$project$My$Extra$ternary, popUp, 135, -45));
		return A2(
			$rtfeldman$elm_css$Html$Styled$span,
			_List_Nil,
			_List_fromArray(
				[
					A2($author$project$View$buttonIcon, theme, name),
					A2(
					$rtfeldman$elm_css$Html$Styled$span,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$class('material-icons'),
							$rtfeldman$elm_css$Html$Styled$Attributes$class($author$project$Theme$expanderIconClass),
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									$rtfeldman$elm_css$Css$fontSize(
									$rtfeldman$elm_css$Css$em(theme.buttonIconFontSize)),
									$rtfeldman$elm_css$Css$color(theme.accent),
									$rtfeldman$elm_css$Css$opacity(
									A3(
										$author$project$My$Extra$ternary,
										popUp,
										$rtfeldman$elm_css$Css$num(1),
										$rtfeldman$elm_css$Css$num(0))),
									A2($rtfeldman$elm_css$Css$property, 'filter', 'drop-shadow(0px 0px 3px white)'),
									$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$absolute),
									$rtfeldman$elm_css$Css$transforms(
									_List_fromArray(
										[
											A2(
											$rtfeldman$elm_css$Css$translate2,
											$rtfeldman$elm_css$Css$em(-(theme.buttonIconFontSize - 0.2)),
											$rtfeldman$elm_css$Css$em(-((theme.buttonIconFontSize / 2) - 0.2))),
											$rtfeldman$elm_css$Css$rotate(rotationAngle)
										])),
									$rtfeldman$elm_css$Css$Transitions$transition(
									_List_fromArray(
										[
											$rtfeldman$elm_css$Css$Transitions$transform($author$project$Theme$popUpDuration),
											$rtfeldman$elm_css$Css$Transitions$opacity(
											A3($author$project$My$Extra$ternary, any, 1.5 * $author$project$Theme$popUpDuration, 400))
										])),
									A2($rtfeldman$elm_css$Css$property, 'user-select', 'none'),
									$rtfeldman$elm_css$Css$zIndex(
									$rtfeldman$elm_css$Css$int(1))
								]))
						]),
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text('expand_circle_down')
						]))
				]));
	});
var $author$project$PopUp$info = function (model) {
	var _v0 = model.pixelsArea;
	if ((_v0.$ === 'Just') && (_v0.a.$ === 'InfoPopUp')) {
		var _v1 = _v0.a;
		return true;
	} else {
		return false;
	}
};
var $author$project$My$Html$onKeydownPreventDefault = function (msg) {
	return A2($author$project$My$Html$onEventPreventDefault, 'keydown', msg);
};
var $author$project$My$Html$safeClickButton = F4(
	function (action, inaction, attributes, children) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$button,
			_Utils_ap(
				attributes,
				_List_fromArray(
					[
						$author$project$My$Html$onClickPreventDefault(action),
						$author$project$My$Html$onKeydownPreventDefault(inaction)
					])),
			children);
	});
var $author$project$PopUp$shutterbug = function (model) {
	return model.shutterbugActions;
};
var $author$project$Main$OpenedNewTab = function (a) {
	return {$: 'OpenedNewTab', a: a};
};
var $author$project$Main$ToggledShutterbugPopUp = {$: 'ToggledShutterbugPopUp'};
var $author$project$Main$WroteShutterbugToClipboard = {$: 'WroteShutterbugToClipboard'};
var $rtfeldman$elm_css$Css$borderTopLeftRadius = $rtfeldman$elm_css$Css$prop1('border-top-left-radius');
var $author$project$LifeCycle$isNormalWSCloseEvent = function (notification) {
	return A2(
		$elm$core$List$member,
		notification.reason,
		A2(
			$elm$core$List$map,
			function (_v0) {
				var terse = _v0.a;
				return terse;
			},
			$author$project$LifeCycle$normalReasons));
};
var $author$project$View$Shutterbug$copyButton = F4(
	function (theme, msg, inactionMsg, maybeNotification) {
		var useless = '( ⚠ It might not be useful now the server is shut down.)';
		var tooltip = 'Copy this tab\'s shutterbug to the clipboard.' + function () {
			if (maybeNotification.$ === 'Just') {
				var notification = maybeNotification.a;
				return $author$project$LifeCycle$isNormalWSCloseEvent(notification) ? ('\n' + useless) : '';
			} else {
				return '';
			}
		}();
		return A4(
			$author$project$My$Html$safeClickButton,
			msg,
			inactionMsg,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					$author$project$Theme$buttonWithMargin(theme).standard),
					$rtfeldman$elm_css$Html$Styled$Attributes$title(tooltip)
				]),
			_List_fromArray(
				[
					A2($author$project$View$buttonIcon, theme, 'content_copy')
				]));
	});
var $author$project$View$Shutterbug$openTabButton = function (data) {
	return function () {
		var _v0 = data.maybeNotification;
		if (_v0.$ === 'Just') {
			var notification = _v0.a;
			return $rtfeldman$elm_css$Html$Styled$button(
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						A2(
							$elm$core$List$cons,
							$author$project$Theme$bottomMargin(data.theme),
							$author$project$Theme$kaputButtonStyles(data.theme))),
						$rtfeldman$elm_css$Html$Styled$Attributes$title(
						$author$project$LifeCycle$explainWSCloseEvent(notification))
					]));
		} else {
			return A3(
				$author$project$My$Html$safeClickButton,
				data.tabMsg,
				data.inactionMsg,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						$author$project$Theme$buttonWithMargin(data.theme).standard),
						$rtfeldman$elm_css$Html$Styled$Attributes$title('Open a new tab which can use an existing shutterbug.')
					]));
		}
	}()(
		_List_fromArray(
			[
				A2($author$project$View$buttonIcon, data.theme, 'open_in_browser')
			]));
};
var $rtfeldman$elm_css$Css$paddingBottom = $rtfeldman$elm_css$Css$prop1('padding-bottom');
var $author$project$Theme$popUpDurationMS = $elm$core$String$fromFloat($author$project$Theme$popUpDuration) + 'ms';
var $rtfeldman$elm_css$Css$translateX = function (_v0) {
	var value = _v0.value;
	return {
		transform: $rtfeldman$elm_css$Css$Structure$Compatible,
		value: A2(
			$rtfeldman$elm_css$Css$cssFunction,
			'translateX',
			_List_fromArray(
				[value]))
	};
};
var $rtfeldman$elm_css$Css$translateY = function (_v0) {
	var value = _v0.value;
	return {
		transform: $rtfeldman$elm_css$Css$Structure$Compatible,
		value: A2(
			$rtfeldman$elm_css$Css$cssFunction,
			'translateY',
			_List_fromArray(
				[value]))
	};
};
var $author$project$Theme$white = A3($rtfeldman$elm_css$Css$rgb, 255, 255, 255);
var $author$project$View$Shutterbug$popUp = function (data) {
	return A2(
		$rtfeldman$elm_css$Html$Styled$div,
		_List_fromArray(
			[
				$rtfeldman$elm_css$Html$Styled$Attributes$css(
				_List_fromArray(
					[
						$rtfeldman$elm_css$Css$backgroundColor($author$project$Theme$white),
						$rtfeldman$elm_css$Css$borderTopLeftRadius(
						$rtfeldman$elm_css$Css$em(data.theme.buttonBorderRadius)),
						$rtfeldman$elm_css$Css$displayFlex,
						$rtfeldman$elm_css$Css$flexDirection($rtfeldman$elm_css$Css$column),
						$rtfeldman$elm_css$Css$paddingTop(
						$rtfeldman$elm_css$Css$em(data.theme.buttonPadding)),
						$rtfeldman$elm_css$Css$paddingBottom(
						$rtfeldman$elm_css$Css$em(data.theme.buttonPadding)),
						$rtfeldman$elm_css$Css$paddingLeft(
						$rtfeldman$elm_css$Css$em(data.theme.buttonPadding)),
						$rtfeldman$elm_css$Css$paddingRight(
						$rtfeldman$elm_css$Css$px(2)),
						$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$absolute),
						$rtfeldman$elm_css$Css$transforms(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$translateX(
								$rtfeldman$elm_css$Css$pct(-100)),
								$rtfeldman$elm_css$Css$translateX(
								$rtfeldman$elm_css$Css$px(2)),
								$rtfeldman$elm_css$Css$translateX(
								$rtfeldman$elm_css$Css$em(-data.theme.buttonPadding)),
								$rtfeldman$elm_css$Css$translateY(
								$rtfeldman$elm_css$Css$pct(
									A3($author$project$My$Extra$ternary, data.shutterbugActionsPopUp, -50, 51)))
							])),
						A2($rtfeldman$elm_css$Css$property, 'transition', 'transform ' + $author$project$Theme$popUpDurationMS),
						A2(
						$rtfeldman$elm_css$Css$property,
						'user-select',
						A3($author$project$My$Extra$ternary, data.shutterbugActionsPopUp, 'auto', 'none'))
					]))
			]),
		_List_fromArray(
			[
				$author$project$View$Shutterbug$openTabButton(data),
				A4($author$project$View$Shutterbug$copyButton, data.theme, data.copyMsg, data.inactionMsg, data.maybeNotification)
			]));
};
var $author$project$View$Shutterbug$actions = function (data) {
	return A2(
		$rtfeldman$elm_css$Html$Styled$div,
		_List_Nil,
		_List_fromArray(
			[
				$author$project$View$Shutterbug$popUp(data),
				A4(
				$author$project$My$Html$safeClickButton,
				data.togglePopUpMsg,
				data.inactionMsg,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						$author$project$Theme$buttonWithMargin(data.theme).standard),
						$rtfeldman$elm_css$Html$Styled$Attributes$title('Access shutterbug actions.')
					]),
				_List_fromArray(
					[
						A4($author$project$View$expanderButtonIcon, data.theme, 'camera', data.shutterbugActionsPopUp, false)
					]))
			]));
};
var $author$project$Main$viewShutterbugActions = F3(
	function (theme, maybeNotification, data) {
		return $author$project$View$Shutterbug$actions(
			{
				copyMsg: $author$project$Main$WroteShutterbugToClipboard,
				extantUrl: data.extantUrl,
				inactionMsg: $author$project$Main$Identity,
				maybeNotification: maybeNotification,
				shutterbugActionsPopUp: data.shutterbugActionsPopUp,
				tabMsg: $author$project$Main$OpenedNewTab(data.extantUrl),
				theme: theme,
				togglePopUpMsg: $author$project$Main$ToggledShutterbugPopUp
			});
	});
var $author$project$Main$viewMetaColumn = F3(
	function (theme, maybeNotification, data) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$alignItems($rtfeldman$elm_css$Css$flexEnd),
							$rtfeldman$elm_css$Css$displayFlex,
							$rtfeldman$elm_css$Css$flexDirection($rtfeldman$elm_css$Css$column),
							$rtfeldman$elm_css$Css$height(
							$rtfeldman$elm_css$Css$pct(100)),
							$rtfeldman$elm_css$Css$justifyContent($rtfeldman$elm_css$Css$flexEnd),
							A2($rtfeldman$elm_css$Css$property, 'overflow-y', 'clip'),
							$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$relative),
							$rtfeldman$elm_css$Css$paddingLeft(
							$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
							$rtfeldman$elm_css$Css$paddingRight(
							$rtfeldman$elm_css$Css$em(theme.buttonPadding))
						]))
				]),
			_List_fromArray(
				[
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									$rtfeldman$elm_css$Css$flex(
									$rtfeldman$elm_css$Css$int(1))
								]))
						]),
					function () {
						if (maybeNotification.$ === 'Just') {
							var notification = maybeNotification.a;
							return _List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Html$Styled$button,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$Attributes$css(
											_Utils_ap(
												_List_fromArray(
													[
														$author$project$Theme$bottomMargin(theme),
														$rtfeldman$elm_css$Css$marginTop(
														$rtfeldman$elm_css$Css$em(theme.buttonPadding))
													]),
												$author$project$Theme$kaputButtonStyles(theme))),
											$rtfeldman$elm_css$Html$Styled$Attributes$title(
											$author$project$LifeCycle$explainWSCloseEvent(notification))
										]),
									_List_fromArray(
										[
											A2($author$project$View$buttonIcon, theme, 'power_settings_new')
										]))
								]);
						} else {
							return _List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Html$Styled$button,
									_List_fromArray(
										[
											$author$project$My$Html$onClickPreventDefault(
											$author$project$Main$IssuedUICommand($author$project$Devoir$Shutdown)),
											$rtfeldman$elm_css$Html$Styled$Attributes$css(
											_Utils_ap(
												_List_fromArray(
													[
														$author$project$Theme$bottomMargin(theme),
														$rtfeldman$elm_css$Css$marginTop(
														$rtfeldman$elm_css$Css$em(theme.buttonPadding))
													]),
												function ($) {
													return $.standard;
												}(
													$author$project$Theme$button(theme)))),
											$rtfeldman$elm_css$Html$Styled$Attributes$title('Shut down Mechane.')
										]),
									_List_fromArray(
										[
											A2($author$project$View$buttonIcon, theme, 'power_settings_new')
										]))
								]);
						}
					}()),
					A4(
					$author$project$My$Html$safeClickButton,
					$author$project$Main$ToggledSettingsPopUp,
					$author$project$Main$Identity,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							function ($) {
								return $.standard;
							}(
								$author$project$Theme$buttonWithMargin(theme))),
							$rtfeldman$elm_css$Html$Styled$Attributes$title('Customise this tab\'s looks and behaviour.')
						]),
					_List_fromArray(
						[
							A4(
							$author$project$View$expanderButtonIcon,
							theme,
							'tab',
							$author$project$PopUp$settings(data.popUps),
							$author$project$PopUp$anyPixelsArea(data.popUps))
						])),
					A4(
					$author$project$My$Html$safeClickButton,
					$author$project$Main$ToggledInfoPopUp,
					$author$project$Main$Identity,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							function ($) {
								return $.standard;
							}(
								$author$project$Theme$buttonWithMargin(theme))),
							$rtfeldman$elm_css$Html$Styled$Attributes$title('Learn more about this Mechane world.')
						]),
					_List_fromArray(
						[
							A4(
							$author$project$View$expanderButtonIcon,
							theme,
							'public',
							$author$project$PopUp$info(data.popUps),
							$author$project$PopUp$anyPixelsArea(data.popUps))
						])),
					A3(
					$author$project$Main$viewShutterbugActions,
					theme,
					maybeNotification,
					{
						extantUrl: data.extantUrl,
						shutterbugActionsPopUp: $author$project$PopUp$shutterbug(data.popUps)
					})
				]));
	});
var $author$project$Main$ChangedInfoTab = function (a) {
	return {$: 'ChangedInfoTab', a: a};
};
var $author$project$Main$ClearedCameraSelections = {$: 'ClearedCameraSelections'};
var $author$project$Main$ToggledAutoCloseCameras = {$: 'ToggledAutoCloseCameras'};
var $author$project$Main$ToggledCameraSelectionMode = {$: 'ToggledCameraSelectionMode'};
var $author$project$Main$ToggledFullscreen = function (a) {
	return {$: 'ToggledFullscreen', a: a};
};
var $author$project$Main$ToggledOutputBorder = {$: 'ToggledOutputBorder'};
var $author$project$Main$ToggledOutputSize = {$: 'ToggledOutputSize'};
var $author$project$Main$ToggledPointerLock = function (a) {
	return {$: 'ToggledPointerLock', a: a};
};
var $author$project$Main$ToggledTextSelect = {$: 'ToggledTextSelect'};
var $author$project$Main$WroteTextToClipboard = function (a) {
	return {$: 'WroteTextToClipboard', a: a};
};
var $author$project$PixScreen$attributePrefix = 'shutterbug-pixels-';
var $author$project$PixScreen$attributes = {border: $author$project$PixScreen$attributePrefix + 'b', colour: $author$project$PixScreen$attributePrefix + 'c', height: $author$project$PixScreen$attributePrefix + 'h', kaput: $author$project$PixScreen$attributePrefix + 'k', selectable: $author$project$PixScreen$attributePrefix + 's', width: $author$project$PixScreen$attributePrefix + 'w'};
var $author$project$PixScreen$border = function (_v0) {
	var borderScreen = _v0.a.borderScreen;
	return borderScreen;
};
var $rtfeldman$elm_css$Css$Transitions$BoxShadow = {$: 'BoxShadow'};
var $rtfeldman$elm_css$Css$Transitions$boxShadow = $rtfeldman$elm_css$Css$Transitions$durationTransition($rtfeldman$elm_css$Css$Transitions$BoxShadow);
var $rtfeldman$elm_css$Css$prop6 = F7(
	function (key, argA, argB, argC, argD, argE, argF) {
		return A2($rtfeldman$elm_css$Css$property, key, argA.value + (' ' + (argB.value + (' ' + (argC.value + (' ' + (argD.value + (' ' + (argE.value + (' ' + argF.value))))))))));
	});
var $rtfeldman$elm_css$Css$boxShadow6 = $rtfeldman$elm_css$Css$prop6('box-shadow');
var $author$project$PixScreen$dimensions = function (_v0) {
	var w = _v0.a.w;
	var h = _v0.a.h;
	return {h: h, w: w};
};
var $author$project$PixScreen$fit = function (_v0) {
	var fitScreen = _v0.a.fitScreen;
	return fitScreen;
};
var $author$project$PixScreen$full = function (_v0) {
	var fullscreen = _v0.a.fullscreen;
	return fullscreen;
};
var $rtfeldman$elm_css$Css$pointerEvents = $rtfeldman$elm_css$Css$prop1('pointer-events');
var $rtfeldman$elm_css$Css$borderBottom3 = $rtfeldman$elm_css$Css$prop3('border-bottom');
var $rtfeldman$elm_css$Css$borderBottomRightRadius = $rtfeldman$elm_css$Css$prop1('border-bottom-right-radius');
var $rtfeldman$elm_css$Css$borderRight3 = $rtfeldman$elm_css$Css$prop3('border-right');
var $rtfeldman$elm_css$Css$borderTop3 = $rtfeldman$elm_css$Css$prop3('border-top');
var $rtfeldman$elm_css$Css$borderTopRightRadius = $rtfeldman$elm_css$Css$prop1('border-top-right-radius');
var $rtfeldman$elm_css$Css$padding4 = $rtfeldman$elm_css$Css$prop4('padding');
var $rtfeldman$elm_css$Svg$Styled$Attributes$d = $rtfeldman$elm_css$VirtualDom$Styled$attribute('d');
var $rtfeldman$elm_css$Svg$Styled$Attributes$fill = $rtfeldman$elm_css$VirtualDom$Styled$attribute('fill');
var $rtfeldman$elm_css$VirtualDom$Styled$NodeNS = F4(
	function (a, b, c, d) {
		return {$: 'NodeNS', a: a, b: b, c: c, d: d};
	});
var $rtfeldman$elm_css$VirtualDom$Styled$nodeNS = $rtfeldman$elm_css$VirtualDom$Styled$NodeNS;
var $rtfeldman$elm_css$Svg$Styled$node = $rtfeldman$elm_css$VirtualDom$Styled$nodeNS('http://www.w3.org/2000/svg');
var $rtfeldman$elm_css$Svg$Styled$path = $rtfeldman$elm_css$Svg$Styled$node('path');
var $rtfeldman$elm_css$Svg$Styled$svg = $rtfeldman$elm_css$Svg$Styled$node('svg');
var $rtfeldman$elm_css$Svg$Styled$Attributes$viewBox = $rtfeldman$elm_css$VirtualDom$Styled$attribute('viewBox');
var $rtfeldman$elm_css$Svg$Styled$Attributes$width = $rtfeldman$elm_css$VirtualDom$Styled$attribute('width');
var $author$project$View$buttonOffIcon = F2(
	function (theme, name) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$span,
			_List_Nil,
			_List_fromArray(
				[
					A2(
					$rtfeldman$elm_css$Html$Styled$span,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$class($author$project$View$defaultIconStyle),
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									$rtfeldman$elm_css$Css$color(theme.primary),
									$rtfeldman$elm_css$Css$fontSize(
									$rtfeldman$elm_css$Css$em(theme.buttonIconFontSize)),
									A2($rtfeldman$elm_css$Css$property, 'user-select', 'none'),
									$rtfeldman$elm_css$Css$verticalAlign($rtfeldman$elm_css$Css$middle),
									$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$absolute)
								]))
						]),
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Svg$Styled$svg,
							_List_fromArray(
								[
									$rtfeldman$elm_css$Svg$Styled$Attributes$fill(
									$author$project$Theme$colToString(theme.primary)),
									$rtfeldman$elm_css$Svg$Styled$Attributes$viewBox('0 0 24 24'),
									$rtfeldman$elm_css$Svg$Styled$Attributes$width('1em')
								]),
							_List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Svg$Styled$path,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Svg$Styled$Attributes$d('M2.81,2.81 L1.39,4.22 L19.77,22.6 L21.19,21.19z')
										]),
									_List_Nil),
									A2(
									$rtfeldman$elm_css$Svg$Styled$path,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Svg$Styled$Attributes$d('M2.81,2.81 L4.23,1.4 L22.6,19.77 L21.19,21.19z'),
											$rtfeldman$elm_css$Svg$Styled$Attributes$fill(
											$author$project$Theme$colToString(theme.buttonOpaqueBackground))
										]),
									_List_Nil)
								]))
						])),
					A2($author$project$View$buttonIcon, theme, name)
				]));
	});
var $author$project$View$Cameras$running = function (data) {
	return A2(
		$rtfeldman$elm_css$Html$Styled$div,
		_List_fromArray(
			[
				$rtfeldman$elm_css$Html$Styled$Attributes$css(
				_List_fromArray(
					[
						$rtfeldman$elm_css$Css$displayFlex,
						$rtfeldman$elm_css$Css$alignItems($rtfeldman$elm_css$Css$flexStart),
						A2($rtfeldman$elm_css$Css$property, 'user-select', 'none')
					]))
			]),
		_List_fromArray(
			[
				A5(
				$author$project$View$Cameras$viewSelectorForm,
				data.theme,
				data.toggleCamMsg,
				data.assocMsg,
				data.cameras,
				$elm$core$Maybe$Just(4)),
				A2(
				$rtfeldman$elm_css$Html$Styled$div,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						_List_fromArray(
							[
								$rtfeldman$elm_css$Css$alignItems($rtfeldman$elm_css$Css$flexStart),
								$rtfeldman$elm_css$Css$displayFlex,
								$rtfeldman$elm_css$Css$flexDirection($rtfeldman$elm_css$Css$column),
								A2(
								$rtfeldman$elm_css$Css$property,
								'gap',
								$elm$core$String$fromFloat(data.theme.buttonPadding) + 'em'),
								$rtfeldman$elm_css$Css$paddingLeft(
								$rtfeldman$elm_css$Css$em(data.theme.buttonPadding))
							]))
					]),
				_List_fromArray(
					[
						A2($author$project$View$Cameras$viewStitchingChooser, data.stitchMsg, data.cameras),
						A2(
						$rtfeldman$elm_css$Html$Styled$div,
						_List_fromArray(
							[
								$rtfeldman$elm_css$Html$Styled$Attributes$css(
								_List_fromArray(
									[
										$rtfeldman$elm_css$Css$displayFlex,
										A2(
										$rtfeldman$elm_css$Css$property,
										'gap',
										$elm$core$String$fromFloat(data.theme.buttonPadding) + 'em')
									]))
							]),
						_List_fromArray(
							[
								$author$project$Cameras$radio(data.cameras) ? A2(
								$rtfeldman$elm_css$Html$Styled$button,
								_List_fromArray(
									[
										$author$project$My$Html$onClickPreventDefault(data.toggleSelModeMsg),
										$rtfeldman$elm_css$Html$Styled$Attributes$css(
										$author$project$Theme$button(data.theme).standard),
										$rtfeldman$elm_css$Html$Styled$Attributes$title('Allow multiple camera selections.')
									]),
								_List_fromArray(
									[
										A2($author$project$View$buttonIcon, data.theme, 'radio_button_checked')
									])) : A2(
								$rtfeldman$elm_css$Html$Styled$button,
								_List_fromArray(
									[
										$author$project$My$Html$onClickPreventDefault(data.toggleSelModeMsg),
										$rtfeldman$elm_css$Html$Styled$Attributes$css(
										$author$project$Theme$button(data.theme).standard),
										$rtfeldman$elm_css$Html$Styled$Attributes$title('Disallow multiple camera selections.')
									]),
								_List_fromArray(
									[
										A2($author$project$View$buttonIcon, data.theme, 'check_box')
									])),
								(!$elm$core$List$length(
								$author$project$Cameras$selectedNames(data.cameras))) ? A2(
								$rtfeldman$elm_css$Html$Styled$div,
								_List_fromArray(
									[
										$rtfeldman$elm_css$Html$Styled$Attributes$css(
										$author$project$Theme$button(data.theme).inactive),
										$rtfeldman$elm_css$Html$Styled$Attributes$title('No camera selections to clear.')
									]),
								_List_fromArray(
									[
										A2($author$project$View$buttonIcon, data.theme, 'remove_done')
									])) : A2(
								$rtfeldman$elm_css$Html$Styled$button,
								_List_fromArray(
									[
										$author$project$My$Html$onClickPreventDefault(data.clearMsg),
										$rtfeldman$elm_css$Html$Styled$Attributes$css(
										$author$project$Theme$button(data.theme).standard),
										$rtfeldman$elm_css$Html$Styled$Attributes$title('Clear camera selections.')
									]),
								_List_fromArray(
									[
										A2($author$project$View$buttonIcon, data.theme, 'remove_done')
									])),
								data.autoClosePopUp ? A2(
								$rtfeldman$elm_css$Html$Styled$button,
								_List_fromArray(
									[
										$author$project$My$Html$onClickPreventDefault(data.autoMsg),
										$rtfeldman$elm_css$Html$Styled$Attributes$css(
										_Utils_ap(
											$author$project$Theme$button(data.theme).standard,
											_List_fromArray(
												[
													$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$relative)
												]))),
										$rtfeldman$elm_css$Html$Styled$Attributes$title('Pin the camera selector open.')
									]),
								_List_fromArray(
									[
										A2($author$project$View$buttonOffIcon, data.theme, 'push_pin')
									])) : A2(
								$rtfeldman$elm_css$Html$Styled$button,
								_List_fromArray(
									[
										$author$project$My$Html$onClickPreventDefault(data.autoMsg),
										$rtfeldman$elm_css$Html$Styled$Attributes$css(
										$author$project$Theme$button(data.theme).standard),
										$rtfeldman$elm_css$Html$Styled$Attributes$title('Don\'t pin the camera selector open.')
									]),
								_List_fromArray(
									[
										A2($author$project$View$buttonIcon, data.theme, 'push_pin')
									]))
							]))
					]))
			]));
};
var $rtfeldman$elm_css$Css$transform = function (only) {
	return $rtfeldman$elm_css$Css$transforms(
		_List_fromArray(
			[only]));
};
var $author$project$View$Cameras$popUp = function (data) {
	var gradientStopOnValue = '10%';
	var gradientStopOffValue = '100%';
	var gradientStop = '--cameras-pop-up-gradient-stop';
	return A2(
		$rtfeldman$elm_css$Html$Styled$div,
		_List_fromArray(
			[
				$rtfeldman$elm_css$Html$Styled$Attributes$css(
				_List_fromArray(
					[
						A2(
						$rtfeldman$elm_css$Css$property,
						'background',
						A2(
							$elm$core$String$join,
							', ',
							_List_fromArray(
								['linear-gradient(to right', 'rgba(255,255,255,1.0)', 'rgba(255,255,255,1.0) var(' + (gradientStop + ')'), 'rgba(255,255,255,0.8) calc(var(' + (gradientStop + ') + 5%)'), 'rgba(255,255,255,0.8))']))),
						A2(
						$rtfeldman$elm_css$Css$property,
						gradientStop,
						data.on ? gradientStopOnValue : gradientStopOffValue),
						A3(
						$rtfeldman$elm_css$Css$borderBottom3,
						$rtfeldman$elm_css$Css$em(data.theme.buttonBorder),
						$rtfeldman$elm_css$Css$solid,
						data.theme.primary),
						A3(
						$rtfeldman$elm_css$Css$borderRight3,
						$rtfeldman$elm_css$Css$em(data.theme.buttonBorder),
						$rtfeldman$elm_css$Css$solid,
						data.theme.primary),
						A3(
						$rtfeldman$elm_css$Css$borderTop3,
						$rtfeldman$elm_css$Css$em(data.theme.buttonBorder),
						$rtfeldman$elm_css$Css$solid,
						data.theme.primary),
						$rtfeldman$elm_css$Css$borderTopRightRadius(
						$rtfeldman$elm_css$Css$em(data.theme.buttonBorderRadius)),
						$rtfeldman$elm_css$Css$borderBottomRightRadius(
						$rtfeldman$elm_css$Css$em(data.theme.buttonBorderRadius)),
						data.on ? A5(
						$rtfeldman$elm_css$Css$boxShadow5,
						$rtfeldman$elm_css$Css$px(0),
						$rtfeldman$elm_css$Css$px(0),
						$rtfeldman$elm_css$Css$px(20),
						$rtfeldman$elm_css$Css$px(4),
						A3($rtfeldman$elm_css$Css$rgb, 112, 145, 145)) : A5(
						$rtfeldman$elm_css$Css$boxShadow5,
						$rtfeldman$elm_css$Css$px(0),
						$rtfeldman$elm_css$Css$px(0),
						$rtfeldman$elm_css$Css$px(0),
						$rtfeldman$elm_css$Css$px(0),
						A3($rtfeldman$elm_css$Css$rgb, 112, 145, 145)),
						A4(
						$rtfeldman$elm_css$Css$padding4,
						$rtfeldman$elm_css$Css$em(data.theme.buttonPadding),
						$rtfeldman$elm_css$Css$em(data.theme.buttonPadding),
						$rtfeldman$elm_css$Css$em(data.theme.buttonPadding),
						$rtfeldman$elm_css$Css$em(0)),
						$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$absolute),
						$rtfeldman$elm_css$Css$transform(
						A2(
							$rtfeldman$elm_css$Css$translate2,
							$rtfeldman$elm_css$Css$pct(
								data.on ? 0 : (-100.1)),
							$rtfeldman$elm_css$Css$pct(-50))),
						A2(
						$rtfeldman$elm_css$Css$property,
						'transition',
						A2(
							$elm$core$String$join,
							', ',
							_List_fromArray(
								['transform ' + $author$project$Theme$popUpDurationMS, 'box-shadow ' + $author$project$Theme$popUpDurationMS, gradientStop + (' ' + $author$project$Theme$popUpDurationMS)]))),
						A2(
						$rtfeldman$elm_css$Css$property,
						'user-select',
						data.on ? 'auto' : 'none')
					]))
			]),
		_List_fromArray(
			[
				$rtfeldman$elm_css$Css$Global$global(
				_List_fromArray(
					[
						A2(
						$rtfeldman$elm_css$Css$Global$selector,
						'@property ' + gradientStop,
						_List_fromArray(
							[
								A2($rtfeldman$elm_css$Css$property, 'syntax', '\'<percentage>\''),
								A2($rtfeldman$elm_css$Css$property, 'inherits', 'false'),
								A2($rtfeldman$elm_css$Css$property, 'initial-value', gradientStopOffValue)
							]))
					])),
				$author$project$View$Cameras$running(data)
			]));
};
var $rtfeldman$elm_css$Css$borderBottomLeftRadius = $rtfeldman$elm_css$Css$prop1('border-bottom-left-radius');
var $author$project$PixScreen$borderButton = F3(
	function (theme, msg, screen) {
		return $author$project$PixScreen$border(screen) ? A2(
			$rtfeldman$elm_css$Html$Styled$button,
			_List_fromArray(
				[
					$author$project$My$Html$onClickPreventDefault(msg),
					$rtfeldman$elm_css$Html$Styled$Attributes$title('Disable the border.'),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					function ($) {
						return $.standard;
					}(
						$author$project$Theme$buttonWithMargin(theme)))
				]),
			_List_fromArray(
				[
					A2($author$project$View$buttonIcon, theme, 'border_outer')
				])) : A2(
			$rtfeldman$elm_css$Html$Styled$button,
			_List_fromArray(
				[
					$author$project$My$Html$onClickPreventDefault(msg),
					$rtfeldman$elm_css$Html$Styled$Attributes$title('Enable the border.'),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					function ($) {
						return $.standard;
					}(
						$author$project$Theme$buttonWithMargin(theme)))
				]),
			_List_fromArray(
				[
					A2($author$project$View$buttonIcon, theme, 'border_clear')
				]));
	});
var $rtfeldman$elm_css$Css$borderLeft3 = $rtfeldman$elm_css$Css$prop3('border-left');
var $author$project$PixScreen$areaDescription = function (_v0) {
	var w = _v0.a.w;
	var h = _v0.a.h;
	return $elm$core$String$fromInt(w) + (' × ' + ($elm$core$String$fromInt(h) + ' pixels'));
};
var $author$project$PixScreen$fitScreenButton = F3(
	function (theme, msg, screen) {
		return $author$project$PixScreen$fit(screen) ? A2(
			$rtfeldman$elm_css$Html$Styled$button,
			_List_fromArray(
				[
					$author$project$My$Html$onClickPreventDefault(msg),
					$rtfeldman$elm_css$Html$Styled$Attributes$title(
					'Disable fit to window height.\n' + ('(The natural size is ' + ($author$project$PixScreen$areaDescription(screen) + '.)'))),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					function ($) {
						return $.standard;
					}(
						$author$project$Theme$buttonWithMargin(theme)))
				]),
			_List_fromArray(
				[
					A2($author$project$View$buttonIcon, theme, 'fit_screen')
				])) : A2(
			$rtfeldman$elm_css$Html$Styled$button,
			_List_fromArray(
				[
					$author$project$My$Html$onClickPreventDefault(msg),
					$rtfeldman$elm_css$Html$Styled$Attributes$title('Enable fit to window height.'),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					function ($) {
						return $.standard;
					}(
						$author$project$Theme$buttonWithMargin(theme)))
				]),
			_List_fromArray(
				[
					A2($author$project$View$buttonOffIcon, theme, 'fit_screen')
				]));
	});
var $author$project$PixScreen$fullscreenButton = F3(
	function (theme, msg, screen) {
		return $author$project$PixScreen$full(screen) ? A2(
			$rtfeldman$elm_css$Html$Styled$button,
			_List_fromArray(
				[
					$author$project$My$Html$onClickPreventDefault(msg),
					$rtfeldman$elm_css$Html$Styled$Attributes$title('Disable fullscreen.'),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					function ($) {
						return $.standard;
					}(
						$author$project$Theme$buttonWithMargin(theme)))
				]),
			_List_fromArray(
				[
					A2($author$project$View$buttonIcon, theme, 'fullscreen')
				])) : A2(
			$rtfeldman$elm_css$Html$Styled$button,
			_List_fromArray(
				[
					$author$project$My$Html$onClickPreventDefault(msg),
					$rtfeldman$elm_css$Html$Styled$Attributes$title('Enable fullscreen.'),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					A2(
						$elm$core$List$cons,
						$author$project$Theme$bottomMargin(theme),
						function ($) {
							return $.standard;
						}(
							$author$project$Theme$button(theme))))
				]),
			_List_fromArray(
				[
					A2($author$project$View$buttonIcon, theme, 'fullscreen_exit')
				]));
	});
var $author$project$PixScreen$pointerLockButton = F5(
	function (theme, msg, inactionMsg, screen, maybeNotification) {
		if (maybeNotification.$ === 'Just') {
			var notification = maybeNotification.a;
			var kaputButtonStyles = A2(
				$elm$core$List$cons,
				$author$project$Theme$bottomMargin(theme),
				$author$project$Theme$kaputButtonStyles(theme));
			return A2(
				$rtfeldman$elm_css$Html$Styled$div,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(kaputButtonStyles),
						$rtfeldman$elm_css$Html$Styled$Attributes$title(
						$author$project$LifeCycle$explainWSCloseEvent(notification))
					]),
				_List_fromArray(
					[
						A2($author$project$View$buttonIcon, theme, 'videogame_asset')
					]));
		} else {
			return $author$project$PixScreen$locked(screen) ? A4(
				$author$project$My$Html$safeClickButton,
				msg,
				inactionMsg,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$title('Disable pointer lock.'),
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						function ($) {
							return $.standard;
						}(
							$author$project$Theme$buttonWithMargin(theme)))
					]),
				_List_fromArray(
					[
						A2($author$project$View$buttonIcon, theme, 'videogame_asset')
					])) : A4(
				$author$project$My$Html$safeClickButton,
				msg,
				inactionMsg,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$title('Enable pointer lock.'),
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						function ($) {
							return $.standard;
						}(
							$author$project$Theme$buttonWithMargin(theme)))
					]),
				_List_fromArray(
					[
						A2($author$project$View$buttonIcon, theme, 'videogame_asset_off')
					]));
		}
	});
var $rtfeldman$elm_css$Css$rowReverse = _Utils_update(
	$rtfeldman$elm_css$Css$row,
	{value: 'row-reverse'});
var $author$project$PixScreen$selectable = function (_v0) {
	var textSelect = _v0.a.textSelect;
	return textSelect;
};
var $author$project$PixScreen$textSelectButton = F5(
	function (theme, msg, inactionMsg, kaput, screen) {
		return $author$project$PixScreen$selectable(screen) ? A4(
			$author$project$My$Html$safeClickButton,
			msg,
			inactionMsg,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$title(
					'Disable text selection mode.' + A3($author$project$My$Extra$ternary, !kaput, '\n Re-enable mouse camera panning.', '')),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					function ($) {
						return $.standard;
					}(
						$author$project$Theme$buttonWithMargin(theme)))
				]),
			_List_fromArray(
				[
					A2($author$project$View$buttonIcon, theme, 'select_all')
				])) : A4(
			$author$project$My$Html$safeClickButton,
			msg,
			inactionMsg,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$title(
					'Enable text selection mode.' + A3($author$project$My$Extra$ternary, !kaput, '\n⚠ Disable mouse camera panning.', '')),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					function ($) {
						return $.standard;
					}(
						$author$project$Theme$buttonWithMargin(theme)))
				]),
			_List_fromArray(
				[
					A2($author$project$View$buttonIcon, theme, 'deselect')
				]));
	});
var $author$project$View$Settings$popUp = F5(
	function (theme, msg, screen, on, maybeNotification) {
		var gradientStopOnValue = '10%';
		var gradientStopOffValue = '100%';
		var gradientStop = '--settings-pop-up-gradient-stop';
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Css$property,
							'background',
							A2(
								$elm$core$String$join,
								', ',
								_List_fromArray(
									['linear-gradient(to left', 'rgba(255,255,255,1.0)', 'rgba(255,255,255,1.0) var(' + (gradientStop + ')'), 'rgba(255,255,255,0.8) calc(var(' + (gradientStop + ') + 15%)'), 'rgba(255,255,255,0.8))']))),
							A2(
							$rtfeldman$elm_css$Css$property,
							gradientStop,
							A3($author$project$My$Extra$ternary, on, gradientStopOnValue, gradientStopOffValue)),
							A3(
							$rtfeldman$elm_css$Css$borderBottom3,
							$rtfeldman$elm_css$Css$em(theme.buttonBorder),
							$rtfeldman$elm_css$Css$solid,
							theme.primary),
							A3(
							$rtfeldman$elm_css$Css$borderLeft3,
							$rtfeldman$elm_css$Css$em(theme.buttonBorder),
							$rtfeldman$elm_css$Css$solid,
							theme.primary),
							A3(
							$rtfeldman$elm_css$Css$borderTop3,
							$rtfeldman$elm_css$Css$em(theme.buttonBorder),
							$rtfeldman$elm_css$Css$solid,
							theme.primary),
							$rtfeldman$elm_css$Css$borderTopLeftRadius(
							$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
							$rtfeldman$elm_css$Css$borderBottomLeftRadius(
							$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
							A5(
							$rtfeldman$elm_css$Css$boxShadow5,
							$rtfeldman$elm_css$Css$px(0),
							$rtfeldman$elm_css$Css$px(0),
							A3(
								$author$project$My$Extra$ternary,
								on,
								$rtfeldman$elm_css$Css$px(20),
								$rtfeldman$elm_css$Css$px(0)),
							A3(
								$author$project$My$Extra$ternary,
								on,
								$rtfeldman$elm_css$Css$px(4),
								$rtfeldman$elm_css$Css$px(0)),
							A3($rtfeldman$elm_css$Css$rgb, 112, 145, 145)),
							A4(
							$rtfeldman$elm_css$Css$padding4,
							$rtfeldman$elm_css$Css$em(theme.buttonPadding),
							$rtfeldman$elm_css$Css$em(theme.buttonPadding),
							$rtfeldman$elm_css$Css$em(0),
							$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
							$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$absolute),
							$rtfeldman$elm_css$Css$transform(
							A2(
								$rtfeldman$elm_css$Css$translate2,
								$rtfeldman$elm_css$Css$pct(
									A3($author$project$My$Extra$ternary, on, -98, 2)),
								$rtfeldman$elm_css$Css$pct(-50))),
							A2(
							$rtfeldman$elm_css$Css$property,
							'transition',
							A2(
								$elm$core$String$join,
								', ',
								_List_fromArray(
									['transform ' + $author$project$Theme$popUpDurationMS, 'box-shadow ' + $author$project$Theme$popUpDurationMS, gradientStop + (' ' + $author$project$Theme$popUpDurationMS)]))),
							A2(
							$rtfeldman$elm_css$Css$property,
							'user-select',
							A3($author$project$My$Extra$ternary, on, 'auto', 'none'))
						]))
				]),
			_List_fromArray(
				[
					$rtfeldman$elm_css$Css$Global$global(
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Css$Global$selector,
							'@property ' + gradientStop,
							_List_fromArray(
								[
									A2($rtfeldman$elm_css$Css$property, 'syntax', '\'<percentage>\''),
									A2($rtfeldman$elm_css$Css$property, 'inherits', 'false'),
									A2($rtfeldman$elm_css$Css$property, 'initial-value', gradientStopOffValue)
								]))
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									$rtfeldman$elm_css$Css$displayFlex,
									$rtfeldman$elm_css$Css$flexDirection($rtfeldman$elm_css$Css$rowReverse),
									A2(
									$rtfeldman$elm_css$Css$property,
									'gap',
									$elm$core$String$fromFloat(theme.buttonPadding) + 'em')
								]))
						]),
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Html$Styled$div,
							_List_fromArray(
								[
									$rtfeldman$elm_css$Html$Styled$Attributes$css(
									_List_fromArray(
										[
											$rtfeldman$elm_css$Css$displayFlex,
											$rtfeldman$elm_css$Css$flexDirection($rtfeldman$elm_css$Css$column)
										]))
								]),
							_List_fromArray(
								[
									A3($author$project$PixScreen$fitScreenButton, theme, msg.fit, screen),
									A3($author$project$PixScreen$borderButton, theme, msg.border, screen),
									A3(
									$author$project$PixScreen$fullscreenButton,
									theme,
									msg.full(screen),
									screen)
								])),
							A2(
							$rtfeldman$elm_css$Html$Styled$div,
							_List_fromArray(
								[
									$rtfeldman$elm_css$Html$Styled$Attributes$css(
									_List_fromArray(
										[
											$rtfeldman$elm_css$Css$displayFlex,
											$rtfeldman$elm_css$Css$flexDirection($rtfeldman$elm_css$Css$column)
										]))
								]),
							_List_fromArray(
								[
									A5(
									$author$project$PixScreen$pointerLockButton,
									theme,
									msg.lock(screen),
									msg.inaction,
									screen,
									maybeNotification),
									A5(
									$author$project$PixScreen$textSelectButton,
									theme,
									msg.selectable,
									msg.inaction,
									$elm_community$maybe_extra$Maybe$Extra$isJust(maybeNotification),
									screen)
								]))
						]))
				]));
	});
var $author$project$PopUp$AttributionsTab = {$: 'AttributionsTab'};
var $author$project$PopUp$DocsTab = {$: 'DocsTab'};
var $author$project$PopUp$InfoTabMsg = function (a) {
	return {$: 'InfoTabMsg', a: a};
};
var $author$project$PopUp$MonitorTab = {$: 'MonitorTab'};
var $author$project$PopUp$infoAttributionsTab = function (model) {
	var _v0 = model.infoTab;
	if (_v0.$ === 'AttributionsTab') {
		return true;
	} else {
		return false;
	}
};
var $author$project$View$Info$attributionButton = F3(
	function (theme, msg, popups) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$button,
			_List_fromArray(
				[
					$author$project$My$Html$onClickPreventDefault(msg),
					$rtfeldman$elm_css$Html$Styled$Attributes$title('Inspect third party attributions.'),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					$author$project$Theme$buttonWithMargin(theme).standard)
				]),
			_List_fromArray(
				[
					A4(
					$author$project$View$expanderButtonIcon,
					theme,
					'attribution',
					$author$project$PopUp$infoAttributionsTab(popups),
					true)
				]));
	});
var $rtfeldman$elm_css$Css$calcExpressionToString = function (expression) {
	if (expression.$ === 'Addition') {
		return '+';
	} else {
		return '-';
	}
};
var $rtfeldman$elm_css$Css$calc = F3(
	function (firstExpr, expression, secondExpr) {
		var withoutCalcStr = function (l) {
			return A2($elm$core$String$startsWith, 'calc(', l.value) ? A2($elm$core$String$dropLeft, 4, l.value) : l.value;
		};
		var calcs = withoutCalcStr(firstExpr) + (' ' + ($rtfeldman$elm_css$Css$calcExpressionToString(expression) + (' ' + withoutCalcStr(secondExpr))));
		var value = A2(
			$rtfeldman$elm_css$Css$cssFunction,
			'calc',
			_List_fromArray(
				[calcs]));
		return {calc: $rtfeldman$elm_css$Css$Structure$Compatible, flexBasis: $rtfeldman$elm_css$Css$Structure$Compatible, fontSize: $rtfeldman$elm_css$Css$Structure$Compatible, length: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrAuto: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrAutoOrCoverOrContain: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrMinMaxDimension: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrNone: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrNoneOrMinMaxDimension: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrNumber: $rtfeldman$elm_css$Css$Structure$Compatible, lengthOrNumberOrAutoOrNoneOrContent: $rtfeldman$elm_css$Css$Structure$Compatible, textIndent: $rtfeldman$elm_css$Css$Structure$Compatible, value: value};
	});
var $author$project$PopUp$infoDocsTab = function (model) {
	var _v0 = model.infoTab;
	if (_v0.$ === 'DocsTab') {
		return true;
	} else {
		return false;
	}
};
var $author$project$View$Info$docsButton = F3(
	function (theme, msg, popups) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$button,
			_List_fromArray(
				[
					$author$project$My$Html$onClickPreventDefault(msg),
					$rtfeldman$elm_css$Html$Styled$Attributes$title('Access a beginner\'s guide to using this world.'),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					$author$project$Theme$buttonWithMargin(theme).standard)
				]),
			_List_fromArray(
				[
					A4(
					$author$project$View$expanderButtonIcon,
					theme,
					'school',
					$author$project$PopUp$infoDocsTab(popups),
					true)
				]));
	});
var $author$project$PopUp$infoMonitorTab = function (model) {
	var _v0 = model.infoTab;
	if (_v0.$ === 'MonitorTab') {
		return true;
	} else {
		return false;
	}
};
var $author$project$View$Info$monitorButton = F3(
	function (theme, msg, popups) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$button,
			_List_fromArray(
				[
					$author$project$My$Html$onClickPreventDefault(msg),
					$rtfeldman$elm_css$Html$Styled$Attributes$title('Monitor the performance of this world.'),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					$author$project$Theme$buttonWithMargin(theme).standard)
				]),
			_List_fromArray(
				[
					A4(
					$author$project$View$expanderButtonIcon,
					theme,
					'analytics',
					$author$project$PopUp$infoMonitorTab(popups),
					true)
				]));
	});
var $rtfeldman$elm_css$Css$Addition = {$: 'Addition'};
var $rtfeldman$elm_css$Css$plus = $rtfeldman$elm_css$Css$Addition;
var $elm$core$Maybe$andThen = F2(
	function (callback, maybeValue) {
		if (maybeValue.$ === 'Just') {
			var value = maybeValue.a;
			return callback(value);
		} else {
			return $elm$core$Maybe$Nothing;
		}
	});
var $rtfeldman$elm_css$Css$spaceBetween = $rtfeldman$elm_css$Css$prop1('space-between');
var $author$project$View$Info$dependencies = F3(
	function (theme, copyMsg, deps) {
		return A2(
			$elm$core$List$map,
			function (dep) {
				return A2(
					$rtfeldman$elm_css$Html$Styled$li,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									$rtfeldman$elm_css$Css$padding(
									$rtfeldman$elm_css$Css$em(0))
								]))
						]),
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Html$Styled$div,
							_List_fromArray(
								[
									$rtfeldman$elm_css$Html$Styled$Attributes$css(
									_List_fromArray(
										[
											$rtfeldman$elm_css$Css$displayFlex,
											$rtfeldman$elm_css$Css$justifyContent($rtfeldman$elm_css$Css$spaceBetween)
										]))
								]),
							_List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Html$Styled$div,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$Attributes$css(
											_List_fromArray(
												[
													$rtfeldman$elm_css$Css$display($rtfeldman$elm_css$Css$inlineBlock)
												]))
										]),
									_List_fromArray(
										[
											A2(
											$rtfeldman$elm_css$Html$Styled$p,
											_List_Nil,
											_List_fromArray(
												[
													A2($author$project$My$Html$a, dep.url, dep.name),
													$rtfeldman$elm_css$Html$Styled$text(' courtesy of '),
													$rtfeldman$elm_css$Html$Styled$text(
													A2($elm$core$String$join, ' / ', dep.licensor)),
													$rtfeldman$elm_css$Html$Styled$text('.')
												])),
											A2(
											$rtfeldman$elm_css$Html$Styled$p,
											_List_Nil,
											_List_fromArray(
												[
													$rtfeldman$elm_css$Html$Styled$text('Published under the '),
													A2($author$project$My$Html$a, dep.license.url, dep.license.name),
													$rtfeldman$elm_css$Html$Styled$text(' license.')
												]))
										])),
									A2(
									$rtfeldman$elm_css$Html$Styled$div,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$Attributes$css(
											_List_fromArray(
												[
													$rtfeldman$elm_css$Css$alignSelf($rtfeldman$elm_css$Css$center),
													$rtfeldman$elm_css$Css$display($rtfeldman$elm_css$Css$inlineBlock)
												]))
										]),
									_List_fromArray(
										[
											A2(
											$rtfeldman$elm_css$Html$Styled$button,
											_List_fromArray(
												[
													$author$project$My$Html$onClickPreventDefault(
													copyMsg(dep.license.text)),
													$rtfeldman$elm_css$Html$Styled$Attributes$title('Copy the ' + (dep.name + ' license legal code to the clipboard.')),
													$rtfeldman$elm_css$Html$Styled$Attributes$css(
													$author$project$Theme$button(theme).standard)
												]),
											_List_fromArray(
												[
													A2($author$project$View$buttonIcon, theme, 'content_copy')
												]))
										]))
								]))
						]));
			},
			deps);
	});
var $author$project$View$Info$author = function (authors) {
	return A2(
		$rtfeldman$elm_css$Html$Styled$span,
		_List_Nil,
		function () {
			if (authors.b) {
				if (!authors.b.b) {
					var sole = authors.a;
					return _List_fromArray(
						[
							A2($author$project$My$Html$a, sole.url, sole.name)
						]);
				} else {
					return A2(
						$elm$core$List$intersperse,
						$rtfeldman$elm_css$Html$Styled$text(', '),
						A2(
							$elm$core$List$map,
							function (licensor) {
								return A2($author$project$My$Html$a, licensor.url, licensor.name);
							},
							authors));
				}
			} else {
				return _List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$text('Unknown')
					]);
			}
		}());
};
var $rtfeldman$elm_css$Css$borderBottomWidth = $rtfeldman$elm_css$Css$prop1('border-bottom-width');
var $rtfeldman$elm_css$Css$borderLeftWidth = $rtfeldman$elm_css$Css$prop1('border-left-width');
var $rtfeldman$elm_css$Css$borderRightWidth = $rtfeldman$elm_css$Css$prop1('border-right-width');
var $rtfeldman$elm_css$Css$borderTopWidth = $rtfeldman$elm_css$Css$prop1('border-top-width');
var $rtfeldman$elm_css$Css$fontStyle = $rtfeldman$elm_css$Css$prop1('font-style');
var $rtfeldman$elm_css$Css$italic = {fontStyle: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'italic'};
var $author$project$View$Info$intro = F2(
	function (theme, about) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_Nil,
			_List_fromArray(
				[
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									A2($rtfeldman$elm_css$Css$property, 'backdrop-filter', 'blur(5px)'),
									A2($rtfeldman$elm_css$Css$property, 'box-shadow', theme.boxShadow),
									$rtfeldman$elm_css$Css$borderColor(theme.primary),
									$rtfeldman$elm_css$Css$borderRadius(
									$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
									$rtfeldman$elm_css$Css$borderStyle($rtfeldman$elm_css$Css$solid),
									$rtfeldman$elm_css$Css$borderBottomWidth(
									$rtfeldman$elm_css$Css$em(0)),
									$rtfeldman$elm_css$Css$borderLeftWidth(
									$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
									$rtfeldman$elm_css$Css$borderRightWidth(
									$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
									$rtfeldman$elm_css$Css$borderTopWidth(
									$rtfeldman$elm_css$Css$em(0)),
									$rtfeldman$elm_css$Css$marginTop(
									$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
									$rtfeldman$elm_css$Css$padding(
									$rtfeldman$elm_css$Css$em(theme.buttonPadding))
								]))
						]),
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Html$Styled$h1,
							_List_fromArray(
								[
									$rtfeldman$elm_css$Html$Styled$Attributes$css(
									_List_fromArray(
										[
											$rtfeldman$elm_css$Css$display($rtfeldman$elm_css$Css$inlineBlock),
											$rtfeldman$elm_css$Css$marginTop(
											$rtfeldman$elm_css$Css$em(0))
										]))
								]),
							_List_fromArray(
								[
									A2($author$project$My$Html$a, about.link, about.name)
								])),
							A2(
							$rtfeldman$elm_css$Html$Styled$span,
							_List_fromArray(
								[
									$rtfeldman$elm_css$Html$Styled$Attributes$css(
									_List_fromArray(
										[
											$rtfeldman$elm_css$Css$fontStyle($rtfeldman$elm_css$Css$italic),
											$rtfeldman$elm_css$Css$transform(
											$rtfeldman$elm_css$Css$translateY(
												$rtfeldman$elm_css$Css$em(0.5)))
										]))
								]),
							_List_fromArray(
								[
									$rtfeldman$elm_css$Html$Styled$text(' courtesy of '),
									$author$project$View$Info$author(about.author)
								])),
							A2(
							$rtfeldman$elm_css$Html$Styled$div,
							_List_Nil,
							A2(
								$elm$core$List$indexedMap,
								F2(
									function (i, sentence) {
										return A2(
											$rtfeldman$elm_css$Html$Styled$p,
											_List_fromArray(
												[
													$rtfeldman$elm_css$Html$Styled$Attributes$css(
													A2(
														$author$project$My$Extra$appendWhen,
														_List_Nil,
														_List_fromArray(
															[
																_Utils_Tuple2(
																!i,
																_List_fromArray(
																	[
																		$rtfeldman$elm_css$Css$marginTop(
																		$rtfeldman$elm_css$Css$em(0))
																	])),
																_Utils_Tuple2(
																_Utils_eq(
																	i,
																	(-1) + $elm$core$List$length(about.blurb)),
																_List_fromArray(
																	[
																		$rtfeldman$elm_css$Css$marginBottom(
																		$rtfeldman$elm_css$Css$em(0))
																	]))
															])))
												]),
											_List_fromArray(
												[
													$rtfeldman$elm_css$Html$Styled$text(sentence)
												]));
									}),
								about.blurb))
						]))
				]));
	});
var $elm$core$Basics$min = F2(
	function (x, y) {
		return (_Utils_cmp(x, y) < 0) ? x : y;
	});
var $author$project$View$Info$linkSectionWidth = F2(
	function (license1, license2) {
		var maxWidth = 43;
		return A2(
			$elm$core$Basics$min,
			maxWidth,
			function () {
				var _v0 = $elm$core$String$length(license2.name);
				if (!_v0) {
					return $elm$core$String$length(license1.name);
				} else {
					var l2 = _v0;
					return A2(
						$elm$core$Basics$max,
						$elm$core$String$length(license1.name),
						l2);
				}
			}());
	});
var $author$project$View$Info$listItemSeparator = F2(
	function (index, list) {
		return _Utils_eq(
			index,
			$elm$core$List$length(list) - 1) ? '.' : ';';
	});
var $rtfeldman$elm_css$Html$Styled$Attributes$alt = $rtfeldman$elm_css$Html$Styled$Attributes$stringProperty('alt');
var $rtfeldman$elm_css$Css$transparent = {color: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'transparent'};
var $rtfeldman$elm_css$Css$colorValueForOverloadedProperty = $rtfeldman$elm_css$Css$transparent;
var $rtfeldman$elm_css$Css$backgroundBlendMode = function (fn) {
	return A3(
		$rtfeldman$elm_css$Css$Internal$getOverloadedProperty,
		'backgroundBlendMode',
		'background-blend-mode',
		fn($rtfeldman$elm_css$Css$colorValueForOverloadedProperty));
};
var $rtfeldman$elm_css$Css$backgroundImage = $rtfeldman$elm_css$Css$prop1('background-image');
var $rtfeldman$elm_css$Css$backgroundPosition = function (fn) {
	return A3(
		$rtfeldman$elm_css$Css$Internal$getOverloadedProperty,
		'backgroundPosition',
		'background-position',
		fn($rtfeldman$elm_css$Css$Internal$lengthForOverloadedProperty));
};
var $rtfeldman$elm_css$Css$prop2 = F3(
	function (key, argA, argB) {
		return A2($rtfeldman$elm_css$Css$property, key, argA.value + (' ' + argB.value));
	});
var $rtfeldman$elm_css$Css$backgroundSize2 = $rtfeldman$elm_css$Css$prop2('background-size');
var $rtfeldman$elm_css$Html$Styled$img = $rtfeldman$elm_css$Html$Styled$node('img');
var $rtfeldman$elm_css$Css$ridge = {borderStyle: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'ridge'};
var $rtfeldman$elm_css$Css$screenBlendMode = $rtfeldman$elm_css$Css$prop1('screen');
var $rtfeldman$elm_css$Html$Styled$Attributes$src = function (url) {
	return A2($rtfeldman$elm_css$Html$Styled$Attributes$stringProperty, 'src', url);
};
var $rtfeldman$elm_css$Css$url = function (urlValue) {
	return {backgroundImage: $rtfeldman$elm_css$Css$Structure$Compatible, value: 'url(' + (urlValue + ')')};
};
var $author$project$My$Extra$useIf = F2(
	function (ok, okValue) {
		return ok ? okValue : _List_Nil;
	});
var $author$project$View$Info$mainLicense = F6(
	function (theme, copyMsg, productName, license, spdxId, linkWidth) {
		var containerStyles = A3(
			$author$project$My$Extra$appendIf,
			license.badge !== '',
			_List_fromArray(
				[
					$rtfeldman$elm_css$Css$alignItems($rtfeldman$elm_css$Css$flexStart),
					A2($rtfeldman$elm_css$Css$property, 'box-shadow', theme.boxShadow),
					$rtfeldman$elm_css$Css$borderColor(theme.primary),
					$rtfeldman$elm_css$Css$borderRadius(
					$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
					$rtfeldman$elm_css$Css$borderStyle($rtfeldman$elm_css$Css$solid),
					$rtfeldman$elm_css$Css$borderBottomWidth(
					$rtfeldman$elm_css$Css$em(0)),
					$rtfeldman$elm_css$Css$borderLeftWidth(
					$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
					$rtfeldman$elm_css$Css$borderRightWidth(
					$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
					$rtfeldman$elm_css$Css$borderTopWidth(
					$rtfeldman$elm_css$Css$em(0)),
					$rtfeldman$elm_css$Css$displayFlex,
					$rtfeldman$elm_css$Css$justifyContent($rtfeldman$elm_css$Css$spaceBetween),
					$rtfeldman$elm_css$Css$padding(
					$rtfeldman$elm_css$Css$em(theme.buttonPadding))
				]),
			_List_fromArray(
				[
					$rtfeldman$elm_css$Css$backgroundColor(
					A4($rtfeldman$elm_css$Css$rgba, 255, 255, 255, 0.7)),
					A2($rtfeldman$elm_css$Css$property, 'backdrop-filter', 'sepia(0.5)')
				]));
		var badgeUrl = 'data:image/png;base64,' + license.badge;
		var backgroundImageStyles = A2(
			$author$project$My$Extra$useIf,
			license.badge !== '',
			_List_fromArray(
				[
					$rtfeldman$elm_css$Css$backgroundBlendMode($rtfeldman$elm_css$Css$screenBlendMode),
					$rtfeldman$elm_css$Css$backgroundColor(theme.accent),
					$rtfeldman$elm_css$Css$backgroundImage(
					$rtfeldman$elm_css$Css$url(badgeUrl)),
					$rtfeldman$elm_css$Css$backgroundPosition($rtfeldman$elm_css$Css$center),
					A2(
					$rtfeldman$elm_css$Css$backgroundSize2,
					$rtfeldman$elm_css$Css$auto,
					$rtfeldman$elm_css$Css$em(theme.buttonHeight * 3)),
					$rtfeldman$elm_css$Css$borderColor(theme.accent),
					$rtfeldman$elm_css$Css$borderRadius(
					$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius / 4)),
					$rtfeldman$elm_css$Css$borderStyle($rtfeldman$elm_css$Css$ridge),
					$rtfeldman$elm_css$Css$borderWidth(
					$rtfeldman$elm_css$Css$em(theme.buttonBorder / 2)),
					$rtfeldman$elm_css$Css$padding(
					$rtfeldman$elm_css$Css$em(theme.buttonPadding))
				]));
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_Nil,
			_List_fromArray(
				[
					A2($rtfeldman$elm_css$Html$Styled$h2, _List_Nil, _List_Nil),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(backgroundImageStyles)
						]),
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Html$Styled$div,
							_List_fromArray(
								[
									$rtfeldman$elm_css$Html$Styled$Attributes$css(containerStyles),
									$rtfeldman$elm_css$Html$Styled$Attributes$title(spdxId)
								]),
							_List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Html$Styled$div,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$Attributes$css(
											_List_fromArray(
												[
													$rtfeldman$elm_css$Css$displayFlex,
													$rtfeldman$elm_css$Css$flexDirection($rtfeldman$elm_css$Css$column)
												]))
										]),
									_List_fromArray(
										[
											A2(
											$rtfeldman$elm_css$Html$Styled$h2,
											_List_fromArray(
												[
													$rtfeldman$elm_css$Html$Styled$Attributes$css(
													_List_fromArray(
														[
															$rtfeldman$elm_css$Css$marginTop(
															$rtfeldman$elm_css$Css$em(0))
														]))
												]),
											_List_fromArray(
												[
													$rtfeldman$elm_css$Html$Styled$text('License')
												])),
											A2(
											$rtfeldman$elm_css$Html$Styled$span,
											_List_fromArray(
												[
													$rtfeldman$elm_css$Html$Styled$Attributes$css(
													_List_fromArray(
														[
															$rtfeldman$elm_css$Css$width(
															$rtfeldman$elm_css$Css$ch(linkWidth))
														]))
												]),
											_List_fromArray(
												[
													A2($author$project$My$Html$a, license.url, license.name)
												]))
										])),
									(license.badge === '') ? $rtfeldman$elm_css$Html$Styled$text('') : A2(
									$rtfeldman$elm_css$Html$Styled$img,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$Attributes$alt('license badge'),
											$rtfeldman$elm_css$Html$Styled$Attributes$css(
											_List_fromArray(
												[
													$rtfeldman$elm_css$Css$alignSelf($rtfeldman$elm_css$Css$center),
													$rtfeldman$elm_css$Css$height(
													$rtfeldman$elm_css$Css$em(theme.buttonHeight))
												])),
											$rtfeldman$elm_css$Html$Styled$Attributes$src(badgeUrl)
										]),
									_List_Nil),
									A2(
									$rtfeldman$elm_css$Html$Styled$div,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$Attributes$css(
											_List_fromArray(
												[
													$rtfeldman$elm_css$Css$alignSelf($rtfeldman$elm_css$Css$center),
													$rtfeldman$elm_css$Css$height(
													$rtfeldman$elm_css$Css$em(theme.buttonHeight))
												]))
										]),
									_List_fromArray(
										[
											A2(
											$rtfeldman$elm_css$Html$Styled$button,
											_List_fromArray(
												[
													$author$project$My$Html$onClickPreventDefault(
													copyMsg(license.text)),
													$rtfeldman$elm_css$Html$Styled$Attributes$title('Copy the ' + (productName + (' ' + (license.id + (' ' + ' license legal code to the clipboard.'))))),
													$rtfeldman$elm_css$Html$Styled$Attributes$css(
													$author$project$Theme$button(theme).standard)
												]),
											_List_fromArray(
												[
													A2($author$project$View$buttonIcon, theme, 'content_copy')
												]))
										]))
								]))
						]))
				]));
	});
var $author$project$Static$About$spdxId = F2(
	function (license1, license2) {
		return A2(
			$elm$core$String$append,
			'SPDX-License-Identifier: ',
			function () {
				var _v0 = _Utils_Tuple2(license1.id, license2.id);
				if (_v0.b === '') {
					if (_v0.a === '') {
						return 'Identifier unknown';
					} else {
						var id = _v0.a;
						return id;
					}
				} else {
					var id1 = _v0.a;
					var id2 = _v0.b;
					return id1 + (' OR ' + id2);
				}
			}());
	});
var $elm_community$string_extra$String$Extra$regexFromString = A2(
	$elm$core$Basics$composeR,
	$elm$regex$Regex$fromString,
	$elm$core$Maybe$withDefault($elm$regex$Regex$never));
var $elm$regex$Regex$replace = _Regex_replaceAtMost(_Regex_infinity);
var $elm_community$string_extra$String$Extra$changeCase = F2(
	function (mutator, word) {
		return A2(
			$elm$core$Maybe$withDefault,
			'',
			A2(
				$elm$core$Maybe$map,
				function (_v0) {
					var head = _v0.a;
					var tail = _v0.b;
					return A2(
						$elm$core$String$cons,
						mutator(head),
						tail);
				},
				$elm$core$String$uncons(word)));
	});
var $elm$core$Char$toUpper = _Char_toUpper;
var $elm_community$string_extra$String$Extra$toSentenceCase = function (word) {
	return A2($elm_community$string_extra$String$Extra$changeCase, $elm$core$Char$toUpper, word);
};
var $elm_community$string_extra$String$Extra$toTitleCase = function (ws) {
	var uppercaseMatch = A2(
		$elm$regex$Regex$replace,
		$elm_community$string_extra$String$Extra$regexFromString('\\w+'),
		A2(
			$elm$core$Basics$composeR,
			function ($) {
				return $.match;
			},
			$elm_community$string_extra$String$Extra$toSentenceCase));
	return A3(
		$elm$regex$Regex$replace,
		$elm_community$string_extra$String$Extra$regexFromString('^([a-z])|\\s+([a-z])'),
		A2(
			$elm$core$Basics$composeR,
			function ($) {
				return $.match;
			},
			uppercaseMatch),
		ws);
};
var $author$project$View$Info$attributions = F3(
	function (theme, copyMsg, about) {
		var titleCaseName = $elm_community$string_extra$String$Extra$toTitleCase(about.name);
		var spdxId = A2($author$project$Static$About$spdxId, about.license, about.alternativeLicense);
		var parseTSImports = function (imports) {
			return $elm$core$List$sort(
				A2(
					$elm$core$List$filterMap,
					function (url) {
						return A2(
							$elm$core$Maybe$andThen,
							function (parsedURL) {
								var _v12 = A2($elm$core$String$split, '/', parsedURL.path);
								if (_v12.b && _v12.b.b) {
									var _v13 = _v12.b;
									var name = _v13.b;
									return $elm$core$Maybe$Just(
										_Utils_Tuple2(
											A2($elm$core$String$join, '/', name),
											url));
								} else {
									return $elm$core$Maybe$Nothing;
								}
							},
							$elm$url$Url$fromString(url));
					},
					imports));
		};
		var parseGoImports = function (imports) {
			return $elm$core$List$sort(
				A2(
					$elm$core$List$filterMap,
					function (url) {
						return A2(
							$elm$core$Maybe$andThen,
							function (parsedURL) {
								var _v6 = A2($elm$core$String$split, '/', parsedURL.path);
								if ((_v6.b && _v6.b.b) && _v6.b.b.b) {
									if (_v6.b.b.b.b) {
										var _v7 = _v6.b;
										var _v8 = _v7.b;
										var licensor = _v8.a;
										var _v9 = _v8.b;
										var name = _v9.a;
										return $elm$core$Maybe$Just(
											_Utils_Tuple3(name, licensor, url));
									} else {
										var _v10 = _v6.b;
										var licensor = _v10.a;
										var _v11 = _v10.b;
										var name = _v11.a;
										return $elm$core$Maybe$Just(
											_Utils_Tuple3(name, licensor, url));
									}
								} else {
									return $elm$core$Maybe$Nothing;
								}
							},
							$elm$url$Url$fromString(url));
					},
					imports));
		};
		var parseElmImports = function (imports) {
			return $elm$core$List$sort(
				A2(
					$elm$core$List$filterMap,
					function (url) {
						return A2(
							$elm$core$Maybe$andThen,
							function (parsedURL) {
								var _v2 = A2($elm$core$String$split, '/', parsedURL.path);
								if (((_v2.b && _v2.b.b) && _v2.b.b.b) && _v2.b.b.b.b) {
									var _v3 = _v2.b;
									var _v4 = _v3.b;
									var licensor = _v4.a;
									var _v5 = _v4.b;
									var name = _v5.a;
									return $elm$core$Maybe$Just(
										_Utils_Tuple3(name, licensor, url));
								} else {
									return $elm$core$Maybe$Nothing;
								}
							},
							$elm$url$Url$fromString(url));
					},
					imports));
		};
		var main = A3($author$project$View$Info$mainLicense, theme, copyMsg, about.name);
		var licenseLinkWidth = A2($author$project$View$Info$linkSectionWidth, about.license, about.alternativeLicense);
		var itemStyles = _List_fromArray(
			[
				$rtfeldman$elm_css$Html$Styled$Attributes$css(
				_List_fromArray(
					[
						$rtfeldman$elm_css$Css$padding(
						$rtfeldman$elm_css$Css$em(theme.buttonPadding))
					]))
			]);
		var viewImports = function (imports) {
			return A2(
				$elm$core$List$indexedMap,
				F2(
					function (i, _v1) {
						var name = _v1.a;
						var licensor = _v1.b;
						var url = _v1.c;
						return A2(
							$rtfeldman$elm_css$Html$Styled$li,
							itemStyles,
							_List_fromArray(
								[
									A2($author$project$My$Html$a, url, name),
									$rtfeldman$elm_css$Html$Styled$text(
									' courtesy of ' + (licensor + A2($author$project$View$Info$listItemSeparator, i, imports)))
								]));
					}),
				imports);
		};
		var viewTSImports = function (imports) {
			return A2(
				$elm$core$List$indexedMap,
				F2(
					function (i, _v0) {
						var name = _v0.a;
						var url = _v0.b;
						return A2(
							$rtfeldman$elm_css$Html$Styled$li,
							itemStyles,
							_List_fromArray(
								[
									A2($author$project$My$Html$a, url, name),
									$rtfeldman$elm_css$Html$Styled$text(
									A2($author$project$View$Info$listItemSeparator, i, imports))
								]));
					}),
				imports);
		};
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$paddingLeft(
							$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
							$rtfeldman$elm_css$Css$paddingRight(
							$rtfeldman$elm_css$Css$em(theme.buttonPadding))
						]))
				]),
			_List_fromArray(
				[
					A2($author$project$View$Info$intro, theme, about),
					A3(main, about.license, spdxId, licenseLinkWidth),
					A3(main, about.alternativeLicense, spdxId, licenseLinkWidth),
					A2(
					$rtfeldman$elm_css$Html$Styled$h2,
					_List_Nil,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text('Fonts')
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$p,
					_List_Nil,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text('Open source fonts distributed with ' + (titleCaseName + ':'))
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$ul,
					_List_Nil,
					A3($author$project$View$Info$dependencies, theme, copyMsg, about.fonts)),
					A2(
					$rtfeldman$elm_css$Html$Styled$h2,
					_List_Nil,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text('Binary dependencies for users')
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$p,
					_List_Nil,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text('Binaries that need to be preinstalled on the system for ' + (titleCaseName + ' to run:'))
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$ul,
					_List_Nil,
					A3($author$project$View$Info$dependencies, theme, copyMsg, about.binaries)),
					A2(
					$rtfeldman$elm_css$Html$Styled$h2,
					_List_Nil,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text('Binary dependencies for developers')
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$p,
					_List_Nil,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text('Other binaries that need to be preinstalled on the system for ' + (titleCaseName + ' to be fully compiled from source:'))
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$ul,
					_List_Nil,
					A3($author$project$View$Info$dependencies, theme, copyMsg, about.devBinaries)),
					A2(
					$rtfeldman$elm_css$Html$Styled$h2,
					_List_Nil,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text('Imported packages')
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$p,
					_List_Nil,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text('Dependencies that were introduced through the import mechanism ' + ('of each of the main programming languages that ' + (titleCaseName + ' is written in:')))
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$ul,
					_List_Nil,
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Html$Styled$li,
							_List_Nil,
							_List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Html$Styled$h3,
									_List_Nil,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$text('Elm language imports')
										])),
									A2(
									$rtfeldman$elm_css$Html$Styled$ul,
									_List_Nil,
									viewImports(
										parseElmImports(about.elmImports)))
								])),
							A2(
							$rtfeldman$elm_css$Html$Styled$li,
							_List_Nil,
							_List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Html$Styled$h3,
									_List_Nil,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$text('Go language imports')
										])),
									A2(
									$rtfeldman$elm_css$Html$Styled$ul,
									_List_Nil,
									viewImports(
										parseGoImports(about.goImports)))
								])),
							A2(
							$rtfeldman$elm_css$Html$Styled$li,
							_List_Nil,
							_List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Html$Styled$h3,
									_List_Nil,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$text('Typescript language imports')
										])),
									A2(
									$rtfeldman$elm_css$Html$Styled$ul,
									_List_Nil,
									viewTSImports(
										parseTSImports(about.tsImports)))
								])),
							A2(
							$rtfeldman$elm_css$Html$Styled$li,
							_List_Nil,
							_List_fromArray(
								[
									A2(
									$rtfeldman$elm_css$Html$Styled$h3,
									_List_Nil,
									_List_fromArray(
										[
											$rtfeldman$elm_css$Html$Styled$text('Typescript language development imports')
										])),
									A2(
									$rtfeldman$elm_css$Html$Styled$ul,
									_List_Nil,
									viewTSImports(
										parseTSImports(about.tsDevImports)))
								]))
						]))
				]));
	});
var $author$project$View$Info$docs = A2(
	$rtfeldman$elm_css$Html$Styled$div,
	_List_Nil,
	_List_fromArray(
		[
			A2(
			$rtfeldman$elm_css$Html$Styled$h1,
			_List_Nil,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$text('Docs')
				])),
			A2(
			$rtfeldman$elm_css$Html$Styled$p,
			_List_Nil,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$text('There are no docs for this world at the moment.')
				]))
		]));
var $rtfeldman$elm_css$Css$Transitions$Filter = {$: 'Filter'};
var $rtfeldman$elm_css$Css$Transitions$filter = $rtfeldman$elm_css$Css$Transitions$durationTransition($rtfeldman$elm_css$Css$Transitions$Filter);
var $author$project$Static$About$mechane = function (_v0) {
	var about = _v0.a;
	return about.mechane;
};
var $author$project$View$Info$monitor = F2(
	function (theme, errors) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_Nil,
			_List_fromArray(
				[
					A2(
					$rtfeldman$elm_css$Html$Styled$h1,
					_List_Nil,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text('Errors')
						])),
					(!$elm$core$List$length(errors)) ? A2(
					$rtfeldman$elm_css$Html$Styled$p,
					_List_Nil,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text('Looks good! There are no errors.')
						])) : A2($author$project$View$errorsList, theme, errors)
				]));
	});
var $rtfeldman$elm_css$Css$overflowX = $rtfeldman$elm_css$Css$prop1('overflow-x');
var $rtfeldman$elm_css$Css$overflowY = $rtfeldman$elm_css$Css$prop1('overflow-y');
var $author$project$Static$About$world = function (_v0) {
	var about = _v0.a;
	return about.world;
};
var $author$project$View$Info$world = F3(
	function (theme, copyMsg, about) {
		var titleCaseName = $elm_community$string_extra$String$Extra$toTitleCase(about.name);
		var spdxId = A2($author$project$Static$About$spdxId, about.license, about.alternativeLicense);
		var main = A3($author$project$View$Info$mainLicense, theme, copyMsg, about.name);
		var licenseLinkWidth = A2($author$project$View$Info$linkSectionWidth, about.license, about.alternativeLicense);
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$paddingLeft(
							$rtfeldman$elm_css$Css$em(theme.buttonPadding)),
							$rtfeldman$elm_css$Css$paddingRight(
							$rtfeldman$elm_css$Css$em(theme.buttonPadding))
						]))
				]),
			A2(
				$author$project$My$Extra$appendWhen,
				_List_fromArray(
					[
						A2($author$project$View$Info$intro, theme, about),
						A3(main, about.license, spdxId, licenseLinkWidth)
					]),
				_List_fromArray(
					[
						_Utils_Tuple2(
						!$elm$core$String$isEmpty(about.alternativeLicense.name),
						_List_fromArray(
							[
								A3(main, about.alternativeLicense, spdxId, licenseLinkWidth)
							])),
						_Utils_Tuple2(
						$elm$core$List$length(about.binaries) > 0,
						_List_fromArray(
							[
								A2(
								$rtfeldman$elm_css$Html$Styled$h2,
								_List_Nil,
								_List_fromArray(
									[
										$rtfeldman$elm_css$Html$Styled$text('Binary dependencies for users')
									])),
								A2(
								$rtfeldman$elm_css$Html$Styled$p,
								_List_Nil,
								_List_fromArray(
									[
										$rtfeldman$elm_css$Html$Styled$text('Binaries that need to be preinstalled on the system for ' + (titleCaseName + ' to run:'))
									])),
								A2(
								$rtfeldman$elm_css$Html$Styled$ul,
								_List_Nil,
								A3($author$project$View$Info$dependencies, theme, copyMsg, about.binaries))
							])),
						_Utils_Tuple2(
						$elm$core$List$length(about.devBinaries) > 0,
						_List_fromArray(
							[
								A2(
								$rtfeldman$elm_css$Html$Styled$h2,
								_List_Nil,
								_List_fromArray(
									[
										$rtfeldman$elm_css$Html$Styled$text('Binary dependencies for developers')
									])),
								A2(
								$rtfeldman$elm_css$Html$Styled$p,
								_List_Nil,
								_List_fromArray(
									[
										$rtfeldman$elm_css$Html$Styled$text('Other binaries that need to be preinstalled on the system for ' + (titleCaseName + ' to be fully compiled from source:'))
									])),
								A2(
								$rtfeldman$elm_css$Html$Styled$ul,
								_List_Nil,
								A3($author$project$View$Info$dependencies, theme, copyMsg, about.devBinaries))
							]))
					])));
	});
var $author$project$View$Info$tabContent = F5(
	function (theme, copyMsg, popups, errors, h) {
		var tabs = _List_fromArray(
			[
				_Utils_Tuple2(
				$author$project$PopUp$WorldTab,
				A3(
					$author$project$View$Info$world,
					theme,
					copyMsg,
					$author$project$Static$About$world(popups.about))),
				_Utils_Tuple2($author$project$PopUp$DocsTab, $author$project$View$Info$docs),
				_Utils_Tuple2(
				$author$project$PopUp$MonitorTab,
				A2($author$project$View$Info$monitor, theme, errors)),
				_Utils_Tuple2(
				$author$project$PopUp$AttributionsTab,
				A3(
					$author$project$View$Info$attributions,
					theme,
					copyMsg,
					$author$project$Static$About$mechane(popups.about)))
			]);
		var absTabContainer = function (_v0) {
			var tab = _v0.a;
			var content = _v0.b;
			var duration = 1000;
			var active = _Utils_eq(tab, popups.infoTab);
			return A2(
				$rtfeldman$elm_css$Html$Styled$div,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css(
						_List_fromArray(
							[
								A2(
								$rtfeldman$elm_css$Css$property,
								'filter',
								A3($author$project$My$Extra$ternary, active, 'none', 'drop-shadow(0 0 0.4em black)')),
								$rtfeldman$elm_css$Css$height(h),
								$rtfeldman$elm_css$Css$opacity(
								$rtfeldman$elm_css$Css$num(
									A3($author$project$My$Extra$ternary, active, 1, 0))),
								$rtfeldman$elm_css$Css$overflowY($rtfeldman$elm_css$Css$auto),
								$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$absolute),
								$rtfeldman$elm_css$Css$transforms(
								_List_fromArray(
									[
										$rtfeldman$elm_css$Css$translateX(
										$rtfeldman$elm_css$Css$pct(
											A3($author$project$My$Extra$ternary, active, 0, 100)))
									])),
								$rtfeldman$elm_css$Css$Transitions$transition(
								_List_fromArray(
									[
										$rtfeldman$elm_css$Css$Transitions$filter(duration),
										$rtfeldman$elm_css$Css$Transitions$opacity(duration),
										$rtfeldman$elm_css$Css$Transitions$transform(duration)
									])),
								$rtfeldman$elm_css$Css$width(
								$rtfeldman$elm_css$Css$pct(100)),
								$rtfeldman$elm_css$Css$zIndex(
								$rtfeldman$elm_css$Css$int(
									A3($author$project$My$Extra$ternary, active, 1, 0)))
							]))
					]),
				_List_fromArray(
					[content]));
		};
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							$rtfeldman$elm_css$Css$overflowX($rtfeldman$elm_css$Css$hidden),
							$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$relative),
							$rtfeldman$elm_css$Css$width(
							$rtfeldman$elm_css$Css$pct(100))
						]))
				]),
			A2($elm$core$List$map, absTabContainer, tabs));
	});
var $author$project$PopUp$infoWorldTab = function (model) {
	var _v0 = model.infoTab;
	if (_v0.$ === 'WorldTab') {
		return true;
	} else {
		return false;
	}
};
var $author$project$View$Info$worldButton = F3(
	function (theme, msg, popups) {
		return A2(
			$rtfeldman$elm_css$Html$Styled$button,
			_List_fromArray(
				[
					$author$project$My$Html$onClickPreventDefault(msg),
					$rtfeldman$elm_css$Html$Styled$Attributes$title('View details about this world.'),
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					$author$project$Theme$buttonWithMargin(theme).standard)
				]),
			_List_fromArray(
				[
					A4(
					$author$project$View$expanderButtonIcon,
					theme,
					'info',
					$author$project$PopUp$infoWorldTab(popups),
					true)
				]));
	});
var $author$project$View$Info$popup = F5(
	function (theme, copyMsg, tabMsg, popups, errors) {
		var infoPopUp = function () {
			var _v0 = popups.pixelsArea;
			if ((_v0.$ === 'Just') && (_v0.a.$ === 'InfoPopUp')) {
				var _v1 = _v0.a;
				return true;
			} else {
				return false;
			}
		}();
		var h = A3(
			$rtfeldman$elm_css$Css$calc,
			$rtfeldman$elm_css$Css$ch(50),
			$rtfeldman$elm_css$Css$plus,
			$rtfeldman$elm_css$Css$em(theme.buttonPadding));
		var gradientStopOnValue = '10%';
		var gradientStopOffValue = '100%';
		var gradientStop = '--info-pop-up-gradient-stop';
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Css$property,
							'background',
							A2(
								$elm$core$String$join,
								', ',
								_List_fromArray(
									['linear-gradient(to top', 'rgba(255,255,255,1.0)', 'rgba(255,255,255,1.0) var(' + (gradientStop + ')'), 'rgba(255,255,255,0.8) calc(var(' + (gradientStop + ') + 15%)'), 'rgba(255,255,255,0.8))']))),
							A2(
							$rtfeldman$elm_css$Css$property,
							gradientStop,
							A3($author$project$My$Extra$ternary, infoPopUp, gradientStopOnValue, gradientStopOffValue)),
							A3(
							$rtfeldman$elm_css$Css$borderLeft3,
							$rtfeldman$elm_css$Css$em(theme.buttonBorder),
							$rtfeldman$elm_css$Css$solid,
							theme.primary),
							A3(
							$rtfeldman$elm_css$Css$borderRight3,
							$rtfeldman$elm_css$Css$em(theme.buttonBorder),
							$rtfeldman$elm_css$Css$solid,
							theme.primary),
							A3(
							$rtfeldman$elm_css$Css$borderTop3,
							$rtfeldman$elm_css$Css$em(theme.buttonBorder),
							$rtfeldman$elm_css$Css$solid,
							theme.primary),
							$rtfeldman$elm_css$Css$borderTopLeftRadius(
							$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
							$rtfeldman$elm_css$Css$borderTopRightRadius(
							$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius)),
							A5(
							$rtfeldman$elm_css$Css$boxShadow5,
							$rtfeldman$elm_css$Css$px(0),
							$rtfeldman$elm_css$Css$px(0),
							A3(
								$author$project$My$Extra$ternary,
								infoPopUp,
								$rtfeldman$elm_css$Css$px(20),
								$rtfeldman$elm_css$Css$px(0)),
							A3(
								$author$project$My$Extra$ternary,
								infoPopUp,
								$rtfeldman$elm_css$Css$px(4),
								$rtfeldman$elm_css$Css$px(0)),
							A3($rtfeldman$elm_css$Css$rgb, 112, 145, 145)),
							A4(
							$rtfeldman$elm_css$Css$padding4,
							$rtfeldman$elm_css$Css$em(theme.buttonPadding),
							$rtfeldman$elm_css$Css$em(theme.buttonPadding),
							$rtfeldman$elm_css$Css$em(theme.buttonPadding),
							$rtfeldman$elm_css$Css$em(0)),
							$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$absolute),
							$rtfeldman$elm_css$Css$transform(
							A2(
								$rtfeldman$elm_css$Css$translate2,
								$rtfeldman$elm_css$Css$pct(-50),
								$rtfeldman$elm_css$Css$pct(
									A3($author$project$My$Extra$ternary, infoPopUp, -99.75, 0)))),
							A2(
							$rtfeldman$elm_css$Css$property,
							'transition',
							A2(
								$elm$core$String$join,
								', ',
								_List_fromArray(
									['transform ' + $author$project$Theme$popUpDurationMS, 'box-shadow ' + $author$project$Theme$popUpDurationMS, gradientStop + (' ' + $author$project$Theme$popUpDurationMS)]))),
							A2(
							$rtfeldman$elm_css$Css$property,
							'user-select',
							A3($author$project$My$Extra$ternary, infoPopUp, 'auto', 'none')),
							$rtfeldman$elm_css$Css$width(
							A3(
								$rtfeldman$elm_css$Css$calc,
								$rtfeldman$elm_css$Css$ch(80),
								$rtfeldman$elm_css$Css$plus,
								$rtfeldman$elm_css$Css$em((4 * theme.buttonPadding) + theme.buttonHeight))),
							$rtfeldman$elm_css$Css$height(h)
						]))
				]),
			_List_fromArray(
				[
					$rtfeldman$elm_css$Css$Global$global(
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Css$Global$selector,
							'@property ' + gradientStop,
							_List_fromArray(
								[
									A2($rtfeldman$elm_css$Css$property, 'syntax', '\'<percentage>\''),
									A2($rtfeldman$elm_css$Css$property, 'inherits', 'false'),
									A2($rtfeldman$elm_css$Css$property, 'initial-value', gradientStopOffValue)
								]))
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[$rtfeldman$elm_css$Css$displayFlex]))
						]),
					_List_fromArray(
						[
							A2(
							$rtfeldman$elm_css$Html$Styled$div,
							_List_fromArray(
								[
									$rtfeldman$elm_css$Html$Styled$Attributes$css(
									_List_fromArray(
										[
											$rtfeldman$elm_css$Css$displayFlex,
											$rtfeldman$elm_css$Css$flexDirection($rtfeldman$elm_css$Css$column),
											$rtfeldman$elm_css$Css$height(h),
											A4(
											$rtfeldman$elm_css$Css$padding4,
											$rtfeldman$elm_css$Css$em(0),
											$rtfeldman$elm_css$Css$em(theme.buttonPadding),
											$rtfeldman$elm_css$Css$em(0),
											$rtfeldman$elm_css$Css$em(theme.buttonPadding))
										]))
								]),
							_List_fromArray(
								[
									A3(
									$author$project$View$Info$worldButton,
									theme,
									tabMsg(
										$author$project$PopUp$InfoTabMsg($author$project$PopUp$WorldTab)),
									popups),
									A3(
									$author$project$View$Info$docsButton,
									theme,
									tabMsg(
										$author$project$PopUp$InfoTabMsg($author$project$PopUp$DocsTab)),
									popups),
									A3(
									$author$project$View$Info$monitorButton,
									theme,
									tabMsg(
										$author$project$PopUp$InfoTabMsg($author$project$PopUp$MonitorTab)),
									popups),
									A3(
									$author$project$View$Info$attributionButton,
									theme,
									tabMsg(
										$author$project$PopUp$InfoTabMsg($author$project$PopUp$AttributionsTab)),
									popups)
								])),
							A5($author$project$View$Info$tabContent, theme, copyMsg, popups, errors, h)
						]))
				]));
	});
var $author$project$PixScreen$ratio = 1.6;
var $author$project$PixScreen$tag = 'shutterbug-pixels';
var $author$project$Main$viewPixelsColumn = F7(
	function (theme, cameras, screen, colour, popups, maybeNotification, errors) {
		var shadowZ = 1;
		var pixelZ = 0;
		var onZ = 3;
		var offZ = 2;
		var dim = $author$project$PixScreen$dimensions(screen);
		var h = $author$project$PixScreen$fit(screen) ? ($author$project$PixScreen$border(screen) ? ('calc(100vh - 2 * ' + ($elm$core$String$fromFloat(theme.buttonBorder) + 'em)')) : '100vh') : ($elm$core$String$fromInt(dim.h) + 'px');
		var w = $author$project$PixScreen$fit(screen) ? ($author$project$PixScreen$border(screen) ? ('calc( ' + ($elm$core$String$fromFloat($author$project$PixScreen$ratio) + (' * ( 100vh - 2 * ' + ($elm$core$String$fromFloat(theme.buttonBorder) + 'em ) )')))) : ('calc( ' + ($elm$core$String$fromFloat($author$project$PixScreen$ratio) + ' * 100vh )'))) : ($elm$core$String$fromInt(dim.w) + 'px');
		var b = 'border: ' + ($elm$core$String$fromFloat(theme.buttonBorder) + ('em solid ' + ($author$project$Theme$colToString(theme.primary) + ('; border-radius: ' + ($elm$core$String$fromFloat(theme.buttonBorderRadius) + ('em;' + ('background-color: ' + ($author$project$Theme$colToString(theme.primary) + ';'))))))));
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					_List_fromArray(
						[
							A2($rtfeldman$elm_css$Css$property, 'display', 'grid'),
							A2($rtfeldman$elm_css$Css$property, 'grid-template-columns', '50% 50%'),
							A2($rtfeldman$elm_css$Css$property, 'grid-template-rows', 'auto'),
							A2($rtfeldman$elm_css$Css$property, 'overflow', 'hidden')
						]))
				]),
			_List_fromArray(
				[
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							A3(
								$author$project$My$Extra$appendIf,
								$author$project$PixScreen$border(screen),
								_List_fromArray(
									[
										function (any) {
										return A6(
											$rtfeldman$elm_css$Css$boxShadow6,
											$rtfeldman$elm_css$Css$inset,
											$rtfeldman$elm_css$Css$px(0),
											$rtfeldman$elm_css$Css$px(0),
											A3(
												$author$project$My$Extra$ternary,
												any,
												$rtfeldman$elm_css$Css$px(20),
												$rtfeldman$elm_css$Css$px(0)),
											A3(
												$author$project$My$Extra$ternary,
												any,
												$rtfeldman$elm_css$Css$px(4),
												$rtfeldman$elm_css$Css$px(0)),
											A3($rtfeldman$elm_css$Css$rgb, 0, 0, 0));
									}(
										$author$project$PopUp$anyPixelsArea(popups)),
										A2($rtfeldman$elm_css$Css$property, 'grid-column', '1 / span 2'),
										A2($rtfeldman$elm_css$Css$property, 'grid-row', '1 / span 1'),
										$rtfeldman$elm_css$Css$height(
										$rtfeldman$elm_css$Css$pct(100)),
										$rtfeldman$elm_css$Css$pointerEvents($rtfeldman$elm_css$Css$none),
										$rtfeldman$elm_css$Css$Transitions$transition(
										_List_fromArray(
											[
												$rtfeldman$elm_css$Css$Transitions$boxShadow($author$project$Theme$popUpDuration)
											])),
										$rtfeldman$elm_css$Css$zIndex(
										$rtfeldman$elm_css$Css$int(shadowZ))
									]),
								_List_fromArray(
									[
										$rtfeldman$elm_css$Css$borderRadius(
										$rtfeldman$elm_css$Css$em(theme.buttonBorderRadius))
									])))
						]),
					_List_Nil),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									A2($rtfeldman$elm_css$Css$property, 'grid-column', '1 / span 2'),
									A2($rtfeldman$elm_css$Css$property, 'grid-row', '1 / span 1'),
									$rtfeldman$elm_css$Css$zIndex(
									$rtfeldman$elm_css$Css$int(pixelZ))
								]))
						]),
					_List_fromArray(
						[
							A3(
							$rtfeldman$elm_css$Html$Styled$node,
							$author$project$PixScreen$tag,
							A2(
								$author$project$My$Extra$appendWhen,
								_List_fromArray(
									[
										A2($rtfeldman$elm_css$Html$Styled$Attributes$attribute, $author$project$PixScreen$attributes.width, w),
										A2($rtfeldman$elm_css$Html$Styled$Attributes$attribute, $author$project$PixScreen$attributes.height, h),
										A2($rtfeldman$elm_css$Html$Styled$Attributes$attribute, $author$project$PixScreen$attributes.colour, colour)
									]),
								_List_fromArray(
									[
										_Utils_Tuple2(
										$author$project$PixScreen$fit(screen),
										_List_fromArray(
											[
												$rtfeldman$elm_css$Html$Styled$Attributes$css(
												_List_fromArray(
													[
														A2($rtfeldman$elm_css$Css$property, 'grid-column', '1 / span 2'),
														A2($rtfeldman$elm_css$Css$property, 'grid-row', '1 / span 1')
													]))
											])),
										_Utils_Tuple2(
										$author$project$PixScreen$border(screen),
										_List_fromArray(
											[
												A2($rtfeldman$elm_css$Html$Styled$Attributes$attribute, $author$project$PixScreen$attributes.border, b)
											])),
										_Utils_Tuple2(
										$author$project$PixScreen$selectable(screen),
										_List_fromArray(
											[
												A2($rtfeldman$elm_css$Html$Styled$Attributes$attribute, $author$project$PixScreen$attributes.selectable, 'true')
											])),
										_Utils_Tuple2(
										!_Utils_eq(maybeNotification, $elm$core$Maybe$Nothing),
										_List_fromArray(
											[
												A2($rtfeldman$elm_css$Html$Styled$Attributes$attribute, $author$project$PixScreen$attributes.kaput, 'true')
											]))
									])),
							_List_Nil)
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									A2($rtfeldman$elm_css$Css$property, 'grid-column', '1 / span 1'),
									A2($rtfeldman$elm_css$Css$property, 'grid-row', '1 / span 1'),
									A2($rtfeldman$elm_css$Css$property, 'align-self', 'center'),
									A2($rtfeldman$elm_css$Css$property, 'justify-self', 'start'),
									$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$relative),
									$rtfeldman$elm_css$Css$zIndex(
									$rtfeldman$elm_css$Css$int(
										A3(
											$author$project$My$Extra$ternary,
											$author$project$PopUp$cameras(popups),
											onZ,
											offZ)))
								]))
						]),
					_List_fromArray(
						[
							$author$project$View$Cameras$popUp(
							{
								assocMsg: $author$project$Main$IssuedCameraAssociations,
								autoClosePopUp: popups.autoCloseCameras,
								autoMsg: $author$project$Main$ToggledAutoCloseCameras,
								cameras: cameras,
								clearMsg: $author$project$Main$ClearedCameraSelections,
								on: $author$project$PopUp$cameras(popups),
								stitchMsg: $author$project$Main$StitchingMsg,
								theme: theme,
								toggleCamMsg: $author$project$Main$ToggledCamera,
								toggleSelModeMsg: $author$project$Main$ToggledCameraSelectionMode
							})
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									A2($rtfeldman$elm_css$Css$property, 'grid-column', '1 / span 2'),
									A2($rtfeldman$elm_css$Css$property, 'grid-row', '1 / span 1'),
									A2($rtfeldman$elm_css$Css$property, 'place-self', 'end center'),
									$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$relative),
									$rtfeldman$elm_css$Css$zIndex(
									$rtfeldman$elm_css$Css$int(
										A3(
											$author$project$My$Extra$ternary,
											$author$project$PopUp$info(popups),
											onZ,
											offZ)))
								]))
						]),
					_List_fromArray(
						[
							A5(
							$author$project$View$Info$popup,
							theme,
							function (text) {
								return $author$project$Main$WroteTextToClipboard(text);
							},
							function (tab) {
								return $author$project$Main$ChangedInfoTab(tab);
							},
							popups,
							errors)
						])),
					A2(
					$rtfeldman$elm_css$Html$Styled$div,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							_List_fromArray(
								[
									A2($rtfeldman$elm_css$Css$property, 'grid-column', '2 / span 1'),
									A2($rtfeldman$elm_css$Css$property, 'grid-row', '1 / span 1'),
									A2($rtfeldman$elm_css$Css$property, 'place-self', 'center end'),
									$rtfeldman$elm_css$Css$position($rtfeldman$elm_css$Css$relative),
									$rtfeldman$elm_css$Css$zIndex(
									$rtfeldman$elm_css$Css$int(
										A3(
											$author$project$My$Extra$ternary,
											$author$project$PopUp$settings(popups),
											onZ,
											offZ)))
								]))
						]),
					_List_fromArray(
						[
							A5(
							$author$project$View$Settings$popUp,
							theme,
							{
								border: $author$project$Main$ToggledOutputBorder,
								fit: $author$project$Main$ToggledOutputSize,
								full: function (pix) {
									return $author$project$Main$ToggledFullscreen(
										!$author$project$PixScreen$full(pix));
								},
								inaction: $author$project$Main$Identity,
								lock: function (pix) {
									return $author$project$Main$ToggledPointerLock(
										!$author$project$PixScreen$locked(pix));
								},
								selectable: $author$project$Main$ToggledTextSelect
							},
							screen,
							$author$project$PopUp$settings(popups),
							maybeNotification)
						]))
				]));
	});
var $author$project$Main$viewKaputAfterRunning = F5(
	function (theme, notification, errors, screen, data) {
		return A3(
			$author$project$View$mainColumns,
			A2($author$project$Main$viewKaputControlColumn, theme, notification),
			A7(
				$author$project$Main$viewPixelsColumn,
				data.theme,
				$author$project$Cameras$init,
				screen,
				$author$project$PixScreen$backgroundColour,
				data.popUps,
				$elm$core$Maybe$Just(notification),
				errors),
			A3(
				$author$project$Main$viewMetaColumn,
				theme,
				$elm$core$Maybe$Just(notification),
				data));
	});
var $author$project$Kaput$viewNotification = function (notification) {
	return A2(
		$elm$core$List$indexedMap,
		F2(
			function (i, s) {
				return (!i) ? A2(
					$rtfeldman$elm_css$Html$Styled$h2,
					_List_Nil,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text(s)
						])) : A2(
					$rtfeldman$elm_css$Html$Styled$p,
					_List_Nil,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$text(s)
						]));
			}),
		A2(
			$elm$core$List$map,
			function (s) {
				return A2($elm$core$String$endsWith, '.', s) ? A2($elm$core$String$dropRight, 1, s) : s;
			},
			A2(
				$elm$core$String$split,
				'\n',
				$author$project$LifeCycle$explainWSCloseEvent(notification))));
};
var $author$project$Main$viewKaputBeforeRunning = F4(
	function (theme, notification, errors, maybeData) {
		return function (middle) {
			return A5(
				$author$project$View$startingGrid,
				theme,
				'ta-ta from',
				A2($rtfeldman$elm_css$Html$Styled$div, _List_Nil, _List_Nil),
				middle,
				function () {
					if (maybeData.$ === 'Just') {
						var data = maybeData.a;
						return A3(
							$author$project$Main$viewShutterbugActions,
							theme,
							$elm$core$Maybe$Just(notification),
							data);
					} else {
						return $rtfeldman$elm_css$Html$Styled$text('');
					}
				}());
		}(
			A2(
				$rtfeldman$elm_css$Html$Styled$div,
				_List_fromArray(
					[
						$rtfeldman$elm_css$Html$Styled$Attributes$css($author$project$Theme$startingGridMiddleDivStyle)
					]),
				A2(
					$elm$core$List$append,
					$author$project$Kaput$viewNotification(notification),
					A2(
						$author$project$My$Extra$useIf,
						$elm$core$List$length(errors) > 0,
						_List_fromArray(
							[
								A2(
								$rtfeldman$elm_css$Html$Styled$h3,
								_List_Nil,
								_List_fromArray(
									[
										$rtfeldman$elm_css$Html$Styled$text('Subsequent errors')
									])),
								A2($author$project$View$errorsList, theme, errors)
							])))));
	});
var $author$project$Main$viewKaput = function (kaput) {
	if (kaput.$ === 'BeforeRunning') {
		var notification = kaput.a;
		var errors = kaput.b;
		var data = kaput.c;
		return A4(
			$author$project$Main$viewKaputBeforeRunning,
			data.theme,
			notification,
			errors,
			$elm$core$Maybe$Just(data));
	} else {
		var notification = kaput.a;
		var errors = kaput.b;
		var screen = kaput.c;
		var data = kaput.d;
		return A5($author$project$Main$viewKaputAfterRunning, data.theme, notification, errors, screen, data);
	}
};
var $author$project$Devoir$Pause = {$: 'Pause'};
var $author$project$Devoir$Play = {$: 'Play'};
var $author$project$Cameras$availability = function (_v0) {
	var registry = _v0.b;
	var _v1 = $elm$core$List$length(registry.allRegistered);
	switch (_v1) {
		case 0:
			return 'No cameras available.';
		case 1:
			return '1 camera available.';
		default:
			var n = _v1;
			return $elm$core$String$fromInt(n) + ' cameras available.';
	}
};
var $author$project$Main$viewControlColumn = F4(
	function (theme, cameras, _v0, popups) {
		var paused = _v0.a;
		return A2(
			$rtfeldman$elm_css$Html$Styled$div,
			_List_fromArray(
				[
					$rtfeldman$elm_css$Html$Styled$Attributes$css(
					$author$project$Theme$controlColumnDivStyle(theme))
				]),
			_List_fromArray(
				[
					A4(
					$author$project$My$Html$safeClickButton,
					$author$project$Main$ToggledCamerasPopUp,
					$author$project$Main$Identity,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							function ($) {
								return $.standard;
							}(
								$author$project$Theme$buttonWithMargin(theme))),
							$rtfeldman$elm_css$Html$Styled$Attributes$title(
							'Select cameras to view.\n' + $author$project$Cameras$availability(cameras))
						]),
					_List_fromArray(
						[
							A4(
							$author$project$View$expanderButtonIcon,
							theme,
							'switch_video',
							$author$project$PopUp$cameras(popups),
							$author$project$PopUp$anyPixelsArea(popups))
						])),
					paused ? A4(
					$author$project$My$Html$safeClickButton,
					$author$project$Main$IssuedUICommand($author$project$Devoir$Play),
					$author$project$Main$Identity,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							function ($) {
								return $.standard;
							}(
								$author$project$Theme$buttonWithMargin(theme))),
							$rtfeldman$elm_css$Html$Styled$Attributes$title('Play world.\n')
						]),
					_List_fromArray(
						[
							A2($author$project$View$buttonIcon, theme, 'play_arrow')
						])) : A4(
					$author$project$My$Html$safeClickButton,
					$author$project$Main$IssuedUICommand($author$project$Devoir$Pause),
					$author$project$Main$Identity,
					_List_fromArray(
						[
							$rtfeldman$elm_css$Html$Styled$Attributes$css(
							function ($) {
								return $.standard;
							}(
								$author$project$Theme$buttonWithMargin(theme))),
							$rtfeldman$elm_css$Html$Styled$Attributes$title('Pause world.\n')
						]),
					_List_fromArray(
						[
							A2($author$project$View$buttonIcon, theme, 'pause')
						]))
				]));
	});
var $author$project$Main$viewRunning = F4(
	function (cameras, screen, devoir, data) {
		var pixelsColumn = A7($author$project$Main$viewPixelsColumn, data.theme, cameras, screen, $author$project$PixScreen$backgroundColour, data.popUps, $elm$core$Maybe$Nothing, _List_Nil);
		var metaColumn = A3($author$project$Main$viewMetaColumn, data.theme, $elm$core$Maybe$Nothing, data);
		var controlColumn = A4($author$project$Main$viewControlColumn, data.theme, cameras, devoir, data.popUps);
		return A3($author$project$View$mainColumns, controlColumn, pixelsColumn, metaColumn);
	});
var $author$project$Main$viewModel = function (model) {
	switch (model.$) {
		case 'Starting':
			var data = model.a;
			var theme = $author$project$Theme$withInitialDefault(data.maybeTheme);
			return (!$author$project$Static$isComplete(data)) ? A5(
				$author$project$View$startingGrid,
				theme,
				'welcome to ',
				A2(
					$author$project$View$tickedList,
					theme,
					A2($elm$core$List$take, 1, $author$project$View$startingTasks)),
				$rtfeldman$elm_css$Html$Styled$text(''),
				$rtfeldman$elm_css$Html$Styled$text('')) : (data.awaitingShutterbug ? A5(
				$author$project$View$startingGrid,
				theme,
				'welcome to ',
				A2(
					$author$project$View$tickedList,
					theme,
					A2($elm$core$List$take, 2, $author$project$View$startingTasks)),
				function () {
					var _v1 = data.shutterbugSource;
					if (_v1.$ === 'MakeNew') {
						return $rtfeldman$elm_css$Html$Styled$text('');
					} else {
						var shutterbugData = _v1.a;
						return A5(
							$author$project$View$Shutterbug$inputBox,
							theme,
							$author$project$Main$SubmittedShutterbug,
							function (input) {
								return $author$project$Main$ChangedShutterbugInputValue(
									{shutterbugInput: input, shutterbugUnmasked: shutterbugData.shutterbugUnmasked});
							},
							$author$project$Main$ChangedShutterbugInputValue(
								{shutterbugInput: shutterbugData.shutterbugInput, shutterbugUnmasked: !shutterbugData.shutterbugUnmasked}),
							{input: shutterbugData.shutterbugInput, unmasked: shutterbugData.shutterbugUnmasked});
					}
				}(),
				$rtfeldman$elm_css$Html$Styled$text('')) : (_Utils_eq(data.websockets, $author$project$Main$Connecting) ? A5(
				$author$project$View$startingGrid,
				theme,
				'welcome to ',
				A2(
					$author$project$View$tickedList,
					theme,
					A2($elm$core$List$take, 3, $author$project$View$startingTasks)),
				$rtfeldman$elm_css$Html$Styled$text(''),
				A3($author$project$Main$viewShutterbugActions, theme, $elm$core$Maybe$Nothing, data)) : ((_Utils_eq(data.websockets, $author$project$Main$Connected) && (_Utils_eq(data.maybeCameras, $elm$core$Maybe$Nothing) || _Utils_eq(data.maybeDevoir, $elm$core$Maybe$Nothing))) ? A5(
				$author$project$View$startingGrid,
				theme,
				'welcome to ',
				A2(
					$author$project$View$tickedList,
					theme,
					A2($elm$core$List$take, 4, $author$project$View$startingTasks)),
				$rtfeldman$elm_css$Html$Styled$text(''),
				A3($author$project$Main$viewShutterbugActions, theme, $elm$core$Maybe$Nothing, data)) : (((!data.sentCameraAssociations) && (!$author$project$Cameras$associated(
				A2($elm$core$Maybe$withDefault, $author$project$Cameras$init, data.maybeCameras)))) ? A5(
				$author$project$View$startingGrid,
				theme,
				'welcome to ',
				A2(
					$author$project$View$tickedList,
					theme,
					A2($elm$core$List$take, 5, $author$project$View$startingTasks)),
				$author$project$View$Cameras$starting(
					{
						assocMsg: $author$project$Main$IssuedCameraAssociations,
						cameras: A2($elm$core$Maybe$withDefault, $author$project$Cameras$init, data.maybeCameras),
						extantUrl: data.extantUrl,
						shutterbugActionsPopUp: data.shutterbugActionsPopUp,
						source: function () {
							var _v2 = data.shutterbugSource;
							if (_v2.$ === 'MakeNew') {
								return $author$project$Shutterbug$MakeNew;
							} else {
								return $author$project$Shutterbug$UseExisting;
							}
						}(),
						stitchMsg: $author$project$Main$StitchingMsg,
						theme: $author$project$Theme$withInitialDefault(data.maybeTheme),
						toggleMsg: $author$project$Main$ToggledCamera
					}),
				A3($author$project$Main$viewShutterbugActions, theme, $elm$core$Maybe$Nothing, data)) : A5(
				$author$project$View$startingGrid,
				theme,
				'welcome to ',
				A2(
					$author$project$View$tickedList,
					theme,
					A2($elm$core$List$take, 6, $author$project$View$startingTasks)),
				$rtfeldman$elm_css$Html$Styled$text(''),
				A3($author$project$Main$viewShutterbugActions, theme, $elm$core$Maybe$Nothing, data))))));
		case 'Running':
			var cameras = model.a;
			var screen = model.b;
			var devoir = model.c;
			var data = model.d;
			return A4($author$project$Main$viewRunning, cameras, screen, devoir, data);
		case 'Errored':
			var theme = model.a;
			var errors = model.b;
			return A2($author$project$Main$viewErrored, theme, errors);
		default:
			var kaput = model.a;
			return $author$project$Main$viewKaput(kaput);
	}
};
var $author$project$Main$view = function (model) {
	return {
		body: A2(
			$elm$core$List$map,
			$rtfeldman$elm_css$Html$Styled$toUnstyled,
			_List_fromArray(
				[
					$author$project$View$globalStyleNode(
					function () {
						switch (model.$) {
							case 'Starting':
								var data = model.a;
								return $author$project$Theme$withInitialDefault(data.maybeTheme);
							case 'Running':
								var data = model.d;
								return data.theme;
							case 'Errored':
								var theme = model.a;
								return theme;
							default:
								var kaput = model.a;
								return $author$project$Kaput$theme(kaput);
						}
					}()),
					$author$project$Main$viewModel(model)
				])),
		title: 'mechane'
	};
};
var $author$project$Main$main = $elm$browser$Browser$document(
	{init: $author$project$Main$init, subscriptions: $author$project$Main$subscriptions, update: $author$project$Main$update, view: $author$project$Main$view});
_Platform_export({'Main':{'init':$author$project$Main$main($elm$json$Json$Decode$value)(0)}});}(this));