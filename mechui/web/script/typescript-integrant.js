/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./build/crule/crule.js":
/*!******************************!*\
  !*** ./build/crule/crule.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "toString": () => (/* binding */ toString)
/* harmony export */ });
/* harmony import */ var _stitches_stringify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @stitches/stringify */ "./node_modules/@stitches/stringify/dist/index.mjs");

function toString(sheet, prettify = false) {
    const s = sheet
        .map((rule) => {
        return rule.selector + " " + `{\n${(0,_stitches_stringify__WEBPACK_IMPORTED_MODULE_0__.stringify)(rule.properties)}\n}\n`;
    })
        .join("\n");
    if (prettify) {
        return s.replaceAll(";", ";\n");
    }
    return s;
}
//# sourceMappingURL=crule.js.map

/***/ }),

/***/ "./build/custom-elements/define.js":
/*!*****************************************!*\
  !*** ./build/custom-elements/define.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _growing_div_growing_div__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./growing-div/growing-div */ "./build/custom-elements/growing-div/growing-div.js");
/* harmony import */ var _shrinking_div_shrinking_div__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shrinking-div/shrinking-div */ "./build/custom-elements/shrinking-div/shrinking-div.js");
/* harmony import */ var _shutterbug_pixels_mechane_asciidoc_mechane_asciidoc__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shutterbug-pixels/mechane-asciidoc/mechane-asciidoc */ "./build/custom-elements/shutterbug-pixels/mechane-asciidoc/mechane-asciidoc.js");
/* harmony import */ var _shutterbug_pixels_mechane_button_mechane_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shutterbug-pixels/mechane-button/mechane-button */ "./build/custom-elements/shutterbug-pixels/mechane-button/mechane-button.js");
/* harmony import */ var _shutterbug_pixels_mechane_pre_mechane_pre__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shutterbug-pixels/mechane-pre/mechane-pre */ "./build/custom-elements/shutterbug-pixels/mechane-pre/mechane-pre.js");
/* harmony import */ var _shutterbug_pixels_mechane_textfield_mechane_textfield__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shutterbug-pixels/mechane-textfield/mechane-textfield */ "./build/custom-elements/shutterbug-pixels/mechane-textfield/mechane-textfield.js");
/* harmony import */ var _shutterbug_pixels_shutterbug_pixels__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shutterbug-pixels/shutterbug-pixels */ "./build/custom-elements/shutterbug-pixels/shutterbug-pixels.js");
/* harmony import */ var _stitching_chooser_stitching_chooser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./stitching-chooser/stitching-chooser */ "./build/custom-elements/stitching-chooser/stitching-chooser.js");








customElements.define(_growing_div_growing_div__WEBPACK_IMPORTED_MODULE_0__.GrowingDiv.tag, _growing_div_growing_div__WEBPACK_IMPORTED_MODULE_0__.GrowingDiv);
customElements.define(_shrinking_div_shrinking_div__WEBPACK_IMPORTED_MODULE_1__.ShrinkingDiv.tag, _shrinking_div_shrinking_div__WEBPACK_IMPORTED_MODULE_1__.ShrinkingDiv);
customElements.define(_shutterbug_pixels_shutterbug_pixels__WEBPACK_IMPORTED_MODULE_6__.ShutterbugPixels.tag, _shutterbug_pixels_shutterbug_pixels__WEBPACK_IMPORTED_MODULE_6__.ShutterbugPixels);
customElements.define(_shutterbug_pixels_mechane_asciidoc_mechane_asciidoc__WEBPACK_IMPORTED_MODULE_2__.MechaneAsciidoc.tag, _shutterbug_pixels_mechane_asciidoc_mechane_asciidoc__WEBPACK_IMPORTED_MODULE_2__.MechaneAsciidoc);
customElements.define(_shutterbug_pixels_mechane_button_mechane_button__WEBPACK_IMPORTED_MODULE_3__.MechaneButton.tag, _shutterbug_pixels_mechane_button_mechane_button__WEBPACK_IMPORTED_MODULE_3__.MechaneButton);
customElements.define(_shutterbug_pixels_mechane_pre_mechane_pre__WEBPACK_IMPORTED_MODULE_4__.MechanePre.tag, _shutterbug_pixels_mechane_pre_mechane_pre__WEBPACK_IMPORTED_MODULE_4__.MechanePre);
customElements.define(_shutterbug_pixels_mechane_textfield_mechane_textfield__WEBPACK_IMPORTED_MODULE_5__.MechaneTextfield.tag, _shutterbug_pixels_mechane_textfield_mechane_textfield__WEBPACK_IMPORTED_MODULE_5__.MechaneTextfield);
customElements.define(_stitching_chooser_stitching_chooser__WEBPACK_IMPORTED_MODULE_7__.StitchingChooser.tag, _stitching_chooser_stitching_chooser__WEBPACK_IMPORTED_MODULE_7__.StitchingChooser);
//# sourceMappingURL=define.js.map

/***/ }),

/***/ "./build/custom-elements/font.js":
/*!***************************************!*\
  !*** ./build/custom-elements/font.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "fonts": () => (/* binding */ fonts),
/* harmony export */   "isMaterialIconsFamily": () => (/* binding */ isMaterialIconsFamily),
/* harmony export */   "materialIcons": () => (/* binding */ materialIcons),
/* harmony export */   "materialIconsClass": () => (/* binding */ materialIconsClass),
/* harmony export */   "setMaterialIconsClass": () => (/* binding */ setMaterialIconsClass)
/* harmony export */ });
// Cascadia Code.
const cascadiaCodeFontFaceProperties = {
    fontFamily: "CascadiaCode",
    src: "url('/font/CascadiaCode-2111.01/woff2/CascadiaCode.woff2') format('woff2')," +
        "url('/font/CascadiaCode-2111.01/ttf/CascadiaCode.ttf') format('truetype')",
};
const cascadiaCode = [
    {
        selector: "@font-face",
        properties: cascadiaCodeFontFaceProperties,
    },
];
// Material Icons.
const materialIconsClass = {
    regular: "material-icons",
    outlined: "material-icons-outlined",
    round: "material-icons-round",
    sharp: "material-icons-sharp",
    twoTone: "material-icons-two-tone",
};
function isMaterialIconsFamily(value) {
    return Object.values(materialIconsClass).includes(value);
}
function setMaterialIconsClass(e, family) {
    for (const iconClass of Object.values(materialIconsClass)) {
        if (family == iconClass) {
            e.classList.add(iconClass);
        }
        else {
            e.classList.remove(iconClass);
        }
    }
}
const materialIconsFontFaceProperties = {
    fontFamily: "Material Icons",
    fontStyle: "normal",
    fontWeight: 400,
    src: "url('/font/material-design-icons/MaterialIcons.woff2') format('woff2')",
};
const materialIconsOutlinedFontFaceProperties = {
    fontFamily: "Material Icons Outlined",
    fontStyle: "normal",
    fontWeight: 400,
    src: "url('/font/material-design-icons/MaterialIconsOutlined.woff2') format('woff2')",
};
const materialIconsRoundFontFaceProperties = {
    fontFamily: "Material Icons Round",
    fontStyle: "normal",
    fontWeight: 400,
    src: "url('/font/material-design-icons/MaterialIconsRound.woff2') format('woff2')",
};
const materialIconsSharpFontFaceProperties = {
    fontFamily: "Material Icons Sharp",
    fontStyle: "normal",
    fontWeight: 400,
    src: "url('/font/material-design-icons/MaterialIconsSharp.woff2') format('woff2')",
};
const materialIconsTwoToneFontFaceProperties = {
    fontFamily: "Material Icons Two Tone",
    fontStyle: "normal",
    fontWeight: 400,
    src: "url('/font/material-design-icons/MaterialIconsTwoTone.woff2') format('woff2')",
};
const materialIconsBase = {
    fontWeight: "normal",
    fontStyle: "normal",
    fontSize: "24px",
    lineHeight: 1,
    letterSpacing: "normal",
    textTransform: "none",
    display: "inline-block",
    whiteSpace: "nowrap",
    wordWrap: "normal",
    direction: "ltr",
    textRendering: "optimizeLegibility",
    fontFeatureSettings: '"liga"',
};
const materialIcons = [
    {
        selector: "@font-face",
        properties: materialIconsFontFaceProperties,
    },
    {
        selector: `.${materialIconsClass.regular}`,
        properties: {
            fontFamily: "Material Icons",
            ...materialIconsBase,
        },
    },
    {
        selector: "@font-face",
        properties: materialIconsOutlinedFontFaceProperties,
    },
    {
        selector: `.${materialIconsClass.outlined}`,
        properties: {
            fontFamily: "Material Icons Outlined",
            ...materialIconsBase,
        },
    },
    {
        selector: "@font-face",
        properties: materialIconsRoundFontFaceProperties,
    },
    {
        selector: `.${materialIconsClass.round}`,
        properties: {
            fontFamily: "Material Icons Round",
            ...materialIconsBase,
        },
    },
    {
        selector: "@font-face",
        properties: materialIconsSharpFontFaceProperties,
    },
    {
        selector: `.${materialIconsClass.sharp}`,
        properties: {
            fontFamily: "Material Icons Sharp",
            ...materialIconsBase,
        },
    },
    {
        selector: "@font-face",
        properties: materialIconsTwoToneFontFaceProperties,
    },
    {
        selector: `.${materialIconsClass.twoTone}`,
        properties: {
            fontFamily: "Material Icons Two Tone",
            ...materialIconsBase,
        },
    },
];
// CSS style rules for the default fonts used by the Mechane UI. They exist for
// the sake of custom elements which need to specify the CSS style rules of
// fonts they use even if the fonts are already globally defined.
const fonts = cascadiaCode.concat(materialIcons);
//# sourceMappingURL=font.js.map

/***/ }),

/***/ "./build/custom-elements/growing-div/growing-div.js":
/*!**********************************************************!*\
  !*** ./build/custom-elements/growing-div/growing-div.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GrowingDiv": () => (/* binding */ GrowingDiv)
/* harmony export */ });
/* harmony import */ var _crule_crule__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../crule/crule */ "./build/crule/crule.js");
/* harmony import */ var _shrinking_div_shrinking_div__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shrinking-div/shrinking-div */ "./build/custom-elements/shrinking-div/shrinking-div.js");


// A GrowingDiv scales its content from zero to one over a given duration.
//
// As a custom element with time varying effects, for it to be used by the Elm
// language integrant, the start time is cached statically by the value of the
// name attribute. If Elm inserts a new instance with the same name the
// animation effect uses the cached start time.
//
// A GrowingDiv with a given name interacts with a ShrinkingDiv with the same
// name; when a GrowingDiv with a given name is created it deletes the cached
// start time for that name in the ShrinkingDiv's start time cache.
class GrowingDiv extends HTMLElement {
    static get observedAttributes() {
        return [
            GrowingDiv.nameAttribute,
            GrowingDiv.namesAttribute,
            GrowingDiv.sessionAttribute,
        ];
    }
    // Attribute giving the instance name.
    static nameAttribute = "name";
    // Attribute giving the instance names to keep cached for now.
    static namesAttribute = "names";
    // Attribute indicating a session. Start dates are cleared by a change of
    // session. It is also added to animation names for uniqueness.
    static sessionAttribute = "session";
    // The tag name of this custom element.
    static tag = "growing-div";
    // Start date cache for persistence of animation effects in elements created
    // by the Elm integrant.
    //
    // The date is the number of milliseconds since the UNIX epoch.
    static startDates = new Map();
    static session = "0";
    // Attribute controlling the duration of the grow.
    static durationAttribute = "duration";
    static defaultDuration = "2s";
    static animationNamePrefix = "growing-div";
    static divID = "growing-div";
    static slotName = "content";
    // Descendants of the shadowRoot.
    contentSlot = document.createElement("slot");
    div = document.createElement("div");
    constructor() {
        super();
        // Instance fields.
        this.div.id = GrowingDiv.divID;
        this.contentSlot.name = GrowingDiv.slotName;
        this.div.append(this.contentSlot);
        // Shadow root and style.
        const shadowRoot = this.attachShadow({ mode: "open" });
        const style = document.createElement("style");
        shadowRoot.append(style, this.div);
    }
    attributeChangedCallback(attrName, oldVal, newVal) {
        if (attrName == GrowingDiv.nameAttribute && oldVal && newVal) {
            // The element instance is being repurposed by the Elm integrant.
            GrowingDiv.startDates.set(newVal, Date.now());
            this.updateStyle(newVal);
        }
    }
    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return;
        }
        // Detect a new session and if so, clear all start dates.
        const session = this.getAttribute(GrowingDiv.sessionAttribute) ?? GrowingDiv.session;
        if (session != GrowingDiv.session) {
            GrowingDiv.startDates.clear();
            GrowingDiv.session = session;
        }
        const connectedDate = Date.now();
        // Record the start date for this name if a start date doesn't already
        // exist.
        const name = this.getAttribute(GrowingDiv.nameAttribute);
        if (name) {
            const date = GrowingDiv.startDates.get(name);
            if (!date) {
                GrowingDiv.startDates.set(name, connectedDate);
            }
            // Delete the equivalent shrinking-div start time, if any.
            _shrinking_div_shrinking_div__WEBPACK_IMPORTED_MODULE_1__.ShrinkingDiv.startDates["delete"](name);
        }
        // Prevent the start date cache accumulating dates for too many obsolete
        // names.
        const names = this.getAttribute(GrowingDiv.namesAttribute);
        if (names) {
            const keeps = names.split(",");
            for (const name of GrowingDiv.startDates.keys()) {
                if (!keeps.includes(name)) {
                    GrowingDiv.startDates.delete(name);
                }
            }
        }
        this.updateStyle();
    }
    updateStyle(newName) {
        const style = this.shadowRoot?.querySelector("style");
        if (!style) {
            return;
        }
        const name = newName ?? this.getAttribute(GrowingDiv.nameAttribute);
        if (!name) {
            return;
        }
        const start = GrowingDiv.startDates.get(name);
        if (!start) {
            return;
        }
        const duration = this.getAttribute(GrowingDiv.durationAttribute) ??
            GrowingDiv.defaultDuration;
        // Get the duration in milliseconds as a number.
        let interval;
        if (duration.endsWith("ms")) {
            interval = Number(duration.slice(0, -2));
        }
        else if (duration.endsWith("s")) {
            interval = Number(duration.slice(0, -1)) * 1000;
        }
        else {
            return;
        }
        if (isNaN(interval)) {
            return;
        }
        // Determine how far through the animation we are.
        const elapsed = Date.now() - start;
        let t = elapsed;
        if (elapsed < 0) {
            t = 0;
        }
        else if (elapsed > interval) {
            t = interval;
        }
        // Accumulate CSS rules in a style sheet.
        const sheet = new Array();
        if (t === interval) {
            return;
        }
        const animationKeyframes = {
            from: {
                transform: `scale(1, ${t / interval})`,
            },
            to: {
                transform: `scale(1, 1)`,
            },
        };
        const animationName = GrowingDiv.animationNamePrefix + name + GrowingDiv.session;
        sheet.push({
            selector: `@keyframes ${animationName}`,
            properties: animationKeyframes,
        });
        sheet.push({
            selector: `#${GrowingDiv.divID}`,
            properties: {
                animation: `
                ${animationName} 
                ${interval - t}ms
                `,
                animationFillMode: "forwards",
            },
        });
        // Apply the accumulated CSS rules.
        style.textContent = (0,_crule_crule__WEBPACK_IMPORTED_MODULE_0__.toString)(sheet);
    }
}

//# sourceMappingURL=growing-div.js.map

/***/ }),

/***/ "./build/custom-elements/shrinking-div/shrinking-div.js":
/*!**************************************************************!*\
  !*** ./build/custom-elements/shrinking-div/shrinking-div.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ShrinkingDiv": () => (/* binding */ ShrinkingDiv)
/* harmony export */ });
/* harmony import */ var _crule_crule__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../crule/crule */ "./build/crule/crule.js");
/* harmony import */ var _growing_div_growing_div__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../growing-div/growing-div */ "./build/custom-elements/growing-div/growing-div.js");


// A ShrinkingDiv scales its content to zero over a given duration then
// vanishes.
//
// As a custom element with time varying effects, for it to be used by the Elm
// language integrant, the start time is cached statically by the value of the
// name attribute. If Elm inserts a new instance with the same name the
// animation effect uses the cached start time.
//
// A ShrinkingDiv with a given name interacts with a GrowingDiv with the same
// name; when a ShrinkingDiv with a given name is created it deletes the cached
// start time for that name in the GrowingDiv's start time cache.
class ShrinkingDiv extends HTMLElement {
    static get observedAttributes() {
        return [
            ShrinkingDiv.nameAttribute,
            ShrinkingDiv.namesAttribute,
            ShrinkingDiv.sessionAttribute,
        ];
    }
    // Attribute giving the instance name.
    static nameAttribute = "name";
    // Attribute giving the instance names to keep cached for now.
    static namesAttribute = "names";
    // Attribute indicating a session. Start dates are cleared by a change of
    // session. It is also added to animation names for uniqueness.
    static sessionAttribute = "session";
    // The tag name of this custom element.
    static tag = "shrinking-div";
    // Start date cache for persistence of animation effects in elements created
    // by the Elm integrant.
    //
    // The date is the number of milliseconds since the UNIX epoch.
    static startDates = new Map();
    static session = "0";
    // Attribute controlling the duration of the shrink.
    static durationAttribute = "duration";
    static defaultDuration = "2s";
    static animationNamePrefix = "shrinking-div";
    static divID = "shrinking-div";
    static slotName = "content";
    // Descendants of the shadowRoot.
    contentSlot = document.createElement("slot");
    div = document.createElement("div");
    constructor() {
        super();
        // Instance fields.
        this.div.id = ShrinkingDiv.divID;
        this.contentSlot.name = ShrinkingDiv.slotName;
        this.div.append(this.contentSlot);
        // Shadow root and style.
        const shadowRoot = this.attachShadow({ mode: "open" });
        const style = document.createElement("style");
        shadowRoot.append(style, this.div);
    }
    attributeChangedCallback(attrName, oldVal, newVal) {
        if (attrName == ShrinkingDiv.nameAttribute && oldVal && newVal) {
            // The element instance is being repurposed by the Elm integrant.
            ShrinkingDiv.startDates.set(newVal, Date.now());
            this.updateStyle(newVal);
        }
    }
    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return;
        }
        const connectedDate = Date.now();
        // Detect a new session and if so, clear all start dates.
        const session = this.getAttribute(ShrinkingDiv.sessionAttribute) ??
            ShrinkingDiv.session;
        if (session != ShrinkingDiv.session) {
            ShrinkingDiv.startDates.clear();
            ShrinkingDiv.session = session;
        }
        // Record the start date for this name if a start date doesn't already
        // exist.
        const name = this.getAttribute(ShrinkingDiv.nameAttribute);
        if (name) {
            const date = ShrinkingDiv.startDates.get(name);
            if (!date) {
                ShrinkingDiv.startDates.set(name, connectedDate);
            }
            // Delete the equivalent growing-div start time, if any.
            _growing_div_growing_div__WEBPACK_IMPORTED_MODULE_1__.GrowingDiv.startDates["delete"](name);
        }
        // Prevent the start date cache accumulating dates for too many obsolete
        // names.
        const names = this.getAttribute(ShrinkingDiv.namesAttribute);
        if (names) {
            const keeps = names.split(",");
            for (const name of ShrinkingDiv.startDates.keys()) {
                if (!keeps.includes(name)) {
                    ShrinkingDiv.startDates.delete(name);
                }
            }
        }
        this.updateStyle();
    }
    updateStyle(newName) {
        const style = this.shadowRoot?.querySelector("style");
        if (!style) {
            return;
        }
        const name = newName ?? this.getAttribute(ShrinkingDiv.nameAttribute);
        if (!name) {
            return;
        }
        const start = ShrinkingDiv.startDates.get(name);
        if (!start) {
            return;
        }
        const duration = this.getAttribute(ShrinkingDiv.durationAttribute) ??
            ShrinkingDiv.defaultDuration;
        // Get the duration in milliseconds as a number.
        let interval;
        if (duration.endsWith("ms")) {
            interval = Number(duration.slice(0, -2));
        }
        else if (duration.endsWith("s")) {
            interval = Number(duration.slice(0, -1)) * 1000;
        }
        else {
            return;
        }
        if (isNaN(interval)) {
            return;
        }
        // Determine how far through the animation we are.
        const elapsed = Date.now() - start;
        let t = elapsed;
        if (elapsed < 0) {
            t = 0;
        }
        else if (elapsed > interval) {
            t = interval;
        }
        // Accumulate CSS rules in a style sheet.
        const sheet = new Array();
        if (t === interval) {
            sheet.push({
                selector: `#${ShrinkingDiv.divID}`,
                properties: {
                    display: "none",
                },
            });
            return;
        }
        const animationKeyframes = {
            from: {
                transform: `scale(1, ${1 - t / interval})`,
            },
            "99.9%": {
                position: "static",
            },
            to: {
                display: "none",
                position: "absolute",
                transform: "scale(1, 0)",
            },
        };
        const animationName = ShrinkingDiv.animationNamePrefix + name + ShrinkingDiv.session;
        sheet.push({
            selector: `@keyframes ${animationName}`,
            properties: animationKeyframes,
        });
        sheet.push({
            selector: `#${ShrinkingDiv.divID}`,
            properties: {
                animation: `
                ${animationName} 
                ${interval - t}ms
                `,
                animationFillMode: "forwards",
            },
        });
        // Apply the accumulated CSS rules.
        style.textContent = (0,_crule_crule__WEBPACK_IMPORTED_MODULE_0__.toString)(sheet);
    }
}

//# sourceMappingURL=shrinking-div.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/event.js":
/*!**********************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/event.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "newKeyboardUIEvent": () => (/* binding */ newKeyboardUIEvent),
/* harmony export */   "newMouseButtonUIEvent": () => (/* binding */ newMouseButtonUIEvent),
/* harmony export */   "newMouseMoveUIEvent": () => (/* binding */ newMouseMoveUIEvent),
/* harmony export */   "newMouseWheelUIEvent": () => (/* binding */ newMouseWheelUIEvent)
/* harmony export */ });
function newKeyboardUIEvent(keyDown, pressedKeys, ev) {
    return {
        keyboard: {
            altKey: ev.altKey,
            ctrlKey: ev.ctrlKey,
            isComposing: ev.isComposing,
            key: ev.key,
            keyDown: keyDown,
            metaKey: ev.metaKey,
            pressed: Array.from(pressedKeys),
            repeat: ev.repeat,
            shiftKey: ev.shiftKey,
        },
    };
}
function newMouseButtonUIEvent(mouseDown, ev) {
    return {
        mouseButton: {
            altKey: ev.altKey,
            button: ev.button,
            clientX: ev.clientX,
            clientY: ev.clientY,
            ctrlKey: ev.ctrlKey,
            detail: ev.detail,
            metaKey: ev.metaKey,
            mouseDown: mouseDown,
            shiftKey: ev.shiftKey,
            windowH: ev.view ? ev.view.innerHeight : 0,
            windowW: ev.view ? ev.view.innerWidth : 0,
        },
    };
}
// The width and height are those of the listened to element.
//
// The invertY flag is to mitigate a possible Y-axis inverting transform which
// has been applied to the element, so that the offsetY is still relative to the
// element's top.
function newMouseMoveUIEvent({ w, h, activeEditable, invertY, ev, }) {
    return {
        mouseMove: {
            activeEditable: activeEditable,
            altKey: ev.altKey,
            buttons: ev.buttons,
            clientX: ev.clientX,
            clientY: ev.clientY,
            ctrlKey: ev.ctrlKey,
            metaKey: ev.metaKey,
            movementX: ev.movementX,
            movementY: ev.movementY,
            pixelsW: w,
            pixelsH: h,
            pixelsOffsetX: ev.offsetX,
            pixelsOffsetY: invertY ? h - ev.offsetY : ev.offsetY,
            pointerLock: document.pointerLockElement !== null,
            shiftKey: ev.shiftKey,
            windowH: ev.view ? ev.view.innerHeight : 0,
            windowW: ev.view ? ev.view.innerWidth : 0,
        },
    };
}
function newMouseWheelUIEvent({ effectName, deltaX, deltaY, ev, }) {
    return {
        mouseWheel: {
            altKey: ev.altKey,
            clientX: ev.clientX,
            clientY: ev.clientY,
            ctrlKey: ev.ctrlKey,
            effectName: effectName,
            deltaX: deltaX,
            deltaY: deltaY,
            metaKey: ev.metaKey,
            shiftKey: ev.shiftKey,
            windowH: ev.view ? ev.view.innerHeight : 0,
            windowW: ev.view ? ev.view.innerWidth : 0,
        },
    };
}
//# sourceMappingURL=event.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/listen.js":
/*!***********************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/listen.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "addListeners": () => (/* binding */ addListeners)
/* harmony export */ });
/* harmony import */ var _event__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./event */ "./build/custom-elements/shutterbug-pixels/event.js");
/* harmony import */ var _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shutterbug-pixels */ "./build/custom-elements/shutterbug-pixels/shutterbug-pixels.js");
/* harmony import */ var _tag__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../tag */ "./build/custom-elements/tag.js");



// Figure out if the mouse is hovering over a focused area where responses to
// mouse input events may need to deviate from the norm. Perhaps, for example,
// mouse movements should not pan the camera in a text editing area.
function activeEditableAt(shadowRoot, clientX, clientY, editable = [_tag__WEBPACK_IMPORTED_MODULE_2__.customTag.overlay.textfield]) {
    const active = shadowRoot.activeElement;
    const elements = shadowRoot.elementsFromPoint(clientX, clientY) ??
        _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_1__.ShutterbugPixels.container;
    for (const element of elements) {
        const tag = element.tagName.toLowerCase();
        if (editable.includes(tag) && active == element)
            return true;
    }
    return false;
}
// For the deepest nameable overlay item at the given position, if any, that use
// a per tag custom effect name attribute get the value of the attribute.
function effectNameAt(shadowRoot, clientX, clientY, nameable = [
    _tag__WEBPACK_IMPORTED_MODULE_2__.customTag.overlay.button,
    _tag__WEBPACK_IMPORTED_MODULE_2__.customTag.overlay.textfield,
]) {
    const elements = shadowRoot.elementsFromPoint(clientX, clientY) ??
        _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_1__.ShutterbugPixels.container;
    for (const element of elements) {
        const tag = element.tagName.toLowerCase();
        if (nameable.includes(tag)) {
            return element.getAttribute(tag + _tag__WEBPACK_IMPORTED_MODULE_2__.effectNameAttrSuffix) ?? "";
        }
    }
    return "";
}
function addMainContainerMouseListeners(context, mainContainer, send, shadowRoot) {
    // The mousemove event listener is attached to the container of the canvas
    // and overlay instead of the canvas element directly so that mouse moves
    // that occur over overlay items are still registered.
    function move(ev) {
        send((0,_event__WEBPACK_IMPORTED_MODULE_0__.newMouseMoveUIEvent)({
            w: context.canvas.clientWidth,
            h: context.canvas.clientHeight,
            activeEditable: activeEditableAt(shadowRoot, ev.clientX, ev.clientY),
            invertY: true,
            ev: ev,
        }));
    }
    function wheel(ev) {
        send((0,_event__WEBPACK_IMPORTED_MODULE_0__.newMouseWheelUIEvent)({
            deltaX: ev.deltaX,
            deltaY: ev.deltaY,
            effectName: effectNameAt(shadowRoot, ev.clientX, ev.clientY),
            ev: ev,
        }));
    }
    mainContainer.addEventListener("mousemove", move);
    mainContainer.addEventListener("wheel", wheel);
    return {
        move: { event: "mousemove", callback: move },
        wheel: { event: "wheel", callback: wheel },
    };
}
function removeMainContainerMouseListeners(listeners) {
    const mouse = listeners.mouse.mainContainer;
    if (!mouse) {
        return;
    }
    listeners.mainContainer.removeEventListener(mouse.move.event, mouse.move.callback);
    listeners.mainContainer.removeEventListener(mouse.wheel.event, mouse.wheel.callback);
    listeners.mouse.mainContainer = undefined;
}
// Add UI event listeners, on the appropriate elements, which forward events to
// the Mechane server.
//
// Store and return the event listeners so they can be removed later.
//
// Shall be called only once.
function addListeners(context, mainContainer, send, shadow) {
    // A key normally becomes pressed on a keydown event and unpressed on a
    // keyup event.
    const pressedKeys = new Set();
    const listeners = {
        keyboard: {
            window: Array(),
            overlayItems: Array(),
        },
        mouse: {
            mainContainer: addMainContainerMouseListeners(context, mainContainer, send, shadow),
            overlayItems: Array(),
            window: Array(),
        },
        mainContainer: mainContainer,
        send: send,
    };
    function keyDown(ev) {
        // These key events, that have a target of ShutterbugPixels, were made
        // somewhere inside the shadowRoot and are handled by the real target.
        const internal = ev.target instanceof _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_1__.ShutterbugPixels;
        if (internal) {
            return;
        }
        // Prevent the default action on the tab key due to the possibility of
        // the focus moving into the hidden popups and causing unintended
        // display behaviour.
        if (ev.key == "Tab") {
            ev.preventDefault();
        }
        pressedKeys.add(ev.key);
        send((0,_event__WEBPACK_IMPORTED_MODULE_0__.newKeyboardUIEvent)(true, pressedKeys, ev));
    }
    function keyUp(ev) {
        pressedKeys.delete(ev.key);
        const internal = ev.target instanceof _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_1__.ShutterbugPixels;
        if (internal) {
            return;
        }
        send((0,_event__WEBPACK_IMPORTED_MODULE_0__.newKeyboardUIEvent)(false, pressedKeys, ev));
    }
    function mouseDown(ev) {
        send((0,_event__WEBPACK_IMPORTED_MODULE_0__.newMouseButtonUIEvent)(true, ev));
    }
    function mouseUp(ev) {
        send((0,_event__WEBPACK_IMPORTED_MODULE_0__.newMouseButtonUIEvent)(false, ev));
    }
    window.addEventListener("keydown", keyDown);
    window.addEventListener("keyup", keyUp);
    window.addEventListener("mousedown", mouseDown);
    window.addEventListener("mouseup", mouseUp);
    listeners.keyboard.window.push({ event: "keydown", callback: keyDown });
    listeners.keyboard.window.push({ event: "keyup", callback: keyUp });
    listeners.mouse.window.push({ event: "mousedown", callback: mouseDown });
    listeners.mouse.window.push({ event: "mouseup", callback: mouseUp });
    const kaputState = { kaput: false };
    return {
        addMouseMoveListener: function () {
            if (kaputState.kaput) {
                return;
            }
            if (listeners.mouse.mainContainer) {
                removeMainContainerMouseListeners(listeners);
            }
            listeners.mouse.mainContainer = addMainContainerMouseListeners(context, mainContainer, send, shadow);
        },
        removeMouseMoveListener: function () {
            removeMainContainerMouseListeners(listeners);
        },
        pushOverlayItemClickListener: function (listener) {
            listeners.mouse.overlayItems.push(listener);
        },
        popOverlayItemClickListener: function (listener) {
            const i = listeners.mouse.overlayItems.indexOf(listener);
            if (i == -1) {
                return;
            }
            listeners.mouse.overlayItems.splice(i, 1);
        },
        pushOverlayItemKeyUpOrDownListener: function (listener) {
            listeners.keyboard.overlayItems.push(listener);
        },
        popOverlayItemKeyUpOrDownListener: function (listener) {
            const i = listeners.keyboard.overlayItems.indexOf(listener);
            if (i == -1) {
                return;
            }
            listeners.keyboard.overlayItems.splice(i, 1);
        },
        removeAllListeners: function removeListeners() {
            kaputState.kaput = true;
            // Keyboard
            for (const listener of listeners.keyboard.window) {
                window.removeEventListener(listener.event, listener.callback);
            }
            listeners.keyboard.window.length = 0;
            for (const listener of listeners.keyboard.overlayItems) {
                listener.element.removeEventListener(listener.event, listener.callback);
            }
            listeners.keyboard.overlayItems.length = 0;
            // Mouse
            removeMainContainerMouseListeners(listeners);
            for (const listener of listeners.mouse.overlayItems) {
                listener.element.removeEventListener(listener.event, listener.callback);
            }
            listeners.mouse.overlayItems.length = 0;
            for (const listener of listeners.mouse.window) {
                window.removeEventListener(listener.event, listener.callback);
            }
            listeners.mouse.window.length = 0;
        },
        send: send,
    };
}
//# sourceMappingURL=listen.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/mechane-asciidoc/mechane-asciidoc.css.js":
/*!******************************************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/mechane-asciidoc/mechane-asciidoc.css.js ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "css": () => (/* binding */ css)
/* harmony export */ });
/* harmony import */ var _style_origin_position__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../style/origin-position */ "./build/custom-elements/shutterbug-pixels/style/origin-position.js");
/* harmony import */ var _style_theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../style/theme */ "./build/custom-elements/shutterbug-pixels/style/theme.js");


// Container element.
const containerID = "mechane-asciidoc-container";
function containerBase({ maxHeight, originPosition, width, }) {
    return {
        background: _style_theme__WEBPACK_IMPORTED_MODULE_1__.theme.buttonBackground,
        borderColor: _style_theme__WEBPACK_IMPORTED_MODULE_1__.theme.primary,
        borderRadius: _style_origin_position__WEBPACK_IMPORTED_MODULE_0__.borderRadiuses.get(originPosition),
        borderTopStyle: "none",
        borderRightStyle: "solid",
        borderBottomStyle: "none",
        borderLeftStyle: "solid",
        borderWidth: _style_theme__WEBPACK_IMPORTED_MODULE_1__.theme.buttonBorderRadius,
        boxShadow: _style_theme__WEBPACK_IMPORTED_MODULE_1__.theme.boxShadow,
        maxHeight: maxHeight,
        overflow: "auto",
        padding: _style_theme__WEBPACK_IMPORTED_MODULE_1__.theme.buttonPadding,
        pointerEvents: "auto",
        position: "relative",
        width: width,
    };
}
// All the CSS.
const css = {
    container: {
        id: containerID,
        selector: `#${containerID}`,
        style: containerBase,
    },
    asciidocSlot: {
        slotName: "asciidoc",
    },
};
//# sourceMappingURL=mechane-asciidoc.css.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/mechane-asciidoc/mechane-asciidoc.js":
/*!**************************************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/mechane-asciidoc/mechane-asciidoc.js ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MechaneAsciidoc": () => (/* binding */ MechaneAsciidoc)
/* harmony export */ });
/* harmony import */ var _crule_crule__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../crule/crule */ "./build/crule/crule.js");
/* harmony import */ var _font__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../font */ "./build/custom-elements/font.js");
/* harmony import */ var _mechane_asciidoc_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mechane-asciidoc.css */ "./build/custom-elements/shutterbug-pixels/mechane-asciidoc/mechane-asciidoc.css.js");
/* harmony import */ var _style_origin_position__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../style/origin-position */ "./build/custom-elements/shutterbug-pixels/style/origin-position.js");
/* harmony import */ var _style_theme__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../style/theme */ "./build/custom-elements/shutterbug-pixels/style/theme.js");
/* harmony import */ var _tag__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../tag */ "./build/custom-elements/tag.js");






// A MechaneAsciidoc custom element provides a slot for the HTML output of the
// Asciidoctor.js AsciiDoc processor.
class MechaneAsciidoc extends HTMLElement {
    // The tag name of this custom element.
    static tag = _tag__WEBPACK_IMPORTED_MODULE_5__.customTag.overlay.asciidoc;
    // Attributes set and unset by the Mechane server.
    //
    // When such an attribute is changed a whole new element is sent to replace
    // the existing element. Thus the connectedCallback() is where the logic for
    // such changes takes place.
    static attribute = {
        maxHeight: `${MechaneAsciidoc.tag}-max-height`,
        originPosition: `${MechaneAsciidoc.tag}-origin-position`,
        theme: `${MechaneAsciidoc.tag}-theme`,
        width: `${MechaneAsciidoc.tag}-width`,
    };
    // Descendants of the shadowRoot.
    container = document.createElement("div");
    asciidocSlot = document.createElement("slot");
    constructor() {
        // Mandatory super call.
        super();
        // Instance fields.
        this.asciidocSlot.setAttribute("name", _mechane_asciidoc_css__WEBPACK_IMPORTED_MODULE_2__.css.asciidocSlot.slotName);
        this.container.append(this.asciidocSlot);
        this.container.id = _mechane_asciidoc_css__WEBPACK_IMPORTED_MODULE_2__.css.container.id;
        // Shadow root and style.
        const shadowRoot = this.attachShadow({ mode: "open" });
        const style = document.createElement("style");
        shadowRoot.append(style, this.container);
    }
    attributeChangedCallback() {
        this.updateStyle();
    }
    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return;
        }
        this.updateStyle();
    }
    disconnectedCallback() {
        return;
    }
    updateStyle() {
        const style = this.shadowRoot?.querySelector("style");
        if (!style) {
            return;
        }
        // if (
        //     this.getAttribute(MechaneAsciidoc.attribute.theme) !=
        //     theme.motifName
        // ) {
        //     return
        // }
        const originPosition = (0,_style_origin_position__WEBPACK_IMPORTED_MODULE_3__.getOriginPosition)(this.getAttribute(MechaneAsciidoc.attribute.originPosition)) ?? _style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.defaultOriginPosition;
        const maxHeight = this.getAttribute(MechaneAsciidoc.attribute.maxHeight) ?? "60ch";
        const width = this.getAttribute(MechaneAsciidoc.attribute.width) ?? "100ch";
        // Accumulate CSS rules in a style sheet.
        const sheet = new Array();
        // Include the font styles. Even though the font rules are defined
        // globally, inside a custom element the font rules need to be defined
        // again.
        sheet.push(..._font__WEBPACK_IMPORTED_MODULE_1__.materialIcons);
        // Container element.
        sheet.push({
            selector: _mechane_asciidoc_css__WEBPACK_IMPORTED_MODULE_2__.css.container.selector,
            properties: _mechane_asciidoc_css__WEBPACK_IMPORTED_MODULE_2__.css.container.style({
                maxHeight: maxHeight,
                originPosition: originPosition,
                width: width,
            }),
        });
        // Apply the accumulated CSS rules.
        style.textContent = (0,_crule_crule__WEBPACK_IMPORTED_MODULE_0__.toString)(sheet);
    }
}

//# sourceMappingURL=mechane-asciidoc.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/mechane-button/event.js":
/*!*************************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/mechane-button/event.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "newMechaneButtonUIEvent": () => (/* binding */ newMechaneButtonUIEvent)
/* harmony export */ });
function newMechaneButtonUIEvent(effectName, ev) {
    return {
        mechaneButton: {
            altKey: ev.altKey,
            button: ev.button,
            clientX: ev.clientX,
            clientY: ev.clientY,
            ctrlKey: ev.ctrlKey,
            detail: ev.detail,
            metaKey: ev.metaKey,
            effectName: effectName,
            shiftKey: ev.shiftKey,
            windowH: ev.view ? ev.view.innerHeight : 0,
            windowW: ev.view ? ev.view.innerWidth : 0,
        },
    };
}
//# sourceMappingURL=event.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/mechane-button/mechane-button.css.js":
/*!**************************************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/mechane-button/mechane-button.css.js ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "css": () => (/* binding */ css)
/* harmony export */ });
/* harmony import */ var _style_flash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../style/flash */ "./build/custom-elements/shutterbug-pixels/style/flash.js");
/* harmony import */ var _style_origin_position__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../style/origin-position */ "./build/custom-elements/shutterbug-pixels/style/origin-position.js");
/* harmony import */ var _style_theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../style/theme */ "./build/custom-elements/shutterbug-pixels/style/theme.js");
/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../style */ "./build/custom-elements/style.js");




// CSS custom properties for the button background transition. These custom
// properties are defined by the Elm integrant in a global style element. They
// are used by both the Elm and Typescript integrants. Defining them here inside
// the Shadow DOM is not supported.
const customs = {
    pct1: "--button-background-stop-1-percentage",
    pct2: "--button-background-stop-2-percentage",
    col1: "--button-background-stop-1-color",
};
const transition = [
    "border-color 100ms",
    "box-shadow 300ms",
    `${customs.pct1} 300ms`,
    `${customs.pct2} 300ms`,
    `${customs.col1} 300ms`,
].join();
// Container element.
const containerID = "mechane-button-container";
const disabledClass = "mechane-button-disabled";
function containerBase(originPosition, slotted) {
    return {
        display: "flex",
        alignItems: "center",
        background: "radial-gradient(" +
            [
                [`var(${customs.col1})`, `var(${customs.pct1})`].join(" "),
                [`${_style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.primaryLight}`, `var(${customs.pct2})`].join(" "),
            ].join(",") +
            ")",
        [customs.pct1]: "100%",
        [customs.pct2]: "100%",
        [customs.col1]: slotted
            ? "rgba(255, 255, 255, 0.0)"
            : _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonBackground,
        borderColor: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.primary,
        borderRadius: slotted
            ? _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonBorderRadius
            : _style_origin_position__WEBPACK_IMPORTED_MODULE_1__.borderRadiuses.get(originPosition),
        borderStyle: "solid",
        borderWidth: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonBorder,
        boxShadow: slotted ? "none" : _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.boxShadow,
        color: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.textNormal,
        fontFamily: "CascadiaCode",
        fontSize: "1em",
        outline: "none",
        padding: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding,
        pointerEvents: "auto",
        position: "relative",
        whiteSpace: "nowrap",
    };
}
function normalContainer(originPosition, slotted) {
    return {
        ...containerBase(originPosition, slotted),
        transition: transition,
    };
}
function clickedContainer(originPosition, slotted) {
    return {
        ...containerBase(originPosition, slotted),
    };
}
function hoveredContainer() {
    return {
        borderColor: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.primaryLight,
        boxShadow: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.boxShadowGlow,
        [customs.pct1]: "62%",
        [customs.pct2]: "72%",
        [customs.col1]: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonHoverBackground,
    };
}
function disabledContainer(slotted) {
    return {
        background: slotted ? "none" : "rgba(255, 255, 255, 0.65)",
        borderColor: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.disabled,
        borderWidth: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonBorder,
        boxShadow: "none",
        filter: _style__WEBPACK_IMPORTED_MODULE_3__.kaputFilter,
        padding: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding,
    };
}
// Click effect element.
const clickEffectID = "mechane-button-click-effect";
// Slot elements.
const iconSlotID = "iconSlotID";
const iconSlotName = "icon";
const textSlotName = "text";
const iconSlot = function (iconColour) {
    return {
        color: `${iconColour}`,
        fontSize: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonIconFontSize,
        zIndex: 2,
    };
};
const disabledIconSlot = {
    color: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.disabled,
};
const textSlot = {
    paddingLeft: "0.5em",
    zIndex: 2,
};
// All the CSS!
const css = {
    disabledClass: disabledClass,
    paddingBorderTotalWidth: `calc(${_style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding} + ${_style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonBorder})`,
    container: {
        id: containerID,
        selector: {
            hover: `#${containerID}:hover:not(.${disabledClass})`,
            normal: `#${containerID}`,
            disabled: `#${containerID}.${disabledClass}`,
        },
        style: {
            clicked: clickedContainer,
            hover: hoveredContainer,
            normal: normalContainer,
            disabled: disabledContainer,
        },
    },
    clickEffect: {
        id: clickEffectID,
        selector: {
            keyframes: `@keyframes ${_style_flash__WEBPACK_IMPORTED_MODULE_0__.flashEffectAnimationName}`,
            normal: `#${clickEffectID}`,
        },
        style: {
            keyframes: _style_flash__WEBPACK_IMPORTED_MODULE_0__.flashEffectKeyframes,
            normal: _style_flash__WEBPACK_IMPORTED_MODULE_0__.flashEffect,
        },
    },
    slots: {
        icon: {
            id: iconSlotID,
            slotName: iconSlotName,
            selector: {
                normal: `#${iconSlotID}`,
                disabled: `.${disabledClass} #${iconSlotID}`,
            },
            style: {
                normal: iconSlot,
                disabled: disabledIconSlot,
            },
        },
        text: {
            slotName: textSlotName,
            selector: `::slotted([slot=${textSlotName}])`,
            style: textSlot,
        },
    },
};
//# sourceMappingURL=mechane-button.css.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/mechane-button/mechane-button.js":
/*!**********************************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/mechane-button/mechane-button.js ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MechaneButton": () => (/* binding */ MechaneButton)
/* harmony export */ });
/* harmony import */ var _crule_crule__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../crule/crule */ "./build/crule/crule.js");
/* harmony import */ var _event__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./event */ "./build/custom-elements/shutterbug-pixels/mechane-button/event.js");
/* harmony import */ var _font__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../font */ "./build/custom-elements/font.js");
/* harmony import */ var _log_log__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../log/log */ "./build/log/log.js");
/* harmony import */ var _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./mechane-button.css */ "./build/custom-elements/shutterbug-pixels/mechane-button/mechane-button.css.js");
/* harmony import */ var _overlay__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../overlay */ "./build/custom-elements/shutterbug-pixels/overlay.js");
/* harmony import */ var _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shutterbug-pixels */ "./build/custom-elements/shutterbug-pixels/shutterbug-pixels.js");
/* harmony import */ var _style_origin_position__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../style/origin-position */ "./build/custom-elements/shutterbug-pixels/style/origin-position.js");
/* harmony import */ var _style_theme__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../style/theme */ "./build/custom-elements/shutterbug-pixels/style/theme.js");
/* harmony import */ var _tag__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../tag */ "./build/custom-elements/tag.js");










// A MechaneButton custom element is a button which can be clicked.
//
// Upon being clicked it makes a mechaneButton object summarising the event
// which is sent to the Mechane server along with the name attribute of the
// button that generated it.
class MechaneButton extends HTMLElement {
    // The tag name of this custom element.
    static tag = _tag__WEBPACK_IMPORTED_MODULE_9__.customTag.overlay.button;
    // Attributes set by the client.
    //
    // The logic for handling a change of these attributes happens in the
    // attributeChangedCallback().
    static get observedAttributes() {
        // Disabled is set by the client when the overlay is permanently
        // disabled. This happens for a Mechane server shutdown or any other
        // connection loss.
        return [_overlay__WEBPACK_IMPORTED_MODULE_5__.disabledAttribute];
    }
    // Attributes set and unset by the Mechane server.
    //
    // When such an attribute is changed a whole new element is sent to replace
    // the existing element. Thus the connectedCallback() is where the logic for
    // such changes takes place.
    static attribute = {
        clickEffect: `${MechaneButton.tag}-click-effect`,
        cooldownDuration: `${MechaneButton.tag}-cooldown-duration`,
        effectName: `${MechaneButton.tag}${_tag__WEBPACK_IMPORTED_MODULE_9__.effectNameAttrSuffix}`,
        iconColour: `${MechaneButton.tag}-icon-colour`,
        iconFamily: `${MechaneButton.tag}-icon-family`,
        originPosition: `${MechaneButton.tag}-origin-position`,
        theme: `${MechaneButton.tag}-theme`,
    };
    // Descendants of the shadowRoot.
    container = document.createElement("div");
    clickEffect = document.createElement("div");
    iconSlot = document.createElement("slot");
    textSlot = document.createElement("slot");
    // The click listener's callback/element. The callback is initially an
    // ineffectual dummy function. It is meant to be replaced by a function
    // which sends clicks to the Mechane server.
    clickListener = {
        callback: (ev) => {
            console.log(`${MechaneButton.tag}: click failed: dummy callback`);
            void ev;
        },
        element: this,
        event: "click",
    };
    constructor() {
        // Mandatory super call.
        super();
        // Instance fields.
        this.iconSlot.name = _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.slots.icon.slotName;
        this.iconSlot.id = _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.slots.icon.id;
        this.textSlot.name = _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.slots.text.slotName;
        this.container.append(this.iconSlot, this.textSlot);
        this.container.id = _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.container.id;
        this.clickEffect.id = _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.clickEffect.id;
        // Shadow root and style.
        const shadowRoot = this.attachShadow({
            delegatesFocus: true,
            mode: "open",
        });
        const style = document.createElement("style");
        shadowRoot.append(style, this.container);
    }
    attributeChangedCallback(name) {
        if (name == _overlay__WEBPACK_IMPORTED_MODULE_5__.disabledAttribute && this.hasAttribute(_overlay__WEBPACK_IMPORTED_MODULE_5__.disabledAttribute)) {
            this.container.classList.add(_mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.disabledClass);
            if (this.hasAttribute(MechaneButton.attribute.clickEffect)) {
                // Undo an active click effect.
                this.removeAttribute(MechaneButton.attribute.clickEffect);
                this.clickEffect.remove();
            }
        }
        this.updateStyle();
    }
    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return;
        }
        // Add and store the click listener.
        const effectName = this.getAttribute(MechaneButton.attribute.effectName);
        if (!effectName) {
            (0,_log_log__WEBPACK_IMPORTED_MODULE_3__.log)(MechaneButton.tag, "no effect name attribute");
        }
        const dummyButton = document.createElement("button"); // Absorbs focus.
        const click = (ev) => {
            if (!effectName) {
                (0,_log_log__WEBPACK_IMPORTED_MODULE_3__.log)(`${MechaneButton.tag}`, "click failed", "no name attribute");
                return;
            }
            dummyButton.focus();
            ev.preventDefault();
            _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_6__.ShutterbugPixels.uiEvents?.send((0,_event__WEBPACK_IMPORTED_MODULE_1__.newMechaneButtonUIEvent)(effectName, ev));
        };
        this.addEventListener("click", click);
        this.clickListener.callback = click;
        _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_6__.ShutterbugPixels.uiEvents?.pushOverlayItemClickListener(this.clickListener);
        // Click effect.
        if (this.hasAttribute(MechaneButton.attribute.clickEffect)) {
            this.container.prepend(this.clickEffect);
        }
        // Styling.
        this.updateStyle();
    }
    disconnectedCallback() {
        // Remove the stored click listener.
        _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_6__.ShutterbugPixels.uiEvents?.popOverlayItemClickListener(this.clickListener);
        this.clickListener.element.removeEventListener(this.clickListener.event, this.clickListener.callback);
    }
    updateStyle() {
        const style = this.shadowRoot?.querySelector("style");
        if (!style) {
            return;
        }
        // if (
        //     this.getAttribute(MechaneButton.attribute.theme) != theme.motifName
        // ) {
        //     return
        // }
        const iconFamily = this.getAttribute(MechaneButton.attribute.iconFamily) ??
            _font__WEBPACK_IMPORTED_MODULE_2__.materialIconsClass.round;
        if ((0,_font__WEBPACK_IMPORTED_MODULE_2__.isMaterialIconsFamily)(iconFamily)) {
            (0,_font__WEBPACK_IMPORTED_MODULE_2__.setMaterialIconsClass)(this.iconSlot, iconFamily);
        }
        const originPosition = (0,_style_origin_position__WEBPACK_IMPORTED_MODULE_7__.getOriginPosition)(this.getAttribute(MechaneButton.attribute.originPosition)) ?? _style_theme__WEBPACK_IMPORTED_MODULE_8__.theme.defaultOriginPosition;
        // Accumulate CSS rules in a style sheet.
        const sheet = new Array();
        // Include the font styles. Even though the font rules are defined
        // globally, inside a custom element the font rules need to be defined
        // again.
        sheet.push(..._font__WEBPACK_IMPORTED_MODULE_2__.materialIcons);
        const clicked = this.hasAttribute(MechaneButton.attribute.clickEffect);
        const slotted = this.hasAttribute("slot");
        // Container element.
        if (clicked) {
            sheet.push({
                selector: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.container.selector.normal,
                properties: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.container.style.clicked(originPosition, slotted),
            });
        }
        else {
            sheet.push({
                selector: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.container.selector.normal,
                properties: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.container.style.normal(originPosition, slotted),
            }, {
                selector: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.container.selector.hover,
                properties: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.container.style.hover(),
            }, {
                selector: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.container.selector.disabled,
                properties: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.container.style.disabled(slotted),
            });
        }
        // Click effect element.
        if (clicked) {
            const flashDuration = this.getAttribute(MechaneButton.attribute.cooldownDuration) ??
                _style_theme__WEBPACK_IMPORTED_MODULE_8__.theme.defaultFlashDuration;
            sheet.push({
                selector: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.clickEffect.selector.normal,
                properties: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.clickEffect.style.normal(flashDuration, slotted ? undefined : originPosition),
            }, {
                selector: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.clickEffect.selector.keyframes,
                properties: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.clickEffect.style.keyframes(_style_theme__WEBPACK_IMPORTED_MODULE_8__.theme.accent),
            });
        }
        // Slot elements.
        const iconColour = this.getAttribute(MechaneButton.attribute.iconColour) ??
            _style_theme__WEBPACK_IMPORTED_MODULE_8__.theme.primary;
        sheet.push({
            selector: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.slots.icon.selector.normal,
            properties: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.slots.icon.style.normal(iconColour),
        }, {
            selector: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.slots.icon.selector.disabled,
            properties: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.slots.icon.style.disabled,
        }, {
            selector: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.slots.text.selector,
            properties: _mechane_button_css__WEBPACK_IMPORTED_MODULE_4__.css.slots.text.style,
        });
        // Apply the accumulated CSS rules.
        style.textContent = (0,_crule_crule__WEBPACK_IMPORTED_MODULE_0__.toString)(sheet);
    }
}

//# sourceMappingURL=mechane-button.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/mechane-pre/mechane-pre.css.js":
/*!********************************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/mechane-pre/mechane-pre.css.js ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "css": () => (/* binding */ css)
/* harmony export */ });
/* harmony import */ var _style_origin_position__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../style/origin-position */ "./build/custom-elements/shutterbug-pixels/style/origin-position.js");
/* harmony import */ var _style_theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../style/theme */ "./build/custom-elements/shutterbug-pixels/style/theme.js");


// Container element.
const containerID = "mechane-pre-container";
function containerBase({ maxHeight, originPosition, width, }) {
    return {
        background: _style_theme__WEBPACK_IMPORTED_MODULE_1__.theme.preBackground,
        borderColor: _style_theme__WEBPACK_IMPORTED_MODULE_1__.theme.primary,
        borderRadius: _style_origin_position__WEBPACK_IMPORTED_MODULE_0__.borderRadiuses.get(originPosition),
        borderTopStyle: "none",
        borderRightStyle: "solid",
        borderBottomStyle: "none",
        borderLeftStyle: "solid",
        borderWidth: _style_theme__WEBPACK_IMPORTED_MODULE_1__.theme.buttonBorderRadius,
        boxShadow: _style_theme__WEBPACK_IMPORTED_MODULE_1__.theme.boxShadow,
        maxHeight: maxHeight,
        overflow: "auto",
        padding: _style_theme__WEBPACK_IMPORTED_MODULE_1__.theme.buttonPadding,
        pointerEvents: "auto",
        position: "relative",
        width: width,
    };
}
// All the CSS.
const css = {
    container: {
        id: containerID,
        selector: `#${containerID}`,
        style: containerBase,
    },
    buttonsSlot: {
        slotName: "buttons",
    },
    preSlot: {
        slotName: "pre",
    },
};
//# sourceMappingURL=mechane-pre.css.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/mechane-pre/mechane-pre.js":
/*!****************************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/mechane-pre/mechane-pre.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MechanePre": () => (/* binding */ MechanePre)
/* harmony export */ });
/* harmony import */ var _crule_crule__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../crule/crule */ "./build/crule/crule.js");
/* harmony import */ var _mechane_pre_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mechane-pre.css */ "./build/custom-elements/shutterbug-pixels/mechane-pre/mechane-pre.css.js");
/* harmony import */ var _style_origin_position__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../style/origin-position */ "./build/custom-elements/shutterbug-pixels/style/origin-position.js");
/* harmony import */ var _style_theme__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../style/theme */ "./build/custom-elements/shutterbug-pixels/style/theme.js");
/* harmony import */ var _tag__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../tag */ "./build/custom-elements/tag.js");





// A MechanePre custom element provides a slot for preformatted text.
class MechanePre extends HTMLElement {
    // The tag name of this custom element.
    static tag = _tag__WEBPACK_IMPORTED_MODULE_4__.customTag.overlay.pre;
    // Attributes set and unset by the Mechane server.
    //
    // When such an attribute is changed a whole new element is sent to replace
    // the existing element. Thus the connectedCallback() is where the logic for
    // such changes takes place.
    static attribute = {
        maxHeight: `${MechanePre.tag}-max-height`,
        originPosition: `${MechanePre.tag}-origin-position`,
        theme: `${MechanePre.tag}-theme`,
        width: `${MechanePre.tag}-width`,
    };
    // Descendants of the shadowRoot.
    container = document.createElement("div");
    preSlot = document.createElement("slot");
    buttonsSlot = document.createElement("slot");
    constructor() {
        // Mandatory super call.
        super();
        // Instance fields.
        this.buttonsSlot.setAttribute("name", _mechane_pre_css__WEBPACK_IMPORTED_MODULE_1__.css.buttonsSlot.slotName);
        this.preSlot.setAttribute("name", _mechane_pre_css__WEBPACK_IMPORTED_MODULE_1__.css.preSlot.slotName);
        this.container.append(this.buttonsSlot, this.preSlot);
        this.container.id = _mechane_pre_css__WEBPACK_IMPORTED_MODULE_1__.css.container.id;
        // Shadow root and style.
        const shadowRoot = this.attachShadow({ mode: "open" });
        const style = document.createElement("style");
        shadowRoot.append(style, this.container);
    }
    attributeChangedCallback() {
        this.updateStyle();
    }
    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return;
        }
        this.updateStyle();
    }
    disconnectedCallback() {
        return;
    }
    updateStyle() {
        const style = this.shadowRoot?.querySelector("style");
        if (!style) {
            return;
        }
        // if (this.getAttribute(MechanePre.attribute.theme) != theme.motifName) {
        //     return
        // }
        const originPosition = (0,_style_origin_position__WEBPACK_IMPORTED_MODULE_2__.getOriginPosition)(this.getAttribute(MechanePre.attribute.originPosition)) ?? _style_theme__WEBPACK_IMPORTED_MODULE_3__.theme.defaultOriginPosition;
        const maxHeight = this.getAttribute(MechanePre.attribute.maxHeight) ?? "60ch";
        const width = this.getAttribute(MechanePre.attribute.width) ?? "100ch";
        // Accumulate CSS rules in a style sheet.
        const sheet = new Array();
        // Container element.
        sheet.push({
            selector: _mechane_pre_css__WEBPACK_IMPORTED_MODULE_1__.css.container.selector,
            properties: _mechane_pre_css__WEBPACK_IMPORTED_MODULE_1__.css.container.style({
                maxHeight: maxHeight,
                originPosition: originPosition,
                width: width,
            }),
        });
        // Apply the accumulated CSS rules.
        style.textContent = (0,_crule_crule__WEBPACK_IMPORTED_MODULE_0__.toString)(sheet);
    }
}

//# sourceMappingURL=mechane-pre.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/mechane-textfield/event.js":
/*!****************************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/mechane-textfield/event.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "newMechaneTextfieldKeyUpOrDownUIEvent": () => (/* binding */ newMechaneTextfieldKeyUpOrDownUIEvent)
/* harmony export */ });
function newMechaneTextfieldKeyUpOrDownUIEvent(effectName, value, keyDown, ev) {
    return {
        mechaneTextfieldKeyUpOrDown: {
            altKey: ev.altKey,
            ctrlKey: ev.ctrlKey,
            isComposing: ev.isComposing,
            key: ev.key,
            keyDown: keyDown,
            metaKey: ev.metaKey,
            effectName: effectName,
            shiftKey: ev.shiftKey,
            value: value,
        },
    };
}
//# sourceMappingURL=event.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/mechane-textfield/mechane-textfield.css.js":
/*!********************************************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/mechane-textfield/mechane-textfield.css.js ***!
  \********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "css": () => (/* binding */ css),
/* harmony export */   "disabledColour": () => (/* binding */ disabledColour)
/* harmony export */ });
/* harmony import */ var _style_flash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../style/flash */ "./build/custom-elements/shutterbug-pixels/style/flash.js");
/* harmony import */ var _style_origin_position__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../style/origin-position */ "./build/custom-elements/shutterbug-pixels/style/origin-position.js");
/* harmony import */ var _style_theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../style/theme */ "./build/custom-elements/shutterbug-pixels/style/theme.js");
/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../style */ "./build/custom-elements/style.js");




const transition = ["border-color 100ms", "box-shadow 300ms"].join();
// Container element.
const containerID = "mechane-textfield-container";
function containerBase(originPosition) {
    return {
        display: "flex",
        alignItems: "center",
        background: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonBackground,
        borderColor: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.primary,
        borderRadius: _style_origin_position__WEBPACK_IMPORTED_MODULE_1__.borderRadiuses.get(originPosition),
        borderStyle: "double",
        borderWidth: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonBorder,
        boxShadow: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.boxShadow,
        padding: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding,
        pointerEvents: "auto",
        position: "relative",
        whiteSpace: "nowrap",
    };
}
function normalContainer(originPosition) {
    return {
        ...containerBase(originPosition),
        transition: transition,
    };
}
function submittedContainer(originPosition) {
    return {
        ...containerBase(originPosition),
    };
}
function hoveredContainer() {
    return {
        borderColor: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.primaryLight,
    };
}
const disabledColour = "rgb(211, 211, 211)";
function disabledContainer(originPosition) {
    return {
        ...containerBase(originPosition),
        background: "rgba(255, 255, 255, 0.65)",
        borderColor: disabledColour,
        borderWidth: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonBorder,
        boxShadow: "none",
        filter: _style__WEBPACK_IMPORTED_MODULE_3__.kaputFilter,
        padding: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding,
    };
}
// All child elements except the submit effect.
const containerChildren = {
    zIndex: 2, // Place above submit effect.
};
// Submit effect element.
const submitEffectID = "mechane-textfield-submit-effect";
// Icon elements.
const icon1SlotID = "icon1SlotID";
const icon2SlotID = "icon2SlotID";
const icon1SlotName = "icon1";
const icon2SlotName = "icon2";
function bothIcons(iconColour) {
    return {
        color: `${iconColour}`,
        fontSize: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonIconFontSize,
    };
}
// Input element.
function inputFieldBase(borderColor) {
    return {
        borderColor: borderColor,
        borderRadius: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonBorderRadius,
        borderStyle: "double",
        borderWidth: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonBorder,
        font: "inherit",
        marginLeft: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding,
        marginRight: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding,
        paddingLeft: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding,
        paddingRight: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding,
        paddingTop: `calc(${_style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding} / 2)`,
        paddingBottom: `calc(${_style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding} / 2)`,
        transition: transition,
    };
}
function inputField(disabled, width) {
    const borderColor = disabled ? _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.disabled : _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.inputBorderPrimary;
    if (isNaN(width)) {
        return inputFieldBase(borderColor);
    }
    return {
        ...inputFieldBase(borderColor),
        width: `${width}ch`,
    };
}
const hoverInputField = {
    boxShadow: `0px 0px 0.5em 0.2em rgb(0 0 0 / 20%)`,
};
function activeInputField(disabled) {
    const props = {};
    if (!disabled) {
        props.borderStyle = "dashed";
        props.boxShadow = `0px 0px 0.6em 0.4em rgb(0 0 0 / 20%)`;
        props.outline = `${_style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.inputBorderFocus} solid calc(${_style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonBorder} / 2)`;
    }
    else {
        props.boxShadow = "none";
        props.outline = "none";
    }
    return props;
}
// Label text element.
const labelText = {
    cursor: "auto",
    display: "inline-block",
    marginLeft: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding,
};
const activeLabelText = {
    outline: "none", // Hide outline in text select mode with focused label.
};
// Button element.
const buttonSlotName = "button";
const button = {
    marginLeft: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonPadding,
};
// All the CSS.
const css = {
    borderWidth: _style_theme__WEBPACK_IMPORTED_MODULE_2__.theme.buttonBorder,
    container: {
        id: containerID,
        selector: {
            hover: `#${containerID}:hover`,
            normal: `#${containerID}`,
        },
        style: {
            submitted: submittedContainer,
            disabled: disabledContainer,
            hover: hoveredContainer,
            normal: normalContainer,
        },
    },
    containerChildren: {
        selector: `#${containerID} > *:not(#${submitEffectID})`,
        style: containerChildren,
    },
    submitEffect: {
        id: submitEffectID,
        selector: {
            keyframes: `@keyframes ${_style_flash__WEBPACK_IMPORTED_MODULE_0__.flashEffectAnimationName}`,
            normal: `#${submitEffectID}`,
        },
        style: {
            keyframes: _style_flash__WEBPACK_IMPORTED_MODULE_0__.flashEffectKeyframes,
            normal: _style_flash__WEBPACK_IMPORTED_MODULE_0__.flashEffect,
        },
    },
    iconSlot1: {
        id: icon1SlotID,
        slotName: icon1SlotName,
        selector: `#${icon1SlotID}`,
        style: bothIcons,
    },
    iconSlot2: {
        id: icon2SlotID,
        slotName: icon2SlotName,
        selector: `#${icon2SlotID}`,
        style: bothIcons,
    },
    inputField: {
        selector: "input",
        style: inputField,
    },
    hoverInputField: {
        selector: `#${containerID}:hover input`,
        style: hoverInputField,
    },
    activeInputField: {
        selector: `#${containerID} input:focus, p:focus + input`,
        style: activeInputField,
    },
    labelText: {
        selector: "label p",
        style: labelText,
    },
    activeLabelText: {
        selector: "label p:focus",
        style: activeLabelText,
    },
    button: {
        slotName: buttonSlotName,
        selector: `::slotted([slot=${buttonSlotName}])`,
        style: button,
    },
};
//# sourceMappingURL=mechane-textfield.css.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/mechane-textfield/mechane-textfield.js":
/*!****************************************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/mechane-textfield/mechane-textfield.js ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MechaneTextfield": () => (/* binding */ MechaneTextfield)
/* harmony export */ });
/* harmony import */ var _crule_crule__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../crule/crule */ "./build/crule/crule.js");
/* harmony import */ var _font__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../font */ "./build/custom-elements/font.js");
/* harmony import */ var _log_log__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../log/log */ "./build/log/log.js");
/* harmony import */ var _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./mechane-textfield.css */ "./build/custom-elements/shutterbug-pixels/mechane-textfield/mechane-textfield.css.js");
/* harmony import */ var _overlay__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../overlay */ "./build/custom-elements/shutterbug-pixels/overlay.js");
/* harmony import */ var _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shutterbug-pixels */ "./build/custom-elements/shutterbug-pixels/shutterbug-pixels.js");
/* harmony import */ var _style_origin_position__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../style/origin-position */ "./build/custom-elements/shutterbug-pixels/style/origin-position.js");
/* harmony import */ var _tag__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../tag */ "./build/custom-elements/tag.js");
/* harmony import */ var _style_theme__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../style/theme */ "./build/custom-elements/shutterbug-pixels/style/theme.js");
/* harmony import */ var _event__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./event */ "./build/custom-elements/shutterbug-pixels/mechane-textfield/event.js");










// A MechaneTextfield custom element is a one line text input area.
class MechaneTextfield extends HTMLElement {
    // The tag name of this custom element.
    static tag = _tag__WEBPACK_IMPORTED_MODULE_7__.customTag.overlay.textfield;
    // Attributes set by the client.
    //
    // The logic for handling a change of these attributes happens in the
    // attributeChangedCallback().
    static get observedAttributes() {
        // Disabled is set by the client when the overlay is permanently
        // disabled. This happens for a Mechane server shutdown or any other
        // connection loss.
        return [_overlay__WEBPACK_IMPORTED_MODULE_4__.disabledAttribute];
    }
    // Attributes set and unset by the Mechane server.
    //
    // When such an attribute is changed a whole new element is sent to replace
    // the existing element. Thus the connectedCallback() is where the logic for
    // such changes takes place.
    static attribute = {
        cooldownDuration: `${MechaneTextfield.tag}-cooldown-duration`,
        effectName: `${MechaneTextfield.tag}${_tag__WEBPACK_IMPORTED_MODULE_7__.effectNameAttrSuffix}`,
        icon1Colour: `${MechaneTextfield.tag}-icon1-colour`,
        icon1Family: `${MechaneTextfield.tag}-icon1-family`,
        icon2Colour: `${MechaneTextfield.tag}-icon2-colour`,
        icon2Family: `${MechaneTextfield.tag}-icon2-family`,
        inputValue: `${MechaneTextfield.tag}-input-value`,
        label: `${MechaneTextfield.tag}-label`,
        mask: `${MechaneTextfield.tag}-mask`,
        originPosition: `${MechaneTextfield.tag}-origin-position`,
        placeholder: `${MechaneTextfield.tag}-placeholder`,
        size: `${MechaneTextfield.tag}-size`,
        spellcheck: `${MechaneTextfield.tag}-spellcheck`,
        submitEffect: `${MechaneTextfield.tag}-submit-effect`,
        theme: `${MechaneTextfield.tag}-theme`,
    };
    // Descendants of the shadowRoot.
    container = document.createElement("label");
    submitEffect = document.createElement("div");
    icon1Slot = document.createElement("slot");
    icon2Slot = document.createElement("slot");
    input = document.createElement("input");
    labelText = document.createElement("p");
    buttonSlot = document.createElement("slot");
    // The keyup/down listener's callback/element. The callback is initially an
    // ineffectual dummy function. It is meant to be replaced by a function
    // which sends keyup events to the Mechane server.
    listener = {
        keyup: {
            callback: (ev) => {
                console.log(`${MechaneTextfield.tag}: keyup failed`);
                void ev;
            },
            element: this.input,
            event: "keyup",
        },
        keydown: {
            callback: (ev) => {
                console.log(`${MechaneTextfield.tag}: keydown failed`);
                void ev;
            },
            element: this.input,
            event: "keydown",
        },
    };
    constructor() {
        // Mandatory super call.
        super();
        // Instance fields.
        this.icon1Slot.setAttribute("name", _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.iconSlot1.slotName);
        this.icon1Slot.id = _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.iconSlot1.id;
        this.icon2Slot.setAttribute("name", _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.iconSlot2.slotName);
        this.icon2Slot.id = _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.iconSlot2.id;
        this.labelText.tabIndex = -1; // Permit focus in text select mode.
        this.buttonSlot.setAttribute("name", _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.button.slotName);
        this.container.append(this.icon1Slot, this.labelText, this.input, this.icon2Slot, this.buttonSlot);
        this.container.id = _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.container.id;
        this.submitEffect.id = _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.submitEffect.id;
        // Shadow root and style.
        const shadowRoot = this.attachShadow({
            delegatesFocus: true,
            mode: "open",
        });
        const style = document.createElement("style");
        shadowRoot.append(style, this.container);
    }
    attributeChangedCallback(name) {
        if (name == _overlay__WEBPACK_IMPORTED_MODULE_4__.disabledAttribute && this.hasAttribute(_overlay__WEBPACK_IMPORTED_MODULE_4__.disabledAttribute)) {
            if (this.hasAttribute(MechaneTextfield.attribute.submitEffect)) {
                // Undo an active submit effect.
                this.removeAttribute(MechaneTextfield.attribute.submitEffect);
                this.submitEffect.remove();
            }
        }
        this.updateStyle();
    }
    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return;
        }
        // Add and store the keyup/down listeners.
        const effectName = this.getAttribute(MechaneTextfield.attribute.effectName);
        if (!effectName) {
            (0,_log_log__WEBPACK_IMPORTED_MODULE_2__.log)(MechaneTextfield.tag, "missing effect name attribute");
        }
        for (const event of ["keyup", "keydown"]) {
            const callback = (ev) => {
                if (!effectName) {
                    (0,_log_log__WEBPACK_IMPORTED_MODULE_2__.log)(`${MechaneTextfield.tag}`, "key failed", "missing name attribute");
                    return;
                }
                _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_5__.ShutterbugPixels.uiEvents?.send((0,_event__WEBPACK_IMPORTED_MODULE_9__.newMechaneTextfieldKeyUpOrDownUIEvent)(effectName, this.input.value, event === "keydown", ev));
            };
            this.input.addEventListener(event, callback);
            this.listener[event].callback = callback;
            _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_5__.ShutterbugPixels.uiEvents?.pushOverlayItemKeyUpOrDownListener(this.listener[event]);
        }
        // Submit effect.
        if (this.hasAttribute(MechaneTextfield.attribute.submitEffect)) {
            this.container.prepend(this.submitEffect);
        }
        // Input value.
        this.input.value =
            this.getAttribute(MechaneTextfield.attribute.inputValue) ?? "";
        // Styling.
        this.updateStyle();
    }
    disconnectedCallback() {
        // Remove the stored keyup/down listeners.
        for (const event of ["keyup", "keydown"]) {
            const listener = this.listener[event];
            _shutterbug_pixels__WEBPACK_IMPORTED_MODULE_5__.ShutterbugPixels.uiEvents?.popOverlayItemKeyUpOrDownListener(listener);
            listener.element.removeEventListener(listener.event, listener.callback);
        }
    }
    updateStyle() {
        const style = this.shadowRoot?.querySelector("style");
        if (!style) {
            return;
        }
        // if (
        //     this.getAttribute(MechaneTextfield.attribute.theme) !=
        //     theme.motifName
        // ) {
        //     return
        // }
        const slotFamilies = [
            [this.icon1Slot, "icon1Family"],
            [this.icon2Slot, "icon2Family"],
        ];
        for (const [e, f] of slotFamilies) {
            const family = this.getAttribute(MechaneTextfield.attribute[f]) ??
                _font__WEBPACK_IMPORTED_MODULE_1__.materialIconsClass.round;
            if ((0,_font__WEBPACK_IMPORTED_MODULE_1__.isMaterialIconsFamily)(family)) {
                (0,_font__WEBPACK_IMPORTED_MODULE_1__.setMaterialIconsClass)(e, family);
            }
        }
        // Input element attributes.
        this.input.type = this.hasAttribute(MechaneTextfield.attribute.mask)
            ? "password"
            : "text";
        const placeholder = this.getAttribute(MechaneTextfield.attribute.placeholder);
        if (placeholder) {
            this.input.placeholder = placeholder;
        }
        this.input.spellcheck = this.hasAttribute(MechaneTextfield.attribute.spellcheck)
            ? true
            : false;
        // Input element style.
        const size = Number(this.getAttribute(MechaneTextfield.attribute.size) ?? NaN);
        // Label.
        const label = this.getAttribute(MechaneTextfield.attribute.label);
        if (label) {
            this.labelText.textContent = label;
        }
        const originPosition = (0,_style_origin_position__WEBPACK_IMPORTED_MODULE_6__.getOriginPosition)(this.getAttribute(MechaneTextfield.attribute.originPosition)) ?? _style_theme__WEBPACK_IMPORTED_MODULE_8__.theme.defaultOriginPosition;
        // Accumulate CSS rules in a style sheet.
        const sheet = new Array();
        // Include the font styles. Even though the font rules are defined
        // globally, inside a custom element the font rules need to be defined
        // again.
        sheet.push(..._font__WEBPACK_IMPORTED_MODULE_1__.materialIcons);
        // Attribute flags can modify which styling is applied.
        const disabled = this.hasAttribute(_overlay__WEBPACK_IMPORTED_MODULE_4__.disabledAttribute);
        const submitted = this.hasAttribute(MechaneTextfield.attribute.submitEffect);
        // Container element.
        if (disabled) {
            sheet.push({
                selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.container.selector.normal,
                properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.container.style.disabled(originPosition),
            });
        }
        else if (submitted) {
            sheet.push({
                selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.container.selector.normal,
                properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.container.style.submitted(originPosition),
            });
        }
        else {
            sheet.push({
                selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.container.selector.normal,
                properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.container.style.normal(originPosition),
            }, {
                selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.container.selector.hover,
                properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.container.style.hover(),
            });
        }
        sheet.push({
            selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.containerChildren.selector,
            properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.containerChildren.style,
        });
        // Submit effect element.
        if (submitted) {
            const flashDuration = this.getAttribute(MechaneTextfield.attribute.cooldownDuration) ?? _style_theme__WEBPACK_IMPORTED_MODULE_8__.theme.defaultFlashDuration;
            sheet.push({
                selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.submitEffect.selector.normal,
                properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.submitEffect.style.normal(flashDuration, originPosition),
            }, {
                selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.submitEffect.selector.keyframes,
                properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.submitEffect.style.keyframes(_style_theme__WEBPACK_IMPORTED_MODULE_8__.theme.accent),
            });
        }
        // Slot elements.
        const iconColour1 = this.getAttribute(MechaneTextfield.attribute.icon1Colour) ??
            _style_theme__WEBPACK_IMPORTED_MODULE_8__.theme.primary;
        sheet.push({
            selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.iconSlot1.selector,
            properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.iconSlot1.style(disabled ? _style_theme__WEBPACK_IMPORTED_MODULE_8__.theme.disabled : iconColour1),
        });
        const iconColour2 = this.getAttribute(MechaneTextfield.attribute.icon2Colour) ??
            _style_theme__WEBPACK_IMPORTED_MODULE_8__.theme.primary;
        sheet.push({
            selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.iconSlot2.selector,
            properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.iconSlot2.style(disabled ? _style_theme__WEBPACK_IMPORTED_MODULE_8__.theme.disabled : iconColour2),
        });
        sheet.push({
            selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.button.selector,
            properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.button.style,
        });
        // Input field.
        sheet.push({
            selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.inputField.selector,
            properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.inputField.style(disabled, size),
        });
        if (!disabled) {
            sheet.push({
                selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.hoverInputField.selector,
                properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.hoverInputField.style,
            });
        }
        sheet.push({
            selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.activeInputField.selector,
            properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.activeInputField.style(disabled),
        });
        // Label text.
        sheet.push({
            selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.labelText.selector,
            properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.labelText.style,
        });
        sheet.push({
            selector: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.activeLabelText.selector,
            properties: _mechane_textfield_css__WEBPACK_IMPORTED_MODULE_3__.css.activeLabelText.style,
        });
        // Apply the accumulated CSS rules.
        style.textContent = (0,_crule_crule__WEBPACK_IMPORTED_MODULE_0__.toString)(sheet);
    }
}

//# sourceMappingURL=mechane-textfield.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/overlay.js":
/*!************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/overlay.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "disableAble": () => (/* binding */ disableAble),
/* harmony export */   "disabledAttribute": () => (/* binding */ disabledAttribute),
/* harmony export */   "hiddenClass": () => (/* binding */ hiddenClass),
/* harmony export */   "newOverlay": () => (/* binding */ newOverlay),
/* harmony export */   "overlayElementID": () => (/* binding */ overlayElementID),
/* harmony export */   "overlayItemElementTag": () => (/* binding */ overlayItemElementTag)
/* harmony export */ });
/* harmony import */ var _tag__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../tag */ "./build/custom-elements/tag.js");

function newOverlay() {
    const e = document.createElement("div");
    e.id = overlayElementID;
    return {
        disabled: false,
        items: new Map(),
        element: e,
    };
}
// The overlay element occupies the region above the shutterbug-pixels' canvas
// element.
const overlayElementID = "canvas-overlay-container";
// Overlay items. They live in the overlay container.
const overlayItemElementTag = "div";
// An overlay item element with this class is a temporarily deactivated
// invisible element.
const hiddenClass = "hidden-overlay-element";
// An overlay item element with this attribute is a permanently disabled
// partially visible element.
const disabledAttribute = "disabled-overlay-element";
// Elements supporting the disabled overlay element attribute.
const disableAble = [
    _tag__WEBPACK_IMPORTED_MODULE_0__.customTag.overlay.button,
    _tag__WEBPACK_IMPORTED_MODULE_0__.customTag.overlay.textfield,
];
//# sourceMappingURL=overlay.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/shutterbug-pixels.js":
/*!**********************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/shutterbug-pixels.js ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ShutterbugPixels": () => (/* binding */ ShutterbugPixels)
/* harmony export */ });
/* harmony import */ var _overlay__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./overlay */ "./build/custom-elements/shutterbug-pixels/overlay.js");
/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../style */ "./build/custom-elements/style.js");


// A ShutterbugPixels custom element displays the pixels of a shutterbug's view.
//
// The ShutterbugPixels element is a singleton class.
//
// The static context field for the canvas may be null, in which case do not use
// the ShutterbugPixels element.
class ShutterbugPixels extends HTMLElement {
    static #instance;
    // The tag name of this custom element.
    static tag = "shutterbug-pixels";
    // Custom attributes start with this prefix.
    static attributePrefix = ShutterbugPixels.tag + "-";
    static attributes = {
        // CSS rules for the container div. Can be used to make a border.
        border: ShutterbugPixels.attributePrefix + "b",
        // Background-colour of the canvas.
        colour: ShutterbugPixels.attributePrefix + "c",
        // Canvas height attribute.
        height: ShutterbugPixels.attributePrefix + "h",
        // Indicates if the client is kaput.
        kaput: ShutterbugPixels.attributePrefix + "k",
        // Indicates if text in the overlay can be selected.
        selectable: ShutterbugPixels.attributePrefix + "s",
        // Canvas width attribute.
        width: ShutterbugPixels.attributePrefix + "w",
    };
    static get observedAttributes() {
        return Object.values(ShutterbugPixels.attributes);
    }
    // The canvas element used to display the view associated with the
    // shutterbug.
    static context = document.createElement("canvas").getContext("2d");
    // The element which overlays the canvas element.
    static overlay = (0,_overlay__WEBPACK_IMPORTED_MODULE_0__.newOverlay)();
    // The element containing the canvas and the canvas overlay. An immediate
    // child of the shadow root.
    static container = document.createElement("div");
    static containerID = "shutterbug-pixels-container";
    static uiEvents = undefined;
    constructor() {
        // As a singleton class only one ShutterbugPixels instance is
        // constructed.
        if (ShutterbugPixels.#instance) {
            return ShutterbugPixels.#instance;
        }
        // Mandatory super call. Store the instance for subsequent constructor
        // calls.
        super();
        ShutterbugPixels.#instance = this;
        // Static fields.
        if (!ShutterbugPixels.context) {
            return;
        }
        ShutterbugPixels.container.id = ShutterbugPixels.containerID;
        ShutterbugPixels.container.append(ShutterbugPixels.context.canvas, ShutterbugPixels.overlay.element);
        // Shadow root and style.
        const style = document.createElement("style");
        const shadowRoot = this.attachShadow({ mode: "open" });
        shadowRoot.append(style, ShutterbugPixels.container);
    }
    attributeChangedCallback(name, oldValue) {
        // Disable the mouse movement listener when text selection is enabled.
        if (name == ShutterbugPixels.attributes.selectable) {
            if (oldValue === null) {
                ShutterbugPixels.uiEvents?.removeMouseMoveListener();
            }
            else {
                ShutterbugPixels.uiEvents?.addMouseMoveListener();
            }
        }
        this.updateStyle();
    }
    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return;
        }
        this.updateStyle();
    }
    updateStyle() {
        const style = this.shadowRoot?.querySelector("style");
        if (!style) {
            return;
        }
        const att = ShutterbugPixels.attributes;
        const b = this.getAttribute(att.border) ?? "";
        const c = this.getAttribute(att.colour) ?? "black";
        const h = this.getAttribute(att.height) ?? 0;
        const k = this.getAttribute(att.kaput) !== null ? true : false;
        const s = this.getAttribute(att.selectable) !== null ? true : false;
        const w = this.getAttribute(att.width) ?? 0;
        // Pixels are provided from the server in bottom-to-top row order but a
        // canvas expects them to be given in top-to-bottom order: mitigate this
        // with a Y-flip on the canvas element.
        style.textContent = `
        canvas {
            background-color: ${c};
            display: block;
            cursor: ${k ? "auto" : "grab"};
            transform: scale(1, -1);
            width: ${w}; height: ${h};
            transition: width 1s, height 1s;
        }

        canvas:active {
            cursor: ${k ? "auto" : "grabbing"};
        }

        .${_overlay__WEBPACK_IMPORTED_MODULE_0__.hiddenClass} {
            visibility: hidden;
        }

        #${ShutterbugPixels.containerID} {
            overflow: hidden;
            /* Allow overlay items to be positioned absolutely to this. */
            position: relative;
            ${b}
        }

        #${_overlay__WEBPACK_IMPORTED_MODULE_0__.overlayElementID} {
            pointer-events: none;
            /* Prevent text selection on overlay items when mouse dragging. */
            user-select: none;
            width: 100%; height: 100%;
        }
        
        #${_overlay__WEBPACK_IMPORTED_MODULE_0__.overlayElementID} > ${_overlay__WEBPACK_IMPORTED_MODULE_0__.overlayItemElementTag} {
            ${k ? "filter: " + _style__WEBPACK_IMPORTED_MODULE_1__.kaputFilter + ";" : ""}
            position: absolute;
            user-select: ${s ? "text" : "none"};
        }
        `;
    }
}

//# sourceMappingURL=shutterbug-pixels.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/style/flash.js":
/*!****************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/style/flash.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "flashEffect": () => (/* binding */ flashEffect),
/* harmony export */   "flashEffectAnimationName": () => (/* binding */ flashEffectAnimationName),
/* harmony export */   "flashEffectKeyframes": () => (/* binding */ flashEffectKeyframes)
/* harmony export */ });
/* harmony import */ var _origin_position__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./origin-position */ "./build/custom-elements/shutterbug-pixels/style/origin-position.js");
/* harmony import */ var _theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./theme */ "./build/custom-elements/shutterbug-pixels/style/theme.js");


const flashEffectAnimationName = "flash";
function flashEffect(animationDuration, originPosition) {
    return {
        animation: `${flashEffectAnimationName} ${animationDuration}`,
        background: "rgba(0, 0, 0, 0)",
        borderRadius: originPosition
            ? _origin_position__WEBPACK_IMPORTED_MODULE_0__.borderRadiuses.get(originPosition)
            : _theme__WEBPACK_IMPORTED_MODULE_1__.theme.buttonBorderRadius,
        // Offset the (padding + border) of the parent.
        // A top margin isn't necessary as the flex container centers the items.
        marginLeft: `calc(-1 * (${_theme__WEBPACK_IMPORTED_MODULE_1__.theme.buttonPadding} + ${_theme__WEBPACK_IMPORTED_MODULE_1__.theme.buttonBorder}))`,
        position: "absolute",
        zIndex: 1,
        width: `calc(100% + 2 * ${_theme__WEBPACK_IMPORTED_MODULE_1__.theme.buttonBorder})`,
        height: `calc(100% + 2 * ${_theme__WEBPACK_IMPORTED_MODULE_1__.theme.buttonBorder})`,
    };
}
function flashEffectKeyframes(flashColour) {
    return {
        from: {
            background: "rgba(0, 0, 0, 0)",
        },
        "50%": {
            background: flashColour,
        },
        to: {
            background: "rgba(0, 0, 0, 0)",
        },
    };
}
//# sourceMappingURL=flash.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/style/origin-position.js":
/*!**************************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/style/origin-position.js ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "borderRadiuses": () => (/* binding */ borderRadiuses),
/* harmony export */   "getOriginPosition": () => (/* binding */ getOriginPosition),
/* harmony export */   "positioningTransforms": () => (/* binding */ positioningTransforms)
/* harmony export */ });
/* harmony import */ var _theme__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./theme */ "./build/custom-elements/shutterbug-pixels/style/theme.js");

// Origin positioning.
const originPositions = [
    "top",
    "bottom",
    "left",
    "right",
    "center",
    "icon-center",
    "top-left",
    "top-right",
    "bottom-right",
    "bottom-left",
];
// Use a circular button corner except when that corner overlaps the origin.
const borderRadiuses = new Map([
    ["top", _theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius],
    ["bottom", _theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius],
    ["left", _theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius],
    ["right", _theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius],
    ["center", _theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius],
    ["icon-center", _theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius],
    [
        "top-left",
        `0 ${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius} ${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius} ${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius}`,
    ],
    [
        "top-right",
        `${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius} 0 ${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius} ${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius}`,
    ],
    [
        "bottom-right",
        `${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius} ${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius} 0 ${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius}`,
    ],
    [
        "bottom-left",
        `${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius} ${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius} ${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorderRadius} 0`,
    ],
]);
function isOriginPosition(value) {
    return originPositions.includes(value);
}
function getOriginPosition(attributeValue) {
    if (!attributeValue || !isOriginPosition(attributeValue)) {
        return null;
    }
    return attributeValue;
}
const positioningTransforms = new Map([
    ["top", "translateX(-50%)"],
    ["bottom", "translate(-50%, -100%)"],
    ["left", "translateY(-50%)"],
    ["right", "translate(-100%, -50%)"],
    ["center", "translate(-50%, -50%)"],
    [
        "icon-center",
        `translate(calc(-${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonBorder} - ${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonPadding} - ${_theme__WEBPACK_IMPORTED_MODULE_0__.theme.buttonIconFontSize} / 2), -50%)`,
    ],
    ["top-left", "translate(0, 0)"],
    ["top-right", "translateX(-100%)"],
    ["bottom-right", "translate(-100%, -100%)"],
    ["bottom-left", "translateY(-100%)"],
]);
//# sourceMappingURL=origin-position.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/style/theme.js":
/*!****************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/style/theme.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "theme": () => (/* binding */ theme)
/* harmony export */ });
/* harmony import */ var _motif_motif__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../motif/motif */ "./build/motif/motif.js");

const theme = {
    ..._motif_motif__WEBPACK_IMPORTED_MODULE_0__.initial,
    // Flash effect.
    defaultFlashDuration: "0.4s",
    // Placement of the on-screen position of the button's mechtree node origin
    // relative to the button.
    //
    // Without a transform applied to the button the origin is at the top left
    // corner.
    defaultOriginPosition: "top-left",
};
//# sourceMappingURL=theme.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/update-canvas.js":
/*!******************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/update-canvas.js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "updateCanvas": () => (/* binding */ updateCanvas)
/* harmony export */ });
// Use the data in the arraybuffer to update the canvas element.
//
// The data in the arraybuffer is an image in binary form along with a suffix
// giving the image dimensions.
function updateCanvas(context, data) {
    const suffixLen = 8; // The data has a suffix occupying this number of bytes.
    const dataLen = data.byteLength;
    const pixLen = dataLen - suffixLen;
    if (dataLen < suffixLen) {
        console.log("pixel socket: message data is too short");
        return;
    }
    // The suffix contains the image dimensions in units of pixels.
    //
    // The first four bytes of the suffix represent an integer, in order of most
    // significant byte first, which is the image's width in pixels. Similarly
    // the remaining four bytes represent the height.
    const dimData = new Uint8Array(data, dataLen - suffixLen, suffixLen);
    const w = dimData[0] * (1 << 24) +
        dimData[1] * (1 << 16) +
        dimData[2] * (1 << 8) +
        dimData[3];
    const h = dimData[4] * (1 << 24) +
        dimData[5] * (1 << 16) +
        dimData[6] * (1 << 8) +
        dimData[7];
    // Each pixel is composed of four channels (RGBA). All channels have the
    // same colour depth, either one or two bytes. Thus there are two possible
    // lengths of the image data for a known width and height.
    const pixStandardLen = w * h * 4;
    const pixDeepLen = pixStandardLen * 2;
    switch (pixLen) {
        case pixStandardLen:
            break;
        case pixDeepLen:
            console.log("pixel socket: 16 bit RGBA channels not yet supported");
            return;
        default:
            console.log("pixel socket: wrong sized image or w/h suffix");
            return;
    }
    const pix = new Uint8ClampedArray(data, 0, pixLen);
    if (context.canvas.width != w || context.canvas.height != h) {
        context.canvas.width = w;
        context.canvas.height = h;
    }
    context.putImageData(new ImageData(pix, w, h), 0, 0);
}
//# sourceMappingURL=update-canvas.js.map

/***/ }),

/***/ "./build/custom-elements/shutterbug-pixels/update-overlay.js":
/*!*******************************************************************!*\
  !*** ./build/custom-elements/shutterbug-pixels/update-overlay.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "disableOverlay": () => (/* binding */ disableOverlay),
/* harmony export */   "updateOverlay": () => (/* binding */ updateOverlay)
/* harmony export */ });
/* harmony import */ var _overlay__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./overlay */ "./build/custom-elements/shutterbug-pixels/overlay.js");
/* harmony import */ var _schema_schema__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../schema/schema */ "./build/schema/schema.js");


function updateOverlay(overlay, overlayUpdate) {
    if (overlay.disabled) {
        return;
    }
    if ("create" in overlayUpdate) {
        for (const name of overlayUpdate.create) {
            const e = document.createElement(_overlay__WEBPACK_IMPORTED_MODULE_0__.overlayItemElementTag);
            e.classList.add(_overlay__WEBPACK_IMPORTED_MODULE_0__.hiddenClass);
            overlay.element.appendChild(e);
            overlay.items.set(name, {
                element: e,
                innerHTMLHash: "",
                left: "",
                top: "",
                transform: "",
                transformOrigin: "",
                zIndex: 0,
            });
        }
    }
    else if ("delete" in overlayUpdate) {
        for (const name of overlayUpdate.delete) {
            const item = overlay.items.get(name);
            if (item) {
                item.element.remove();
                overlay.items.delete(name);
            }
            else {
                console.log(`cannot delete missing overlay item: ${name}`);
            }
        }
    }
    else if ("hide" in overlayUpdate) {
        for (const name of overlayUpdate.hide) {
            const item = overlay.items.get(name);
            if (item) {
                item.element.classList.add(_overlay__WEBPACK_IMPORTED_MODULE_0__.hiddenClass);
            }
            else {
                console.log(`cannot hide missing overlay item: ${name}`);
            }
        }
    }
    else if ("update" in overlayUpdate) {
        for (const itemUpdate of overlayUpdate.update) {
            const item = overlay.items.get(itemUpdate.name);
            if (item) {
                updateItem(item, itemUpdate);
                item.element.classList.remove(_overlay__WEBPACK_IMPORTED_MODULE_0__.hiddenClass);
            }
            else {
                console.log(`cannot update missing overlay item: ${itemUpdate.name}`);
            }
        }
    }
}
function disableOverlay(overlay, closeEvent) {
    overlay.disabled = true;
    let title = "No connection.\n";
    const reasonResult = _schema_schema__WEBPACK_IMPORTED_MODULE_1__.normalCloseReasons.safeParse(closeEvent.reason);
    if (reasonResult.success) {
        switch (reasonResult.data) {
            case "intercepted terminal interrupt":
                title += "Mechane was shut down from a terminal.\n";
                break;
            case "received client request":
                title += "Mechane was shut down by a client.\n";
                break;
        }
    }
    else {
        if (closeEvent.reason.length > 0) {
            title += `Websocket close reason: ${closeEvent.reason}.\n`;
        }
        title += [
            `Websocket close code: ${closeEvent.code}.`,
            `Websocket path: ${closeEvent.sourceWebSocket}.`,
            "",
        ].join("\n");
    }
    overlay.items.forEach((item) => {
        for (const tag of _overlay__WEBPACK_IMPORTED_MODULE_0__.disableAble) {
            item.element.querySelectorAll(tag).forEach((e) => {
                e.setAttribute(_overlay__WEBPACK_IMPORTED_MODULE_0__.disabledAttribute, "true");
                e.setAttribute("title", title);
            });
        }
    });
}
function updateItem(item, update) {
    if (item.innerHTMLHash != update.innerHTMLHash) {
        item.element.innerHTML = update.innerHTML;
        item.innerHTMLHash = update.innerHTMLHash;
    }
    // Style properties.
    if (item.left != update.left) {
        item.element.style.left = update.left;
        item.left = update.left;
    }
    if (item.top != update.top) {
        item.element.style.top = update.top;
        item.top = update.top;
    }
    if (item.transform != update.transform) {
        item.element.style.transform = update.transform;
        item.transform = update.transform;
    }
    if (item.transformOrigin != update.transformOrigin) {
        item.element.style.transformOrigin = update.transformOrigin;
        item.transformOrigin = update.transformOrigin;
    }
    if (item.zIndex != update.zIndex) {
        item.element.style.zIndex = update.zIndex.toString();
        item.zIndex = update.zIndex;
    }
}
//# sourceMappingURL=update-overlay.js.map

/***/ }),

/***/ "./build/custom-elements/stitching-chooser/stitching-chooser.js":
/*!**********************************************************************!*\
  !*** ./build/custom-elements/stitching-chooser/stitching-chooser.js ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StitchingChooser": () => (/* binding */ StitchingChooser)
/* harmony export */ });
/* harmony import */ var roughjs_bin_rough__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! roughjs/bin/rough */ "./node_modules/roughjs/bin/rough.js");
/* harmony import */ var _stitches_stringify__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @stitches/stringify */ "./node_modules/@stitches/stringify/dist/index.mjs");
/* harmony import */ var _crule_crule__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../crule/crule */ "./build/crule/crule.js");
/* harmony import */ var _font__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../font */ "./build/custom-elements/font.js");
/* harmony import */ var _schema_schema__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../schema/schema */ "./build/schema/schema.js");
/* harmony import */ var _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shutterbug-pixels/style/theme */ "./build/custom-elements/shutterbug-pixels/style/theme.js");






// Allow a user to choose, from the restricted set of allowed options, the
// position and size of cameras on the ShutterbugPixels view screen.
class StitchingChooser extends HTMLElement {
    static get observedAttributes() {
        return [StitchingChooser.associationsAttribute, "w"];
    }
    // Attribute providing the stitching method and camera names.
    static associationsAttribute = "associations";
    // The tag name of this custom element.
    static tag = "stitching-chooser";
    // The class used by buttons that only appear when the :hover is on for the
    // canvas element.
    static vanishingButtonClass = StitchingChooser.tag + "-vanishing-button-class";
    // The camera swap buttons fire a custom event to the Elm integrant in
    // order to swap two camera positions.
    static cameraSwapEvent = StitchingChooser.tag + "-switch-cameras";
    // The stitching size toggle button fires a custom event to the Elm
    // integrant in order to switch between partial and full size camera
    // stitching.
    static toggleStitchingSizeEvent = StitchingChooser.tag + "-toggle-stitching-size";
    // The width to height ratio of the size of the canvas element.
    static ratio = 1.6;
    // The canvas dimensions in terms of pixels. To scale the canvas to occupy a
    // different sized area but still leaving the canvas pixel dimensions the
    // same use the "w" attribute.
    static height = 240;
    static width = StitchingChooser.height * StitchingChooser.ratio;
    static canvasContainerID = StitchingChooser.tag + "-canvas-container";
    canvasContainer = document.createElement("div");
    // The stitching chooser is visually implemented with a canvas. The canvas
    // is used to draw, for example, the outlines of screens showing the camera
    // stitching situation.
    context = document.createElement("canvas").getContext("2d");
    // Camera swappers allow the user to swap two camera positions.
    cameraSwapper1 = cameraSwapper();
    cameraSwapper2 = cameraSwapper();
    cameraSwapper3 = cameraSwapper();
    // Stitching size togglers.
    fullSizeOn = stitchingSizeToggler(true);
    fullSizeOff = stitchingSizeToggler(false);
    constructor() {
        super();
        // Instance fields.
        if (!this.context) {
            console.log(`${StitchingChooser.tag}: failed to create a canvas rendering context`);
            return;
        }
        this.context.canvas.width = StitchingChooser.width;
        this.context.canvas.height = StitchingChooser.height;
        this.canvasContainer.id = StitchingChooser.canvasContainerID;
        this.canvasContainer.style.position = "relative"; // For absolutely positioned canvas overlay buttons.
        this.canvasContainer.append(this.context.canvas, this.cameraSwapper1, this.cameraSwapper2, this.cameraSwapper3, this.fullSizeOff, this.fullSizeOn);
        // Shadow root and style.
        const shadowRoot = this.attachShadow({ mode: "open" });
        const style = document.createElement("style");
        shadowRoot.append(style, this.canvasContainer);
    }
    attributeChangedCallback() {
        this.updateStyle();
    }
    connectedCallback() {
        if (!this.isConnected) {
            // connectedCallback() may be called once your element is no longer
            // connected.
            return;
        }
        this.updateStyle();
    }
    updateStyle() {
        if (!this.shadowRoot) {
            return;
        }
        const style = this.shadowRoot.querySelector("style");
        if (!style) {
            return;
        }
        if (!this.context) {
            return;
        }
        const associationsValue = this.getAttribute(StitchingChooser.associationsAttribute);
        if (!associationsValue) {
            return;
        }
        const associations = associationsValue.split(",");
        const stitching = associations.at(0);
        const cameras = associations.slice(1);
        if (!stitching || !_schema_schema__WEBPACK_IMPORTED_MODULE_3__.stitching.safeParse(stitching).success) {
            console.log(`${StitchingChooser.tag}: ` +
                (stitching
                    ? `unknown stitching method: ${stitching}`
                    : "a stitching method is required"));
            return;
        }
        // Reset camera switchers to hidden, then enable later as required.
        this.cameraSwapper1.style.display = "none";
        this.cameraSwapper2.style.display = "none";
        this.cameraSwapper3.style.display = "none";
        // Likewise with the camera size togglers.
        this.fullSizeOff.style.display = "none";
        this.fullSizeOn.style.display = "none";
        const w = StitchingChooser.width;
        const h = StitchingChooser.height;
        this.context.clearRect(0, 0, w, h);
        const roughCanvas = roughjs_bin_rough__WEBPACK_IMPORTED_MODULE_0__["default"].canvas(this.context.canvas);
        const padding = 5;
        const innerH = h - 2 * padding;
        const innerW = w - 2 * padding;
        const smallFontH = 15;
        const fullFontH = 24;
        // Accumulate CSS rules in a style sheet.
        const sheet = new Array();
        // Include the font styles. Even though the font rules are defined
        // globally, inside a custom element the font rules need to be defined
        // again.
        sheet.push(..._font__WEBPACK_IMPORTED_MODULE_2__.fonts);
        sheet.push({
            selector: `#${StitchingChooser.canvasContainerID}`,
            properties: {
                "--vanishing-button-opacity": 0,
            },
        }, {
            selector: `#${StitchingChooser.canvasContainerID}:hover`,
            properties: {
                "--vanishing-button-opacity": 1,
            },
        });
        // They are circular buttons with a variable border and box shadow.
        const buttonBorder = "0.1em";
        const buttonPadding = "0.3em";
        sheet.push({
            selector: `.${StitchingChooser.vanishingButtonClass}`,
            properties: {
                background: _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.buttonBackground,
                borderRadius: `calc(0.5em + ${buttonBorder} + ${buttonPadding})`,
                color: _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.primary,
                border: `${_shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.primary} dotted ${buttonBorder}`,
                opacity: "var(--vanishing-button-opacity)",
                padding: buttonPadding,
                transition: "box-shadow 300ms, opacity 300ms",
                userSelect: "none",
            },
        }, {
            selector: `.${StitchingChooser.vanishingButtonClass}:hover`,
            properties: {
                borderStyle: "solid",
                boxShadow: `0 0 0.3em 0.2em ${_shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.primaryGlow}`,
            },
        });
        // If required, scale the canvas element beyond its actual pixels unit
        // dimensions with CSS.
        const cssWidth = this.getAttribute("w");
        if (cssWidth) {
            this.canvasContainer.style.width = cssWidth;
            sheet.push({
                selector: "canvas",
                properties: { width: `${cssWidth}` },
            });
        }
        // Apply the accumulated CSS rules.
        style.textContent = (0,_crule_crule__WEBPACK_IMPORTED_MODULE_1__.toString)(sheet);
        if (cameras.length == 0 || !cameras[0]) {
            if (stitching == "uni") {
                // Deal with the no camera associations special case.
                roughCanvas.rectangle(padding, padding, innerW, innerH, {
                    stroke: _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.textPassive,
                });
                roughCanvas.circle(w / 2, h / 2, h / 4, { fill: _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.error });
            }
            else {
                console.log(`${StitchingChooser.tag}: ` + "camera names are required");
            }
            return;
        }
        const drawContext = {
            bowing: 3,
            cameras: cameras,
            cameraSwappers: [
                this.cameraSwapper1,
                this.cameraSwapper2,
                this.cameraSwapper3,
            ],
            context: this.context,
            fullFillStyle: "hachure",
            fullFont: `${fullFontH}px CascadiaCode`,
            fullFontBold: `bold ${fullFontH}px CascadiaCode`,
            fullFontH: fullFontH,
            fullPadding: padding * 3,
            fullSizeOn: this.fullSizeOn,
            fullSizeOff: this.fullSizeOff,
            h: h,
            innerH: innerH,
            innerW: innerW,
            padding: padding,
            roughCanvas: roughCanvas,
            smallFill: _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.stitchingMinor1,
            smallFont: `${smallFontH}px CascadiaCode`,
            smallFontH: smallFontH,
            smallStroke: _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.stitchingMinor2,
            smallH: innerH / 3,
            smallW: innerW / 3,
            w: w,
        };
        // Draw the major screen. It always occupies the whole view and is the
        // farthest back.
        roughCanvas.rectangle(padding, padding, innerW, innerH, {
            fill: _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.stitchingMajor,
            hachureGap: 12,
            stroke: _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.textPassive,
        });
        // Draw the minor screen(s).
        switch (stitching) {
            case "uni":
                drawPartialModeMajorName(drawContext, cameras[0]);
                break;
            case "bi":
            case "tri":
            case "quad":
                drawFullModeMinorScreens(drawContext);
                break;
            case "biPartial":
            case "triPartial":
            case "quadPartial":
                drawPartialModeMinorScreens(drawContext);
                break;
        }
    }
}

function cameraSwapper() {
    const textStyle = {
        // Hide by default. There are more camera swappers than required in some
        // cases.
        display: "none",
        position: "absolute",
    };
    const text = document.createElement("text");
    text.style.cssText = (0,_stitches_stringify__WEBPACK_IMPORTED_MODULE_5__.stringify)(textStyle);
    const spanStyle = {
        transform: "translate(-50%, -50%)",
    };
    const span = document.createElement("span");
    span.style.cssText = (0,_stitches_stringify__WEBPACK_IMPORTED_MODULE_5__.stringify)(spanStyle);
    span.classList.add("material-icons", StitchingChooser.vanishingButtonClass);
    span.innerHTML = "cameraswitch";
    text.append(span);
    return text;
}
function stitchingSizeToggler(isPartial) {
    const textStyle = {
        display: "none",
        left: "1em",
        position: "absolute",
        top: "1em",
    };
    const text = document.createElement("text");
    text.title = isPartial ? "Go full size mode." : "Go mixed size mode.";
    text.style.cssText = (0,_stitches_stringify__WEBPACK_IMPORTED_MODULE_5__.stringify)(textStyle);
    text.onclick = () => {
        const event = new CustomEvent(StitchingChooser.toggleStitchingSizeEvent, {
            bubbles: true,
            cancelable: false,
            composed: true,
        });
        text.dispatchEvent(event);
    };
    const span = document.createElement("span");
    span.classList.add("material-icons", StitchingChooser.vanishingButtonClass);
    if (isPartial) {
        span.innerHTML = "open_in_full";
    }
    else {
        span.innerHTML = "close_fullscreen";
    }
    text.append(span);
    return text;
}
function drawSmallScreen(x, y, major, minor, switcher, { bowing, context, h, roughCanvas, smallFill, smallFont, smallFontH, smallStroke, smallH, smallW, w, }) {
    // Fade the background.
    context.beginPath();
    context.rect(x, y, smallW, smallH);
    context.closePath();
    context.fillStyle = _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.buttonBackground;
    context.fill();
    // Draw the small screen.
    roughCanvas.rectangle(x, y, smallW, smallH, {
        bowing: bowing,
        fill: smallFill,
        fillWeight: 0.5,
        hachureAngle: 41,
        hachureGap: 10,
        stroke: smallStroke,
        strokeWidth: 3,
    });
    // Name the small screen.
    drawTextWithShadow(minor, x + smallW / 2, y + smallH / 2 + smallFontH / 2, context, smallFill, smallFont, smallW);
    // Add a camera switcher.
    switcher.style.display = "inline"; // Undo display none.
    switcher.style.left = `${(100 * (x + smallW / 2)) / w}%`;
    switcher.style.top = `${(100 * y) / h}%`;
    switcher.style.transform = "translate(0%, -20%)";
    switcher.title = `Swap ${major} with ${minor}.`;
    switcher.onclick = () => {
        const event = new CustomEvent(StitchingChooser.cameraSwapEvent, {
            bubbles: true,
            cancelable: false,
            composed: true,
            detail: { camera1: major, camera2: minor },
        });
        switcher.dispatchEvent(event);
    };
}
function drawTextWithShadow(text, x, y, context, fill, font, maxWidth) {
    context.filter =
        `drop-shadow(0px 0px 5px ${_shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.buttonOpaqueBackground}) `.repeat(3);
    context.fillStyle = fill;
    if (font) {
        context.font = font;
    }
    context.fillText(text, x, y, maxWidth);
    context.filter = "none";
}
function drawFullModeName(dc, name, y, fill) {
    dc.context.font = dc.fullFontBold;
    dc.context.textAlign = "end";
    drawTextWithShadow(name, dc.w - (2 * dc.smallW) / 3, y, dc.context, fill);
}
function drawFullModeMinorScreens(dc) {
    const l = dc.cameras.length;
    if (l < 2 || l > 4) {
        return;
    }
    const dest = document.createElement("canvas").getContext("2d");
    if (!dest) {
        return;
    }
    const src = document.createElement("canvas").getContext("2d");
    if (!src) {
        return;
    }
    dest.canvas.width = dc.w;
    dest.canvas.height = dc.h;
    src.canvas.width = dc.w;
    src.canvas.height = dc.h;
    const roughSrc = roughjs_bin_rough__WEBPACK_IMPORTED_MODULE_0__["default"].canvas(src.canvas);
    const angle = -41; // A default angle in the roughjs package.
    const angles = [angle + 45, angle + 2 * 45, angle + 3 * 45];
    const fills = [
        _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.stitchingMinor1,
        _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.stitchingMinor2,
        _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.stitchingMinor3,
    ];
    const paddingRatios = [1, 0.75, 0.5]; // Ratio of the default padding.
    const textEnd = dc.w - (2 * dc.smallW) / 3; // Text is right aligned to this.
    const switcherMiddle = dc.w - dc.smallW / 3;
    // Ratios of the height.
    const fontRatio = dc.fullFontH / dc.h;
    const gapRatio = (1 - 0.15 * 2) / 3; // Gap between baselines of text.
    const startRatio = 0.85; // Position the first text at this height.
    // Draw the name of the major screen.
    drawFullModeName(dc, dc.cameras[0], dc.h * startRatio + dc.fullFontH / 2, _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.primaryLight);
    for (let i = 0; i < l - 1; i++) {
        // Fill dest with the fuzzed up text of previous screens.
        dest.clearRect(0, 0, dc.w, dc.h);
        dest.globalCompositeOperation = "source-over"; // Restore the normal op.
        dest.filter = "blur(6px)"; // Makes dest a partially transparent mask.
        dest.fillStyle = _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.buttonOpaqueBackground;
        dest.font = dc.fullFontBold;
        dest.textAlign = "end";
        for (let j = i; j >= 0; j--) {
            // Increase iterations to increase text visibility below subsequent
            // hachured rectangular screen areas.
            const fuzzIterations = 1;
            for (let k = 0; k < fuzzIterations; k++) {
                dest.fillText(dc.cameras[j], textEnd, dc.h * (startRatio - gapRatio * j) + dc.fullFontH / 2);
            }
        }
        dest.filter = "none";
        // When there are four cams you want to start with the first (biggest)
        // padding for the innermost minor camera. Then each successive camera
        // uses less padding so the strokes of closer cameras are outwith the
        // strokes of farther cameras.
        //
        // When there are only two cams, the single minor camera should use the
        // smallest padding.
        const paddingStartIndex = 4 - l;
        const paddingRatio = paddingRatios[i + paddingStartIndex];
        // Fill src with the hachured rectangular screen area.
        src.clearRect(0, 0, dc.w, dc.h);
        roughSrc.rectangle(dc.fullPadding * paddingRatio, dc.fullPadding * paddingRatio, dc.w - 2 * dc.fullPadding * paddingRatio, dc.h - 2 * dc.fullPadding * paddingRatio, {
            bowing: dc.bowing,
            fill: fills[i],
            fillStyle: dc.fullFillStyle,
            fillWeight: 6,
            hachureAngle: angles[i],
            hachureGap: 45,
            stroke: fills[i],
            strokeWidth: 3,
        });
        // Draw the hachured rectangular screen area but leave partial holes
        // above the text of previous rectangular screen areas.
        // The leave-holes-where-dest-exists compositing operation.
        dest.globalCompositeOperation = "source-out";
        dest.drawImage(src.canvas, 0, 0);
        dc.context.drawImage(dest.canvas, 0, 0);
        // Draw the text of the current screen.
        const y = dc.h * (startRatio - gapRatio * (i + 1)) + dc.fullFontH / 2;
        drawFullModeName(dc, dc.cameras[i + 1], y, fills[i]);
        // Add a camera switcher.
        const switcher = dc.cameraSwappers[i];
        const prevCamera = dc.cameras[i];
        const camera = dc.cameras[i + 1];
        if (!switcher || !prevCamera || !camera) {
            return;
        }
        switcher.style.display = "inline"; // Undo display none.
        switcher.style.left = `${100 * (switcherMiddle / dc.w)}%`;
        switcher.style.top = `${100 * (y / dc.h + (gapRatio - fontRatio) / 2)}%`;
        switcher.style.transform = "translate(0%, 0%)"; // Clear transform from partial mode.
        switcher.title = `Swap ${prevCamera} with ${camera}.`;
        switcher.onclick = () => {
            const event = new CustomEvent(StitchingChooser.cameraSwapEvent, {
                bubbles: true,
                cancelable: false,
                composed: true,
                detail: { camera1: prevCamera, camera2: camera },
            });
            switcher.dispatchEvent(event);
        };
    }
    // Show the stitching size toggler which enables mixed size mode.
    dc.fullSizeOff.style.display = "inline"; // Undo display none.
}
function drawPartialModeMajorName(dc, name) {
    dc.context.font = dc.fullFont;
    dc.context.textAlign = "center";
    drawTextWithShadow(name, dc.padding + dc.innerW / 2, dc.padding + dc.smallH, dc.context, _shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_4__.theme.stitchingMajor);
}
function drawPartialModeMinorScreens(dc) {
    if (dc.cameras.length < 2 || dc.cameras.length > 4) {
        return;
    }
    const major = dc.cameras[0];
    // Name the major screen.
    drawPartialModeMajorName(dc, major);
    // Show the stitching size toggler which enables full size mode.
    dc.fullSizeOn.style.display = "inline"; // Undo display none.
    // Draw the minor screens left to right.
    for (let i = 0; i < dc.cameras.length - 1; i++) {
        let xPadding = 0;
        switch (dc.cameras.length) {
            case 2:
                xPadding = dc.padding + 2 * dc.smallW;
                break;
            case 3:
                xPadding = dc.padding + 2 * i * dc.smallW;
                break;
            case 4:
                xPadding = dc.padding + i * dc.smallW;
                break;
        }
        drawSmallScreen(xPadding, dc.padding + 2 * dc.smallH, major, dc.cameras[i + 1], dc.cameraSwappers[i], dc);
    }
}
//# sourceMappingURL=stitching-chooser.js.map

/***/ }),

/***/ "./build/custom-elements/style.js":
/*!****************************************!*\
  !*** ./build/custom-elements/style.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "kaputFilter": () => (/* binding */ kaputFilter)
/* harmony export */ });
const kaputFilter = "blur(0.08em)";
//# sourceMappingURL=style.js.map

/***/ }),

/***/ "./build/custom-elements/tag.js":
/*!**************************************!*\
  !*** ./build/custom-elements/tag.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "customTag": () => (/* binding */ customTag),
/* harmony export */   "effectNameAttrSuffix": () => (/* binding */ effectNameAttrSuffix)
/* harmony export */ });
// Custom element tag names.
const customTag = {
    // Custom tags used as overlay items.
    overlay: {
        asciidoc: "mechane-asciidoc",
        button: "mechane-button",
        pre: "mechane-pre",
        textfield: "mechane-textfield",
    },
    shutterbugPixels: "shutterbug-pixels",
};
// A common suffix for the per tag name attributes used by overlay items which
// have a side effect.
//
// The full attribute name is formed by appending this suffix to the tag name.
const effectNameAttrSuffix = "-effect-name";
//# sourceMappingURL=tag.js.map

/***/ }),

/***/ "./build/init/init.js":
/*!****************************!*\
  !*** ./build/init/init.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ports": () => (/* binding */ ports)
/* harmony export */ });
const elmApp = Elm.Main.init({ flags: JSON.stringify(window.location.href) });
const ports = elmApp.ports;
//# sourceMappingURL=init.js.map

/***/ }),

/***/ "./build/log/log.js":
/*!**************************!*\
  !*** ./build/log/log.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "log": () => (/* binding */ log)
/* harmony export */ });
function log(...parts) {
    console.log(parts.join(": "));
}
//# sourceMappingURL=log.js.map

/***/ }),

/***/ "./build/main.js":
/*!***********************!*\
  !*** ./build/main.js ***!
  \***********************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _custom_elements_define__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./custom-elements/define */ "./build/custom-elements/define.js");
/* harmony import */ var _init_init__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./init/init */ "./build/init/init.js");
/* harmony import */ var _custom_elements_shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./custom-elements/shutterbug-pixels/style/theme */ "./build/custom-elements/shutterbug-pixels/style/theme.js");
/* harmony import */ var _custom_elements_shutterbug_pixels_update_overlay__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./custom-elements/shutterbug-pixels/update-overlay */ "./build/custom-elements/shutterbug-pixels/update-overlay.js");
/* harmony import */ var _custom_elements_shutterbug_pixels_shutterbug_pixels__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./custom-elements/shutterbug-pixels/shutterbug-pixels */ "./build/custom-elements/shutterbug-pixels/shutterbug-pixels.js");
/* harmony import */ var _motif_motif__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./motif/motif */ "./build/motif/motif.js");
/* harmony import */ var _schema_schema__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./schema/schema */ "./build/schema/schema.js");
/* harmony import */ var _websockets_opened_websockets__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./websockets/opened-websockets */ "./build/websockets/opened-websockets.js");
/* harmony import */ var _websockets_websocket_to_camera__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./websockets/websocket-to-camera */ "./build/websockets/websocket-to-camera.js");
/* harmony import */ var _websockets_websocket_to_devoir__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./websockets/websocket-to-devoir */ "./build/websockets/websocket-to-devoir.js");
/* harmony import */ var _websockets_websocket_to_overlayer__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./websockets/websocket-to-overlayer */ "./build/websockets/websocket-to-overlayer.js");
/* harmony import */ var _websockets_websocket_to_pixel__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./websockets/websocket-to-pixel */ "./build/websockets/websocket-to-pixel.js");
/* harmony import */ var _websockets_websocket_to_uievent__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./websockets/websocket-to-uievent */ "./build/websockets/websocket-to-uievent.js");
// Define custom elements first.

// Initialise the Elm app.












await main();
async function main() {
    // Obtain the motif.
    const motifGetResult = await (0,_motif_motif__WEBPACK_IMPORTED_MODULE_5__.getMotif)();
    if (motifGetResult.result === "failure") {
        _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.receiveLifeCycle.send({
            problem: "failed to get the Typescript motif",
        });
        return;
    }
    Object.assign(_custom_elements_shutterbug_pixels_style_theme__WEBPACK_IMPORTED_MODULE_2__.theme, motifGetResult.motif);
    // Assert the successful acquisition of a canvas context and shadowRoot on
    // the ShutterbugPixels singleton class.
    const context = _custom_elements_shutterbug_pixels_shutterbug_pixels__WEBPACK_IMPORTED_MODULE_4__.ShutterbugPixels.context;
    if (context === null) {
        _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.receiveLifeCycle.send({
            problem: "failed to create a canvas rendering context",
        });
        return;
    }
    const shadowRoot = new _custom_elements_shutterbug_pixels_shutterbug_pixels__WEBPACK_IMPORTED_MODULE_4__.ShutterbugPixels().shadowRoot;
    if (shadowRoot === null) {
        _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.receiveLifeCycle.send({
            problem: "failed to create a shadowRoot",
        });
        return;
    }
    // The Elm integrant acquires a shutterbug from manual user entry or the
    // Mechane server via a HTTP request and forwards it to here, the Typescript
    // integrant.
    //
    // An extant shutterbug is needed to successfully open websockets on the
    // Mechane server.
    const shutterbug = await new Promise(function (resolve) {
        // Receive the 'shutterbug' message from the Elm integrant.
        _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.sendShutterbug.subscribe(function callback(message) {
            _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.sendShutterbug.unsubscribe(callback); // Only need first message.
            const result = _schema_schema__WEBPACK_IMPORTED_MODULE_6__.shutterbug.safeParse(message);
            if (!result.success) {
                console.error(result.error.issues);
            }
            // Listen to the Elm integrant for user requests to change the
            // fullscreen state.
            _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.sendFullscreen.subscribe(function (fullscreen) {
                if (fullscreen && document.fullscreenElement === null) {
                    void document.documentElement.requestFullscreen();
                }
                else if (!fullscreen && document.fullscreenElement !== null) {
                    void document.exitFullscreen();
                }
            });
            // Listen to the Elm integrant for user requests to change the
            // pointer lock state.
            _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.sendPointerLock.subscribe(function (pointerLock) {
                if (pointerLock && document.pointerLockElement === null) {
                    void context.canvas.requestPointerLock();
                }
                else if (!pointerLock &&
                    document.pointerLockElement !== null) {
                    // The pointer unlock button will generally be inaccessible
                    // by the mouse in pointer lock mode and therefore useless
                    // but listen for clicks anyway for completeness sake.
                    void document.exitPointerLock();
                }
            });
            // Listen to the Elm integrant for user requests to open a new tab.
            _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.sendOpenNewTab.subscribe(function (url) {
                window.open(url, "_blank");
            });
            // Listen to the Elm integrant for user requests to copy the
            // shutterbug to the clipboard.
            _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.sendWriteShutterbugToClipboard.subscribe(function () {
                void navigator.clipboard.writeText(shutterbug);
            });
            // Listen to the Elm integrant for user requests to copy some kind
            // of text to the clipboard.
            _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.sendWriteTextToClipboard.subscribe(function (text) {
                void navigator.clipboard.writeText(text);
            });
            resolve(message);
        });
    });
    const openUIEventResult = (0,_websockets_websocket_to_uievent__WEBPACK_IMPORTED_MODULE_12__.openUIEventWebSocket)(shutterbug, _custom_elements_shutterbug_pixels_shutterbug_pixels__WEBPACK_IMPORTED_MODULE_4__.ShutterbugPixels.container, context, shadowRoot);
    const webSocketLifeCycles = [
        (0,_websockets_websocket_to_camera__WEBPACK_IMPORTED_MODULE_8__.openCameraWebSocket)(shutterbug),
        (0,_websockets_websocket_to_devoir__WEBPACK_IMPORTED_MODULE_9__.openDevoirWebSocket)(shutterbug),
        (0,_websockets_websocket_to_pixel__WEBPACK_IMPORTED_MODULE_11__.openPixelWebSocket)(shutterbug, context),
        (0,_websockets_websocket_to_overlayer__WEBPACK_IMPORTED_MODULE_10__.openOverlayerWebSocket)(shutterbug, _custom_elements_shutterbug_pixels_shutterbug_pixels__WEBPACK_IMPORTED_MODULE_4__.ShutterbugPixels.overlay),
        openUIEventResult,
    ];
    const opened = webSocketLifeCycles.map((lifeCycle) => lifeCycle.opened);
    const closed = webSocketLifeCycles.map((lifeCycle) => lifeCycle.closed);
    const openWebSocketsResult = await Promise.any([
        Promise.all(opened).then(() => "success"),
        Promise.any(closed).then(() => "failure"),
    ]);
    if (openWebSocketsResult === "success") {
        _custom_elements_shutterbug_pixels_shutterbug_pixels__WEBPACK_IMPORTED_MODULE_4__.ShutterbugPixels.uiEvents = await openUIEventResult.opened;
        _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.receiveLifeCycle.send({ stage: "all WebSockets are open" });
    }
    const closeEvent = await Promise.any(closed);
    _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.receiveKaput.send(closeEvent);
    (0,_custom_elements_shutterbug_pixels_update_overlay__WEBPACK_IMPORTED_MODULE_3__.disableOverlay)(_custom_elements_shutterbug_pixels_shutterbug_pixels__WEBPACK_IMPORTED_MODULE_4__.ShutterbugPixels.overlay, closeEvent);
    _custom_elements_shutterbug_pixels_shutterbug_pixels__WEBPACK_IMPORTED_MODULE_4__.ShutterbugPixels.uiEvents?.removeAllListeners();
    (0,_websockets_opened_websockets__WEBPACK_IMPORTED_MODULE_7__.closeOpenedWebSockets)(1000, "client is kaput; closing all websockets");
    if (document.pointerLockElement !== null) {
        void document.exitPointerLock();
    }
}
//# sourceMappingURL=main.js.map
__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } }, 1);

/***/ }),

/***/ "./build/motif/motif.js":
/*!******************************!*\
  !*** ./build/motif/motif.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getMotif": () => (/* binding */ getMotif),
/* harmony export */   "initial": () => (/* binding */ initial)
/* harmony export */ });
/* harmony import */ var _schema_schema__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../schema/schema */ "./build/schema/schema.js");

const glow = "rgb(255, 210, 0)";
async function getMotif() {
    const response = await fetch(path, {
        method: "GET",
        headers: { Accept: "application/json" },
    });
    if (!response.ok) {
        return { result: "failure" };
    }
    const motif = _schema_schema__WEBPACK_IMPORTED_MODULE_0__.motif.safeParse(await response.json());
    if (!motif.success) {
        return { result: "failure" };
    }
    return { motif: motif.data, result: "success" };
}
const initial = {
    // The initial theme before the world's motif is known.
    motifName: "InitialMotif",
    accent: "deepskyblue",
    buttonBackground: "rgba(255, 255, 255, 0.85)",
    buttonOpaqueBackground: "rgb(255, 255, 255)",
    error: "red",
    primary: "darkorange",
    primaryGlow: glow,
    primaryLight: "orange",
    ready: "seagreen",
    textNormal: "black",
    textPassive: "gray",
    warning: "orange",
    boxShadow: "0px 0.1em 0.5em 1px DimGray",
    boxShadowGlow: `0px 0px 0.7em 0.2em ${glow}`,
    buttonHoverBackground: "rgba(255, 255, 255, 0.95)",
    disabled: "rgb(211, 211, 211)",
    inputBorderFocus: "rgb(30, 144, 255)",
    inputBorderPrimary: "gold",
    preBackground: "rgba(255, 248, 231, 0.85)",
    stitchingMajor: "orange",
    stitchingMinor1: "deepskyblue",
    stitchingMinor2: "gold",
    stitchingMinor3: "silver",
    buttonBorder: "0.4",
    buttonBorderRadius: "1.2em",
    buttonIconFontSize: "2em",
    buttonPadding: "0.6em",
};
const path = "/motif/typescript";
//# sourceMappingURL=motif.js.map

/***/ }),

/***/ "./build/schema/schema.js":
/*!********************************!*\
  !*** ./build/schema/schema.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cameraAssociations": () => (/* binding */ cameraAssociations),
/* harmony export */   "devoir": () => (/* binding */ devoir),
/* harmony export */   "motif": () => (/* binding */ motif),
/* harmony export */   "normalCloseReasons": () => (/* binding */ normalCloseReasons),
/* harmony export */   "overlayItemElementProps": () => (/* binding */ overlayItemElementProps),
/* harmony export */   "overlayItemUpdate": () => (/* binding */ overlayItemUpdate),
/* harmony export */   "overlayUpdate": () => (/* binding */ overlayUpdate),
/* harmony export */   "registeredAssociatedCameras": () => (/* binding */ registeredAssociatedCameras),
/* harmony export */   "shutterbug": () => (/* binding */ shutterbug),
/* harmony export */   "stitching": () => (/* binding */ stitching),
/* harmony export */   "uiCommand": () => (/* binding */ uiCommand)
/* harmony export */ });
/* harmony import */ var zod__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! zod */ "./node_modules/zod/lib/index.mjs");

const shutterbug = zod__WEBPACK_IMPORTED_MODULE_0__.z.string()
    .length(44)
    .regex(/^[\w-]{43}=$/);
const motif = zod__WEBPACK_IMPORTED_MODULE_0__.z.object({
    motifName: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    accent: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    boxShadow: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    buttonBackground: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    buttonOpaqueBackground: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    error: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    primary: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    primaryGlow: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    primaryLight: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    ready: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    textNormal: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    textPassive: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    warning: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    boxShadowGlow: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    buttonHoverBackground: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    disabled: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    inputBorderFocus: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    inputBorderPrimary: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    preBackground: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    stitchingMajor: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    stitchingMinor1: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    stitchingMinor2: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    stitchingMinor3: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    buttonBorder: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    buttonBorderRadius: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    buttonIconFontSize: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    buttonPadding: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
});
const devoir = zod__WEBPACK_IMPORTED_MODULE_0__.z.object({
    isPaused: zod__WEBPACK_IMPORTED_MODULE_0__.z.boolean(),
});
const uiCommand = zod__WEBPACK_IMPORTED_MODULE_0__.z.object({
    uiCommand: zod__WEBPACK_IMPORTED_MODULE_0__.z["enum"](["pause", "play", "shutdown"]),
});
const stitching = zod__WEBPACK_IMPORTED_MODULE_0__.z["enum"]([
    "uni",
    "bi",
    "biPartial",
    "tri",
    "triPartial",
    "quad",
    "quadPartial",
]);
const cameraAssociations = zod__WEBPACK_IMPORTED_MODULE_0__.z.object({
    cameras: zod__WEBPACK_IMPORTED_MODULE_0__.z.string().array(),
    stitching: stitching,
});
const registeredAssociatedCameras = zod__WEBPACK_IMPORTED_MODULE_0__.z.object({
    associated: zod__WEBPACK_IMPORTED_MODULE_0__.z.string().array(),
    registered: zod__WEBPACK_IMPORTED_MODULE_0__.z.string().array(),
    stitching: stitching,
});
const overlayItemElementProps = zod__WEBPACK_IMPORTED_MODULE_0__.z.object({
    innerHTML: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    innerHTMLHash: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    // Style properties.
    left: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    top: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    transform: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    transformOrigin: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
    zIndex: zod__WEBPACK_IMPORTED_MODULE_0__.z.number(),
});
const overlayItemUpdate = overlayItemElementProps.extend({
    name: zod__WEBPACK_IMPORTED_MODULE_0__.z.string(),
});
const overlayUpdate = zod__WEBPACK_IMPORTED_MODULE_0__.z.union([
    zod__WEBPACK_IMPORTED_MODULE_0__.z.object({ create: zod__WEBPACK_IMPORTED_MODULE_0__.z.string().array() }),
    zod__WEBPACK_IMPORTED_MODULE_0__.z.object({ delete: zod__WEBPACK_IMPORTED_MODULE_0__.z.string().array() }),
    zod__WEBPACK_IMPORTED_MODULE_0__.z.object({ hide: zod__WEBPACK_IMPORTED_MODULE_0__.z.string().array() }),
    zod__WEBPACK_IMPORTED_MODULE_0__.z.object({ update: zod__WEBPACK_IMPORTED_MODULE_0__.z.array(overlayItemUpdate) }),
]);
const normalCloseReasons = zod__WEBPACK_IMPORTED_MODULE_0__.z["enum"]([
    "received client request",
    "intercepted terminal interrupt",
]);
//# sourceMappingURL=schema.js.map

/***/ }),

/***/ "./build/websockets/opened-websockets.js":
/*!***********************************************!*\
  !*** ./build/websockets/opened-websockets.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "closeOpenedWebSockets": () => (/* binding */ closeOpenedWebSockets),
/* harmony export */   "openedWebSockets": () => (/* binding */ openedWebSockets),
/* harmony export */   "removeWebSocket": () => (/* binding */ removeWebSocket)
/* harmony export */ });
// Opened websockets are placed in this array. Closed websockets are removed
// from the array.
const openedWebSockets = [];
// Close any websockets remaining in the array of opened websockets.
function closeOpenedWebSockets(code, reason) {
    while (openedWebSockets.length > 0) {
        const ws = openedWebSockets.pop();
        if (ws !== undefined) {
            ws.close(code, reason);
        }
    }
}
// Remove a previously closed websocket from the websockets array.
function removeWebSocket(webSocket) {
    const i = openedWebSockets.indexOf(webSocket);
    if (i != -1) {
        openedWebSockets.splice(i, 1);
    }
}
//# sourceMappingURL=opened-websockets.js.map

/***/ }),

/***/ "./build/websockets/websocket-event.js":
/*!*********************************************!*\
  !*** ./build/websockets/websocket-event.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "newOnCloseCallback": () => (/* binding */ newOnCloseCallback)
/* harmony export */ });
/* harmony import */ var _opened_websockets__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./opened-websockets */ "./build/websockets/opened-websockets.js");

function newOnCloseCallback(ws, path, resolve) {
    return function (closeEvent) {
        (0,_opened_websockets__WEBPACK_IMPORTED_MODULE_0__.removeWebSocket)(ws);
        resolve({
            code: closeEvent.code,
            reason: closeEvent.reason,
            sourceWebSocket: path,
            wasClean: closeEvent.wasClean,
        });
    };
}
//# sourceMappingURL=websocket-event.js.map

/***/ }),

/***/ "./build/websockets/websocket-to-camera.js":
/*!*************************************************!*\
  !*** ./build/websockets/websocket-to-camera.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "openCameraWebSocket": () => (/* binding */ openCameraWebSocket)
/* harmony export */ });
/* harmony import */ var _opened_websockets__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./opened-websockets */ "./build/websockets/opened-websockets.js");
/* harmony import */ var _init_init_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../init/init.js */ "./build/init/init.js");
/* harmony import */ var _schema_schema_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../schema/schema.js */ "./build/schema/schema.js");
/* harmony import */ var _websocket_event__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./websocket-event */ "./build/websockets/websocket-event.js");




// Open a camera websocket on the Mechane server.
//
// A Mechane server needs to be able to send 'registered and associated cameras'
// messages to the Elm integrant.
//
// The Elm integrant needs to be able to send 'camera associations' messages to
// the Mechane server.
function openCameraWebSocket(shutterbug) {
    const cameraPath = "/camera";
    const ws = new WebSocket("ws://" + window.location.host + cameraPath);
    const closePromise = new Promise((resolve) => {
        ws.onclose = (0,_websocket_event__WEBPACK_IMPORTED_MODULE_3__.newOnCloseCallback)(ws, cameraPath, resolve);
    });
    const openPromise = new Promise(function (resolve) {
        ws.onopen = function () {
            ws.send(JSON.stringify(shutterbug));
            _opened_websockets__WEBPACK_IMPORTED_MODULE_0__.openedWebSockets.push(ws);
            // Receive 'camera associations' messages from the Elm integrant.
            _init_init_js__WEBPACK_IMPORTED_MODULE_1__.ports.sendCameraAssociations.subscribe(function (message) {
                const result = _schema_schema_js__WEBPACK_IMPORTED_MODULE_2__.cameraAssociations.safeParse(message);
                if (!result.success) {
                    console.log(result.error.issues);
                }
                // Forward the 'camera associations' message to the Mechane
                // server.
                ws.send(JSON.stringify(message));
            });
            resolve();
        };
    });
    // Receive 'registered and associated cameras' messages from the Mechane
    // server.
    ws.onmessage = function (message) {
        if (typeof message.data !== "string") {
            return;
        }
        const data = JSON.parse(message.data);
        const result = _schema_schema_js__WEBPACK_IMPORTED_MODULE_2__.registeredAssociatedCameras.safeParse(data);
        if (!result.success) {
            console.log(result.error.issues);
        }
        // Forward the 'registered and associated cameras' message to the Elm
        // integrant.
        //
        // @ts-expect-error: forward good and bad input alike.
        _init_init_js__WEBPACK_IMPORTED_MODULE_1__.ports.receiveRegisteredAssociatedCameras.send(data);
    };
    return {
        closed: closePromise,
        opened: openPromise,
    };
}
//# sourceMappingURL=websocket-to-camera.js.map

/***/ }),

/***/ "./build/websockets/websocket-to-devoir.js":
/*!*************************************************!*\
  !*** ./build/websockets/websocket-to-devoir.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "openDevoirWebSocket": () => (/* binding */ openDevoirWebSocket)
/* harmony export */ });
/* harmony import */ var _opened_websockets__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./opened-websockets */ "./build/websockets/opened-websockets.js");
/* harmony import */ var _init_init_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../init/init.js */ "./build/init/init.js");
/* harmony import */ var _schema_schema_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../schema/schema.js */ "./build/schema/schema.js");
/* harmony import */ var _websocket_event__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./websocket-event */ "./build/websockets/websocket-event.js");




// Open a devoir websocket on the Mechane server.
//
// A Mechane server needs to be able to send 'devoir' messages to the Elm
// integrant.
//
// The Elm integrant needs to be able to send 'UI command' messages that may
// alter the devoir state to the Mechane server.
function openDevoirWebSocket(shutterbug) {
    const devoirPath = "/devoir";
    const ws = new WebSocket("ws://" + window.location.host + devoirPath);
    const closePromise = new Promise((resolve) => {
        ws.onclose = (0,_websocket_event__WEBPACK_IMPORTED_MODULE_3__.newOnCloseCallback)(ws, devoirPath, resolve);
    });
    const openPromise = new Promise(function (resolve) {
        ws.onopen = function () {
            ws.send(JSON.stringify(shutterbug));
            _opened_websockets__WEBPACK_IMPORTED_MODULE_0__.openedWebSockets.push(ws);
            // Receive 'UI command' messages from the Elm integrant.
            _init_init_js__WEBPACK_IMPORTED_MODULE_1__.ports.sendUICommand.subscribe(function (message) {
                const result = _schema_schema_js__WEBPACK_IMPORTED_MODULE_2__.uiCommand.safeParse(message);
                if (!result.success) {
                    console.log(result.error.issues);
                }
                // Forward the 'UI command' message to the Mechane server.
                ws.send(JSON.stringify(message));
            });
            resolve();
        };
    });
    // Receive 'devoir' messages from the Mechane server.
    ws.onmessage = function (message) {
        if (typeof message.data !== "string") {
            return;
        }
        const data = JSON.parse(message.data);
        const result = _schema_schema_js__WEBPACK_IMPORTED_MODULE_2__.devoir.safeParse(data);
        if (!result.success) {
            console.log(result.error.issues);
        }
        // Forward the 'devoir' message to the Elm integrant.
        //
        // @ts-expect-error: forward good and bad input alike.
        _init_init_js__WEBPACK_IMPORTED_MODULE_1__.ports.receiveDevoir.send(data);
    };
    return {
        closed: closePromise,
        opened: openPromise,
    };
}
//# sourceMappingURL=websocket-to-devoir.js.map

/***/ }),

/***/ "./build/websockets/websocket-to-overlayer.js":
/*!****************************************************!*\
  !*** ./build/websockets/websocket-to-overlayer.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "openOverlayerWebSocket": () => (/* binding */ openOverlayerWebSocket)
/* harmony export */ });
/* harmony import */ var _opened_websockets__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./opened-websockets */ "./build/websockets/opened-websockets.js");
/* harmony import */ var _schema_schema_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../schema/schema.js */ "./build/schema/schema.js");
/* harmony import */ var _custom_elements_shutterbug_pixels_update_overlay__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../custom-elements/shutterbug-pixels/update-overlay */ "./build/custom-elements/shutterbug-pixels/update-overlay.js");
/* harmony import */ var _websocket_event__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./websocket-event */ "./build/websockets/websocket-event.js");




// Open an overlayer websocket on the Mechane server.
//
// A Mechane server needs to be able to send 'overlay update' messages to the
// Typescript integrant.
function openOverlayerWebSocket(shutterbug, overlay) {
    const overlayerPath = "/overlayer";
    const ws = new WebSocket("ws://" + window.location.host + overlayerPath);
    const closePromise = new Promise((resolve) => {
        ws.onclose = (0,_websocket_event__WEBPACK_IMPORTED_MODULE_3__.newOnCloseCallback)(ws, overlayerPath, resolve);
    });
    const openPromise = new Promise(function (resolve) {
        ws.onopen = function () {
            ws.send(JSON.stringify(shutterbug));
            _opened_websockets__WEBPACK_IMPORTED_MODULE_0__.openedWebSockets.push(ws);
            resolve();
        };
    });
    // Receive 'overlay update' messages from the Mechane server.
    ws.onmessage = function (message) {
        if (typeof message.data !== "string") {
            return;
        }
        const data = JSON.parse(message.data);
        const result = _schema_schema_js__WEBPACK_IMPORTED_MODULE_1__.overlayUpdate.safeParse(data);
        if (!result.success) {
            console.log(result.error.issues);
            return;
        }
        (0,_custom_elements_shutterbug_pixels_update_overlay__WEBPACK_IMPORTED_MODULE_2__.updateOverlay)(overlay, result.data);
    };
    return {
        closed: closePromise,
        opened: openPromise,
    };
}
//# sourceMappingURL=websocket-to-overlayer.js.map

/***/ }),

/***/ "./build/websockets/websocket-to-pixel.js":
/*!************************************************!*\
  !*** ./build/websockets/websocket-to-pixel.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "openPixelWebSocket": () => (/* binding */ openPixelWebSocket)
/* harmony export */ });
/* harmony import */ var _opened_websockets__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./opened-websockets */ "./build/websockets/opened-websockets.js");
/* harmony import */ var _init_init__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../init/init */ "./build/init/init.js");
/* harmony import */ var _custom_elements_shutterbug_pixels_update_canvas__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../custom-elements/shutterbug-pixels/update-canvas */ "./build/custom-elements/shutterbug-pixels/update-canvas.js");
/* harmony import */ var _websocket_event__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./websocket-event */ "./build/websockets/websocket-event.js");




// Open a pixel websocket on the Mechane server.
//
// A Mechane server needs to be able to send 'shutterbug pixels' messages to the
// Typescript integrant.
function openPixelWebSocket(shutterbug, context) {
    const pixelPath = "/pixel";
    const ws = new WebSocket("ws://" + window.location.host + pixelPath);
    ws.binaryType = "arraybuffer";
    const closePromise = new Promise((resolve) => {
        ws.onclose = (0,_websocket_event__WEBPACK_IMPORTED_MODULE_3__.newOnCloseCallback)(ws, pixelPath, resolve);
    });
    const openPromise = new Promise(function (resolve) {
        ws.onopen = function () {
            ws.send(JSON.stringify(shutterbug));
            _opened_websockets__WEBPACK_IMPORTED_MODULE_0__.openedWebSockets.push(ws);
            resolve();
        };
    });
    function pixelsMeta() {
        return {
            w: context.canvas.width,
            h: context.canvas.height,
            fullscreen: document.fullscreenElement !== null,
            pointerLock: document.pointerLockElement !== null,
        };
    }
    // Receive 'shutterbug pixels' messages from the Mechane server.
    ws.onmessage = function (message) {
        if (!(message.data instanceof ArrayBuffer)) {
            return;
        }
        const w = context.canvas.width;
        const h = context.canvas.height;
        (0,_custom_elements_shutterbug_pixels_update_canvas__WEBPACK_IMPORTED_MODULE_2__.updateCanvas)(context, message.data);
        if (context.canvas.width != w || context.canvas.height != h) {
            // Let the Elm integrant know the new dimensions of the image.
            _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.receivePixelsMeta.send(pixelsMeta());
        }
    };
    document.addEventListener("fullscreenchange", function () {
        // Let the Elm integrant know the fullscreen status.
        _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.receivePixelsMeta.send(pixelsMeta());
    });
    document.addEventListener("pointerlockchange", function () {
        // Let the Elm integrant know the pointerlock status.
        _init_init__WEBPACK_IMPORTED_MODULE_1__.ports.receivePixelsMeta.send(pixelsMeta());
    });
    return {
        closed: closePromise,
        opened: openPromise,
    };
}
//# sourceMappingURL=websocket-to-pixel.js.map

/***/ }),

/***/ "./build/websockets/websocket-to-uievent.js":
/*!**************************************************!*\
  !*** ./build/websockets/websocket-to-uievent.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "openUIEventWebSocket": () => (/* binding */ openUIEventWebSocket)
/* harmony export */ });
/* harmony import */ var _custom_elements_shutterbug_pixels_listen__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../custom-elements/shutterbug-pixels/listen */ "./build/custom-elements/shutterbug-pixels/listen.js");
/* harmony import */ var _opened_websockets__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./opened-websockets */ "./build/websockets/opened-websockets.js");
/* harmony import */ var _websocket_event__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./websocket-event */ "./build/websockets/websocket-event.js");



/*

Handling of UI events in the browser based UI is split between the Elm and
Typescript integrants.

Typescript handles the ShutterbugPixels' events such as keyboard controlled
(WASD style) camera movements, mouse camera panning, overlay button clicks and
so on. These events are sent straight to the Mechane server bypassing the Elm
integrant.

Elm handles the Devoir buttons and settings for the UI. Some of these UI
commands may be forwarded to the Mechane Server via the Typescript integrant.

*/
// Open a uievent websocket on the Mechane server.
//
// The Typescript integrant needs to be able to send 'UI event' messages to the
// Mechane server.
//
// Fill out the listeners object.
function openUIEventWebSocket(shutterbug, mainContainer, context, shadowRoot) {
    const uieventPath = "/uievent";
    const ws = new WebSocket("ws://" + window.location.host + uieventPath);
    const closePromise = new Promise((resolve) => {
        ws.onclose = (0,_websocket_event__WEBPACK_IMPORTED_MODULE_2__.newOnCloseCallback)(ws, uieventPath, resolve);
    });
    const openPromise = new Promise(function (resolve) {
        ws.onopen = function () {
            ws.send(JSON.stringify(shutterbug));
            _opened_websockets__WEBPACK_IMPORTED_MODULE_1__.openedWebSockets.push(ws);
            // Add UI event listeners to the aspects of the UI handled by the
            // Typescript integrant. These events are forwarded to the Mechane
            // server.
            const listenersController = (0,_custom_elements_shutterbug_pixels_listen__WEBPACK_IMPORTED_MODULE_0__.addListeners)(context, mainContainer, function (event) {
                ws.send(JSON.stringify(event));
            }, shadowRoot);
            resolve(listenersController);
        };
    });
    return {
        closed: closePromise,
        opened: openPromise,
    };
}
//# sourceMappingURL=websocket-to-uievent.js.map

/***/ }),

/***/ "./node_modules/path-data-parser/lib/absolutize.js":
/*!*********************************************************!*\
  !*** ./node_modules/path-data-parser/lib/absolutize.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "absolutize": () => (/* binding */ absolutize)
/* harmony export */ });
// Translate relative commands to absolute commands
function absolutize(segments) {
    let cx = 0, cy = 0;
    let subx = 0, suby = 0;
    const out = [];
    for (const { key, data } of segments) {
        switch (key) {
            case 'M':
                out.push({ key: 'M', data: [...data] });
                [cx, cy] = data;
                [subx, suby] = data;
                break;
            case 'm':
                cx += data[0];
                cy += data[1];
                out.push({ key: 'M', data: [cx, cy] });
                subx = cx;
                suby = cy;
                break;
            case 'L':
                out.push({ key: 'L', data: [...data] });
                [cx, cy] = data;
                break;
            case 'l':
                cx += data[0];
                cy += data[1];
                out.push({ key: 'L', data: [cx, cy] });
                break;
            case 'C':
                out.push({ key: 'C', data: [...data] });
                cx = data[4];
                cy = data[5];
                break;
            case 'c': {
                const newdata = data.map((d, i) => (i % 2) ? (d + cy) : (d + cx));
                out.push({ key: 'C', data: newdata });
                cx = newdata[4];
                cy = newdata[5];
                break;
            }
            case 'Q':
                out.push({ key: 'Q', data: [...data] });
                cx = data[2];
                cy = data[3];
                break;
            case 'q': {
                const newdata = data.map((d, i) => (i % 2) ? (d + cy) : (d + cx));
                out.push({ key: 'Q', data: newdata });
                cx = newdata[2];
                cy = newdata[3];
                break;
            }
            case 'A':
                out.push({ key: 'A', data: [...data] });
                cx = data[5];
                cy = data[6];
                break;
            case 'a':
                cx += data[5];
                cy += data[6];
                out.push({ key: 'A', data: [data[0], data[1], data[2], data[3], data[4], cx, cy] });
                break;
            case 'H':
                out.push({ key: 'H', data: [...data] });
                cx = data[0];
                break;
            case 'h':
                cx += data[0];
                out.push({ key: 'H', data: [cx] });
                break;
            case 'V':
                out.push({ key: 'V', data: [...data] });
                cy = data[0];
                break;
            case 'v':
                cy += data[0];
                out.push({ key: 'V', data: [cy] });
                break;
            case 'S':
                out.push({ key: 'S', data: [...data] });
                cx = data[2];
                cy = data[3];
                break;
            case 's': {
                const newdata = data.map((d, i) => (i % 2) ? (d + cy) : (d + cx));
                out.push({ key: 'S', data: newdata });
                cx = newdata[2];
                cy = newdata[3];
                break;
            }
            case 'T':
                out.push({ key: 'T', data: [...data] });
                cx = data[0];
                cy = data[1];
                break;
            case 't':
                cx += data[0];
                cy += data[1];
                out.push({ key: 'T', data: [cx, cy] });
                break;
            case 'Z':
            case 'z':
                out.push({ key: 'Z', data: [] });
                cx = subx;
                cy = suby;
                break;
        }
    }
    return out;
}


/***/ }),

/***/ "./node_modules/path-data-parser/lib/index.js":
/*!****************************************************!*\
  !*** ./node_modules/path-data-parser/lib/index.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "absolutize": () => (/* reexport safe */ _absolutize_js__WEBPACK_IMPORTED_MODULE_1__.absolutize),
/* harmony export */   "normalize": () => (/* reexport safe */ _normalize_js__WEBPACK_IMPORTED_MODULE_2__.normalize),
/* harmony export */   "parsePath": () => (/* reexport safe */ _parser_js__WEBPACK_IMPORTED_MODULE_0__.parsePath),
/* harmony export */   "serialize": () => (/* reexport safe */ _parser_js__WEBPACK_IMPORTED_MODULE_0__.serialize)
/* harmony export */ });
/* harmony import */ var _parser_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./parser.js */ "./node_modules/path-data-parser/lib/parser.js");
/* harmony import */ var _absolutize_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./absolutize.js */ "./node_modules/path-data-parser/lib/absolutize.js");
/* harmony import */ var _normalize_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./normalize.js */ "./node_modules/path-data-parser/lib/normalize.js");





/***/ }),

/***/ "./node_modules/path-data-parser/lib/normalize.js":
/*!********************************************************!*\
  !*** ./node_modules/path-data-parser/lib/normalize.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "normalize": () => (/* binding */ normalize)
/* harmony export */ });
// Normalize path to include only M, L, C, and Z commands
function normalize(segments) {
    const out = [];
    let lastType = '';
    let cx = 0, cy = 0;
    let subx = 0, suby = 0;
    let lcx = 0, lcy = 0;
    for (const { key, data } of segments) {
        switch (key) {
            case 'M':
                out.push({ key: 'M', data: [...data] });
                [cx, cy] = data;
                [subx, suby] = data;
                break;
            case 'C':
                out.push({ key: 'C', data: [...data] });
                cx = data[4];
                cy = data[5];
                lcx = data[2];
                lcy = data[3];
                break;
            case 'L':
                out.push({ key: 'L', data: [...data] });
                [cx, cy] = data;
                break;
            case 'H':
                cx = data[0];
                out.push({ key: 'L', data: [cx, cy] });
                break;
            case 'V':
                cy = data[0];
                out.push({ key: 'L', data: [cx, cy] });
                break;
            case 'S': {
                let cx1 = 0, cy1 = 0;
                if (lastType === 'C' || lastType === 'S') {
                    cx1 = cx + (cx - lcx);
                    cy1 = cy + (cy - lcy);
                }
                else {
                    cx1 = cx;
                    cy1 = cy;
                }
                out.push({ key: 'C', data: [cx1, cy1, ...data] });
                lcx = data[0];
                lcy = data[1];
                cx = data[2];
                cy = data[3];
                break;
            }
            case 'T': {
                const [x, y] = data;
                let x1 = 0, y1 = 0;
                if (lastType === 'Q' || lastType === 'T') {
                    x1 = cx + (cx - lcx);
                    y1 = cy + (cy - lcy);
                }
                else {
                    x1 = cx;
                    y1 = cy;
                }
                const cx1 = cx + 2 * (x1 - cx) / 3;
                const cy1 = cy + 2 * (y1 - cy) / 3;
                const cx2 = x + 2 * (x1 - x) / 3;
                const cy2 = y + 2 * (y1 - y) / 3;
                out.push({ key: 'C', data: [cx1, cy1, cx2, cy2, x, y] });
                lcx = x1;
                lcy = y1;
                cx = x;
                cy = y;
                break;
            }
            case 'Q': {
                const [x1, y1, x, y] = data;
                const cx1 = cx + 2 * (x1 - cx) / 3;
                const cy1 = cy + 2 * (y1 - cy) / 3;
                const cx2 = x + 2 * (x1 - x) / 3;
                const cy2 = y + 2 * (y1 - y) / 3;
                out.push({ key: 'C', data: [cx1, cy1, cx2, cy2, x, y] });
                lcx = x1;
                lcy = y1;
                cx = x;
                cy = y;
                break;
            }
            case 'A': {
                const r1 = Math.abs(data[0]);
                const r2 = Math.abs(data[1]);
                const angle = data[2];
                const largeArcFlag = data[3];
                const sweepFlag = data[4];
                const x = data[5];
                const y = data[6];
                if (r1 === 0 || r2 === 0) {
                    out.push({ key: 'C', data: [cx, cy, x, y, x, y] });
                    cx = x;
                    cy = y;
                }
                else {
                    if (cx !== x || cy !== y) {
                        const curves = arcToCubicCurves(cx, cy, x, y, r1, r2, angle, largeArcFlag, sweepFlag);
                        curves.forEach(function (curve) {
                            out.push({ key: 'C', data: curve });
                        });
                        cx = x;
                        cy = y;
                    }
                }
                break;
            }
            case 'Z':
                out.push({ key: 'Z', data: [] });
                cx = subx;
                cy = suby;
                break;
        }
        lastType = key;
    }
    return out;
}
function degToRad(degrees) {
    return (Math.PI * degrees) / 180;
}
function rotate(x, y, angleRad) {
    const X = x * Math.cos(angleRad) - y * Math.sin(angleRad);
    const Y = x * Math.sin(angleRad) + y * Math.cos(angleRad);
    return [X, Y];
}
function arcToCubicCurves(x1, y1, x2, y2, r1, r2, angle, largeArcFlag, sweepFlag, recursive) {
    const angleRad = degToRad(angle);
    let params = [];
    let f1 = 0, f2 = 0, cx = 0, cy = 0;
    if (recursive) {
        [f1, f2, cx, cy] = recursive;
    }
    else {
        [x1, y1] = rotate(x1, y1, -angleRad);
        [x2, y2] = rotate(x2, y2, -angleRad);
        const x = (x1 - x2) / 2;
        const y = (y1 - y2) / 2;
        let h = (x * x) / (r1 * r1) + (y * y) / (r2 * r2);
        if (h > 1) {
            h = Math.sqrt(h);
            r1 = h * r1;
            r2 = h * r2;
        }
        const sign = (largeArcFlag === sweepFlag) ? -1 : 1;
        const r1Pow = r1 * r1;
        const r2Pow = r2 * r2;
        const left = r1Pow * r2Pow - r1Pow * y * y - r2Pow * x * x;
        const right = r1Pow * y * y + r2Pow * x * x;
        const k = sign * Math.sqrt(Math.abs(left / right));
        cx = k * r1 * y / r2 + (x1 + x2) / 2;
        cy = k * -r2 * x / r1 + (y1 + y2) / 2;
        f1 = Math.asin(parseFloat(((y1 - cy) / r2).toFixed(9)));
        f2 = Math.asin(parseFloat(((y2 - cy) / r2).toFixed(9)));
        if (x1 < cx) {
            f1 = Math.PI - f1;
        }
        if (x2 < cx) {
            f2 = Math.PI - f2;
        }
        if (f1 < 0) {
            f1 = Math.PI * 2 + f1;
        }
        if (f2 < 0) {
            f2 = Math.PI * 2 + f2;
        }
        if (sweepFlag && f1 > f2) {
            f1 = f1 - Math.PI * 2;
        }
        if (!sweepFlag && f2 > f1) {
            f2 = f2 - Math.PI * 2;
        }
    }
    let df = f2 - f1;
    if (Math.abs(df) > (Math.PI * 120 / 180)) {
        const f2old = f2;
        const x2old = x2;
        const y2old = y2;
        if (sweepFlag && f2 > f1) {
            f2 = f1 + (Math.PI * 120 / 180) * (1);
        }
        else {
            f2 = f1 + (Math.PI * 120 / 180) * (-1);
        }
        x2 = cx + r1 * Math.cos(f2);
        y2 = cy + r2 * Math.sin(f2);
        params = arcToCubicCurves(x2, y2, x2old, y2old, r1, r2, angle, 0, sweepFlag, [f2, f2old, cx, cy]);
    }
    df = f2 - f1;
    const c1 = Math.cos(f1);
    const s1 = Math.sin(f1);
    const c2 = Math.cos(f2);
    const s2 = Math.sin(f2);
    const t = Math.tan(df / 4);
    const hx = 4 / 3 * r1 * t;
    const hy = 4 / 3 * r2 * t;
    const m1 = [x1, y1];
    const m2 = [x1 + hx * s1, y1 - hy * c1];
    const m3 = [x2 + hx * s2, y2 - hy * c2];
    const m4 = [x2, y2];
    m2[0] = 2 * m1[0] - m2[0];
    m2[1] = 2 * m1[1] - m2[1];
    if (recursive) {
        return [m2, m3, m4].concat(params);
    }
    else {
        params = [m2, m3, m4].concat(params);
        const curves = [];
        for (let i = 0; i < params.length; i += 3) {
            const r1 = rotate(params[i][0], params[i][1], angleRad);
            const r2 = rotate(params[i + 1][0], params[i + 1][1], angleRad);
            const r3 = rotate(params[i + 2][0], params[i + 2][1], angleRad);
            curves.push([r1[0], r1[1], r2[0], r2[1], r3[0], r3[1]]);
        }
        return curves;
    }
}


/***/ }),

/***/ "./node_modules/path-data-parser/lib/parser.js":
/*!*****************************************************!*\
  !*** ./node_modules/path-data-parser/lib/parser.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "parsePath": () => (/* binding */ parsePath),
/* harmony export */   "serialize": () => (/* binding */ serialize)
/* harmony export */ });
const COMMAND = 0;
const NUMBER = 1;
const EOD = 2;
const PARAMS = { A: 7, a: 7, C: 6, c: 6, H: 1, h: 1, L: 2, l: 2, M: 2, m: 2, Q: 4, q: 4, S: 4, s: 4, T: 2, t: 2, V: 1, v: 1, Z: 0, z: 0 };
function tokenize(d) {
    const tokens = new Array();
    while (d !== '') {
        if (d.match(/^([ \t\r\n,]+)/)) {
            d = d.substr(RegExp.$1.length);
        }
        else if (d.match(/^([aAcChHlLmMqQsStTvVzZ])/)) {
            tokens[tokens.length] = { type: COMMAND, text: RegExp.$1 };
            d = d.substr(RegExp.$1.length);
        }
        else if (d.match(/^(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)/)) {
            tokens[tokens.length] = { type: NUMBER, text: `${parseFloat(RegExp.$1)}` };
            d = d.substr(RegExp.$1.length);
        }
        else {
            return [];
        }
    }
    tokens[tokens.length] = { type: EOD, text: '' };
    return tokens;
}
function isType(token, type) {
    return token.type === type;
}
function parsePath(d) {
    const segments = [];
    const tokens = tokenize(d);
    let mode = 'BOD';
    let index = 0;
    let token = tokens[index];
    while (!isType(token, EOD)) {
        let paramsCount = 0;
        const params = [];
        if (mode === 'BOD') {
            if (token.text === 'M' || token.text === 'm') {
                index++;
                paramsCount = PARAMS[token.text];
                mode = token.text;
            }
            else {
                return parsePath('M0,0' + d);
            }
        }
        else if (isType(token, NUMBER)) {
            paramsCount = PARAMS[mode];
        }
        else {
            index++;
            paramsCount = PARAMS[token.text];
            mode = token.text;
        }
        if ((index + paramsCount) < tokens.length) {
            for (let i = index; i < index + paramsCount; i++) {
                const numbeToken = tokens[i];
                if (isType(numbeToken, NUMBER)) {
                    params[params.length] = +numbeToken.text;
                }
                else {
                    throw new Error('Param not a number: ' + mode + ',' + numbeToken.text);
                }
            }
            if (typeof PARAMS[mode] === 'number') {
                const segment = { key: mode, data: params };
                segments.push(segment);
                index += paramsCount;
                token = tokens[index];
                if (mode === 'M')
                    mode = 'L';
                if (mode === 'm')
                    mode = 'l';
            }
            else {
                throw new Error('Bad segment: ' + mode);
            }
        }
        else {
            throw new Error('Path data ended short');
        }
    }
    return segments;
}
function serialize(segments) {
    const tokens = [];
    for (const { key, data } of segments) {
        tokens.push(key);
        switch (key) {
            case 'C':
            case 'c':
                tokens.push(data[0], `${data[1]},`, data[2], `${data[3]},`, data[4], data[5]);
                break;
            case 'S':
            case 's':
            case 'Q':
            case 'q':
                tokens.push(data[0], `${data[1]},`, data[2], data[3]);
                break;
            default:
                tokens.push(...data);
                break;
        }
    }
    return tokens.join(' ');
}


/***/ }),

/***/ "./node_modules/points-on-curve/lib/curve-to-bezier.js":
/*!*************************************************************!*\
  !*** ./node_modules/points-on-curve/lib/curve-to-bezier.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "curveToBezier": () => (/* binding */ curveToBezier)
/* harmony export */ });
function clone(p) {
    return [...p];
}
function curveToBezier(pointsIn, curveTightness = 0) {
    const len = pointsIn.length;
    if (len < 3) {
        throw new Error('A curve must have at least three points.');
    }
    const out = [];
    if (len === 3) {
        out.push(clone(pointsIn[0]), clone(pointsIn[1]), clone(pointsIn[2]), clone(pointsIn[2]));
    }
    else {
        const points = [];
        points.push(pointsIn[0], pointsIn[0]);
        for (let i = 1; i < pointsIn.length; i++) {
            points.push(pointsIn[i]);
            if (i === (pointsIn.length - 1)) {
                points.push(pointsIn[i]);
            }
        }
        const b = [];
        const s = 1 - curveTightness;
        out.push(clone(points[0]));
        for (let i = 1; (i + 2) < points.length; i++) {
            const cachedVertArray = points[i];
            b[0] = [cachedVertArray[0], cachedVertArray[1]];
            b[1] = [cachedVertArray[0] + (s * points[i + 1][0] - s * points[i - 1][0]) / 6, cachedVertArray[1] + (s * points[i + 1][1] - s * points[i - 1][1]) / 6];
            b[2] = [points[i + 1][0] + (s * points[i][0] - s * points[i + 2][0]) / 6, points[i + 1][1] + (s * points[i][1] - s * points[i + 2][1]) / 6];
            b[3] = [points[i + 1][0], points[i + 1][1]];
            out.push(b[1], b[2], b[3]);
        }
    }
    return out;
}


/***/ }),

/***/ "./node_modules/points-on-curve/lib/index.js":
/*!***************************************************!*\
  !*** ./node_modules/points-on-curve/lib/index.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "pointsOnBezierCurves": () => (/* binding */ pointsOnBezierCurves),
/* harmony export */   "simplify": () => (/* binding */ simplify)
/* harmony export */ });
// distance between 2 points
function distance(p1, p2) {
    return Math.sqrt(distanceSq(p1, p2));
}
// distance between 2 points squared
function distanceSq(p1, p2) {
    return Math.pow(p1[0] - p2[0], 2) + Math.pow(p1[1] - p2[1], 2);
}
// Sistance squared from a point p to the line segment vw
function distanceToSegmentSq(p, v, w) {
    const l2 = distanceSq(v, w);
    if (l2 === 0) {
        return distanceSq(p, v);
    }
    let t = ((p[0] - v[0]) * (w[0] - v[0]) + (p[1] - v[1]) * (w[1] - v[1])) / l2;
    t = Math.max(0, Math.min(1, t));
    return distanceSq(p, lerp(v, w, t));
}
function lerp(a, b, t) {
    return [
        a[0] + (b[0] - a[0]) * t,
        a[1] + (b[1] - a[1]) * t,
    ];
}
// Adapted from https://seant23.wordpress.com/2010/11/12/offset-bezier-curves/
function flatness(points, offset) {
    const p1 = points[offset + 0];
    const p2 = points[offset + 1];
    const p3 = points[offset + 2];
    const p4 = points[offset + 3];
    let ux = 3 * p2[0] - 2 * p1[0] - p4[0];
    ux *= ux;
    let uy = 3 * p2[1] - 2 * p1[1] - p4[1];
    uy *= uy;
    let vx = 3 * p3[0] - 2 * p4[0] - p1[0];
    vx *= vx;
    let vy = 3 * p3[1] - 2 * p4[1] - p1[1];
    vy *= vy;
    if (ux < vx) {
        ux = vx;
    }
    if (uy < vy) {
        uy = vy;
    }
    return ux + uy;
}
function getPointsOnBezierCurveWithSplitting(points, offset, tolerance, newPoints) {
    const outPoints = newPoints || [];
    if (flatness(points, offset) < tolerance) {
        const p0 = points[offset + 0];
        if (outPoints.length) {
            const d = distance(outPoints[outPoints.length - 1], p0);
            if (d > 1) {
                outPoints.push(p0);
            }
        }
        else {
            outPoints.push(p0);
        }
        outPoints.push(points[offset + 3]);
    }
    else {
        // subdivide
        const t = .5;
        const p1 = points[offset + 0];
        const p2 = points[offset + 1];
        const p3 = points[offset + 2];
        const p4 = points[offset + 3];
        const q1 = lerp(p1, p2, t);
        const q2 = lerp(p2, p3, t);
        const q3 = lerp(p3, p4, t);
        const r1 = lerp(q1, q2, t);
        const r2 = lerp(q2, q3, t);
        const red = lerp(r1, r2, t);
        getPointsOnBezierCurveWithSplitting([p1, q1, r1, red], 0, tolerance, outPoints);
        getPointsOnBezierCurveWithSplitting([red, r2, q3, p4], 0, tolerance, outPoints);
    }
    return outPoints;
}
function simplify(points, distance) {
    return simplifyPoints(points, 0, points.length, distance);
}
// Ramer–Douglas–Peucker algorithm
// https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm
function simplifyPoints(points, start, end, epsilon, newPoints) {
    const outPoints = newPoints || [];
    // find the most distance point from the endpoints
    const s = points[start];
    const e = points[end - 1];
    let maxDistSq = 0;
    let maxNdx = 1;
    for (let i = start + 1; i < end - 1; ++i) {
        const distSq = distanceToSegmentSq(points[i], s, e);
        if (distSq > maxDistSq) {
            maxDistSq = distSq;
            maxNdx = i;
        }
    }
    // if that point is too far, split
    if (Math.sqrt(maxDistSq) > epsilon) {
        simplifyPoints(points, start, maxNdx + 1, epsilon, outPoints);
        simplifyPoints(points, maxNdx, end, epsilon, outPoints);
    }
    else {
        if (!outPoints.length) {
            outPoints.push(s);
        }
        outPoints.push(e);
    }
    return outPoints;
}
function pointsOnBezierCurves(points, tolerance = 0.15, distance) {
    const newPoints = [];
    const numSegments = (points.length - 1) / 3;
    for (let i = 0; i < numSegments; i++) {
        const offset = i * 3;
        getPointsOnBezierCurveWithSplitting(points, offset, tolerance, newPoints);
    }
    if (distance && distance > 0) {
        return simplifyPoints(newPoints, 0, newPoints.length, distance);
    }
    return newPoints;
}


/***/ }),

/***/ "./node_modules/points-on-path/lib/index.js":
/*!**************************************************!*\
  !*** ./node_modules/points-on-path/lib/index.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "pointsOnPath": () => (/* binding */ pointsOnPath)
/* harmony export */ });
/* harmony import */ var points_on_curve__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! points-on-curve */ "./node_modules/points-on-curve/lib/index.js");
/* harmony import */ var path_data_parser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! path-data-parser */ "./node_modules/path-data-parser/lib/index.js");


function pointsOnPath(path, tolerance, distance) {
    const segments = (0,path_data_parser__WEBPACK_IMPORTED_MODULE_1__.parsePath)(path);
    const normalized = (0,path_data_parser__WEBPACK_IMPORTED_MODULE_1__.normalize)((0,path_data_parser__WEBPACK_IMPORTED_MODULE_1__.absolutize)(segments));
    const sets = [];
    let currentPoints = [];
    let start = [0, 0];
    let pendingCurve = [];
    const appendPendingCurve = () => {
        if (pendingCurve.length >= 4) {
            currentPoints.push(...(0,points_on_curve__WEBPACK_IMPORTED_MODULE_0__.pointsOnBezierCurves)(pendingCurve, tolerance));
        }
        pendingCurve = [];
    };
    const appendPendingPoints = () => {
        appendPendingCurve();
        if (currentPoints.length) {
            sets.push(currentPoints);
            currentPoints = [];
        }
    };
    for (const { key, data } of normalized) {
        switch (key) {
            case 'M':
                appendPendingPoints();
                start = [data[0], data[1]];
                currentPoints.push(start);
                break;
            case 'L':
                appendPendingCurve();
                currentPoints.push([data[0], data[1]]);
                break;
            case 'C':
                if (!pendingCurve.length) {
                    const lastPoint = currentPoints.length ? currentPoints[currentPoints.length - 1] : start;
                    pendingCurve.push([lastPoint[0], lastPoint[1]]);
                }
                pendingCurve.push([data[0], data[1]]);
                pendingCurve.push([data[2], data[3]]);
                pendingCurve.push([data[4], data[5]]);
                break;
            case 'Z':
                appendPendingCurve();
                currentPoints.push([start[0], start[1]]);
                break;
        }
    }
    appendPendingPoints();
    if (!distance) {
        return sets;
    }
    const out = [];
    for (const set of sets) {
        const simplifiedSet = (0,points_on_curve__WEBPACK_IMPORTED_MODULE_0__.simplify)(set, distance);
        if (simplifiedSet.length) {
            out.push(simplifiedSet);
        }
    }
    return out;
}


/***/ }),

/***/ "./node_modules/roughjs/bin/canvas.js":
/*!********************************************!*\
  !*** ./node_modules/roughjs/bin/canvas.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RoughCanvas": () => (/* binding */ RoughCanvas)
/* harmony export */ });
/* harmony import */ var _generator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./generator */ "./node_modules/roughjs/bin/generator.js");

class RoughCanvas {
    constructor(canvas, config) {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');
        this.gen = new _generator__WEBPACK_IMPORTED_MODULE_0__.RoughGenerator(config);
    }
    draw(drawable) {
        const sets = drawable.sets || [];
        const o = drawable.options || this.getDefaultOptions();
        const ctx = this.ctx;
        const precision = drawable.options.fixedDecimalPlaceDigits;
        for (const drawing of sets) {
            switch (drawing.type) {
                case 'path':
                    ctx.save();
                    ctx.strokeStyle = o.stroke === 'none' ? 'transparent' : o.stroke;
                    ctx.lineWidth = o.strokeWidth;
                    if (o.strokeLineDash) {
                        ctx.setLineDash(o.strokeLineDash);
                    }
                    if (o.strokeLineDashOffset) {
                        ctx.lineDashOffset = o.strokeLineDashOffset;
                    }
                    this._drawToContext(ctx, drawing, precision);
                    ctx.restore();
                    break;
                case 'fillPath': {
                    ctx.save();
                    ctx.fillStyle = o.fill || '';
                    const fillRule = (drawable.shape === 'curve' || drawable.shape === 'polygon' || drawable.shape === 'path') ? 'evenodd' : 'nonzero';
                    this._drawToContext(ctx, drawing, precision, fillRule);
                    ctx.restore();
                    break;
                }
                case 'fillSketch':
                    this.fillSketch(ctx, drawing, o);
                    break;
            }
        }
    }
    fillSketch(ctx, drawing, o) {
        let fweight = o.fillWeight;
        if (fweight < 0) {
            fweight = o.strokeWidth / 2;
        }
        ctx.save();
        if (o.fillLineDash) {
            ctx.setLineDash(o.fillLineDash);
        }
        if (o.fillLineDashOffset) {
            ctx.lineDashOffset = o.fillLineDashOffset;
        }
        ctx.strokeStyle = o.fill || '';
        ctx.lineWidth = fweight;
        this._drawToContext(ctx, drawing, o.fixedDecimalPlaceDigits);
        ctx.restore();
    }
    _drawToContext(ctx, drawing, fixedDecimals, rule = 'nonzero') {
        ctx.beginPath();
        for (const item of drawing.ops) {
            const data = ((typeof fixedDecimals === 'number') && fixedDecimals >= 0) ? (item.data.map((d) => +d.toFixed(fixedDecimals))) : item.data;
            switch (item.op) {
                case 'move':
                    ctx.moveTo(data[0], data[1]);
                    break;
                case 'bcurveTo':
                    ctx.bezierCurveTo(data[0], data[1], data[2], data[3], data[4], data[5]);
                    break;
                case 'lineTo':
                    ctx.lineTo(data[0], data[1]);
                    break;
            }
        }
        if (drawing.type === 'fillPath') {
            ctx.fill(rule);
        }
        else {
            ctx.stroke();
        }
    }
    get generator() {
        return this.gen;
    }
    getDefaultOptions() {
        return this.gen.defaultOptions;
    }
    line(x1, y1, x2, y2, options) {
        const d = this.gen.line(x1, y1, x2, y2, options);
        this.draw(d);
        return d;
    }
    rectangle(x, y, width, height, options) {
        const d = this.gen.rectangle(x, y, width, height, options);
        this.draw(d);
        return d;
    }
    ellipse(x, y, width, height, options) {
        const d = this.gen.ellipse(x, y, width, height, options);
        this.draw(d);
        return d;
    }
    circle(x, y, diameter, options) {
        const d = this.gen.circle(x, y, diameter, options);
        this.draw(d);
        return d;
    }
    linearPath(points, options) {
        const d = this.gen.linearPath(points, options);
        this.draw(d);
        return d;
    }
    polygon(points, options) {
        const d = this.gen.polygon(points, options);
        this.draw(d);
        return d;
    }
    arc(x, y, width, height, start, stop, closed = false, options) {
        const d = this.gen.arc(x, y, width, height, start, stop, closed, options);
        this.draw(d);
        return d;
    }
    curve(points, options) {
        const d = this.gen.curve(points, options);
        this.draw(d);
        return d;
    }
    path(d, options) {
        const drawing = this.gen.path(d, options);
        this.draw(drawing);
        return drawing;
    }
}


/***/ }),

/***/ "./node_modules/roughjs/bin/core.js":
/*!******************************************!*\
  !*** ./node_modules/roughjs/bin/core.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SVGNS": () => (/* binding */ SVGNS)
/* harmony export */ });
const SVGNS = 'http://www.w3.org/2000/svg';


/***/ }),

/***/ "./node_modules/roughjs/bin/fillers/dashed-filler.js":
/*!***********************************************************!*\
  !*** ./node_modules/roughjs/bin/fillers/dashed-filler.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DashedFiller": () => (/* binding */ DashedFiller)
/* harmony export */ });
/* harmony import */ var _geometry__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../geometry */ "./node_modules/roughjs/bin/geometry.js");
/* harmony import */ var _scan_line_hachure__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scan-line-hachure */ "./node_modules/roughjs/bin/fillers/scan-line-hachure.js");


class DashedFiller {
    constructor(helper) {
        this.helper = helper;
    }
    fillPolygons(polygonList, o) {
        const lines = (0,_scan_line_hachure__WEBPACK_IMPORTED_MODULE_1__.polygonHachureLines)(polygonList, o);
        return { type: 'fillSketch', ops: this.dashedLine(lines, o) };
    }
    dashedLine(lines, o) {
        const offset = o.dashOffset < 0 ? (o.hachureGap < 0 ? (o.strokeWidth * 4) : o.hachureGap) : o.dashOffset;
        const gap = o.dashGap < 0 ? (o.hachureGap < 0 ? (o.strokeWidth * 4) : o.hachureGap) : o.dashGap;
        const ops = [];
        lines.forEach((line) => {
            const length = (0,_geometry__WEBPACK_IMPORTED_MODULE_0__.lineLength)(line);
            const count = Math.floor(length / (offset + gap));
            const startOffset = (length + gap - (count * (offset + gap))) / 2;
            let p1 = line[0];
            let p2 = line[1];
            if (p1[0] > p2[0]) {
                p1 = line[1];
                p2 = line[0];
            }
            const alpha = Math.atan((p2[1] - p1[1]) / (p2[0] - p1[0]));
            for (let i = 0; i < count; i++) {
                const lstart = i * (offset + gap);
                const lend = lstart + offset;
                const start = [p1[0] + (lstart * Math.cos(alpha)) + (startOffset * Math.cos(alpha)), p1[1] + lstart * Math.sin(alpha) + (startOffset * Math.sin(alpha))];
                const end = [p1[0] + (lend * Math.cos(alpha)) + (startOffset * Math.cos(alpha)), p1[1] + (lend * Math.sin(alpha)) + (startOffset * Math.sin(alpha))];
                ops.push(...this.helper.doubleLineOps(start[0], start[1], end[0], end[1], o));
            }
        });
        return ops;
    }
}


/***/ }),

/***/ "./node_modules/roughjs/bin/fillers/dot-filler.js":
/*!********************************************************!*\
  !*** ./node_modules/roughjs/bin/fillers/dot-filler.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DotFiller": () => (/* binding */ DotFiller)
/* harmony export */ });
/* harmony import */ var _geometry__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../geometry */ "./node_modules/roughjs/bin/geometry.js");
/* harmony import */ var _scan_line_hachure__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scan-line-hachure */ "./node_modules/roughjs/bin/fillers/scan-line-hachure.js");


class DotFiller {
    constructor(helper) {
        this.helper = helper;
    }
    fillPolygons(polygonList, o) {
        o = Object.assign({}, o, { hachureAngle: 0 });
        const lines = (0,_scan_line_hachure__WEBPACK_IMPORTED_MODULE_1__.polygonHachureLines)(polygonList, o);
        return this.dotsOnLines(lines, o);
    }
    dotsOnLines(lines, o) {
        const ops = [];
        let gap = o.hachureGap;
        if (gap < 0) {
            gap = o.strokeWidth * 4;
        }
        gap = Math.max(gap, 0.1);
        let fweight = o.fillWeight;
        if (fweight < 0) {
            fweight = o.strokeWidth / 2;
        }
        const ro = gap / 4;
        for (const line of lines) {
            const length = (0,_geometry__WEBPACK_IMPORTED_MODULE_0__.lineLength)(line);
            const dl = length / gap;
            const count = Math.ceil(dl) - 1;
            const offset = length - (count * gap);
            const x = ((line[0][0] + line[1][0]) / 2) - (gap / 4);
            const minY = Math.min(line[0][1], line[1][1]);
            for (let i = 0; i < count; i++) {
                const y = minY + offset + (i * gap);
                const cx = (x - ro) + Math.random() * 2 * ro;
                const cy = (y - ro) + Math.random() * 2 * ro;
                const el = this.helper.ellipse(cx, cy, fweight, fweight, o);
                ops.push(...el.ops);
            }
        }
        return { type: 'fillSketch', ops };
    }
}


/***/ }),

/***/ "./node_modules/roughjs/bin/fillers/filler.js":
/*!****************************************************!*\
  !*** ./node_modules/roughjs/bin/fillers/filler.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getFiller": () => (/* binding */ getFiller)
/* harmony export */ });
/* harmony import */ var _hachure_filler__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./hachure-filler */ "./node_modules/roughjs/bin/fillers/hachure-filler.js");
/* harmony import */ var _zigzag_filler__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./zigzag-filler */ "./node_modules/roughjs/bin/fillers/zigzag-filler.js");
/* harmony import */ var _hatch_filler__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./hatch-filler */ "./node_modules/roughjs/bin/fillers/hatch-filler.js");
/* harmony import */ var _dot_filler__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dot-filler */ "./node_modules/roughjs/bin/fillers/dot-filler.js");
/* harmony import */ var _dashed_filler__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashed-filler */ "./node_modules/roughjs/bin/fillers/dashed-filler.js");
/* harmony import */ var _zigzag_line_filler__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./zigzag-line-filler */ "./node_modules/roughjs/bin/fillers/zigzag-line-filler.js");






const fillers = {};
function getFiller(o, helper) {
    let fillerName = o.fillStyle || 'hachure';
    if (!fillers[fillerName]) {
        switch (fillerName) {
            case 'zigzag':
                if (!fillers[fillerName]) {
                    fillers[fillerName] = new _zigzag_filler__WEBPACK_IMPORTED_MODULE_1__.ZigZagFiller(helper);
                }
                break;
            case 'cross-hatch':
                if (!fillers[fillerName]) {
                    fillers[fillerName] = new _hatch_filler__WEBPACK_IMPORTED_MODULE_2__.HatchFiller(helper);
                }
                break;
            case 'dots':
                if (!fillers[fillerName]) {
                    fillers[fillerName] = new _dot_filler__WEBPACK_IMPORTED_MODULE_3__.DotFiller(helper);
                }
                break;
            case 'dashed':
                if (!fillers[fillerName]) {
                    fillers[fillerName] = new _dashed_filler__WEBPACK_IMPORTED_MODULE_4__.DashedFiller(helper);
                }
                break;
            case 'zigzag-line':
                if (!fillers[fillerName]) {
                    fillers[fillerName] = new _zigzag_line_filler__WEBPACK_IMPORTED_MODULE_5__.ZigZagLineFiller(helper);
                }
                break;
            case 'hachure':
            default:
                fillerName = 'hachure';
                if (!fillers[fillerName]) {
                    fillers[fillerName] = new _hachure_filler__WEBPACK_IMPORTED_MODULE_0__.HachureFiller(helper);
                }
                break;
        }
    }
    return fillers[fillerName];
}


/***/ }),

/***/ "./node_modules/roughjs/bin/fillers/hachure-filler.js":
/*!************************************************************!*\
  !*** ./node_modules/roughjs/bin/fillers/hachure-filler.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HachureFiller": () => (/* binding */ HachureFiller)
/* harmony export */ });
/* harmony import */ var _scan_line_hachure__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./scan-line-hachure */ "./node_modules/roughjs/bin/fillers/scan-line-hachure.js");

class HachureFiller {
    constructor(helper) {
        this.helper = helper;
    }
    fillPolygons(polygonList, o) {
        return this._fillPolygons(polygonList, o);
    }
    _fillPolygons(polygonList, o) {
        const lines = (0,_scan_line_hachure__WEBPACK_IMPORTED_MODULE_0__.polygonHachureLines)(polygonList, o);
        const ops = this.renderLines(lines, o);
        return { type: 'fillSketch', ops };
    }
    renderLines(lines, o) {
        const ops = [];
        for (const line of lines) {
            ops.push(...this.helper.doubleLineOps(line[0][0], line[0][1], line[1][0], line[1][1], o));
        }
        return ops;
    }
}


/***/ }),

/***/ "./node_modules/roughjs/bin/fillers/hatch-filler.js":
/*!**********************************************************!*\
  !*** ./node_modules/roughjs/bin/fillers/hatch-filler.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HatchFiller": () => (/* binding */ HatchFiller)
/* harmony export */ });
/* harmony import */ var _hachure_filler__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./hachure-filler */ "./node_modules/roughjs/bin/fillers/hachure-filler.js");

class HatchFiller extends _hachure_filler__WEBPACK_IMPORTED_MODULE_0__.HachureFiller {
    fillPolygons(polygonList, o) {
        const set = this._fillPolygons(polygonList, o);
        const o2 = Object.assign({}, o, { hachureAngle: o.hachureAngle + 90 });
        const set2 = this._fillPolygons(polygonList, o2);
        set.ops = set.ops.concat(set2.ops);
        return set;
    }
}


/***/ }),

/***/ "./node_modules/roughjs/bin/fillers/scan-line-hachure.js":
/*!***************************************************************!*\
  !*** ./node_modules/roughjs/bin/fillers/scan-line-hachure.js ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "polygonHachureLines": () => (/* binding */ polygonHachureLines)
/* harmony export */ });
/* harmony import */ var _geometry__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../geometry */ "./node_modules/roughjs/bin/geometry.js");

function polygonHachureLines(polygonList, o) {
    const angle = o.hachureAngle + 90;
    let gap = o.hachureGap;
    if (gap < 0) {
        gap = o.strokeWidth * 4;
    }
    gap = Math.max(gap, 0.1);
    const rotationCenter = [0, 0];
    if (angle) {
        for (const polygon of polygonList) {
            (0,_geometry__WEBPACK_IMPORTED_MODULE_0__.rotatePoints)(polygon, rotationCenter, angle);
        }
    }
    const lines = straightHachureLines(polygonList, gap);
    if (angle) {
        for (const polygon of polygonList) {
            (0,_geometry__WEBPACK_IMPORTED_MODULE_0__.rotatePoints)(polygon, rotationCenter, -angle);
        }
        (0,_geometry__WEBPACK_IMPORTED_MODULE_0__.rotateLines)(lines, rotationCenter, -angle);
    }
    return lines;
}
function straightHachureLines(polygonList, gap) {
    const vertexArray = [];
    for (const polygon of polygonList) {
        const vertices = [...polygon];
        if (vertices[0].join(',') !== vertices[vertices.length - 1].join(',')) {
            vertices.push([vertices[0][0], vertices[0][1]]);
        }
        if (vertices.length > 2) {
            vertexArray.push(vertices);
        }
    }
    const lines = [];
    gap = Math.max(gap, 0.1);
    // Create sorted edges table
    const edges = [];
    for (const vertices of vertexArray) {
        for (let i = 0; i < vertices.length - 1; i++) {
            const p1 = vertices[i];
            const p2 = vertices[i + 1];
            if (p1[1] !== p2[1]) {
                const ymin = Math.min(p1[1], p2[1]);
                edges.push({
                    ymin,
                    ymax: Math.max(p1[1], p2[1]),
                    x: ymin === p1[1] ? p1[0] : p2[0],
                    islope: (p2[0] - p1[0]) / (p2[1] - p1[1]),
                });
            }
        }
    }
    edges.sort((e1, e2) => {
        if (e1.ymin < e2.ymin) {
            return -1;
        }
        if (e1.ymin > e2.ymin) {
            return 1;
        }
        if (e1.x < e2.x) {
            return -1;
        }
        if (e1.x > e2.x) {
            return 1;
        }
        if (e1.ymax === e2.ymax) {
            return 0;
        }
        return (e1.ymax - e2.ymax) / Math.abs((e1.ymax - e2.ymax));
    });
    if (!edges.length) {
        return lines;
    }
    // Start scanning
    let activeEdges = [];
    let y = edges[0].ymin;
    while (activeEdges.length || edges.length) {
        if (edges.length) {
            let ix = -1;
            for (let i = 0; i < edges.length; i++) {
                if (edges[i].ymin > y) {
                    break;
                }
                ix = i;
            }
            const removed = edges.splice(0, ix + 1);
            removed.forEach((edge) => {
                activeEdges.push({ s: y, edge });
            });
        }
        activeEdges = activeEdges.filter((ae) => {
            if (ae.edge.ymax <= y) {
                return false;
            }
            return true;
        });
        activeEdges.sort((ae1, ae2) => {
            if (ae1.edge.x === ae2.edge.x) {
                return 0;
            }
            return (ae1.edge.x - ae2.edge.x) / Math.abs((ae1.edge.x - ae2.edge.x));
        });
        // fill between the edges
        if (activeEdges.length > 1) {
            for (let i = 0; i < activeEdges.length; i = i + 2) {
                const nexti = i + 1;
                if (nexti >= activeEdges.length) {
                    break;
                }
                const ce = activeEdges[i].edge;
                const ne = activeEdges[nexti].edge;
                lines.push([
                    [Math.round(ce.x), y],
                    [Math.round(ne.x), y],
                ]);
            }
        }
        y += gap;
        activeEdges.forEach((ae) => {
            ae.edge.x = ae.edge.x + (gap * ae.edge.islope);
        });
    }
    return lines;
}


/***/ }),

/***/ "./node_modules/roughjs/bin/fillers/zigzag-filler.js":
/*!***********************************************************!*\
  !*** ./node_modules/roughjs/bin/fillers/zigzag-filler.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ZigZagFiller": () => (/* binding */ ZigZagFiller)
/* harmony export */ });
/* harmony import */ var _hachure_filler__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./hachure-filler */ "./node_modules/roughjs/bin/fillers/hachure-filler.js");
/* harmony import */ var _scan_line_hachure__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scan-line-hachure */ "./node_modules/roughjs/bin/fillers/scan-line-hachure.js");
/* harmony import */ var _geometry__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../geometry */ "./node_modules/roughjs/bin/geometry.js");



class ZigZagFiller extends _hachure_filler__WEBPACK_IMPORTED_MODULE_0__.HachureFiller {
    fillPolygons(polygonList, o) {
        let gap = o.hachureGap;
        if (gap < 0) {
            gap = o.strokeWidth * 4;
        }
        gap = Math.max(gap, 0.1);
        const o2 = Object.assign({}, o, { hachureGap: gap });
        const lines = (0,_scan_line_hachure__WEBPACK_IMPORTED_MODULE_1__.polygonHachureLines)(polygonList, o2);
        const zigZagAngle = (Math.PI / 180) * o.hachureAngle;
        const zigzagLines = [];
        const dgx = gap * 0.5 * Math.cos(zigZagAngle);
        const dgy = gap * 0.5 * Math.sin(zigZagAngle);
        for (const [p1, p2] of lines) {
            if ((0,_geometry__WEBPACK_IMPORTED_MODULE_2__.lineLength)([p1, p2])) {
                zigzagLines.push([
                    [p1[0] - dgx, p1[1] + dgy],
                    [...p2],
                ], [
                    [p1[0] + dgx, p1[1] - dgy],
                    [...p2],
                ]);
            }
        }
        const ops = this.renderLines(zigzagLines, o);
        return { type: 'fillSketch', ops };
    }
}


/***/ }),

/***/ "./node_modules/roughjs/bin/fillers/zigzag-line-filler.js":
/*!****************************************************************!*\
  !*** ./node_modules/roughjs/bin/fillers/zigzag-line-filler.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ZigZagLineFiller": () => (/* binding */ ZigZagLineFiller)
/* harmony export */ });
/* harmony import */ var _geometry__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../geometry */ "./node_modules/roughjs/bin/geometry.js");
/* harmony import */ var _scan_line_hachure__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scan-line-hachure */ "./node_modules/roughjs/bin/fillers/scan-line-hachure.js");


class ZigZagLineFiller {
    constructor(helper) {
        this.helper = helper;
    }
    fillPolygons(polygonList, o) {
        const gap = o.hachureGap < 0 ? (o.strokeWidth * 4) : o.hachureGap;
        const zo = o.zigzagOffset < 0 ? gap : o.zigzagOffset;
        o = Object.assign({}, o, { hachureGap: gap + zo });
        const lines = (0,_scan_line_hachure__WEBPACK_IMPORTED_MODULE_1__.polygonHachureLines)(polygonList, o);
        return { type: 'fillSketch', ops: this.zigzagLines(lines, zo, o) };
    }
    zigzagLines(lines, zo, o) {
        const ops = [];
        lines.forEach((line) => {
            const length = (0,_geometry__WEBPACK_IMPORTED_MODULE_0__.lineLength)(line);
            const count = Math.round(length / (2 * zo));
            let p1 = line[0];
            let p2 = line[1];
            if (p1[0] > p2[0]) {
                p1 = line[1];
                p2 = line[0];
            }
            const alpha = Math.atan((p2[1] - p1[1]) / (p2[0] - p1[0]));
            for (let i = 0; i < count; i++) {
                const lstart = i * 2 * zo;
                const lend = (i + 1) * 2 * zo;
                const dz = Math.sqrt(2 * Math.pow(zo, 2));
                const start = [p1[0] + (lstart * Math.cos(alpha)), p1[1] + lstart * Math.sin(alpha)];
                const end = [p1[0] + (lend * Math.cos(alpha)), p1[1] + (lend * Math.sin(alpha))];
                const middle = [start[0] + dz * Math.cos(alpha + Math.PI / 4), start[1] + dz * Math.sin(alpha + Math.PI / 4)];
                ops.push(...this.helper.doubleLineOps(start[0], start[1], middle[0], middle[1], o), ...this.helper.doubleLineOps(middle[0], middle[1], end[0], end[1], o));
            }
        });
        return ops;
    }
}


/***/ }),

/***/ "./node_modules/roughjs/bin/generator.js":
/*!***********************************************!*\
  !*** ./node_modules/roughjs/bin/generator.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RoughGenerator": () => (/* binding */ RoughGenerator)
/* harmony export */ });
/* harmony import */ var _renderer_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./renderer.js */ "./node_modules/roughjs/bin/renderer.js");
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./math.js */ "./node_modules/roughjs/bin/math.js");
/* harmony import */ var points_on_curve_lib_curve_to_bezier_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! points-on-curve/lib/curve-to-bezier.js */ "./node_modules/points-on-curve/lib/curve-to-bezier.js");
/* harmony import */ var points_on_curve__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! points-on-curve */ "./node_modules/points-on-curve/lib/index.js");
/* harmony import */ var points_on_path__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! points-on-path */ "./node_modules/points-on-path/lib/index.js");





const NOS = 'none';
class RoughGenerator {
    constructor(config) {
        this.defaultOptions = {
            maxRandomnessOffset: 2,
            roughness: 1,
            bowing: 1,
            stroke: '#000',
            strokeWidth: 1,
            curveTightness: 0,
            curveFitting: 0.95,
            curveStepCount: 9,
            fillStyle: 'hachure',
            fillWeight: -1,
            hachureAngle: -41,
            hachureGap: -1,
            dashOffset: -1,
            dashGap: -1,
            zigzagOffset: -1,
            seed: 0,
            disableMultiStroke: false,
            disableMultiStrokeFill: false,
            preserveVertices: false,
        };
        this.config = config || {};
        if (this.config.options) {
            this.defaultOptions = this._o(this.config.options);
        }
    }
    static newSeed() {
        return (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.randomSeed)();
    }
    _o(options) {
        return options ? Object.assign({}, this.defaultOptions, options) : this.defaultOptions;
    }
    _d(shape, sets, options) {
        return { shape, sets: sets || [], options: options || this.defaultOptions };
    }
    line(x1, y1, x2, y2, options) {
        const o = this._o(options);
        return this._d('line', [(0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.line)(x1, y1, x2, y2, o)], o);
    }
    rectangle(x, y, width, height, options) {
        const o = this._o(options);
        const paths = [];
        const outline = (0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.rectangle)(x, y, width, height, o);
        if (o.fill) {
            const points = [[x, y], [x + width, y], [x + width, y + height], [x, y + height]];
            if (o.fillStyle === 'solid') {
                paths.push((0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.solidFillPolygon)([points], o));
            }
            else {
                paths.push((0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.patternFillPolygons)([points], o));
            }
        }
        if (o.stroke !== NOS) {
            paths.push(outline);
        }
        return this._d('rectangle', paths, o);
    }
    ellipse(x, y, width, height, options) {
        const o = this._o(options);
        const paths = [];
        const ellipseParams = (0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.generateEllipseParams)(width, height, o);
        const ellipseResponse = (0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.ellipseWithParams)(x, y, o, ellipseParams);
        if (o.fill) {
            if (o.fillStyle === 'solid') {
                const shape = (0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.ellipseWithParams)(x, y, o, ellipseParams).opset;
                shape.type = 'fillPath';
                paths.push(shape);
            }
            else {
                paths.push((0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.patternFillPolygons)([ellipseResponse.estimatedPoints], o));
            }
        }
        if (o.stroke !== NOS) {
            paths.push(ellipseResponse.opset);
        }
        return this._d('ellipse', paths, o);
    }
    circle(x, y, diameter, options) {
        const ret = this.ellipse(x, y, diameter, diameter, options);
        ret.shape = 'circle';
        return ret;
    }
    linearPath(points, options) {
        const o = this._o(options);
        return this._d('linearPath', [(0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.linearPath)(points, false, o)], o);
    }
    arc(x, y, width, height, start, stop, closed = false, options) {
        const o = this._o(options);
        const paths = [];
        const outline = (0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.arc)(x, y, width, height, start, stop, closed, true, o);
        if (closed && o.fill) {
            if (o.fillStyle === 'solid') {
                const fillOptions = Object.assign({}, o);
                fillOptions.disableMultiStroke = true;
                const shape = (0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.arc)(x, y, width, height, start, stop, true, false, fillOptions);
                shape.type = 'fillPath';
                paths.push(shape);
            }
            else {
                paths.push((0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.patternFillArc)(x, y, width, height, start, stop, o));
            }
        }
        if (o.stroke !== NOS) {
            paths.push(outline);
        }
        return this._d('arc', paths, o);
    }
    curve(points, options) {
        const o = this._o(options);
        const paths = [];
        const outline = (0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.curve)(points, o);
        if (o.fill && o.fill !== NOS && points.length >= 3) {
            const bcurve = (0,points_on_curve_lib_curve_to_bezier_js__WEBPACK_IMPORTED_MODULE_2__.curveToBezier)(points);
            const polyPoints = (0,points_on_curve__WEBPACK_IMPORTED_MODULE_3__.pointsOnBezierCurves)(bcurve, 10, (1 + o.roughness) / 2);
            if (o.fillStyle === 'solid') {
                paths.push((0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.solidFillPolygon)([polyPoints], o));
            }
            else {
                paths.push((0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.patternFillPolygons)([polyPoints], o));
            }
        }
        if (o.stroke !== NOS) {
            paths.push(outline);
        }
        return this._d('curve', paths, o);
    }
    polygon(points, options) {
        const o = this._o(options);
        const paths = [];
        const outline = (0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.linearPath)(points, true, o);
        if (o.fill) {
            if (o.fillStyle === 'solid') {
                paths.push((0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.solidFillPolygon)([points], o));
            }
            else {
                paths.push((0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.patternFillPolygons)([points], o));
            }
        }
        if (o.stroke !== NOS) {
            paths.push(outline);
        }
        return this._d('polygon', paths, o);
    }
    path(d, options) {
        const o = this._o(options);
        const paths = [];
        if (!d) {
            return this._d('path', paths, o);
        }
        d = (d || '').replace(/\n/g, ' ').replace(/(-\s)/g, '-').replace('/(\s\s)/g', ' ');
        const hasFill = o.fill && o.fill !== 'transparent' && o.fill !== NOS;
        const hasStroke = o.stroke !== NOS;
        const simplified = !!(o.simplification && (o.simplification < 1));
        const distance = simplified ? (4 - 4 * (o.simplification)) : ((1 + o.roughness) / 2);
        const sets = (0,points_on_path__WEBPACK_IMPORTED_MODULE_4__.pointsOnPath)(d, 1, distance);
        if (hasFill) {
            if (o.fillStyle === 'solid') {
                paths.push((0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.solidFillPolygon)(sets, o));
            }
            else {
                paths.push((0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.patternFillPolygons)(sets, o));
            }
        }
        if (hasStroke) {
            if (simplified) {
                sets.forEach((set) => {
                    paths.push((0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.linearPath)(set, false, o));
                });
            }
            else {
                paths.push((0,_renderer_js__WEBPACK_IMPORTED_MODULE_0__.svgPath)(d, o));
            }
        }
        return this._d('path', paths, o);
    }
    opsToPath(drawing, fixedDecimals) {
        let path = '';
        for (const item of drawing.ops) {
            const data = ((typeof fixedDecimals === 'number') && fixedDecimals >= 0) ? (item.data.map((d) => +d.toFixed(fixedDecimals))) : item.data;
            switch (item.op) {
                case 'move':
                    path += `M${data[0]} ${data[1]} `;
                    break;
                case 'bcurveTo':
                    path += `C${data[0]} ${data[1]}, ${data[2]} ${data[3]}, ${data[4]} ${data[5]} `;
                    break;
                case 'lineTo':
                    path += `L${data[0]} ${data[1]} `;
                    break;
            }
        }
        return path.trim();
    }
    toPaths(drawable) {
        const sets = drawable.sets || [];
        const o = drawable.options || this.defaultOptions;
        const paths = [];
        for (const drawing of sets) {
            let path = null;
            switch (drawing.type) {
                case 'path':
                    path = {
                        d: this.opsToPath(drawing),
                        stroke: o.stroke,
                        strokeWidth: o.strokeWidth,
                        fill: NOS,
                    };
                    break;
                case 'fillPath':
                    path = {
                        d: this.opsToPath(drawing),
                        stroke: NOS,
                        strokeWidth: 0,
                        fill: o.fill || NOS,
                    };
                    break;
                case 'fillSketch':
                    path = this.fillSketch(drawing, o);
                    break;
            }
            if (path) {
                paths.push(path);
            }
        }
        return paths;
    }
    fillSketch(drawing, o) {
        let fweight = o.fillWeight;
        if (fweight < 0) {
            fweight = o.strokeWidth / 2;
        }
        return {
            d: this.opsToPath(drawing),
            stroke: o.fill || NOS,
            strokeWidth: fweight,
            fill: NOS,
        };
    }
}


/***/ }),

/***/ "./node_modules/roughjs/bin/geometry.js":
/*!**********************************************!*\
  !*** ./node_modules/roughjs/bin/geometry.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "lineLength": () => (/* binding */ lineLength),
/* harmony export */   "rotateLines": () => (/* binding */ rotateLines),
/* harmony export */   "rotatePoints": () => (/* binding */ rotatePoints)
/* harmony export */ });
function rotatePoints(points, center, degrees) {
    if (points && points.length) {
        const [cx, cy] = center;
        const angle = (Math.PI / 180) * degrees;
        const cos = Math.cos(angle);
        const sin = Math.sin(angle);
        points.forEach((p) => {
            const [x, y] = p;
            p[0] = ((x - cx) * cos) - ((y - cy) * sin) + cx;
            p[1] = ((x - cx) * sin) + ((y - cy) * cos) + cy;
        });
    }
}
function rotateLines(lines, center, degrees) {
    const points = [];
    lines.forEach((line) => points.push(...line));
    rotatePoints(points, center, degrees);
}
function lineLength(line) {
    const p1 = line[0];
    const p2 = line[1];
    return Math.sqrt(Math.pow(p1[0] - p2[0], 2) + Math.pow(p1[1] - p2[1], 2));
}


/***/ }),

/***/ "./node_modules/roughjs/bin/math.js":
/*!******************************************!*\
  !*** ./node_modules/roughjs/bin/math.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Random": () => (/* binding */ Random),
/* harmony export */   "randomSeed": () => (/* binding */ randomSeed)
/* harmony export */ });
function randomSeed() {
    return Math.floor(Math.random() * 2 ** 31);
}
class Random {
    constructor(seed) {
        this.seed = seed;
    }
    next() {
        if (this.seed) {
            return ((2 ** 31 - 1) & (this.seed = Math.imul(48271, this.seed))) / 2 ** 31;
        }
        else {
            return Math.random();
        }
    }
}


/***/ }),

/***/ "./node_modules/roughjs/bin/renderer.js":
/*!**********************************************!*\
  !*** ./node_modules/roughjs/bin/renderer.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "arc": () => (/* binding */ arc),
/* harmony export */   "curve": () => (/* binding */ curve),
/* harmony export */   "doubleLineFillOps": () => (/* binding */ doubleLineFillOps),
/* harmony export */   "ellipse": () => (/* binding */ ellipse),
/* harmony export */   "ellipseWithParams": () => (/* binding */ ellipseWithParams),
/* harmony export */   "generateEllipseParams": () => (/* binding */ generateEllipseParams),
/* harmony export */   "line": () => (/* binding */ line),
/* harmony export */   "linearPath": () => (/* binding */ linearPath),
/* harmony export */   "patternFillArc": () => (/* binding */ patternFillArc),
/* harmony export */   "patternFillPolygons": () => (/* binding */ patternFillPolygons),
/* harmony export */   "polygon": () => (/* binding */ polygon),
/* harmony export */   "randOffset": () => (/* binding */ randOffset),
/* harmony export */   "randOffsetWithRange": () => (/* binding */ randOffsetWithRange),
/* harmony export */   "rectangle": () => (/* binding */ rectangle),
/* harmony export */   "solidFillPolygon": () => (/* binding */ solidFillPolygon),
/* harmony export */   "svgPath": () => (/* binding */ svgPath)
/* harmony export */ });
/* harmony import */ var _fillers_filler_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fillers/filler.js */ "./node_modules/roughjs/bin/fillers/filler.js");
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./math.js */ "./node_modules/roughjs/bin/math.js");
/* harmony import */ var path_data_parser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! path-data-parser */ "./node_modules/path-data-parser/lib/index.js");



const helper = {
    randOffset,
    randOffsetWithRange,
    ellipse,
    doubleLineOps: doubleLineFillOps,
};
function line(x1, y1, x2, y2, o) {
    return { type: 'path', ops: _doubleLine(x1, y1, x2, y2, o) };
}
function linearPath(points, close, o) {
    const len = (points || []).length;
    if (len > 2) {
        const ops = [];
        for (let i = 0; i < (len - 1); i++) {
            ops.push(..._doubleLine(points[i][0], points[i][1], points[i + 1][0], points[i + 1][1], o));
        }
        if (close) {
            ops.push(..._doubleLine(points[len - 1][0], points[len - 1][1], points[0][0], points[0][1], o));
        }
        return { type: 'path', ops };
    }
    else if (len === 2) {
        return line(points[0][0], points[0][1], points[1][0], points[1][1], o);
    }
    return { type: 'path', ops: [] };
}
function polygon(points, o) {
    return linearPath(points, true, o);
}
function rectangle(x, y, width, height, o) {
    const points = [
        [x, y],
        [x + width, y],
        [x + width, y + height],
        [x, y + height],
    ];
    return polygon(points, o);
}
function curve(points, o) {
    let o1 = _curveWithOffset(points, 1 * (1 + o.roughness * 0.2), o);
    if (!o.disableMultiStroke) {
        const o2 = _curveWithOffset(points, 1.5 * (1 + o.roughness * 0.22), cloneOptionsAlterSeed(o));
        o1 = o1.concat(o2);
    }
    return { type: 'path', ops: o1 };
}
function ellipse(x, y, width, height, o) {
    const params = generateEllipseParams(width, height, o);
    return ellipseWithParams(x, y, o, params).opset;
}
function generateEllipseParams(width, height, o) {
    const psq = Math.sqrt(Math.PI * 2 * Math.sqrt((Math.pow(width / 2, 2) + Math.pow(height / 2, 2)) / 2));
    const stepCount = Math.ceil(Math.max(o.curveStepCount, (o.curveStepCount / Math.sqrt(200)) * psq));
    const increment = (Math.PI * 2) / stepCount;
    let rx = Math.abs(width / 2);
    let ry = Math.abs(height / 2);
    const curveFitRandomness = 1 - o.curveFitting;
    rx += _offsetOpt(rx * curveFitRandomness, o);
    ry += _offsetOpt(ry * curveFitRandomness, o);
    return { increment, rx, ry };
}
function ellipseWithParams(x, y, o, ellipseParams) {
    const [ap1, cp1] = _computeEllipsePoints(ellipseParams.increment, x, y, ellipseParams.rx, ellipseParams.ry, 1, ellipseParams.increment * _offset(0.1, _offset(0.4, 1, o), o), o);
    let o1 = _curve(ap1, null, o);
    if ((!o.disableMultiStroke) && (o.roughness !== 0)) {
        const [ap2] = _computeEllipsePoints(ellipseParams.increment, x, y, ellipseParams.rx, ellipseParams.ry, 1.5, 0, o);
        const o2 = _curve(ap2, null, o);
        o1 = o1.concat(o2);
    }
    return {
        estimatedPoints: cp1,
        opset: { type: 'path', ops: o1 },
    };
}
function arc(x, y, width, height, start, stop, closed, roughClosure, o) {
    const cx = x;
    const cy = y;
    let rx = Math.abs(width / 2);
    let ry = Math.abs(height / 2);
    rx += _offsetOpt(rx * 0.01, o);
    ry += _offsetOpt(ry * 0.01, o);
    let strt = start;
    let stp = stop;
    while (strt < 0) {
        strt += Math.PI * 2;
        stp += Math.PI * 2;
    }
    if ((stp - strt) > (Math.PI * 2)) {
        strt = 0;
        stp = Math.PI * 2;
    }
    const ellipseInc = (Math.PI * 2) / o.curveStepCount;
    const arcInc = Math.min(ellipseInc / 2, (stp - strt) / 2);
    const ops = _arc(arcInc, cx, cy, rx, ry, strt, stp, 1, o);
    if (!o.disableMultiStroke) {
        const o2 = _arc(arcInc, cx, cy, rx, ry, strt, stp, 1.5, o);
        ops.push(...o2);
    }
    if (closed) {
        if (roughClosure) {
            ops.push(..._doubleLine(cx, cy, cx + rx * Math.cos(strt), cy + ry * Math.sin(strt), o), ..._doubleLine(cx, cy, cx + rx * Math.cos(stp), cy + ry * Math.sin(stp), o));
        }
        else {
            ops.push({ op: 'lineTo', data: [cx, cy] }, { op: 'lineTo', data: [cx + rx * Math.cos(strt), cy + ry * Math.sin(strt)] });
        }
    }
    return { type: 'path', ops };
}
function svgPath(path, o) {
    const segments = (0,path_data_parser__WEBPACK_IMPORTED_MODULE_2__.normalize)((0,path_data_parser__WEBPACK_IMPORTED_MODULE_2__.absolutize)((0,path_data_parser__WEBPACK_IMPORTED_MODULE_2__.parsePath)(path)));
    const ops = [];
    let first = [0, 0];
    let current = [0, 0];
    for (const { key, data } of segments) {
        switch (key) {
            case 'M': {
                const ro = 1 * (o.maxRandomnessOffset || 0);
                const pv = o.preserveVertices;
                ops.push({ op: 'move', data: data.map((d) => d + (pv ? 0 : _offsetOpt(ro, o))) });
                current = [data[0], data[1]];
                first = [data[0], data[1]];
                break;
            }
            case 'L':
                ops.push(..._doubleLine(current[0], current[1], data[0], data[1], o));
                current = [data[0], data[1]];
                break;
            case 'C': {
                const [x1, y1, x2, y2, x, y] = data;
                ops.push(..._bezierTo(x1, y1, x2, y2, x, y, current, o));
                current = [x, y];
                break;
            }
            case 'Z':
                ops.push(..._doubleLine(current[0], current[1], first[0], first[1], o));
                current = [first[0], first[1]];
                break;
        }
    }
    return { type: 'path', ops };
}
// Fills
function solidFillPolygon(polygonList, o) {
    const ops = [];
    for (const points of polygonList) {
        if (points.length) {
            const offset = o.maxRandomnessOffset || 0;
            const len = points.length;
            if (len > 2) {
                ops.push({ op: 'move', data: [points[0][0] + _offsetOpt(offset, o), points[0][1] + _offsetOpt(offset, o)] });
                for (let i = 1; i < len; i++) {
                    ops.push({ op: 'lineTo', data: [points[i][0] + _offsetOpt(offset, o), points[i][1] + _offsetOpt(offset, o)] });
                }
            }
        }
    }
    return { type: 'fillPath', ops };
}
function patternFillPolygons(polygonList, o) {
    return (0,_fillers_filler_js__WEBPACK_IMPORTED_MODULE_0__.getFiller)(o, helper).fillPolygons(polygonList, o);
}
function patternFillArc(x, y, width, height, start, stop, o) {
    const cx = x;
    const cy = y;
    let rx = Math.abs(width / 2);
    let ry = Math.abs(height / 2);
    rx += _offsetOpt(rx * 0.01, o);
    ry += _offsetOpt(ry * 0.01, o);
    let strt = start;
    let stp = stop;
    while (strt < 0) {
        strt += Math.PI * 2;
        stp += Math.PI * 2;
    }
    if ((stp - strt) > (Math.PI * 2)) {
        strt = 0;
        stp = Math.PI * 2;
    }
    const increment = (stp - strt) / o.curveStepCount;
    const points = [];
    for (let angle = strt; angle <= stp; angle = angle + increment) {
        points.push([cx + rx * Math.cos(angle), cy + ry * Math.sin(angle)]);
    }
    points.push([cx + rx * Math.cos(stp), cy + ry * Math.sin(stp)]);
    points.push([cx, cy]);
    return patternFillPolygons([points], o);
}
function randOffset(x, o) {
    return _offsetOpt(x, o);
}
function randOffsetWithRange(min, max, o) {
    return _offset(min, max, o);
}
function doubleLineFillOps(x1, y1, x2, y2, o) {
    return _doubleLine(x1, y1, x2, y2, o, true);
}
// Private helpers
function cloneOptionsAlterSeed(ops) {
    const result = Object.assign({}, ops);
    result.randomizer = undefined;
    if (ops.seed) {
        result.seed = ops.seed + 1;
    }
    return result;
}
function random(ops) {
    if (!ops.randomizer) {
        ops.randomizer = new _math_js__WEBPACK_IMPORTED_MODULE_1__.Random(ops.seed || 0);
    }
    return ops.randomizer.next();
}
function _offset(min, max, ops, roughnessGain = 1) {
    return ops.roughness * roughnessGain * ((random(ops) * (max - min)) + min);
}
function _offsetOpt(x, ops, roughnessGain = 1) {
    return _offset(-x, x, ops, roughnessGain);
}
function _doubleLine(x1, y1, x2, y2, o, filling = false) {
    const singleStroke = filling ? o.disableMultiStrokeFill : o.disableMultiStroke;
    const o1 = _line(x1, y1, x2, y2, o, true, false);
    if (singleStroke) {
        return o1;
    }
    const o2 = _line(x1, y1, x2, y2, o, true, true);
    return o1.concat(o2);
}
function _line(x1, y1, x2, y2, o, move, overlay) {
    const lengthSq = Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2);
    const length = Math.sqrt(lengthSq);
    let roughnessGain = 1;
    if (length < 200) {
        roughnessGain = 1;
    }
    else if (length > 500) {
        roughnessGain = 0.4;
    }
    else {
        roughnessGain = (-0.0016668) * length + 1.233334;
    }
    let offset = o.maxRandomnessOffset || 0;
    if ((offset * offset * 100) > lengthSq) {
        offset = length / 10;
    }
    const halfOffset = offset / 2;
    const divergePoint = 0.2 + random(o) * 0.2;
    let midDispX = o.bowing * o.maxRandomnessOffset * (y2 - y1) / 200;
    let midDispY = o.bowing * o.maxRandomnessOffset * (x1 - x2) / 200;
    midDispX = _offsetOpt(midDispX, o, roughnessGain);
    midDispY = _offsetOpt(midDispY, o, roughnessGain);
    const ops = [];
    const randomHalf = () => _offsetOpt(halfOffset, o, roughnessGain);
    const randomFull = () => _offsetOpt(offset, o, roughnessGain);
    const preserveVertices = o.preserveVertices;
    if (move) {
        if (overlay) {
            ops.push({
                op: 'move', data: [
                    x1 + (preserveVertices ? 0 : randomHalf()),
                    y1 + (preserveVertices ? 0 : randomHalf()),
                ],
            });
        }
        else {
            ops.push({
                op: 'move', data: [
                    x1 + (preserveVertices ? 0 : _offsetOpt(offset, o, roughnessGain)),
                    y1 + (preserveVertices ? 0 : _offsetOpt(offset, o, roughnessGain)),
                ],
            });
        }
    }
    if (overlay) {
        ops.push({
            op: 'bcurveTo',
            data: [
                midDispX + x1 + (x2 - x1) * divergePoint + randomHalf(),
                midDispY + y1 + (y2 - y1) * divergePoint + randomHalf(),
                midDispX + x1 + 2 * (x2 - x1) * divergePoint + randomHalf(),
                midDispY + y1 + 2 * (y2 - y1) * divergePoint + randomHalf(),
                x2 + (preserveVertices ? 0 : randomHalf()),
                y2 + (preserveVertices ? 0 : randomHalf()),
            ],
        });
    }
    else {
        ops.push({
            op: 'bcurveTo',
            data: [
                midDispX + x1 + (x2 - x1) * divergePoint + randomFull(),
                midDispY + y1 + (y2 - y1) * divergePoint + randomFull(),
                midDispX + x1 + 2 * (x2 - x1) * divergePoint + randomFull(),
                midDispY + y1 + 2 * (y2 - y1) * divergePoint + randomFull(),
                x2 + (preserveVertices ? 0 : randomFull()),
                y2 + (preserveVertices ? 0 : randomFull()),
            ],
        });
    }
    return ops;
}
function _curveWithOffset(points, offset, o) {
    const ps = [];
    ps.push([
        points[0][0] + _offsetOpt(offset, o),
        points[0][1] + _offsetOpt(offset, o),
    ]);
    ps.push([
        points[0][0] + _offsetOpt(offset, o),
        points[0][1] + _offsetOpt(offset, o),
    ]);
    for (let i = 1; i < points.length; i++) {
        ps.push([
            points[i][0] + _offsetOpt(offset, o),
            points[i][1] + _offsetOpt(offset, o),
        ]);
        if (i === (points.length - 1)) {
            ps.push([
                points[i][0] + _offsetOpt(offset, o),
                points[i][1] + _offsetOpt(offset, o),
            ]);
        }
    }
    return _curve(ps, null, o);
}
function _curve(points, closePoint, o) {
    const len = points.length;
    const ops = [];
    if (len > 3) {
        const b = [];
        const s = 1 - o.curveTightness;
        ops.push({ op: 'move', data: [points[1][0], points[1][1]] });
        for (let i = 1; (i + 2) < len; i++) {
            const cachedVertArray = points[i];
            b[0] = [cachedVertArray[0], cachedVertArray[1]];
            b[1] = [cachedVertArray[0] + (s * points[i + 1][0] - s * points[i - 1][0]) / 6, cachedVertArray[1] + (s * points[i + 1][1] - s * points[i - 1][1]) / 6];
            b[2] = [points[i + 1][0] + (s * points[i][0] - s * points[i + 2][0]) / 6, points[i + 1][1] + (s * points[i][1] - s * points[i + 2][1]) / 6];
            b[3] = [points[i + 1][0], points[i + 1][1]];
            ops.push({ op: 'bcurveTo', data: [b[1][0], b[1][1], b[2][0], b[2][1], b[3][0], b[3][1]] });
        }
        if (closePoint && closePoint.length === 2) {
            const ro = o.maxRandomnessOffset;
            ops.push({ op: 'lineTo', data: [closePoint[0] + _offsetOpt(ro, o), closePoint[1] + _offsetOpt(ro, o)] });
        }
    }
    else if (len === 3) {
        ops.push({ op: 'move', data: [points[1][0], points[1][1]] });
        ops.push({
            op: 'bcurveTo',
            data: [
                points[1][0], points[1][1],
                points[2][0], points[2][1],
                points[2][0], points[2][1],
            ],
        });
    }
    else if (len === 2) {
        ops.push(..._doubleLine(points[0][0], points[0][1], points[1][0], points[1][1], o));
    }
    return ops;
}
function _computeEllipsePoints(increment, cx, cy, rx, ry, offset, overlap, o) {
    const coreOnly = o.roughness === 0;
    const corePoints = [];
    const allPoints = [];
    if (coreOnly) {
        increment = increment / 4;
        allPoints.push([
            cx + rx * Math.cos(-increment),
            cy + ry * Math.sin(-increment),
        ]);
        for (let angle = 0; angle <= Math.PI * 2; angle = angle + increment) {
            const p = [
                cx + rx * Math.cos(angle),
                cy + ry * Math.sin(angle),
            ];
            corePoints.push(p);
            allPoints.push(p);
        }
        allPoints.push([
            cx + rx * Math.cos(0),
            cy + ry * Math.sin(0),
        ]);
        allPoints.push([
            cx + rx * Math.cos(increment),
            cy + ry * Math.sin(increment),
        ]);
    }
    else {
        const radOffset = _offsetOpt(0.5, o) - (Math.PI / 2);
        allPoints.push([
            _offsetOpt(offset, o) + cx + 0.9 * rx * Math.cos(radOffset - increment),
            _offsetOpt(offset, o) + cy + 0.9 * ry * Math.sin(radOffset - increment),
        ]);
        const endAngle = Math.PI * 2 + radOffset - 0.01;
        for (let angle = radOffset; angle < endAngle; angle = angle + increment) {
            const p = [
                _offsetOpt(offset, o) + cx + rx * Math.cos(angle),
                _offsetOpt(offset, o) + cy + ry * Math.sin(angle),
            ];
            corePoints.push(p);
            allPoints.push(p);
        }
        allPoints.push([
            _offsetOpt(offset, o) + cx + rx * Math.cos(radOffset + Math.PI * 2 + overlap * 0.5),
            _offsetOpt(offset, o) + cy + ry * Math.sin(radOffset + Math.PI * 2 + overlap * 0.5),
        ]);
        allPoints.push([
            _offsetOpt(offset, o) + cx + 0.98 * rx * Math.cos(radOffset + overlap),
            _offsetOpt(offset, o) + cy + 0.98 * ry * Math.sin(radOffset + overlap),
        ]);
        allPoints.push([
            _offsetOpt(offset, o) + cx + 0.9 * rx * Math.cos(radOffset + overlap * 0.5),
            _offsetOpt(offset, o) + cy + 0.9 * ry * Math.sin(radOffset + overlap * 0.5),
        ]);
    }
    return [allPoints, corePoints];
}
function _arc(increment, cx, cy, rx, ry, strt, stp, offset, o) {
    const radOffset = strt + _offsetOpt(0.1, o);
    const points = [];
    points.push([
        _offsetOpt(offset, o) + cx + 0.9 * rx * Math.cos(radOffset - increment),
        _offsetOpt(offset, o) + cy + 0.9 * ry * Math.sin(radOffset - increment),
    ]);
    for (let angle = radOffset; angle <= stp; angle = angle + increment) {
        points.push([
            _offsetOpt(offset, o) + cx + rx * Math.cos(angle),
            _offsetOpt(offset, o) + cy + ry * Math.sin(angle),
        ]);
    }
    points.push([
        cx + rx * Math.cos(stp),
        cy + ry * Math.sin(stp),
    ]);
    points.push([
        cx + rx * Math.cos(stp),
        cy + ry * Math.sin(stp),
    ]);
    return _curve(points, null, o);
}
function _bezierTo(x1, y1, x2, y2, x, y, current, o) {
    const ops = [];
    const ros = [o.maxRandomnessOffset || 1, (o.maxRandomnessOffset || 1) + 0.3];
    let f = [0, 0];
    const iterations = o.disableMultiStroke ? 1 : 2;
    const preserveVertices = o.preserveVertices;
    for (let i = 0; i < iterations; i++) {
        if (i === 0) {
            ops.push({ op: 'move', data: [current[0], current[1]] });
        }
        else {
            ops.push({ op: 'move', data: [current[0] + (preserveVertices ? 0 : _offsetOpt(ros[0], o)), current[1] + (preserveVertices ? 0 : _offsetOpt(ros[0], o))] });
        }
        f = preserveVertices ? [x, y] : [x + _offsetOpt(ros[i], o), y + _offsetOpt(ros[i], o)];
        ops.push({
            op: 'bcurveTo',
            data: [
                x1 + _offsetOpt(ros[i], o), y1 + _offsetOpt(ros[i], o),
                x2 + _offsetOpt(ros[i], o), y2 + _offsetOpt(ros[i], o),
                f[0], f[1],
            ],
        });
    }
    return ops;
}


/***/ }),

/***/ "./node_modules/roughjs/bin/rough.js":
/*!*******************************************!*\
  !*** ./node_modules/roughjs/bin/rough.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _canvas__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./canvas */ "./node_modules/roughjs/bin/canvas.js");
/* harmony import */ var _generator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./generator */ "./node_modules/roughjs/bin/generator.js");
/* harmony import */ var _svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./svg */ "./node_modules/roughjs/bin/svg.js");



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
    canvas(canvas, config) {
        return new _canvas__WEBPACK_IMPORTED_MODULE_0__.RoughCanvas(canvas, config);
    },
    svg(svg, config) {
        return new _svg__WEBPACK_IMPORTED_MODULE_2__.RoughSVG(svg, config);
    },
    generator(config) {
        return new _generator__WEBPACK_IMPORTED_MODULE_1__.RoughGenerator(config);
    },
    newSeed() {
        return _generator__WEBPACK_IMPORTED_MODULE_1__.RoughGenerator.newSeed();
    },
});


/***/ }),

/***/ "./node_modules/roughjs/bin/svg.js":
/*!*****************************************!*\
  !*** ./node_modules/roughjs/bin/svg.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RoughSVG": () => (/* binding */ RoughSVG)
/* harmony export */ });
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core */ "./node_modules/roughjs/bin/core.js");
/* harmony import */ var _generator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./generator */ "./node_modules/roughjs/bin/generator.js");


class RoughSVG {
    constructor(svg, config) {
        this.svg = svg;
        this.gen = new _generator__WEBPACK_IMPORTED_MODULE_1__.RoughGenerator(config);
    }
    draw(drawable) {
        const sets = drawable.sets || [];
        const o = drawable.options || this.getDefaultOptions();
        const doc = this.svg.ownerDocument || window.document;
        const g = doc.createElementNS(_core__WEBPACK_IMPORTED_MODULE_0__.SVGNS, 'g');
        const precision = drawable.options.fixedDecimalPlaceDigits;
        for (const drawing of sets) {
            let path = null;
            switch (drawing.type) {
                case 'path': {
                    path = doc.createElementNS(_core__WEBPACK_IMPORTED_MODULE_0__.SVGNS, 'path');
                    path.setAttribute('d', this.opsToPath(drawing, precision));
                    path.setAttribute('stroke', o.stroke);
                    path.setAttribute('stroke-width', o.strokeWidth + '');
                    path.setAttribute('fill', 'none');
                    if (o.strokeLineDash) {
                        path.setAttribute('stroke-dasharray', o.strokeLineDash.join(' ').trim());
                    }
                    if (o.strokeLineDashOffset) {
                        path.setAttribute('stroke-dashoffset', `${o.strokeLineDashOffset}`);
                    }
                    break;
                }
                case 'fillPath': {
                    path = doc.createElementNS(_core__WEBPACK_IMPORTED_MODULE_0__.SVGNS, 'path');
                    path.setAttribute('d', this.opsToPath(drawing, precision));
                    path.setAttribute('stroke', 'none');
                    path.setAttribute('stroke-width', '0');
                    path.setAttribute('fill', o.fill || '');
                    if (drawable.shape === 'curve' || drawable.shape === 'polygon') {
                        path.setAttribute('fill-rule', 'evenodd');
                    }
                    break;
                }
                case 'fillSketch': {
                    path = this.fillSketch(doc, drawing, o);
                    break;
                }
            }
            if (path) {
                g.appendChild(path);
            }
        }
        return g;
    }
    fillSketch(doc, drawing, o) {
        let fweight = o.fillWeight;
        if (fweight < 0) {
            fweight = o.strokeWidth / 2;
        }
        const path = doc.createElementNS(_core__WEBPACK_IMPORTED_MODULE_0__.SVGNS, 'path');
        path.setAttribute('d', this.opsToPath(drawing, o.fixedDecimalPlaceDigits));
        path.setAttribute('stroke', o.fill || '');
        path.setAttribute('stroke-width', fweight + '');
        path.setAttribute('fill', 'none');
        if (o.fillLineDash) {
            path.setAttribute('stroke-dasharray', o.fillLineDash.join(' ').trim());
        }
        if (o.fillLineDashOffset) {
            path.setAttribute('stroke-dashoffset', `${o.fillLineDashOffset}`);
        }
        return path;
    }
    get generator() {
        return this.gen;
    }
    getDefaultOptions() {
        return this.gen.defaultOptions;
    }
    opsToPath(drawing, fixedDecimalPlaceDigits) {
        return this.gen.opsToPath(drawing, fixedDecimalPlaceDigits);
    }
    line(x1, y1, x2, y2, options) {
        const d = this.gen.line(x1, y1, x2, y2, options);
        return this.draw(d);
    }
    rectangle(x, y, width, height, options) {
        const d = this.gen.rectangle(x, y, width, height, options);
        return this.draw(d);
    }
    ellipse(x, y, width, height, options) {
        const d = this.gen.ellipse(x, y, width, height, options);
        return this.draw(d);
    }
    circle(x, y, diameter, options) {
        const d = this.gen.circle(x, y, diameter, options);
        return this.draw(d);
    }
    linearPath(points, options) {
        const d = this.gen.linearPath(points, options);
        return this.draw(d);
    }
    polygon(points, options) {
        const d = this.gen.polygon(points, options);
        return this.draw(d);
    }
    arc(x, y, width, height, start, stop, closed = false, options) {
        const d = this.gen.arc(x, y, width, height, start, stop, closed, options);
        return this.draw(d);
    }
    curve(points, options) {
        const d = this.gen.curve(points, options);
        return this.draw(d);
    }
    path(d, options) {
        const drawing = this.gen.path(d, options);
        return this.draw(drawing);
    }
}


/***/ }),

/***/ "./node_modules/@stitches/stringify/dist/index.mjs":
/*!*********************************************************!*\
  !*** ./node_modules/@stitches/stringify/dist/index.mjs ***!
  \*********************************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "stringify": () => (/* binding */ r)
/* harmony export */ });
var e=e=>e.includes("-")?e:e.replace(/[A-Z]/g,(e=>"-"+e.toLowerCase())),t=(e,t)=>e.reduce(((e,o)=>(e.push(...t.map((e=>e.includes("&")?e.replace(/&/g,/[ +>|~]/.test(o)&&/&.*&/.test(e)?`:is(${o})`:o):o+" "+e))),e)),[]),{isArray:o}=Array,{prototype:{toString:s}}=Object,n=/\s*,\s*(?![^()]*\))/,r=(r,c)=>{const l=new WeakSet,a=(r,i,d,p,h)=>{let f="";e:for(const u in r){const g=64===u.charCodeAt(0);for(const y of g&&o(r[u])?r[u]:[r[u]]){if(c&&(u!==p||y!==h)){const e=c(u,y,r);if(null!==e){f+="object"==typeof e&&e?a(e,i,d,u,y):null==e?"":e;continue e}}if("object"==typeof y&&y&&y.toString===s){l.has(i)&&(l.delete(i),f+="}");const e=Object(u),o=g?i:i.length?t(i,u.split(n)):u.split(n);f+=a(y,o,g?d.concat(e):d),l.has(e)&&(l.delete(e),f+="}"),l.has(o)&&(l.delete(o),f+="}")}else{for(let e=0;e<d.length;++e)l.has(d[e])||(l.add(d[e]),f+=d[e]+"{");i.length&&!l.has(i)&&(l.add(i),f+=i+"{"),f+=(g?u+" ":e(u)+":")+String(y)+";"}}}return f};return a(r,[],[])};
//# sourceMappingUrl=index.map

/***/ }),

/***/ "./node_modules/zod/lib/index.mjs":
/*!****************************************!*\
  !*** ./node_modules/zod/lib/index.mjs ***!
  \****************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BRAND": () => (/* binding */ BRAND),
/* harmony export */   "DIRTY": () => (/* binding */ DIRTY),
/* harmony export */   "EMPTY_PATH": () => (/* binding */ EMPTY_PATH),
/* harmony export */   "INVALID": () => (/* binding */ INVALID),
/* harmony export */   "NEVER": () => (/* binding */ NEVER),
/* harmony export */   "OK": () => (/* binding */ OK),
/* harmony export */   "ParseStatus": () => (/* binding */ ParseStatus),
/* harmony export */   "Schema": () => (/* binding */ ZodType),
/* harmony export */   "ZodAny": () => (/* binding */ ZodAny),
/* harmony export */   "ZodArray": () => (/* binding */ ZodArray),
/* harmony export */   "ZodBigInt": () => (/* binding */ ZodBigInt),
/* harmony export */   "ZodBoolean": () => (/* binding */ ZodBoolean),
/* harmony export */   "ZodBranded": () => (/* binding */ ZodBranded),
/* harmony export */   "ZodCatch": () => (/* binding */ ZodCatch),
/* harmony export */   "ZodDate": () => (/* binding */ ZodDate),
/* harmony export */   "ZodDefault": () => (/* binding */ ZodDefault),
/* harmony export */   "ZodDiscriminatedUnion": () => (/* binding */ ZodDiscriminatedUnion),
/* harmony export */   "ZodEffects": () => (/* binding */ ZodEffects),
/* harmony export */   "ZodEnum": () => (/* binding */ ZodEnum),
/* harmony export */   "ZodError": () => (/* binding */ ZodError),
/* harmony export */   "ZodFirstPartyTypeKind": () => (/* binding */ ZodFirstPartyTypeKind),
/* harmony export */   "ZodFunction": () => (/* binding */ ZodFunction),
/* harmony export */   "ZodIntersection": () => (/* binding */ ZodIntersection),
/* harmony export */   "ZodIssueCode": () => (/* binding */ ZodIssueCode),
/* harmony export */   "ZodLazy": () => (/* binding */ ZodLazy),
/* harmony export */   "ZodLiteral": () => (/* binding */ ZodLiteral),
/* harmony export */   "ZodMap": () => (/* binding */ ZodMap),
/* harmony export */   "ZodNaN": () => (/* binding */ ZodNaN),
/* harmony export */   "ZodNativeEnum": () => (/* binding */ ZodNativeEnum),
/* harmony export */   "ZodNever": () => (/* binding */ ZodNever),
/* harmony export */   "ZodNull": () => (/* binding */ ZodNull),
/* harmony export */   "ZodNullable": () => (/* binding */ ZodNullable),
/* harmony export */   "ZodNumber": () => (/* binding */ ZodNumber),
/* harmony export */   "ZodObject": () => (/* binding */ ZodObject),
/* harmony export */   "ZodOptional": () => (/* binding */ ZodOptional),
/* harmony export */   "ZodParsedType": () => (/* binding */ ZodParsedType),
/* harmony export */   "ZodPipeline": () => (/* binding */ ZodPipeline),
/* harmony export */   "ZodPromise": () => (/* binding */ ZodPromise),
/* harmony export */   "ZodRecord": () => (/* binding */ ZodRecord),
/* harmony export */   "ZodSchema": () => (/* binding */ ZodType),
/* harmony export */   "ZodSet": () => (/* binding */ ZodSet),
/* harmony export */   "ZodString": () => (/* binding */ ZodString),
/* harmony export */   "ZodSymbol": () => (/* binding */ ZodSymbol),
/* harmony export */   "ZodTransformer": () => (/* binding */ ZodEffects),
/* harmony export */   "ZodTuple": () => (/* binding */ ZodTuple),
/* harmony export */   "ZodType": () => (/* binding */ ZodType),
/* harmony export */   "ZodUndefined": () => (/* binding */ ZodUndefined),
/* harmony export */   "ZodUnion": () => (/* binding */ ZodUnion),
/* harmony export */   "ZodUnknown": () => (/* binding */ ZodUnknown),
/* harmony export */   "ZodVoid": () => (/* binding */ ZodVoid),
/* harmony export */   "addIssueToContext": () => (/* binding */ addIssueToContext),
/* harmony export */   "any": () => (/* binding */ anyType),
/* harmony export */   "array": () => (/* binding */ arrayType),
/* harmony export */   "bigint": () => (/* binding */ bigIntType),
/* harmony export */   "boolean": () => (/* binding */ booleanType),
/* harmony export */   "coerce": () => (/* binding */ coerce),
/* harmony export */   "custom": () => (/* binding */ custom),
/* harmony export */   "date": () => (/* binding */ dateType),
/* harmony export */   "default": () => (/* binding */ z),
/* harmony export */   "defaultErrorMap": () => (/* binding */ errorMap),
/* harmony export */   "discriminatedUnion": () => (/* binding */ discriminatedUnionType),
/* harmony export */   "effect": () => (/* binding */ effectsType),
/* harmony export */   "enum": () => (/* binding */ enumType),
/* harmony export */   "function": () => (/* binding */ functionType),
/* harmony export */   "getErrorMap": () => (/* binding */ getErrorMap),
/* harmony export */   "getParsedType": () => (/* binding */ getParsedType),
/* harmony export */   "instanceof": () => (/* binding */ instanceOfType),
/* harmony export */   "intersection": () => (/* binding */ intersectionType),
/* harmony export */   "isAborted": () => (/* binding */ isAborted),
/* harmony export */   "isAsync": () => (/* binding */ isAsync),
/* harmony export */   "isDirty": () => (/* binding */ isDirty),
/* harmony export */   "isValid": () => (/* binding */ isValid),
/* harmony export */   "late": () => (/* binding */ late),
/* harmony export */   "lazy": () => (/* binding */ lazyType),
/* harmony export */   "literal": () => (/* binding */ literalType),
/* harmony export */   "makeIssue": () => (/* binding */ makeIssue),
/* harmony export */   "map": () => (/* binding */ mapType),
/* harmony export */   "nan": () => (/* binding */ nanType),
/* harmony export */   "nativeEnum": () => (/* binding */ nativeEnumType),
/* harmony export */   "never": () => (/* binding */ neverType),
/* harmony export */   "null": () => (/* binding */ nullType),
/* harmony export */   "nullable": () => (/* binding */ nullableType),
/* harmony export */   "number": () => (/* binding */ numberType),
/* harmony export */   "object": () => (/* binding */ objectType),
/* harmony export */   "objectUtil": () => (/* binding */ objectUtil),
/* harmony export */   "oboolean": () => (/* binding */ oboolean),
/* harmony export */   "onumber": () => (/* binding */ onumber),
/* harmony export */   "optional": () => (/* binding */ optionalType),
/* harmony export */   "ostring": () => (/* binding */ ostring),
/* harmony export */   "pipeline": () => (/* binding */ pipelineType),
/* harmony export */   "preprocess": () => (/* binding */ preprocessType),
/* harmony export */   "promise": () => (/* binding */ promiseType),
/* harmony export */   "quotelessJson": () => (/* binding */ quotelessJson),
/* harmony export */   "record": () => (/* binding */ recordType),
/* harmony export */   "set": () => (/* binding */ setType),
/* harmony export */   "setErrorMap": () => (/* binding */ setErrorMap),
/* harmony export */   "strictObject": () => (/* binding */ strictObjectType),
/* harmony export */   "string": () => (/* binding */ stringType),
/* harmony export */   "symbol": () => (/* binding */ symbolType),
/* harmony export */   "transformer": () => (/* binding */ effectsType),
/* harmony export */   "tuple": () => (/* binding */ tupleType),
/* harmony export */   "undefined": () => (/* binding */ undefinedType),
/* harmony export */   "union": () => (/* binding */ unionType),
/* harmony export */   "unknown": () => (/* binding */ unknownType),
/* harmony export */   "util": () => (/* binding */ util),
/* harmony export */   "void": () => (/* binding */ voidType),
/* harmony export */   "z": () => (/* binding */ z)
/* harmony export */ });
var util;
(function (util) {
    util.assertEqual = (val) => val;
    function assertIs(_arg) { }
    util.assertIs = assertIs;
    function assertNever(_x) {
        throw new Error();
    }
    util.assertNever = assertNever;
    util.arrayToEnum = (items) => {
        const obj = {};
        for (const item of items) {
            obj[item] = item;
        }
        return obj;
    };
    util.getValidEnumValues = (obj) => {
        const validKeys = util.objectKeys(obj).filter((k) => typeof obj[obj[k]] !== "number");
        const filtered = {};
        for (const k of validKeys) {
            filtered[k] = obj[k];
        }
        return util.objectValues(filtered);
    };
    util.objectValues = (obj) => {
        return util.objectKeys(obj).map(function (e) {
            return obj[e];
        });
    };
    util.objectKeys = typeof Object.keys === "function" // eslint-disable-line ban/ban
        ? (obj) => Object.keys(obj) // eslint-disable-line ban/ban
        : (object) => {
            const keys = [];
            for (const key in object) {
                if (Object.prototype.hasOwnProperty.call(object, key)) {
                    keys.push(key);
                }
            }
            return keys;
        };
    util.find = (arr, checker) => {
        for (const item of arr) {
            if (checker(item))
                return item;
        }
        return undefined;
    };
    util.isInteger = typeof Number.isInteger === "function"
        ? (val) => Number.isInteger(val) // eslint-disable-line ban/ban
        : (val) => typeof val === "number" && isFinite(val) && Math.floor(val) === val;
    function joinValues(array, separator = " | ") {
        return array
            .map((val) => (typeof val === "string" ? `'${val}'` : val))
            .join(separator);
    }
    util.joinValues = joinValues;
    util.jsonStringifyReplacer = (_, value) => {
        if (typeof value === "bigint") {
            return value.toString();
        }
        return value;
    };
})(util || (util = {}));
var objectUtil;
(function (objectUtil) {
    objectUtil.mergeShapes = (first, second) => {
        return {
            ...first,
            ...second, // second overwrites first
        };
    };
})(objectUtil || (objectUtil = {}));
const ZodParsedType = util.arrayToEnum([
    "string",
    "nan",
    "number",
    "integer",
    "float",
    "boolean",
    "date",
    "bigint",
    "symbol",
    "function",
    "undefined",
    "null",
    "array",
    "object",
    "unknown",
    "promise",
    "void",
    "never",
    "map",
    "set",
]);
const getParsedType = (data) => {
    const t = typeof data;
    switch (t) {
        case "undefined":
            return ZodParsedType.undefined;
        case "string":
            return ZodParsedType.string;
        case "number":
            return isNaN(data) ? ZodParsedType.nan : ZodParsedType.number;
        case "boolean":
            return ZodParsedType.boolean;
        case "function":
            return ZodParsedType.function;
        case "bigint":
            return ZodParsedType.bigint;
        case "symbol":
            return ZodParsedType.symbol;
        case "object":
            if (Array.isArray(data)) {
                return ZodParsedType.array;
            }
            if (data === null) {
                return ZodParsedType.null;
            }
            if (data.then &&
                typeof data.then === "function" &&
                data.catch &&
                typeof data.catch === "function") {
                return ZodParsedType.promise;
            }
            if (typeof Map !== "undefined" && data instanceof Map) {
                return ZodParsedType.map;
            }
            if (typeof Set !== "undefined" && data instanceof Set) {
                return ZodParsedType.set;
            }
            if (typeof Date !== "undefined" && data instanceof Date) {
                return ZodParsedType.date;
            }
            return ZodParsedType.object;
        default:
            return ZodParsedType.unknown;
    }
};

const ZodIssueCode = util.arrayToEnum([
    "invalid_type",
    "invalid_literal",
    "custom",
    "invalid_union",
    "invalid_union_discriminator",
    "invalid_enum_value",
    "unrecognized_keys",
    "invalid_arguments",
    "invalid_return_type",
    "invalid_date",
    "invalid_string",
    "too_small",
    "too_big",
    "invalid_intersection_types",
    "not_multiple_of",
    "not_finite",
]);
const quotelessJson = (obj) => {
    const json = JSON.stringify(obj, null, 2);
    return json.replace(/"([^"]+)":/g, "$1:");
};
class ZodError extends Error {
    constructor(issues) {
        super();
        this.issues = [];
        this.addIssue = (sub) => {
            this.issues = [...this.issues, sub];
        };
        this.addIssues = (subs = []) => {
            this.issues = [...this.issues, ...subs];
        };
        const actualProto = new.target.prototype;
        if (Object.setPrototypeOf) {
            // eslint-disable-next-line ban/ban
            Object.setPrototypeOf(this, actualProto);
        }
        else {
            this.__proto__ = actualProto;
        }
        this.name = "ZodError";
        this.issues = issues;
    }
    get errors() {
        return this.issues;
    }
    format(_mapper) {
        const mapper = _mapper ||
            function (issue) {
                return issue.message;
            };
        const fieldErrors = { _errors: [] };
        const processError = (error) => {
            for (const issue of error.issues) {
                if (issue.code === "invalid_union") {
                    issue.unionErrors.map(processError);
                }
                else if (issue.code === "invalid_return_type") {
                    processError(issue.returnTypeError);
                }
                else if (issue.code === "invalid_arguments") {
                    processError(issue.argumentsError);
                }
                else if (issue.path.length === 0) {
                    fieldErrors._errors.push(mapper(issue));
                }
                else {
                    let curr = fieldErrors;
                    let i = 0;
                    while (i < issue.path.length) {
                        const el = issue.path[i];
                        const terminal = i === issue.path.length - 1;
                        if (!terminal) {
                            curr[el] = curr[el] || { _errors: [] };
                            // if (typeof el === "string") {
                            //   curr[el] = curr[el] || { _errors: [] };
                            // } else if (typeof el === "number") {
                            //   const errorArray: any = [];
                            //   errorArray._errors = [];
                            //   curr[el] = curr[el] || errorArray;
                            // }
                        }
                        else {
                            curr[el] = curr[el] || { _errors: [] };
                            curr[el]._errors.push(mapper(issue));
                        }
                        curr = curr[el];
                        i++;
                    }
                }
            }
        };
        processError(this);
        return fieldErrors;
    }
    toString() {
        return this.message;
    }
    get message() {
        return JSON.stringify(this.issues, util.jsonStringifyReplacer, 2);
    }
    get isEmpty() {
        return this.issues.length === 0;
    }
    flatten(mapper = (issue) => issue.message) {
        const fieldErrors = {};
        const formErrors = [];
        for (const sub of this.issues) {
            if (sub.path.length > 0) {
                fieldErrors[sub.path[0]] = fieldErrors[sub.path[0]] || [];
                fieldErrors[sub.path[0]].push(mapper(sub));
            }
            else {
                formErrors.push(mapper(sub));
            }
        }
        return { formErrors, fieldErrors };
    }
    get formErrors() {
        return this.flatten();
    }
}
ZodError.create = (issues) => {
    const error = new ZodError(issues);
    return error;
};

const errorMap = (issue, _ctx) => {
    let message;
    switch (issue.code) {
        case ZodIssueCode.invalid_type:
            if (issue.received === ZodParsedType.undefined) {
                message = "Required";
            }
            else {
                message = `Expected ${issue.expected}, received ${issue.received}`;
            }
            break;
        case ZodIssueCode.invalid_literal:
            message = `Invalid literal value, expected ${JSON.stringify(issue.expected, util.jsonStringifyReplacer)}`;
            break;
        case ZodIssueCode.unrecognized_keys:
            message = `Unrecognized key(s) in object: ${util.joinValues(issue.keys, ", ")}`;
            break;
        case ZodIssueCode.invalid_union:
            message = `Invalid input`;
            break;
        case ZodIssueCode.invalid_union_discriminator:
            message = `Invalid discriminator value. Expected ${util.joinValues(issue.options)}`;
            break;
        case ZodIssueCode.invalid_enum_value:
            message = `Invalid enum value. Expected ${util.joinValues(issue.options)}, received '${issue.received}'`;
            break;
        case ZodIssueCode.invalid_arguments:
            message = `Invalid function arguments`;
            break;
        case ZodIssueCode.invalid_return_type:
            message = `Invalid function return type`;
            break;
        case ZodIssueCode.invalid_date:
            message = `Invalid date`;
            break;
        case ZodIssueCode.invalid_string:
            if (typeof issue.validation === "object") {
                if ("includes" in issue.validation) {
                    message = `Invalid input: must include "${issue.validation.includes}"`;
                    if (typeof issue.validation.position === "number") {
                        message = `${message} at one or more positions greater than or equal to ${issue.validation.position}`;
                    }
                }
                else if ("startsWith" in issue.validation) {
                    message = `Invalid input: must start with "${issue.validation.startsWith}"`;
                }
                else if ("endsWith" in issue.validation) {
                    message = `Invalid input: must end with "${issue.validation.endsWith}"`;
                }
                else {
                    util.assertNever(issue.validation);
                }
            }
            else if (issue.validation !== "regex") {
                message = `Invalid ${issue.validation}`;
            }
            else {
                message = "Invalid";
            }
            break;
        case ZodIssueCode.too_small:
            if (issue.type === "array")
                message = `Array must contain ${issue.exact ? "exactly" : issue.inclusive ? `at least` : `more than`} ${issue.minimum} element(s)`;
            else if (issue.type === "string")
                message = `String must contain ${issue.exact ? "exactly" : issue.inclusive ? `at least` : `over`} ${issue.minimum} character(s)`;
            else if (issue.type === "number")
                message = `Number must be ${issue.exact
                    ? `exactly equal to `
                    : issue.inclusive
                        ? `greater than or equal to `
                        : `greater than `}${issue.minimum}`;
            else if (issue.type === "date")
                message = `Date must be ${issue.exact
                    ? `exactly equal to `
                    : issue.inclusive
                        ? `greater than or equal to `
                        : `greater than `}${new Date(Number(issue.minimum))}`;
            else
                message = "Invalid input";
            break;
        case ZodIssueCode.too_big:
            if (issue.type === "array")
                message = `Array must contain ${issue.exact ? `exactly` : issue.inclusive ? `at most` : `less than`} ${issue.maximum} element(s)`;
            else if (issue.type === "string")
                message = `String must contain ${issue.exact ? `exactly` : issue.inclusive ? `at most` : `under`} ${issue.maximum} character(s)`;
            else if (issue.type === "number")
                message = `Number must be ${issue.exact
                    ? `exactly`
                    : issue.inclusive
                        ? `less than or equal to`
                        : `less than`} ${issue.maximum}`;
            else if (issue.type === "bigint")
                message = `BigInt must be ${issue.exact
                    ? `exactly`
                    : issue.inclusive
                        ? `less than or equal to`
                        : `less than`} ${issue.maximum}`;
            else if (issue.type === "date")
                message = `Date must be ${issue.exact
                    ? `exactly`
                    : issue.inclusive
                        ? `smaller than or equal to`
                        : `smaller than`} ${new Date(Number(issue.maximum))}`;
            else
                message = "Invalid input";
            break;
        case ZodIssueCode.custom:
            message = `Invalid input`;
            break;
        case ZodIssueCode.invalid_intersection_types:
            message = `Intersection results could not be merged`;
            break;
        case ZodIssueCode.not_multiple_of:
            message = `Number must be a multiple of ${issue.multipleOf}`;
            break;
        case ZodIssueCode.not_finite:
            message = "Number must be finite";
            break;
        default:
            message = _ctx.defaultError;
            util.assertNever(issue);
    }
    return { message };
};

let overrideErrorMap = errorMap;
function setErrorMap(map) {
    overrideErrorMap = map;
}
function getErrorMap() {
    return overrideErrorMap;
}

const makeIssue = (params) => {
    const { data, path, errorMaps, issueData } = params;
    const fullPath = [...path, ...(issueData.path || [])];
    const fullIssue = {
        ...issueData,
        path: fullPath,
    };
    let errorMessage = "";
    const maps = errorMaps
        .filter((m) => !!m)
        .slice()
        .reverse();
    for (const map of maps) {
        errorMessage = map(fullIssue, { data, defaultError: errorMessage }).message;
    }
    return {
        ...issueData,
        path: fullPath,
        message: issueData.message || errorMessage,
    };
};
const EMPTY_PATH = [];
function addIssueToContext(ctx, issueData) {
    const issue = makeIssue({
        issueData: issueData,
        data: ctx.data,
        path: ctx.path,
        errorMaps: [
            ctx.common.contextualErrorMap,
            ctx.schemaErrorMap,
            getErrorMap(),
            errorMap, // then global default map
        ].filter((x) => !!x),
    });
    ctx.common.issues.push(issue);
}
class ParseStatus {
    constructor() {
        this.value = "valid";
    }
    dirty() {
        if (this.value === "valid")
            this.value = "dirty";
    }
    abort() {
        if (this.value !== "aborted")
            this.value = "aborted";
    }
    static mergeArray(status, results) {
        const arrayValue = [];
        for (const s of results) {
            if (s.status === "aborted")
                return INVALID;
            if (s.status === "dirty")
                status.dirty();
            arrayValue.push(s.value);
        }
        return { status: status.value, value: arrayValue };
    }
    static async mergeObjectAsync(status, pairs) {
        const syncPairs = [];
        for (const pair of pairs) {
            syncPairs.push({
                key: await pair.key,
                value: await pair.value,
            });
        }
        return ParseStatus.mergeObjectSync(status, syncPairs);
    }
    static mergeObjectSync(status, pairs) {
        const finalObject = {};
        for (const pair of pairs) {
            const { key, value } = pair;
            if (key.status === "aborted")
                return INVALID;
            if (value.status === "aborted")
                return INVALID;
            if (key.status === "dirty")
                status.dirty();
            if (value.status === "dirty")
                status.dirty();
            if (typeof value.value !== "undefined" || pair.alwaysSet) {
                finalObject[key.value] = value.value;
            }
        }
        return { status: status.value, value: finalObject };
    }
}
const INVALID = Object.freeze({
    status: "aborted",
});
const DIRTY = (value) => ({ status: "dirty", value });
const OK = (value) => ({ status: "valid", value });
const isAborted = (x) => x.status === "aborted";
const isDirty = (x) => x.status === "dirty";
const isValid = (x) => x.status === "valid";
const isAsync = (x) => typeof Promise !== "undefined" && x instanceof Promise;

var errorUtil;
(function (errorUtil) {
    errorUtil.errToObj = (message) => typeof message === "string" ? { message } : message || {};
    errorUtil.toString = (message) => typeof message === "string" ? message : message === null || message === void 0 ? void 0 : message.message;
})(errorUtil || (errorUtil = {}));

class ParseInputLazyPath {
    constructor(parent, value, path, key) {
        this._cachedPath = [];
        this.parent = parent;
        this.data = value;
        this._path = path;
        this._key = key;
    }
    get path() {
        if (!this._cachedPath.length) {
            if (this._key instanceof Array) {
                this._cachedPath.push(...this._path, ...this._key);
            }
            else {
                this._cachedPath.push(...this._path, this._key);
            }
        }
        return this._cachedPath;
    }
}
const handleResult = (ctx, result) => {
    if (isValid(result)) {
        return { success: true, data: result.value };
    }
    else {
        if (!ctx.common.issues.length) {
            throw new Error("Validation failed but no issues detected.");
        }
        return {
            success: false,
            get error() {
                if (this._error)
                    return this._error;
                const error = new ZodError(ctx.common.issues);
                this._error = error;
                return this._error;
            },
        };
    }
};
function processCreateParams(params) {
    if (!params)
        return {};
    const { errorMap, invalid_type_error, required_error, description } = params;
    if (errorMap && (invalid_type_error || required_error)) {
        throw new Error(`Can't use "invalid_type_error" or "required_error" in conjunction with custom error map.`);
    }
    if (errorMap)
        return { errorMap: errorMap, description };
    const customMap = (iss, ctx) => {
        if (iss.code !== "invalid_type")
            return { message: ctx.defaultError };
        if (typeof ctx.data === "undefined") {
            return { message: required_error !== null && required_error !== void 0 ? required_error : ctx.defaultError };
        }
        return { message: invalid_type_error !== null && invalid_type_error !== void 0 ? invalid_type_error : ctx.defaultError };
    };
    return { errorMap: customMap, description };
}
class ZodType {
    constructor(def) {
        /** Alias of safeParseAsync */
        this.spa = this.safeParseAsync;
        this._def = def;
        this.parse = this.parse.bind(this);
        this.safeParse = this.safeParse.bind(this);
        this.parseAsync = this.parseAsync.bind(this);
        this.safeParseAsync = this.safeParseAsync.bind(this);
        this.spa = this.spa.bind(this);
        this.refine = this.refine.bind(this);
        this.refinement = this.refinement.bind(this);
        this.superRefine = this.superRefine.bind(this);
        this.optional = this.optional.bind(this);
        this.nullable = this.nullable.bind(this);
        this.nullish = this.nullish.bind(this);
        this.array = this.array.bind(this);
        this.promise = this.promise.bind(this);
        this.or = this.or.bind(this);
        this.and = this.and.bind(this);
        this.transform = this.transform.bind(this);
        this.brand = this.brand.bind(this);
        this.default = this.default.bind(this);
        this.catch = this.catch.bind(this);
        this.describe = this.describe.bind(this);
        this.pipe = this.pipe.bind(this);
        this.isNullable = this.isNullable.bind(this);
        this.isOptional = this.isOptional.bind(this);
    }
    get description() {
        return this._def.description;
    }
    _getType(input) {
        return getParsedType(input.data);
    }
    _getOrReturnCtx(input, ctx) {
        return (ctx || {
            common: input.parent.common,
            data: input.data,
            parsedType: getParsedType(input.data),
            schemaErrorMap: this._def.errorMap,
            path: input.path,
            parent: input.parent,
        });
    }
    _processInputParams(input) {
        return {
            status: new ParseStatus(),
            ctx: {
                common: input.parent.common,
                data: input.data,
                parsedType: getParsedType(input.data),
                schemaErrorMap: this._def.errorMap,
                path: input.path,
                parent: input.parent,
            },
        };
    }
    _parseSync(input) {
        const result = this._parse(input);
        if (isAsync(result)) {
            throw new Error("Synchronous parse encountered promise.");
        }
        return result;
    }
    _parseAsync(input) {
        const result = this._parse(input);
        return Promise.resolve(result);
    }
    parse(data, params) {
        const result = this.safeParse(data, params);
        if (result.success)
            return result.data;
        throw result.error;
    }
    safeParse(data, params) {
        var _a;
        const ctx = {
            common: {
                issues: [],
                async: (_a = params === null || params === void 0 ? void 0 : params.async) !== null && _a !== void 0 ? _a : false,
                contextualErrorMap: params === null || params === void 0 ? void 0 : params.errorMap,
            },
            path: (params === null || params === void 0 ? void 0 : params.path) || [],
            schemaErrorMap: this._def.errorMap,
            parent: null,
            data,
            parsedType: getParsedType(data),
        };
        const result = this._parseSync({ data, path: ctx.path, parent: ctx });
        return handleResult(ctx, result);
    }
    async parseAsync(data, params) {
        const result = await this.safeParseAsync(data, params);
        if (result.success)
            return result.data;
        throw result.error;
    }
    async safeParseAsync(data, params) {
        const ctx = {
            common: {
                issues: [],
                contextualErrorMap: params === null || params === void 0 ? void 0 : params.errorMap,
                async: true,
            },
            path: (params === null || params === void 0 ? void 0 : params.path) || [],
            schemaErrorMap: this._def.errorMap,
            parent: null,
            data,
            parsedType: getParsedType(data),
        };
        const maybeAsyncResult = this._parse({ data, path: ctx.path, parent: ctx });
        const result = await (isAsync(maybeAsyncResult)
            ? maybeAsyncResult
            : Promise.resolve(maybeAsyncResult));
        return handleResult(ctx, result);
    }
    refine(check, message) {
        const getIssueProperties = (val) => {
            if (typeof message === "string" || typeof message === "undefined") {
                return { message };
            }
            else if (typeof message === "function") {
                return message(val);
            }
            else {
                return message;
            }
        };
        return this._refinement((val, ctx) => {
            const result = check(val);
            const setError = () => ctx.addIssue({
                code: ZodIssueCode.custom,
                ...getIssueProperties(val),
            });
            if (typeof Promise !== "undefined" && result instanceof Promise) {
                return result.then((data) => {
                    if (!data) {
                        setError();
                        return false;
                    }
                    else {
                        return true;
                    }
                });
            }
            if (!result) {
                setError();
                return false;
            }
            else {
                return true;
            }
        });
    }
    refinement(check, refinementData) {
        return this._refinement((val, ctx) => {
            if (!check(val)) {
                ctx.addIssue(typeof refinementData === "function"
                    ? refinementData(val, ctx)
                    : refinementData);
                return false;
            }
            else {
                return true;
            }
        });
    }
    _refinement(refinement) {
        return new ZodEffects({
            schema: this,
            typeName: ZodFirstPartyTypeKind.ZodEffects,
            effect: { type: "refinement", refinement },
        });
    }
    superRefine(refinement) {
        return this._refinement(refinement);
    }
    optional() {
        return ZodOptional.create(this, this._def);
    }
    nullable() {
        return ZodNullable.create(this, this._def);
    }
    nullish() {
        return this.nullable().optional();
    }
    array() {
        return ZodArray.create(this, this._def);
    }
    promise() {
        return ZodPromise.create(this, this._def);
    }
    or(option) {
        return ZodUnion.create([this, option], this._def);
    }
    and(incoming) {
        return ZodIntersection.create(this, incoming, this._def);
    }
    transform(transform) {
        return new ZodEffects({
            ...processCreateParams(this._def),
            schema: this,
            typeName: ZodFirstPartyTypeKind.ZodEffects,
            effect: { type: "transform", transform },
        });
    }
    default(def) {
        const defaultValueFunc = typeof def === "function" ? def : () => def;
        return new ZodDefault({
            ...processCreateParams(this._def),
            innerType: this,
            defaultValue: defaultValueFunc,
            typeName: ZodFirstPartyTypeKind.ZodDefault,
        });
    }
    brand() {
        return new ZodBranded({
            typeName: ZodFirstPartyTypeKind.ZodBranded,
            type: this,
            ...processCreateParams(this._def),
        });
    }
    catch(def) {
        const catchValueFunc = typeof def === "function" ? def : () => def;
        return new ZodCatch({
            ...processCreateParams(this._def),
            innerType: this,
            catchValue: catchValueFunc,
            typeName: ZodFirstPartyTypeKind.ZodCatch,
        });
    }
    describe(description) {
        const This = this.constructor;
        return new This({
            ...this._def,
            description,
        });
    }
    pipe(target) {
        return ZodPipeline.create(this, target);
    }
    isOptional() {
        return this.safeParse(undefined).success;
    }
    isNullable() {
        return this.safeParse(null).success;
    }
}
const cuidRegex = /^c[^\s-]{8,}$/i;
const cuid2Regex = /^[a-z][a-z0-9]*$/;
const ulidRegex = /[0-9A-HJKMNP-TV-Z]{26}/;
const uuidRegex = /^([a-f0-9]{8}-[a-f0-9]{4}-[1-5][a-f0-9]{3}-[a-f0-9]{4}-[a-f0-9]{12}|00000000-0000-0000-0000-000000000000)$/i;
// from https://stackoverflow.com/a/46181/1550155
// old version: too slow, didn't support unicode
// const emailRegex = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;
//old email regex
// const emailRegex = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@((?!-)([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{1,})[^-<>()[\].,;:\s@"]$/i;
// eslint-disable-next-line
const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[(((25[0-5])|(2[0-4][0-9])|(1[0-9]{2})|([0-9]{1,2}))\.){3}((25[0-5])|(2[0-4][0-9])|(1[0-9]{2})|([0-9]{1,2}))\])|(\[IPv6:(([a-f0-9]{1,4}:){7}|::([a-f0-9]{1,4}:){0,6}|([a-f0-9]{1,4}:){1}:([a-f0-9]{1,4}:){0,5}|([a-f0-9]{1,4}:){2}:([a-f0-9]{1,4}:){0,4}|([a-f0-9]{1,4}:){3}:([a-f0-9]{1,4}:){0,3}|([a-f0-9]{1,4}:){4}:([a-f0-9]{1,4}:){0,2}|([a-f0-9]{1,4}:){5}:([a-f0-9]{1,4}:){0,1})([a-f0-9]{1,4}|(((25[0-5])|(2[0-4][0-9])|(1[0-9]{2})|([0-9]{1,2}))\.){3}((25[0-5])|(2[0-4][0-9])|(1[0-9]{2})|([0-9]{1,2})))\])|([A-Za-z0-9]([A-Za-z0-9-]*[A-Za-z0-9])*(\.[A-Za-z]{2,})+))$/;
// from https://thekevinscott.com/emojis-in-javascript/#writing-a-regular-expression
const emojiRegex = /^(\p{Extended_Pictographic}|\p{Emoji_Component})+$/u;
const ipv4Regex = /^(((25[0-5])|(2[0-4][0-9])|(1[0-9]{2})|([0-9]{1,2}))\.){3}((25[0-5])|(2[0-4][0-9])|(1[0-9]{2})|([0-9]{1,2}))$/;
const ipv6Regex = /^(([a-f0-9]{1,4}:){7}|::([a-f0-9]{1,4}:){0,6}|([a-f0-9]{1,4}:){1}:([a-f0-9]{1,4}:){0,5}|([a-f0-9]{1,4}:){2}:([a-f0-9]{1,4}:){0,4}|([a-f0-9]{1,4}:){3}:([a-f0-9]{1,4}:){0,3}|([a-f0-9]{1,4}:){4}:([a-f0-9]{1,4}:){0,2}|([a-f0-9]{1,4}:){5}:([a-f0-9]{1,4}:){0,1})([a-f0-9]{1,4}|(((25[0-5])|(2[0-4][0-9])|(1[0-9]{2})|([0-9]{1,2}))\.){3}((25[0-5])|(2[0-4][0-9])|(1[0-9]{2})|([0-9]{1,2})))$/;
// Adapted from https://stackoverflow.com/a/3143231
const datetimeRegex = (args) => {
    if (args.precision) {
        if (args.offset) {
            return new RegExp(`^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{${args.precision}}(([+-]\\d{2}(:?\\d{2})?)|Z)$`);
        }
        else {
            return new RegExp(`^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{${args.precision}}Z$`);
        }
    }
    else if (args.precision === 0) {
        if (args.offset) {
            return new RegExp(`^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}(([+-]\\d{2}(:?\\d{2})?)|Z)$`);
        }
        else {
            return new RegExp(`^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z$`);
        }
    }
    else {
        if (args.offset) {
            return new RegExp(`^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}(\\.\\d+)?(([+-]\\d{2}(:?\\d{2})?)|Z)$`);
        }
        else {
            return new RegExp(`^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}(\\.\\d+)?Z$`);
        }
    }
};
function isValidIP(ip, version) {
    if ((version === "v4" || !version) && ipv4Regex.test(ip)) {
        return true;
    }
    if ((version === "v6" || !version) && ipv6Regex.test(ip)) {
        return true;
    }
    return false;
}
class ZodString extends ZodType {
    constructor() {
        super(...arguments);
        this._regex = (regex, validation, message) => this.refinement((data) => regex.test(data), {
            validation,
            code: ZodIssueCode.invalid_string,
            ...errorUtil.errToObj(message),
        });
        /**
         * @deprecated Use z.string().min(1) instead.
         * @see {@link ZodString.min}
         */
        this.nonempty = (message) => this.min(1, errorUtil.errToObj(message));
        this.trim = () => new ZodString({
            ...this._def,
            checks: [...this._def.checks, { kind: "trim" }],
        });
        this.toLowerCase = () => new ZodString({
            ...this._def,
            checks: [...this._def.checks, { kind: "toLowerCase" }],
        });
        this.toUpperCase = () => new ZodString({
            ...this._def,
            checks: [...this._def.checks, { kind: "toUpperCase" }],
        });
    }
    _parse(input) {
        if (this._def.coerce) {
            input.data = String(input.data);
        }
        const parsedType = this._getType(input);
        if (parsedType !== ZodParsedType.string) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.string,
                received: ctx.parsedType,
            }
            //
            );
            return INVALID;
        }
        const status = new ParseStatus();
        let ctx = undefined;
        for (const check of this._def.checks) {
            if (check.kind === "min") {
                if (input.data.length < check.value) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.too_small,
                        minimum: check.value,
                        type: "string",
                        inclusive: true,
                        exact: false,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "max") {
                if (input.data.length > check.value) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.too_big,
                        maximum: check.value,
                        type: "string",
                        inclusive: true,
                        exact: false,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "length") {
                const tooBig = input.data.length > check.value;
                const tooSmall = input.data.length < check.value;
                if (tooBig || tooSmall) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    if (tooBig) {
                        addIssueToContext(ctx, {
                            code: ZodIssueCode.too_big,
                            maximum: check.value,
                            type: "string",
                            inclusive: true,
                            exact: true,
                            message: check.message,
                        });
                    }
                    else if (tooSmall) {
                        addIssueToContext(ctx, {
                            code: ZodIssueCode.too_small,
                            minimum: check.value,
                            type: "string",
                            inclusive: true,
                            exact: true,
                            message: check.message,
                        });
                    }
                    status.dirty();
                }
            }
            else if (check.kind === "email") {
                if (!emailRegex.test(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        validation: "email",
                        code: ZodIssueCode.invalid_string,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "emoji") {
                if (!emojiRegex.test(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        validation: "emoji",
                        code: ZodIssueCode.invalid_string,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "uuid") {
                if (!uuidRegex.test(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        validation: "uuid",
                        code: ZodIssueCode.invalid_string,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "cuid") {
                if (!cuidRegex.test(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        validation: "cuid",
                        code: ZodIssueCode.invalid_string,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "cuid2") {
                if (!cuid2Regex.test(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        validation: "cuid2",
                        code: ZodIssueCode.invalid_string,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "ulid") {
                if (!ulidRegex.test(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        validation: "ulid",
                        code: ZodIssueCode.invalid_string,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "url") {
                try {
                    new URL(input.data);
                }
                catch (_a) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        validation: "url",
                        code: ZodIssueCode.invalid_string,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "regex") {
                check.regex.lastIndex = 0;
                const testResult = check.regex.test(input.data);
                if (!testResult) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        validation: "regex",
                        code: ZodIssueCode.invalid_string,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "trim") {
                input.data = input.data.trim();
            }
            else if (check.kind === "includes") {
                if (!input.data.includes(check.value, check.position)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.invalid_string,
                        validation: { includes: check.value, position: check.position },
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "toLowerCase") {
                input.data = input.data.toLowerCase();
            }
            else if (check.kind === "toUpperCase") {
                input.data = input.data.toUpperCase();
            }
            else if (check.kind === "startsWith") {
                if (!input.data.startsWith(check.value)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.invalid_string,
                        validation: { startsWith: check.value },
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "endsWith") {
                if (!input.data.endsWith(check.value)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.invalid_string,
                        validation: { endsWith: check.value },
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "datetime") {
                const regex = datetimeRegex(check);
                if (!regex.test(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.invalid_string,
                        validation: "datetime",
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "ip") {
                if (!isValidIP(input.data, check.version)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        validation: "ip",
                        code: ZodIssueCode.invalid_string,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else {
                util.assertNever(check);
            }
        }
        return { status: status.value, value: input.data };
    }
    _addCheck(check) {
        return new ZodString({
            ...this._def,
            checks: [...this._def.checks, check],
        });
    }
    email(message) {
        return this._addCheck({ kind: "email", ...errorUtil.errToObj(message) });
    }
    url(message) {
        return this._addCheck({ kind: "url", ...errorUtil.errToObj(message) });
    }
    emoji(message) {
        return this._addCheck({ kind: "emoji", ...errorUtil.errToObj(message) });
    }
    uuid(message) {
        return this._addCheck({ kind: "uuid", ...errorUtil.errToObj(message) });
    }
    cuid(message) {
        return this._addCheck({ kind: "cuid", ...errorUtil.errToObj(message) });
    }
    cuid2(message) {
        return this._addCheck({ kind: "cuid2", ...errorUtil.errToObj(message) });
    }
    ulid(message) {
        return this._addCheck({ kind: "ulid", ...errorUtil.errToObj(message) });
    }
    ip(options) {
        return this._addCheck({ kind: "ip", ...errorUtil.errToObj(options) });
    }
    datetime(options) {
        var _a;
        if (typeof options === "string") {
            return this._addCheck({
                kind: "datetime",
                precision: null,
                offset: false,
                message: options,
            });
        }
        return this._addCheck({
            kind: "datetime",
            precision: typeof (options === null || options === void 0 ? void 0 : options.precision) === "undefined" ? null : options === null || options === void 0 ? void 0 : options.precision,
            offset: (_a = options === null || options === void 0 ? void 0 : options.offset) !== null && _a !== void 0 ? _a : false,
            ...errorUtil.errToObj(options === null || options === void 0 ? void 0 : options.message),
        });
    }
    regex(regex, message) {
        return this._addCheck({
            kind: "regex",
            regex: regex,
            ...errorUtil.errToObj(message),
        });
    }
    includes(value, options) {
        return this._addCheck({
            kind: "includes",
            value: value,
            position: options === null || options === void 0 ? void 0 : options.position,
            ...errorUtil.errToObj(options === null || options === void 0 ? void 0 : options.message),
        });
    }
    startsWith(value, message) {
        return this._addCheck({
            kind: "startsWith",
            value: value,
            ...errorUtil.errToObj(message),
        });
    }
    endsWith(value, message) {
        return this._addCheck({
            kind: "endsWith",
            value: value,
            ...errorUtil.errToObj(message),
        });
    }
    min(minLength, message) {
        return this._addCheck({
            kind: "min",
            value: minLength,
            ...errorUtil.errToObj(message),
        });
    }
    max(maxLength, message) {
        return this._addCheck({
            kind: "max",
            value: maxLength,
            ...errorUtil.errToObj(message),
        });
    }
    length(len, message) {
        return this._addCheck({
            kind: "length",
            value: len,
            ...errorUtil.errToObj(message),
        });
    }
    get isDatetime() {
        return !!this._def.checks.find((ch) => ch.kind === "datetime");
    }
    get isEmail() {
        return !!this._def.checks.find((ch) => ch.kind === "email");
    }
    get isURL() {
        return !!this._def.checks.find((ch) => ch.kind === "url");
    }
    get isEmoji() {
        return !!this._def.checks.find((ch) => ch.kind === "emoji");
    }
    get isUUID() {
        return !!this._def.checks.find((ch) => ch.kind === "uuid");
    }
    get isCUID() {
        return !!this._def.checks.find((ch) => ch.kind === "cuid");
    }
    get isCUID2() {
        return !!this._def.checks.find((ch) => ch.kind === "cuid2");
    }
    get isULID() {
        return !!this._def.checks.find((ch) => ch.kind === "ulid");
    }
    get isIP() {
        return !!this._def.checks.find((ch) => ch.kind === "ip");
    }
    get minLength() {
        let min = null;
        for (const ch of this._def.checks) {
            if (ch.kind === "min") {
                if (min === null || ch.value > min)
                    min = ch.value;
            }
        }
        return min;
    }
    get maxLength() {
        let max = null;
        for (const ch of this._def.checks) {
            if (ch.kind === "max") {
                if (max === null || ch.value < max)
                    max = ch.value;
            }
        }
        return max;
    }
}
ZodString.create = (params) => {
    var _a;
    return new ZodString({
        checks: [],
        typeName: ZodFirstPartyTypeKind.ZodString,
        coerce: (_a = params === null || params === void 0 ? void 0 : params.coerce) !== null && _a !== void 0 ? _a : false,
        ...processCreateParams(params),
    });
};
// https://stackoverflow.com/questions/3966484/why-does-modulus-operator-return-fractional-number-in-javascript/31711034#31711034
function floatSafeRemainder(val, step) {
    const valDecCount = (val.toString().split(".")[1] || "").length;
    const stepDecCount = (step.toString().split(".")[1] || "").length;
    const decCount = valDecCount > stepDecCount ? valDecCount : stepDecCount;
    const valInt = parseInt(val.toFixed(decCount).replace(".", ""));
    const stepInt = parseInt(step.toFixed(decCount).replace(".", ""));
    return (valInt % stepInt) / Math.pow(10, decCount);
}
class ZodNumber extends ZodType {
    constructor() {
        super(...arguments);
        this.min = this.gte;
        this.max = this.lte;
        this.step = this.multipleOf;
    }
    _parse(input) {
        if (this._def.coerce) {
            input.data = Number(input.data);
        }
        const parsedType = this._getType(input);
        if (parsedType !== ZodParsedType.number) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.number,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        let ctx = undefined;
        const status = new ParseStatus();
        for (const check of this._def.checks) {
            if (check.kind === "int") {
                if (!util.isInteger(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.invalid_type,
                        expected: "integer",
                        received: "float",
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "min") {
                const tooSmall = check.inclusive
                    ? input.data < check.value
                    : input.data <= check.value;
                if (tooSmall) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.too_small,
                        minimum: check.value,
                        type: "number",
                        inclusive: check.inclusive,
                        exact: false,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "max") {
                const tooBig = check.inclusive
                    ? input.data > check.value
                    : input.data >= check.value;
                if (tooBig) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.too_big,
                        maximum: check.value,
                        type: "number",
                        inclusive: check.inclusive,
                        exact: false,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "multipleOf") {
                if (floatSafeRemainder(input.data, check.value) !== 0) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.not_multiple_of,
                        multipleOf: check.value,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "finite") {
                if (!Number.isFinite(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.not_finite,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else {
                util.assertNever(check);
            }
        }
        return { status: status.value, value: input.data };
    }
    gte(value, message) {
        return this.setLimit("min", value, true, errorUtil.toString(message));
    }
    gt(value, message) {
        return this.setLimit("min", value, false, errorUtil.toString(message));
    }
    lte(value, message) {
        return this.setLimit("max", value, true, errorUtil.toString(message));
    }
    lt(value, message) {
        return this.setLimit("max", value, false, errorUtil.toString(message));
    }
    setLimit(kind, value, inclusive, message) {
        return new ZodNumber({
            ...this._def,
            checks: [
                ...this._def.checks,
                {
                    kind,
                    value,
                    inclusive,
                    message: errorUtil.toString(message),
                },
            ],
        });
    }
    _addCheck(check) {
        return new ZodNumber({
            ...this._def,
            checks: [...this._def.checks, check],
        });
    }
    int(message) {
        return this._addCheck({
            kind: "int",
            message: errorUtil.toString(message),
        });
    }
    positive(message) {
        return this._addCheck({
            kind: "min",
            value: 0,
            inclusive: false,
            message: errorUtil.toString(message),
        });
    }
    negative(message) {
        return this._addCheck({
            kind: "max",
            value: 0,
            inclusive: false,
            message: errorUtil.toString(message),
        });
    }
    nonpositive(message) {
        return this._addCheck({
            kind: "max",
            value: 0,
            inclusive: true,
            message: errorUtil.toString(message),
        });
    }
    nonnegative(message) {
        return this._addCheck({
            kind: "min",
            value: 0,
            inclusive: true,
            message: errorUtil.toString(message),
        });
    }
    multipleOf(value, message) {
        return this._addCheck({
            kind: "multipleOf",
            value: value,
            message: errorUtil.toString(message),
        });
    }
    finite(message) {
        return this._addCheck({
            kind: "finite",
            message: errorUtil.toString(message),
        });
    }
    safe(message) {
        return this._addCheck({
            kind: "min",
            inclusive: true,
            value: Number.MIN_SAFE_INTEGER,
            message: errorUtil.toString(message),
        })._addCheck({
            kind: "max",
            inclusive: true,
            value: Number.MAX_SAFE_INTEGER,
            message: errorUtil.toString(message),
        });
    }
    get minValue() {
        let min = null;
        for (const ch of this._def.checks) {
            if (ch.kind === "min") {
                if (min === null || ch.value > min)
                    min = ch.value;
            }
        }
        return min;
    }
    get maxValue() {
        let max = null;
        for (const ch of this._def.checks) {
            if (ch.kind === "max") {
                if (max === null || ch.value < max)
                    max = ch.value;
            }
        }
        return max;
    }
    get isInt() {
        return !!this._def.checks.find((ch) => ch.kind === "int" ||
            (ch.kind === "multipleOf" && util.isInteger(ch.value)));
    }
    get isFinite() {
        let max = null, min = null;
        for (const ch of this._def.checks) {
            if (ch.kind === "finite" ||
                ch.kind === "int" ||
                ch.kind === "multipleOf") {
                return true;
            }
            else if (ch.kind === "min") {
                if (min === null || ch.value > min)
                    min = ch.value;
            }
            else if (ch.kind === "max") {
                if (max === null || ch.value < max)
                    max = ch.value;
            }
        }
        return Number.isFinite(min) && Number.isFinite(max);
    }
}
ZodNumber.create = (params) => {
    return new ZodNumber({
        checks: [],
        typeName: ZodFirstPartyTypeKind.ZodNumber,
        coerce: (params === null || params === void 0 ? void 0 : params.coerce) || false,
        ...processCreateParams(params),
    });
};
class ZodBigInt extends ZodType {
    constructor() {
        super(...arguments);
        this.min = this.gte;
        this.max = this.lte;
    }
    _parse(input) {
        if (this._def.coerce) {
            input.data = BigInt(input.data);
        }
        const parsedType = this._getType(input);
        if (parsedType !== ZodParsedType.bigint) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.bigint,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        let ctx = undefined;
        const status = new ParseStatus();
        for (const check of this._def.checks) {
            if (check.kind === "min") {
                const tooSmall = check.inclusive
                    ? input.data < check.value
                    : input.data <= check.value;
                if (tooSmall) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.too_small,
                        type: "bigint",
                        minimum: check.value,
                        inclusive: check.inclusive,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "max") {
                const tooBig = check.inclusive
                    ? input.data > check.value
                    : input.data >= check.value;
                if (tooBig) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.too_big,
                        type: "bigint",
                        maximum: check.value,
                        inclusive: check.inclusive,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "multipleOf") {
                if (input.data % check.value !== BigInt(0)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.not_multiple_of,
                        multipleOf: check.value,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
            else {
                util.assertNever(check);
            }
        }
        return { status: status.value, value: input.data };
    }
    gte(value, message) {
        return this.setLimit("min", value, true, errorUtil.toString(message));
    }
    gt(value, message) {
        return this.setLimit("min", value, false, errorUtil.toString(message));
    }
    lte(value, message) {
        return this.setLimit("max", value, true, errorUtil.toString(message));
    }
    lt(value, message) {
        return this.setLimit("max", value, false, errorUtil.toString(message));
    }
    setLimit(kind, value, inclusive, message) {
        return new ZodBigInt({
            ...this._def,
            checks: [
                ...this._def.checks,
                {
                    kind,
                    value,
                    inclusive,
                    message: errorUtil.toString(message),
                },
            ],
        });
    }
    _addCheck(check) {
        return new ZodBigInt({
            ...this._def,
            checks: [...this._def.checks, check],
        });
    }
    positive(message) {
        return this._addCheck({
            kind: "min",
            value: BigInt(0),
            inclusive: false,
            message: errorUtil.toString(message),
        });
    }
    negative(message) {
        return this._addCheck({
            kind: "max",
            value: BigInt(0),
            inclusive: false,
            message: errorUtil.toString(message),
        });
    }
    nonpositive(message) {
        return this._addCheck({
            kind: "max",
            value: BigInt(0),
            inclusive: true,
            message: errorUtil.toString(message),
        });
    }
    nonnegative(message) {
        return this._addCheck({
            kind: "min",
            value: BigInt(0),
            inclusive: true,
            message: errorUtil.toString(message),
        });
    }
    multipleOf(value, message) {
        return this._addCheck({
            kind: "multipleOf",
            value,
            message: errorUtil.toString(message),
        });
    }
    get minValue() {
        let min = null;
        for (const ch of this._def.checks) {
            if (ch.kind === "min") {
                if (min === null || ch.value > min)
                    min = ch.value;
            }
        }
        return min;
    }
    get maxValue() {
        let max = null;
        for (const ch of this._def.checks) {
            if (ch.kind === "max") {
                if (max === null || ch.value < max)
                    max = ch.value;
            }
        }
        return max;
    }
}
ZodBigInt.create = (params) => {
    var _a;
    return new ZodBigInt({
        checks: [],
        typeName: ZodFirstPartyTypeKind.ZodBigInt,
        coerce: (_a = params === null || params === void 0 ? void 0 : params.coerce) !== null && _a !== void 0 ? _a : false,
        ...processCreateParams(params),
    });
};
class ZodBoolean extends ZodType {
    _parse(input) {
        if (this._def.coerce) {
            input.data = Boolean(input.data);
        }
        const parsedType = this._getType(input);
        if (parsedType !== ZodParsedType.boolean) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.boolean,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        return OK(input.data);
    }
}
ZodBoolean.create = (params) => {
    return new ZodBoolean({
        typeName: ZodFirstPartyTypeKind.ZodBoolean,
        coerce: (params === null || params === void 0 ? void 0 : params.coerce) || false,
        ...processCreateParams(params),
    });
};
class ZodDate extends ZodType {
    _parse(input) {
        if (this._def.coerce) {
            input.data = new Date(input.data);
        }
        const parsedType = this._getType(input);
        if (parsedType !== ZodParsedType.date) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.date,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        if (isNaN(input.data.getTime())) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_date,
            });
            return INVALID;
        }
        const status = new ParseStatus();
        let ctx = undefined;
        for (const check of this._def.checks) {
            if (check.kind === "min") {
                if (input.data.getTime() < check.value) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.too_small,
                        message: check.message,
                        inclusive: true,
                        exact: false,
                        minimum: check.value,
                        type: "date",
                    });
                    status.dirty();
                }
            }
            else if (check.kind === "max") {
                if (input.data.getTime() > check.value) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.too_big,
                        message: check.message,
                        inclusive: true,
                        exact: false,
                        maximum: check.value,
                        type: "date",
                    });
                    status.dirty();
                }
            }
            else {
                util.assertNever(check);
            }
        }
        return {
            status: status.value,
            value: new Date(input.data.getTime()),
        };
    }
    _addCheck(check) {
        return new ZodDate({
            ...this._def,
            checks: [...this._def.checks, check],
        });
    }
    min(minDate, message) {
        return this._addCheck({
            kind: "min",
            value: minDate.getTime(),
            message: errorUtil.toString(message),
        });
    }
    max(maxDate, message) {
        return this._addCheck({
            kind: "max",
            value: maxDate.getTime(),
            message: errorUtil.toString(message),
        });
    }
    get minDate() {
        let min = null;
        for (const ch of this._def.checks) {
            if (ch.kind === "min") {
                if (min === null || ch.value > min)
                    min = ch.value;
            }
        }
        return min != null ? new Date(min) : null;
    }
    get maxDate() {
        let max = null;
        for (const ch of this._def.checks) {
            if (ch.kind === "max") {
                if (max === null || ch.value < max)
                    max = ch.value;
            }
        }
        return max != null ? new Date(max) : null;
    }
}
ZodDate.create = (params) => {
    return new ZodDate({
        checks: [],
        coerce: (params === null || params === void 0 ? void 0 : params.coerce) || false,
        typeName: ZodFirstPartyTypeKind.ZodDate,
        ...processCreateParams(params),
    });
};
class ZodSymbol extends ZodType {
    _parse(input) {
        const parsedType = this._getType(input);
        if (parsedType !== ZodParsedType.symbol) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.symbol,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        return OK(input.data);
    }
}
ZodSymbol.create = (params) => {
    return new ZodSymbol({
        typeName: ZodFirstPartyTypeKind.ZodSymbol,
        ...processCreateParams(params),
    });
};
class ZodUndefined extends ZodType {
    _parse(input) {
        const parsedType = this._getType(input);
        if (parsedType !== ZodParsedType.undefined) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.undefined,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        return OK(input.data);
    }
}
ZodUndefined.create = (params) => {
    return new ZodUndefined({
        typeName: ZodFirstPartyTypeKind.ZodUndefined,
        ...processCreateParams(params),
    });
};
class ZodNull extends ZodType {
    _parse(input) {
        const parsedType = this._getType(input);
        if (parsedType !== ZodParsedType.null) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.null,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        return OK(input.data);
    }
}
ZodNull.create = (params) => {
    return new ZodNull({
        typeName: ZodFirstPartyTypeKind.ZodNull,
        ...processCreateParams(params),
    });
};
class ZodAny extends ZodType {
    constructor() {
        super(...arguments);
        // to prevent instances of other classes from extending ZodAny. this causes issues with catchall in ZodObject.
        this._any = true;
    }
    _parse(input) {
        return OK(input.data);
    }
}
ZodAny.create = (params) => {
    return new ZodAny({
        typeName: ZodFirstPartyTypeKind.ZodAny,
        ...processCreateParams(params),
    });
};
class ZodUnknown extends ZodType {
    constructor() {
        super(...arguments);
        // required
        this._unknown = true;
    }
    _parse(input) {
        return OK(input.data);
    }
}
ZodUnknown.create = (params) => {
    return new ZodUnknown({
        typeName: ZodFirstPartyTypeKind.ZodUnknown,
        ...processCreateParams(params),
    });
};
class ZodNever extends ZodType {
    _parse(input) {
        const ctx = this._getOrReturnCtx(input);
        addIssueToContext(ctx, {
            code: ZodIssueCode.invalid_type,
            expected: ZodParsedType.never,
            received: ctx.parsedType,
        });
        return INVALID;
    }
}
ZodNever.create = (params) => {
    return new ZodNever({
        typeName: ZodFirstPartyTypeKind.ZodNever,
        ...processCreateParams(params),
    });
};
class ZodVoid extends ZodType {
    _parse(input) {
        const parsedType = this._getType(input);
        if (parsedType !== ZodParsedType.undefined) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.void,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        return OK(input.data);
    }
}
ZodVoid.create = (params) => {
    return new ZodVoid({
        typeName: ZodFirstPartyTypeKind.ZodVoid,
        ...processCreateParams(params),
    });
};
class ZodArray extends ZodType {
    _parse(input) {
        const { ctx, status } = this._processInputParams(input);
        const def = this._def;
        if (ctx.parsedType !== ZodParsedType.array) {
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.array,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        if (def.exactLength !== null) {
            const tooBig = ctx.data.length > def.exactLength.value;
            const tooSmall = ctx.data.length < def.exactLength.value;
            if (tooBig || tooSmall) {
                addIssueToContext(ctx, {
                    code: tooBig ? ZodIssueCode.too_big : ZodIssueCode.too_small,
                    minimum: (tooSmall ? def.exactLength.value : undefined),
                    maximum: (tooBig ? def.exactLength.value : undefined),
                    type: "array",
                    inclusive: true,
                    exact: true,
                    message: def.exactLength.message,
                });
                status.dirty();
            }
        }
        if (def.minLength !== null) {
            if (ctx.data.length < def.minLength.value) {
                addIssueToContext(ctx, {
                    code: ZodIssueCode.too_small,
                    minimum: def.minLength.value,
                    type: "array",
                    inclusive: true,
                    exact: false,
                    message: def.minLength.message,
                });
                status.dirty();
            }
        }
        if (def.maxLength !== null) {
            if (ctx.data.length > def.maxLength.value) {
                addIssueToContext(ctx, {
                    code: ZodIssueCode.too_big,
                    maximum: def.maxLength.value,
                    type: "array",
                    inclusive: true,
                    exact: false,
                    message: def.maxLength.message,
                });
                status.dirty();
            }
        }
        if (ctx.common.async) {
            return Promise.all([...ctx.data].map((item, i) => {
                return def.type._parseAsync(new ParseInputLazyPath(ctx, item, ctx.path, i));
            })).then((result) => {
                return ParseStatus.mergeArray(status, result);
            });
        }
        const result = [...ctx.data].map((item, i) => {
            return def.type._parseSync(new ParseInputLazyPath(ctx, item, ctx.path, i));
        });
        return ParseStatus.mergeArray(status, result);
    }
    get element() {
        return this._def.type;
    }
    min(minLength, message) {
        return new ZodArray({
            ...this._def,
            minLength: { value: minLength, message: errorUtil.toString(message) },
        });
    }
    max(maxLength, message) {
        return new ZodArray({
            ...this._def,
            maxLength: { value: maxLength, message: errorUtil.toString(message) },
        });
    }
    length(len, message) {
        return new ZodArray({
            ...this._def,
            exactLength: { value: len, message: errorUtil.toString(message) },
        });
    }
    nonempty(message) {
        return this.min(1, message);
    }
}
ZodArray.create = (schema, params) => {
    return new ZodArray({
        type: schema,
        minLength: null,
        maxLength: null,
        exactLength: null,
        typeName: ZodFirstPartyTypeKind.ZodArray,
        ...processCreateParams(params),
    });
};
function deepPartialify(schema) {
    if (schema instanceof ZodObject) {
        const newShape = {};
        for (const key in schema.shape) {
            const fieldSchema = schema.shape[key];
            newShape[key] = ZodOptional.create(deepPartialify(fieldSchema));
        }
        return new ZodObject({
            ...schema._def,
            shape: () => newShape,
        });
    }
    else if (schema instanceof ZodArray) {
        return new ZodArray({
            ...schema._def,
            type: deepPartialify(schema.element),
        });
    }
    else if (schema instanceof ZodOptional) {
        return ZodOptional.create(deepPartialify(schema.unwrap()));
    }
    else if (schema instanceof ZodNullable) {
        return ZodNullable.create(deepPartialify(schema.unwrap()));
    }
    else if (schema instanceof ZodTuple) {
        return ZodTuple.create(schema.items.map((item) => deepPartialify(item)));
    }
    else {
        return schema;
    }
}
class ZodObject extends ZodType {
    constructor() {
        super(...arguments);
        this._cached = null;
        /**
         * @deprecated In most cases, this is no longer needed - unknown properties are now silently stripped.
         * If you want to pass through unknown properties, use `.passthrough()` instead.
         */
        this.nonstrict = this.passthrough;
        // extend<
        //   Augmentation extends ZodRawShape,
        //   NewOutput extends util.flatten<{
        //     [k in keyof Augmentation | keyof Output]: k extends keyof Augmentation
        //       ? Augmentation[k]["_output"]
        //       : k extends keyof Output
        //       ? Output[k]
        //       : never;
        //   }>,
        //   NewInput extends util.flatten<{
        //     [k in keyof Augmentation | keyof Input]: k extends keyof Augmentation
        //       ? Augmentation[k]["_input"]
        //       : k extends keyof Input
        //       ? Input[k]
        //       : never;
        //   }>
        // >(
        //   augmentation: Augmentation
        // ): ZodObject<
        //   extendShape<T, Augmentation>,
        //   UnknownKeys,
        //   Catchall,
        //   NewOutput,
        //   NewInput
        // > {
        //   return new ZodObject({
        //     ...this._def,
        //     shape: () => ({
        //       ...this._def.shape(),
        //       ...augmentation,
        //     }),
        //   }) as any;
        // }
        /**
         * @deprecated Use `.extend` instead
         *  */
        this.augment = this.extend;
    }
    _getCached() {
        if (this._cached !== null)
            return this._cached;
        const shape = this._def.shape();
        const keys = util.objectKeys(shape);
        return (this._cached = { shape, keys });
    }
    _parse(input) {
        const parsedType = this._getType(input);
        if (parsedType !== ZodParsedType.object) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.object,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        const { status, ctx } = this._processInputParams(input);
        const { shape, keys: shapeKeys } = this._getCached();
        const extraKeys = [];
        if (!(this._def.catchall instanceof ZodNever &&
            this._def.unknownKeys === "strip")) {
            for (const key in ctx.data) {
                if (!shapeKeys.includes(key)) {
                    extraKeys.push(key);
                }
            }
        }
        const pairs = [];
        for (const key of shapeKeys) {
            const keyValidator = shape[key];
            const value = ctx.data[key];
            pairs.push({
                key: { status: "valid", value: key },
                value: keyValidator._parse(new ParseInputLazyPath(ctx, value, ctx.path, key)),
                alwaysSet: key in ctx.data,
            });
        }
        if (this._def.catchall instanceof ZodNever) {
            const unknownKeys = this._def.unknownKeys;
            if (unknownKeys === "passthrough") {
                for (const key of extraKeys) {
                    pairs.push({
                        key: { status: "valid", value: key },
                        value: { status: "valid", value: ctx.data[key] },
                    });
                }
            }
            else if (unknownKeys === "strict") {
                if (extraKeys.length > 0) {
                    addIssueToContext(ctx, {
                        code: ZodIssueCode.unrecognized_keys,
                        keys: extraKeys,
                    });
                    status.dirty();
                }
            }
            else if (unknownKeys === "strip") ;
            else {
                throw new Error(`Internal ZodObject error: invalid unknownKeys value.`);
            }
        }
        else {
            // run catchall validation
            const catchall = this._def.catchall;
            for (const key of extraKeys) {
                const value = ctx.data[key];
                pairs.push({
                    key: { status: "valid", value: key },
                    value: catchall._parse(new ParseInputLazyPath(ctx, value, ctx.path, key) //, ctx.child(key), value, getParsedType(value)
                    ),
                    alwaysSet: key in ctx.data,
                });
            }
        }
        if (ctx.common.async) {
            return Promise.resolve()
                .then(async () => {
                const syncPairs = [];
                for (const pair of pairs) {
                    const key = await pair.key;
                    syncPairs.push({
                        key,
                        value: await pair.value,
                        alwaysSet: pair.alwaysSet,
                    });
                }
                return syncPairs;
            })
                .then((syncPairs) => {
                return ParseStatus.mergeObjectSync(status, syncPairs);
            });
        }
        else {
            return ParseStatus.mergeObjectSync(status, pairs);
        }
    }
    get shape() {
        return this._def.shape();
    }
    strict(message) {
        errorUtil.errToObj;
        return new ZodObject({
            ...this._def,
            unknownKeys: "strict",
            ...(message !== undefined
                ? {
                    errorMap: (issue, ctx) => {
                        var _a, _b, _c, _d;
                        const defaultError = (_c = (_b = (_a = this._def).errorMap) === null || _b === void 0 ? void 0 : _b.call(_a, issue, ctx).message) !== null && _c !== void 0 ? _c : ctx.defaultError;
                        if (issue.code === "unrecognized_keys")
                            return {
                                message: (_d = errorUtil.errToObj(message).message) !== null && _d !== void 0 ? _d : defaultError,
                            };
                        return {
                            message: defaultError,
                        };
                    },
                }
                : {}),
        });
    }
    strip() {
        return new ZodObject({
            ...this._def,
            unknownKeys: "strip",
        });
    }
    passthrough() {
        return new ZodObject({
            ...this._def,
            unknownKeys: "passthrough",
        });
    }
    // const AugmentFactory =
    //   <Def extends ZodObjectDef>(def: Def) =>
    //   <Augmentation extends ZodRawShape>(
    //     augmentation: Augmentation
    //   ): ZodObject<
    //     extendShape<ReturnType<Def["shape"]>, Augmentation>,
    //     Def["unknownKeys"],
    //     Def["catchall"]
    //   > => {
    //     return new ZodObject({
    //       ...def,
    //       shape: () => ({
    //         ...def.shape(),
    //         ...augmentation,
    //       }),
    //     }) as any;
    //   };
    extend(augmentation) {
        return new ZodObject({
            ...this._def,
            shape: () => ({
                ...this._def.shape(),
                ...augmentation,
            }),
        });
    }
    /**
     * Prior to zod@1.0.12 there was a bug in the
     * inferred type of merged objects. Please
     * upgrade if you are experiencing issues.
     */
    merge(merging) {
        const merged = new ZodObject({
            unknownKeys: merging._def.unknownKeys,
            catchall: merging._def.catchall,
            shape: () => ({
                ...this._def.shape(),
                ...merging._def.shape(),
            }),
            typeName: ZodFirstPartyTypeKind.ZodObject,
        });
        return merged;
    }
    // merge<
    //   Incoming extends AnyZodObject,
    //   Augmentation extends Incoming["shape"],
    //   NewOutput extends {
    //     [k in keyof Augmentation | keyof Output]: k extends keyof Augmentation
    //       ? Augmentation[k]["_output"]
    //       : k extends keyof Output
    //       ? Output[k]
    //       : never;
    //   },
    //   NewInput extends {
    //     [k in keyof Augmentation | keyof Input]: k extends keyof Augmentation
    //       ? Augmentation[k]["_input"]
    //       : k extends keyof Input
    //       ? Input[k]
    //       : never;
    //   }
    // >(
    //   merging: Incoming
    // ): ZodObject<
    //   extendShape<T, ReturnType<Incoming["_def"]["shape"]>>,
    //   Incoming["_def"]["unknownKeys"],
    //   Incoming["_def"]["catchall"],
    //   NewOutput,
    //   NewInput
    // > {
    //   const merged: any = new ZodObject({
    //     unknownKeys: merging._def.unknownKeys,
    //     catchall: merging._def.catchall,
    //     shape: () =>
    //       objectUtil.mergeShapes(this._def.shape(), merging._def.shape()),
    //     typeName: ZodFirstPartyTypeKind.ZodObject,
    //   }) as any;
    //   return merged;
    // }
    setKey(key, schema) {
        return this.augment({ [key]: schema });
    }
    // merge<Incoming extends AnyZodObject>(
    //   merging: Incoming
    // ): //ZodObject<T & Incoming["_shape"], UnknownKeys, Catchall> = (merging) => {
    // ZodObject<
    //   extendShape<T, ReturnType<Incoming["_def"]["shape"]>>,
    //   Incoming["_def"]["unknownKeys"],
    //   Incoming["_def"]["catchall"]
    // > {
    //   // const mergedShape = objectUtil.mergeShapes(
    //   //   this._def.shape(),
    //   //   merging._def.shape()
    //   // );
    //   const merged: any = new ZodObject({
    //     unknownKeys: merging._def.unknownKeys,
    //     catchall: merging._def.catchall,
    //     shape: () =>
    //       objectUtil.mergeShapes(this._def.shape(), merging._def.shape()),
    //     typeName: ZodFirstPartyTypeKind.ZodObject,
    //   }) as any;
    //   return merged;
    // }
    catchall(index) {
        return new ZodObject({
            ...this._def,
            catchall: index,
        });
    }
    pick(mask) {
        const shape = {};
        util.objectKeys(mask).forEach((key) => {
            if (mask[key] && this.shape[key]) {
                shape[key] = this.shape[key];
            }
        });
        return new ZodObject({
            ...this._def,
            shape: () => shape,
        });
    }
    omit(mask) {
        const shape = {};
        util.objectKeys(this.shape).forEach((key) => {
            if (!mask[key]) {
                shape[key] = this.shape[key];
            }
        });
        return new ZodObject({
            ...this._def,
            shape: () => shape,
        });
    }
    /**
     * @deprecated
     */
    deepPartial() {
        return deepPartialify(this);
    }
    partial(mask) {
        const newShape = {};
        util.objectKeys(this.shape).forEach((key) => {
            const fieldSchema = this.shape[key];
            if (mask && !mask[key]) {
                newShape[key] = fieldSchema;
            }
            else {
                newShape[key] = fieldSchema.optional();
            }
        });
        return new ZodObject({
            ...this._def,
            shape: () => newShape,
        });
    }
    required(mask) {
        const newShape = {};
        util.objectKeys(this.shape).forEach((key) => {
            if (mask && !mask[key]) {
                newShape[key] = this.shape[key];
            }
            else {
                const fieldSchema = this.shape[key];
                let newField = fieldSchema;
                while (newField instanceof ZodOptional) {
                    newField = newField._def.innerType;
                }
                newShape[key] = newField;
            }
        });
        return new ZodObject({
            ...this._def,
            shape: () => newShape,
        });
    }
    keyof() {
        return createZodEnum(util.objectKeys(this.shape));
    }
}
ZodObject.create = (shape, params) => {
    return new ZodObject({
        shape: () => shape,
        unknownKeys: "strip",
        catchall: ZodNever.create(),
        typeName: ZodFirstPartyTypeKind.ZodObject,
        ...processCreateParams(params),
    });
};
ZodObject.strictCreate = (shape, params) => {
    return new ZodObject({
        shape: () => shape,
        unknownKeys: "strict",
        catchall: ZodNever.create(),
        typeName: ZodFirstPartyTypeKind.ZodObject,
        ...processCreateParams(params),
    });
};
ZodObject.lazycreate = (shape, params) => {
    return new ZodObject({
        shape,
        unknownKeys: "strip",
        catchall: ZodNever.create(),
        typeName: ZodFirstPartyTypeKind.ZodObject,
        ...processCreateParams(params),
    });
};
class ZodUnion extends ZodType {
    _parse(input) {
        const { ctx } = this._processInputParams(input);
        const options = this._def.options;
        function handleResults(results) {
            // return first issue-free validation if it exists
            for (const result of results) {
                if (result.result.status === "valid") {
                    return result.result;
                }
            }
            for (const result of results) {
                if (result.result.status === "dirty") {
                    // add issues from dirty option
                    ctx.common.issues.push(...result.ctx.common.issues);
                    return result.result;
                }
            }
            // return invalid
            const unionErrors = results.map((result) => new ZodError(result.ctx.common.issues));
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_union,
                unionErrors,
            });
            return INVALID;
        }
        if (ctx.common.async) {
            return Promise.all(options.map(async (option) => {
                const childCtx = {
                    ...ctx,
                    common: {
                        ...ctx.common,
                        issues: [],
                    },
                    parent: null,
                };
                return {
                    result: await option._parseAsync({
                        data: ctx.data,
                        path: ctx.path,
                        parent: childCtx,
                    }),
                    ctx: childCtx,
                };
            })).then(handleResults);
        }
        else {
            let dirty = undefined;
            const issues = [];
            for (const option of options) {
                const childCtx = {
                    ...ctx,
                    common: {
                        ...ctx.common,
                        issues: [],
                    },
                    parent: null,
                };
                const result = option._parseSync({
                    data: ctx.data,
                    path: ctx.path,
                    parent: childCtx,
                });
                if (result.status === "valid") {
                    return result;
                }
                else if (result.status === "dirty" && !dirty) {
                    dirty = { result, ctx: childCtx };
                }
                if (childCtx.common.issues.length) {
                    issues.push(childCtx.common.issues);
                }
            }
            if (dirty) {
                ctx.common.issues.push(...dirty.ctx.common.issues);
                return dirty.result;
            }
            const unionErrors = issues.map((issues) => new ZodError(issues));
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_union,
                unionErrors,
            });
            return INVALID;
        }
    }
    get options() {
        return this._def.options;
    }
}
ZodUnion.create = (types, params) => {
    return new ZodUnion({
        options: types,
        typeName: ZodFirstPartyTypeKind.ZodUnion,
        ...processCreateParams(params),
    });
};
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
//////////                                 //////////
//////////      ZodDiscriminatedUnion      //////////
//////////                                 //////////
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
const getDiscriminator = (type) => {
    if (type instanceof ZodLazy) {
        return getDiscriminator(type.schema);
    }
    else if (type instanceof ZodEffects) {
        return getDiscriminator(type.innerType());
    }
    else if (type instanceof ZodLiteral) {
        return [type.value];
    }
    else if (type instanceof ZodEnum) {
        return type.options;
    }
    else if (type instanceof ZodNativeEnum) {
        // eslint-disable-next-line ban/ban
        return Object.keys(type.enum);
    }
    else if (type instanceof ZodDefault) {
        return getDiscriminator(type._def.innerType);
    }
    else if (type instanceof ZodUndefined) {
        return [undefined];
    }
    else if (type instanceof ZodNull) {
        return [null];
    }
    else {
        return null;
    }
};
class ZodDiscriminatedUnion extends ZodType {
    _parse(input) {
        const { ctx } = this._processInputParams(input);
        if (ctx.parsedType !== ZodParsedType.object) {
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.object,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        const discriminator = this.discriminator;
        const discriminatorValue = ctx.data[discriminator];
        const option = this.optionsMap.get(discriminatorValue);
        if (!option) {
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_union_discriminator,
                options: Array.from(this.optionsMap.keys()),
                path: [discriminator],
            });
            return INVALID;
        }
        if (ctx.common.async) {
            return option._parseAsync({
                data: ctx.data,
                path: ctx.path,
                parent: ctx,
            });
        }
        else {
            return option._parseSync({
                data: ctx.data,
                path: ctx.path,
                parent: ctx,
            });
        }
    }
    get discriminator() {
        return this._def.discriminator;
    }
    get options() {
        return this._def.options;
    }
    get optionsMap() {
        return this._def.optionsMap;
    }
    /**
     * The constructor of the discriminated union schema. Its behaviour is very similar to that of the normal z.union() constructor.
     * However, it only allows a union of objects, all of which need to share a discriminator property. This property must
     * have a different value for each object in the union.
     * @param discriminator the name of the discriminator property
     * @param types an array of object schemas
     * @param params
     */
    static create(discriminator, options, params) {
        // Get all the valid discriminator values
        const optionsMap = new Map();
        // try {
        for (const type of options) {
            const discriminatorValues = getDiscriminator(type.shape[discriminator]);
            if (!discriminatorValues) {
                throw new Error(`A discriminator value for key \`${discriminator}\` could not be extracted from all schema options`);
            }
            for (const value of discriminatorValues) {
                if (optionsMap.has(value)) {
                    throw new Error(`Discriminator property ${String(discriminator)} has duplicate value ${String(value)}`);
                }
                optionsMap.set(value, type);
            }
        }
        return new ZodDiscriminatedUnion({
            typeName: ZodFirstPartyTypeKind.ZodDiscriminatedUnion,
            discriminator,
            options,
            optionsMap,
            ...processCreateParams(params),
        });
    }
}
function mergeValues(a, b) {
    const aType = getParsedType(a);
    const bType = getParsedType(b);
    if (a === b) {
        return { valid: true, data: a };
    }
    else if (aType === ZodParsedType.object && bType === ZodParsedType.object) {
        const bKeys = util.objectKeys(b);
        const sharedKeys = util
            .objectKeys(a)
            .filter((key) => bKeys.indexOf(key) !== -1);
        const newObj = { ...a, ...b };
        for (const key of sharedKeys) {
            const sharedValue = mergeValues(a[key], b[key]);
            if (!sharedValue.valid) {
                return { valid: false };
            }
            newObj[key] = sharedValue.data;
        }
        return { valid: true, data: newObj };
    }
    else if (aType === ZodParsedType.array && bType === ZodParsedType.array) {
        if (a.length !== b.length) {
            return { valid: false };
        }
        const newArray = [];
        for (let index = 0; index < a.length; index++) {
            const itemA = a[index];
            const itemB = b[index];
            const sharedValue = mergeValues(itemA, itemB);
            if (!sharedValue.valid) {
                return { valid: false };
            }
            newArray.push(sharedValue.data);
        }
        return { valid: true, data: newArray };
    }
    else if (aType === ZodParsedType.date &&
        bType === ZodParsedType.date &&
        +a === +b) {
        return { valid: true, data: a };
    }
    else {
        return { valid: false };
    }
}
class ZodIntersection extends ZodType {
    _parse(input) {
        const { status, ctx } = this._processInputParams(input);
        const handleParsed = (parsedLeft, parsedRight) => {
            if (isAborted(parsedLeft) || isAborted(parsedRight)) {
                return INVALID;
            }
            const merged = mergeValues(parsedLeft.value, parsedRight.value);
            if (!merged.valid) {
                addIssueToContext(ctx, {
                    code: ZodIssueCode.invalid_intersection_types,
                });
                return INVALID;
            }
            if (isDirty(parsedLeft) || isDirty(parsedRight)) {
                status.dirty();
            }
            return { status: status.value, value: merged.data };
        };
        if (ctx.common.async) {
            return Promise.all([
                this._def.left._parseAsync({
                    data: ctx.data,
                    path: ctx.path,
                    parent: ctx,
                }),
                this._def.right._parseAsync({
                    data: ctx.data,
                    path: ctx.path,
                    parent: ctx,
                }),
            ]).then(([left, right]) => handleParsed(left, right));
        }
        else {
            return handleParsed(this._def.left._parseSync({
                data: ctx.data,
                path: ctx.path,
                parent: ctx,
            }), this._def.right._parseSync({
                data: ctx.data,
                path: ctx.path,
                parent: ctx,
            }));
        }
    }
}
ZodIntersection.create = (left, right, params) => {
    return new ZodIntersection({
        left: left,
        right: right,
        typeName: ZodFirstPartyTypeKind.ZodIntersection,
        ...processCreateParams(params),
    });
};
class ZodTuple extends ZodType {
    _parse(input) {
        const { status, ctx } = this._processInputParams(input);
        if (ctx.parsedType !== ZodParsedType.array) {
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.array,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        if (ctx.data.length < this._def.items.length) {
            addIssueToContext(ctx, {
                code: ZodIssueCode.too_small,
                minimum: this._def.items.length,
                inclusive: true,
                exact: false,
                type: "array",
            });
            return INVALID;
        }
        const rest = this._def.rest;
        if (!rest && ctx.data.length > this._def.items.length) {
            addIssueToContext(ctx, {
                code: ZodIssueCode.too_big,
                maximum: this._def.items.length,
                inclusive: true,
                exact: false,
                type: "array",
            });
            status.dirty();
        }
        const items = [...ctx.data]
            .map((item, itemIndex) => {
            const schema = this._def.items[itemIndex] || this._def.rest;
            if (!schema)
                return null;
            return schema._parse(new ParseInputLazyPath(ctx, item, ctx.path, itemIndex));
        })
            .filter((x) => !!x); // filter nulls
        if (ctx.common.async) {
            return Promise.all(items).then((results) => {
                return ParseStatus.mergeArray(status, results);
            });
        }
        else {
            return ParseStatus.mergeArray(status, items);
        }
    }
    get items() {
        return this._def.items;
    }
    rest(rest) {
        return new ZodTuple({
            ...this._def,
            rest,
        });
    }
}
ZodTuple.create = (schemas, params) => {
    if (!Array.isArray(schemas)) {
        throw new Error("You must pass an array of schemas to z.tuple([ ... ])");
    }
    return new ZodTuple({
        items: schemas,
        typeName: ZodFirstPartyTypeKind.ZodTuple,
        rest: null,
        ...processCreateParams(params),
    });
};
class ZodRecord extends ZodType {
    get keySchema() {
        return this._def.keyType;
    }
    get valueSchema() {
        return this._def.valueType;
    }
    _parse(input) {
        const { status, ctx } = this._processInputParams(input);
        if (ctx.parsedType !== ZodParsedType.object) {
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.object,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        const pairs = [];
        const keyType = this._def.keyType;
        const valueType = this._def.valueType;
        for (const key in ctx.data) {
            pairs.push({
                key: keyType._parse(new ParseInputLazyPath(ctx, key, ctx.path, key)),
                value: valueType._parse(new ParseInputLazyPath(ctx, ctx.data[key], ctx.path, key)),
            });
        }
        if (ctx.common.async) {
            return ParseStatus.mergeObjectAsync(status, pairs);
        }
        else {
            return ParseStatus.mergeObjectSync(status, pairs);
        }
    }
    get element() {
        return this._def.valueType;
    }
    static create(first, second, third) {
        if (second instanceof ZodType) {
            return new ZodRecord({
                keyType: first,
                valueType: second,
                typeName: ZodFirstPartyTypeKind.ZodRecord,
                ...processCreateParams(third),
            });
        }
        return new ZodRecord({
            keyType: ZodString.create(),
            valueType: first,
            typeName: ZodFirstPartyTypeKind.ZodRecord,
            ...processCreateParams(second),
        });
    }
}
class ZodMap extends ZodType {
    _parse(input) {
        const { status, ctx } = this._processInputParams(input);
        if (ctx.parsedType !== ZodParsedType.map) {
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.map,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        const keyType = this._def.keyType;
        const valueType = this._def.valueType;
        const pairs = [...ctx.data.entries()].map(([key, value], index) => {
            return {
                key: keyType._parse(new ParseInputLazyPath(ctx, key, ctx.path, [index, "key"])),
                value: valueType._parse(new ParseInputLazyPath(ctx, value, ctx.path, [index, "value"])),
            };
        });
        if (ctx.common.async) {
            const finalMap = new Map();
            return Promise.resolve().then(async () => {
                for (const pair of pairs) {
                    const key = await pair.key;
                    const value = await pair.value;
                    if (key.status === "aborted" || value.status === "aborted") {
                        return INVALID;
                    }
                    if (key.status === "dirty" || value.status === "dirty") {
                        status.dirty();
                    }
                    finalMap.set(key.value, value.value);
                }
                return { status: status.value, value: finalMap };
            });
        }
        else {
            const finalMap = new Map();
            for (const pair of pairs) {
                const key = pair.key;
                const value = pair.value;
                if (key.status === "aborted" || value.status === "aborted") {
                    return INVALID;
                }
                if (key.status === "dirty" || value.status === "dirty") {
                    status.dirty();
                }
                finalMap.set(key.value, value.value);
            }
            return { status: status.value, value: finalMap };
        }
    }
}
ZodMap.create = (keyType, valueType, params) => {
    return new ZodMap({
        valueType,
        keyType,
        typeName: ZodFirstPartyTypeKind.ZodMap,
        ...processCreateParams(params),
    });
};
class ZodSet extends ZodType {
    _parse(input) {
        const { status, ctx } = this._processInputParams(input);
        if (ctx.parsedType !== ZodParsedType.set) {
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.set,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        const def = this._def;
        if (def.minSize !== null) {
            if (ctx.data.size < def.minSize.value) {
                addIssueToContext(ctx, {
                    code: ZodIssueCode.too_small,
                    minimum: def.minSize.value,
                    type: "set",
                    inclusive: true,
                    exact: false,
                    message: def.minSize.message,
                });
                status.dirty();
            }
        }
        if (def.maxSize !== null) {
            if (ctx.data.size > def.maxSize.value) {
                addIssueToContext(ctx, {
                    code: ZodIssueCode.too_big,
                    maximum: def.maxSize.value,
                    type: "set",
                    inclusive: true,
                    exact: false,
                    message: def.maxSize.message,
                });
                status.dirty();
            }
        }
        const valueType = this._def.valueType;
        function finalizeSet(elements) {
            const parsedSet = new Set();
            for (const element of elements) {
                if (element.status === "aborted")
                    return INVALID;
                if (element.status === "dirty")
                    status.dirty();
                parsedSet.add(element.value);
            }
            return { status: status.value, value: parsedSet };
        }
        const elements = [...ctx.data.values()].map((item, i) => valueType._parse(new ParseInputLazyPath(ctx, item, ctx.path, i)));
        if (ctx.common.async) {
            return Promise.all(elements).then((elements) => finalizeSet(elements));
        }
        else {
            return finalizeSet(elements);
        }
    }
    min(minSize, message) {
        return new ZodSet({
            ...this._def,
            minSize: { value: minSize, message: errorUtil.toString(message) },
        });
    }
    max(maxSize, message) {
        return new ZodSet({
            ...this._def,
            maxSize: { value: maxSize, message: errorUtil.toString(message) },
        });
    }
    size(size, message) {
        return this.min(size, message).max(size, message);
    }
    nonempty(message) {
        return this.min(1, message);
    }
}
ZodSet.create = (valueType, params) => {
    return new ZodSet({
        valueType,
        minSize: null,
        maxSize: null,
        typeName: ZodFirstPartyTypeKind.ZodSet,
        ...processCreateParams(params),
    });
};
class ZodFunction extends ZodType {
    constructor() {
        super(...arguments);
        this.validate = this.implement;
    }
    _parse(input) {
        const { ctx } = this._processInputParams(input);
        if (ctx.parsedType !== ZodParsedType.function) {
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.function,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        function makeArgsIssue(args, error) {
            return makeIssue({
                data: args,
                path: ctx.path,
                errorMaps: [
                    ctx.common.contextualErrorMap,
                    ctx.schemaErrorMap,
                    getErrorMap(),
                    errorMap,
                ].filter((x) => !!x),
                issueData: {
                    code: ZodIssueCode.invalid_arguments,
                    argumentsError: error,
                },
            });
        }
        function makeReturnsIssue(returns, error) {
            return makeIssue({
                data: returns,
                path: ctx.path,
                errorMaps: [
                    ctx.common.contextualErrorMap,
                    ctx.schemaErrorMap,
                    getErrorMap(),
                    errorMap,
                ].filter((x) => !!x),
                issueData: {
                    code: ZodIssueCode.invalid_return_type,
                    returnTypeError: error,
                },
            });
        }
        const params = { errorMap: ctx.common.contextualErrorMap };
        const fn = ctx.data;
        if (this._def.returns instanceof ZodPromise) {
            return OK(async (...args) => {
                const error = new ZodError([]);
                const parsedArgs = await this._def.args
                    .parseAsync(args, params)
                    .catch((e) => {
                    error.addIssue(makeArgsIssue(args, e));
                    throw error;
                });
                const result = await fn(...parsedArgs);
                const parsedReturns = await this._def.returns._def.type
                    .parseAsync(result, params)
                    .catch((e) => {
                    error.addIssue(makeReturnsIssue(result, e));
                    throw error;
                });
                return parsedReturns;
            });
        }
        else {
            return OK((...args) => {
                const parsedArgs = this._def.args.safeParse(args, params);
                if (!parsedArgs.success) {
                    throw new ZodError([makeArgsIssue(args, parsedArgs.error)]);
                }
                const result = fn(...parsedArgs.data);
                const parsedReturns = this._def.returns.safeParse(result, params);
                if (!parsedReturns.success) {
                    throw new ZodError([makeReturnsIssue(result, parsedReturns.error)]);
                }
                return parsedReturns.data;
            });
        }
    }
    parameters() {
        return this._def.args;
    }
    returnType() {
        return this._def.returns;
    }
    args(...items) {
        return new ZodFunction({
            ...this._def,
            args: ZodTuple.create(items).rest(ZodUnknown.create()),
        });
    }
    returns(returnType) {
        return new ZodFunction({
            ...this._def,
            returns: returnType,
        });
    }
    implement(func) {
        const validatedFunc = this.parse(func);
        return validatedFunc;
    }
    strictImplement(func) {
        const validatedFunc = this.parse(func);
        return validatedFunc;
    }
    static create(args, returns, params) {
        return new ZodFunction({
            args: (args
                ? args
                : ZodTuple.create([]).rest(ZodUnknown.create())),
            returns: returns || ZodUnknown.create(),
            typeName: ZodFirstPartyTypeKind.ZodFunction,
            ...processCreateParams(params),
        });
    }
}
class ZodLazy extends ZodType {
    get schema() {
        return this._def.getter();
    }
    _parse(input) {
        const { ctx } = this._processInputParams(input);
        const lazySchema = this._def.getter();
        return lazySchema._parse({ data: ctx.data, path: ctx.path, parent: ctx });
    }
}
ZodLazy.create = (getter, params) => {
    return new ZodLazy({
        getter: getter,
        typeName: ZodFirstPartyTypeKind.ZodLazy,
        ...processCreateParams(params),
    });
};
class ZodLiteral extends ZodType {
    _parse(input) {
        if (input.data !== this._def.value) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                received: ctx.data,
                code: ZodIssueCode.invalid_literal,
                expected: this._def.value,
            });
            return INVALID;
        }
        return { status: "valid", value: input.data };
    }
    get value() {
        return this._def.value;
    }
}
ZodLiteral.create = (value, params) => {
    return new ZodLiteral({
        value: value,
        typeName: ZodFirstPartyTypeKind.ZodLiteral,
        ...processCreateParams(params),
    });
};
function createZodEnum(values, params) {
    return new ZodEnum({
        values: values,
        typeName: ZodFirstPartyTypeKind.ZodEnum,
        ...processCreateParams(params),
    });
}
class ZodEnum extends ZodType {
    _parse(input) {
        if (typeof input.data !== "string") {
            const ctx = this._getOrReturnCtx(input);
            const expectedValues = this._def.values;
            addIssueToContext(ctx, {
                expected: util.joinValues(expectedValues),
                received: ctx.parsedType,
                code: ZodIssueCode.invalid_type,
            });
            return INVALID;
        }
        if (this._def.values.indexOf(input.data) === -1) {
            const ctx = this._getOrReturnCtx(input);
            const expectedValues = this._def.values;
            addIssueToContext(ctx, {
                received: ctx.data,
                code: ZodIssueCode.invalid_enum_value,
                options: expectedValues,
            });
            return INVALID;
        }
        return OK(input.data);
    }
    get options() {
        return this._def.values;
    }
    get enum() {
        const enumValues = {};
        for (const val of this._def.values) {
            enumValues[val] = val;
        }
        return enumValues;
    }
    get Values() {
        const enumValues = {};
        for (const val of this._def.values) {
            enumValues[val] = val;
        }
        return enumValues;
    }
    get Enum() {
        const enumValues = {};
        for (const val of this._def.values) {
            enumValues[val] = val;
        }
        return enumValues;
    }
    extract(values) {
        return ZodEnum.create(values);
    }
    exclude(values) {
        return ZodEnum.create(this.options.filter((opt) => !values.includes(opt)));
    }
}
ZodEnum.create = createZodEnum;
class ZodNativeEnum extends ZodType {
    _parse(input) {
        const nativeEnumValues = util.getValidEnumValues(this._def.values);
        const ctx = this._getOrReturnCtx(input);
        if (ctx.parsedType !== ZodParsedType.string &&
            ctx.parsedType !== ZodParsedType.number) {
            const expectedValues = util.objectValues(nativeEnumValues);
            addIssueToContext(ctx, {
                expected: util.joinValues(expectedValues),
                received: ctx.parsedType,
                code: ZodIssueCode.invalid_type,
            });
            return INVALID;
        }
        if (nativeEnumValues.indexOf(input.data) === -1) {
            const expectedValues = util.objectValues(nativeEnumValues);
            addIssueToContext(ctx, {
                received: ctx.data,
                code: ZodIssueCode.invalid_enum_value,
                options: expectedValues,
            });
            return INVALID;
        }
        return OK(input.data);
    }
    get enum() {
        return this._def.values;
    }
}
ZodNativeEnum.create = (values, params) => {
    return new ZodNativeEnum({
        values: values,
        typeName: ZodFirstPartyTypeKind.ZodNativeEnum,
        ...processCreateParams(params),
    });
};
class ZodPromise extends ZodType {
    unwrap() {
        return this._def.type;
    }
    _parse(input) {
        const { ctx } = this._processInputParams(input);
        if (ctx.parsedType !== ZodParsedType.promise &&
            ctx.common.async === false) {
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.promise,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        const promisified = ctx.parsedType === ZodParsedType.promise
            ? ctx.data
            : Promise.resolve(ctx.data);
        return OK(promisified.then((data) => {
            return this._def.type.parseAsync(data, {
                path: ctx.path,
                errorMap: ctx.common.contextualErrorMap,
            });
        }));
    }
}
ZodPromise.create = (schema, params) => {
    return new ZodPromise({
        type: schema,
        typeName: ZodFirstPartyTypeKind.ZodPromise,
        ...processCreateParams(params),
    });
};
class ZodEffects extends ZodType {
    innerType() {
        return this._def.schema;
    }
    sourceType() {
        return this._def.schema._def.typeName === ZodFirstPartyTypeKind.ZodEffects
            ? this._def.schema.sourceType()
            : this._def.schema;
    }
    _parse(input) {
        const { status, ctx } = this._processInputParams(input);
        const effect = this._def.effect || null;
        if (effect.type === "preprocess") {
            const processed = effect.transform(ctx.data);
            if (ctx.common.async) {
                return Promise.resolve(processed).then((processed) => {
                    return this._def.schema._parseAsync({
                        data: processed,
                        path: ctx.path,
                        parent: ctx,
                    });
                });
            }
            else {
                return this._def.schema._parseSync({
                    data: processed,
                    path: ctx.path,
                    parent: ctx,
                });
            }
        }
        const checkCtx = {
            addIssue: (arg) => {
                addIssueToContext(ctx, arg);
                if (arg.fatal) {
                    status.abort();
                }
                else {
                    status.dirty();
                }
            },
            get path() {
                return ctx.path;
            },
        };
        checkCtx.addIssue = checkCtx.addIssue.bind(checkCtx);
        if (effect.type === "refinement") {
            const executeRefinement = (acc
            // effect: RefinementEffect<any>
            ) => {
                const result = effect.refinement(acc, checkCtx);
                if (ctx.common.async) {
                    return Promise.resolve(result);
                }
                if (result instanceof Promise) {
                    throw new Error("Async refinement encountered during synchronous parse operation. Use .parseAsync instead.");
                }
                return acc;
            };
            if (ctx.common.async === false) {
                const inner = this._def.schema._parseSync({
                    data: ctx.data,
                    path: ctx.path,
                    parent: ctx,
                });
                if (inner.status === "aborted")
                    return INVALID;
                if (inner.status === "dirty")
                    status.dirty();
                // return value is ignored
                executeRefinement(inner.value);
                return { status: status.value, value: inner.value };
            }
            else {
                return this._def.schema
                    ._parseAsync({ data: ctx.data, path: ctx.path, parent: ctx })
                    .then((inner) => {
                    if (inner.status === "aborted")
                        return INVALID;
                    if (inner.status === "dirty")
                        status.dirty();
                    return executeRefinement(inner.value).then(() => {
                        return { status: status.value, value: inner.value };
                    });
                });
            }
        }
        if (effect.type === "transform") {
            if (ctx.common.async === false) {
                const base = this._def.schema._parseSync({
                    data: ctx.data,
                    path: ctx.path,
                    parent: ctx,
                });
                if (!isValid(base))
                    return base;
                const result = effect.transform(base.value, checkCtx);
                if (result instanceof Promise) {
                    throw new Error(`Asynchronous transform encountered during synchronous parse operation. Use .parseAsync instead.`);
                }
                return { status: status.value, value: result };
            }
            else {
                return this._def.schema
                    ._parseAsync({ data: ctx.data, path: ctx.path, parent: ctx })
                    .then((base) => {
                    if (!isValid(base))
                        return base;
                    return Promise.resolve(effect.transform(base.value, checkCtx)).then((result) => ({ status: status.value, value: result }));
                });
            }
        }
        util.assertNever(effect);
    }
}
ZodEffects.create = (schema, effect, params) => {
    return new ZodEffects({
        schema,
        typeName: ZodFirstPartyTypeKind.ZodEffects,
        effect,
        ...processCreateParams(params),
    });
};
ZodEffects.createWithPreprocess = (preprocess, schema, params) => {
    return new ZodEffects({
        schema,
        effect: { type: "preprocess", transform: preprocess },
        typeName: ZodFirstPartyTypeKind.ZodEffects,
        ...processCreateParams(params),
    });
};
class ZodOptional extends ZodType {
    _parse(input) {
        const parsedType = this._getType(input);
        if (parsedType === ZodParsedType.undefined) {
            return OK(undefined);
        }
        return this._def.innerType._parse(input);
    }
    unwrap() {
        return this._def.innerType;
    }
}
ZodOptional.create = (type, params) => {
    return new ZodOptional({
        innerType: type,
        typeName: ZodFirstPartyTypeKind.ZodOptional,
        ...processCreateParams(params),
    });
};
class ZodNullable extends ZodType {
    _parse(input) {
        const parsedType = this._getType(input);
        if (parsedType === ZodParsedType.null) {
            return OK(null);
        }
        return this._def.innerType._parse(input);
    }
    unwrap() {
        return this._def.innerType;
    }
}
ZodNullable.create = (type, params) => {
    return new ZodNullable({
        innerType: type,
        typeName: ZodFirstPartyTypeKind.ZodNullable,
        ...processCreateParams(params),
    });
};
class ZodDefault extends ZodType {
    _parse(input) {
        const { ctx } = this._processInputParams(input);
        let data = ctx.data;
        if (ctx.parsedType === ZodParsedType.undefined) {
            data = this._def.defaultValue();
        }
        return this._def.innerType._parse({
            data,
            path: ctx.path,
            parent: ctx,
        });
    }
    removeDefault() {
        return this._def.innerType;
    }
}
ZodDefault.create = (type, params) => {
    return new ZodDefault({
        innerType: type,
        typeName: ZodFirstPartyTypeKind.ZodDefault,
        defaultValue: typeof params.default === "function"
            ? params.default
            : () => params.default,
        ...processCreateParams(params),
    });
};
class ZodCatch extends ZodType {
    _parse(input) {
        const { ctx } = this._processInputParams(input);
        // newCtx is used to not collect issues from inner types in ctx
        const newCtx = {
            ...ctx,
            common: {
                ...ctx.common,
                issues: [],
            },
        };
        const result = this._def.innerType._parse({
            data: newCtx.data,
            path: newCtx.path,
            parent: {
                ...newCtx,
            },
        });
        if (isAsync(result)) {
            return result.then((result) => {
                return {
                    status: "valid",
                    value: result.status === "valid"
                        ? result.value
                        : this._def.catchValue({
                            get error() {
                                return new ZodError(newCtx.common.issues);
                            },
                            input: newCtx.data,
                        }),
                };
            });
        }
        else {
            return {
                status: "valid",
                value: result.status === "valid"
                    ? result.value
                    : this._def.catchValue({
                        get error() {
                            return new ZodError(newCtx.common.issues);
                        },
                        input: newCtx.data,
                    }),
            };
        }
    }
    removeCatch() {
        return this._def.innerType;
    }
}
ZodCatch.create = (type, params) => {
    return new ZodCatch({
        innerType: type,
        typeName: ZodFirstPartyTypeKind.ZodCatch,
        catchValue: typeof params.catch === "function" ? params.catch : () => params.catch,
        ...processCreateParams(params),
    });
};
class ZodNaN extends ZodType {
    _parse(input) {
        const parsedType = this._getType(input);
        if (parsedType !== ZodParsedType.nan) {
            const ctx = this._getOrReturnCtx(input);
            addIssueToContext(ctx, {
                code: ZodIssueCode.invalid_type,
                expected: ZodParsedType.nan,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        return { status: "valid", value: input.data };
    }
}
ZodNaN.create = (params) => {
    return new ZodNaN({
        typeName: ZodFirstPartyTypeKind.ZodNaN,
        ...processCreateParams(params),
    });
};
const BRAND = Symbol("zod_brand");
class ZodBranded extends ZodType {
    _parse(input) {
        const { ctx } = this._processInputParams(input);
        const data = ctx.data;
        return this._def.type._parse({
            data,
            path: ctx.path,
            parent: ctx,
        });
    }
    unwrap() {
        return this._def.type;
    }
}
class ZodPipeline extends ZodType {
    _parse(input) {
        const { status, ctx } = this._processInputParams(input);
        if (ctx.common.async) {
            const handleAsync = async () => {
                const inResult = await this._def.in._parseAsync({
                    data: ctx.data,
                    path: ctx.path,
                    parent: ctx,
                });
                if (inResult.status === "aborted")
                    return INVALID;
                if (inResult.status === "dirty") {
                    status.dirty();
                    return DIRTY(inResult.value);
                }
                else {
                    return this._def.out._parseAsync({
                        data: inResult.value,
                        path: ctx.path,
                        parent: ctx,
                    });
                }
            };
            return handleAsync();
        }
        else {
            const inResult = this._def.in._parseSync({
                data: ctx.data,
                path: ctx.path,
                parent: ctx,
            });
            if (inResult.status === "aborted")
                return INVALID;
            if (inResult.status === "dirty") {
                status.dirty();
                return {
                    status: "dirty",
                    value: inResult.value,
                };
            }
            else {
                return this._def.out._parseSync({
                    data: inResult.value,
                    path: ctx.path,
                    parent: ctx,
                });
            }
        }
    }
    static create(a, b) {
        return new ZodPipeline({
            in: a,
            out: b,
            typeName: ZodFirstPartyTypeKind.ZodPipeline,
        });
    }
}
const custom = (check, params = {}, 
/*
 * @deprecated
 *
 * Pass `fatal` into the params object instead:
 *
 * ```ts
 * z.string().custom((val) => val.length > 5, { fatal: false })
 * ```
 *
 */
fatal) => {
    if (check)
        return ZodAny.create().superRefine((data, ctx) => {
            var _a, _b;
            if (!check(data)) {
                const p = typeof params === "function"
                    ? params(data)
                    : typeof params === "string"
                        ? { message: params }
                        : params;
                const _fatal = (_b = (_a = p.fatal) !== null && _a !== void 0 ? _a : fatal) !== null && _b !== void 0 ? _b : true;
                const p2 = typeof p === "string" ? { message: p } : p;
                ctx.addIssue({ code: "custom", ...p2, fatal: _fatal });
            }
        });
    return ZodAny.create();
};
const late = {
    object: ZodObject.lazycreate,
};
var ZodFirstPartyTypeKind;
(function (ZodFirstPartyTypeKind) {
    ZodFirstPartyTypeKind["ZodString"] = "ZodString";
    ZodFirstPartyTypeKind["ZodNumber"] = "ZodNumber";
    ZodFirstPartyTypeKind["ZodNaN"] = "ZodNaN";
    ZodFirstPartyTypeKind["ZodBigInt"] = "ZodBigInt";
    ZodFirstPartyTypeKind["ZodBoolean"] = "ZodBoolean";
    ZodFirstPartyTypeKind["ZodDate"] = "ZodDate";
    ZodFirstPartyTypeKind["ZodSymbol"] = "ZodSymbol";
    ZodFirstPartyTypeKind["ZodUndefined"] = "ZodUndefined";
    ZodFirstPartyTypeKind["ZodNull"] = "ZodNull";
    ZodFirstPartyTypeKind["ZodAny"] = "ZodAny";
    ZodFirstPartyTypeKind["ZodUnknown"] = "ZodUnknown";
    ZodFirstPartyTypeKind["ZodNever"] = "ZodNever";
    ZodFirstPartyTypeKind["ZodVoid"] = "ZodVoid";
    ZodFirstPartyTypeKind["ZodArray"] = "ZodArray";
    ZodFirstPartyTypeKind["ZodObject"] = "ZodObject";
    ZodFirstPartyTypeKind["ZodUnion"] = "ZodUnion";
    ZodFirstPartyTypeKind["ZodDiscriminatedUnion"] = "ZodDiscriminatedUnion";
    ZodFirstPartyTypeKind["ZodIntersection"] = "ZodIntersection";
    ZodFirstPartyTypeKind["ZodTuple"] = "ZodTuple";
    ZodFirstPartyTypeKind["ZodRecord"] = "ZodRecord";
    ZodFirstPartyTypeKind["ZodMap"] = "ZodMap";
    ZodFirstPartyTypeKind["ZodSet"] = "ZodSet";
    ZodFirstPartyTypeKind["ZodFunction"] = "ZodFunction";
    ZodFirstPartyTypeKind["ZodLazy"] = "ZodLazy";
    ZodFirstPartyTypeKind["ZodLiteral"] = "ZodLiteral";
    ZodFirstPartyTypeKind["ZodEnum"] = "ZodEnum";
    ZodFirstPartyTypeKind["ZodEffects"] = "ZodEffects";
    ZodFirstPartyTypeKind["ZodNativeEnum"] = "ZodNativeEnum";
    ZodFirstPartyTypeKind["ZodOptional"] = "ZodOptional";
    ZodFirstPartyTypeKind["ZodNullable"] = "ZodNullable";
    ZodFirstPartyTypeKind["ZodDefault"] = "ZodDefault";
    ZodFirstPartyTypeKind["ZodCatch"] = "ZodCatch";
    ZodFirstPartyTypeKind["ZodPromise"] = "ZodPromise";
    ZodFirstPartyTypeKind["ZodBranded"] = "ZodBranded";
    ZodFirstPartyTypeKind["ZodPipeline"] = "ZodPipeline";
})(ZodFirstPartyTypeKind || (ZodFirstPartyTypeKind = {}));
const instanceOfType = (
// const instanceOfType = <T extends new (...args: any[]) => any>(
cls, params = {
    message: `Input not instance of ${cls.name}`,
}) => custom((data) => data instanceof cls, params);
const stringType = ZodString.create;
const numberType = ZodNumber.create;
const nanType = ZodNaN.create;
const bigIntType = ZodBigInt.create;
const booleanType = ZodBoolean.create;
const dateType = ZodDate.create;
const symbolType = ZodSymbol.create;
const undefinedType = ZodUndefined.create;
const nullType = ZodNull.create;
const anyType = ZodAny.create;
const unknownType = ZodUnknown.create;
const neverType = ZodNever.create;
const voidType = ZodVoid.create;
const arrayType = ZodArray.create;
const objectType = ZodObject.create;
const strictObjectType = ZodObject.strictCreate;
const unionType = ZodUnion.create;
const discriminatedUnionType = ZodDiscriminatedUnion.create;
const intersectionType = ZodIntersection.create;
const tupleType = ZodTuple.create;
const recordType = ZodRecord.create;
const mapType = ZodMap.create;
const setType = ZodSet.create;
const functionType = ZodFunction.create;
const lazyType = ZodLazy.create;
const literalType = ZodLiteral.create;
const enumType = ZodEnum.create;
const nativeEnumType = ZodNativeEnum.create;
const promiseType = ZodPromise.create;
const effectsType = ZodEffects.create;
const optionalType = ZodOptional.create;
const nullableType = ZodNullable.create;
const preprocessType = ZodEffects.createWithPreprocess;
const pipelineType = ZodPipeline.create;
const ostring = () => stringType().optional();
const onumber = () => numberType().optional();
const oboolean = () => booleanType().optional();
const coerce = {
    string: ((arg) => ZodString.create({ ...arg, coerce: true })),
    number: ((arg) => ZodNumber.create({ ...arg, coerce: true })),
    boolean: ((arg) => ZodBoolean.create({
        ...arg,
        coerce: true,
    })),
    bigint: ((arg) => ZodBigInt.create({ ...arg, coerce: true })),
    date: ((arg) => ZodDate.create({ ...arg, coerce: true })),
};
const NEVER = INVALID;

var z = /*#__PURE__*/Object.freeze({
    __proto__: null,
    defaultErrorMap: errorMap,
    setErrorMap: setErrorMap,
    getErrorMap: getErrorMap,
    makeIssue: makeIssue,
    EMPTY_PATH: EMPTY_PATH,
    addIssueToContext: addIssueToContext,
    ParseStatus: ParseStatus,
    INVALID: INVALID,
    DIRTY: DIRTY,
    OK: OK,
    isAborted: isAborted,
    isDirty: isDirty,
    isValid: isValid,
    isAsync: isAsync,
    get util () { return util; },
    get objectUtil () { return objectUtil; },
    ZodParsedType: ZodParsedType,
    getParsedType: getParsedType,
    ZodType: ZodType,
    ZodString: ZodString,
    ZodNumber: ZodNumber,
    ZodBigInt: ZodBigInt,
    ZodBoolean: ZodBoolean,
    ZodDate: ZodDate,
    ZodSymbol: ZodSymbol,
    ZodUndefined: ZodUndefined,
    ZodNull: ZodNull,
    ZodAny: ZodAny,
    ZodUnknown: ZodUnknown,
    ZodNever: ZodNever,
    ZodVoid: ZodVoid,
    ZodArray: ZodArray,
    ZodObject: ZodObject,
    ZodUnion: ZodUnion,
    ZodDiscriminatedUnion: ZodDiscriminatedUnion,
    ZodIntersection: ZodIntersection,
    ZodTuple: ZodTuple,
    ZodRecord: ZodRecord,
    ZodMap: ZodMap,
    ZodSet: ZodSet,
    ZodFunction: ZodFunction,
    ZodLazy: ZodLazy,
    ZodLiteral: ZodLiteral,
    ZodEnum: ZodEnum,
    ZodNativeEnum: ZodNativeEnum,
    ZodPromise: ZodPromise,
    ZodEffects: ZodEffects,
    ZodTransformer: ZodEffects,
    ZodOptional: ZodOptional,
    ZodNullable: ZodNullable,
    ZodDefault: ZodDefault,
    ZodCatch: ZodCatch,
    ZodNaN: ZodNaN,
    BRAND: BRAND,
    ZodBranded: ZodBranded,
    ZodPipeline: ZodPipeline,
    custom: custom,
    Schema: ZodType,
    ZodSchema: ZodType,
    late: late,
    get ZodFirstPartyTypeKind () { return ZodFirstPartyTypeKind; },
    coerce: coerce,
    any: anyType,
    array: arrayType,
    bigint: bigIntType,
    boolean: booleanType,
    date: dateType,
    discriminatedUnion: discriminatedUnionType,
    effect: effectsType,
    'enum': enumType,
    'function': functionType,
    'instanceof': instanceOfType,
    intersection: intersectionType,
    lazy: lazyType,
    literal: literalType,
    map: mapType,
    nan: nanType,
    nativeEnum: nativeEnumType,
    never: neverType,
    'null': nullType,
    nullable: nullableType,
    number: numberType,
    object: objectType,
    oboolean: oboolean,
    onumber: onumber,
    optional: optionalType,
    ostring: ostring,
    pipeline: pipelineType,
    preprocess: preprocessType,
    promise: promiseType,
    record: recordType,
    set: setType,
    strictObject: strictObjectType,
    string: stringType,
    symbol: symbolType,
    transformer: effectsType,
    tuple: tupleType,
    'undefined': undefinedType,
    union: unionType,
    unknown: unknownType,
    'void': voidType,
    NEVER: NEVER,
    ZodIssueCode: ZodIssueCode,
    quotelessJson: quotelessJson,
    ZodError: ZodError
});




/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/async module */
/******/ 	(() => {
/******/ 		var webpackQueues = typeof Symbol === "function" ? Symbol("webpack queues") : "__webpack_queues__";
/******/ 		var webpackExports = typeof Symbol === "function" ? Symbol("webpack exports") : "__webpack_exports__";
/******/ 		var webpackError = typeof Symbol === "function" ? Symbol("webpack error") : "__webpack_error__";
/******/ 		var resolveQueue = (queue) => {
/******/ 			if(queue && !queue.d) {
/******/ 				queue.d = 1;
/******/ 				queue.forEach((fn) => (fn.r--));
/******/ 				queue.forEach((fn) => (fn.r-- ? fn.r++ : fn()));
/******/ 			}
/******/ 		}
/******/ 		var wrapDeps = (deps) => (deps.map((dep) => {
/******/ 			if(dep !== null && typeof dep === "object") {
/******/ 				if(dep[webpackQueues]) return dep;
/******/ 				if(dep.then) {
/******/ 					var queue = [];
/******/ 					queue.d = 0;
/******/ 					dep.then((r) => {
/******/ 						obj[webpackExports] = r;
/******/ 						resolveQueue(queue);
/******/ 					}, (e) => {
/******/ 						obj[webpackError] = e;
/******/ 						resolveQueue(queue);
/******/ 					});
/******/ 					var obj = {};
/******/ 					obj[webpackQueues] = (fn) => (fn(queue));
/******/ 					return obj;
/******/ 				}
/******/ 			}
/******/ 			var ret = {};
/******/ 			ret[webpackQueues] = x => {};
/******/ 			ret[webpackExports] = dep;
/******/ 			return ret;
/******/ 		}));
/******/ 		__webpack_require__.a = (module, body, hasAwait) => {
/******/ 			var queue;
/******/ 			hasAwait && ((queue = []).d = 1);
/******/ 			var depQueues = new Set();
/******/ 			var exports = module.exports;
/******/ 			var currentDeps;
/******/ 			var outerResolve;
/******/ 			var reject;
/******/ 			var promise = new Promise((resolve, rej) => {
/******/ 				reject = rej;
/******/ 				outerResolve = resolve;
/******/ 			});
/******/ 			promise[webpackExports] = exports;
/******/ 			promise[webpackQueues] = (fn) => (queue && fn(queue), depQueues.forEach(fn), promise["catch"](x => {}));
/******/ 			module.exports = promise;
/******/ 			body((deps) => {
/******/ 				currentDeps = wrapDeps(deps);
/******/ 				var fn;
/******/ 				var getResult = () => (currentDeps.map((d) => {
/******/ 					if(d[webpackError]) throw d[webpackError];
/******/ 					return d[webpackExports];
/******/ 				}))
/******/ 				var promise = new Promise((resolve) => {
/******/ 					fn = () => (resolve(getResult));
/******/ 					fn.r = 0;
/******/ 					var fnQueue = (q) => (q !== queue && !depQueues.has(q) && (depQueues.add(q), q && !q.d && (fn.r++, q.push(fn))));
/******/ 					currentDeps.map((dep) => (dep[webpackQueues](fnQueue)));
/******/ 				});
/******/ 				return fn.r ? promise : getResult();
/******/ 			}, (err) => ((err ? reject(promise[webpackError] = err) : outerResolve(exports)), resolveQueue(queue)));
/******/ 			queue && (queue.d = 0);
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module used 'module' so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./build/main.js");
/******/ 	
/******/ })()
;
//# sourceMappingURL=typescript-integrant.js.map