module Stitch exposing
    ( AssociateSchema
    , Associations(..)
    , Model(..)
    , StitchingMsg(..)
    , SwitchCamerasMsg(..)
    , ToggleStitchingSizeMsg
    , associate
    , associationsAttribute
    , attribute
    , decode
    , decodeSwitchCameras
    , decodeToggleStitchingSize
    , deselect
    , initAssociations
    , names
    , sessionAttribute
    , summariseChanges
    , switch
    , switchCamerasEvent
    , tag
    , toString
    , toggle
    , togglePartial
    , toggleStitchingSizeEvent
    , unassociated
    , widthAttribute
    )

import Json.Decode as D
import Set



-- Decode a "stitching" field value.


decode : D.Decoder Model
decode =
    D.string
        |> D.andThen
            (\str ->
                if str == uni then
                    D.succeed UniStitching

                else if str == bi then
                    D.succeed BiStitching

                else if str == biPartial then
                    D.succeed BiPartialStitching

                else if str == tri then
                    D.succeed TriStitching

                else if str == triPartial then
                    D.succeed TriPartialStitching

                else if str == quad then
                    D.succeed QuadStitching

                else if str == quadPartial then
                    D.succeed QuadPartialStitching

                else
                    D.fail <| "unknown stitching: " ++ str
            )



-- Decode a switch cameras custom event.


decodeSwitchCameras : D.Decoder SwitchCamerasMsg
decodeSwitchCameras =
    D.field "detail" <|
        D.map2 SwitchCamerasMsg
            (D.field "camera1" D.string)
            (D.field "camera2" D.string)



-- Decode a toggle stitching size custom event.


decodeToggleStitchingSize : D.Decoder ToggleStitchingSizeMsg
decodeToggleStitchingSize =
    D.succeed ToggleStitchingSizeMsg



-- When multiple cameras are associated with a shutterbug they are stitched
-- together in a certain way into the single picture representing the
-- shutterbug's view.


type Model
    = -- No stitching. Either a single camera occupies the whole view or there
      -- are no associated cameras at all.
      UniStitching
      -- Fully overlapping. Both cameras occupy the whole view.
    | BiStitching
      -- Partially overlapping. The rear camera occupies the whole view. The
      -- closest occupies a smaller rectangle.
    | BiPartialStitching
      -- Fully overlapping. All three cameras occupy the whole view.
    | TriStitching
      -- Partially overlapping. The rear camera occupies the whole view. The two
      -- closer cameras occupy mutually exclusive smaller rectangles.
    | TriPartialStitching
      -- Fully overlapping. All four cameras occupy the whole view.
    | QuadStitching
      -- Partially overlapping. The rear camera occupies the whole view. The
      -- three closer cameras occupy mutually exclusive smaller rectangles.
    | QuadPartialStitching



-- Camera names and stitching method associated with a shutterbug.


type Associations
    = Uni (Maybe String)
    | Bi String String
    | BiPartial String String
    | Tri String String String
    | TriPartial String String String
    | Quad String String String String
    | QuadPartial String String String String


type Mode
    = UniMode -- One camera only mode.
    | FullMode -- All full size cameras mode.
    | PartialMode -- Mixed size cameras mode.



-- A camera associations request which can be sent to the Typescript integrant.


type alias AssociateSchema =
    { cameras : List String
    , stitching : String
    }


type SwitchCamerasMsg
    = SwitchCamerasMsg String String


type ToggleStitchingSizeMsg
    = ToggleStitchingSizeMsg


type StitchingMsg
    = SwitchedCameras SwitchCamerasMsg
    | ToggledStitchingSize ToggleStitchingSizeMsg


associationsAttribute : String
associationsAttribute =
    "associations"


sessionAttribute : String
sessionAttribute =
    "session"


switchCamerasEvent : String
switchCamerasEvent =
    tag ++ "-switch-cameras"


toggleStitchingSizeEvent : String
toggleStitchingSizeEvent =
    tag ++ "-toggle-stitching-size"


tag : String
tag =
    "stitching-chooser"


widthAttribute : String
widthAttribute =
    "w"



-- An empty associations.


unassociated : Associations
unassociated =
    Uni Nothing



-- Make a new camera associations request which can be sent to the Typescript
-- integrant.


associate : Associations -> AssociateSchema
associate associations =
    case associations of
        Uni maybeMajor ->
            case maybeMajor of
                Just major ->
                    AssociateSchema [ major ] <| toString UniStitching

                Nothing ->
                    AssociateSchema [] <| toString UniStitching

        Bi major minor ->
            AssociateSchema [ major, minor ] <| toString BiStitching

        BiPartial major minor ->
            AssociateSchema [ major, minor ] <| toString BiPartialStitching

        Tri major minor1 minor2 ->
            AssociateSchema [ major, minor1, minor2 ] <| toString TriStitching

        TriPartial major minor1 minor2 ->
            AssociateSchema [ major, minor1, minor2 ] <|
                toString TriPartialStitching

        Quad major minor1 minor2 minor3 ->
            AssociateSchema [ major, minor1, minor2, minor3 ] <|
                toString QuadStitching

        QuadPartial major minor1 minor2 minor3 ->
            AssociateSchema [ major, minor1, minor2, minor3 ] <|
                toString QuadPartialStitching



-- An attribute value containing the stitching method and camera names.


attribute : Associations -> String
attribute associations =
    let
        schema =
            associate associations
    in
    String.join "," <| schema.stitching :: schema.cameras



-- Check the number of associated cameras implied by the stitching is valid.


checkAssociatedCamerasLength : Model -> Int -> Result String ()
checkAssociatedCamerasLength stitching length =
    case stitching of
        UniStitching ->
            if length == 0 || length == 1 then
                Ok ()

            else
                Err "uni stitching expects 0 or 1 associated cameras"

        BiStitching ->
            case length of
                2 ->
                    Ok ()

                _ ->
                    Err "bi stitching expects 2 associated cameras"

        BiPartialStitching ->
            case length of
                2 ->
                    Ok ()

                _ ->
                    Err "biPartial stitching expects 2 associated cameras"

        TriStitching ->
            case length of
                3 ->
                    Ok ()

                _ ->
                    Err "tri stitching expects 3 associated cameras"

        TriPartialStitching ->
            case length of
                3 ->
                    Ok ()

                _ ->
                    Err "triPartial stitching expects 3 associated cameras"

        QuadStitching ->
            case length of
                4 ->
                    Ok ()

                _ ->
                    Err "quad stitching expects 4 associated cameras"

        QuadPartialStitching ->
            case length of
                4 ->
                    Ok ()

                _ ->
                    Err "quadPartial stitching expects 4 associated cameras"



-- If the camera is a member of the associations then remove it. The new
-- associations may have a different stitching as a result of removing the
-- camera.


deselect : Associations -> String -> Associations
deselect associations camera =
    if List.member camera <| names associations then
        toggle associations camera

    else
        associations



-- Make a new associated and check the combination of inputs is in a valid
-- state.


initAssociations : Model -> List String -> Result String Associations
initAssociations stitching associations =
    case checkAssociatedCamerasLength stitching (List.length associations) of
        Ok () ->
            case associations of
                [ major ] ->
                    Ok <| Uni <| Just major

                [ major, minor ] ->
                    case stitching of
                        BiStitching ->
                            Ok <| Bi major minor

                        _ ->
                            Ok <| BiPartial major minor

                [ major, minor1, minor2 ] ->
                    case stitching of
                        TriStitching ->
                            Ok <| Tri major minor1 minor2

                        _ ->
                            Ok <| TriPartial major minor1 minor2

                [ major, minor1, minor2, major3 ] ->
                    case stitching of
                        QuadStitching ->
                            Ok <| Quad major minor1 minor2 major3

                        _ ->
                            Ok <| QuadPartial major minor1 minor2 major3

                _ ->
                    Ok <| Uni Nothing

        Err err ->
            Err err


names : Associations -> List String
names associations =
    case associations of
        Uni maybeMajor ->
            case maybeMajor of
                Just major ->
                    [ major ]

                Nothing ->
                    []

        Bi major minor ->
            [ major, minor ]

        BiPartial major minor ->
            [ major, minor ]

        Tri major minor1 minor2 ->
            [ major, minor1, minor2 ]

        TriPartial major minor1 minor2 ->
            [ major, minor1, minor2 ]

        Quad major minor1 minor2 minor3 ->
            [ major, minor1, minor2, minor3 ]

        QuadPartial major minor1 minor2 minor3 ->
            [ major, minor1, minor2, minor3 ]


mode : Model -> Mode
mode stitching =
    case stitching of
        UniStitching ->
            UniMode

        BiStitching ->
            FullMode

        BiPartialStitching ->
            PartialMode

        TriStitching ->
            FullMode

        TriPartialStitching ->
            PartialMode

        QuadStitching ->
            FullMode

        QuadPartialStitching ->
            PartialMode



-- Set the names of the cameras in the associations in the given order. The
-- length of the list should match the number of camera names in the
-- associations.


setNames : Associations -> List String -> Associations
setNames associations cameras =
    if (List.length <| names associations) == List.length cameras then
        case cameras of
            [ major, minor1, minor2, minor3 ] ->
                case associations of
                    Quad _ _ _ _ ->
                        Quad major minor1 minor2 minor3

                    _ ->
                        QuadPartial major minor1 minor2 minor3

            [ major, minor1, minor2 ] ->
                case associations of
                    Tri _ _ _ ->
                        Tri major minor1 minor2

                    _ ->
                        TriPartial major minor1 minor2

            [ major, minor ] ->
                case associations of
                    Bi _ _ ->
                        Bi major minor

                    _ ->
                        BiPartial major minor

            [ major ] ->
                Uni <| Just major

            [] ->
                Uni Nothing

            _ ->
                associations

    else
        associations


stitchingOf : Associations -> Model
stitchingOf associations =
    case associations of
        Uni _ ->
            UniStitching

        Bi _ _ ->
            BiStitching

        BiPartial _ _ ->
            BiPartialStitching

        Tri _ _ _ ->
            TriStitching

        TriPartial _ _ _ ->
            TriPartialStitching

        Quad _ _ _ _ ->
            QuadStitching

        QuadPartial _ _ _ _ ->
            QuadPartialStitching


summariseChanges : Associations -> Associations -> String
summariseChanges from to =
    let
        fromNames =
            names from

        toNames =
            names to

        fromMode =
            mode <| stitchingOf from

        toMode =
            mode <| stitchingOf to

        adding =
            Set.diff (Set.fromList toNames) (Set.fromList fromNames)
                |> Set.toList
                |> List.sort

        removing =
            Set.diff (Set.fromList fromNames) (Set.fromList toNames)
                |> Set.toList
                |> List.sort

        toAdd =
            List.length adding

        toRemove =
            List.length removing
    in
    if toAdd + toRemove == 0 then
        if fromMode == toMode then
            if toNames == fromNames then
                ""

            else
                "\nAdjust camera positioning."

        else if toMode == FullMode then
            "\nSwitch to full size mode."

        else
            "\nSwitch to mixed size mode."

    else
        (case List.length adding of
            0 ->
                ""

            1 ->
                "\nAssociate 1 new camera: "
                    ++ String.join ", " adding
                    ++ "."

            n ->
                "\nAssociate "
                    ++ String.fromInt n
                    ++ " new cameras: "
                    ++ String.join ", " adding
                    ++ "."
        )
            ++ (case List.length removing of
                    0 ->
                        ""

                    1 ->
                        "\nDisassociate 1 camera: "
                            ++ String.join ", " removing
                            ++ "."

                    n ->
                        "\nDisassociate "
                            ++ String.fromInt n
                            ++ " cameras: "
                            ++ String.join ", " removing
                            ++ "."
               )
            ++ (if toMode == fromMode then
                    ""

                else
                    case toMode of
                        UniMode ->
                            "\nSwitch to single camera mode."

                        FullMode ->
                            "\nSwitch to full size mode."

                        PartialMode ->
                            "\nSwitch to mixed size mode."
               )



-- Switch the positions of two cameras in the associations.


switch : Associations -> String -> String -> Associations
switch associations camera1 camera2 =
    if
        (Set.size <|
            Set.intersect
                (Set.fromList <| names associations)
                (Set.fromList [ camera1, camera2 ])
        )
            == 2
    then
        List.map
            (\camera ->
                if camera == camera1 then
                    camera2

                else if camera == camera2 then
                    camera1

                else
                    camera
            )
            (names associations)
            |> setNames associations

    else
        associations



-- Add the camera if it doesn't exist in the associations otherwise remove it.


toggle : Associations -> String -> Associations
toggle associations camera =
    case associations of
        Uni maybeMajor ->
            case maybeMajor of
                Just major ->
                    if camera == major then
                        Uni Nothing

                    else
                        BiPartial major camera

                Nothing ->
                    Uni <| Just camera

        Bi major minor ->
            if List.member camera [ major, minor ] then
                if camera == major then
                    Uni <| Just minor

                else
                    Uni <| Just major

            else
                Tri major minor camera

        BiPartial major minor ->
            if List.member camera [ major, minor ] then
                if camera == major then
                    Uni <| Just minor

                else
                    Uni <| Just major

            else
                TriPartial major camera minor

        Tri major minor1 minor2 ->
            if List.member camera [ major, minor1, minor2 ] then
                if camera == major then
                    Bi minor1 minor2

                else if camera == minor1 then
                    Bi major minor2

                else
                    Bi major minor1

            else
                Quad major minor1 minor2 camera

        TriPartial major minor1 minor2 ->
            if List.member camera [ major, minor1, minor2 ] then
                if camera == major then
                    BiPartial minor1 minor2

                else if camera == minor1 then
                    BiPartial major minor2

                else
                    BiPartial major minor1

            else
                QuadPartial major minor1 camera minor2

        Quad major minor1 minor2 minor3 ->
            if List.member camera [ major, minor1, minor2, minor3 ] then
                if camera == major then
                    Tri minor1 minor2 minor3

                else if camera == minor1 then
                    Tri major minor2 minor3

                else if camera == minor2 then
                    Tri major minor1 minor3

                else
                    Tri major minor1 minor2

            else
                Quad camera major minor1 minor2

        QuadPartial major minor1 minor2 minor3 ->
            if List.member camera [ major, minor1, minor2, minor3 ] then
                if camera == major then
                    TriPartial minor1 minor2 minor3

                else if camera == minor1 then
                    TriPartial major minor2 minor3

                else if camera == minor2 then
                    TriPartial major minor1 minor3

                else
                    TriPartial major minor1 minor2

            else
                QuadPartial camera major minor1 minor2



-- Toggle between partially overlapping and full size modes where possible.


togglePartial : Associations -> Associations
togglePartial associations =
    case associations of
        Uni _ ->
            associations

        Bi major minor ->
            BiPartial major minor

        BiPartial major minor ->
            Bi major minor

        Tri major minor1 minor2 ->
            TriPartial major minor1 minor2

        TriPartial major minor1 minor2 ->
            Tri major minor1 minor2

        Quad major minor1 minor2 minor3 ->
            QuadPartial major minor1 minor2 minor3

        QuadPartial major minor1 minor2 minor3 ->
            Quad major minor1 minor2 minor3


toString : Model -> String
toString stitching =
    case stitching of
        UniStitching ->
            uni

        BiStitching ->
            bi

        BiPartialStitching ->
            biPartial

        TriStitching ->
            tri

        TriPartialStitching ->
            triPartial

        QuadStitching ->
            quad

        QuadPartialStitching ->
            quadPartial


uni : String
uni =
    "uni"


bi : String
bi =
    "bi"


biPartial : String
biPartial =
    "biPartial"


tri : String
tri =
    "tri"


triPartial : String
triPartial =
    "triPartial"


quad : String
quad =
    "quad"


quadPartial : String
quadPartial =
    "quadPartial"
