module View.Settings exposing (popUp)

import Css as C
import Css.Global as CG
import Html.Styled as H exposing (Html)
import Html.Styled.Attributes as HA
import LifeCycle
import Maybe.Extra
import My.Extra as ME
import PixScreen
import Theme exposing (Theme)


popUp :
    Theme
    ->
        { inaction : a
        , border : a
        , fit : a
        , full : PixScreen.Model -> a
        , lock : PixScreen.Model -> a
        , selectable : a
        }
    -> PixScreen.Model
    -> Bool
    -> Maybe LifeCycle.WSCloseEvent
    -> Html a
popUp theme msg screen on maybeNotification =
    let
        gradientStop =
            "--settings-pop-up-gradient-stop"

        gradientStopOnValue =
            "10%"

        gradientStopOffValue =
            "100%"
    in
    H.div
        [ HA.css
            [ C.property "background" <|
                String.join
                    ", "
                    [ "linear-gradient(to left"
                    , "rgba(255,255,255,1.0)"
                    , "rgba(255,255,255,1.0) var("
                        ++ gradientStop
                        ++ ")"
                    , "rgba(255,255,255,0.8) calc(var("
                        ++ gradientStop
                        ++ ") + 15%)"
                    , "rgba(255,255,255,0.8))"
                    ]
            , C.property gradientStop
                (ME.ternary on gradientStopOnValue gradientStopOffValue)
            , C.borderBottom3 (C.em theme.buttonBorder)
                C.solid
                theme.primary
            , C.borderLeft3 (C.em theme.buttonBorder)
                C.solid
                theme.primary
            , C.borderTop3 (C.em theme.buttonBorder)
                C.solid
                theme.primary
            , C.borderTopLeftRadius (C.em theme.buttonBorderRadius)
            , C.borderBottomLeftRadius (C.em theme.buttonBorderRadius)
            , C.boxShadow5
                (C.px 0)
                (C.px 0)
                (ME.ternary on (C.px 20) (C.px 0))
                (ME.ternary on (C.px 4) (C.px 0))
                (C.rgb 112 145 145)
            , C.padding4
                (C.em theme.buttonPadding)
                (C.em theme.buttonPadding)
                (C.em 0)
                (C.em theme.buttonPadding)
            , C.position C.absolute
            , C.transform <|
                C.translate2
                    -- Ideally 100%/0% respectively but mitigating transform
                    -- related pixel glitches.
                    (C.pct <| ME.ternary on -98 2)
                    (C.pct -50)
            , C.property
                "transition"
              <|
                String.join ", "
                    [ "transform " ++ Theme.popUpDurationMS
                    , "box-shadow " ++ Theme.popUpDurationMS
                    , gradientStop ++ " " ++ Theme.popUpDurationMS
                    ]
            , C.property "user-select" (ME.ternary on "auto" "none")
            ]
        ]
        [ CG.global
            [ CG.selector ("@property " ++ gradientStop)
                [ C.property "syntax" "'<percentage>'"
                , C.property "inherits" "false"
                , C.property "initial-value" gradientStopOffValue
                ]
            ]
        , H.div
            [ HA.css
                [ C.displayFlex
                , C.flexDirection C.rowReverse
                , C.property "gap" <|
                    String.fromFloat theme.buttonPadding
                        ++ "em"
                ]
            ]
            [ H.div
                [ HA.css
                    [ C.displayFlex, C.flexDirection C.column ]
                ]
                [ PixScreen.fitScreenButton theme msg.fit screen
                , PixScreen.borderButton theme msg.border screen
                , PixScreen.fullscreenButton theme (msg.full screen) screen
                ]
            , H.div
                [ HA.css
                    [ C.displayFlex, C.flexDirection C.column ]
                ]
                [ PixScreen.pointerLockButton
                    theme
                    (msg.lock screen)
                    msg.inaction
                    screen
                    maybeNotification
                , PixScreen.textSelectButton
                    theme
                    msg.selectable
                    msg.inaction
                    (Maybe.Extra.isJust maybeNotification)
                    screen
                ]
            ]
        ]
