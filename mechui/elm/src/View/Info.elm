module View.Info exposing (popup)

import Css as C
import Css.Global as CG
import Css.Transitions as CT
import Html.Styled as H exposing (Html)
import Html.Styled.Attributes as HA
import My.Extra as ME
import My.Html
import PopUp
import Static.About
import String.Extra
import Theme exposing (Theme)
import Url
import View


attributions : Theme -> (String -> a) -> Static.About.Mechane -> Html a
attributions theme copyMsg about =
    let
        main =
            mainLicense theme copyMsg about.name

        licenseLinkWidth =
            linkSectionWidth about.license about.alternativeLicense

        spdxId =
            Static.About.spdxId about.license about.alternativeLicense

        titleCaseName =
            String.Extra.toTitleCase about.name

        parseElmImports imports =
            List.filterMap
                (\url ->
                    Maybe.andThen
                        (\parsedURL ->
                            case String.split "/" parsedURL.path of
                                _ :: _ :: licensor :: name :: _ ->
                                    Just ( name, licensor, url )

                                _ ->
                                    Nothing
                        )
                        (Url.fromString url)
                )
                imports
                |> List.sort

        parseGoImports imports =
            List.filterMap
                (\url ->
                    Maybe.andThen
                        (\parsedURL ->
                            case String.split "/" parsedURL.path of
                                _ :: _ :: licensor :: name :: _ ->
                                    Just ( name, licensor, url )

                                _ :: licensor :: name :: _ ->
                                    Just ( name, licensor, url )

                                _ ->
                                    Nothing
                        )
                        (Url.fromString url)
                )
                imports
                |> List.sort

        parseTSImports imports =
            List.filterMap
                (\url ->
                    Maybe.andThen
                        (\parsedURL ->
                            case String.split "/" parsedURL.path of
                                _ :: _ :: name ->
                                    Just <| ( String.join "/" name, url )

                                _ ->
                                    Nothing
                        )
                        (Url.fromString url)
                )
                imports
                |> List.sort

        itemStyles =
            [ HA.css [ C.padding <| C.em theme.buttonPadding ] ]

        viewImports imports =
            List.indexedMap
                (\i ( name, licensor, url ) ->
                    H.li
                        itemStyles
                        [ My.Html.a url name
                        , H.text <|
                            " courtesy of "
                                ++ licensor
                                ++ listItemSeparator i imports
                        ]
                )
                imports

        viewTSImports imports =
            List.indexedMap
                (\i ( name, url ) ->
                    H.li
                        itemStyles
                        [ My.Html.a url name
                        , H.text <| listItemSeparator i imports
                        ]
                )
                imports
    in
    H.div
        [ HA.css
            [ C.paddingLeft <| C.em theme.buttonPadding
            , C.paddingRight <| C.em theme.buttonPadding
            ]
        ]
        [ -- Mechane name, author, and blurb.
          intro theme about

        -- License.
        , main about.license spdxId licenseLinkWidth

        -- Alt license.
        , main about.alternativeLicense spdxId licenseLinkWidth

        -- Fonts.
        , H.h2 [] [ H.text "Fonts" ]
        , H.p []
            [ H.text <|
                "Open source fonts distributed with "
                    ++ titleCaseName
                    ++ ":"
            ]
        , H.ul [] <| dependencies theme copyMsg about.fonts
        , H.h2 [] [ H.text "Binary dependencies for users" ]
        , H.p []
            [ H.text <|
                "Binaries that need to be preinstalled on the system for "
                    ++ titleCaseName
                    ++ " to run:"
            ]
        , H.ul [] <| dependencies theme copyMsg about.binaries
        , H.h2 [] [ H.text "Binary dependencies for developers" ]
        , H.p []
            [ H.text <|
                "Other binaries that need to be preinstalled on the system for "
                    ++ titleCaseName
                    ++ " to be fully compiled from source:"
            ]
        , H.ul [] <| dependencies theme copyMsg about.devBinaries
        , H.h2 [] [ H.text "Imported packages" ]
        , H.p []
            [ H.text <|
                "Dependencies that were introduced through the import mechanism "
                    ++ "of each of the main programming languages that "
                    ++ titleCaseName
                    ++ " is written in:"
            ]
        , H.ul []
            [ H.li []
                [ H.h3 [] [ H.text "Elm language imports" ]
                , H.ul [] <| viewImports (parseElmImports about.elmImports)
                ]
            , H.li []
                [ H.h3 [] [ H.text "Go language imports" ]
                , H.ul [] <| viewImports (parseGoImports about.goImports)
                ]
            , H.li []
                [ H.h3 [] [ H.text "Typescript language imports" ]
                , H.ul [] <| viewTSImports (parseTSImports about.tsImports)
                ]
            , H.li []
                [ H.h3 [] [ H.text "Typescript language development imports" ]
                , H.ul [] <| viewTSImports (parseTSImports about.tsDevImports)
                ]
            ]
        ]


author : List Static.About.Author -> Html a
author authors =
    (case authors of
        [ sole ] ->
            [ My.Html.a sole.url sole.name ]

        [] ->
            [ H.text "Unknown" ]

        _ ->
            List.map
                (\licensor -> My.Html.a licensor.url licensor.name)
                authors
                |> List.intersperse (H.text ", ")
    )
        |> H.span []


dependencies :
    Theme
    -> (String -> a)
    -> List Static.About.Dependency
    -> List (Html a)
dependencies theme copyMsg deps =
    List.map
        (\dep ->
            H.li
                [ HA.css [ C.padding <| C.em 0 ] ]
                [ H.div
                    [ HA.css
                        [ C.displayFlex
                        , C.justifyContent C.spaceBetween
                        ]
                    ]
                    [ H.div
                        [ HA.css
                            [ C.display C.inlineBlock
                            ]
                        ]
                        [ H.p []
                            [ My.Html.a
                                dep.url
                                dep.name
                            , H.text " courtesy of "
                            , H.text <| String.join " / " dep.licensor
                            , H.text "."
                            ]
                        , H.p []
                            [ H.text "Published under the "
                            , My.Html.a dep.license.url dep.license.name
                            , H.text " license."
                            ]
                        ]
                    , H.div
                        [ HA.css
                            [ C.alignSelf C.center
                            , C.display C.inlineBlock
                            ]
                        ]
                        [ H.button
                            [ My.Html.onClickPreventDefault <|
                                copyMsg dep.license.text
                            , HA.title <|
                                "Copy the "
                                    ++ dep.name
                                    ++ " license legal code to the clipboard."
                            , HA.css <| .standard <| Theme.button theme
                            ]
                            [ View.buttonIcon theme "content_copy" ]
                        ]
                    ]
                ]
        )
        deps


docs : Html a
docs =
    H.div []
        [ H.h1 [] [ H.text "Docs" ]
        , H.p []
            [ H.text "There are no docs for this world at the moment." ]
        ]


intro :
    Theme
    ->
        { r
            | author : List Static.About.Author
            , blurb : List String
            , link : String
            , name : String
        }
    -> Html a
intro theme about =
    -- Mechane name, author, and blurb.
    H.div []
        [ H.div
            [ HA.css
                [ C.property "backdrop-filter" "blur(5px)"
                , C.property "box-shadow" theme.boxShadow
                , C.borderColor theme.primary
                , C.borderRadius <| C.em theme.buttonBorderRadius
                , C.borderStyle C.solid
                , C.borderBottomWidth <| C.em 0
                , C.borderLeftWidth <| C.em <| theme.buttonBorderRadius
                , C.borderRightWidth <| C.em <| theme.buttonBorderRadius
                , C.borderTopWidth <| C.em 0
                , C.marginTop <| C.em theme.buttonPadding
                , C.padding <| C.em theme.buttonPadding
                ]
            ]
            [ H.h1
                [ HA.css
                    [ C.display C.inlineBlock
                    , C.marginTop <| C.em 0
                    ]
                ]
                [ My.Html.a about.link about.name ]
            , H.span
                [ HA.css
                    [ C.fontStyle C.italic
                    , C.transform <| C.translateY <| C.em 0.5
                    ]
                ]
                [ H.text <| " courtesy of "
                , author about.author
                ]
            , H.div [] <|
                List.indexedMap
                    (\i sentence ->
                        H.p
                            [ ME.appendWhen
                                []
                                [ ( i == 0
                                  , [ C.marginTop <| C.em 0 ]
                                  )
                                , ( i == -1 + List.length about.blurb
                                  , [ C.marginBottom <| C.em 0 ]
                                  )
                                ]
                                |> HA.css
                            ]
                            [ H.text sentence ]
                    )
                    about.blurb
            ]
        ]


listItemSeparator : Int -> List a -> String
listItemSeparator index list =
    if index == List.length list - 1 then
        "."

    else
        ";"



-- Decide the width of the license name link so the badges will line up nicely.


linkSectionWidth : Static.About.License -> Static.About.License -> Float
linkSectionWidth license1 license2 =
    let
        maxWidth =
            43
    in
    (case String.length license2.name of
        0 ->
            String.length license1.name

        l2 ->
            Basics.max (String.length license1.name) l2
    )
        |> Basics.min maxWidth
        |> Basics.toFloat


mainLicense :
    Theme
    -> (String -> a)
    -> String
    -> Static.About.License
    -> String
    -> Float
    -> Html a
mainLicense theme copyMsg productName license spdxId linkWidth =
    let
        badgeUrl =
            "data:image/png;base64," ++ license.badge

        backgroundImageStyles =
            ME.useIf
                (license.badge /= "")
                [ C.backgroundBlendMode C.screenBlendMode
                , C.backgroundColor theme.accent
                , C.backgroundImage <| C.url badgeUrl
                , C.backgroundPosition C.center
                , C.backgroundSize2
                    C.auto
                    (C.em <| theme.buttonHeight * 3)
                , C.borderColor theme.accent
                , C.borderRadius <| C.em <| theme.buttonBorderRadius / 4
                , C.borderStyle C.ridge
                , C.borderWidth <| C.em <| theme.buttonBorder / 2
                , C.padding <| C.em theme.buttonPadding
                ]

        containerStyles =
            ME.appendIf (license.badge /= "")
                [ C.alignItems C.flexStart
                , C.property "box-shadow" theme.boxShadow
                , C.borderColor theme.primary
                , C.borderRadius <| C.em theme.buttonBorderRadius
                , C.borderStyle C.solid
                , C.borderBottomWidth <| C.em 0
                , C.borderLeftWidth <| C.em <| theme.buttonBorderRadius
                , C.borderRightWidth <| C.em <| theme.buttonBorderRadius
                , C.borderTopWidth <| C.em 0
                , C.displayFlex
                , C.justifyContent C.spaceBetween
                , C.padding <| C.em theme.buttonPadding
                ]
                [ C.backgroundColor <| C.rgba 255 255 255 0.7
                , C.property "backdrop-filter" "sepia(0.5)"
                ]
    in
    H.div []
        [ -- Empty heading for its margin.
          H.h2 [] []
        , H.div
            [ HA.css backgroundImageStyles ]
            [ H.div
                [ HA.css containerStyles, HA.title spdxId ]
                [ H.div
                    [ HA.css
                        [ C.displayFlex
                        , C.flexDirection C.column
                        ]
                    ]
                    [ H.h2
                        [ HA.css [ C.marginTop <| C.em 0 ] ]
                        [ H.text "License" ]
                    , H.span
                        [ HA.css [ C.width <| C.ch linkWidth ] ]
                        [ My.Html.a license.url license.name ]
                    ]
                , if license.badge == "" then
                    H.text ""

                  else
                    H.img
                        [ HA.alt "license badge"
                        , HA.css
                            [ C.alignSelf C.center
                            , C.height <| C.em theme.buttonHeight
                            ]
                        , HA.src badgeUrl
                        ]
                        []
                , H.div
                    [ HA.css
                        [ C.alignSelf C.center
                        , C.height <| C.em theme.buttonHeight
                        ]
                    ]
                    [ H.button
                        [ My.Html.onClickPreventDefault <|
                            copyMsg license.text
                        , HA.title <|
                            "Copy the "
                                ++ productName
                                ++ " "
                                ++ license.id
                                ++ " "
                                ++ " license legal code to the clipboard."
                        , HA.css <| .standard <| Theme.button theme
                        ]
                        [ View.buttonIcon theme "content_copy" ]
                    ]
                ]
            ]
        ]


monitor : Theme -> List (List String) -> Html a
monitor theme errors =
    H.div []
        [ H.h1 [] [ H.text "Errors" ]
        , if List.length errors == 0 then
            H.p []
                [ H.text "Looks good! There are no errors." ]

          else
            View.errorsList theme errors
        ]


popup :
    Theme
    -> (String -> a)
    -> (PopUp.InfoTabMsg -> a)
    -> PopUp.Model
    -> List (List String)
    -> Html a
popup theme copyMsg tabMsg popups errors =
    let
        gradientStop =
            "--info-pop-up-gradient-stop"

        gradientStopOnValue =
            "10%"

        gradientStopOffValue =
            "100%"

        h =
            C.calc (C.ch 50) C.plus (C.em theme.buttonPadding)

        infoPopUp =
            case popups.pixelsArea of
                Just PopUp.InfoPopUp ->
                    True

                _ ->
                    False
    in
    H.div
        [ HA.css
            [ C.property "background" <|
                String.join
                    ", "
                    [ "linear-gradient(to top"
                    , "rgba(255,255,255,1.0)"
                    , "rgba(255,255,255,1.0) var("
                        ++ gradientStop
                        ++ ")"
                    , "rgba(255,255,255,0.8) calc(var("
                        ++ gradientStop
                        ++ ") + 15%)"
                    , "rgba(255,255,255,0.8))"
                    ]
            , C.property gradientStop
                (ME.ternary infoPopUp gradientStopOnValue gradientStopOffValue)
            , C.borderLeft3 (C.em theme.buttonBorder)
                C.solid
                theme.primary
            , C.borderRight3 (C.em theme.buttonBorder)
                C.solid
                theme.primary
            , C.borderTop3 (C.em theme.buttonBorder)
                C.solid
                theme.primary
            , C.borderTopLeftRadius (C.em theme.buttonBorderRadius)
            , C.borderTopRightRadius (C.em theme.buttonBorderRadius)
            , C.boxShadow5
                (C.px 0)
                (C.px 0)
                (ME.ternary infoPopUp (C.px 20) (C.px 0))
                (ME.ternary infoPopUp (C.px 4) (C.px 0))
                (C.rgb 112 145 145)
            , C.padding4 (C.em theme.buttonPadding)
                (C.em theme.buttonPadding)
                (C.em theme.buttonPadding)
                (C.em 0)
            , C.position C.absolute
            , C.transform <|
                C.translate2 (C.pct -50) <|
                    -- Not quite 100% to mitigate transform pixel glitch.
                    C.pct (ME.ternary infoPopUp -99.75 0)
            , C.property
                "transition"
              <|
                String.join ", "
                    [ "transform " ++ Theme.popUpDurationMS
                    , "box-shadow " ++ Theme.popUpDurationMS
                    , gradientStop ++ " " ++ Theme.popUpDurationMS
                    ]
            , C.property "user-select" (ME.ternary infoPopUp "auto" "none")
            , C.width <|
                C.calc
                    (C.ch 80)
                    C.plus
                    -- Left column: button + 2 paddings + 2 inner paddings
                    (C.em (4 * theme.buttonPadding + theme.buttonHeight))
            , C.height h
            ]
        ]
        [ CG.global
            [ CG.selector ("@property " ++ gradientStop)
                [ C.property "syntax" "'<percentage>'"
                , C.property "inherits" "false"
                , C.property "initial-value" gradientStopOffValue
                ]
            ]
        , H.div
            [ HA.css [ C.displayFlex ] ]
            [ H.div
                [ HA.css
                    [ C.displayFlex
                    , C.flexDirection C.column
                    , C.height h
                    , C.padding4
                        (C.em 0)
                        (C.em theme.buttonPadding)
                        (C.em 0)
                        (C.em theme.buttonPadding)
                    ]
                ]
                [ worldButton
                    theme
                    (tabMsg (PopUp.InfoTabMsg PopUp.WorldTab))
                    popups
                , docsButton
                    theme
                    (tabMsg (PopUp.InfoTabMsg PopUp.DocsTab))
                    popups
                , monitorButton
                    theme
                    (tabMsg (PopUp.InfoTabMsg PopUp.MonitorTab))
                    popups
                , attributionButton
                    theme
                    (tabMsg (PopUp.InfoTabMsg PopUp.AttributionsTab))
                    popups
                ]
            , tabContent theme copyMsg popups errors h
            ]
        ]


tabContent :
    Theme
    -> (String -> a)
    -> PopUp.Model
    -> List (List String)
    -> C.CalculatedLength
    -> Html a
tabContent theme copyMsg popups errors h =
    let
        tabs =
            [ ( PopUp.WorldTab
              , world theme copyMsg <| Static.About.world popups.about
              )
            , ( PopUp.DocsTab, docs )
            , ( PopUp.MonitorTab, monitor theme errors )
            , ( PopUp.AttributionsTab
              , attributions theme copyMsg <| Static.About.mechane popups.about
              )
            ]

        absTabContainer : ( PopUp.InfoTab, Html a ) -> Html a
        absTabContainer =
            \( tab, content ) ->
                let
                    active =
                        tab == popups.infoTab

                    duration =
                        1000
                in
                H.div
                    [ HA.css
                        [ C.property "filter" <|
                            ME.ternary active
                                "none"
                                "drop-shadow(0 0 0.4em black)"
                        , C.height h
                        , C.opacity <| C.num <| ME.ternary active 1 0
                        , C.overflowY C.auto
                        , C.position C.absolute

                        -- The absolutely positioned tab content for every tab
                        -- is positioned off-tab to the right waiting to
                        -- transition in.
                        , C.transforms
                            [ C.translateX <| C.pct <| ME.ternary active 0 100
                            ]
                        , CT.transition
                            [ CT.filter duration
                            , CT.opacity duration
                            , CT.transform duration
                            ]
                        , C.width <| C.pct 100 -- Occupy full width for X transforms.
                        , C.zIndex <| C.int <| ME.ternary active 1 0
                        ]
                    ]
                    [ content ]
    in
    H.div
        [ HA.css
            [ C.overflowX C.hidden
            , C.position C.relative
            , C.width <| C.pct 100
            ]
        ]
        (List.map absTabContainer tabs)


world : Theme -> (String -> a) -> Static.About.World -> Html a
world theme copyMsg about =
    let
        main =
            mainLicense theme copyMsg about.name

        licenseLinkWidth =
            linkSectionWidth about.license about.alternativeLicense

        spdxId =
            Static.About.spdxId about.license about.alternativeLicense

        titleCaseName =
            String.Extra.toTitleCase about.name
    in
    ME.appendWhen
        [ intro theme about

        -- License.
        , main about.license spdxId licenseLinkWidth
        ]
        [ -- Alt license.
          ( not <| String.isEmpty about.alternativeLicense.name
          , [ main about.alternativeLicense spdxId licenseLinkWidth
            ]
          )

        -- User binary deps.
        , ( List.length about.binaries > 0
          , [ H.h2 [] [ H.text "Binary dependencies for users" ]
            , H.p []
                [ H.text <|
                    "Binaries that need to be preinstalled on the system for "
                        ++ titleCaseName
                        ++ " to run:"
                ]
            , H.ul [] <| dependencies theme copyMsg about.binaries
            ]
          )

        -- Dev binary deps.
        , ( List.length about.devBinaries > 0
          , [ H.h2 [] [ H.text "Binary dependencies for developers" ]
            , H.p []
                [ H.text <|
                    "Other binaries that need to be preinstalled on the system for "
                        ++ titleCaseName
                        ++ " to be fully compiled from source:"
                ]
            , H.ul [] <| dependencies theme copyMsg about.devBinaries
            ]
          )
        ]
        |> H.div
            [ HA.css
                [ C.paddingLeft <| C.em theme.buttonPadding
                , C.paddingRight <| C.em theme.buttonPadding
                ]
            ]


attributionButton : Theme -> a -> PopUp.Model -> Html a
attributionButton theme msg popups =
    H.button
        [ My.Html.onClickPreventDefault msg
        , HA.title "Inspect third party attributions."
        , HA.css <| .standard <| Theme.buttonWithMargin theme
        ]
        [ View.expanderButtonIcon
            theme
            "attribution"
            (PopUp.infoAttributionsTab popups)
            True
        ]


docsButton : Theme -> a -> PopUp.Model -> Html a
docsButton theme msg popups =
    H.button
        [ My.Html.onClickPreventDefault msg
        , HA.title "Access a beginner's guide to using this world."
        , HA.css <| .standard <| Theme.buttonWithMargin theme
        ]
        [ View.expanderButtonIcon
            theme
            "school"
            (PopUp.infoDocsTab popups)
            True
        ]


monitorButton : Theme -> a -> PopUp.Model -> Html a
monitorButton theme msg popups =
    H.button
        [ My.Html.onClickPreventDefault msg
        , HA.title "Monitor the performance of this world."
        , HA.css <| .standard <| Theme.buttonWithMargin theme
        ]
        [ View.expanderButtonIcon
            theme
            "analytics"
            (PopUp.infoMonitorTab popups)
            True
        ]


worldButton : Theme -> a -> PopUp.Model -> Html a
worldButton theme msg popups =
    H.button
        [ My.Html.onClickPreventDefault msg
        , HA.title "View details about this world."
        , HA.css <| .standard <| Theme.buttonWithMargin theme
        ]
        [ View.expanderButtonIcon
            theme
            "info"
            (PopUp.infoWorldTab popups)
            True
        ]
