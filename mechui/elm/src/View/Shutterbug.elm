module View.Shutterbug exposing (actions, inputBox)

import Css as C
import Html.Styled as H exposing (Html)
import Html.Styled.Attributes as HA
import Html.Styled.Events as HE
import Json.Decode as D
import LifeCycle
import My.Extra as ME
import My.Html
import Regex
import Shutterbug
import Theme exposing (Theme)
import View



-- A button to toggle the shutterbug actions popup.


actions :
    { theme : Theme
    , tabMsg : a
    , togglePopUpMsg : a
    , copyMsg : a
    , inactionMsg : a
    , maybeNotification : Maybe LifeCycle.WSCloseEvent
    , shutterbugActionsPopUp : Bool
    , extantUrl : String
    }
    -> Html a
actions data =
    H.div []
        [ popUp data
        , My.Html.safeClickButton
            data.togglePopUpMsg
            data.inactionMsg
            [ HA.css <| .standard <| Theme.buttonWithMargin data.theme
            , HA.title "Access shutterbug actions."
            ]
            [ View.expanderButtonIcon
                data.theme
                "camera"
                data.shutterbugActionsPopUp
                False
            ]
        ]



-- A button to copy the shutterbug to the clipboard.


copyButton : Theme -> a -> a -> Maybe LifeCycle.WSCloseEvent -> Html a
copyButton theme msg inactionMsg maybeNotification =
    let
        useless =
            "( ⚠ It might not be useful now the server is shut down.)"

        tooltip =
            "Copy this tab's shutterbug to the clipboard."
                ++ (case maybeNotification of
                        Just notification ->
                            if LifeCycle.isNormalWSCloseEvent notification then
                                "\n" ++ useless

                            else
                                ""

                        Nothing ->
                            ""
                   )
    in
    My.Html.safeClickButton
        msg
        inactionMsg
        [ HA.css <| .standard <| Theme.buttonWithMargin theme
        , HA.title tooltip
        ]
        [ View.buttonIcon theme "content_copy" ]



-- An input box and buttons where a shutterbug can be entered and submitted.


inputBox :
    Theme
    -> (String -> a)
    -> (String -> a)
    -> a
    -> { input : String, unmasked : Bool }
    -> Html a
inputBox theme submitMsg changeInputMsg changeMaskMsg shutterbug =
    let
        inputOk =
            if String.length shutterbug.input > 0 then
                if Regex.contains Shutterbug.regex shutterbug.input then
                    Just True

                else
                    Just False

            else
                Nothing

        tickOrError ok =
            if ok then
                H.span
                    [ HA.class View.defaultIconStyle
                    , HA.css
                        [ C.color Theme.gold
                        , C.verticalAlign C.textBottom
                        ]
                    ]
                    [ H.text "done_outline" ]

            else
                H.span
                    [ HA.class View.defaultIconStyle
                    , HA.css
                        [ C.color Theme.red
                        , C.verticalAlign C.textBottom
                        ]
                    ]
                    [ H.text "error" ]

        -- Checks whether the non padding characters are compliant.
        b64regex =
            case Regex.fromString "^[\\w-]+=?$" of
                Just r ->
                    r

                Nothing ->
                    Regex.never

        charactersOk =
            Regex.contains b64regex shutterbug.input

        paddingOk =
            String.endsWith "=" shutterbug.input

        inputHelp =
            [ H.p []
                [ H.text "The shutterbug should be:" ]
            , H.ul []
                [ H.li []
                    [ tickOrError <| String.length shutterbug.input == 44
                    , H.text " 44 characters long;"
                    ]
                , H.li []
                    [ tickOrError <| charactersOk && paddingOk
                    , H.text " a "
                    , My.Html.a "https://en.wikipedia.org/wiki/Base64" "Base64URL"
                    , H.text " encoding which:"
                    , H.ul []
                        [ H.li []
                            [ tickOrError <| paddingOk
                            , H.text " ends with a '"
                            , H.span
                                [ HA.css [ C.fontWeight C.bold ] ]
                                [ H.text "=" ]
                            , H.text "' (the padding character);"
                            ]
                        , H.li []
                            [ tickOrError <| charactersOk
                            , H.text
                                " contains only (apart from the padding character):"
                            , H.ul []
                                [ H.li [] [ H.text "a-z" ]
                                , H.li [] [ H.text "A-Z" ]
                                , H.li [] [ H.text "0-9" ]
                                , H.li [] [ H.text "_-" ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
    in
    H.div
        [ HA.css
            [ C.alignItems C.center
            , C.displayFlex
            , C.flexDirection C.column
            ]
        ]
        [ H.p
            [ HA.css
                [ C.marginBottom <| C.em 3
                , C.marginTop <| C.em 3
                ]
            ]
            [ H.text <|
                "Connect to Mechane using an already existing shutterbug."
            ]
        , H.div
            [ HA.css
                [ C.alignItems C.center
                , C.border3 (C.em theme.buttonBorder) C.double theme.primary
                , C.borderRadius <| C.em theme.buttonBorderRadius
                , C.displayFlex
                , C.padding <| C.em theme.buttonPadding
                ]
            ]
            [ H.input
                ([ HA.css
                    [ C.backgroundColor Theme.black
                    , C.borderColor Theme.gold
                    , C.borderRadius <| C.em 0.5
                    , C.borderStyle C.inset
                    , C.borderWidth <| C.em 0.5
                    , C.color Theme.consoleGreen
                    , C.property "caret-color" <|
                        Theme.colToString Theme.consoleGreen
                    , C.fontSize <| C.em 1
                    , C.height <| C.em 2
                    , C.marginRight <| C.em theme.buttonPadding
                    , C.property "user-select" "none"
                    , C.width <| C.ch 44
                    , C.pseudoClass
                        "focus"
                        [ C.outlineColor <| Theme.dodgerBlue
                        , C.outlineStyle C.solid
                        , C.outlineWidth <| C.em 0.3
                        ]
                    , C.pseudoClass
                        ":placeholder"
                        [ C.color Theme.consoleGreen ]
                    ]
                 , HA.autocomplete False

                 --Padding is mandatory, thus is the maximum length.
                 , HA.minlength Shutterbug.maxLength
                 , HA.maxlength Shutterbug.maxLength
                 , HA.pattern "[\\w-]{43}="
                 , HA.placeholder "Paste the shutterbug here..."
                 , HA.spellcheck False
                 , HA.title <|
                    "Enter a Base64URL encoding with a length of 44 sextets"
                        ++ " including the padding character."
                 , HA.type_
                    (if shutterbug.unmasked then
                        "text"

                     else
                        "password"
                    )
                 , HA.value shutterbug.input
                 , HE.onInput changeInputMsg
                 ]
                    ++ (case inputOk of
                            Just True ->
                                [ HE.on "keydown" <|
                                    D.map
                                        (\key ->
                                            case key of
                                                "Enter" ->
                                                    submitMsg shutterbug.input

                                                _ ->
                                                    submitMsg ""
                                        )
                                        (D.field "key" D.string)
                                ]

                            _ ->
                                []
                       )
                )
                []
            , submitButton theme (submitMsg shutterbug.input) shutterbug.input
            , H.p []
                [ H.span
                    [ My.Html.onClickPreventDefault changeMaskMsg
                    , HA.css <|
                        [ C.display C.inlineBlock
                        , C.margin <| C.em 0
                        , C.marginLeft <| C.em theme.buttonPadding
                        ]
                            ++ (Theme.button theme).standard
                    , HA.title
                        (if shutterbug.unmasked then
                            "Mask the shutterbug."

                         else
                            "Unmask the shutterbug.\n"
                                ++ "( ⚠ Only unmask in private.)"
                        )
                    ]
                    [ View.buttonIcon theme
                        (if shutterbug.unmasked then
                            "visibility"

                         else
                            "visibility_off"
                        )
                    ]
                ]
            ]
        , case inputOk of
            Nothing ->
                H.text ""

            Just True ->
                H.p
                    [ HA.css [ C.color theme.primary ]
                    , HA.title <| "Looks good!"
                    ]
                    [ View.buttonIcon theme "thumb_up" ]

            Just False ->
                H.p
                    [ HA.css [ C.color theme.primary ]
                    , HA.title <| "Oh no, this doesn't look right!"
                    ]
                    [ View.buttonIcon theme "thumb_down" ]
        , case inputOk of
            Nothing ->
                H.text ""

            Just True ->
                H.text ""

            Just False ->
                H.div [] inputHelp
        ]



-- A button to open a new tab on the shutterbug entry page.


openTabButton :
    { r
        | theme : Theme
        , tabMsg : a
        , inactionMsg : a
        , maybeNotification : Maybe LifeCycle.WSCloseEvent
    }
    -> Html a
openTabButton data =
    (case data.maybeNotification of
        Just notification ->
            H.button
                [ HA.css <|
                    Theme.bottomMargin data.theme
                        :: Theme.kaputButtonStyles data.theme
                , HA.title <| LifeCycle.explainWSCloseEvent notification
                ]

        Nothing ->
            My.Html.safeClickButton
                data.tabMsg
                data.inactionMsg
                [ HA.css <| .standard <| Theme.buttonWithMargin data.theme
                , HA.title
                    "Open a new tab which can use an existing shutterbug."
                ]
    )
        [ View.buttonIcon data.theme "open_in_browser" ]



-- The shutterbug actions popup div.


popUp :
    { r
        | theme : Theme
        , tabMsg : a
        , copyMsg : a
        , inactionMsg : a
        , maybeNotification : Maybe LifeCycle.WSCloseEvent
        , shutterbugActionsPopUp : Bool
        , extantUrl : String
    }
    -> Html a
popUp data =
    H.div
        [ HA.css
            [ C.backgroundColor Theme.white
            , C.borderTopLeftRadius <| C.em data.theme.buttonBorderRadius
            , C.displayFlex
            , C.flexDirection C.column
            , C.paddingTop <| C.em data.theme.buttonPadding
            , C.paddingBottom <| C.em data.theme.buttonPadding
            , C.paddingLeft <| C.em data.theme.buttonPadding
            , C.paddingRight <| C.px 2 -- Added for transform pixel glitch.
            , C.position C.absolute
            , C.transforms
                [ C.translateX (C.pct -100)
                , C.translateX (C.px 2)
                , C.translateX (C.em -data.theme.buttonPadding)
                , C.translateY <|
                    C.pct (ME.ternary data.shutterbugActionsPopUp -50 51)
                ]
            , C.property
                "transition"
                ("transform " ++ Theme.popUpDurationMS)
            , C.property "user-select"
                (ME.ternary data.shutterbugActionsPopUp "auto" "none")
            ]
        ]
        [ openTabButton data
        , copyButton
            data.theme
            data.copyMsg
            data.inactionMsg
            data.maybeNotification
        ]



-- A button to submit a shutterbug.


submitButton : Theme -> a -> String -> Html a
submitButton theme msg shutterbugInput =
    let
        valid =
            Regex.contains Shutterbug.regex shutterbugInput

        summary =
            if shutterbugInput /= "" then
                if valid then
                    "Attempt to connect using the provided shutterbug."

                else
                    "Awaiting a valid shutterbug."

            else
                "Awaiting a shutterbug."

        flashingStyle =
            if valid then
                Theme.flashingReadyToSubmitStyles theme

            else
                []
    in
    if valid then
        H.span
            [ My.Html.onClickPreventDefault msg
            , HA.css <|
                [ C.display C.inlineBlock
                , C.margin <| C.em 0
                ]
                    ++ (Theme.button theme).standard
                    ++ flashingStyle
            , HA.title summary
            ]
            [ View.buttonIcon theme "double_arrow" ]

    else
        View.inactiveSubmitButton theme
