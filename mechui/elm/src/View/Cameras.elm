module View.Cameras exposing (popUp, starting)

import Cameras
import Css as C
import Css.Animations as CA
import Css.Global as CG
import Html.Styled as H exposing (Html)
import Html.Styled.Attributes as HA
import Html.Styled.Events as HE
import Json.Decode as D
import Maybe.Extra
import My.Extra
import My.Html
import Shutterbug
import Stitch
import Theme exposing (Theme)
import View


popUp :
    { theme : Theme
    , toggleCamMsg : Cameras.ToggleMsg -> a
    , toggleSelModeMsg : a
    , assocMsg : Cameras.AssociateMsg -> a
    , stitchMsg : Stitch.StitchingMsg -> a
    , clearMsg : a
    , autoMsg : a
    , cameras : Cameras.Model
    , on : Bool
    , autoClosePopUp : Bool
    }
    -> Html a
popUp data =
    let
        gradientStop =
            "--cameras-pop-up-gradient-stop"

        gradientStopOnValue =
            "10%"

        gradientStopOffValue =
            "100%"
    in
    H.div
        [ HA.css
            [ C.property "background" <|
                String.join
                    ", "
                    [ "linear-gradient(to right"
                    , "rgba(255,255,255,1.0)"
                    , "rgba(255,255,255,1.0) var("
                        ++ gradientStop
                        ++ ")"
                    , "rgba(255,255,255,0.8) calc(var("
                        ++ gradientStop
                        ++ ") + 5%)"
                    , "rgba(255,255,255,0.8))"
                    ]
            , C.property gradientStop
                (if data.on then
                    gradientStopOnValue

                 else
                    gradientStopOffValue
                )
            , C.borderBottom3 (C.em data.theme.buttonBorder)
                C.solid
                data.theme.primary
            , C.borderRight3 (C.em data.theme.buttonBorder)
                C.solid
                data.theme.primary
            , C.borderTop3 (C.em data.theme.buttonBorder)
                C.solid
                data.theme.primary
            , C.borderTopRightRadius (C.em data.theme.buttonBorderRadius)
            , C.borderBottomRightRadius (C.em data.theme.buttonBorderRadius)
            , if data.on then
                C.boxShadow5
                    (C.px 0)
                    (C.px 0)
                    (C.px 20)
                    (C.px 4)
                    (C.rgb 112 145 145)

              else
                C.boxShadow5
                    (C.px 0)
                    (C.px 0)
                    (C.px 0)
                    (C.px 0)
                    (C.rgb 112 145 145)
            , C.padding4 (C.em data.theme.buttonPadding)
                (C.em data.theme.buttonPadding)
                (C.em data.theme.buttonPadding)
                (C.em 0)
            , C.position C.absolute
            , C.transform <|
                C.translate2
                    (C.pct
                        (if data.on then
                            0

                         else
                            -100.1
                        )
                    )
                    (C.pct -50)
            , C.property
                "transition"
              <|
                String.join ", "
                    [ "transform " ++ Theme.popUpDurationMS
                    , "box-shadow " ++ Theme.popUpDurationMS
                    , gradientStop ++ " " ++ Theme.popUpDurationMS
                    ]
            , C.property "user-select"
                (if data.on then
                    "auto"

                 else
                    "none"
                )
            ]
        ]
        [ CG.global
            [ CG.selector ("@property " ++ gradientStop)
                [ C.property "syntax" "'<percentage>'"
                , C.property "inherits" "false"
                , C.property "initial-value" gradientStopOffValue
                ]
            ]
        , running data
        ]


running :
    { theme : Theme
    , toggleCamMsg : Cameras.ToggleMsg -> a
    , toggleSelModeMsg : a
    , assocMsg : Cameras.AssociateMsg -> a
    , stitchMsg : Stitch.StitchingMsg -> a
    , clearMsg : a
    , autoMsg : a
    , cameras : Cameras.Model
    , on : Bool
    , autoClosePopUp : Bool
    }
    -> Html a
running data =
    H.div
        [ HA.css
            [ C.displayFlex
            , C.alignItems C.flexStart
            , C.property "user-select" "none"
            ]
        ]
        [ viewSelectorForm
            data.theme
            data.toggleCamMsg
            data.assocMsg
            data.cameras
            (Just 4)
        , H.div
            [ HA.css
                [ C.alignItems C.flexStart
                , C.displayFlex
                , C.flexDirection C.column
                , C.property "gap" <|
                    String.fromFloat data.theme.buttonPadding
                        ++ "em"
                , C.paddingLeft <| C.em data.theme.buttonPadding
                ]
            ]
            [ viewStitchingChooser data.stitchMsg data.cameras
            , H.div
                [ HA.css
                    [ C.displayFlex
                    , C.property "gap" <|
                        String.fromFloat data.theme.buttonPadding
                            ++ "em"
                    ]
                ]
                [ if Cameras.radio data.cameras then
                    H.button
                        [ My.Html.onClickPreventDefault data.toggleSelModeMsg
                        , HA.css <| (Theme.button data.theme).standard
                        , HA.title "Allow multiple camera selections."
                        ]
                        [ View.buttonIcon data.theme "radio_button_checked" ]

                  else
                    H.button
                        [ My.Html.onClickPreventDefault data.toggleSelModeMsg
                        , HA.css <| (Theme.button data.theme).standard
                        , HA.title "Disallow multiple camera selections."
                        ]
                        [ View.buttonIcon data.theme "check_box" ]
                , if List.length (Cameras.selectedNames data.cameras) == 0 then
                    H.div
                        -- For the inactive case use a different element, div is
                        -- used here, so the transitions don't happen when the
                        -- button is active again.
                        [ HA.css <| .inactive <| Theme.button data.theme
                        , HA.title "No camera selections to clear."
                        ]
                        [ View.buttonIcon data.theme "remove_done" ]

                  else
                    H.button
                        [ My.Html.onClickPreventDefault data.clearMsg
                        , HA.css <| .standard <| Theme.button data.theme
                        , HA.title "Clear camera selections."
                        ]
                        [ View.buttonIcon data.theme "remove_done" ]
                , if data.autoClosePopUp then
                    H.button
                        [ My.Html.onClickPreventDefault data.autoMsg
                        , HA.css <|
                            (Theme.button data.theme).standard
                                ++ [ C.position C.relative ]
                        , HA.title
                            "Pin the camera selector open."
                        ]
                        [ View.buttonOffIcon data.theme "push_pin"
                        ]

                  else
                    H.button
                        [ My.Html.onClickPreventDefault data.autoMsg
                        , HA.css <| (Theme.button data.theme).standard
                        , HA.title
                            "Don't pin the camera selector open."
                        ]
                        [ View.buttonIcon data.theme "push_pin" ]
                ]
            ]
        ]


starting :
    { theme : Theme
    , toggleMsg : Cameras.ToggleMsg -> a
    , assocMsg : Cameras.AssociateMsg -> a
    , stitchMsg : Stitch.StitchingMsg -> a
    , cameras : Cameras.Model
    , extantUrl : String
    , shutterbugActionsPopUp : Bool
    , source : Shutterbug.Source
    }
    -> Html a
starting data =
    let
        fadeIn =
            [ C.animationName <|
                CA.keyframes
                    [ ( 0, [ CA.property "opacity" "0" ] )
                    , ( 50, [ CA.property "opacity" "0" ] )
                    , ( 100, [ CA.property "opacity" "1" ] )
                    ]
            , C.animationDuration <| C.ms 1000
            , C.animationIterationCount <| C.num 1
            ]
    in
    H.div
        [ HA.css <|
            [ C.textAlign C.center
            , C.minWidth <| C.em 50
            ]
                ++ (case data.source of
                        Shutterbug.MakeNew ->
                            []

                        Shutterbug.UseExisting ->
                            fadeIn
                   )
        ]
        [ H.p
            [ HA.css
                [ C.marginBottom <| C.em 3
                , C.marginTop <| C.em 3
                ]
            ]
            [ H.text "To continue select a camera(s):" ]
        , H.div
            [ HA.css
                [ C.displayFlex
                , C.alignItems C.flexStart
                , C.justifyContent C.center
                ]
            ]
            [ viewSelectorForm
                data.theme
                data.toggleMsg
                data.assocMsg
                data.cameras
                Nothing
            , H.div
                [ HA.css
                    [ C.paddingLeft <| C.em data.theme.buttonPadding
                    ]
                ]
                [ viewStitchingChooser data.stitchMsg data.cameras ]
            ]
        ]


viewSelectionSubmitButton :
    Theme
    -> (Cameras.AssociateMsg -> a)
    -> Cameras.Model
    -> Html a
viewSelectionSubmitButton theme msg cameras =
    let
        noChanges =
            "\n(There are no changes.)"

        changes =
            Cameras.difference cameras

        summary =
            if changes /= "" then
                changes

            else
                noChanges

        flashingStyle =
            if changes /= "" then
                Theme.flashingReadyToSubmitStyles theme

            else
                []
    in
    [ case Cameras.selectedNames cameras of
        [] ->
            View.inactiveSubmitButton theme

        _ ->
            H.button
                [ My.Html.onClickPreventDefault
                    (msg
                        (Cameras.AssociateMsg <|
                            Stitch.associate (Cameras.selected cameras)
                        )
                    )
                , HA.css <| (Theme.button theme).standard ++ flashingStyle
                , case List.length (Cameras.selectedNames cameras) of
                    1 ->
                        HA.title <|
                            "Submit camera selection."
                                ++ summary

                    _ ->
                        HA.title <|
                            "Submit camera selections."
                                ++ summary
                ]
                [ View.buttonIcon theme "double_arrow" ]
    , viewSelectionSummary theme cameras
    ]
        |> H.div
            [ HA.css
                [ C.height <| C.em theme.buttonHeight ]
            ]


viewSelectionSummary : Theme -> Cameras.Model -> Html a
viewSelectionSummary theme cameras =
    (case Cameras.registered cameras |> List.length of
        0 ->
            ( "No cameras to choose from", theme.textPassive )

        1 ->
            case Cameras.selectedNames cameras |> List.length of
                0 ->
                    ( "Camera not selected", theme.textPassive )

                _ ->
                    ( "Camera selected", theme.textNormal )

        _ ->
            case Cameras.selectedNames cameras |> List.length of
                0 ->
                    ( "No cameras selected", theme.textPassive )

                1 ->
                    ( "Selected 1 camera", theme.textNormal )

                n ->
                    ( "Selected "
                        ++ String.fromInt n
                        ++ " cameras"
                    , theme.textNormal
                    )
    )
        |> (\( text, color ) ->
                H.span
                    [ HA.css
                        [ C.color color
                        , C.padding <| C.em theme.buttonPadding
                        ]
                    ]
                    [ H.text text ]
           )


viewSelectorForm :
    Theme
    -> (Cameras.ToggleMsg -> a)
    -> (Cameras.AssociateMsg -> a)
    -> Cameras.Model
    -> Maybe Int
    -> Html a
viewSelectorForm theme toggleMsg assocMsg cameras maybeMaxVisibleCams =
    let
        maxVisibleCams =
            toFloat <|
                case maybeMaxVisibleCams of
                    Just max ->
                        max

                    Nothing ->
                        6
    in
    H.form
        [ HA.css
            [ C.border3 (C.em theme.buttonBorder) C.double theme.primary
            , C.borderRadius <| C.em theme.buttonBorderRadius
            , C.display C.inlineBlock
            , C.maxHeight <|
                C.em <|
                    (maxVisibleCams + 1)
                        * theme.buttonHeight
                        + maxVisibleCams
                        * theme.buttonPadding
            , C.overflow C.auto
            , C.padding <| C.em theme.buttonPadding
            , C.position C.relative
            , C.textAlign C.left
            , C.width <| C.em 20
            ]
        ]
        [ viewSelectionSubmitButton theme assocMsg cameras
        , viewSelectors theme toggleMsg cameras
        ]


viewSelectors : Theme -> (Cameras.ToggleMsg -> a) -> Cameras.Model -> Html a
viewSelectors theme msg cameras =
    let
        margin =
            theme.buttonPadding

        minHeight =
            4 * theme.buttonHeight + 3 * margin
    in
    case Cameras.displayedNames cameras of
        [] ->
            H.div
                [ HA.css
                    [ C.alignItems C.center
                    , C.color theme.textPassive
                    , C.displayFlex
                    , C.flexDirection C.column
                    , C.height <| C.em minHeight
                    , C.justifyContent C.center
                    , C.marginTop <| C.em margin
                    ]
                ]
                [ H.span [] [ H.text "Available cameras appear here" ] ]

        displayed ->
            displayed
                |> List.sort
                |> List.map
                    (\cam ->
                        viewTogglerField theme msg cameras cam
                    )
                |> H.div
                    [ HA.css
                        [ C.border <| C.em 0
                        , C.minHeight <| C.em minHeight
                        ]
                    ]


viewStitchingChooser : (Stitch.StitchingMsg -> a) -> Cameras.Model -> Html a
viewStitchingChooser msg cameras =
    H.node Stitch.tag
        [ Cameras.selected cameras
            |> Stitch.attribute
            |> HA.attribute Stitch.associationsAttribute
        , Cameras.selectionSession cameras
            |> String.fromInt
            |> HA.attribute Stitch.sessionAttribute
        , HA.attribute Stitch.widthAttribute "25em"
        , HE.on
            Stitch.switchCamerasEvent
            (D.map Stitch.SwitchedCameras Stitch.decodeSwitchCameras
                |> D.map msg
            )
        , HE.on
            Stitch.toggleStitchingSizeEvent
            (D.map Stitch.ToggledStitchingSize Stitch.decodeToggleStitchingSize
                |> D.map msg
            )
        ]
        []


viewTogglerButton :
    Theme
    -> (Cameras.ToggleMsg -> a)
    -> Cameras.Model
    -> String
    -> Html a
viewTogglerButton theme msg cameras cam =
    let
        inactive =
            Maybe.Extra.isJust <| Cameras.recentDifference cameras

        selected =
            List.member cam <| Cameras.selectedNames cameras

        buttonStyle =
            My.Extra.ternary inactive
                .inactive
                (My.Extra.ternary selected
                    .standard
                    .unselected
                )

        autoDeselected =
            Cameras.recentAutoDeselected cam cameras

        autoDisassociated =
            Cameras.recentAutoDisassociated cam cameras
    in
    H.button
        [ My.Html.onClickPreventDefault (msg (Cameras.ToggleMsg cam))
        , HA.css <|
            My.Extra.appendWhen
                (buttonStyle <| Theme.button theme)
                [ ( autoDeselected || autoDisassociated
                  , [ My.Extra.ternary autoDisassociated
                        (C.backgroundColor theme.error)
                        (C.backgroundColor theme.warning)
                    ]
                  )
                ]
        ]
        [ View.buttonIcon
            theme
            (if Cameras.radio cameras then
                My.Extra.ternary selected
                    "radio_button_checked"
                    "radio_button_unchecked"

             else
                My.Extra.ternary selected "check_box" "check_box_outline_blank"
            )
        ]


viewTogglerField :
    Theme
    -> (Cameras.ToggleMsg -> a)
    -> Cameras.Model
    -> String
    -> Html a
viewTogglerField theme msg cameras cam =
    let
        viewRecentAddOrRemove : Bool -> Html a
        viewRecentAddOrRemove removed =
            H.node
                (if removed then
                    "shrinking-div"

                 else
                    "growing-div"
                )
                [ HA.attribute "duration" Cameras.buttonAnimationDuration
                , HA.attribute "name" cam
                , Cameras.displayedNames cameras
                    |> String.join ","
                    |> HA.attribute "names"
                ]
                [ H.div
                    [ HA.css
                        [ C.height <| C.em theme.buttonHeight
                        , C.marginTop <| C.em theme.buttonPadding
                        ]
                    , HA.attribute "slot" "content"
                    ]
                    [ viewTogglerButton theme msg cameras cam
                    , H.label
                        [ HA.css <|
                            My.Extra.appendWhen
                                [ C.paddingLeft <| C.em theme.buttonPadding ]
                                [ ( removed
                                  , [ C.textDecoration3
                                        C.lineThrough
                                        C.wavy
                                        (C.rgb 255 0 0)
                                    ]
                                  )
                                ]
                        ]
                        [ H.text <| cam ]
                    ]
                ]
    in
    if List.member cam (Cameras.recentRemovedNames cameras) then
        viewRecentAddOrRemove True

    else if List.member cam (Cameras.recentAddedNames cameras) then
        viewRecentAddOrRemove False

    else
        H.div
            [ HA.css
                [ C.height <| C.em theme.buttonHeight
                , C.marginTop <| C.em theme.buttonPadding
                ]
            ]
            [ viewTogglerButton theme msg cameras cam
            , H.label
                [ HA.css
                    [ C.paddingLeft <| C.em theme.buttonPadding ]
                ]
                [ H.text <| cam ]
            ]
