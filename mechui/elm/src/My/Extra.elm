module My.Extra exposing (..)


appendIf : Bool -> List a -> List a -> List a
appendIf ok prefix potentialSuffix =
    if ok then
        prefix ++ potentialSuffix

    else
        prefix



-- Conditionally append each list from the list of lists to the prefix list.
--
-- Use the paired Boolean value of each list to decide if the list should be
-- appended into the final list or not.


appendWhen : List a -> List ( Bool, List a ) -> List a
appendWhen prefix potentialSuffixes =
    List.foldl
        (\( ok, suffix ) acc ->
            if ok then
                acc ++ suffix

            else
                acc
        )
        prefix
        potentialSuffixes



-- Conditionally return one of two values.
--
-- Return the first value for a True condition, else the second.


ternary : Bool -> a -> a -> a
ternary ok okValue notOkValue =
    if ok then
        okValue

    else
        notOkValue



-- Conditionally use the given list.
--
-- Return the given list for a True condition, else the empty list.


useIf : Bool -> List a -> List a
useIf ok okValue =
    if ok then
        okValue

    else
        []
