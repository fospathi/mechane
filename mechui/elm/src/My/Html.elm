module My.Html exposing
    ( a
    , onClickPreventDefault
    , onKeydownPreventDefault
    , safeClickButton
    )

import Html.Styled as H exposing (Html)
import Html.Styled.Attributes as HA
import Html.Styled.Events as HE
import Json.Decode as D



-- Open a link in a new tab.


a : String -> String -> Html msg
a href text =
    H.a
        [ HA.href href
        , HA.rel "noopener noreferrer"
        , HA.target "_blank"
        ]
        [ H.text text ]



-- An attribute that can be applied to an element such as a button element and
-- which listens for the event, prevents the default action for the event such
-- as a submit action, and instead initiates the given message.


onEventPreventDefault : String -> a -> H.Attribute a
onEventPreventDefault event msg =
    HE.custom event
        (D.succeed
            { message = msg
            , stopPropagation = False
            , preventDefault = True
            }
        )


onClickPreventDefault : a -> H.Attribute a
onClickPreventDefault msg =
    onEventPreventDefault "click" msg


onKeydownPreventDefault : a -> H.Attribute a
onKeydownPreventDefault msg =
    onEventPreventDefault "keydown" msg



-- Only perform the button action within the Elm integrant on a button click,
-- ignoring the keydown events including the space and enter keys which can
-- activate focused buttons.


safeClickButton : a -> a -> List (H.Attribute a) -> List (Html a) -> Html a
safeClickButton action inaction attributes children =
    H.button
        (attributes
            ++ [ onClickPreventDefault action
               , onKeydownPreventDefault inaction
               ]
        )
        children
