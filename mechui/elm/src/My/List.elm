module My.List exposing (..)

import Result.Extra



-- Return either a list of errors for the bad input values failing the test,
-- else a list of validated output values.


validate :
    (a -> Result String b)
    -> List a
    -> Result (List String) (List b)
validate test strings =
    let
        results =
            List.map test strings
    in
    if List.any Result.Extra.isErr results then
        Err (List.filterMap Result.Extra.error results)

    else
        Ok (List.filterMap Result.toMaybe results)
