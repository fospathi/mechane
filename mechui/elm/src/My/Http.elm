module My.Http exposing (..)

import Http


errorToString : Http.Error -> String
errorToString error =
    case error of
        Http.BadBody message ->
            "bad body: " ++ message

        Http.BadStatus code ->
            "bad status: " ++ String.fromInt code

        Http.BadUrl url ->
            "bad URL: " ++ url

        Http.NetworkError ->
            "network error"

        Http.Timeout ->
            "timeout"



-- Shorten long error messages. An Http error message reproduces the entire
-- body, which can be a lot, for example, in the case of the static JSON
-- responses.


formatLongError : String -> List String
formatLongError err =
    let
        -- Filter more than one consecutive space.
        ( substantial, _ ) =
            String.foldl
                (\char ( s, n ) ->
                    if char == ' ' then
                        if n == 0 then
                            ( s ++ " ", 1 )

                        else
                            ( s, n + 1 )

                    else
                        ( s ++ String.fromChar char, 0 )
                )
                ( "", 0 )
                err
    in
    if String.length substantial > 300 then
        String.lines substantial
            |> List.map
                (\line ->
                    if String.length line > 150 then
                        String.left 30 line ++ " ... " ++ String.right 30 line

                    else
                        line
                )

    else
        [ err ]
