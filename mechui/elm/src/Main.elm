port module Main exposing
    ( Model
    , Msg
    , init
    , main
    , subscriptions
    , update
    , view
    )

import Browser
import Cameras
import Css as C
import Css.Transitions as CT
import Devoir
import Html.Styled as H exposing (Html)
import Html.Styled.Attributes as HA
import Json.Decode as D
import Kaput
import LifeCycle
import Maybe.Extra
import My.Extra as ME
import My.Html
import My.Http
import PixScreen
import PopUp
import Process
import Route
import Shutterbug
import Static
import Static.About
import Static.Motif
import Stitch
import String
import Task
import Theme exposing (Theme)
import Time
import View
import View.Cameras
import View.Info
import View.Settings
import View.Shutterbug



-- IO


subscriptions : Model -> Sub Msg
subscriptions model =
    (if registeredCamerasChanged model then
        [ Time.every 100 TryToUnblockCameraToggleButtons ]

     else
        []
    )
        ++ [ receiveLifeCycle ReceivedLifeCycle
           , receiveRegisteredAssociatedCameras
           , receiveDevoir ReceivedDevoir
           , receivePixelsMeta ReceivedPixelsMeta
           , receiveKaput ReceivedKaput
           ]
        |> Sub.batch



-- Listen for updates to the progress of the Typescript integrant in getting
-- ready.


port receiveLifeCycle : (D.Value -> msg) -> Sub msg



-- Listen to the stream of registered and associated cameras messages sent from
-- the Typescript integrant.


receiveRegisteredAssociatedCameras : Sub Msg
receiveRegisteredAssociatedCameras =
    Sub.map ReceivedRegAssocCameras Cameras.registeredAssociatedSub



-- Listen to the stream of Devoir state messages sent from the Typescript
-- integrant.


port receiveDevoir : (D.Value -> msg) -> Sub msg



-- Listen to the stream of pixels' meta data messages sent from the Typescript
-- integrant.


port receivePixelsMeta : (D.Value -> msg) -> Sub msg



-- Listen for a websocket disconnection notification sent from the Typescript
-- integrant.


port receiveKaput : (D.Value -> msg) -> Sub msg



-- Listen for changes to the fullscreen status sent from the Typescript
-- integrant.


port receiveFullscreen : (D.Value -> msg) -> Sub msg



-- Send the shutterbug to the Typescript integrant so it can be used to connect
-- to Mechane server websockets that expect a valid shutterbug.


port sendShutterbug : String -> Cmd msg



-- Send the selected camera(s) to the Typescript integrant so they can be sent
-- to the Mechane server as a request to associate the shutterbug with those
-- cameras.


port sendCameraAssociations : Stitch.AssociateSchema -> Cmd msg



-- Send an instruction to the Typescript integrant to open a new tab at the
-- given URL.


port sendOpenNewTab : String -> Cmd msg



-- Send an instruction to the Typescript integrant to engage or disengage
-- fullscreen mode.


port sendFullscreen : Bool -> Cmd msg



-- Send an instruction to the Typescript integrant to engage or disengage
-- pointer lock mode.


port sendPointerLock : Bool -> Cmd msg



-- Send an instruction to the Typescript integrant to write the shutterbug
-- (which was previously sent to the Typescript integrant) to the clipboard.


port sendWriteShutterbugToClipboard : () -> Cmd msg



-- Send text to the Typescript integrant to write to the clipboard.


port sendWriteTextToClipboard : String -> Cmd msg



-- Send a UI command to the Typescript integrant so it can be sent to the
-- Mechane server.


port sendUICommand : { uiCommand : String } -> Cmd msg



-- INIT


init : D.Value -> ( Model, Cmd Msg )
init url =
    case Route.parseURL url of
        Ok shutterbugMeta ->
            ( Starting
                { maybeAbout = Nothing
                , maybeTheme = Nothing
                , maybeCameras = Nothing
                , maybeDevoir = Nothing
                , maybePix = Nothing
                , awaitingShutterbug = True
                , sentCameraAssociations = False
                , websockets = Connecting
                , extantUrl = shutterbugMeta.manualEntryURL
                , shutterbugActionsPopUp = False
                , shutterbugSource =
                    case shutterbugMeta.source of
                        Shutterbug.MakeNew ->
                            MakeNew

                        Shutterbug.UseExisting ->
                            UseExisting
                                { shutterbugInput = ""
                                , shutterbugUnmasked = False
                                }
                }
            , Cmd.batch
                [ Cmd.map ReceivedAbout Static.About.create
                , Cmd.map ReceivedMotif Static.Motif.create
                ]
            )

        Err error ->
            ( Errored Theme.initial [ [ error ] ], Cmd.none )



-- MODEL


type ShutterbugSource
    = MakeNew
    | UseExisting
        { shutterbugInput : String
        , shutterbugUnmasked : Bool
        }


type alias StartingData =
    { maybeAbout : Maybe Static.About.Model
    , maybeTheme : Maybe Theme
    , maybeCameras : Maybe Cameras.Model
    , maybeDevoir : Maybe Devoir
    , maybePix : Maybe PixScreen.Model
    , awaitingShutterbug : Bool
    , sentCameraAssociations : Bool
    , websockets : WebSockets
    , extantUrl : String
    , shutterbugActionsPopUp : Bool
    , shutterbugSource : ShutterbugSource
    }


type Model
    = Errored Theme (List (List String))
    | Kaput Kaput.Model
    | Starting StartingData
    | Running
        Cameras.Model
        PixScreen.Model
        Devoir
        { extantUrl : String
        , theme : Theme
        , popUps : PopUp.Model
        }



-- A Devoir is kept in sync with the Mechane server's Devoir state; a small
-- subset of the Mechane world's state that effects UI command buttons
-- implemented by the Elm integrant.


type Devoir
    = Paused Bool


type WebSockets
    = Connected
    | Connecting



-- UPDATE MESSAGES


type Msg
    = ReceivedAbout Static.About.Msg
    | ReceivedMotif Static.Motif.Msg
    | ReceivedShutterbug Shutterbug.Msg
    | DelayedShutterbug Shutterbug.Msg
    | ReceivedLifeCycle D.Value
    | DelayedLifeCycle D.Value
    | ReceivedRegAssocCameras Cameras.RegisteredAssociatedMsg
    | DelayedRegAssocCameras Cameras.RegisteredAssociatedMsg
    | ReceivedPixelsMeta D.Value
    | DelayedPixelsMeta D.Value
    | ReceivedDevoir D.Value
    | ReceivedKaput D.Value
      -- Wholly internal messages:
    | TryToUnblockCameraToggleButtons Time.Posix
    | UpdateCamerasTime Time.Posix
      -- User actions:
    | IssuedCameraAssociations Cameras.AssociateMsg
    | ClearedCameraSelections
    | StitchingMsg Stitch.StitchingMsg -- Stitching related custom HTML events.
    | ToggledCamera Cameras.ToggleMsg
    | ToggledCameraSelectionMode
    | ToggledOutputSize
    | ToggledOutputBorder
    | ToggledTextSelect
    | ToggledCamerasPopUp
    | ToggledAutoCloseCameras
    | ToggledInfoPopUp
    | ToggledSettingsPopUp
    | ToggledShutterbugPopUp
    | ChangedInfoTab PopUp.InfoTabMsg
    | OpenedNewTab String
    | ToggledFullscreen Bool
    | ToggledPointerLock Bool
    | WroteShutterbugToClipboard
    | WroteTextToClipboard String
    | IssuedUICommand Devoir.UICommand
    | ChangedShutterbugInputValue
        { shutterbugInput : String
        , shutterbugUnmasked : Bool
        }
    | SubmittedShutterbug String
    | Identity



-- Delays can be useful during testing.


delayMsg : Float -> Msg -> Cmd Msg
delayMsg delay msg =
    Task.perform (always msg) (Process.sleep delay)



-- Check if recent changes to the cameras model caused by a registered cameras
-- message are blocking the camera toggle buttons.


registeredCamerasChanged : Model -> Bool
registeredCamerasChanged model =
    let
        getCameras : Model -> Maybe Cameras.Model
        getCameras mdl =
            case mdl of
                Errored _ _ ->
                    Nothing

                Kaput _ ->
                    Nothing

                Starting { maybeCameras } ->
                    maybeCameras

                Running cameras _ _ _ ->
                    Just cameras
    in
    case getCameras model of
        Just cameras ->
            case Cameras.recentDifference cameras of
                Just _ ->
                    True

                Nothing ->
                    False

        Nothing ->
            False



-- Check if the conditions are met to progress from starting to running and
-- return the running state if so.


attemptToRun : StartingData -> Model
attemptToRun data =
    case
        ( Just
            (\about motif cameras devoir pix ->
                { about = about
                , motif = motif
                , cameras = cameras
                , devoir = devoir
                , pix = pix
                }
            )
            |> Maybe.Extra.andMap data.maybeAbout
            |> Maybe.Extra.andMap data.maybeTheme
            |> Maybe.Extra.andMap data.maybeCameras
            |> Maybe.Extra.andMap data.maybeDevoir
            |> Maybe.Extra.andMap data.maybePix
        , data.websockets == Connected
        )
    of
        ( Just justData, True ) ->
            Running
                justData.cameras
                justData.pix
                justData.devoir
                { popUps =
                    if data.shutterbugActionsPopUp then
                        PopUp.toggleShutterbugActionsPopUp <|
                            PopUp.init justData.about

                    else
                        PopUp.init justData.about
                , extantUrl =
                    data.extantUrl
                , theme = justData.motif
                }

        ( _, _ ) ->
            Starting data



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        delay =
            delayMsg 0
    in
    case msg of
        ReceivedAbout aboutMsg ->
            case Static.About.init aboutMsg of
                Ok about ->
                    updateStatic (Just about) Nothing model

                Err err ->
                    handleError model <| My.Http.formatLongError err

        ReceivedMotif motifMsg ->
            case Static.Motif.init motifMsg of
                Ok motif ->
                    updateStatic Nothing (Just motif) model

                Err err ->
                    handleError model <| My.Http.formatLongError err

        ReceivedShutterbug shutterbugMsg ->
            ( model, delay <| DelayedShutterbug shutterbugMsg )

        DelayedShutterbug shutterbugMsg ->
            case Shutterbug.init shutterbugMsg of
                Ok shutterbug ->
                    updateShutterbug shutterbug model

                Err err ->
                    handleError model [ err ]

        ReceivedLifeCycle lifeCycleMsg ->
            ( model, delay <| DelayedLifeCycle lifeCycleMsg )

        DelayedLifeCycle lifeCycleMsg ->
            case D.decodeValue LifeCycle.decodeLifeCycleMessage lifeCycleMsg of
                Ok lifeCycle ->
                    case lifeCycle of
                        Ok event ->
                            updateLifeCycle event model

                        Err err ->
                            handleError model [ err ]

                Err err ->
                    handleError model [ D.errorToString err ]

        ReceivedRegAssocCameras camerasMsg ->
            ( model, delay <| DelayedRegAssocCameras camerasMsg )

        DelayedRegAssocCameras camerasMsg ->
            case Cameras.decodeRegisteredAssociatedMessage camerasMsg of
                Ok allRegistered ->
                    updateCameras allRegistered model

                Err err ->
                    handleError model [ err ]

        ReceivedPixelsMeta value ->
            ( model
            , delay <| DelayedPixelsMeta value
            )

        DelayedPixelsMeta value ->
            case D.decodeValue PixScreen.metaDecoder value of
                Ok meta ->
                    updatePixScreen meta model

                Err err ->
                    handleError model [ D.errorToString err ]

        ReceivedDevoir value ->
            case D.decodeValue Devoir.devoirDecoder value of
                Ok paused ->
                    updateDevoir paused model

                Err err ->
                    handleError model [ D.errorToString err ]

        ReceivedKaput value ->
            case D.decodeValue LifeCycle.decodeWSCloseEvent value of
                Ok notification ->
                    updateKaput notification model

                Err err ->
                    handleError model [ D.errorToString err ]

        TryToUnblockCameraToggleButtons now ->
            updateTryToUnblockCameraToggleButtons model now

        UpdateCamerasTime now ->
            updateCamerasTime model now

        IssuedCameraAssociations (Cameras.AssociateMsg associations) ->
            case model of
                Starting data ->
                    ( Starting { data | sentCameraAssociations = True }
                    , sendCameraAssociations associations
                    )

                Running _ _ _ data ->
                    ( if
                        data.popUps.autoCloseCameras
                            && PopUp.cameras data.popUps
                      then
                        case update ToggledCamerasPopUp model of
                            ( newModel, _ ) ->
                                newModel

                      else
                        model
                    , sendCameraAssociations associations
                    )

                _ ->
                    handleMsgError model "camera association"

        ClearedCameraSelections ->
            case model of
                Running cameras screen devoir data ->
                    ( Running
                        (Cameras.clearSelection cameras)
                        screen
                        devoir
                        data
                    , Cmd.none
                    )

                _ ->
                    handleMsgError model "cleared camera selections"

        StitchingMsg stitchingMsg ->
            case stitchingMsg of
                Stitch.SwitchedCameras switchCamerasMsg ->
                    case model of
                        Starting data ->
                            case data.maybeCameras of
                                Just cameras ->
                                    ( Starting
                                        { data
                                            | maybeCameras =
                                                Just
                                                    (Cameras.switchCameras
                                                        cameras
                                                        switchCamerasMsg
                                                    )
                                        }
                                    , Cmd.none
                                    )

                                Nothing ->
                                    handleMsgError model "switched cameras"

                        Running cameras screen devoir data ->
                            ( Running
                                (Cameras.switchCameras cameras switchCamerasMsg)
                                screen
                                devoir
                                data
                            , Cmd.none
                            )

                        _ ->
                            handleMsgError model "switched cameras"

                Stitch.ToggledStitchingSize _ ->
                    case model of
                        Starting data ->
                            case data.maybeCameras of
                                Just cameras ->
                                    ( Starting
                                        { data
                                            | maybeCameras =
                                                Just (Cameras.toggleStitchingSize cameras)
                                        }
                                    , Cmd.none
                                    )

                                _ ->
                                    handleMsgError model "toggled stitching size"

                        Running cameras screen devoir data ->
                            ( Running
                                (Cameras.toggleStitchingSize cameras)
                                screen
                                devoir
                                data
                            , Cmd.none
                            )

                        _ ->
                            handleMsgError model "toggled stitching size"

        ToggledCamera toggleMsg ->
            updateToggledCamera model toggleMsg

        ToggledCameraSelectionMode ->
            case model of
                Running cameras screen devoir data ->
                    ( Running
                        (Cameras.toggleSelectionMode cameras)
                        screen
                        devoir
                        data
                    , Cmd.none
                    )

                _ ->
                    handleMsgError model "camera selection mode toggle"

        ToggledOutputSize ->
            case model of
                Running cameras screen devoir data ->
                    ( Running
                        cameras
                        (PixScreen.toggleFit screen)
                        devoir
                        data
                    , Cmd.none
                    )

                Kaput (Kaput.AfterRunning notification errors screen data) ->
                    ( Kaput <|
                        Kaput.AfterRunning
                            notification
                            errors
                            (PixScreen.toggleFit screen)
                            data
                    , Cmd.none
                    )

                _ ->
                    handleMsgError model "screen size toggle"

        ToggledOutputBorder ->
            case model of
                Running cameras screen devoir data ->
                    ( Running
                        cameras
                        (PixScreen.toggleBorder screen)
                        devoir
                        data
                    , Cmd.none
                    )

                Kaput (Kaput.AfterRunning notification errors screen data) ->
                    ( Kaput <|
                        Kaput.AfterRunning
                            notification
                            errors
                            (PixScreen.toggleBorder screen)
                            data
                    , Cmd.none
                    )

                _ ->
                    handleMsgError model "screen border toggle"

        ToggledTextSelect ->
            case model of
                Running cameras screen devoir data ->
                    ( Running
                        cameras
                        (PixScreen.toggleTextSelect screen)
                        devoir
                        data
                    , Cmd.none
                    )

                Kaput (Kaput.AfterRunning notification errors screen data) ->
                    ( Kaput <|
                        Kaput.AfterRunning
                            notification
                            errors
                            (PixScreen.toggleTextSelect screen)
                            data
                    , Cmd.none
                    )

                _ ->
                    handleMsgError model "screen text select"

        ToggledCamerasPopUp ->
            case model of
                Running cameras screen devoir data ->
                    ( PopUp.togglePixelsAreaPopUp data.popUps PopUp.CamerasPopUp
                        |> (\popups -> { data | popUps = popups })
                        |> Running
                            (if PopUp.cameras data.popUps then
                                cameras

                             else
                                Cameras.incrementSelectionSession cameras
                            )
                            screen
                            devoir
                    , Cmd.none
                    )

                _ ->
                    handleMsgError model "cameras pop-up toggle"

        ToggledAutoCloseCameras ->
            case model of
                Running cameras screen devoir data ->
                    ( ( not data.popUps.autoCloseCameras, data.popUps )
                        |> (\( autoCloseCameras, popups ) ->
                                { popups
                                    | autoCloseCameras =
                                        autoCloseCameras
                                }
                           )
                        |> (\popups -> { data | popUps = popups })
                        |> Running cameras screen devoir
                    , Cmd.none
                    )

                _ ->
                    handleMsgError model "cameras auto close toggle"

        ToggledInfoPopUp ->
            case model of
                Running cameras screen devoir data ->
                    ( PopUp.togglePixelsAreaPopUp data.popUps PopUp.InfoPopUp
                        |> (\popups -> { data | popUps = popups })
                        |> Running cameras screen devoir
                    , Cmd.none
                    )

                Kaput (Kaput.AfterRunning reason errors screen data) ->
                    ( PopUp.togglePixelsAreaPopUp data.popUps PopUp.InfoPopUp
                        |> (\popups -> { data | popUps = popups })
                        |> Kaput.AfterRunning
                            reason
                            errors
                            screen
                        |> Kaput
                    , Cmd.none
                    )

                _ ->
                    handleMsgError model "info pop-up toggle"

        ToggledSettingsPopUp ->
            case model of
                Running cameras screen devoir data ->
                    ( PopUp.togglePixelsAreaPopUp
                        data.popUps
                        PopUp.SettingsPopUp
                        |> (\popups -> { data | popUps = popups })
                        |> Running cameras screen devoir
                    , Cmd.none
                    )

                Kaput (Kaput.AfterRunning reason errors screen data) ->
                    ( PopUp.togglePixelsAreaPopUp
                        data.popUps
                        PopUp.SettingsPopUp
                        |> (\popups -> { data | popUps = popups })
                        |> Kaput.AfterRunning
                            reason
                            errors
                            screen
                        |> Kaput
                    , Cmd.none
                    )

                _ ->
                    handleMsgError model "settings pop-up toggle"

        ToggledShutterbugPopUp ->
            case model of
                Starting data ->
                    ( Starting
                        { data
                            | shutterbugActionsPopUp =
                                not data.shutterbugActionsPopUp
                        }
                    , Cmd.none
                    )

                Running cameras screen devoir data ->
                    ( PopUp.toggleShutterbugActionsPopUp data.popUps
                        |> (\popups -> { data | popUps = popups })
                        |> Running cameras screen devoir
                    , Cmd.none
                    )

                Kaput (Kaput.BeforeRunning reason errors data) ->
                    ( { data
                        | shutterbugActionsPopUp =
                            not data.shutterbugActionsPopUp
                      }
                        |> Kaput.BeforeRunning
                            reason
                            errors
                        |> Kaput
                    , Cmd.none
                    )

                Kaput (Kaput.AfterRunning reason errors screen data) ->
                    ( PopUp.toggleShutterbugActionsPopUp data.popUps
                        |> (\popups -> { data | popUps = popups })
                        |> Kaput.AfterRunning
                            reason
                            errors
                            screen
                        |> Kaput
                    , Cmd.none
                    )

                _ ->
                    handleMsgError model "shutterbug pop-up toggle"

        ChangedInfoTab tab ->
            case model of
                Running cameras screen devoir data ->
                    ( { data | popUps = PopUp.changeTab data.popUps tab }
                        |> Running cameras screen devoir
                    , Cmd.none
                    )

                Kaput (Kaput.AfterRunning reason errors screen data) ->
                    ( { data | popUps = PopUp.changeTab data.popUps tab }
                        |> Kaput.AfterRunning
                            reason
                            errors
                            screen
                        |> Kaput
                    , Cmd.none
                    )

                _ ->
                    handleMsgError model "info tab change"

        OpenedNewTab url ->
            ( model, sendOpenNewTab url )

        ToggledFullscreen full ->
            ( model, sendFullscreen full )

        ToggledPointerLock locked ->
            ( case model of
                Running cameras screen devoir data ->
                    Running
                        cameras
                        (ME.ternary locked
                            (PixScreen.disableTextSelect screen)
                            screen
                        )
                        devoir
                        data

                _ ->
                    model
            , sendPointerLock locked
            )

        WroteShutterbugToClipboard ->
            ( model, sendWriteShutterbugToClipboard () )

        WroteTextToClipboard text ->
            ( model, sendWriteTextToClipboard text )

        IssuedUICommand command ->
            ( model, sendUICommand <| Devoir.toRecord command )

        ChangedShutterbugInputValue shutterbugInput ->
            case model of
                Starting data ->
                    case data.shutterbugSource of
                        UseExisting _ ->
                            ( Starting
                                { data
                                    | shutterbugSource =
                                        UseExisting shutterbugInput
                                }
                            , Cmd.none
                            )

                        MakeNew ->
                            handleError model <|
                                [ "shutterbug input value changed" ]

                _ ->
                    handleError model <|
                        [ "shutterbug input value changed" ]

        SubmittedShutterbug shutterbug ->
            case shutterbug of
                "" ->
                    -- An empty shutterbug here indicates a keydown event which
                    -- wasn't the 'Enter' key. Just ignore.
                    ( model, Cmd.none )

                _ ->
                    updateShutterbug (Shutterbug.fromString shutterbug) model

        Identity ->
            ( model, Cmd.none )


updateStatic :
    Maybe Static.About.Model
    -> Maybe Static.Motif.Model
    -> Model
    -> ( Model, Cmd Msg )
updateStatic maybeAbout maybeMotif model =
    let
        updateTheAbout : Maybe Static.About.Model -> StartingData -> StartingData
        updateTheAbout maybe data =
            case maybe of
                Just about ->
                    { data | maybeAbout = Just about }

                Nothing ->
                    data

        updateTheMotif : Maybe Static.Motif.Model -> StartingData -> StartingData
        updateTheMotif maybe data =
            case maybe of
                Just motif ->
                    { data | maybeTheme = Just <| Static.Motif.motif motif }

                Nothing ->
                    data
    in
    case model of
        Errored _ _ ->
            handleError model [ "received static data while errored" ]

        Kaput _ ->
            handleError model [ "received static data while kaput" ]

        Starting startingData ->
            updateTheAbout maybeAbout startingData
                |> updateTheMotif maybeMotif
                |> (\updatedStartingData ->
                        ( Starting updatedStartingData
                        , if Static.isComplete updatedStartingData then
                            case startingData.shutterbugSource of
                                MakeNew ->
                                    Cmd.map ReceivedShutterbug Shutterbug.create

                                UseExisting _ ->
                                    Cmd.none

                          else
                            Cmd.none
                        )
                   )

        _ ->
            handleError model [ "received more static data" ]


updateShutterbug : Shutterbug.Model -> Model -> ( Model, Cmd Msg )
updateShutterbug shutterbug model =
    case model of
        Errored _ _ ->
            handleError model [ "received a shutterbug while errored" ]

        Kaput _ ->
            handleError model [ "received a shutterbug while kaput" ]

        Starting data ->
            ( Starting { data | awaitingShutterbug = False }
            , sendShutterbug (Shutterbug.toString shutterbug)
            )

        _ ->
            handleError model [ "received another shutterbug" ]


updateLifeCycle : LifeCycle.Event -> Model -> ( Model, Cmd Msg )
updateLifeCycle event model =
    case model of
        Starting data ->
            case event of
                LifeCycle.AllWebSocketsAreOpen ->
                    ( attemptToRun { data | websockets = Connected }
                    , Cmd.none
                    )

                LifeCycle.CanvasContextCreationFailure ->
                    handleError model
                        [ "could not create a canvas rendering context" ]

                LifeCycle.GetTypescriptMotifFailure ->
                    handleError model
                        [ "could not obtain the world's motif from the server" ]

                LifeCycle.ShadowRootCreationFailure ->
                    handleError model
                        [ "could not create a shadowRoot" ]

        _ ->
            handleError model <|
                [ "received an unhandled life cycle event: "
                    ++ LifeCycle.toString event
                ]


updateCameras : Cameras.RegisteredAssociated -> Model -> ( Model, Cmd Msg )
updateCameras registeredAssociated model =
    let
        makeCmd : Cameras.Model -> Cmd Msg
        makeCmd cameras =
            if Cameras.timeNeedsUpdating cameras then
                Task.perform UpdateCamerasTime Time.now

            else
                Cmd.none
    in
    case model of
        Errored _ _ ->
            handleError model [ "received the cameras while errored" ]

        Kaput _ ->
            handleError model [ "received the cameras while kaput" ]

        Starting data ->
            Maybe.withDefault Cameras.init data.maybeCameras
                |> Cameras.update registeredAssociated
                |> (\cams ->
                        ( attemptToRun { data | maybeCameras = Just cams }
                        , makeCmd cams
                        )
                   )

        Running cameras screen devoir popups ->
            cameras
                |> Cameras.update registeredAssociated
                |> (\cams ->
                        ( Running cams screen devoir popups
                        , makeCmd cams
                        )
                   )


updatePixScreen : PixScreen.Meta -> Model -> ( Model, Cmd Msg )
updatePixScreen meta model =
    case model of
        Errored _ _ ->
            handleError model [ "received the pixels-meta while errored" ]

        Kaput (Kaput.AfterRunning notification errors pix data) ->
            ( Kaput
                (Kaput.AfterRunning
                    notification
                    errors
                    (PixScreen.updateMeta meta pix)
                    data
                )
            , Cmd.none
            )

        Kaput _ ->
            handleError model [ "received the pixels-meta while kaput" ]

        Starting data ->
            ( attemptToRun { data | maybePix = Just (PixScreen.init meta) }
            , Cmd.none
            )

        Running cameras screen devoir popups ->
            ( ( PixScreen.updateMeta meta screen
              , if
                    -- Close settings popup when pointer lock is engaged.
                    meta.pointerLock
                        && not (PixScreen.locked screen)
                        && PopUp.settings popups.popUps
                then
                    PopUp.togglePixelsAreaPopUp
                        popups.popUps
                        PopUp.SettingsPopUp
                        |> (\pops -> { popups | popUps = pops })

                else
                    popups
              )
                |> (\( pixels, pops ) -> Running cameras pixels devoir pops)
            , Cmd.none
            )


updateDevoir : Bool -> Model -> ( Model, Cmd Msg )
updateDevoir paused model =
    case model of
        Errored _ _ ->
            handleError model [ "received the devoir while errored" ]

        Kaput _ ->
            handleError model [ "received the devoir while kaput" ]

        Starting data ->
            ( attemptToRun { data | maybeDevoir = Just (Paused paused) }
            , Cmd.none
            )

        Running cameras screen _ popups ->
            ( Paused paused
                |> (\devoir -> Running cameras screen devoir popups)
            , Cmd.none
            )


updateKaput : LifeCycle.WSCloseEvent -> Model -> ( Model, Cmd Msg )
updateKaput notification model =
    case model of
        Errored _ _ ->
            handleError model [ "received a kaput while errored" ]

        Kaput _ ->
            handleError model [ "received a kaput while already kaput" ]

        Starting data ->
            ( Kaput <|
                Kaput.BeforeRunning notification
                    []
                    { extantUrl = data.extantUrl
                    , shutterbugActionsPopUp = data.shutterbugActionsPopUp
                    , theme = Theme.withInitialDefault data.maybeTheme
                    }
            , Cmd.none
            )

        Running _ screen _ data ->
            ( PopUp.hidePixelsAreaPopUp data.popUps PopUp.CamerasPopUp
                |> (\popUps -> { data | popUps = popUps })
                |> Kaput.AfterRunning notification [] screen
                |> Kaput
            , Cmd.none
            )


updateTryToUnblockCameraToggleButtons :
    Model
    -> Time.Posix
    -> ( Model, Cmd Msg )
updateTryToUnblockCameraToggleButtons model now =
    case model of
        Errored _ _ ->
            ( model, Cmd.none )

        Kaput _ ->
            ( model, Cmd.none )

        Starting data ->
            case data.maybeCameras of
                Just cameras ->
                    ( Starting
                        { data
                            | maybeCameras =
                                Just <| Cameras.tryToUnblock cameras now
                        }
                    , Cmd.none
                    )

                Nothing ->
                    handleMsgError model "unblock selectors"

        Running cameras screen devoir popups ->
            ( Running (Cameras.tryToUnblock cameras now) screen devoir popups
            , Cmd.none
            )


updateCamerasTime : Model -> Time.Posix -> ( Model, Cmd Msg )
updateCamerasTime model now =
    case model of
        Errored _ _ ->
            handleError model [ "received the cameras time while errored" ]

        Kaput _ ->
            handleError model [ "received the cameras time while kaput" ]

        Starting data ->
            case data.maybeCameras of
                Just cameras ->
                    ( Starting
                        { data
                            | maybeCameras =
                                Just <| Cameras.updateTime cameras now
                        }
                    , Cmd.none
                    )

                Nothing ->
                    handleMsgError model "received the cameras time early"

        Running cameras screen devoir popups ->
            ( Cameras.updateTime cameras now
                |> (\cams -> Running cams screen devoir popups)
            , Cmd.none
            )


updateToggledCamera : Model -> Cameras.ToggleMsg -> ( Model, Cmd Msg )
updateToggledCamera model toggleMsg =
    case model of
        Errored _ _ ->
            handleError model [ "received a camera toggle while errored" ]

        Kaput _ ->
            handleError model [ "received a camera toggle while kaput" ]

        Starting data ->
            case data.maybeCameras of
                Just cameras ->
                    ( Starting
                        { data
                            | maybeCameras =
                                Just <| Cameras.toggle toggleMsg cameras
                        }
                    , Cmd.none
                    )

                Nothing ->
                    handleMsgError model "received a camera toggle early"

        Running cameras screen devoir popups ->
            ( Running
                (Cameras.toggle toggleMsg cameras)
                screen
                devoir
                popups
            , Cmd.none
            )


handleError : Model -> List String -> ( Model, Cmd Msg )
handleError model err =
    case model of
        Errored theme errors ->
            ( Errored theme <| errors ++ [ err ], Cmd.none )

        Kaput (Kaput.BeforeRunning reason errors popups) ->
            ( errors
                ++ [ err ]
                |> (\errs -> Kaput.BeforeRunning reason errs popups)
                |> Kaput
            , Cmd.none
            )

        Kaput (Kaput.AfterRunning reason errors screen popups) ->
            ( errors
                ++ [ err ]
                |> (\errs -> Kaput.AfterRunning reason errs screen popups)
                |> Kaput
            , Cmd.none
            )

        Starting data ->
            ( Errored (Theme.withInitialDefault data.maybeTheme) [ err ], Cmd.none )

        Running _ _ _ data ->
            ( Errored data.theme [ err ], Cmd.none )


handleMsgError : Model -> String -> ( Model, Cmd Msg )
handleMsgError model msgDescription =
    handleError
        model
        [ "received " ++ msgDescription ++ " message from an invalid state" ]



-- VIEW


view : Model -> Browser.Document Msg
view model =
    { title = "mechane"
    , body =
        List.map H.toUnstyled
            [ View.globalStyleNode
                (case model of
                    Starting data ->
                        Theme.withInitialDefault data.maybeTheme

                    Running _ _ _ data ->
                        data.theme

                    Errored theme _ ->
                        theme

                    Kaput kaput ->
                        Kaput.theme kaput
                )
            , viewModel model
            ]
    }


viewModel : Model -> Html Msg
viewModel model =
    case model of
        Starting data ->
            let
                theme =
                    Theme.withInitialDefault data.maybeTheme
            in
            if not <| Static.isComplete data then
                View.startingGrid
                    theme
                    "welcome to "
                    (View.tickedList theme <| List.take 1 View.startingTasks)
                    (H.text "")
                    (H.text "")

            else if data.awaitingShutterbug then
                View.startingGrid
                    theme
                    "welcome to "
                    (View.tickedList theme <| List.take 2 View.startingTasks)
                    (case data.shutterbugSource of
                        MakeNew ->
                            H.text ""

                        UseExisting shutterbugData ->
                            View.Shutterbug.inputBox
                                theme
                                SubmittedShutterbug
                                (\input ->
                                    ChangedShutterbugInputValue
                                        { shutterbugInput =
                                            input
                                        , shutterbugUnmasked =
                                            shutterbugData.shutterbugUnmasked
                                        }
                                )
                                (ChangedShutterbugInputValue
                                    { shutterbugInput =
                                        shutterbugData.shutterbugInput
                                    , shutterbugUnmasked =
                                        not shutterbugData.shutterbugUnmasked
                                    }
                                )
                                { input =
                                    shutterbugData.shutterbugInput
                                , unmasked =
                                    shutterbugData.shutterbugUnmasked
                                }
                    )
                    (H.text "")

            else if data.websockets == Connecting then
                View.startingGrid
                    theme
                    "welcome to "
                    (View.tickedList theme <| List.take 3 View.startingTasks)
                    (H.text "")
                    (viewShutterbugActions theme Nothing data)

            else if
                data.websockets
                    == Connected
                    && ((data.maybeCameras == Nothing)
                            || (data.maybeDevoir == Nothing)
                       )
            then
                View.startingGrid
                    theme
                    "welcome to "
                    (View.tickedList theme <| List.take 4 View.startingTasks)
                    (H.text "")
                    (viewShutterbugActions theme Nothing data)

            else if
                not data.sentCameraAssociations
                    && not
                        (Cameras.associated
                            (Maybe.withDefault
                                Cameras.init
                                data.maybeCameras
                            )
                        )
            then
                View.startingGrid
                    theme
                    "welcome to "
                    (View.tickedList theme <| List.take 5 View.startingTasks)
                    (View.Cameras.starting
                        { theme = Theme.withInitialDefault data.maybeTheme
                        , toggleMsg = ToggledCamera
                        , assocMsg = IssuedCameraAssociations
                        , stitchMsg = StitchingMsg
                        , cameras =
                            Maybe.withDefault Cameras.init data.maybeCameras
                        , extantUrl = data.extantUrl
                        , shutterbugActionsPopUp = data.shutterbugActionsPopUp
                        , source =
                            case data.shutterbugSource of
                                MakeNew ->
                                    Shutterbug.MakeNew

                                UseExisting _ ->
                                    Shutterbug.UseExisting
                        }
                    )
                    (viewShutterbugActions theme Nothing data)

            else
                View.startingGrid
                    theme
                    "welcome to "
                    (View.tickedList theme <| List.take 6 View.startingTasks)
                    (H.text "")
                    (viewShutterbugActions theme Nothing data)

        Running cameras screen devoir data ->
            viewRunning cameras screen devoir data

        Errored theme errors ->
            viewErrored theme errors

        Kaput kaput ->
            viewKaput kaput


viewRunning :
    Cameras.Model
    -> PixScreen.Model
    -> Devoir
    ->
        { extantUrl : String
        , popUps : PopUp.Model
        , theme : Theme
        }
    -> Html Msg
viewRunning cameras screen devoir data =
    let
        controlColumn =
            viewControlColumn data.theme cameras devoir data.popUps

        pixelsColumn =
            viewPixelsColumn
                data.theme
                cameras
                screen
                PixScreen.backgroundColour
                data.popUps
                Nothing
                []

        metaColumn =
            viewMetaColumn data.theme Nothing data
    in
    View.mainColumns
        controlColumn
        pixelsColumn
        metaColumn


viewMetaColumn :
    Theme
    -> Maybe LifeCycle.WSCloseEvent
    ->
        { a
            | extantUrl : String
            , popUps : PopUp.Model
        }
    -> Html Msg
viewMetaColumn theme maybeNotification data =
    H.div
        [ HA.css
            [ C.alignItems C.flexEnd
            , C.displayFlex
            , C.flexDirection C.column
            , C.height <| C.pct 100
            , C.justifyContent C.flexEnd
            , C.property "overflow-y" "clip"
            , C.position C.relative
            , C.paddingLeft <| C.em theme.buttonPadding
            , C.paddingRight <| C.em theme.buttonPadding
            ]
        ]
        [ H.div
            [ HA.css [ C.flex <| C.int 1 ] ]
            (case maybeNotification of
                Just notification ->
                    [ H.button
                        [ HA.css <|
                            [ Theme.bottomMargin theme
                            , C.marginTop <| C.em theme.buttonPadding
                            ]
                                ++ Theme.kaputButtonStyles theme
                        , HA.title <| LifeCycle.explainWSCloseEvent notification
                        ]
                        [ View.buttonIcon theme "power_settings_new" ]
                    ]

                Nothing ->
                    [ H.button
                        [ My.Html.onClickPreventDefault <|
                            IssuedUICommand Devoir.Shutdown
                        , HA.css <|
                            [ Theme.bottomMargin theme
                            , C.marginTop <| C.em theme.buttonPadding
                            ]
                                ++ .standard (Theme.button theme)
                        , HA.title "Shut down Mechane."
                        ]
                        [ View.buttonIcon theme "power_settings_new" ]
                    ]
            )
        , My.Html.safeClickButton
            ToggledSettingsPopUp
            Identity
            [ HA.css <| .standard (Theme.buttonWithMargin theme)
            , HA.title "Customise this tab's looks and behaviour."
            ]
            [ View.expanderButtonIcon
                theme
                "tab"
                (PopUp.settings data.popUps)
                (PopUp.anyPixelsArea data.popUps)
            ]
        , My.Html.safeClickButton
            ToggledInfoPopUp
            Identity
            [ HA.css <| .standard (Theme.buttonWithMargin theme)
            , HA.title "Learn more about this Mechane world."
            ]
            [ View.expanderButtonIcon
                theme
                "public"
                (PopUp.info data.popUps)
                (PopUp.anyPixelsArea data.popUps)
            ]
        , viewShutterbugActions theme maybeNotification <|
            { extantUrl = data.extantUrl
            , shutterbugActionsPopUp = PopUp.shutterbug data.popUps
            }
        ]


viewControlColumn :
    Theme
    -> Cameras.Model
    -> Devoir
    -> PopUp.Model
    -> Html Msg
viewControlColumn theme cameras (Paused paused) popups =
    H.div
        [ HA.css <| Theme.controlColumnDivStyle theme ]
        [ My.Html.safeClickButton
            ToggledCamerasPopUp
            Identity
            [ HA.css <| .standard (Theme.buttonWithMargin theme)
            , HA.title <|
                "Select cameras to view.\n"
                    ++ Cameras.availability cameras
            ]
            [ View.expanderButtonIcon
                theme
                "switch_video"
                (PopUp.cameras popups)
                (PopUp.anyPixelsArea popups)
            ]
        , if paused then
            My.Html.safeClickButton
                (IssuedUICommand Devoir.Play)
                Identity
                [ HA.css <| .standard (Theme.buttonWithMargin theme)
                , HA.title "Play world.\n"
                ]
                [ View.buttonIcon theme "play_arrow" ]

          else
            My.Html.safeClickButton
                (IssuedUICommand Devoir.Pause)
                Identity
                [ HA.css <| .standard (Theme.buttonWithMargin theme)
                , HA.title "Pause world.\n"
                ]
                [ View.buttonIcon theme "pause" ]
        ]


viewPixelsColumn :
    Theme
    -> Cameras.Model
    -> PixScreen.Model
    -> String
    -> PopUp.Model
    -> Maybe LifeCycle.WSCloseEvent
    -> List (List String)
    -> Html Msg
viewPixelsColumn theme cameras screen colour popups maybeNotification errors =
    -- The custom element that displays the views from the cameras associated
    -- with the shutterbug.
    let
        dim =
            PixScreen.dimensions screen

        pixelZ =
            0

        shadowZ =
            1

        offZ =
            2

        onZ =
            3

        h =
            if PixScreen.fit screen then
                if PixScreen.border screen then
                    "calc(100vh - 2 * "
                        ++ String.fromFloat theme.buttonBorder
                        ++ "em)"

                else
                    "100vh"

            else
                String.fromInt dim.h ++ "px"

        w =
            if PixScreen.fit screen then
                if PixScreen.border screen then
                    "calc( "
                        ++ String.fromFloat PixScreen.ratio
                        ++ " * ( 100vh - 2 * "
                        ++ String.fromFloat theme.buttonBorder
                        ++ "em ) )"

                else
                    "calc( "
                        ++ String.fromFloat PixScreen.ratio
                        ++ " * 100vh )"

            else
                String.fromInt dim.w ++ "px"

        b =
            "border: "
                ++ String.fromFloat theme.buttonBorder
                ++ "em solid "
                ++ Theme.colToString theme.primary
                ++ "; border-radius: "
                ++ String.fromFloat theme.buttonBorderRadius
                ++ "em;"
                ++ "background-color: "
                ++ Theme.colToString theme.primary
                ++ ";"
    in
    H.div
        [ HA.css
            [ C.property "display" "grid"
            , C.property "grid-template-columns" "50% 50%"
            , C.property "grid-template-rows" "auto"
            , C.property "overflow" "hidden"
            ]
        ]
        [ H.div
            [ HA.css <|
                ME.appendIf (PixScreen.border screen)
                    [ PopUp.anyPixelsArea popups
                        |> (\any ->
                                C.boxShadow6
                                    C.inset
                                    (C.px 0)
                                    (C.px 0)
                                    (ME.ternary any (C.px 20) (C.px 0))
                                    (ME.ternary any (C.px 4) (C.px 0))
                                    (C.rgb 0 0 0)
                           )
                    , C.property "grid-column" "1 / span 2"
                    , C.property "grid-row" "1 / span 1"
                    , C.height <| C.pct 100
                    , C.pointerEvents C.none
                    , CT.transition [ CT.boxShadow Theme.popUpDuration ]
                    , C.zIndex <| C.int shadowZ
                    ]
                    [ C.borderRadius <| C.em theme.buttonBorderRadius ]
            ]
            []
        , H.div
            [ HA.css
                [ C.property "grid-column" "1 / span 2"
                , C.property "grid-row" "1 / span 1"
                , C.zIndex (C.int pixelZ)
                ]
            ]
            [ H.node PixScreen.tag
                (ME.appendWhen
                    [ HA.attribute PixScreen.attributes.width w
                    , HA.attribute PixScreen.attributes.height h
                    , HA.attribute PixScreen.attributes.colour colour
                    ]
                    [ ( PixScreen.fit screen
                      , [ HA.css
                            [ C.property "grid-column" "1 / span 2"
                            , C.property "grid-row" "1 / span 1"
                            ]
                        ]
                      )
                    , ( PixScreen.border screen
                      , [ HA.attribute PixScreen.attributes.border b ]
                      )
                    , ( PixScreen.selectable screen
                      , [ HA.attribute PixScreen.attributes.selectable "true" ]
                      )
                    , ( maybeNotification /= Nothing
                      , [ HA.attribute PixScreen.attributes.kaput "true"
                        ]
                      )
                    ]
                )
                []
            ]
        , H.div
            [ HA.css
                [ C.property "grid-column" "1 / span 1"
                , C.property "grid-row" "1 / span 1"
                , C.property "align-self" "center"
                , C.property "justify-self" "start"
                , C.position C.relative
                , C.zIndex
                    (C.int <| ME.ternary (PopUp.cameras popups) onZ offZ)
                ]
            ]
            [ View.Cameras.popUp
                { theme = theme
                , toggleCamMsg = ToggledCamera
                , toggleSelModeMsg = ToggledCameraSelectionMode
                , assocMsg = IssuedCameraAssociations
                , stitchMsg = StitchingMsg
                , clearMsg = ClearedCameraSelections
                , autoMsg = ToggledAutoCloseCameras
                , cameras = cameras
                , on = PopUp.cameras popups
                , autoClosePopUp = popups.autoCloseCameras
                }
            ]
        , H.div
            [ HA.css
                [ C.property "grid-column" "1 / span 2"
                , C.property "grid-row" "1 / span 1"
                , C.property "place-self" "end center"
                , C.position C.relative
                , C.zIndex
                    (C.int <| ME.ternary (PopUp.info popups) onZ offZ)
                ]
            ]
            [ View.Info.popup
                theme
                (\text -> WroteTextToClipboard text)
                (\tab -> ChangedInfoTab tab)
                popups
                errors
            ]
        , H.div
            [ HA.css
                [ C.property "grid-column" "2 / span 1"
                , C.property "grid-row" "1 / span 1"
                , C.property "place-self" "center end"
                , C.position C.relative
                , C.zIndex
                    (C.int <| ME.ternary (PopUp.settings popups) onZ offZ)
                ]
            ]
            [ View.Settings.popUp
                theme
                { inaction = Identity
                , border = ToggledOutputBorder
                , fit = ToggledOutputSize
                , full =
                    \pix ->
                        ToggledFullscreen
                            (not <| PixScreen.full pix)
                , lock =
                    \pix ->
                        ToggledPointerLock
                            (not <| PixScreen.locked pix)
                , selectable = ToggledTextSelect
                }
                screen
                (PopUp.settings popups)
                maybeNotification
            ]
        ]


viewShutterbugActions :
    Theme
    -> Maybe LifeCycle.WSCloseEvent
    ->
        { a
            | shutterbugActionsPopUp : Bool
            , extantUrl : String
        }
    -> Html Msg
viewShutterbugActions theme maybeNotification data =
    View.Shutterbug.actions
        { theme = theme
        , tabMsg = OpenedNewTab data.extantUrl
        , togglePopUpMsg = ToggledShutterbugPopUp
        , copyMsg = WroteShutterbugToClipboard
        , inactionMsg = Identity
        , maybeNotification = maybeNotification
        , shutterbugActionsPopUp = data.shutterbugActionsPopUp
        , extantUrl = data.extantUrl
        }


viewKaput :
    Kaput.Model
    -> Html Msg
viewKaput kaput =
    case kaput of
        Kaput.BeforeRunning notification errors data ->
            viewKaputBeforeRunning data.theme notification errors (Just data)

        Kaput.AfterRunning notification errors screen data ->
            viewKaputAfterRunning data.theme notification errors screen data


viewKaputBeforeRunning :
    Theme
    -> LifeCycle.WSCloseEvent
    -> List (List String)
    ->
        Maybe
            { a
                | shutterbugActionsPopUp : Bool
                , extantUrl : String
            }
    -> Html Msg
viewKaputBeforeRunning theme notification errors maybeData =
    List.append (Kaput.viewNotification notification)
        ([ H.h3 [] [ H.text "Subsequent errors" ]
         , View.errorsList theme errors
         ]
            |> ME.useIf (List.length errors > 0)
        )
        |> H.div [ HA.css Theme.startingGridMiddleDivStyle ]
        |> (\middle ->
                View.startingGrid
                    theme
                    "ta-ta from"
                    (H.div [] [])
                    middle
                    (case maybeData of
                        Just data ->
                            viewShutterbugActions theme (Just notification) data

                        Nothing ->
                            H.text ""
                    )
           )


viewKaputAfterRunning :
    Theme
    -> LifeCycle.WSCloseEvent
    -> List (List String)
    -> PixScreen.Model
    ->
        { a
            | extantUrl : String
            , popUps : PopUp.Model
            , theme : Theme
        }
    -> Html Msg
viewKaputAfterRunning theme notification errors screen data =
    View.mainColumns
        (viewKaputControlColumn theme notification)
        (viewPixelsColumn
            data.theme
            -- Use an empty cameras since the cameras popup is disabled.
            Cameras.init
            screen
            PixScreen.backgroundColour
            data.popUps
            (Just notification)
            errors
        )
        (viewMetaColumn theme (Just notification) data)


viewKaputControlColumn : Theme -> LifeCycle.WSCloseEvent -> Html Msg
viewKaputControlColumn theme notification =
    let
        kaputButtonStyles =
            Theme.bottomMargin theme :: Theme.kaputButtonStyles theme
    in
    H.div
        [ HA.css <| Theme.controlColumnDivStyle theme ]
        [ H.div
            [ HA.css <| kaputButtonStyles
            , HA.title <| LifeCycle.explainWSCloseEvent notification
            ]
            [ View.buttonIcon theme "videocam_off" ]
        , H.button
            [ HA.css <| kaputButtonStyles
            , HA.title <| LifeCycle.explainWSCloseEvent notification
            ]
            [ View.buttonIcon theme "play_arrow" ]
        ]


viewErrored : Theme -> List (List String) -> Html Msg
viewErrored theme errors =
    (case errors of
        [] ->
            [ H.h2 [] [ H.text "Error" ]
            , View.errorsList theme [ [ "empty errors list" ] ]
            ]

        x :: [] ->
            [ H.h2 [] [ H.text "Error" ]
            , View.errorsList theme [ x ]
            ]

        x :: xs ->
            [ H.h2 [] [ H.text "Error" ]
            , H.h3 [] [ H.text "Main error" ]
            , View.errorsList theme [ x ]
            , H.h3 [] [ H.text "Subsequent errors" ]
            , View.errorsList theme xs
            ]
    )
        |> H.div [ HA.css Theme.startingGridMiddleDivStyle ]
        |> (\middle ->
                View.startingGrid
                    theme
                    "toodle-oo from"
                    (H.div [] [])
                    middle
                    (H.div [] [])
           )



-- MAIN


main : Program D.Value Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
