module LifeCycle exposing
    ( Event(..)
    , WSCloseEvent
    , decodeLifeCycleMessage
    , decodeWSCloseEvent
    , explainWSCloseEvent
    , isNormalWSCloseEvent
    , toString
    )

import Json.Decode as D


decodeProblem : D.Decoder String
decodeProblem =
    D.field "problem" D.string


decodeStage : D.Decoder String
decodeStage =
    D.field "stage" D.string


decodeLifeCycleMessage : D.Decoder (Result String Event)
decodeLifeCycleMessage =
    D.oneOf
        [ decodeProblem |> D.map problemToEvent
        , decodeStage |> D.map stageToEvent
        ]


decodeWSCloseEvent : D.Decoder WSCloseEvent
decodeWSCloseEvent =
    D.map4 WSCloseEvent
        (D.field "code" D.int)
        (D.field "reason" D.string)
        (D.field "sourceWebSocket" D.string)
        (D.field "wasClean" D.bool)


type Event
    = CanvasContextCreationFailure
    | GetTypescriptMotifFailure
    | ShadowRootCreationFailure
    | AllWebSocketsAreOpen



-- A notification of a websocket close event with details of the event.


type alias WSCloseEvent =
    { code : Int -- https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent
    , reason : String
    , sourceSocket : String
    , wasClean : Bool
    }



-- The exact possible lifecycle event messages' text content sent from the
-- Typescript integrant.


eventMessages :
    { problem : { context : String, motif : String, shadowRoot : String }
    , stage : { webSocketsOpen : String }
    }
eventMessages =
    { problem =
        { context = "failed to create a canvas rendering context"
        , motif = "failed to get the motif"
        , shadowRoot = "failed to create a shadowRoot"
        }
    , stage =
        { webSocketsOpen = "all WebSockets are open"
        }
    }


explainWSCloseEvent : WSCloseEvent -> String
explainWSCloseEvent notification =
    case friendlyReason notification.reason of
        Just explanation ->
            "No connection.\n" ++ explanation

        Nothing ->
            "No connection.\n"
                ++ (if String.length notification.reason > 0 then
                        "Websocket close reason: " ++ notification.reason ++ ".\n"

                    else
                        ""
                   )
                ++ "Websocket close code: "
                ++ String.fromInt notification.code
                ++ ".\n"
                ++ "Websocket path: "
                ++ notification.sourceSocket
                ++ ".\n"


friendlyReason : String -> Maybe String
friendlyReason terseReason =
    case
        List.foldl
            (\( terse, friendly ) acc ->
                if terse == terseReason then
                    friendly

                else
                    acc
            )
            ""
            normalReasons
    of
        "" ->
            Nothing

        friendly ->
            Just friendly


isNormalWSCloseEvent : WSCloseEvent -> Bool
isNormalWSCloseEvent notification =
    List.map (\( terse, _ ) -> terse) normalReasons
        |> List.member notification.reason



-- Normal closure reasons.
--
-- The first string in each tuple needs to be an exact match to one of the set
-- of reasons which can be sent to a client by the Mechane server.
--
-- The second string in each tuple is a friendly concise explanation of the
-- closure.


normalReasons : List ( String, String )
normalReasons =
    [ ( "received client request"
      , "Mechane was shut down by a client.\n"
      )
    , ( "intercepted terminal interrupt"
      , "Mechane was shut down from a terminal.\n"
      )
    ]


toString : Event -> String
toString event =
    case event of
        CanvasContextCreationFailure ->
            eventMessages.problem.context

        GetTypescriptMotifFailure ->
            eventMessages.problem.motif

        ShadowRootCreationFailure ->
            eventMessages.problem.shadowRoot

        AllWebSocketsAreOpen ->
            eventMessages.stage.webSocketsOpen


problemToEvent : String -> Result String Event
problemToEvent problem =
    case problem of
        "failed to create a canvas rendering context" ->
            Ok CanvasContextCreationFailure

        "failed to get the Typescript motif" ->
            Ok GetTypescriptMotifFailure

        "failed to create a shadowRoot" ->
            Ok ShadowRootCreationFailure

        _ ->
            Err <| "this problem is not accounted for: " ++ problem


stageToEvent : String -> Result String Event
stageToEvent stage =
    case stage of
        "all WebSockets are open" ->
            Ok AllWebSocketsAreOpen

        _ ->
            Err <| "this stage is not accounted for: " ++ stage
