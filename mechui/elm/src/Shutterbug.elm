module Shutterbug exposing
    ( Meta
    , Model
    , Msg
    , Source(..)
    , create
    , defaultSource
    , fromString
    , init
    , manualEntryURL
    , maxLength
    , minLength
    , queryKey
    , queryValues
    , regex
    , toString
    , validateLength
    )

import Http
import Interval
import Json.Decode as D
import My.Http
import Regex
import Url


type alias Meta =
    { source : Source
    , manualEntryURL : String
    }


type Model
    = Model String -- A base64 encoded string of the shutterbug's bits.


type Msg
    = Msg (Result Http.Error String)



-- Determines how the shutterbug is obtained.


type Source
    = UseExisting -- An existing shutterbug is provided manually.
    | MakeNew -- A new shutterbug is requested from the server.


defaultSource : Source
defaultSource =
    MakeNew



-- The number of 8-bit bytes in an unencoded shutterbug.


input : Int
input =
    -- 256 random bits.
    32



-- Maximum expected length of the shutterbug in Base64 form. Implies padding was
-- used.


maxLength : Int
maxLength =
    (input + 2) // 3 * 4



-- Minimum expected length of the shutterbug in Base64 form. Implies no padding
-- was used.


minLength : Int
minLength =
    (input * 4 + 2) // 3



-- The path on a Mechane server where shutterbugs can be created.


path : String
path =
    "/shutterbug"


queryKey : String
queryKey =
    "shutterbug"


queryValues : { extant : String, new : String }
queryValues =
    { extant = "extant" -- Use an existing shutterbug.
    , new = "new" -- Create a new shutterbug on the server.
    }


regex : Regex.Regex
regex =
    case Regex.fromString "^[\\w-]{43}=$" of
        Just r ->
            r

        Nothing ->
            Regex.never


create : Cmd Msg
create =
    Http.post
        { url = path
        , body = Http.emptyBody
        , expect = Http.expectJson Msg D.string
        }


fromString : String -> Model
fromString shutterbug =
    Model shutterbug


init : Msg -> Result String Model
init msg =
    case msg of
        Msg (Ok shutterbug) ->
            case validateLength shutterbug of
                Ok _ ->
                    Ok (Model shutterbug)

                Err err ->
                    Err err

        Msg (Err err) ->
            Err (My.Http.errorToString err)



-- Use parts taken from the given host URL to make a URL which upon visiting in
-- a new tab allows manual entry of an extant shutterbug for that tab to use.


manualEntryURL : Url.Url -> String
manualEntryURL location =
    String.concat
        [ case location.protocol of
            Url.Http ->
                "http://"

            Url.Https ->
                "https://"
        , location.host
        , case location.port_ of
            Just urlPort ->
                ":" ++ String.fromInt urlPort

            Nothing ->
                ""
        , "/?"
            ++ queryKey
            ++ "="
            ++ queryValues.extant
        ]


toString : Model -> String
toString model =
    case model of
        Model shutterbug ->
            shutterbug


validateLength : String -> Result String String
validateLength shutterbug =
    if
        Interval.contains
            (String.length shutterbug)
            (Interval.from minLength maxLength)
    then
        Result.Ok shutterbug

    else
        Result.Err "the shutterbug's length is invalid"
