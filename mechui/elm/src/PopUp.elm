module PopUp exposing (..)

import Static.About


type alias Model =
    { about : Static.About.Model
    , autoCloseCameras : Bool
    , infoTab : InfoTab
    , pixelsArea : Maybe PixelsAreaPopUp
    , shutterbugActions : Bool
    }


type PixelsAreaPopUp
    = CamerasPopUp
    | InfoPopUp
    | SettingsPopUp


type InfoTab
    = WorldTab
    | DocsTab
    | MonitorTab
    | AttributionsTab


type InfoTabMsg
    = InfoTabMsg InfoTab


anyPixelsArea : Model -> Bool
anyPixelsArea model =
    case model.pixelsArea of
        Just _ ->
            True

        Nothing ->
            False


cameras : Model -> Bool
cameras model =
    case model.pixelsArea of
        Just CamerasPopUp ->
            True

        _ ->
            False


changeTab : Model -> InfoTabMsg -> Model
changeTab model (InfoTabMsg tab) =
    { model | infoTab = tab }



-- Hide a particular pop up if it is on. If it is off then leave it off.


hidePixelsAreaPopUp : Model -> PixelsAreaPopUp -> Model
hidePixelsAreaPopUp model popUp =
    case model.pixelsArea of
        Just currentPopUp ->
            if currentPopUp == popUp then
                { model | pixelsArea = Nothing }

            else
                model

        Nothing ->
            model


info : Model -> Bool
info model =
    case model.pixelsArea of
        Just InfoPopUp ->
            True

        _ ->
            False


infoAttributionsTab : Model -> Bool
infoAttributionsTab model =
    case model.infoTab of
        AttributionsTab ->
            True

        _ ->
            False


infoDocsTab : Model -> Bool
infoDocsTab model =
    case model.infoTab of
        DocsTab ->
            True

        _ ->
            False


infoMonitorTab : Model -> Bool
infoMonitorTab model =
    case model.infoTab of
        MonitorTab ->
            True

        _ ->
            False


infoWorldTab : Model -> Bool
infoWorldTab model =
    case model.infoTab of
        WorldTab ->
            True

        _ ->
            False


init : Static.About.Model -> Model
init about =
    { about = about
    , autoCloseCameras = True
    , infoTab = WorldTab
    , pixelsArea = Nothing
    , shutterbugActions = False
    }


settings : Model -> Bool
settings model =
    case model.pixelsArea of
        Just SettingsPopUp ->
            True

        _ ->
            False


shutterbug : Model -> Bool
shutterbug model =
    model.shutterbugActions


togglePixelsAreaPopUp : Model -> PixelsAreaPopUp -> Model
togglePixelsAreaPopUp model popUp =
    case model.pixelsArea of
        Just currentPopUp ->
            if currentPopUp == popUp then
                { model | pixelsArea = Nothing }

            else
                { model | pixelsArea = Just popUp }

        Nothing ->
            { model | pixelsArea = Just popUp }


toggleShutterbugActionsPopUp : Model -> Model
toggleShutterbugActionsPopUp model =
    { model | shutterbugActions = not model.shutterbugActions }
