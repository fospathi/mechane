port module Cameras exposing
    ( AssociateMsg(..)
    , Changes
    , Model
    , RegisteredAssociated
    , RegisteredAssociatedMsg
    , ToggleMsg(..)
    , associated
    , availability
    , blockedInterval
    , buttonAnimationDuration
    , clearSelection
    , decodeRegisteredAssociatedMessage
    , difference
    , displayedNames
    , incrementSelectionSession
    , init
    , radio
    , recentAddedNames
    , recentAutoDeselected
    , recentAutoDisassociated
    , recentDifference
    , recentRemovedNames
    , registered
    , registeredAssociatedSub
    , selected
    , selectedNames
    , selectionSession
    , switchCameras
    , timeNeedsUpdating
    , toggle
    , toggleSelectionMode
    , toggleStitchingSize
    , tryToUnblock
    , unblock
    , update
    , updateTime
    )

import Camera
import Json.Decode as D
import List.Extra
import My.Extra
import Set exposing (Set)
import Stitch
import Svg.Styled.Attributes exposing (mode)
import Time



-- A port to receive the stream of 'registered and associated cameras' messages
-- received from the Typescript integrant.


port receiveRegisteredAssociatedCameras : (D.Value -> msg) -> Sub msg



-- A subscription to the stream of 'registered and associated cameras' messages
-- received from the Typescript integrant.


registeredAssociatedSub : Sub RegisteredAssociatedMsg
registeredAssociatedSub =
    receiveRegisteredAssociatedCameras RegisteredAssociatedMsg


decodeListCameras : D.Decoder (List Camera.Model)
decodeListCameras =
    D.list D.string |> D.andThen (\ss -> D.succeed <| List.map Camera.init ss)



-- Decode the 'registered and associated cameras' message.


decodeRegisteredAssociatedMessage :
    RegisteredAssociatedMsg
    -> Result String RegisteredAssociated
decodeRegisteredAssociatedMessage (RegisteredAssociatedMsg value) =
    case
        D.decodeValue
            (D.map3 RegisteredAssociatedSchema
                (D.at [ "associated" ] decodeListCameras)
                (D.at [ "registered" ] decodeListCameras)
                (D.at [ "stitching" ] Stitch.decode)
            )
            value
    of
        Ok registeredCameras ->
            case
                Stitch.initAssociations
                    registeredCameras.stitching
                    (Camera.namesOf registeredCameras.associated)
            of
                Ok associations ->
                    Ok <|
                        RegisteredAssociated
                            associations
                            registeredCameras.registered

                Err err ->
                    Err err

        Err err ->
            Err (D.errorToString err)



-- Current camera selections and available cameras.


type Model
    = Model SelectionMode Registry


type SelectionMode
    = Multiple
    | Single



-- A registry of cameras. The registered cameras list encompasses all possible
-- cameras including the selected ones.


type alias Registry =
    { allRegistered : List Camera.Model
    , associated : Stitch.Associations
    , recentChanges : Maybe Changes
    , selection : Stitch.Associations
    , selectionSession : Int
    }



-- A summary of the changes in the model due to a registered cameras message.
--
-- The deselected are a subset of the removed. Here the deselected are not user
-- but auto deselections when a camera was selected but became no longer
-- available. Likewise for the disassociated.


type alias Changes =
    { added : List String
    , autoDeselected : Set String
    , autoDisassociated : Set String
    , removed : List String
    , latestUpdate : Time.Posix
    , needToUpdateTime : Bool
    }



-- The result of a successful decoding of a 'registered and associated cameras'
-- message received from the Typescript integrant.
--
-- Includes all currently registered cameras along with which cameras the
-- shutterbug is associated with and the camera stitching method.


type alias RegisteredAssociatedSchema =
    { associated : List Camera.Model -- Farthest to closest stacking order.
    , registered : List Camera.Model
    , stitching : Stitch.Model
    }



-- A condensed version of the RegisteredAssociatedSchema.


type alias RegisteredAssociated =
    { associated : Stitch.Associations
    , registered : List Camera.Model
    }



-- A request that the selected cameras are associated with the shutterbug used
-- by this client, or equivalently, that the selected cameras are displayed in
-- the pixels area. The message originates from a UI event, contains the names
-- of all the selected cameras, is sent through a port to the Typescript
-- integrant, and is then forwarded to the Mechane server.


type AssociateMsg
    = AssociateMsg Stitch.AssociateSchema



-- A message originating from the Mechane server which contains the names of all
-- the registered cameras and which is used to update the model.


type RegisteredAssociatedMsg
    = RegisteredAssociatedMsg D.Value



-- A message originating from a UI event which contains the name of a camera and
-- which is used to toggle the camera between its selected and deselected
-- state.


type ToggleMsg
    = ToggleMsg String


init : Model
init =
    Model
        Single
        { allRegistered = []
        , associated = Stitch.unassociated
        , recentChanges = Nothing
        , selection = Stitch.unassociated
        , selectionSession = 0
        }



-- The number of milliseconds to stop side effects caused by clicking camera
-- toggle buttons after a change in the registered cameras.


blockedInterval : Int
blockedInterval =
    1750



-- How long to shrink and grow buttons. Should be shorter than the block
-- interval duration.


buttonAnimationDuration : String
buttonAnimationDuration =
    "1.4s"



-- A placeholder value for a time field.


timeZeroValue : Time.Posix
timeZeroValue =
    Time.millisToPosix 0



-- True if the shutterbug is associated with any camera.


associated : Model -> Bool
associated (Model _ registry) =
    List.length (Stitch.names registry.associated) > 0



-- A brief explanation of the number of available cameras.


availability : Model -> String
availability (Model _ registry) =
    case List.length registry.allRegistered of
        0 ->
            "No cameras available."

        1 ->
            "1 camera available."

        n ->
            String.fromInt n ++ " cameras available."



-- Clear the selected cameras in a camera selection session.


clearSelection : Model -> Model
clearSelection (Model mode registry) =
    Model
        mode
        { registry
            | selection = Stitch.Uni Nothing
        }



-- Explain what difference submitting the current selection in a selection
-- session would make to the camera associations.


difference : Model -> String
difference (Model _ registry) =
    Stitch.summariseChanges registry.associated registry.selection



-- All the names of available cameras including selected and unselected cameras
-- plus any recently removed camera names which were removed due to a registered
-- cameras message.


displayedNames : Model -> List String
displayedNames (Model _ registry) =
    case registry.recentChanges of
        Just changes ->
            List.map Camera.nameOf registry.allRegistered ++ changes.removed

        Nothing ->
            List.map Camera.nameOf registry.allRegistered



-- Increase the selection session number by one and set the selection to the
-- current associations.


incrementSelectionSession : Model -> Model
incrementSelectionSession (Model mode registry) =
    Model
        mode
        { registry
            | selectionSession = registry.selectionSession + 1
            , selection = registry.associated
        }


radio : Model -> Bool
radio (Model mode _) =
    case mode of
        Single ->
            True

        Multiple ->
            False



-- Recently added camera names due to a registered cameras message.


recentAddedNames : Model -> List String
recentAddedNames (Model _ registry) =
    case registry.recentChanges of
        Just changes ->
            changes.added

        Nothing ->
            []


recentAutoDeselected : String -> Model -> Bool
recentAutoDeselected name (Model _ registry) =
    case registry.recentChanges of
        Just changes ->
            Set.member name changes.autoDeselected

        Nothing ->
            False


recentAutoDisassociated : String -> Model -> Bool
recentAutoDisassociated name (Model _ registry) =
    case registry.recentChanges of
        Just changes ->
            Set.member name changes.autoDisassociated

        Nothing ->
            False



-- Recent changes to the model due to a registered cameras message.


recentDifference : Model -> Maybe Changes
recentDifference (Model _ registry) =
    registry.recentChanges



-- Recently removed camera names due to a registered cameras message.


recentRemovedNames : Model -> List String
recentRemovedNames (Model _ registry) =
    case registry.recentChanges of
        Just changes ->
            changes.removed

        Nothing ->
            []



-- All the available cameras including selected and unselected cameras.


registered : Model -> List Camera.Model
registered (Model _ registry) =
    registry.allRegistered


selected : Model -> Stitch.Associations
selected (Model _ registry) =
    registry.selection



-- Names of the selected cameras.


selectedNames : Model -> List String
selectedNames (Model _ registry) =
    Stitch.names registry.selection



-- The current selection session. A number which increases monotonically by 1
-- each time the selection session is incremented. The first ever session is
-- numbered 0.


selectionSession : Model -> Int
selectionSession (Model _ registry) =
    registry.selectionSession


switchCameras : Model -> Stitch.SwitchCamerasMsg -> Model
switchCameras (Model mode registry) (Stitch.SwitchCamerasMsg cam1 cam2) =
    Model
        mode
        { registry | selection = Stitch.switch registry.selection cam1 cam2 }



-- Check if a time value, to be set as the time at which the latest update
-- occurred, is needed by the model.


timeNeedsUpdating : Model -> Bool
timeNeedsUpdating (Model _ { recentChanges }) =
    case recentChanges of
        Just changes ->
            changes.needToUpdateTime

        Nothing ->
            False


toggleExclusive : String -> Model -> Model
toggleExclusive name (Model mode registry) =
    let
        names =
            Stitch.names registry.selection

        toToggle =
            My.Extra.appendIf
                (List.Extra.notMember name names)
                names
                [ name ]
    in
    Model
        mode
        { registry
            | selection =
                List.foldl
                    (\cam sel -> Stitch.toggle sel cam)
                    registry.selection
                    toToggle
        }


toggleIndependent : String -> Model -> Model
toggleIndependent name (Model mode registry) =
    Model
        mode
        { registry
            | selection = Stitch.toggle registry.selection name
        }



-- Toggle the selected state of a camera.


toggle : ToggleMsg -> Model -> Model
toggle (ToggleMsg name) model =
    case recentDifference model of
        Just _ ->
            -- Toggle buttons are blocked while recent changes to the registered
            -- cameras exist.
            model

        Nothing ->
            let
                (Model mode _) =
                    model
            in
            case mode of
                Single ->
                    toggleExclusive name model

                Multiple ->
                    toggleIndependent name model


toggleSelectionMode : Model -> Model
toggleSelectionMode (Model mode registry) =
    case mode of
        Single ->
            Model Multiple registry

        Multiple ->
            Model Single
                { registry
                    | selection =
                        case Stitch.names registry.selection of
                            _ :: minors ->
                                List.foldl
                                    (\cam sel -> Stitch.toggle sel cam)
                                    registry.selection
                                    minors

                            _ ->
                                registry.selection
                }


toggleStitchingSize : Model -> Model
toggleStitchingSize (Model mode registry) =
    Model
        mode
        { registry | selection = Stitch.togglePartial registry.selection }



-- If the time now has, since the latest update, progressed past the block
-- interval then unblock the camera toggle buttons.


tryToUnblock : Model -> Time.Posix -> Model
tryToUnblock (Model mode registry) now =
    case registry.recentChanges of
        Just changes ->
            if
                Time.posixToMillis now
                    > Time.posixToMillis changes.latestUpdate
                    + blockedInterval
            then
                Model mode { registry | recentChanges = Nothing }

            else
                Model mode registry

        Nothing ->
            Model mode registry



-- Clear any memory of recent changes from the model hence unblocking the camera
-- selection toggle buttons.


unblock : Model -> Model
unblock (Model mode registry) =
    Model mode { registry | recentChanges = Nothing }



-- Update the cameras from a registered and associated cameras.


update : RegisteredAssociated -> Model -> Model
update regAssoc (Model mode registry) =
    let
        trueRegistered =
            Camera.namesOf regAssoc.registered

        filterTrueRegistered =
            List.filter (\cam -> not <| List.member cam trueRegistered)

        added =
            List.filter
                (\cam ->
                    not <|
                        List.member
                            cam
                            (Camera.namesOf registry.allRegistered)
                )
                trueRegistered

        autoDeselected =
            filterTrueRegistered <| Stitch.names registry.selection

        autoDisassociated =
            filterTrueRegistered <| Stitch.names registry.associated

        removed =
            filterTrueRegistered <| Camera.namesOf registry.allRegistered
    in
    Model
        (case mode of
            Single ->
                if (List.length <| Stitch.names regAssoc.associated) > 1 then
                    Multiple

                else
                    Single

            Multiple ->
                Multiple
        )
        { allRegistered = regAssoc.registered
        , associated = regAssoc.associated
        , selection =
            List.foldl
                (\camera associations ->
                    Stitch.deselect associations camera
                )
                registry.selection
                removed
        , selectionSession = registry.selectionSession
        , recentChanges =
            case registry.recentChanges of
                Just changes ->
                    Just
                        { added =
                            added
                                ++ changes.added
                                |> List.filter
                                    (\cam -> not <| List.member cam removed)
                        , autoDeselected =
                            Set.union changes.autoDeselected <|
                                Set.fromList autoDeselected
                        , autoDisassociated =
                            Set.diff
                                (Set.union changes.autoDisassociated <|
                                    Set.fromList autoDisassociated
                                )
                                (Set.fromList <|
                                    Stitch.names regAssoc.associated
                                )
                        , removed =
                            removed
                                ++ changes.removed
                                |> List.filter
                                    (\cam -> not <| List.member cam added)
                        , latestUpdate = changes.latestUpdate
                        , needToUpdateTime = True
                        }

                Nothing ->
                    if List.length added > 0 || List.length removed > 0 then
                        Just
                            { added = added
                            , autoDeselected = Set.fromList autoDeselected
                            , autoDisassociated = Set.fromList autoDisassociated
                            , removed = removed
                            , latestUpdate = timeZeroValue
                            , needToUpdateTime = True
                            }

                    else
                        Nothing
        }



-- Set the latest update time in the model.


updateTime : Model -> Time.Posix -> Model
updateTime (Model mode registry) time =
    case registry.recentChanges of
        Just changes ->
            Model
                mode
                { registry
                    | recentChanges =
                        Just
                            { changes
                                | needToUpdateTime = False
                                , latestUpdate = time
                            }
                }

        Nothing ->
            Model mode registry
