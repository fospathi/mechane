module Static.About exposing
    ( Author
    , Dependency
    , License
    , Mechane
    , Model
    , Msg
    , World
    , create
    , init
    , mechane
    , spdxId
    , world
    )

import Http
import Json.Decode as D
import Json.Decode.Pipeline as DP
import My.Extra
import My.Http
import My.List
import Result.Extra
import Url


aboutDecoder : D.Decoder About
aboutDecoder =
    D.succeed About
        |> DP.required "mechane" aboutMechaneDecoder
        |> DP.required "world" aboutWorldDecoder


aboutMechaneDecoder : D.Decoder Mechane
aboutMechaneDecoder =
    D.succeed Mechane
        |> DP.required "author" authorDecoder
        |> DP.required "blurb" (D.list D.string)
        |> DP.required "link" D.string
        |> DP.required "name" D.string
        |> DP.required "license" licenseDecoder
        |> DP.required "alternativeLicense" licenseDecoder
        |> DP.required "binaries" depsDecoder
        |> DP.required "devBinaries" depsDecoder
        |> DP.required "fonts" depsDecoder
        |> DP.required "elmImports" (D.list D.string)
        |> DP.required "goImports" (D.list D.string)
        |> DP.required "tsImports" (D.list D.string)
        |> DP.required "tsDevImports" (D.list D.string)
        |> D.andThen validateMechane


aboutWorldDecoder : D.Decoder World
aboutWorldDecoder =
    D.succeed World
        |> DP.required "author" authorDecoder
        |> DP.required "blurb" (D.list D.string)
        |> DP.required "link" D.string
        |> DP.required "name" D.string
        |> DP.required "license" licenseDecoder
        |> DP.required "alternativeLicense" licenseDecoder
        |> DP.optional "binaries" depsDecoder []
        |> DP.optional "devBinaries" depsDecoder []
        |> D.andThen validateWorld


authorDecoder : D.Decoder (List Author)
authorDecoder =
    D.list
        (D.map2
            Author
            (D.field "name" D.string)
            (D.field "url" D.string)
        )


depsDecoder : D.Decoder (List Dependency)
depsDecoder =
    D.list
        (D.map4
            Dependency
            (D.field "license" licenseDecoder)
            (D.field "licensor" (D.list D.string))
            (D.field "name" D.string)
            (D.field "url" D.string)
        )


licenseDecoder : D.Decoder License
licenseDecoder =
    D.map5
        License
        (D.field "badge" D.string)
        (D.field "id" D.string)
        (D.field "name" D.string)
        (D.field "text" D.string)
        (D.field "url" D.string)


type Model
    = Model About


type alias Author =
    { name : String, url : String }


type alias License =
    { badge : String, id : String, name : String, text : String, url : String }


type alias Dependency =
    { license : License
    , licensor : List String
    , name : String
    , url : String
    }


type alias Mechane =
    { author : List Author
    , blurb : List String
    , link : String
    , name : String
    , license : License
    , alternativeLicense : License
    , binaries : List Dependency
    , devBinaries : List Dependency
    , fonts : List Dependency
    , elmImports : List String
    , goImports : List String
    , tsImports : List String
    , tsDevImports : List String
    }


type alias World =
    { author : List Author
    , blurb : List String
    , link : String
    , name : String
    , license : License
    , alternativeLicense : License
    , binaries : List Dependency
    , devBinaries : List Dependency
    }


type alias About =
    { mechane : Mechane
    , world : World
    }


type Msg
    = Msg (Result Http.Error Model)


elmPackagePrefix : String
elmPackagePrefix =
    "https://package.elm-lang.org/packages"


goPackagePrefix : String
goPackagePrefix =
    "https://pkg.go.dev"



-- The path on a Mechane server where the 'about' info can be retrieved.


path : String
path =
    "/about"


create : Cmd Msg
create =
    Http.get
        { url = path
        , expect = Http.expectJson Msg <| D.map Model aboutDecoder
        }


depURLs : List Dependency -> List String
depURLs deps =
    List.concat <| List.map (\dep -> [ dep.url, dep.license.url ]) deps


init : Msg -> Result String Model
init (Msg msg) =
    case msg of
        Ok model ->
            Ok model

        Err err ->
            Err <| My.Http.errorToString err


mechane : Model -> Mechane
mechane (Model about) =
    about.mechane


spdxId : License -> License -> String
spdxId license1 license2 =
    (case ( license1.id, license2.id ) of
        ( "", "" ) ->
            "Identifier unknown"

        ( id, "" ) ->
            id

        ( id1, id2 ) ->
            id1 ++ " OR " ++ id2
    )
        |> String.append "SPDX-License-Identifier: "


validateAuthor : Author -> Result String ()
validateAuthor author =
    if List.any String.isEmpty [ author.name, author.url ] then
        Err "empty field in author"

    else
        case validateURL author.url of
            Ok _ ->
                Ok ()

            Err err ->
                Err err


validateDependency : Dependency -> Result String ()
validateDependency dep =
    case
        [ validateLicense dep.license
        , My.Extra.ternary
            (List.any (\s -> not <| String.isEmpty s) dep.licensor)
            (Ok ())
            (Err "empty dependency licensor field")
        , My.Extra.ternary
            (not <| String.isEmpty dep.name)
            (Ok ())
            (Err "empty dependency name field ")
        , Result.map (\_ -> ()) <| validateURL dep.url
        ]
            |> Result.Extra.combine
    of
        Ok _ ->
            Ok ()

        Err error ->
            Err error


validateElmURL : String -> Result String Url.Url
validateElmURL url =
    if String.isEmpty url then
        Err "cannot parse empty Elm package URL"

    else
        case Url.fromString url of
            Just parsedURL ->
                if String.startsWith elmPackagePrefix url then
                    -- Package paths should have the format /packages/author/name
                    if List.length (String.split "/" parsedURL.path) == 4 then
                        Ok parsedURL

                    else
                        Err <| "unexpected Elm package URL path: " ++ url

                else
                    Err <| "unexpected Elm package URL prefix: " ++ url

            Nothing ->
                Err <| "cannot parse Elm package URL: " ++ url


validateGoURL : String -> Result String Url.Url
validateGoURL url =
    if String.isEmpty url then
        Err "cannot parse empty Go package URL"

    else
        let
            -- There are two types of supported Go package URLs:
            --   1.) goPackagePrefix/repository/author/packageName
            --   2.) goPackagePrefix/author/packageName
            minPathSegments =
                3
        in
        case Url.fromString url of
            Just parsedURL ->
                if String.startsWith goPackagePrefix url then
                    if
                        List.length (String.split "/" parsedURL.path)
                            >= minPathSegments
                    then
                        Ok parsedURL

                    else
                        Err <| "unexpected Go package URL path: " ++ url

                else
                    Err <| "unexpected Go package URL prefix: " ++ url

            Nothing ->
                Err <| "cannot parse Go package URL: " ++ url


validateLicense : License -> Result String ()
validateLicense license =
    -- The badge is optional.
    if
        List.any String.isEmpty
            [ license.id
            , license.name
            , license.text
            , license.url
            ]
    then
        Err "empty field in license"

    else
        case validateURL license.url of
            Ok _ ->
                Ok ()

            Err err ->
                Err err



-- Validate a list, combining multiple error messages into one multi line error
-- string in the case of one or more errors.


validateList : (a -> Result String b) -> List a -> Result String ()
validateList test list =
    My.List.validate test list
        |> Result.map (\_ -> ())
        |> Result.mapError (String.join "\n")


validateMechane : Mechane -> D.Decoder Mechane
validateMechane about =
    let
        authorURLs =
            List.map (\{ url } -> url) about.author

        depsURLs =
            List.concat
                (List.map depURLs
                    [ about.binaries
                    , about.devBinaries
                    , about.fonts
                    ]
                )

        importURLs =
            about.elmImports
                ++ about.goImports
                ++ about.tsImports
                ++ about.tsDevImports
    in
    case
        My.List.validate validateURL <|
            List.concat
                [ [ about.link ]
                , authorURLs
                , [ about.license.url ]
                , depsURLs
                , importURLs
                ]
    of
        Ok _ ->
            case
                Result.Extra.combine
                    [ My.List.validate validateElmURL about.elmImports
                    , My.List.validate validateGoURL about.goImports
                    ]
            of
                Ok _ ->
                    D.succeed <| about

                Err errors ->
                    D.fail <| String.join "\n" errors

        Err errors ->
            D.fail <| String.join "\n" errors


validateURL : String -> Result String Url.Url
validateURL url =
    if String.isEmpty url then
        Err "cannot parse empty URL"

    else
        case Url.fromString url of
            Just parsedURL ->
                if parsedURL.query /= Nothing then
                    Err <| "unexpected query: " ++ url

                else
                    Ok parsedURL

            Nothing ->
                Err <| "cannot parse URL: " ++ url


validateWorld : World -> D.Decoder World
validateWorld about =
    case
        My.Extra.appendWhen
            [ My.Extra.ternary
                (not <| List.isEmpty about.author)
                (Ok ())
                (Err "empty world author field")
            , My.Extra.ternary
                (List.any (\s -> not <| String.isEmpty s) about.blurb)
                (Ok ())
                (Err "empty world blurb field")
            , My.Extra.ternary
                (not <| String.isEmpty about.link)
                (Ok ())
                (Err "empty world link field ")
            , My.Extra.ternary
                (not <| String.isEmpty about.name)
                (Ok ())
                (Err "empty world name field ")
            , validateList validateAuthor about.author
            , validateLicense about.license
            ]
            [ ( List.length about.binaries > 0
              , [ validateList validateDependency about.binaries ]
              )
            , ( List.length about.devBinaries > 0
              , [ validateList validateDependency about.devBinaries ]
              )
            ]
            |> Result.Extra.combine
    of
        Ok _ ->
            case validateURL about.link of
                Ok _ ->
                    D.succeed about

                Err err ->
                    D.fail err

        Err error ->
            D.fail error


world : Model -> World
world (Model about) =
    about.world
