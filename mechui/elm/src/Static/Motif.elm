module Static.Motif exposing
    ( Model
    , Motif
    , Msg
    , create
    , init
    , motif
    )

import Css as C
import Http
import Json.Decode as D
import Json.Decode.Pipeline as DP
import My.Http


colorDecoder : D.Decoder Colour
colorDecoder =
    D.succeed Colour
        |> DP.required "r" D.int
        |> DP.required "g" D.int
        |> DP.required "b" D.int
        |> DP.required "a" D.float


cssColorDecoder : D.Decoder C.Color
cssColorDecoder =
    D.map colourToCSSColor colorDecoder


floatStringDecoder : D.Decoder Float
floatStringDecoder =
    D.andThen
        (\s ->
            case String.toFloat s of
                Just f ->
                    D.succeed f

                Nothing ->
                    D.fail "could not convert string to float"
        )
        D.string


motifDecoder : D.Decoder Model
motifDecoder =
    D.succeed Motif
        |> DP.required "motifName" D.string
        --
        |> DP.required "accent" cssColorDecoder
        |> DP.required "boxShadow" D.string
        |> DP.required "buttonBackground" cssColorDecoder
        |> DP.required "buttonOpaqueBackground" cssColorDecoder
        |> DP.required "error" cssColorDecoder
        |> DP.required "primaryGlow" cssColorDecoder
        |> DP.required "primaryLight" cssColorDecoder
        |> DP.required "primary" cssColorDecoder
        |> DP.required "ready" cssColorDecoder
        |> DP.required "textNormal" cssColorDecoder
        |> DP.required "textPassive" cssColorDecoder
        |> DP.required "warning" cssColorDecoder
        --
        |> DP.required "textBigPassive" cssColorDecoder
        --
        |> DP.required "buttonBorder" floatStringDecoder
        |> DP.required "buttonBorderRadius" floatStringDecoder
        |> DP.required "buttonIconFontSize" floatStringDecoder
        |> DP.required "buttonPadding" floatStringDecoder
        --
        |> DP.required "buttonHeight" D.float
        |> D.map Model


type alias Colour =
    { r : Int
    , g : Int
    , b : Int
    , a : Float
    }


type Model
    = Model Motif


type alias Motif =
    { motifName : String

    --
    , accent : C.Color
    , boxShadow : String
    , buttonBackground : C.Color
    , buttonOpaqueBackground : C.Color
    , error : C.Color
    , primaryGlow : C.Color
    , primaryLight : C.Color
    , primary : C.Color
    , ready : C.Color
    , textNormal : C.Color
    , textPassive : C.Color
    , warning : C.Color

    --
    , textBigPassive : C.Color

    --
    , buttonBorder : Float
    , buttonBorderRadius : Float
    , buttonIconFontSize : Float
    , buttonPadding : Float

    --
    , buttonHeight : Float
    }


type Msg
    = Msg (Result Http.Error Model)


path : String
path =
    "/motif/elm"


colourToCSSColor : Colour -> C.Color
colourToCSSColor col =
    C.rgba col.r col.g col.b col.a


create : Cmd Msg
create =
    Http.get
        { url = path
        , expect = Http.expectJson Msg motifDecoder
        }


init : Msg -> Result String Model
init (Msg msg) =
    case msg of
        Ok model ->
            Ok model

        Err err ->
            Err <| My.Http.errorToString err


motif : Model -> Motif
motif (Model theme) =
    theme
