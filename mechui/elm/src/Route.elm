module Route exposing (parseURL)

import Dict
import Json.Decode as D
import Shutterbug
import String.Extra
import Url
import Url.Parser exposing ((</>))
import Url.Parser.Query as Query


type Route
    = Home (Maybe Shutterbug.Source)


parseQuery : Query.Parser (Maybe Shutterbug.Source)
parseQuery =
    Query.enum Shutterbug.queryKey <|
        Dict.fromList
            [ ( Shutterbug.queryValues.extant, Shutterbug.UseExisting )
            , ( Shutterbug.queryValues.new, Shutterbug.MakeNew )
            ]


parseRoute : Url.Url -> Maybe Route
parseRoute url =
    Url.Parser.parse
        (Url.Parser.oneOf
            [ Url.Parser.map
                Home
                (Url.Parser.top </> Url.Parser.query parseQuery)
            ]
        )
        url


parseURL : D.Value -> Result String Shutterbug.Meta
parseURL maybeURL =
    case D.decodeValue D.string maybeURL of
        Ok urlString ->
            case Url.fromString (String.Extra.unquote urlString) of
                Just url ->
                    case parseRoute url of
                        Just (Home Nothing) ->
                            case url.query of
                                Just _ ->
                                    Err "the URL query is not as expected"

                                Nothing ->
                                    Ok
                                        { source = Shutterbug.defaultSource
                                        , manualEntryURL = Shutterbug.manualEntryURL url
                                        }

                        Just (Home (Just source)) ->
                            Ok
                                { source = source
                                , manualEntryURL = Shutterbug.manualEntryURL url
                                }

                        Nothing ->
                            Err "URL processing error"

                Nothing ->
                    Err "the URL is invalid"

        Err error ->
            Err <| D.errorToString error
