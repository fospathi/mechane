module PixScreen exposing
    ( Meta
    , Model
    , areaDescription
    , attributes
    , backgroundColour
    , border
    , borderButton
    , dimensions
    , disableTextSelect
    , fit
    , fitScreenButton
    , full
    , fullscreenButton
    , init
    , locked
    , metaDecoder
    , pointerLockButton
    , ratio
    , selectable
    , tag
    , textSelectButton
    , toggleBorder
    , toggleFit
    , toggleTextSelect
    , updateMeta
    )

import Html.Styled as H exposing (Html)
import Html.Styled.Attributes as HA
import Json.Decode as D
import LifeCycle
import My.Extra as ME
import My.Html
import Theme exposing (Theme)
import View


metaDecoder : D.Decoder Meta
metaDecoder =
    D.map4 Meta
        (D.field "w" D.int)
        (D.field "h" D.int)
        (D.field "fullscreen" D.bool)
        (D.field "pointerLock" D.bool)



-- A Model holding meta data about the 'shutterbug-pixels' custom element,
-- referred to here as the PixScreen, used to display the view from the
-- shutterbug's associated cameras.
--
-- There is only one PixScreen; when multiple cameras are being viewed
-- simultaneously they all share the same PixScreen.


type Model
    = Model
        { w : Int
        , h : Int
        , borderScreen : Bool
        , fitScreen : Bool
        , fullscreen : Bool
        , pointerLock : Bool
        , textSelect : Bool
        }



-- The unscaled size of the shutterbug's raw pixel data in units of pixels, the
-- tab's fullscreen status, and the tab's pointer lock status.


type alias Meta =
    { w : Int
    , h : Int
    , fullscreen : Bool
    , pointerLock : Bool
    }



-- Custom attributes on the ShutterbugPixels custom element.


attributePrefix : String
attributePrefix =
    "shutterbug-pixels-"


attributes :
    { width : String
    , height : String
    , border : String
    , colour : String
    , kaput : String
    , selectable : String
    }
attributes =
    { border = attributePrefix ++ "b"
    , colour = attributePrefix ++ "c"
    , height = attributePrefix ++ "h"
    , kaput = attributePrefix ++ "k"
    , selectable = attributePrefix ++ "s"
    , width = attributePrefix ++ "w"
    }



-- If the pixels' display area contains transparent pixels then this is the
-- colour that shows through.


backgroundColour : String
backgroundColour =
    "white"



-- Width to height ratio of the screen.


ratio : Float
ratio =
    1.6


tag : String
tag =
    "shutterbug-pixels"


init : Meta -> Model
init meta =
    Model
        { w = meta.w
        , h = meta.h
        , borderScreen = False
        , fitScreen = True
        , fullscreen = meta.fullscreen
        , pointerLock = meta.pointerLock
        , textSelect = False
        }



-- Border status


border : Model -> Bool
border (Model { borderScreen }) =
    borderScreen



-- Scaling status.


fit : Model -> Bool
fit (Model { fitScreen }) =
    fitScreen



-- Fullscreen status.


full : Model -> Bool
full (Model { fullscreen }) =
    fullscreen



-- Pointer lock status.


locked : Model -> Bool
locked (Model { pointerLock }) =
    pointerLock



-- CSS user-select property status.


selectable : Model -> Bool
selectable (Model { textSelect }) =
    textSelect


areaDescription : Model -> String
areaDescription (Model { w, h }) =
    String.fromInt w
        ++ " × "
        ++ String.fromInt h
        ++ " pixels"


dimensions : Model -> { w : Int, h : Int }
dimensions (Model { w, h }) =
    { w = w, h = h }


disableTextSelect : Model -> Model
disableTextSelect (Model data) =
    Model { data | textSelect = False }


toggleBorder : Model -> Model
toggleBorder (Model data) =
    Model { data | borderScreen = not data.borderScreen }


toggleFit : Model -> Model
toggleFit (Model data) =
    Model { data | fitScreen = not data.fitScreen }


toggleTextSelect : Model -> Model
toggleTextSelect (Model data) =
    Model { data | textSelect = not data.textSelect }


updateMeta : Meta -> Model -> Model
updateMeta meta (Model data) =
    Model
        { data
            | w = meta.w
            , h = meta.h
            , fullscreen = meta.fullscreen
            , pointerLock = meta.pointerLock
        }


borderButton : Theme -> a -> Model -> Html a
borderButton theme msg screen =
    if border screen then
        H.button
            [ My.Html.onClickPreventDefault msg
            , HA.title "Disable the border."
            , HA.css <| .standard (Theme.buttonWithMargin theme)
            ]
            [ View.buttonIcon theme "border_outer" ]

    else
        H.button
            [ My.Html.onClickPreventDefault msg
            , HA.title "Enable the border."
            , HA.css <| .standard (Theme.buttonWithMargin theme)
            ]
            [ View.buttonIcon theme "border_clear" ]


fitScreenButton : Theme -> a -> Model -> Html a
fitScreenButton theme msg screen =
    if fit screen then
        H.button
            [ My.Html.onClickPreventDefault msg
            , HA.title
                ("Disable fit to window height.\n"
                    ++ "(The natural size is "
                    ++ areaDescription screen
                    ++ ".)"
                )
            , HA.css <| .standard (Theme.buttonWithMargin theme)
            ]
            [ View.buttonIcon theme "fit_screen" ]

    else
        H.button
            [ My.Html.onClickPreventDefault msg
            , HA.title "Enable fit to window height."
            , HA.css <| .standard (Theme.buttonWithMargin theme)
            ]
            [ View.buttonOffIcon theme "fit_screen" ]


fullscreenButton : Theme -> a -> Model -> Html a
fullscreenButton theme msg screen =
    if full screen then
        H.button
            [ My.Html.onClickPreventDefault msg
            , HA.title "Disable fullscreen."
            , HA.css <| .standard (Theme.buttonWithMargin theme)
            ]
            [ View.buttonIcon theme "fullscreen" ]

    else
        H.button
            [ My.Html.onClickPreventDefault msg
            , HA.title "Enable fullscreen."
            , HA.css
                (Theme.bottomMargin theme
                    :: .standard (Theme.button theme)
                )
            ]
            [ View.buttonIcon theme "fullscreen_exit" ]


pointerLockButton : Theme -> a -> a -> Model -> Maybe LifeCycle.WSCloseEvent -> Html a
pointerLockButton theme msg inactionMsg screen maybeNotification =
    case maybeNotification of
        Just notification ->
            let
                kaputButtonStyles =
                    Theme.bottomMargin theme :: Theme.kaputButtonStyles theme
            in
            H.div
                [ HA.css <| kaputButtonStyles
                , HA.title <| LifeCycle.explainWSCloseEvent notification
                ]
                [ View.buttonIcon theme "videogame_asset" ]

        Nothing ->
            if locked screen then
                My.Html.safeClickButton
                    msg
                    inactionMsg
                    [ HA.title "Disable pointer lock."
                    , HA.css <| .standard (Theme.buttonWithMargin theme)
                    ]
                    [ View.buttonIcon theme "videogame_asset" ]

            else
                My.Html.safeClickButton
                    msg
                    inactionMsg
                    [ HA.title "Enable pointer lock."
                    , HA.css <| .standard (Theme.buttonWithMargin theme)
                    ]
                    [ View.buttonIcon theme "videogame_asset_off" ]


textSelectButton : Theme -> a -> a -> Bool -> Model -> Html a
textSelectButton theme msg inactionMsg kaput screen =
    if selectable screen then
        My.Html.safeClickButton
            msg
            inactionMsg
            [ HA.title <|
                "Disable text selection mode."
                    ++ ME.ternary (not kaput)
                        "\n Re-enable mouse camera panning."
                        ""
            , HA.css <| .standard (Theme.buttonWithMargin theme)
            ]
            [ View.buttonIcon theme "select_all" ]

    else
        My.Html.safeClickButton
            msg
            inactionMsg
            [ HA.title <|
                "Enable text selection mode."
                    ++ ME.ternary (not kaput)
                        "\n⚠ Disable mouse camera panning."
                        ""
            , HA.css <| .standard (Theme.buttonWithMargin theme)
            ]
            [ View.buttonIcon theme "deselect" ]
