module Camera exposing (Model, init, nameOf, namesOf, sort)


type Model
    = Model String


init : String -> Model
init name =
    Model name


nameOf : Model -> String
nameOf (Model name) =
    name


namesOf : List Model -> List String
namesOf cams =
    List.map (\cam -> nameOf cam) cams


compareNames : Model -> Model -> Order
compareNames a b =
    compare (nameOf a) (nameOf b)



-- Sort a list of cameras by camera name.


sort : List Model -> List Model
sort cams =
    List.sortWith compareNames cams
