module View exposing (..)

import Css as C
import Css.Animations as CA
import Css.Global
import Css.Transitions
import Html.Styled as H exposing (Html)
import Html.Styled.Attributes as HA
import My.Extra as ME
import Svg.Styled as S
import Svg.Styled.Attributes as SA
import Theme exposing (Theme)


defaultIconStyle : String
defaultIconStyle =
    "material-icons-round"


startingTasks : List String
startingTasks =
    [ "Awaiting static data"
    , "Awaiting shutterbug"
    , "Opening WebSockets"
    , "Awaiting dynamic data"
    , "Selecting cameras"
    , "Awaiting shutterbug pixels"
    ]


inactiveSubmitButton : Theme -> Html a
inactiveSubmitButton theme =
    H.span
        [ HA.css <|
            [ C.display C.inlineBlock
            , C.margin <| C.em 0
            ]
                ++ .inactive (Theme.button theme)
        ]
        [ buttonIcon theme "double_arrow" ]


errorsList : Theme -> List (List String) -> Html a
errorsList theme errors =
    H.ul
        [ HA.css
            ([ C.marginTop (C.em (2 * theme.buttonPadding))
             , C.overflow C.auto
             , C.textAlign C.left
             ]
                ++ Theme.errorContainerStyles theme
            )
        ]
        (List.map
            (\err ->
                H.li []
                    [ List.map (\line -> H.span [] [ H.text line ]) err
                        |> List.intersperse (H.br [] [])
                        |> H.p []
                    ]
            )
            errors
        )


globalStyleNode : Theme -> Html a
globalStyleNode theme =
    let
        -- Resort to a global style node to completely remove the body margin
        -- and reliably occupy the full viewport height.
        fullHeight =
            [ Css.Global.body
                [ C.margin4 (C.px 0) (C.px 0) (C.px 0) (C.px 0)
                ]
            , Css.Global.each [ Css.Global.body, Css.Global.html ]
                [ C.height (C.pct 100)
                ]
            ]

        -- Custom properties for the buttons' radial gradient transition.
        buttonGradient =
            [ Css.Global.selector
                ("@property " ++ Theme.buttonBackgroundStop1Percentage)
                [ C.property "syntax" "'<percentage>'"
                , C.property "inherits" "false"
                , C.property "initial-value" "100%"
                ]
            , Css.Global.selector
                ("@property " ++ Theme.buttonBackgroundStop2Percentage)
                [ C.property "syntax" "'<percentage>'"
                , C.property "inherits" "false"
                , C.property "initial-value" "100%"
                ]
            , Css.Global.selector
                ("@property " ++ Theme.buttonBackgroundStop1Color)
                [ C.property "syntax" "'<color>'"
                , C.property "inherits" "false"
                , C.property
                    "initial-value"
                    (Theme.colToString theme.buttonBackground)
                ]
            ]
    in
    Css.Global.global <| fullHeight ++ buttonGradient


mainColumns : Html a -> Html a -> Html a -> Html a
mainColumns col1 col2 col3 =
    H.div
        [ HA.css
            [ C.height <| C.vh 100
            , C.displayFlex
            , C.alignItems C.center
            , C.position C.relative
            ]
        ]
        [ H.div
            [ HA.css
                [ C.flex <| C.int 1
                ]
            ]
            [ col1 ]
        , H.div
            [ HA.css
                [ C.flex <| C.int 0
                ]
            ]
            [ col2 ]
        , H.div
            [ HA.css
                [ C.alignSelf C.flexEnd
                , C.flex <| C.int 1
                , C.height <| C.pct 100

                -- Let the shutterbug actions popup be above the screen when it
                -- overlaps it due to cramped width.
                , C.zIndex <| C.int 1
                ]
            ]
            [ col3 ]
        ]


expanderButtonIcon : Theme -> String -> Bool -> Bool -> Html a
expanderButtonIcon theme name popUp any =
    let
        -- The rotation angle is in units of degrees and is relative to the
        -- positive X-axis.
        rotationAngle =
            C.deg <| ME.ternary popUp 135 -45
    in
    H.span []
        [ buttonIcon theme name
        , H.span
            [ HA.class "material-icons"
            , HA.class Theme.expanderIconClass
            , HA.css
                [ C.fontSize <| C.em theme.buttonIconFontSize
                , C.color theme.accent
                , C.opacity <| ME.ternary popUp (C.num 1) (C.num 0)
                , C.property "filter" "drop-shadow(0px 0px 3px white)"
                , C.position C.absolute
                , C.transforms
                    [ C.translate2
                        (C.em <| -(theme.buttonIconFontSize - 0.2))
                        (C.em <| -(theme.buttonIconFontSize / 2 - 0.2))
                    , C.rotate rotationAngle
                    ]
                , Css.Transitions.transition
                    [ Css.Transitions.transform Theme.popUpDuration
                    , Css.Transitions.opacity
                        (ME.ternary any (1.5 * Theme.popUpDuration) 400)
                    ]
                , C.property "user-select" "none"
                , C.zIndex <| C.int 1
                ]
            ]
            [ H.text "expand_circle_down" ]
        ]


buttonIcon : Theme -> String -> Html a
buttonIcon theme name =
    H.span
        [ HA.class defaultIconStyle
        , HA.css
            [ C.fontSize <| C.em theme.buttonIconFontSize
            , C.property "user-select" "none"
            , C.verticalAlign C.middle
            ]
        ]
        [ H.text name ]



-- For a Material Design icon that doesn't have its own off version then
-- approximate the effect with an SVG overlay.
--
-- The containing button should use relative positioning.


buttonOffIcon : Theme -> String -> Html a
buttonOffIcon theme name =
    H.span []
        [ H.span
            [ HA.class defaultIconStyle
            , HA.css
                [ C.color theme.primary
                , C.fontSize <| C.em theme.buttonIconFontSize
                , C.property "user-select" "none"
                , C.verticalAlign C.middle
                , C.position C.absolute
                ]
            ]
            [ S.svg
                [ SA.fill <| Theme.colToString theme.primary
                , SA.viewBox "0 0 24 24"
                , SA.width "1em"
                ]
                [ S.path
                    [ SA.d "M2.81,2.81 L1.39,4.22 L19.77,22.6 L21.19,21.19z"
                    ]
                    []
                , S.path
                    [ SA.d "M2.81,2.81 L4.23,1.4 L22.6,19.77 L21.19,21.19z"
                    , SA.fill <| Theme.colToString theme.buttonOpaqueBackground
                    ]
                    []
                ]
            ]
        , buttonIcon theme name
        ]



-- A list where all list items are prefixed with a tick icon except for the last
-- item.


tickedList : Theme -> List String -> Html a
tickedList theme items =
    let
        height =
            2.5

        unTickedItem : String -> Html a
        unTickedItem item =
            H.li
                [ HA.css
                    [ C.height <| C.em height
                    , C.lineHeight <| C.em height
                    , C.animationName <|
                        CA.keyframes
                            [ ( 0
                              , [ CA.property "color" "black" ]
                              )
                            , ( 50
                              , [ CA.property "color" "gray" ]
                              )
                            , ( 100
                              , [ CA.property "color" "black" ]
                              )
                            ]
                    , C.animationDuration <| C.ms 1000
                    , C.animationIterationCount C.infinite
                    ]
                ]
                [ buttonIcon theme "pending"
                , H.span
                    [ HA.css
                        [ C.paddingLeft <| C.em theme.buttonPadding ]
                    ]
                    [ H.text item ]
                ]

        tickedItem : String -> Html a
        tickedItem item =
            H.li
                [ HA.css
                    [ C.color theme.textPassive
                    , C.height <| C.em height
                    , C.lineHeight <| C.em height
                    ]
                ]
                [ buttonIcon theme "check_circle"
                , H.span
                    [ HA.css
                        [ C.paddingLeft <| C.em theme.buttonPadding ]
                    ]
                    [ H.text item ]
                ]
    in
    (case items of
        [] ->
            []

        x :: [] ->
            [ unTickedItem x ]

        _ ->
            let
                ticked =
                    List.take (List.length items - 1) items

                unTicked =
                    List.take 1 (List.reverse items)
            in
            List.map tickedItem ticked ++ List.map unTickedItem unTicked
    )
        |> H.ol
            [ HA.css
                [ C.listStyleType C.none
                , C.minWidth <| C.em 20
                , C.marginLeft <| C.em theme.buttonPadding
                , C.marginRight <| C.em theme.buttonPadding
                ]
            ]


startingGrid : Theme -> String -> Html a -> Html a -> Html a -> Html a
startingGrid theme prefix leftTrack middleCell rightTrack =
    H.div
        [ HA.css
            [ C.property "display" "grid"
            , C.property
                "grid-template-columns"
                "20em minmax(50em, auto) minmax(10em, 20em)"
            , C.property "grid-template-rows" "4em auto"
            , C.height <| C.vh 100
            , C.minHeight <| C.em 40
            ]
        ]
        [ H.div
            []
            [ H.h1
                [ HA.css
                    [ C.property "grid-column" "2"
                    , C.property "grid-row" "1"
                    , C.marginTop <| C.em 0
                    , C.paddingTop <| C.em theme.buttonPadding
                    , C.textAlign C.center
                    ]
                ]
                [ H.span
                    [ HA.css [ C.color theme.textBigPassive ] ]
                    [ H.text (String.trim prefix ++ " ") ]
                , H.span
                    [ HA.css [ C.color theme.primaryLight ] ]
                    [ H.text "mechane" ]
                ]
            ]
        , H.div
            [ HA.css
                [ C.property "grid-column" "1"
                , C.property "grid-row" "1 / 3"
                ]
            ]
            [ leftTrack ]
        , H.div
            [ HA.css
                [ C.property "grid-column" "2"
                , C.property "grid-row" "2"
                ]
            ]
            [ middleCell ]
        , H.div
            [ HA.css
                [ C.property "grid-column" "3"
                , C.property "grid-row" "1 / 3"
                , C.alignItems C.flexEnd
                , C.displayFlex
                , C.justifyContent C.flexEnd
                , C.paddingRight <| C.em theme.buttonPadding
                , C.position C.relative
                , C.overflow C.hidden
                ]
            ]
            [ rightTrack ]
        ]
