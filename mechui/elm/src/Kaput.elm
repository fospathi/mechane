module Kaput exposing
    ( Model(..)
    , theme
    , viewNotification
    )

import Html.Styled as H exposing (Html)
import LifeCycle
import List
import PixScreen
import PopUp
import Theme exposing (Theme)



-- A model with information about when and why a disconnection to the Mechane
-- server occurred.


type Model
    = BeforeRunning
        LifeCycle.WSCloseEvent
        (List (List String))
        { extantUrl : String
        , shutterbugActionsPopUp : Bool
        , theme : Theme
        }
    | AfterRunning
        LifeCycle.WSCloseEvent
        (List (List String))
        PixScreen.Model
        { extantUrl : String
        , popUps : PopUp.Model
        , theme : Theme
        }


theme : Model -> Theme
theme model =
    case model of
        BeforeRunning _ _ data ->
            data.theme

        AfterRunning _ _ _ data ->
            data.theme


viewNotification : LifeCycle.WSCloseEvent -> List (Html a)
viewNotification notification =
    String.split "\n" (LifeCycle.explainWSCloseEvent notification)
        |> List.map
            (\s ->
                if String.endsWith "." s then
                    String.dropRight 1 s

                else
                    s
            )
        |> List.indexedMap
            (\i s ->
                if i == 0 then
                    H.h2 [] [ H.text s ]

                else
                    H.p [] [ H.text s ]
            )
