module Devoir exposing (..)

import Json.Decode as D


devoirDecoder : D.Decoder Bool
devoirDecoder =
    D.field "isPaused" D.bool


type UICommand
    = Pause
    | Play
    | Shutdown


toRecord : UICommand -> { uiCommand : String }
toRecord command =
    case command of
        Pause ->
            { uiCommand = "pause" }

        Play ->
            { uiCommand = "play" }

        Shutdown ->
            { uiCommand = "shutdown" }
