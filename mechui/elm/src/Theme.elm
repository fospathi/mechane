module Theme exposing (..)

import Css as C
import Css.Animations as CA
import Css.Global
import Css.Transitions as CT
import Static.Motif


type Colour
    = Colour C.Color
    | CustomProperty String


type alias Theme =
    Static.Motif.Motif



-- FUNCTIONS


init : Static.Motif.Model -> Theme
init model =
    Static.Motif.motif model


withInitialDefault : Maybe Theme -> Theme
withInitialDefault maybeTheme =
    Maybe.withDefault initial maybeTheme



-- THEME


initial : Theme
initial =
    -- The initial theme before the world's motif is known.
    { motifName = "InitialMotif"

    --
    , accent = deepSkyBlue
    , boxShadow = "0px 0.1em 0.5em 1px DimGray"
    , buttonBackground = C.rgba 255 255 255 0.85
    , primary = darkOrange
    , primaryGlow = C.rgba 255 210 0 1.0
    , primaryLight = orange
    , ready = green
    , textNormal = black

    --
    , buttonOpaqueBackground = C.rgb 255 255 255
    , error = C.rgb 255 0 0
    , warning = C.rgb 255 100 100
    , textPassive = darkGray
    , textBigPassive = lightGray

    --
    , buttonBorder = 0.4
    , buttonBorderRadius = 1.2
    , buttonIconFontSize = 2.0
    , buttonPadding = 0.6

    --
    , buttonHeight = 4 -- 4 == 2 + (0.4 + 0.6) + (0.4 + 0.6)
    }



-- Custom CSS properties to store values for button background hover transition
-- animations.


buttonBackgroundStop1Percentage : String
buttonBackgroundStop1Percentage =
    "--button-background-stop-1-percentage"


buttonBackgroundStop2Percentage : String
buttonBackgroundStop2Percentage =
    "--button-background-stop-2-percentage"


buttonBackgroundStop1Color : String
buttonBackgroundStop1Color =
    "--button-background-stop-1-color"



-- The class for the little icon which is drawn over a button to indicates that
-- the button opens and closes a retractable pop up of some kind.


expanderIconClass : String
expanderIconClass =
    "mechane-elm-expander-icon"


popUpDuration : Float
popUpDuration =
    750


popUpDurationMS : String
popUpDurationMS =
    String.fromFloat popUpDuration ++ "ms"



-- Wrap a CSS custom property in a var(...) expression.


var : String -> String
var name =
    "var(" ++ name ++ ")"



-- STYLES


button :
    Theme
    ->
        { inactive : List C.Style
        , standard : List C.Style
        , unselected : List C.Style
        }
button theme =
    let
        backgroundGradient =
            radialGradient
                [ ( CustomProperty buttonBackgroundStop1Color
                  , buttonBackgroundStop1Percentage
                  )
                , ( Colour theme.primaryLight
                  , buttonBackgroundStop2Percentage
                  )
                ]

        hover =
            [ C.border3 (C.em theme.buttonBorder) C.solid theme.primaryLight
            , C.boxShadow5
                (C.em 0)
                (C.em 0)
                (C.em 0.7)
                (C.em 0.2)
                theme.primaryGlow
            , C.property buttonBackgroundStop1Percentage "65%"
            , C.property buttonBackgroundStop2Percentage "75%"
            ]

        transitions =
            transition
                [ ( "border", 100 )
                , ( "box-shadow", 300 )
                , ( buttonBackgroundStop1Percentage, 300 )
                , ( buttonBackgroundStop2Percentage, 300 )
                , ( buttonBackgroundStop1Color, 300 )
                ]
    in
    { inactive =
        [ C.backgroundColor theme.buttonBackground
        , C.border3 (C.em theme.buttonBorder) C.solid theme.textBigPassive
        , C.borderRadius (C.em theme.buttonBorderRadius)
        , C.color theme.textBigPassive
        , C.padding (C.em theme.buttonPadding)
        ]
    , standard =
        [ backgroundGradient
        , C.border3 (C.em theme.buttonBorder) C.solid theme.primary
        , C.borderRadius (C.em theme.buttonBorderRadius)
        , C.color theme.primary
        , C.outline C.none
        , C.padding (C.em theme.buttonPadding)
        , transitions
        , C.hover <|
            Css.Global.descendants
                [ Css.Global.typeSelector ("." ++ expanderIconClass)
                    [ C.opacity (C.num 1)
                    , CT.transition
                        [ CT.transform popUpDuration
                        , CT.opacity 400
                        ]
                    ]
                ]
                :: hover
        ]
    , unselected =
        [ backgroundGradient
        , C.border3 (C.em theme.buttonBorder) C.solid theme.primary
        , C.borderRadius (C.em theme.buttonBorderRadius)
        , C.color theme.textBigPassive
        , C.padding (C.em theme.buttonPadding)
        , transitions
        , C.hover hover
        ]
    }


buttonWithMargin :
    Theme
    ->
        { inactive : List C.Style
        , standard : List C.Style
        , unselected : List C.Style
        }
buttonWithMargin theme =
    { inactive = bottomMargin theme :: .inactive (button theme)
    , standard = bottomMargin theme :: .standard (button theme)
    , unselected = bottomMargin theme :: .unselected (button theme)
    }


bottomMargin : Theme -> C.Style
bottomMargin theme =
    C.marginBottom (C.em theme.buttonPadding)


errorContainerStyles : Theme -> List C.Style
errorContainerStyles theme =
    [ C.border3 (C.em 0.3) C.dotted theme.primaryLight
    , C.maxWidth (C.em 70)
    , C.marginLeft C.auto
    , C.marginRight C.auto
    ]


flashingReadyToSubmitStyles : Theme -> List C.Style
flashingReadyToSubmitStyles theme =
    let
        property =
            buttonBackgroundStop1Color

        buttonBackground =
            colToString theme.buttonBackground
    in
    [ C.animationName <|
        CA.keyframes
            [ ( 0
              , [ CA.property property buttonBackground ]
              )
            , ( 10
              , [ CA.property property <| colToString theme.ready ]
              )
            , ( 20
              , [ CA.property property <| colToString theme.ready ]
              )
            , ( 30
              , [ CA.property property buttonBackground ]
              )
            , ( 100
              , [ CA.property property buttonBackground ]
              )
            ]
    , C.animationDuration (C.ms 2000)
    , C.animationIterationCount C.infinite
    ]


kaputButtonStyles : Theme -> List C.Style
kaputButtonStyles theme =
    C.property "filter" "blur(0.08em)" :: .inactive (button theme)


radialGradient : List ( Colour, String ) -> C.Style
radialGradient colStops =
    List.map
        (\( c, s ) ->
            (case c of
                Colour col ->
                    colToString col

                CustomProperty col ->
                    var col
            )
                ++ " "
                ++ var s
        )
        colStops
        |> String.join ","
        |> (\stops -> "radial-gradient(" ++ stops ++ ")")
        |> C.property "background"


transition : List ( String, Int ) -> C.Style
transition transitions =
    List.map (\( p, d ) -> p ++ " " ++ String.fromInt d ++ "ms") transitions
        |> String.join ","
        |> C.property "transition"



-- DIV STYLES


controlColumnDivStyle : Theme -> List C.Style
controlColumnDivStyle theme =
    [ C.alignItems C.flexEnd
    , C.displayFlex
    , C.flexDirection C.column
    , C.justifyContent C.center
    , C.minWidth <| C.em theme.buttonHeight
    , C.paddingLeft <| C.em theme.buttonPadding
    , C.paddingRight <| C.em theme.buttonPadding
    ]


startingGridMiddleDivStyle : List C.Style
startingGridMiddleDivStyle =
    [ C.height <| C.pct 100
    , C.marginTop <| C.em 0
    , C.overflow C.auto
    , C.paddingTop <| C.em 0
    , C.textAlign C.center
    ]



-- COLOURS


colToString : C.Color -> String
colToString col =
    String.join ","
        [ "rgba(" ++ String.fromInt col.red
        , String.fromInt col.green
        , String.fromInt col.blue
        , String.fromFloat col.alpha ++ ")"
        ]


colToTransparent : C.Color -> C.Color
colToTransparent col =
    C.rgba col.red col.green col.blue 0


black : C.Color
black =
    C.rgb 0 0 0


consoleGreen : C.Color
consoleGreen =
    C.rgb 74 246 38


darkGray : C.Color
darkGray =
    C.rgb 169 169 169


darkOrange : C.Color
darkOrange =
    C.rgb 255 140 0


deepSkyBlue : C.Color
deepSkyBlue =
    C.rgb 0 191 255


dodgerBlue : C.Color
dodgerBlue =
    C.rgb 30 144 255


gold : C.Color
gold =
    C.rgb 255 215 0


green : C.Color
green =
    C.rgb 0 255 0


lightGray : C.Color
lightGray =
    C.rgb 211 211 211


orange : C.Color
orange =
    C.rgb 255 165 0


red : C.Color
red =
    C.rgb 255 0 0


white : C.Color
white =
    C.rgb 255 255 255
