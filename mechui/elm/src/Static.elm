module Static exposing (isComplete)

import Maybe.Extra
import Static.About
import Theme exposing (Theme)


isComplete :
    { r
        | maybeAbout : Maybe Static.About.Model
        , maybeTheme : Maybe Theme
    }
    -> Bool
isComplete data =
    List.all (\bool -> bool)
        [ Maybe.Extra.isJust data.maybeAbout
        , Maybe.Extra.isJust data.maybeTheme
        ]
