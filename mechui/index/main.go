//go:generate go run .

// Generate the index.html file under the web directory.
//
// Changes the working directory to this file's directory.
package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"gitlab.com/fospathi/universal/mech"
)

// Directory paths relative to this directory.
const (
	asciidoctorFontDir = "../web/font/asciidoctor"
	serverRootDir      = "../web/"
)

func main() {

	// Get the Asciidoctor font names.
	var vaf []string
	{
		const (
			fontCount  = 45       // Expected number of font URLs.
			fontSuffix = ".woff2" // The fonts are WOFF 2.0 format.
		)

		ve, err := os.ReadDir(asciidoctorFontDir)
		if err != nil {
			log.Fatalln(err)
		}
		for _, e := range ve {
			if e.IsDir() || !strings.HasSuffix(e.Name(), fontSuffix) {
				continue
			}
			vaf = append(vaf, e.Name())
		}
		if len(vaf) != fontCount {
			log.Fatalln(fmt.Errorf("expected %v Asciidoctor fonts: got %v",
				fontCount, len(vaf)))
		}
	}

	type indexTemplateData struct {
		AsciidoctorFontNames []string
	}

	// Populate the template's input data.
	var (
		tmplData indexTemplateData
	)

	tmplData.AsciidoctorFontNames = vaf

	// Generate the HTML.
	sb := &strings.Builder{}
	if tmpl, err := template.ParseFiles("index.html"); err != nil {
		log.Fatalln(err)
	} else if err = tmpl.Execute(sb, tmplData); err != nil {
		log.Fatalln(err)
	}

	// Make sure the relative path to the server's root directory is valid.
	mech.ChdirToHere()
	if _, err := os.Stat(serverRootDir); err != nil {
		log.Fatalln(err)
	}

	// Write the HTML to disk.
	index := filepath.Join(serverRootDir, "index.html")
	if f, err := os.Create(index); err != nil {
		log.Fatalln(err)
	} else if _, err := f.WriteString(sb.String()); err != nil {
		log.Fatalln(err)
	}
}
