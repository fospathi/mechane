package asciidoc_test

import (
	"log"
	"runtime"
	"strings"
	"testing"
	"time"

	"gitlab.com/fospathi/mechane/mechui/asciidoc"
)

func TestNewConverter(t *testing.T) {

	// Record the ambient number of Goroutines.

	nGoAtT0 := runtime.NumGoroutine()

	// Test creating a new Converter.

	sb := &strings.Builder{}
	l := log.New(sb, "testlog: ", log.LstdFlags)
	a2h, err := asciidoc.NewConverter(l)

	{
		want := error(nil)
		got := err
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test some simple conversions.

	a := "= Document Title"
	h, err := a2h.Convert(a)

	{
		want1, want2 := "<h1>Document Title</h1>\n", error(nil)
		got1, got2 := h, err

		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}
	}

	a = "== Document Subtitle"
	h, err = a2h.Convert(a)

	{
		want1, want2 :=
			"<div class=\"sect1\">\n"+
				"<h2 id=\"_document_subtitle\">Document Subtitle</h2>\n"+
				"<div class=\"sectionbody\">\n\n</div>\n</div>",
			error(nil)
		got1, got2 := h, err

		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}
	}

	// Test a Converter shut down.

	a2h.Shutdown()
	time.Sleep(2 * time.Second)

	{
		want, got := "", sb.String()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	a = "= Another Document Title"
	_, err = a2h.Convert(a)

	{
		want, got := true, asciidoc.IsShutdownError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test that the ambient number of Goroutines is restored after the shut
	// down.

	nGoAtT1 := runtime.NumGoroutine()

	{
		want, got := nGoAtT0, nGoAtT1
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
