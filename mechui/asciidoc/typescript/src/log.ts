// Before a WebSocket connection to the caller's server is established use
// stderr to communicate errors to the caller.
export function logToStderr(err: string) {
    process.stderr.write("asciidoctor: " + err + "\n")
}
