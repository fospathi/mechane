import { CloseEvent, ErrorEvent, Event, MessageEvent, WebSocket } from "ws"

import { createInterface } from "readline"
import { logToStderr } from "./log"
import { ConvertResult, InitialData, initialDataSchema } from "./schema"

// Listen for asciidoc messages and apply the given converter to each message.
//
// The returned promise only resolves when the processing of messages can no
// longer proceed.
export async function listen(
    converter: (ev: MessageEvent) => ConvertResult
): Promise<"initial data failure" | CloseEvent> {
    const rl = createInterface({
        input: process.stdin, // Initial data arrives from the caller via stdin.
        output: process.stdout,
        terminal: false,
    })

    // Wait for the initial data to arrive from the caller.

    const initialDataPromise = () => {
        return new Promise<InitialData | undefined>((resolve) => {
            rl.once("line", (line) => {
                try {
                    const initialData: unknown = JSON.parse(line)
                    const result = initialDataSchema.safeParse(initialData)
                    if (result.success) {
                        resolve(result.data)
                    } else {
                        const issues = result.error.issues.join(", ")
                        logToStderr(`initial data: incorrect format: ${issues}`)
                        resolve(undefined)
                    }
                } catch (error) {
                    if (error instanceof SyntaxError) {
                        logToStderr(`initial data: JSON: ${error.message}`)
                    } else if (error instanceof Error) {
                        logToStderr(`initial data: parse: ${error.message}`)
                    }
                    resolve(undefined)
                }
            })
        })
    }

    const initialData = await initialDataPromise()
    if (!initialData) {
        logToStderr("initial data failure")
        return "initial data failure"
    }

    // Use the initial data to open a WebSocket to the caller's server.

    const socketPath = "/asciidoc"
    const ws = new WebSocket("ws://" + initialData.socketHost + socketPath)

    ws.onerror = (ev: ErrorEvent) => {
        logToStderr(`websocket: ${ev.message}`)
    }

    const closePromise = new Promise<CloseEvent>((resolve) => {
        ws.onclose = (ev: CloseEvent) => resolve(ev)
    })

    const openPromise = new Promise<Event>((resolve) => {
        ws.onopen = (ev: Event) => resolve(ev)
    })

    const openWebSocketsResult = await Promise.any<"failure" | "success">([
        closePromise.then(() => "failure"),
        openPromise.then(() => "success"),
    ])

    if (openWebSocketsResult === "success") {
        ws.onmessage = (ev: MessageEvent) => {
            ws.send(JSON.stringify(converter(ev)))
        }

        // Messages don't start to arrive until after the access code is sent as
        // the first message of the first client to connect.
        ws.send(JSON.stringify(initialData.socketAccessCode))
    } else {
        logToStderr("websocket: failed to open")
    }

    return closePromise
}
