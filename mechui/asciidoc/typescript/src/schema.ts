import { z } from "zod"

// The caller is expected to send the initial data to this node process over
// stdin. The initial data is then used to open a WebSocket to the caller's
// server.
export type InitialData = z.infer<typeof initialDataSchema>
export const initialDataSchema = z.object({
    // The caller only trusts the first client which sends the access code as
    // the first message on the WebSocket.
    socketAccessCode: z.string(),
    // A WebSocket address from which asciidoc messages are received.
    //
    // For each asciidoc message received, reply on the same socket with the
    // HTML conversion result before reading the next message.
    socketHost: z.string(),
})

// A successful convert result is indicated by an empty error array.
export interface ConvertResult {
    error: string[]
    html: string
}
