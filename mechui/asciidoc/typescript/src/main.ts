import Processor from "asciidoctor"

import { newMathJaxSTEMProcessor, stemEnabled } from "./mathjax"
import { listen } from "./listen"

// Convert asciidoc to HTML using the official asciidoctor JS package.
//
// Open a WebSocket connection to the calling process' server and run the
// following procedure in a loop: receive an asciidoc message from the calling
// process and reply with the result of converting the asciidoc message to HTML.
async function main() {
    const processMathJaxSTEM = newMathJaxSTEMProcessor()
    const processor = Processor()
    const loggerManager = processor.LoggerManager

    const closeReason = await listen((message) => {
        const memoryLogger = processor.MemoryLogger.create()
        loggerManager.setLogger(memoryLogger)

        try {
            const asciidoc: unknown = JSON.parse(message.data.toString())
            if (typeof asciidoc !== "string") {
                return { error: ["convert: expected string input"], html: "" }
            }

            const html = processor.convert(asciidoc, {
                attributes: {
                    showtitle: true,
                },
                safe: "secure",
            })

            const errors = memoryLogger.getMessages()
            if (errors.length > 0) {
                return {
                    error: errors.map((error) => error.getText()),
                    html: "",
                }
            } else if (typeof html !== "string") {
                return {
                    error: ["convert: expected string output"],
                    html: "",
                }
            }

            return {
                error: [],
                html: stemEnabled(asciidoc) ? processMathJaxSTEM(html) : html,
            }
        } catch (error) {
            return {
                error:
                    error instanceof Error
                        ? [error.message]
                        : ["convert: unknown error"],
                html: "",
            }
        }
    })

    // Close the pipes from this end. Allow a little time for stderr to flush.
    // This duration should be less than the server's WebSocket close delay so
    // the server can obtain this process' exit code after that duration.
    await new Promise((resolve) => setTimeout(resolve, 500))
    process.stdin.destroy()
    process.stderr.destroy()

    if (closeReason !== "initial data failure") {
        const normalClosure = 1000
        if (closeReason.code === normalClosure) {
            return
        }
    }

    process.exitCode = 1 // Signal an abnormal exit.
}

await main()
