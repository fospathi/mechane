import { mathjax } from "mathjax-full/js/mathjax"
import { AsciiMath } from "mathjax-full/js/input/asciimath"
import { SVG } from "mathjax-full/js/output/svg"
import { TeX } from "mathjax-full/js/input/tex"
import { liteAdaptor } from "mathjax-full/js/adaptors/liteAdaptor"
import type { LiteDocument } from "mathjax-full/js/adaptors/lite/Document"
import { RegisterHTMLHandler } from "mathjax-full/js/handlers/html"
import { AssistiveMmlHandler } from "mathjax-full/js/a11y/assistive-mml.js"

import { AllPackages } from "mathjax-full/js/input/tex/AllPackages"

import "mathjax-full/js/util/entities/all.js"

// The returned function processes the STEM content in the HTML passed to it,
// which shall be the output of the Asciidoctor HTML converter, which has been
// delimited and is intended for further conversion by MathJax.
//
// Note that the STEM content in the HTML to be processed by the function shall
// use the Asciidoctor delimiters for STEM content documented here:
// https://github.com/asciidoctor/asciidoctor/issues/1055#issuecomment-51165207
export function newMathJaxSTEMProcessor(): (s: string) => string {
    const adaptor = liteAdaptor()
    const handler = RegisterHTMLHandler(adaptor)
    AssistiveMmlHandler(handler)

    const asciimath = new AsciiMath({ delimiters: [["\\$", "\\$"]] })
    const tex = new TeX({
        inlineMath: [["\\(", "\\)"]],
        displayMath: [["\\[", "\\]"]],
        packages: AllPackages,
    })

    const em = 16.0
    const ex = 8.0
    const svg = new SVG({ fontCache: "local", exFactor: ex / em })

    function processMathJaxSTEM(html: string): string {
        const asciiDoc = mathjax.document(html, {
            InputJax: asciimath,
            OutputJax: svg,
        })
        asciiDoc.render()
        const asciiOutput = adaptor.outerHTML(
            adaptor.root(asciiDoc.document as LiteDocument)
        )

        const texDoc = mathjax.document(asciiOutput, {
            InputJax: tex,
            OutputJax: svg,
        })
        texDoc.render()
        const texOutput = adaptor.outerHTML(
            adaptor.root(texDoc.document as LiteDocument)
        )

        return texOutput
    }

    return processMathJaxSTEM
}

// If the AsciiDoc contains a line starting with the given STEM mode activator
// then STEM mode is enabled.
export function stemEnabled(asciidoc: string, activator = ":stem:"): boolean {
    for (const line of asciidoc.split("\n")) {
        if (line.startsWith(activator)) {
            return true
        }
    }
    return false
}
