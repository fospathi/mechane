/* 
eslint-disable
    no-undef,
    @typescript-eslint/no-var-requires,
*/

const path = require("path")

module.exports = {
    entry: "./build/main.js",
    experiments: {
        futureDefaults: true,
    },
    mode: "development",
    output: {
        path: path.resolve(__dirname, "../nodejs"),
        filename: "asciidoc.js",
    },
    target: "node",
}
