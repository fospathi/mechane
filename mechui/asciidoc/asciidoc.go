/*
Package asciidoc converts AsciiDoc to HTML by sending the AsciiDoc to a Node.js
process that uses the Asciidoctor.js Processor API.
*/
package asciidoc
