package asciidoc_test

import (
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/fospathi/mechane/mechui/asciidoc"
)

func TestConvert(t *testing.T) {
	sb := &strings.Builder{}
	l := log.New(sb, "testlog: ", log.LstdFlags)
	a2h, err := asciidoc.NewConverter(l)
	const testDir = "./convert_test"
	const outputFile = "convert_test.html"
	const html = `<h1>Annie And Tammy</h1>
<div class="paragraph">
<p>Annie and Tammy are the coolest cats.</p>
</div>`

	want := error(nil)
	{ // Check the converter is ok.
		got := err
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{ // Convert the AsciiDoc in the test directory.
		got := asciidoc.Convert(a2h, testDir)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{ // Check the generated HTML.
		want1, want2 := html, error(nil)
		vb, err := os.ReadFile(filepath.Join(testDir, outputFile))
		got1, got2 := string(vb), err
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}
	}

	{ // Clean up for the next test.
		got := os.Remove(filepath.Join(testDir, outputFile))
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
