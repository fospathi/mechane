package asciidoc

import (
	"errors"
	"os"
	"path/filepath"
	"strings"
	"time"

	"io/fs"

	"gitlab.com/fospathi/dryad/eqset"
)

// Convert AsciiDoc files with the '.asciidoc' extension to HTML if they are
// found in the filesystem rooted at the given directory.
//
// Skip conversion of AsciiDoc files with an existing output file modified later
// than itself. Generally, conversion overwrites existing output files.
//
// An HTML output file will take the same filename as the AsciiDoc input file
// but with a .html extension and will be a sibling of the input file.
func Convert(cnv *Converter, rootDir string) error {
	if rootDir == "" {
		return errors.New("empty string for root directory")
	}

	files, err := findAsciiDocAndHTML(rootDir)
	if err != nil {
		return err
	}

	const htmlExt = ".html"
	for path, modTime := range files {
		if strings.HasSuffix(path, htmlExt) {
			continue
		}
		output := path[:strings.LastIndex(path, ".")] + htmlExt
		if htmlModTime, ok := files[output]; ok && htmlModTime.After(modTime) {
			continue
		} else if asciidoc, err := os.ReadFile(path); err != nil {
			return err
		} else if html, err := cnv.Convert(string(asciidoc)); err != nil {
			return err
		} else {
			os.WriteFile(output, []byte(html), 0666)
		}
	}

	return nil
}

func findAsciiDocAndHTML(rootDir string) (map[string]time.Time, error) {
	root := os.DirFS(rootDir)

	type Containser interface {
		Contains(string) bool
	}
	const fullExt = ".asciidoc" // Convert AsciiDoc files with the full extension.
	relevantExts := Containser(eqset.New(fullExt, ".html"))
	filesWithExts := map[string]time.Time{} // Path and modification time.

	return filesWithExts, fs.WalkDir(root, ".",
		func(path string, d fs.DirEntry, err error) error {
			if err != nil {
				if d.IsDir() {
					return fs.SkipDir
				}
				return err
			} else if d.IsDir() {
				return nil
			} else if ext := filepath.Ext(path); !relevantExts.Contains(ext) {
				return nil
			} else if info, err := d.Info(); err != nil {
				return nil
			} else if strings.HasPrefix(".", info.Name()) {
				return nil
			} else {
				filesWithExts[filepath.Join(rootDir, path)] = info.ModTime()
			}

			return nil
		})
}
