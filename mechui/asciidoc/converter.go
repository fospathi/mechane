package asciidoc

import (
	"bufio"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"sync"
	"time"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/universal/mech"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

// Converter is an AsciiDoc to HTML converter.
type Converter struct {
	m sync.Mutex

	asciidoc chan string
	html     chan convertResult

	shutdown chan struct{}
}

type convertResult struct {
	err  error
	html string
}

func (cvr *Converter) convert(
	asciidoc string, c wsConnection,
) (convertResult, error) {
	type conversionResult struct {
		Error []string `json:"error"`
		HTML  string   `json:"html"`
	}

	const (
		// Max time to convert an AsciiDoc to HTML.
		conversionTimeout = 10 * time.Second
	)

	// Send the AsciiDoc message to the converter.
	err := func() error {
		ctx, cancel :=
			context.WithTimeout(c.rCon, 1*time.Second)
		defer cancel()

		return wsjson.Write(ctx, c.c, &asciidoc)
	}()
	if err != nil {
		return convertResult{}, err
	}

	// Wait for the outcome of the conversion.
	result, err := func() (conversionResult, error) {
		ctx, cancel :=
			context.WithTimeout(c.rCon, conversionTimeout)
		defer cancel()

		var v conversionResult
		err = wsjson.Read(ctx, c.c, &v)
		return v, err
	}()
	if err != nil {
		return convertResult{}, err
	}

	if len(result.Error) > 0 {
		return convertResult{
			errors.New(strings.Join(result.Error, ", ")),
			result.HTML,
		}, nil
	}
	return convertResult{nil, result.HTML}, nil
}

// Convert the AsciiDoc to HTML.
//
// If the returned error is neither nil nor a shutdown error, as indicated by
// [IsShutdownError], then it is an AsciiDoc conversion error.
//
// If the returned error is a shutdown error, as indicated by [IsShutdownError],
// then the Converter log contains the underlying error if the Converter
// creation was successful and the shutdown was not graceful.
func (cvr *Converter) Convert(asciidoc string) (string, error) {
	select {
	case <-cvr.shutdown:
		return "", errShutdown
	case cvr.asciidoc <- asciidoc:
	}
	res := <-cvr.html
	return res.html, res.err
}

// Shutdown the converter gracefully and release its resources.
func (cvr *Converter) Shutdown() {
	cvr.m.Lock()
	defer cvr.m.Unlock()

	select {
	case <-cvr.shutdown:
		return
	default:
	}
	close(cvr.shutdown)
}

// NewConverter constructs a new Converter.
//
// A nil error return value indicates a successful start. A converter that
// encounters an internal error after a successful start writes internal errors
// to the given log. Conversion errors are not written to the log.
//
// The working directory is temporarily changed during execution and may be left
// in an undefined state for the failed start case.
func NewConverter(l *log.Logger) (*Converter, error) {
	// Temporarily change the working directory to here so Node.js can find the
	// asciidoc converter JS files.
	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	mech.ChdirToHere()

	const (
		// Max time for converter process to connect to HTTP server.
		connectTimeout = 5 * time.Second
		// Max time to allow the HTTP server to shutdown gracefully.
		shutdownTimeout = 15 * time.Second
	)

	cvr := &Converter{
		m: sync.Mutex{},

		asciidoc: make(chan string),
		html:     make(chan convertResult),
		shutdown: make(chan struct{}),
	}

	// A secret code known to the server which is sent by stdin to the converter
	// process so it can connect to the server's WebSocket.
	code, err := mech.RandBase64String(32)
	if err != nil {
		cvr.Shutdown()
		return nil, err
	}

	h := newHandler(code)

	svr, err := newServer(h.hf)
	if err != nil {
		cvr.Shutdown()
		return nil, err
	}

	stop := func() {
		ctx, cancel :=
			context.WithTimeout(context.Background(), shutdownTimeout)
		defer cancel()

		close(h.done)
		cvr.Shutdown()
		svr.Shutdown(ctx)
	}

	cnv, err := newConverterProc(code, svr.host)
	if err != nil {
		stop()
		return nil, err
	}

	if err := os.Chdir(wd); err != nil {
		stop()
		return nil, err
	}

	// Wait for the first connection to the HTTP server.
	var c wsConnection
	select {
	case c = <-h.wsC:

	case <-time.After(connectTimeout):
		select {
		// Prevent a WebSocket connection lest a slow unconnected converter
		// process wastes effort connecting to the now defunct server.
		case <-h.first:
		default:
		}
		stop()
		return nil, errors.New("converter process did not connect in time")

	case err := <-h.errC:
		stop()
		return nil, err

	case err := <-svr.errC:
		stop()
		return nil, err

	case err := <-cnv.errC:
		stop()
		return nil, err
	}

	go func() {
		defer stop()

	loop:
		for {
			select {
			case asciidoc := <-cvr.asciidoc:
				if result, err := cvr.convert(asciidoc, c); err != nil {
					l.Println(err)
					cvr.html <- convertResult{err: errShutdown, html: ""}
					c.c.Close(websocket.StatusInternalError, "conversion error")
					time.Sleep(mech.WSCloseDelay)
					break loop
				} else {
					cvr.html <- result
				}

			case <-cvr.shutdown:
				c.c.Close(websocket.StatusNormalClosure, "converter shutdown")
				time.Sleep(mech.WSCloseDelay)
				if exit := cnv.exitCode(); exit != 0 {
					l.Println(fmt.Errorf("non zero exit code: (%v)", exit))
				}
				break loop

			case err := <-svr.errC:
				l.Println(err)
				c.c.Close(websocket.StatusInternalError, "server error")
				time.Sleep(mech.WSCloseDelay)
				break loop

			case err := <-cnv.errC:
				l.Println(err)
				c.c.Close(websocket.StatusInternalError, "converter process error")
				time.Sleep(mech.WSCloseDelay)
				break loop

			}
		}
	}()

	return cvr, nil
}

type server struct {
	host string
	*http.Server
	errC chan error
}

func newServer(handler http.HandlerFunc) (server, error) {
	const (
		socketHost = "127.0.0.1:0" // By using 0 a port number is automatically chosen.
		socketPath = "/asciidoc"
	)
	var (
		err      error
		errC     = make(chan error, 1)
		httpSvr  http.Server
		listener net.Listener
		mux      = http.NewServeMux()
	)

	if listener, err = net.Listen("tcp", socketHost); err != nil {
		return server{}, err
	}

	mux.Handle(socketPath, handler)

	httpSvr.Addr = listener.Addr().String()
	httpSvr.Handler = mux

	go func() {
		if err = httpSvr.Serve(listener); err != nil {
			errC <- err
		}
	}()

	return server{listener.Addr().String(), &httpSvr, errC}, nil
}

type wsConnection struct {
	c    *websocket.Conn
	rCon context.Context
}

type handler struct {
	done  chan struct{}
	errC  chan error
	first <-chan struct{}
	hf    http.HandlerFunc
	wsC   chan wsConnection
}

func newHandler(code string) handler {
	// Max time for handler to wait for access code after connection.
	const accessCodeTimeout = 2 * time.Second
	const readLimit = int64(1 << 20) // Incoming message size limit.

	h := handler{
		done:  make(chan struct{}),
		errC:  make(chan error, 1),
		first: dryad.NewFirst(struct{}{}),
		wsC:   make(chan wsConnection),
	}

	h.hf = func(w http.ResponseWriter, r *http.Request) {
		select {
		case <-h.first: // Only the first request is accepted as potentially valid.
		default:
			http.Error(w, "request window is closed", http.StatusForbidden)
			return
		}

		c, err := websocket.Accept(w, r, nil)
		if err != nil {
			h.errC <- err
		}
		c.SetReadLimit(readLimit)

		// Read the first message and check it is the access code that was
		// previously sent to the converter process by stdin.
		err = func() error {
			ctx, cancel := context.WithTimeout(r.Context(), accessCodeTimeout)
			defer cancel()

			var v string
			err = wsjson.Read(ctx, c, &v)
			if err != nil {
				return err
			} else if v != code {
				return errors.New("error: first message: incorrect access code")
			}
			return nil
		}()
		if err != nil {
			h.errC <- err
		} else {
			h.wsC <- wsConnection{c: c, rCon: r.Context()}
		}

		<-h.done // Keep the connection's Goroutine alive until shutdown.
	}

	return h
}

type converterProc struct {
	errC     chan error
	exitCode func() int
	stdin    io.WriteCloser
}

func newConverterProc(code string, host string) (converterProc, error) {
	const converterPath = "./nodejs/asciidoc.js"

	var (
		cmd      = exec.Command("node", converterPath)
		cmdMu    = &sync.Mutex{}
		err      error
		errC     = make(chan error, 1)
		errTasks []func() error
		exitCode = func() int {
			cmdMu.Lock()
			defer cmdMu.Unlock()

			return cmd.ProcessState.ExitCode()
		}
		stdin  io.WriteCloser
		stderr io.ReadCloser
		wait   = func() {
			cmdMu.Lock()
			defer cmdMu.Unlock()

			cmd.Wait()
		}
	)

	doErrTasks := func() {
		for _, f := range errTasks {
			f()
		}
	}

	if stdin, err = cmd.StdinPipe(); err != nil {
		return converterProc{}, err
	}
	errTasks = append(errTasks, stdin.Close)

	if stderr, err = cmd.StderrPipe(); err != nil {
		doErrTasks()
		return converterProc{}, err
	}
	errTasks = append(errTasks, stderr.Close)

	if err = cmd.Start(); err != nil {
		doErrTasks()
		return converterProc{}, err
	}
	errTasks = append(errTasks, cmd.Process.Kill)

	go func() {
		// Read errors from the converter process.

		const batchErr = true // True is the 'production' setting.
		if batchErr {
			if vb, err := io.ReadAll(stderr); err != nil {
				if string(vb) == "" {
					errC <- err
				} else {
					errC <- fmt.Errorf("%v: %w", string(vb), err)
				}
			} else {
				if string(vb) != "" {
					errC <- errors.New(string(vb))
				}
			}
		} else {
			// This if-else branch prints 'lines' of errors from stderr as they
			// occur. Good for debugging issues with the converter process.
			fileScanner := bufio.NewScanner(stderr)
			for fileScanner.Scan() {
				fmt.Printf("converter process: %v\n", fileScanner.Text())
			}
			if err := fileScanner.Err(); err != nil {
				fmt.Printf("stderr fileScanner: %v\n", err)
			}
			errC <- nil
		}

		// Update the converter process ProcessState, including the exit code.
		wait()
	}()

	// Send the host and access code to the converter process so it can connect
	// to the WebSocket.
	err = func() error {
		initial, err := json.Marshal(struct {
			SocketAccessCode string `json:"socketAccessCode"`
			SocketHost       string `json:"socketHost"`
		}{
			SocketAccessCode: code,
			SocketHost:       host,
		})
		if err != nil {
			return err
		}
		if _, err = stdin.Write(append(initial, []byte("\n")...)); err != nil {
			return err
		}
		return nil
	}()
	if err != nil {
		doErrTasks()
		return converterProc{}, err
	}

	return converterProc{errC, exitCode, stdin}, nil
}

// IsShutdownError reports whether the given conversion error is due to a shut
// down converter.
func IsShutdownError(err error) bool {
	return errors.Is(err, errShutdown)
}

var errShutdown = errors.New("converter is shutdown")
