/*
Package mechane is an app that implements a number of interactive 3D worlds.
*/
package mechane

import (
	_ "embed"
)

// GoMod is the mechane module's go.mod file.
//
//go:embed go.mod
var GoMod []byte
