/*
Package mechrun sets up a Mechane server and runs a Mechane world on it.
*/
package mechrun

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/fospathi/mechane/about"
	"gitlab.com/fospathi/mechane/mechserver"
	"gitlab.com/fospathi/mechane/motif"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/wiregl"
)

// A World which runs on the argument Mechane server.
type World func(server mechserver.Server)

// RunSpec specifies the properties and resources of a new Mechane server used
// to run a world.
type RunSpec struct {
	Address    string
	Rasteriser string
	UIWeb      http.FileSystem
	WorldWeb   http.FileSystem

	About about.Software
	Motif motif.Motif
}

// Run the given Mechane world with the given name from your app's main()
// function/thread.
//
// Shall only be called from the main thread, that is, not from a goroutine!
func Run(spec RunSpec, name string, world World) {

	var (
		ec  <-chan error
		err error
		mc  wiregl.MainThread
		svr mechserver.Server
	)
	svr, mc, ec, err = mechserver.New(mechserver.ServerSpec{
		Host: mechserver.Host{
			Rasteriser: spec.Rasteriser,
			Server:     spec.Address,
		},

		About:    spec.About,
		Motif:    spec.Motif,
		UIWeb:    spec.UIWeb,
		WorldWeb: spec.WorldWeb,
	})
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		// Rasterise an empty scene to block until the rasteriser is ready.
		if _, err := svr.SceneRasteriser.Rasterise(wiregl.Scene{}); err != nil {
			log.Fatalf("error: %v\n", err)
		}
		fmt.Printf("\"%v\" @ %v\n", name, svr.URL)
		world(svr)
	}()

	done := make(chan struct{})
	go func() {
		// Catch a terminal interrupt.
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		<-c
		fmt.Printf("\nIntercepted interrupt signal.\n")
		fmt.Printf("Shutting down...\n")
		svr.BeginShutdown(mech.ShutdownFromTerminal)
		time.Sleep(mech.ShutdownPeriodLimit)
		close(done)
	}()
loop:
	for {
		select {
		// Functions received on this channel must be run in the main thread.
		case f := <-mc:
			f()
		case err := <-ec:
			log.Fatalf("error: %v\n", err)
		case <-done:
			fmt.Printf("warning: Mechane world \"%v\" did not quit in time!\n",
				name)
			break loop
		case <-svr.ShutdownFinalised():
			break loop
		}
	}
}
