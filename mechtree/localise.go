package mechtree

import (
	"fmt"
	"math"

	"gitlab.com/fospathi/dryad/leafstar"
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechserver/mechoverlayer"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/wiregl"
)

// Localise the visual components on the tree from their various frames to that
// of the given camera's frame.
func Localise(cam Camera) (mech.Depiction, mechoverlayer.Elements) {
	var (
		es = newElements()
		db = wiregl.NewDepictionBuilder()
	)

	if cam.Node == nil {
		return db.Complete(), es.complete()
	}

	localise := func(node *Node, toLocal maf.Aff) {
		node.Lock()
		defer node.Unlock()

		for _, pic := range node.Depictioners {
			db.Add(pic.Depiction(), toLocal.ToOpenGLMat4x3())
		}

		const zGap = 1e-3 // Ignore overlay elements very close to the screen.

		el := node.OverlayElement
		if el == (OverlayElement{}) {
			return
		}

		var (
			o      = toLocal.Transform(maf.Zero)
			orient = el.Orientation != Flat && o.Z < -zGap
			tfm    string
			tfmO   = el.OriginPosition.transformOrigin()
		)
		if !orient {
			tfm = el.OriginPosition.transformTranslate()
		} else {

			// Use a Y flipping reflection as the first transformation to
			// map from upside down CSS space to camera space. Also, later
			// on, it should be used as the final transformation to reverse
			// the effect.
			refl := maf.NewXZRefl()

			if wiregl.IsOrthographic(cam.Lens.ToOpenGLMat4()) {

				var (
					scaleRear = el.Orientation == XYFront &&
						o.To(toLocal.Transform(maf.PosZ)).Dot(maf.NegZ) > 0
				)

				// The thought process here is to compose the transformations in
				// left to right order, so that the reference frame for each
				// transformation is the current transformed frame.
				//
				// A nice explainer, by Bali Balo, of right vs left ordering is
				// https://codepen.io/bali_balo/post/chaining-transforms.

				tfm += el.OriginPosition.transformTranslate()
				m := refl.Multiply(toLocal.ToMat3().ToAff()).Multiply(refl)
				tfm += fmt.Sprintf(" %v ", m.CSS())
				if scaleRear {
					tfm += " scaleX(-1) "
				}
			} else {

				var (
					scaleRear = el.Orientation == XYFront && (maf.Plane{
						P: o, Normal: o.To(toLocal.Transform(maf.PosZ)),
					}.SignedDistTo(maf.Zero) < 0)
				)

				// The thought process here is to compose the transformations in
				// right to left order, so that the reference frame for each
				// transformation is the global static frame.

				if scaleRear {
					tfm = " scaleX(-1) " + tfm
				}

				tfm = fmt.Sprintf(" %v ", refl.CSS()) + tfm

				toCam := fmt.Sprintf(" %v ", toLocal.ToMat3().ToAff().CSS())
				tfm = toCam + tfm

				// Rotate the element's frame onto the camera's central line of
				// sight. This orients the frame as though it is being viewed
				// along a perspective projection line between itself and the
				// perspective focal point at the global static frame origin.
				target := o.Unit()
				rot := maf.NegZ.Cross(target)
				ang := math.Copysign(
					target.Angle(maf.NegZ),
					-maf.ScalarTripleProduct(maf.NegZ, target, rot),
				)
				toCentreLine := fmt.Sprintf(" rotate3d(%v, %v, %v, %vrad) ",
					rot.X, rot.Y, rot.Z, ang,
				)
				tfm = toCentreLine + tfm

				tfm = fmt.Sprintf(" %v ", refl.CSS()) + tfm

				tfm = el.OriginPosition.transformTranslate() + tfm
			}

		}

		if tfm == "" {
			tfm = "none"
		}

		if o.Z < -zGap {
			es.addAhead(el, cssOrient{
				cam:             cam,
				nodeOrigin:      o,
				transform:       tfm,
				transformOrigin: tfmO,
			})
		} else {
			es.addBehind(el)
		}
	}

	leafstar.Traverse[*Node, maf.Frame](
		cam.Node,
		LocAcc{},
		NewLocaliser(localise),
	)

	return db.Complete(), es.complete()
}

// LocaliseDepictions on the tree from their various frames to that of the given
// node's frame.
func LocaliseDepictions(local *Node) mech.Depiction {
	db := wiregl.NewDepictionBuilder()

	localise := func(node *Node, toLocal maf.Aff) {
		node.Lock()
		defer node.Unlock()

		for _, pic := range node.Depictioners {
			db.Add(pic.Depiction(), toLocal.ToOpenGLMat4x3())
		}
	}

	leafstar.Traverse[*Node, maf.Frame](
		local,
		LocAcc{},
		NewLocaliser(localise),
	)

	return db.Complete()
}

// LocAcc is the accumulator for a localising leafstar traversal.
type LocAcc struct {
	// The previous node's frame.
	from maf.Frame
	// The localising transformation going from the current node to the start
	// node of the leafstar traversal.
	toStart maf.Aff
}

// NewLocaliser maps the given function, which expects a localising
// transformation argument, to a function suitable for use in a leafstar
// traversal.
//
// A leafstar traversal used in conjunction with the returned function calls the
// given function for each node: providing the given function with the current
// node and a change of frame transformation that goes from the current node's
// frame to the leafstar traversal's start frame.
func NewLocaliser(
	f func(*Node, maf.Aff),
) func(*Node, leafstar.PreviousStep, LocAcc) LocAcc {

	return func(node *Node, step leafstar.PreviousStep, a LocAcc) LocAcc {

		var toStart maf.Aff

		switch step {

		case leafstar.Down:
			// The traversal went down the tree. The inbound transformation to
			// the start frame goes up the tree, that is, out from its frame to
			// the implied reference (parent) frame.
			toStart = a.toStart.Multiply(node.Out())

		case leafstar.Up:
			// The traversal went up the tree. The inbound transformation to the
			// start frame goes down the tree, that is, in to its child sender
			// frame from this implied reference (parent) frame.
			toStart = a.toStart.Multiply(a.from.In())

		case leafstar.Start:
			fallthrough
		default:
			// The start node of the leafstar traversal. The inbound
			// transformation is the identity.
			toStart = maf.AffIdentity
		}

		f(node, toStart)
		return LocAcc{
			from:    node.Get(),
			toStart: toStart,
		}
	}
}
