package mechtree_test

import (
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
)

func TestNewChangeOfFrame(t *testing.T) {
	r, a, b, c, d := mechtree.NewNode(), mechtree.NewNode(), mechtree.NewNode(),
		mechtree.NewNode(), mechtree.NewNode()
	r.AddChild(a)
	r.AddChild(b)
	b.AddChild(c)
	c.AddChild(d)

	f1 := maf.Frame{
		O:    maf.Vec{X: 1, Y: 1, Z: 1},
		Axes: maf.StdBasis,
	}
	f2 := maf.Frame{
		O:    maf.Vec{X: -1, Y: 1, Z: -1},
		Axes: maf.Basis{X: maf.NegZ, Y: maf.PosY, Z: maf.PosX},
	}

	a.Orient(f2)
	b.Orient(f1)
	c.Orient(f1)
	d.Orient(f2)

	// Test the change of frame transformation between various nodes on the tree
	// by transforming the same point from one to the other.
	p := maf.Vec{X: 1, Y: 1, Z: 1}

	var (
		aff       maf.Aff
		want, got maf.Vec
	)

	aff, _ = mechtree.NewChangeOfFrame(c, b)

	want = maf.Vec{X: 2, Y: 2, Z: 2}
	got = aff.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aff, _ = mechtree.NewChangeOfFrame(b, c)

	want = maf.Vec{X: 0, Y: 0, Z: 0}
	got = aff.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aff, _ = mechtree.NewChangeOfFrame(b, b)

	want = maf.Vec{X: 1, Y: 1, Z: 1}
	got = aff.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aff, _ = mechtree.NewChangeOfFrame(a, b)

	want = maf.Vec{X: -1, Y: 1, Z: -3}
	got = aff.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aff, _ = mechtree.NewChangeOfFrame(b, a)

	want = maf.Vec{X: -3, Y: 1, Z: 3}
	got = aff.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aff, _ = mechtree.NewChangeOfFrame(a, r)

	want = maf.Vec{X: 0, Y: 2, Z: -2}
	got = aff.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aff, _ = mechtree.NewChangeOfFrame(a, c)

	want = maf.Vec{X: -2, Y: 0, Z: -4}
	got = aff.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aff, _ = mechtree.NewChangeOfFrame(a, d)

	want = maf.Vec{X: 3, Y: -1, Z: -1}
	got = aff.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Change the shape of the tree.

	a.AddChild(b)

	aff, _ = mechtree.NewChangeOfFrame(a, d)

	want = maf.Vec{X: 0, Y: -2, Z: 0}
	got = aff.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aff, _ = mechtree.NewChangeOfFrame(d, a)

	want = maf.Vec{X: 2, Y: 4, Z: 0}
	got = aff.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test disjoint tree scenarios.

	c.Detach()

	{
		var (
			err       error
			want, got bool
		)
		_, err = mechtree.NewChangeOfFrame(r, d)
		want = true
		got = err != nil
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		_, err = mechtree.NewChangeOfFrame(a, d)
		want = true
		got = err != nil
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		_, err = mechtree.NewChangeOfFrame(b, d)
		want = true
		got = err != nil
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		_, err = mechtree.NewChangeOfFrame(d, a)
		want = true
		got = err != nil
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestInterframe(t *testing.T) {
	r, a, b, c := mechtree.NewNode(), mechtree.NewNode(), mechtree.NewNode(),
		mechtree.NewNode()
	r.AddChild(a)
	r.AddChild(b)
	b.AddChild(c)

	f1 := maf.Frame{
		O:    maf.Vec{X: -1, Y: 1, Z: -1},
		Axes: maf.Basis{X: maf.NegZ, Y: maf.PosY, Z: maf.PosX},
	}
	f2 := maf.Frame{
		O:    maf.Vec{X: 1, Y: -1, Z: 1},
		Axes: maf.StdBasis,
	}

	a.Orient(f1)
	b.Orient(f2)
	c.Orient(f2)

	// Test the change of frame transformations between various nodes on the
	// tree by transforming the same point from one to the other.
	p := maf.Vec{X: 1, Y: 1, Z: 1}

	cof, _ := mechtree.NewInterframe(a, c)

	want := maf.Vec{X: -2, Y: 4, Z: -4}
	got := cof.AToB.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = p
	got = cof.BToA.Transform(maf.Vec{X: -2, Y: 4, Z: -4})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	cof, _ = mechtree.NewInterframe(c, a)

	want = maf.Vec{X: -4, Y: -2, Z: 4}
	got = cof.AToB.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = p
	got = cof.BToA.Transform(maf.Vec{X: -4, Y: -2, Z: 4})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	cof, _ = mechtree.NewInterframe(r, c)

	want = maf.Vec{X: -1, Y: 3, Z: -1}
	got = cof.AToB.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = p
	got = cof.BToA.Transform(maf.Vec{X: -1, Y: 3, Z: -1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	cof, _ = mechtree.NewInterframe(c, r)

	want = maf.Vec{X: 3, Y: -1, Z: 3}
	got = cof.AToB.Transform(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = p
	got = cof.BToA.Transform(maf.Vec{X: 3, Y: -1, Z: 3})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
