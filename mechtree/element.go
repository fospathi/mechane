package mechtree

import (
	"math"
	"sync"

	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechserver/mechoverlayer"
	"gitlab.com/fospathi/universal/mech"
)

const (
	buttonHalfHeight = "2em"
)

const (
	Top         = OverlayOrigin("top")
	Bottom      = OverlayOrigin("bottom")
	Left        = OverlayOrigin("left")
	Right       = OverlayOrigin("right")
	Center      = OverlayOrigin("center")
	IconCenter  = OverlayOrigin("icon-center") // Useful for a button with an icon.
	TopLeft     = OverlayOrigin("top-left")
	TopRight    = OverlayOrigin("top-right")
	BottomRight = OverlayOrigin("bottom-right")
	BottomLeft  = OverlayOrigin("bottom-left")
)

const (
	// Flat does not apply an orientation to the overlay element, it is always
	// flat on and front facing.
	Flat = OverlayOrientation(iota)
	// XY orientation orients the overlay element so it matches the XY-plane
	// (Z=0) of its in-world frame as seen from the camera. When viewing the
	// element from the rear of the frame you see the same contents as the front
	// face but in an unusual right to left order, making it obvious it is the
	// rear side.
	XY
	// XYFront orientation orients the overlay element so it matches the
	// XY-plane (Z=0) of its in-world frame as seen from the camera, but does
	// not allow the overlay element's rear face to be seen. When viewing the
	// element from the rear of the frame the overlay element is rotated around
	// its in-world frame's Y-axis by 180 degrees.
	XYFront
)

// An OverlayOrientation decides how an overlay element should be oriented on
// the screen.
//
// The element's on-screen orientation can be made to match its in-world
// orientation.
type OverlayOrientation int

// An OverlayOrigin specifies which part of the overlay element coincides with
// the node's onscreen origin.
type OverlayOrigin string

func (oo OverlayOrigin) transformOrigin() string {
	// https://developer.mozilla.org/en-US/docs/Web/CSS/transform-origin

	switch oo {
	case Top:
		return " center top "
	case Bottom:
		return " center bottom "
	case Left:
		return " left center "
	case Right:
		return " right center "
	case IconCenter:
		return " -" + buttonHalfHeight + " center "
	case TopLeft:
		return " top left "
	case TopRight:
		return " top right "
	case BottomRight:
		return " bottom right "
	case BottomLeft:
		return " bottom left "
	}
	return "center center"
}

// A translation to place the element's origin, which by default for HTML
// elements is at the top left, at the desired position relative to the
// element's in-world node origin on the screen.
func (oo OverlayOrigin) transformTranslate() string {
	switch oo {
	case Top:
		return " translate(-50%, 0%) "
	case Bottom:
		return " translate(-50%, -100%) "
	case Left:
		return " translate(0%, -50%) "
	case Right:
		return " translate(-100%, -50%) "
	case IconCenter:
		return " translate(-2em, -50%) "
	case TopLeft:
		return "" // Empty on purpose: translate(0%, 0%) has no effect.
	case TopRight:
		return " translate(-100%, 0%) "
	case BottomRight:
		return " translate(-100%, -100%) "
	case BottomLeft:
		return " translate(0%, -100%) "
	}
	return "translate(-50%, -50%)"
}

// An OverlayElement attached to a node on a tree.
type OverlayElement struct {
	// The HTML of the overlay element.
	HTML string
	// Each overlay element on a tree should have a unique name.
	Name string
	// On-screen orientation of an overlay element relative to its in-world
	// orientation.
	Orientation OverlayOrientation
	// Adjust the on-screen position of an overlay element relative to its node.
	OriginPosition OverlayOrigin

	htmlHash string
}

func (oe OverlayElement) toOverlayer(ori cssOrient) mechoverlayer.Element {
	ndc := ori.localOriginNDC()
	return mechoverlayer.Element{
		InnerHTML:       oe.HTML,
		InnerHTMLHash:   oe.htmlHash,
		Transform:       ori.transform,
		TransformOrigin: ori.transformOrigin,
		X:               (ndc.X / 2) * mech.WToHPixelsRatio,
		Y:               ndc.Y / 2,
		Z:               math.Abs(ori.nodeOrigin.Z),
	}
}

// newElements constructs a new elements.
func newElements() *elements {
	return &elements{
		sync.Mutex{},
		mechoverlayer.Elements{
			Ahead:  map[string]mechoverlayer.Element{},
			Behind: eqset.New[string](),
		},
	}
}

// An elements store used while collecting elements from a tree.
type elements struct {
	sync.Mutex
	mechoverlayer.Elements
}

// Add the element which has the given position to the ahead elements.
//
// The origin position shall be given in terms of the coordinates of the
// camera's own frame. It is assumed the origin position has a negative Z-value.
func (els *elements) addAhead(e OverlayElement, ori cssOrient) {
	els.Lock()
	defer els.Unlock()

	els.Ahead[e.Name] = e.toOverlayer(ori)
}

// Add the element with the given name to the behind elements.
func (els *elements) addBehind(e OverlayElement) {
	els.Lock()
	defer els.Unlock()

	els.Behind.Add(e.Name)
}

// Return the elements collected from the tree.
func (els *elements) complete() mechoverlayer.Elements {
	els.Lock()
	defer els.Unlock()

	return els.Elements
}

type cssOrient struct {
	cam             Camera
	nodeOrigin      maf.Vec // Element's node origin in the camera's local coordinates.
	transform       string
	transformOrigin string
}

func (ori cssOrient) localOriginNDC() maf.Vec {
	return ori.cam.Project(ori.nodeOrigin)
}
