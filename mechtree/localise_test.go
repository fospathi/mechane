package mechtree_test

import (
	"sync"
	"testing"

	"gitlab.com/fospathi/dryad/leafstar"
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/universal/mech"
)

func TestNewLocaliser(t *testing.T) {
	r, a, b, c, d := mechtree.NewNode(), mechtree.NewNode(), mechtree.NewNode(),
		mechtree.NewNode(), mechtree.NewNode()
	r.AddChild(a)
	r.AddChild(b)
	b.AddChild(c)
	c.AddChild(d)

	f1 := maf.Frame{O: maf.Vec{X: 1, Y: 1, Z: 1}, Axes: maf.StdBasis}
	f2 := maf.Frame{O: maf.Vec{X: -1, Y: 1, Z: 1}, Axes: maf.StdBasis}

	a.Orient(f2)
	b.Orient(f1)
	c.Orient(f1)
	d.Orient(f1)

	// Test the localising transformation for each node on the tree by
	// transforming the same point in each node to a specified 'local' node.
	p := maf.Vec{X: 1, Y: 1, Z: 1}

	mm := sync.Mutex{}
	mGot := map[*mechtree.Node]maf.Vec{}
	f := mechtree.NewLocaliser(func(n *mechtree.Node, toLocal maf.Aff) {
		v := toLocal.Transform(p)
		mm.Lock()
		mGot[n] = v
		mm.Unlock()
	})

	// Localise the same point in each node to the root node.
	mWant := map[*mechtree.Node]maf.Vec{
		r: {X: 1, Y: 1, Z: 1},
		a: {X: 0, Y: 2, Z: 2},
		b: {X: 2, Y: 2, Z: 2},
		c: {X: 3, Y: 3, Z: 3},
		d: {X: 4, Y: 4, Z: 4},
	}

	leafstar.Traverse[*mechtree.Node, maf.Frame](
		r, mechtree.LocAcc{}, f)

	{
		want := mWant
		got := mGot
		if !compareNodeToVectorMap(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Localise the same point in each node to the c node.
	mWant = map[*mechtree.Node]maf.Vec{
		r: {X: -1, Y: -1, Z: -1},
		a: {X: -2, Y: 0, Z: 0},
		b: {X: 0, Y: 0, Z: 0},
		c: {X: 1, Y: 1, Z: 1},
		d: {X: 2, Y: 2, Z: 2},
	}

	leafstar.Traverse[*mechtree.Node, maf.Frame](
		c, mechtree.LocAcc{}, f)

	{
		want := mWant
		got := mGot
		if !compareNodeToVectorMap(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

// Implements the Depictioner interface.
type Dpr struct {
	pic  mech.Depiction
	m    *sync.Mutex
	name string
}

func (fig Dpr) Depiction() mech.Depiction {
	fig.m.Lock()
	defer fig.m.Unlock()
	return fig.pic.Copy()
}

func (fig Dpr) Name() string {
	fig.m.Lock()
	defer fig.m.Unlock()
	return fig.name
}

func TestLocaliseDepictions(t *testing.T) {

	// va           => vector of (vertex) attributes.
	// r            => root node
	// a, b, c, ... => non root nodes
	rVA := nextVF32()
	aVA := nextVF32()
	bVA := nextVF32()
	identity := maf.AffIdentity.ToOpenGLMat4x3()

	r := mechtree.NewNode()
	r.AddDepictioner(Dpr{
		pic:  mech.Depiction{VertexAttributes: copyVF32(rVA)},
		m:    &sync.Mutex{},
		name: "fig0",
	})

	// Test the simplest single node case.

	var pic mech.Depiction
	pic = mechtree.LocaliseDepictions(r)
	{
		want := true
		got := pic.Equals(mech.Depiction{
			VertexAttributes: appendMatrixVF32(copyVF32(rVA), identity),
		})
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	a := mechtree.NewNode()
	a.AddDepictioner(Dpr{
		pic:  mech.Depiction{VertexAttributes: copyVF32(aVA)},
		m:    &sync.Mutex{},
		name: "fig1",
	})
	a.Orient(maf.NewFrame(maf.Vec{X: 1, Y: 1, Z: 1}))

	r.AddChild(a)

	// Test two connected nodes, a parent and child.

	pic = mechtree.LocaliseDepictions(r)
	{
		haystack := pic.VertexAttributes
		needle1 := appendMatrixVF32(copyVF32(rVA), identity)
		needle2 := appendMatrixVF32(copyVF32(aVA),
			maf.OpenGLMat4x3{1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1})
		want := true
		got := containsVF32(haystack, needle1) && containsVF32(haystack, needle2)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	b := mechtree.NewNode()
	b.AddDepictioner(Dpr{
		pic:  mech.Depiction{VertexAttributes: copyVF32(bVA)},
		m:    &sync.Mutex{},
		name: "fig2",
	})
	b.Orient(maf.NewFrame(maf.Vec{X: -1, Y: 1, Z: 1}))

	r.AddChild(b)

	// Test a three node tree, a parent with two children.

	pic = mechtree.LocaliseDepictions(b)
	{
		haystack := pic.VertexAttributes
		needle1 := appendMatrixVF32(copyVF32(rVA),
			maf.OpenGLMat4x3{1, 0, 0, 0, 1, 0, 0, 0, 1, 1, -1, -1})
		needle2 := appendMatrixVF32(copyVF32(aVA),
			maf.OpenGLMat4x3{1, 0, 0, 0, 1, 0, 0, 0, 1, 2, 0, 0})
		want := true
		got := containsVF32(haystack, needle1) &&
			containsVF32(haystack, needle2) &&
			containsVF32(haystack, appendMatrixVF32(copyVF32(bVA), identity))
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

// NewFloat32SeqGen splits the whole positive numbers into equal intervals and,
// starting with the interval beginning with 1, generates the next interval each
// time the returned function is called.
func NewFloat32SeqGen(l int) func() []float32 {
	n := 1
	return func() []float32 {
		vf := make([]float32, l)
		for i := 0; i < l; i++ {
			vf[i] = float32(n)
			n++
		}
		return vf
	}
}

var nextVF32 func() []float32 = NewFloat32SeqGen(mech.Float32sNonEye)

func copyVF32(vf []float32) []float32 {
	var res []float32
	res = append(res, vf...)
	return res
}

func containsVF32(haystack []float32, needle []float32) bool {
	for i := 0; i < len(haystack)-(len(needle)-1); i++ {
		for j := 0; j < len(needle); j++ {
			if haystack[i+j] != needle[j] {
				break
			}
			if j == len(needle)-1 {
				return true
			}
		}
	}
	return false
}

func appendMatrixVF32(vf []float32, m maf.OpenGLMat4x3) []float32 {
	for i := 0; i < len(m); i++ {
		vf = append(vf, m[i])
	}
	return vf
}

func compareNodeToVectorMap(m1 map[*mechtree.Node]maf.Vec,
	m2 map[*mechtree.Node]maf.Vec) bool {

	if len(m1) != len(m2) {
		return false
	}
	for k, val := range m1 {
		v, ok := m2[k]
		if !ok || !maf.CompareVec(v, val, 5) {
			return false
		}
	}
	return true
}
