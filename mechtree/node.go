/*
Package mechtree implements a hierarchical tree structure of nodes in 3D space
that ultimately form the visual content of a Mechane world.
*/
package mechtree

import (
	"crypto/sha256"
	"slices"
	"sync"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
)

func NewNode() *Node {
	return &Node{
		Mutex: sync.Mutex{},

		Depictioners: map[string]mech.Depictioner{},
		Frame:        maf.StdFrame,
	}
}

// A Node with its own coordinate system, a coordinate system which is embedded
// in (and specified relative to) the coordinate system of its parent node.
//
// For the root node of a Mechane tree with no parent node the position and
// orientation of its frame are effectively meaningless.
type Node struct {
	sync.Mutex
	Cashpoint    *Node
	Depictioners map[string]mech.Depictioner
	maf.Frame
	Kids           []*Node
	OverlayElement OverlayElement
}

// AddDepictioner to the node.
func (nde *Node) AddDepictioner(dpr mech.Depictioner) {
	nde.Lock()
	defer nde.Unlock()

	nde.Depictioners[dpr.Name()] = dpr
}

// AddDepictioners to the node.
func (nde *Node) AddDepictioners(vd ...mech.Depictioner) {
	nde.Lock()
	defer nde.Unlock()

	for _, d := range vd {
		nde.Depictioners[d.Name()] = d
	}
}

// AddChild to the receiver node, detaching it from its current parent, if any.
//
// Adding a child is not an atomic operation: the detach is done first as a
// separate op.
//
// Nothing happens if the child and receiver are the same node.
func (nde *Node) AddChild(child *Node) {
	if nde == child {
		return
	}

	child.Detach()

	// All dual mutex parent-child ops lock the child first to avoid deadlocks.
	child.Lock()
	defer child.Unlock()

	nde.Lock()
	defer nde.Unlock()

	child.Cashpoint = nde
	nde.Kids = append(nde.Kids, child)
}

// Children of the receiver node.
func (nde *Node) Children() []*Node {
	nde.Lock()
	defer nde.Unlock()

	return nde.Kids
}

// Detach the receiver node from its parent making it the root of its own tree.
func (nde *Node) Detach() {

	// All dual mutex parent-child ops lock the child first to avoid deadlocks.
	nde.Lock()
	defer nde.Unlock()

	if nde.Cashpoint == nil {
		return
	}

	nde.Cashpoint.Lock()
	defer nde.Cashpoint.Unlock()

	defer func() { nde.Cashpoint = nil }()

	if i := slices.Index(nde.Cashpoint.Kids, nde); i != -1 {
		nde.Cashpoint.Kids = slices.Delete(nde.Cashpoint.Kids, i, i+1)
	}
}

// GetElement of the node.
func (nde *Node) GetElement() OverlayElement {
	nde.Lock()
	defer nde.Unlock()

	return nde.OverlayElement
}

// Get the frame of the node.
func (nde *Node) Get() maf.Frame {
	nde.Lock()
	defer nde.Unlock()

	return nde.Frame
}

// IsMe reports whether the argument is the same instance as the receiver.
func (nde *Node) IsMe(node *Node) bool {
	nde.Lock()
	defer nde.Unlock()

	return nde == node
}

// IsRoot reports whether the receiver node has no parent.
func (nde *Node) IsRoot() bool {
	nde.Lock()
	defer nde.Unlock()

	return nde.Cashpoint == nil
}

// Orient the frame of the node.
func (nde *Node) Orient(fr maf.Frame) {
	nde.Lock()
	defer nde.Unlock()

	nde.Frame = fr
}

// Overlay the display area with the given element.
//
// The top left corner of the overlay element will coincide with the origin of
// the node's frame.
func (nde *Node) Overlay(e OverlayElement) {
	nde.Lock()
	defer nde.Unlock()

	h := sha256.New()
	h.Write([]byte(e.HTML))
	e.htmlHash = string(h.Sum(nil))

	nde.OverlayElement = e
}

// Parent of the node which may be nil for a root node.
func (nde *Node) Parent() *Node {
	nde.Lock()
	defer nde.Unlock()

	return nde.Cashpoint
}

// RemoveDepictioner from the node.
func (nde *Node) RemoveDepictioner(dpr mech.Depictioner) {
	nde.Lock()
	defer nde.Unlock()

	delete(nde.Depictioners, dpr.Name())
}

// Set the frame of the node.
func (nde *Node) Set(fr maf.Frame) {
	nde.Lock()
	defer nde.Unlock()

	nde.Frame = fr
}
