package mechtree

import (
	"errors"

	"gitlab.com/fospathi/maf"
)

// NewChangeOfFrame transformation from coordinates in the 'from' frame to
// coordinates in the 'to' frame.
func NewChangeOfFrame(from *Node, to *Node) (maf.Aff, error) {
	if from == to {
		return maf.AffIdentity, nil
	}

	var (
		node   *Node
		vf, vt []*Node // Ancestors of the from and to nodes.
		fl, tl int
	)

	{
		node = from.Parent()
		for node != nil {
			vf = append(vf, node)
			if node == to {
				return cofFromDescendant(append([]*Node{from}, vf...)), nil
			}
			node = node.Parent()
		}
		fl = len(vf)
	}

	{
		node = to.Parent()
	loop:
		for node != nil {
			vt = append(vt, node)
			if node == from {
				return cofToDescendant(append([]*Node{to}, vt...)), nil
			}
			for i := 0; i < fl; i++ {
				if vf[i] == node {
					// A ⅄ shape scenario. Just keep the ⋀ shape parts including
					// the common ancestor.
					vf = vf[:i+1]
					fl = len(vf)
					break loop
				}
			}
			node = node.Parent()
		}
		tl = len(vt)
	}

	if fl == 0 || tl == 0 || vf[fl-1] != vt[tl-1] {
		return maf.Aff{}, errors.New("error: disjoint trees")
	}

	up := cofFromDescendant(append([]*Node{from}, vf...))
	down := cofToDescendant(append([]*Node{to}, vt...))
	return down.Multiply(up), nil
}

// An Interframe has two change of frame transformations, one for each direction
// between two frames on a tree.
type Interframe struct {
	// Transform the coordinates of a point in frame A to its coordinates in
	// frame B.
	AToB maf.Aff
	// Transform the coordinates of a point in frame B to its coordinates in
	// frame A.
	BToA maf.Aff
}

// NewInterframe to change coordinates in either direction between the two given
// nodes on a tree.
func NewInterframe(a *Node, b *Node) (Interframe, error) {
	if a == b {
		return Interframe{maf.AffIdentity, maf.AffIdentity}, nil
	}

	var (
		node   *Node
		va, vb []*Node // Ancestors of the a and b nodes.
		al, bl int
	)

	{
		node = a.Parent()
		for node != nil {
			va = append(va, node)
			if node == b {
				vn := append([]*Node{a}, va...)
				return Interframe{
					AToB: cofFromDescendant(vn),
					BToA: cofToDescendant(vn),
				}, nil
			}
			node = node.Parent()
		}
		al = len(va)
	}

	{
		node = b.Parent()
	loop:
		for node != nil {
			vb = append(vb, node)
			if node == a {
				vn := append([]*Node{b}, vb...)
				return Interframe{
					AToB: cofToDescendant(vn),
					BToA: cofFromDescendant(vn),
				}, nil
			}
			for i := 0; i < al; i++ {
				if va[i] == node {
					// A ⅄ shape scenario. Just keep the ⋀ shape parts including
					// the common ancestor.
					va = va[:i+1]
					al = len(va)
					break loop
				}
			}
			node = node.Parent()
		}
		bl = len(vb)
	}

	if al == 0 || bl == 0 || va[al-1] != vb[bl-1] {
		return Interframe{}, errors.New("error: disjoint trees")
	}

	va = append([]*Node{a}, va...)
	vb = append([]*Node{b}, vb...)
	upA, downA := cofFromDescendant(va), cofToDescendant(va)
	upB, downB := cofFromDescendant(vb), cofToDescendant(vb)
	return Interframe{
		AToB: downB.Multiply(upA),
		BToA: downA.Multiply(upB),
	}, nil
}

// Change of frame transformation from a descendant to an ancestor on a tree.
//
// The order of the given nodes is from descendant to ancestor: the slice starts
// with the descendant and ends with the ancestor.
func cofFromDescendant(vn []*Node) maf.Aff {
	l := len(vn)
	aff := maf.AffIdentity
	for i := 0; i < l-1; i++ {
		aff = vn[i].Get().Out().Multiply(aff)
	}
	return aff
}

// Change of frame transformation to a descendant from an ancestor on a tree.
//
// The order of the given nodes is from descendant to ancestor: the slice starts
// with the descendant and ends with the ancestor.
func cofToDescendant(vn []*Node) maf.Aff {
	l := len(vn)
	aff := maf.AffIdentity
	for i := l - 2; i >= 0; i-- {
		aff = vn[i].Get().In().Multiply(aff)
	}
	return aff
}
