package main

import (
	"image"
	"image/draw"
	"image/png"
	"log"
	"os"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
	"gitlab.com/fospathi/wire/colour"
	"gitlab.com/fospathi/wire/colour/rgb"
	"gitlab.com/fospathi/wire/wiregl"
)

func main() {
	for f := range runApp() {
		f()
	}
}

func runApp() wiregl.MainThread {
	h, w := mech.HPixels, mech.WPixels
	mc, rc := wiregl.NewContext(wiregl.ContextSpec{
		Debug: false, // Try setting this to true if you encounter a problem.
	})
	go func() {
		initRes := <-rc
		if initRes.Err != nil {
			log.Fatal(initRes.Err)
		}
		composeImg(h, w, initRes.Context)
	}()
	return mc
}

// Use the given wiregl context to draw images of a sin curve as seen from
// various positions and then compose those images into a single image.
//
// The image is written to disk.
func composeImg(h, w int, cx *wiregl.Context) {
	defer cx.Close()
	ratio := float64(w) / float64(h)

	type camera struct {
		frame     maf.Frame
		eyeToClip maf.OpenGLMat4
	}

	const vbh = 8.0
	orthoLens := wiregl.NewOrthographicProjection(wiregl.ViewingBox{
		F: 20, N: 0, W: vbh * ratio, H: vbh,
	}).Matrix.ToOpenGLMat4()
	perspLens := wiregl.NewPerspectiveProjection(wiregl.ViewingFrustum{
		Aspect: ratio, FOV: maf.DegToRad(75), F: 20, N: 1,
	}).Matrix.ToOpenGLMat4()

	frontOrtho := camera{
		maf.NewFrame(maf.Vec{X: 0, Y: 0, Z: 6}),
		orthoLens,
	}
	frontPersp := camera{
		maf.NewFrame(maf.Vec{X: 0, Y: 0, Z: 6}),
		perspLens,
	}
	backOrtho := camera{
		maf.NewFrame(maf.Vec{X: 0, Y: 0, Z: -6}).View(maf.Zero),
		orthoLens,
	}
	backPersp := camera{
		maf.NewFrame(maf.Vec{X: 0, Y: 0, Z: -6}).View(maf.Zero),
		perspLens,
	}
	leftDiagOrtho := camera{
		maf.NewFrame(maf.Vec{X: -6, Y: 0, Z: 6}).View(maf.Zero),
		orthoLens,
	}
	leftDiagPersp := camera{
		maf.NewFrame(maf.Vec{X: -6, Y: 0, Z: 6}).View(maf.Zero),
		perspLens,
	}
	rightDiagOrtho := camera{
		maf.NewFrame(maf.Vec{X: 6, Y: 0, Z: 6}).View(maf.Zero),
		orthoLens,
	}
	rightDiagPersp := camera{
		maf.NewFrame(maf.Vec{X: 6, Y: 0, Z: 6}).View(maf.Zero),
		perspLens,
	}
	leftOrtho := camera{
		maf.NewFrame(maf.Vec{X: -6, Y: 0, Z: 0}).View(maf.Zero),
		orthoLens,
	}
	leftPersp := camera{
		maf.NewFrame(maf.Vec{X: -6, Y: 0, Z: 0}).View(maf.Zero),
		perspLens,
	}
	rightOrtho := camera{
		maf.NewFrame(maf.Vec{X: 6, Y: 0, Z: 0}).View(maf.Zero),
		orthoLens,
	}
	rightPersp := camera{
		maf.NewFrame(maf.Vec{X: 6, Y: 0, Z: 0}).View(maf.Zero),
		perspLens,
	}
	topOrtho := camera{
		maf.Frame{
			O:    maf.Vec{X: 0, Y: 6, Z: 0},
			Axes: maf.Basis{X: maf.PosX, Y: maf.NegZ, Z: maf.PosY},
		},
		orthoLens,
	}
	topPersp := camera{
		maf.Frame{
			O:    maf.Vec{X: 0, Y: 6, Z: 0},
			Axes: maf.Basis{X: maf.PosX, Y: maf.NegZ, Z: maf.PosY},
		},
		perspLens,
	}
	bottomOrtho := camera{
		maf.Frame{
			O:    maf.Vec{X: 0, Y: -6, Z: 0},
			Axes: maf.Basis{X: maf.PosX, Y: maf.PosZ, Z: maf.NegY},
		},
		orthoLens,
	}
	bottomPersp := camera{
		maf.Frame{
			O:    maf.Vec{X: 0, Y: -6, Z: 0},
			Axes: maf.Basis{X: maf.PosX, Y: maf.PosZ, Z: maf.NegY},
		},
		perspLens,
	}

	// Draw the sin curve from each of these cameras.
	cams := []camera{
		frontOrtho,
		frontPersp,
		backOrtho,
		backPersp,

		leftDiagOrtho,
		leftDiagPersp,
		rightDiagOrtho,
		rightDiagPersp,

		leftOrtho,
		leftPersp,
		rightOrtho,
		rightPersp,

		topOrtho,
		topPersp,
		bottomOrtho,
		bottomPersp,
	}

	sin := wire.NewSin()
	sin.SetName("sin")
	colGrad, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
	const thickness = 0.04 // Thickness values are a proportion of the display area's height in the range 0..1.
	sin.Depict(mech.Portion{
		In:       interval.PiIn,
		Colourer: colGrad,
		Th:       [2]float64{thickness, thickness},
		Qual:     0,
	})

	r := mechtree.NewNode()
	c := mechtree.NewNode()
	r.AddChild(c)
	r.AddDepictioner(sin)

	var vPix []wiregl.Pixels
	for _, cam := range cams {
		c.Orient(cam.frame)
		scene := wiregl.Scene{
			Depiction: mechtree.LocaliseDepictions(c),
			EyeToClip: cam.eyeToClip,
		}
		vPix = append(vPix, cx.Rasterise(scene))
	}

	// Compose the various sin curve images upon the cells of an imaginary
	// rectangular table superimposed over an empty image.
	n := len(cams)
	dim := 1
	for {
		if n <= dim*dim {
			break
		}
		dim++
	}
	composite := image.NewRGBA(image.Rect(0, 0, dim*w, dim*h))
	for i, pix := range vPix {
		m := pix.Full.ToImage()
		x0, y0 := (i%dim)*w, (i/dim)*h
		r := image.Rect(x0, y0, x0+w, y0+h)
		draw.Draw(composite, r, m, image.Point{}, draw.Src)
	}
	f, _ := os.Create("sin.png")
	defer f.Close()
	png.Encode(f, composite)
}
