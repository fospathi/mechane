package main

import (
	"image"
	"image/draw"
	"image/png"
	"log"
	"os"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
	"gitlab.com/fospathi/wire/colour"
	"gitlab.com/fospathi/wire/colour/rgb"
	"gitlab.com/fospathi/wire/wiregl"
)

func main() {
	for f := range runApp() {
		f()
	}
}

func runApp() wiregl.MainThread {
	h, w := mech.HPixels, mech.WPixels
	mc, rc := wiregl.NewContext(wiregl.ContextSpec{
		Debug: false, // Try setting this to true if you encounter a problem.
	})
	go func() {
		initRes := <-rc
		if initRes.Err != nil {
			log.Fatal(initRes.Err)
		}
		composeFigs(h, w, initRes.Context)
	}()
	return mc
}

// Use the given wiregl context to draw images of wireframe figures as seen from
// various positions and then compose those images into a single image.
//
// The image is written to disk.
func composeFigs(h, w int, cx *wiregl.Context) {
	defer cx.Close()
	ratio := float64(w) / float64(h)

	type camera struct {
		frame     maf.Frame
		eyeToClip maf.OpenGLMat4
	}

	const vbh = 8.0
	frontOrtho := camera{
		maf.NewFrame(maf.Vec{X: 0, Y: 0, Z: 6}),
		wiregl.NewOrthographicProjection(wiregl.ViewingBox{
			F: 20, N: 0, W: vbh * ratio, H: vbh,
		}).Matrix.ToOpenGLMat4(),
	}
	sidePersp := camera{
		maf.NewFrame(maf.Vec{X: 6, Y: 6, Z: 6}).View(maf.Zero),
		wiregl.NewPerspectiveProjection(wiregl.ViewingFrustum{
			Aspect: ratio, FOV: maf.DegToRad(75), F: 20, N: 1,
		}).Matrix.ToOpenGLMat4(),
	}

	// Draw the figures from each of these cameras.
	cams := []camera{frontOrtho, sidePersp}

	colGrad, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
	const thickness = 0.015 // Thickness values are a proportion of the display area's height in the range 0..1.

	cone := wire.NewCone(wire.ConeFiguration{
		R:  4,
		H:  4,
		LC: 5,
		CC: 4,
	})
	cone.Orientation = cone.Orientation.LocalRotPosX(-maf.HalfPi)
	cone.SetName("cone")
	cone.Depict(wire.ConePortion{
		Concentric: []mech.Portion{
			{
				In:       interval.In{maf.QuartPi, maf.TwoPi - maf.QuartPi},
				Colourer: colGrad,
				Th:       [2]float64{thickness, thickness},
				Qual:     0,
			},
		},
		Directrix: []mech.Portion{
			{
				In:       interval.In{maf.QuartPi, maf.TwoPi - maf.QuartPi},
				Colourer: colGrad,
				Th:       [2]float64{thickness, thickness},
				Qual:     0,
			},
		},
		Lateral: []mech.Portion{
			{
				In:       interval.In{maf.QuartPi, maf.TwoPi - maf.QuartPi},
				Colourer: colGrad,
				Th:       [2]float64{thickness, thickness},
				Qual:     0,
			},
		},
	})

	helix := wire.NewHelix(kirv.Helix{
		P: 1,
		R: 2,
	})
	helix.Orientation = helix.Orientation.LocalRotPosY(maf.QuartPi)
	helix.SetName("helix")
	helix.Depict(mech.Portion{
		In:       interval.In{0, maf.TwoPi * 3},
		Colourer: colGrad,
		Th:       [2]float64{thickness, thickness},
		Qual:     0,
	})

	sphere := wire.NewSphere(wire.SphereFiguration{
		R:  2,
		NX: 5, NY: 5, NZ: 5,
	})
	sphere.SetName("sphere")
	sphGrad := colour.NewMonoGrad(rgb.Red.LCh())
	sphere.Depict(wire.SpherePortion{
		X: []mech.Portion{
			{
				In:       interval.PiIn,
				Colourer: sphGrad,
				Th:       [2]float64{thickness / 2, thickness / 2},
				Qual:     0,
			},
		},
		Y: []mech.Portion{
			{
				In:       interval.PiIn,
				Colourer: sphGrad,
				Th:       [2]float64{thickness / 2, thickness / 2},
				Qual:     0,
			},
		},
		Z: []mech.Portion{
			{
				In:       interval.PiIn,
				Colourer: sphGrad,
				Th:       [2]float64{thickness / 2, thickness / 2},
				Qual:     0,
			},
		},
	})

	figures := []mech.Depictioner{
		cone,
		helix,
		sphere,
	}

	r := mechtree.NewNode()
	c := mechtree.NewNode()
	r.AddChild(c)

	var vPix []wiregl.Pixels
	for _, fig := range figures {
		r.AddDepictioner(fig)
		for _, cam := range cams {
			c.Orient(cam.frame)
			scene := wiregl.Scene{
				Depiction: mechtree.LocaliseDepictions(c),
				EyeToClip: cam.eyeToClip,
				Small:     true,
			}
			vPix = append(vPix, cx.Rasterise(scene))
		}
		r.RemoveDepictioner(fig)
	}

	// Compose the various figure images upon the cells of an imaginary
	// rectangular table superimposed over an empty image.
	n := len(vPix)
	dim := 1
	for n > dim*dim {
		dim++
	}

	composite := image.NewRGBA(image.Rect(0, 0, dim*w, dim*h))
	for i, pix := range vPix {
		stitch := cx.Stitch(wiregl.GPUStitch{
			Bottom:     0,
			Left:       0,
			Full8:      pix.Full.Data8,
			Small8:     pix.Small.Data8,
			SmallAlpha: 0.5,
		})
		x0, y0 := (i%dim)*w, (i/dim)*h
		r := image.Rect(x0, y0, x0+w, y0+h)
		draw.Draw(composite, r, stitch.ToImage(), image.Point{}, draw.Src)
	}
	f, _ := os.Create("figures.png")
	defer f.Close()
	png.Encode(f, composite)
}
