package main

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"log"
	"os"
	"time"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
	"gitlab.com/fospathi/wire/colour"
	"gitlab.com/fospathi/wire/colour/rgb"
	"gitlab.com/fospathi/wire/wiregl"
)

func main() {
	for f := range runApp() {
		f()
	}
}

func runApp() wiregl.MainThread {
	mc, rc := wiregl.NewContext(wiregl.ContextSpec{
		Debug: false, // Try setting this to true if you encounter a problem.
	})
	go func() {
		initRes := <-rc
		if initRes.Err != nil {
			log.Fatal(initRes.Err)
		}
		composeCurves(initRes.Context)
	}()
	return mc
}

// Use the given wiregl context to draw images of various curves and then
// compose those images into a single image.
//
// The image is written to disk.
func composeCurves(cx *wiregl.Context) {
	defer cx.Close()
	w, h := mech.WPixels, mech.HPixels
	sw, sh := mech.SmallWPixels, mech.SmallHPixels
	ratio := float64(w) / float64(h)

	type camera struct {
		frame     maf.Frame
		eyeToClip maf.OpenGLMat4
	}

	// Draw the curves as seen from this camera.
	const vbh = 8.0
	frontOrtho := camera{
		maf.NewFrame(maf.Vec{X: 0, Y: 0, Z: 6}),
		wiregl.NewOrthographicProjection(wiregl.ViewingBox{
			F: 20, N: 0, W: vbh * ratio, H: vbh,
		}).Matrix.ToOpenGLMat4(),
	}

	colGrad, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
	const thickness = 0.04 // Thickness values are a proportion of the display area's height in the range 0..1.

	sin := wire.NewSin()
	sin.SetName("sin")
	sin.Depict(mech.Portion{
		In:       interval.PiIn,
		Colourer: colGrad,
		Th:       [2]float64{thickness, thickness},
		Qual:     0,
	})

	cos := wire.NewCos()
	cos.SetName("cos")
	cos.Depict(mech.Portion{
		In:       interval.PiIn,
		Colourer: colGrad,
		Th:       [2]float64{thickness, thickness},
		Qual:     0,
	})

	arc := wire.NewCircle(maf.HalfPi)
	arc.SetName("arc")
	arc.Depict(mech.Portion{
		In:       interval.In{maf.QuartPi, maf.TwoPi - maf.QuartPi},
		Colourer: colGrad,
		Th:       [2]float64{thickness, thickness},
		Qual:     0,
	})

	undividedQuadraticBez := wire.NewQuadraticBezier(maf.Bez{
		A: maf.Vec{X: -4, Y: 0, Z: 0},
		B: maf.Vec{X: 4, Y: 5, Z: 0},
		C: maf.Vec{X: 4, Y: 0, Z: 0},
	})
	undividedQuadraticBez.SetName("undivided-quadratic-bez")
	undividedQuadraticBez.Depict(mech.Portion{
		In:       interval.In{0, 1},
		Colourer: colGrad,
		Th:       [2]float64{thickness, thickness},
		Qual:     0,
	})

	dividedQuadraticBez := wire.NewQuadraticBezier(maf.Bez{
		A: maf.Vec{X: -4, Y: 0, Z: 0},
		B: maf.Vec{X: 4, Y: 5, Z: 0},
		C: maf.Vec{X: 4, Y: 0, Z: 0},
	})
	dividedQuadraticBez.SetName("divided-quadratic-bez")
	dividedQuadraticBez.Depict(mech.Portion{
		In:       interval.In{0, 1},
		Colourer: colGrad,
		Th:       [2]float64{thickness, thickness},
		Qual:     8,
	})

	curves := []mech.Depictioner{
		sin,
		cos,
		arc,
		undividedQuadraticBez,
		dividedQuadraticBez,
	}

	r := mechtree.NewNode()
	c := mechtree.NewNode()
	r.AddChild(c)
	c.Orient(frontOrtho.frame)

	var vPix []wiregl.Pixels
	for _, curve := range curves {
		r.AddDepictioner(curve)
		scene := wiregl.Scene{
			Depiction: mechtree.LocaliseDepictions(c),
			EyeToClip: frontOrtho.eyeToClip,
			Small:     true,
		}
		vPix = append(vPix, cx.Rasterise(scene))
		r.RemoveDepictioner(curve)
	}

	// Compose the images of various curves upon the cells of an imaginary
	// rectangular table superimposed over an empty image.
	n := len(curves)
	dim := 1
	for n > dim*dim {
		dim++
	}

	composite := image.NewRGBA(image.Rect(0, 0, dim*w, dim*h))
	halfTransparent := image.NewAlpha(image.Rect(0, 0, sw, sh))
	for i := 0; i < sw; i++ {
		for j := 0; j < sh; j++ {
			halfTransparent.SetAlpha(i, j, color.Alpha{A: 127})
		}
	}

	cores := 8
	stitcher := wiregl.NewCPUStitcher(cores)
	time.Sleep(time.Second)
	benchmarkStitch := true
	for i, pix := range vPix {
		if benchmarkStitch {
			stitchMethod := 0
			switch stitchMethod {
			case 0: // Custom multi-core stitch.
				start := time.Now()
				stitcher.Stitch(pix.Full, pix.Small, 0.5, 2*pix.Full.Height/3, 0)
				duration := time.Since(start)
				fmt.Printf("stitch cpu@%v: duration (micro-secs): %v\n",
					cores, duration.Microseconds())
				x0, y0 := (i%dim)*w, (i/dim)*h
				r := image.Rect(x0, y0, x0+w, y0+h)
				draw.Draw(composite, r, pix.Full.ToImage(), image.Point{}, draw.Src)
			case 1: // Custom single core stitch. May be slower than Go: RGBA vs faster premultiplied RGB.
				start := time.Now()
				pix.Full.Stitch(pix.Small, 0.5, 2*pix.Full.Height/3, 0)
				duration := time.Since(start)
				fmt.Printf("stitch cpu@1: duration (micro-secs): %v\n",
					duration.Microseconds())
				x0, y0 := (i%dim)*w, (i/dim)*h
				r := image.Rect(x0, y0, x0+w, y0+h)
				draw.Draw(composite, r, pix.Full.ToImage(), image.Point{}, draw.Src)
			case 2: // Golang's image/draw single core stitch.
				full := pix.Full.ToImage()
				x0, y0 := (i%dim)*w, (i/dim)*h
				r := image.Rect(x0, y0, x0+w, y0+h)
				draw.Draw(composite, r, full, image.Point{}, draw.Src)

				small := pix.Small.ToImage()
				r = image.Rect(x0, y0, x0+sw, y0+sh)
				start := time.Now()
				draw.DrawMask(
					composite, r,
					small, image.Point{},
					halfTransparent, image.Point{},
					draw.Over)
				duration := time.Since(start)
				fmt.Printf("image/draw: duration (micro-secs): %v\n",
					duration.Microseconds())
			}
			continue
		}

		// Draw the full size image in the cell.
		full := pix.Full.ToImage()
		x0, y0 := (i%dim)*w, (i/dim)*h
		r := image.Rect(x0, y0, x0+w, y0+h)
		draw.Draw(composite, r, full, image.Point{}, draw.Src)

		// Draw a smaller partially transparent version in the corner of the
		// cell.
		small := pix.Small.ToImage()
		r = image.Rect(x0, y0, x0+sw, y0+sh)
		draw.DrawMask(
			composite, r,
			small, image.Point{},
			halfTransparent, image.Point{},
			draw.Over)
	}
	f, _ := os.Create("curves.png")
	defer f.Close()
	png.Encode(f, composite)
}
