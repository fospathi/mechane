# Drawing curves

This example uses package [wire](https://gitlab.com/fospathi/wire/) to draw curves.

## Dependencies

Package wire depends on [GLFW](https://www.glfw.org/). GLFW has its own dependencies; on Ubuntu for example you need the `libgl1-mesa-dev` and `xorg-dev` packages:

```sh
sudo apt install libgl1-mesa-dev xorg-dev
```

## Run the example

To run the example from a terminal, with the current working directory as this directory, execute the command:
```sh
go run .
```

## Example output

<img src="output.png" alt="curves"
	title="Example output" width="720" height="450" />
