package mechtree

import "gitlab.com/fospathi/maf"

// A Camera which is attached to a tree node and is used to view the contents of
// the tree from that node.
//
// The camera is positioned at the origin of the node and looks down its
// negative Z-axis with the positive Y-axis direction as the 'up' direction.
type Camera struct {
	Inverse maf.Mat4 // Inverse transformation to the lens transformation.
	Lens    maf.Mat4
	Name    string
	Node    *Node
}

// Project the given point, which shall be specified in the coords of the
// camera's frame, to the OpenGL NDC space/cube.
func (c Camera) Project(p maf.Vec) maf.Vec {
	v := c.Lens.Transform(maf.Vec4{X: p.X, Y: p.Y, Z: p.Z, W: 1})
	return maf.Vec{X: v.X, Y: v.Y, Z: v.Z}.Scale(1 / v.W)
}
