module gitlab.com/fospathi/mechane

go 1.21

require (
	github.com/gorilla/websocket v1.5.1
	github.com/shopspring/decimal v1.4.0
	gitlab.com/fospathi/dryad v0.0.0-20231105005730-60ed499ed55b
	gitlab.com/fospathi/maf v0.0.0-20231113084256-78680f1d8255
	gitlab.com/fospathi/mico v0.0.0-20240514143353-07886da084c3
	gitlab.com/fospathi/phyz v0.0.0-20230417003036-46d2e35d15e1
	gitlab.com/fospathi/universal v0.0.0-20230605055348-86065fb86769
	gitlab.com/fospathi/wire v0.0.0-20231105022433-68837325f043
	golang.org/x/exp v0.0.0-20240506185415-9bf2ced13842
	golang.org/x/mod v0.17.0
	nhooyr.io/websocket v1.8.11
)

require (
	github.com/bcmills/unsafeslice v0.2.0 // indirect
	github.com/go-gl/gl v0.0.0-20231021071112-07e5d0ea2e71 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20240506104042-037f3cc74f2a // indirect
	golang.org/x/net v0.25.0 // indirect
)

replace gitlab.com/fospathi/dryad => ../dryad

replace gitlab.com/fospathi/maf => ../maf

replace gitlab.com/fospathi/mico => ../mico

replace gitlab.com/fospathi/phyz => ../phyz

replace gitlab.com/fospathi/wire => ../wire

replace gitlab.com/fospathi/universal => ../universal
