package mechserver_test

import (
	"embed"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"os"
	"path"
	"slices"
	"testing"
	"time"

	"github.com/gorilla/websocket"

	"gitlab.com/fospathi/mechane/about"
	"gitlab.com/fospathi/mechane/mechserver"
	"gitlab.com/fospathi/mechane/mechserver/mechshutterbug"
	"gitlab.com/fospathi/mechane/mechserver/mechuievent"
	"gitlab.com/fospathi/mechane/mechui"
	"gitlab.com/fospathi/mechane/motif"
	"gitlab.com/fospathi/universal/mech"
)

// An example go.mod file.
const goMod = `
module gitlab.com/fospathi/mechane

go 1.19

require (
	github.com/gorilla/websocket v1.5.0
	gitlab.com/fospathi/wire v0.0.0-20220923015417-d3866d8773d7
	golang.org/x/exp v0.0.0-20220927162542-c76eaa363f9d
	golang.org/x/mod v0.6.0-dev.0.20220922195421-2adab6b8c60e
)

require (
	github.com/bcmills/unsafeslice v0.2.0 // indirect
	github.com/go-gl/gl v0.0.0-20211210172815-726fda9656d6 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20220806181222-55e207c401ad // indirect
)
`

//go:embed mechserver.go
var worldweb embed.FS

func TestServer(t *testing.T) {

	abt, err := about.NewMechane([]byte(goMod))
	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}

	{
		// Change directory to the same as Mechane's main package.
		wd, _ := os.Getwd()
		os.Chdir(path.Dir(wd + "/../"))
	}

	root, _ := mechui.Root()
	site := "127.0.0.1"
	mechserver, _, _, _ := mechserver.New(mechserver.ServerSpec{
		Host: mechserver.Host{
			Rasteriser: mech.EmbeddedFlag,
			Server:     site,
		},

		About: about.Software{
			Mechane: abt,
			World:   about.World{},
		},
		Motif:    motif.MetroOrange,
		UIWeb:    root,
		WorldWeb: http.FS(worldweb),
	},
	)

	// Test requesting a shutterbug incorrectly.

	resp, err2 := http.Get(mechserver.URL + mech.ShutterbugPath)
	resp.Body.Close()
	{
		want := true
		got := err2 == nil && resp.StatusCode == 405
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test requesting a shutterbug.

	resp, err2 = http.Post(mechserver.URL+mech.ShutterbugPath, "", nil)
	s, err1 := io.ReadAll(resp.Body)
	resp.Body.Close()
	var shutterbug string
	json.Unmarshal(s, &shutterbug)
	{
		want := true
		got := err2 == nil && err1 == nil && resp.StatusCode == 201 &&
			mechserver.ShutterbugRoll.Contains(shutterbug)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	u := url.URL{Scheme: "ws", Host: mechserver.Address, Path: mech.UIEventPath}

	// Test not sending a valid shutterbug as the first message on the UI events
	// handler.

	c, _, err2 := websocket.DefaultDialer.Dial(u.String(), nil)
	c.WriteJSON("Z")
	err1 = c.ReadJSON(nil)
	{
		want := true
		got := err2 == nil &&
			websocket.IsCloseError(err1, websocket.ClosePolicyViolation) &&
			err1.(*websocket.CloseError).Text ==
				mechshutterbug.ErrMsgExpectedShutterbug
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test sending a shutterbug followed by input events on the UI events
	// handler.

	c, _, _ = websocket.DefaultDialer.Dial(u.String(), nil)
	c.WriteJSON(shutterbug)
	c.WriteMessage(websocket.TextMessage, []byte(`{"keyboard":{"key":"X"}}`))
	time.Sleep(100 * time.Millisecond)
	collected := mechserver.UIEventsCollector.Collections()
	{
		want := true
		got := len(collected) == 1
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
	for _, col := range collected {
		for _, event := range col.Events {
			want := true
			got := (event.E.(mechuievent.KeyboardEvent)).Key == "X"
			if want != got {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}
	}

	// Test a static data request.

	resp, _ = http.Get(mechserver.URL + mech.ElmMotifPath)
	{
		want, _ := json.Marshal(motif.MetroOrange.ToElmMotif())
		got, _ := io.ReadAll(resp.Body)
		if slices.Compare(want, got) != 0 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", string(want), string(got))
		}
	}
	resp.Body.Close()

	// Gracefully close the server.

	c.WriteMessage(websocket.CloseMessage,
		websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	time.Sleep(100 * time.Millisecond)
}
