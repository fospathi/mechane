/*
Package mechshutterbug helps manage shutterbugs which are needed to connect to a
Mechane server.
*/
package mechshutterbug

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"sync"
	"time"

	"gitlab.com/fospathi/universal/mech"
)

// NewRoll constructs a new Roll.
func NewRoll(l *log.Logger) Roll {
	return Roll{
		Logger: l,
		m:      &sync.Mutex{},
		sb:     map[string]struct{}{},
	}
}

// A Roll call of shutterbugs.
type Roll struct {
	Logger *log.Logger
	m      *sync.Mutex
	sb     map[string]struct{}
}

// Contains is true if the given shutterbug is present in the roll.
func (r Roll) Contains(shutterbug string) bool {
	r.m.Lock()
	defer r.m.Unlock()

	return timePad(mech.ShutterbugMinCheckTime, func() interface{} {
		_, ok := r.sb[shutterbug]
		return ok
	}).(bool)
}

// Remove the given shutterbug from the roll.
func (r Roll) Remove(shutterbug string) {
	r.m.Lock()
	defer r.m.Unlock()

	timePad(mech.ShutterbugMinCheckTime, func() interface{} {
		delete(r.sb, shutterbug)
		return nil
	})
}

func (r Roll) addShutterbug(shutterbug string) {
	r.m.Lock()
	defer r.m.Unlock()

	timePad(mech.ShutterbugMinCheckTime, func() interface{} {
		r.sb[shutterbug] = struct{}{}
		return nil
	})
}

// NewShutterbugHandleFunc is a HTTP request handler for creating new
// shutterbugs in the given shutterbug roll call.
func NewShutterbugHandleFunc(roll Roll) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var (
			err        error
			js         []byte
			shutterbug string
		)
		if r.Method != http.MethodPost {
			w.Header().Set("Allow", "POST")
			http.Error(w, "new shutterbug: method error",
				http.StatusMethodNotAllowed)
			return
		}
		if shutterbug, err = newShutterbug(); err != nil {
			msg := "new shutterbug: create error: " + err.Error()
			http.Error(w, msg, http.StatusInternalServerError)
			roll.Logger.Println(msg)
			return
		}
		if js, err = json.Marshal(shutterbug); err != nil {
			msg := "new shutterbug: json marshal error: " + err.Error()
			http.Error(w, msg, http.StatusInternalServerError)
			roll.Logger.Println(msg)
			return
		}
		roll.addShutterbug(shutterbug)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		w.Write(js)
	}
}

// newShutterbug makes a new random shutterbug.
func newShutterbug() (string, error) {
	val := timePad(mech.ShutterbugMinCreationTime, func() interface{} {
		const bitsPerByte = 8
		shutterbug, err :=
			mech.RandBase64String(mech.ShutterbugBits / bitsPerByte)
		if err != nil {
			return err
		}
		return shutterbug
	})
	switch res := val.(type) {
	case error:
		return "", res
	case string:
		return res, nil
	}
	return "", errors.New("unexpected time pad result type")
}

// timePad may help prevent a timing attack, for example one that tries to
// figure out shutterbug IDs char by char.
func timePad(dur time.Duration, f func() interface{}) interface{} {
	done := make(chan struct{})
	go func() {
		time.Sleep(dur)
		close(done)
	}()
	res := f()
	<-done
	return res
}
