package mechshutterbug

import (
	"errors"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/universal/mech"
)

// UpgradeAndVerify a HTTP request to a Mechane websocket that only accepts
// connections from clients that possess a valid shutterbug.
//
// Return an upgraded connection and a shutterbug or else an error.
func UpgradeAndVerify(
	w http.ResponseWriter,
	r *http.Request,
	l *log.Logger,
	n string, // A descriptive name prefixed to log entries.
	shutterbugs Containser,
) (*websocket.Conn, string, error) {
	u := websocket.Upgrader{}
	c, err := u.Upgrade(w, r, nil)
	if err != nil {
		logFailure(l, n, "upgrade", err)
		return c, "", err
	}

	var shutterbug string
	{
		errC := make(chan error, 1)
		go func() {
			errC <- c.ReadJSON(&shutterbug)
		}()
		select {
		case <-time.NewTimer(mech.ShutterbugTimeout).C:
			logFailure(l, n, "timeout: "+r.RemoteAddr)
			mech.PolicyViolation(c, ErrMsgTimeout)
			c.Close()
			return c, "", errors.New("timeout")
		case err = <-errC:
			if err != nil {
				logFailure(l, n, "readJSON", err)
				mech.PolicyViolation(c, ErrMsgExpectedJSONString)
				c.Close()
				return c, "", err
			}
			if !shutterbugs.Contains(shutterbug) {
				logFailure(l, n, "no such shutterbug: "+r.RemoteAddr)
				mech.PolicyViolation(c, ErrMsgExpectedShutterbug)
				c.Close()
				return c, "", errors.New("no such shutterbug")
			}
		}
	}

	return c, shutterbug, nil
}

// A Containser is a container of shutterbugs.
type Containser interface {
	Contains(string) bool
}

func logFailure(l *log.Logger, prefix, midfix string, ve ...error) {
	if len(ve) > 0 {
		l.Printf("%v: shutterbug: %v: %v\n", prefix, midfix, ve)
		return
	}
	l.Printf("%v: shutterbug: %v\n", prefix, midfix)
}

// The text sent to the client in a websocket close error.
const (
	ErrMsgExpectedJSONString = "expected JSON string"
	ErrMsgExpectedShutterbug = "expected extant shutterbug as the first message"
	ErrMsgTimeout            = "timeout"
)
