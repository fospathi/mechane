package mechshutterbug

import (
	"bytes"
	"log"
	"net"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/gorilla/websocket"
)

// TestUpgradeAndVerify tests a HTTP request handler that upgrades to websocket
// and expects to receive a valid shutterbug as its first incoming message.
func TestUpgradeAndVerify(
	t *testing.T,
	u url.URL, // The URL of the handler to test.
	l *log.Logger, // A logger set up to log its output to the argument empty log buffer.
	logBuf *bytes.Buffer,
	absent string, // A nonexistent shutterbug.
) {

	// Test sending a shutterbug which does not exist.

	c, _, _ := websocket.DefaultDialer.Dial(u.String(), nil)
	c.WriteJSON(absent)
	err := c.ReadJSON(nil)
	logBufLen := logBuf.Len()

	{
		want1, want2 := websocket.ClosePolicyViolation, ErrMsgExpectedShutterbug
		got1, got2 := err.(*websocket.CloseError).Code,
			err.(*websocket.CloseError).Text

		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}

		want := true
		got := logBufLen > 0
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test sending an incorrect type as the first message.

	c, _, _ = websocket.DefaultDialer.Dial(u.String(), nil)
	c.WriteMessage(websocket.TextMessage, []byte("3"))
	err = c.ReadJSON(nil)

	{
		want1, want2 := websocket.ClosePolicyViolation, ErrMsgExpectedJSONString
		got1, got2 := err.(*websocket.CloseError).Code,
			err.(*websocket.CloseError).Text

		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}

		want := true
		got := logBuf.Len() > logBufLen
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	logBufLen = logBuf.Len()

	// Test not sending a first message.

	c, _, _ = websocket.DefaultDialer.Dial(u.String(), nil)
	err = c.ReadJSON(nil)

	{
		want1, want2 := websocket.ClosePolicyViolation, ErrMsgTimeout
		got1, got2 := err.(*websocket.CloseError).Code,
			err.(*websocket.CloseError).Text

		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}

		want := true
		got := logBuf.Len() > logBufLen
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

// NewTestServer sets up a new server to test the given handler which handles
// requests on the given path.
//
// Return the URL of the handler.
func NewTestServer(path string, handler http.HandlerFunc) url.URL {
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		log.Fatal(err)
	}

	mux := http.NewServeMux()
	mux.HandleFunc(path, handler)

	var httpSvr http.Server
	httpSvr.Addr = listener.Addr().String()
	httpSvr.Handler = mux

	go func() {
		if err := httpSvr.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()

	time.Sleep(100 * time.Millisecond)

	u := url.URL{Scheme: "ws", Host: httpSvr.Addr, Path: path}

	return u
}
