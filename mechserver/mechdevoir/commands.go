package mechdevoir

import (
	"bytes"
	"encoding/json"
	"errors"
)

/*
User interface commands sent from a client to a server.
*/

// PauseUIC is sent by a client to pause the world.
type PauseUIC struct {
	UICommand string
}

// PlayUIC is sent by a client to un-pause the world.
type PlayUIC struct {
	UICommand string
}

// ShutdownUIC is sent by a client to shutdown the world.
type ShutdownUIC struct {
	UICommand string
}

func NewPauseUIC() PauseUIC {
	return PauseUIC{
		UICommand: pause,
	}
}

func NewPlayUIC() PlayUIC {
	return PlayUIC{
		UICommand: play,
	}
}

func NewShutdownUIC() ShutdownUIC {
	return ShutdownUIC{
		UICommand: shutdown,
	}
}

type simpleUIC struct {
	UICommand string
}

const (
	pause    = "pause"
	play     = "play"
	shutdown = "shutdown"
)

func unmarshalUICommand(jsonData string) (interface{}, error) {

	decode := func(uic interface{}) error {
		dec := json.NewDecoder(bytes.NewReader([]byte(jsonData)))
		dec.DisallowUnknownFields()
		return dec.Decode(uic)
	}

	uic := &simpleUIC{}

	if err := decode(uic); err != nil {
		return nil, err
	}

	switch uic.UICommand {
	case pause:
		return PauseUIC{UICommand: pause}, nil
	case play:
		return PlayUIC{UICommand: play}, nil
	case shutdown:
		return ShutdownUIC{UICommand: shutdown}, nil
	}

	return nil, errors.New("no matching UI command found")
}
