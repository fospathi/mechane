/*
Package mechdevoir collects certain high level UI commands from clients and
distributes some associated world state affected by these commands.

The semantic meaning of these UI commands is already standardised and known
before they are sent to the server. Examples include pause, play, and shutdown.
The UI elements for these commands will normally be visually distinct from a
Mechane world's own UI elements, for example this may be accomplished by
locating them on an exclusive sidebar.

UI events such as keyboard key events, mouse move events, and ShutterbugPixels'
overlayer clicks are dealt with by a different package.
*/
package mechdevoir

import (
	"log"
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/mechane/mechserver/mechshutterbug"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/universal/watchable"
)

// InitialState specifies the initial world state used by the Devoir.
type InitialState struct {
	IsPaused bool
}

// NewDevoir constructs a new Devoir.
func NewDevoir(l *log.Logger, spec InitialState) *Devoir {
	return &Devoir{
		Logger:     l,
		m:          sync.Mutex{},
		q:          watchable.NewBoundedQ(mech.DevoirCmdCap),
		recs:       map[recipient]struct{}{},
		worldState: worldState(spec),
	}
}

// A small subset of a Mechane world's worldwide state of interest to the UI.
type worldState struct {
	IsPaused bool `json:"isPaused"`
}

// A Devoir collects certain UI commands from clients and also distrubutes some
// world state events to clients.
type Devoir struct {
	Logger *log.Logger

	m          sync.Mutex
	q          *watchable.BoundedQ    // Command queue.
	recs       map[recipient]struct{} // The recipients/clients of world state updates.
	worldState worldState
}

type UICommand struct {
	Shutterbug string      // The command originated from a client using this shutterbug.
	Value      interface{} // An instance of one of the command types.
}

// A recipient of world state information.
type recipient struct {
	wsc        chan worldState // Closed by the collector end.
	done       <-chan struct{} // Closed by the web handler end on handler exit.
	shutterbug string
}

// Paused lets clients know the new play/pause state is paused.
func (dvr *Devoir) Paused() {
	dvr.m.Lock()
	defer dvr.m.Unlock()

	if dvr.worldState.IsPaused {
		return
	}

	dvr.worldState.IsPaused = true
	dvr.updateRecipients()
}

// Playing lets clients know the new play/pause state is playing.
func (dvr *Devoir) Playing() {
	dvr.m.Lock()
	defer dvr.m.Unlock()

	if !dvr.worldState.IsPaused {
		return
	}

	dvr.worldState.IsPaused = false
	dvr.updateRecipients()
}

// Pop a UI command event from the front of the commands queue.
func (dvr *Devoir) Pop() (UICommand, error) {
	dvr.m.Lock()
	defer dvr.m.Unlock()

	v, err := dvr.q.Pop()
	if err != nil {
		return UICommand{}, err
	}
	return v.(UICommand), nil
}

// RemoveShutterbug from the devoir closing the associated HTTP request handlers
// using that shutterbug.
func (dvr *Devoir) RemoveShutterbug(shutterbug string) {
	dvr.m.Lock()
	defer dvr.m.Unlock()

	for rec := range dvr.recs {
		if rec.shutterbug == shutterbug {
			close(rec.wsc)
			delete(dvr.recs, rec)
		}
	}
}

// addRecipient to the devoir and return the new recipient which gets updated
// with the world state any time there is a change. Return the current world
// state.
//
// After the argument channel is closed no more updates are sent to the
// recipient and it is eventually removed from the devoir.
func (dvr *Devoir) addRecipient(
	done <-chan struct{},
	shutterbug string,
) (recipient, worldState) {

	dvr.m.Lock()
	defer dvr.m.Unlock()

	rec := recipient{
		done: done, wsc: make(chan worldState), shutterbug: shutterbug}
	dvr.recs[rec] = struct{}{}
	return rec, dvr.worldState
}

func (dvr *Devoir) enqueue(uic UICommand) {
	dvr.m.Lock()
	defer dvr.m.Unlock()

	dvr.q.Add(uic)
}

func (dvr *Devoir) updateRecipients() {
	for rec := range dvr.recs {
		select {
		case <-rec.done:
			delete(dvr.recs, rec)
		case rec.wsc <- dvr.worldState:
		}
	}
}

// NewDevoirHandleFunc creates a HTTP request handler for receiving UI commands
// and sending out some world state useful to the UI.
//
// The handler upgrades the HTTP request to a websocket. The websocket's first
// ever incoming message should be a valid shutterbug.
func NewDevoirHandleFunc(
	dvr *Devoir,
	shutterbugs mechshutterbug.Containser,
) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		l := dvr.Logger
		c, shutterbug, err := mechshutterbug.UpgradeAndVerify(
			w, r, l, "devoir", shutterbugs)
		if err != nil {
			return
		}
		defer c.Close()

		rDone, wDone := make(chan struct{}), make(chan struct{})

		go func() {
			defer close(rDone)
			for {
				mt, p, err := c.ReadMessage()
				if err != nil {
					if !websocket.IsCloseError(
						err,
						websocket.CloseNormalClosure,
						websocket.CloseGoingAway,
					) {
						l.Println("devoir: ReadMessage: ", err)
					}
					break
				}
				if mt != websocket.TextMessage {
					l.Println("devoir: ReadMessage: ",
						"expected a text type websocket message")
					break
				}
				val, err := unmarshalUICommand(string(p))
				if err != nil {
					l.Println("devoir: unmarshalCommand: ", err)
					break
				}
				dvr.enqueue(UICommand{
					Shutterbug: shutterbug,
					Value:      val,
				})
			}
		}()

		go func() {
			defer close(wDone)
			rec, wst := dvr.addRecipient(wDone, shutterbug)
			if err := sendWorldState(c, l, wst); err != nil {
				return
			}

			for {
				select {
				case <-rDone:
					return
				case wst, ok := <-rec.wsc:
					if !ok {
						return
					} else if err := sendWorldState(c, l, wst); err != nil {
						return
					}
				}
			}
		}()

		select {
		case <-rDone:
		case <-wDone:
		}
	}
}

func sendWorldState(c *websocket.Conn, l *log.Logger, wst worldState) error {
	if err := c.WriteJSON(wst); err != nil {
		if err != websocket.ErrCloseSent {
			l.Println("devoir: update: writeJSON: ", err)
		}
		return err
	}
	return nil
}
