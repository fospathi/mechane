package mechdevoir_test

import (
	"bytes"
	"log"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/mechane/mechserver/mechdevoir"
	"gitlab.com/fospathi/mechane/mechserver/mechshutterbug"
	"gitlab.com/fospathi/universal/mech"
)

func TestDevoir(t *testing.T) {
	logBuf := &bytes.Buffer{}
	l := log.New(logBuf, "mechane ", log.LstdFlags)
	dvr := mechdevoir.NewDevoir(l, mechdevoir.InitialState{})

	// Set up a test server.

	shutterbugs := eqset.New[string]()
	u := mechshutterbug.NewTestServer(
		mech.DevoirPath, mechdevoir.NewDevoirHandleFunc(dvr, shutterbugs))

	// Do the standard tests for a shutterbug verifying websocket.

	mechshutterbug.TestUpgradeAndVerify(t, u, l, logBuf, "shutterbug1")

	// Add a shutterbug.

	shutterbugs.Add("shutterbug2")

	// Test the initial world state.

	c, _, _ := websocket.DefaultDialer.Dial(u.String(), nil)
	c.WriteJSON("shutterbug2")
	mt, p, err := c.ReadMessage()

	{
		want1 := error(nil)
		got1 := err
		want2 := "{\"isPaused\":false}\n"
		got2 := string(p)
		want3 := websocket.TextMessage
		got3 := mt
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\n"+
				"Want2:\n%#v\nGot2:\n%#v\n"+
				"Want3:\n%#v\nGot3:\n%#v\n",
				want1, got1, want2, got2, want3, got3)
		}
	}

	// Test sending a pause command.

	c.WriteMessage(websocket.TextMessage, []byte("{\"uiCommand\": \"pause\"}"))
	time.Sleep(100 * time.Millisecond)

	{
		uic, err := dvr.Pop()
		want1 := error(nil)
		got1 := err
		want2 := true
		_, got2 := uic.Value.(mechdevoir.PauseUIC)
		want3 := "shutterbug2"
		got3 := uic.Shutterbug
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\n"+
				"Want2:\n%#v\nGot2:\n%#v\n"+
				"Want3:\n%#v\nGot3:\n%#v\n",
				want1, got1, want2, got2, want3, got3)
		}
	}

	// Test telling the client that the state is now paused.

	dvr.Paused()
	mt, p, err = c.ReadMessage()

	{
		want1 := error(nil)
		got1 := err
		want2 := "{\"isPaused\":true}\n"
		got2 := string(p)
		want3 := websocket.TextMessage
		got3 := mt
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\n"+
				"Want2:\n%#v\nGot2:\n%#v\n"+
				"Want3:\n%#v\nGot3:\n%#v\n",
				want1, got1, want2, got2, want3, got3)
		}
	}

	// Test sending a shutdown command.

	uic := mechdevoir.NewShutdownUIC()
	c.WriteJSON(uic)
	time.Sleep(100 * time.Millisecond)

	{
		uic, err := dvr.Pop()
		want1 := error(nil)
		got1 := err
		want2 := true
		_, got2 := uic.Value.(mechdevoir.ShutdownUIC)
		want3 := "shutterbug2"
		got3 := uic.Shutterbug
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\n"+
				"Want2:\n%#v\nGot2:\n%#v\n"+
				"Want3:\n%#v\nGot3:\n%#v\n",
				want1, got1, want2, got2, want3, got3)
		}
	}
}
