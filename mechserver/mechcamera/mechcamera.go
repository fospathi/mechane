/*
Package mechcamera distributes available camera names along with
shutterbug-to-camera associations to clients and receives camera association
requests from clients.
*/
package mechcamera

import (
	"log"
	"net/http"
	"slices"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/mechane/mechserver/mechshutterbug"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/universal/watchable"
)

// NewRegistry constructs a new Registry.
func NewRegistry(l *log.Logger) Registry {
	return Registry{
		Logger: l,
		a:      map[string]Associations{},
		c:      eqset.New[string](),
		m:      &sync.Mutex{},
		q:      watchable.NewBoundedQ(mech.CameraAssocReqCap),
		recs:   map[recipient]struct{}{},
	}
}

// Registry is a register of currently available cameras by name and shutterbug
// to camera associations.
//
// When the registered cameras or associated cameras state changes the new state
// is sent out to clients connected to the registry.
//
// It also receives the new shutterbug/camera association requests from the
// connected clients and stores them in a queue.
type Registry struct {
	Logger *log.Logger

	a    map[string]Associations // Map of shutterbugs to associated cameras.
	c    dryad.Set[string]       // The registered cameras.
	m    *sync.Mutex
	q    *watchable.BoundedQ    // Association requests queue.
	recs map[recipient]struct{} // The recipients/clients of registered-cameras states.
}

// A recipient of registered cameras and shutterbug to camera associations.
type recipient struct {
	rac        chan RegAsc     // Closed by the registry end.
	done       <-chan struct{} // Closed by the web handler end on handler exit.
	shutterbug string
}

// AllAssociatedCameras to any shutterbug.
func (r Registry) AllAssociatedCameras() []string {
	r.m.Lock()
	defer r.m.Unlock()

	res := eqset.New[string]()
	for _, a := range r.a {
		res.AddAll(a.Cameras...)
	}
	return res.Elements()
}

// AllAssociatedShutterbugs to any camera.
func (r Registry) AllAssociatedShutterbugs() []string {
	r.m.Lock()
	defer r.m.Unlock()

	res := eqset.New[string]()
	for shutterbug, a := range r.a {
		if len(a.Cameras) > 0 {
			res.Add(shutterbug)
		}
	}
	return res.Elements()
}

// Associate a shutterbug with the given cameras overwriting any existing
// associations.
//
// Return false and do nothing if any of the given cameras are not registered.
func (r Registry) Associate(a ShutterbugAssociations) bool {
	r.m.Lock()
	defer r.m.Unlock()

	if !r.c.ContainsAll(a.Cameras...) {
		return false
	}

	r.a[a.Shutterbug] = Associations{
		Cameras:   slices.Clone(a.Cameras),
		Stitching: a.Stitching,
	}
	r.updateShutterbugRecipients(a.Shutterbug)

	return true
}

// Associated cameras to the shutterbug.
func (r Registry) Associated(shutterbug string) []string {
	r.m.Lock()
	defer r.m.Unlock()

	a, ok := r.a[shutterbug]
	if !ok {
		return nil
	}
	return slices.Clone(a.Cameras)
}

// AssociatedShutterbugs is the shutterbugs associated with the camera.
func (r Registry) AssociatedShutterbugs(camera string) []string {
	r.m.Lock()
	defer r.m.Unlock()

	if !r.c.Contains(camera) {
		return nil
	}
	var res []string
	for shutterbug, a := range r.a {
		if slices.Contains(a.Cameras, camera) {
			res = append(res, shutterbug)
		}
	}
	return res
}

// Associations of the shutterbug if the shutterbug has any. Return false if
// there are no associations.
func (r Registry) Associations(shutterbug string) (Associations, bool) {
	r.m.Lock()
	defer r.m.Unlock()

	a, ok := r.a[shutterbug]
	if !ok {
		return Associations{}, false
	}
	if len(a.Cameras) == 0 {
		return Associations{}, false
	}

	return Associations{
		Cameras:   slices.Clone(a.Cameras),
		Stitching: a.Stitching,
	}, true
}

// Contains is true if the given camera is registered.
func (r Registry) Contains(camera string) bool {
	r.m.Lock()
	defer r.m.Unlock()

	return r.c.Contains(camera)
}

// Deregister the cameras with the given names and disassociate them from any
// shutterbugs.
func (r Registry) Deregister(cameras ...string) {
	r.m.Lock()
	defer r.m.Unlock()

	if len(cameras) == 0 {
		return
	}
	l := r.c.Len()
	defer func() {
		if l > r.c.Len() {
			r.updateAllRecipients()
		}
	}()

	for _, c := range cameras {
		if !r.c.Contains(c) {
			continue
		}
		r.c.Delete(c)
		for shutterbug, a := range r.a {
			r.disassociate(shutterbug, a, c)
		}
	}
}

func (r Registry) disassociate(
	shutterbug string, a Associations, c string,
) bool {

	if !slices.Contains(a.Cameras, c) {
		return false
	}

	// Reset the stitching after a cam removal.
	var furthest []string
	for _, cam := range a.Cameras {
		if cam != c {
			furthest = append(furthest, cam)
			break
		}
	}
	r.a[shutterbug] = Associations{
		Cameras:   furthest,
		Stitching: Uni,
	}

	return true
}

// Disassociate the given camera from any associated shutterbugs.
func (r Registry) Disassociate(camera string) {
	r.m.Lock()
	defer r.m.Unlock()

	if len(camera) == 0 {
		return
	}

	var toUpdate []string
	for shutterbug, a := range r.a {
		if r.disassociate(shutterbug, a, camera) {
			toUpdate = append(toUpdate, shutterbug)
		}
	}
	r.updateShutterbugRecipients(toUpdate...)
}

// DisassociateShutterbug clears the given shutterbug's associations.
func (r Registry) DisassociateShutterbug(shutterbug string) {
	r.m.Lock()
	defer r.m.Unlock()

	delete(r.a, shutterbug)
	r.updateShutterbugRecipients(shutterbug)
}

// IsPartial is true if the given camera is associated with any shutterbug in a
// partial capacity, that is, it is placed non first in any shutterbug's list of
// associated cameras and the association uses a partial stitching.
func (r Registry) IsPartial(camera string) bool {
	r.m.Lock()
	defer r.m.Unlock()

	for _, a := range r.a {
		if len(a.Cameras) > 0 && a.IsPartial(camera) {
			return true
		}
	}

	return false
}

// Pop an association request from the front of the association requests queue.
func (r Registry) Pop() (ShutterbugAssociations, error) {
	r.m.Lock()
	defer r.m.Unlock()

	v, err := r.q.Pop()
	if err != nil {
		return ShutterbugAssociations{}, err
	}
	return v.(ShutterbugAssociations), nil
}

// Register the cameras with the given names.
func (r Registry) Register(cameras ...string) {
	r.m.Lock()
	defer r.m.Unlock()

	l := r.c.Len()
	for _, camera := range cameras {
		if len(camera) == 0 {
			continue
		}
		r.c.Add(camera)
	}
	if l < r.c.Len() {
		r.updateAllRecipients()
	}
}

// Registered cameras.
func (r Registry) Registered() []string {
	r.m.Lock()
	defer r.m.Unlock()

	return r.c.Elements()
}

// RemoveShutterbug from the registry disassociating it from any cameras and
// closing the associated HTTP request handlers using that shutterbug.
func (r Registry) RemoveShutterbug(shutterbug string) {
	r.m.Lock()
	defer r.m.Unlock()

	for rec := range r.recs {
		if rec.shutterbug == shutterbug {
			close(rec.rac)
			delete(r.recs, rec)
		}
	}
	delete(r.a, shutterbug)
}

// Watch the association request queue.
//
// The returned watch channel is closed already if the queue is not empty, else
// it is closed when the next association request is added.
func (r Registry) Watch() <-chan struct{} {
	r.m.Lock()
	defer r.m.Unlock()

	return r.q.Watch()
}

// addRecipient to the register and return the newly added recipient.
//
// A recipient gets updated with any pertinent registry changes.
//
// After the argument channel is closed no more updates are sent to the
// recipient and it is eventually removed from the register.
func (r Registry) addRecipient(
	done <-chan struct{},
	shutterbug string,
	send func(RegAsc) error,
) (recipient, error) {

	r.m.Lock()
	defer r.m.Unlock()

	rec := recipient{
		done: done, rac: make(chan RegAsc), shutterbug: shutterbug,
	}
	r.recs[rec] = struct{}{}

	var (
		associated []string
		stitching  = Uni
	)
	if a, ok := r.a[shutterbug]; ok {
		associated = slices.Clone(a.Cameras)
		stitching = a.Stitching
	}
	err := send(RegAsc{
		Associated: associated,
		Registered: r.c.Elements(),
		Stitching:  stitching.String(),
	})

	return rec, err
}

func (r Registry) enqueue(a ShutterbugAssociations) {
	r.m.Lock()
	defer r.m.Unlock()

	// TODO process/log error if queue is full and association is lost.
	r.q.Add(a)
}

func (r Registry) registered(shutterbug string) RegAsc {
	var (
		associated []string
		stitching  = Uni
	)
	if a, ok := r.a[shutterbug]; ok {
		associated = slices.Clone(a.Cameras)
		stitching = a.Stitching
	}
	return RegAsc{
		Associated: associated,
		Registered: r.c.Elements(),
		Stitching:  stitching.String(),
	}
}

func (r Registry) updateAllRecipients() {
	for rec := range r.recs {
		select {
		case <-rec.done:
			delete(r.recs, rec)
		case rec.rac <- r.registered(rec.shutterbug):
		}
	}
}

func (r Registry) updateShutterbugRecipients(shutterbugs ...string) {
	if len(shutterbugs) == 0 {
		return
	}
	for rec := range r.recs {
		if slices.Contains(shutterbugs, rec.shutterbug) {
			select {
			case <-rec.done:
				delete(r.recs, rec)
			case rec.rac <- r.registered(rec.shutterbug):
			}
		}
	}
}

// NewCameraHandleFunc creates a HTTP request handler which allows clients to
// interface with the camera registry.
//
// The handler upgrades the HTTP request to a websocket. The websocket's first
// ever incoming message should be a valid shutterbug.
func NewCameraHandleFunc(
	reg Registry,
	shutterbugs mechshutterbug.Containser,
) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		l := reg.Logger
		c, shutterbug, err := mechshutterbug.UpgradeAndVerify(
			w, r, l, "camera", shutterbugs)
		if err != nil {
			return
		}
		defer c.Close()

		rDone, wDone := make(chan struct{}), make(chan struct{})

		go func() {
			defer close(rDone)
			var (
				a   AssociationsSchema
				err error
				s   Stitching
			)
			for {
				if err = c.ReadJSON(&a); err != nil {
					if !websocket.IsCloseError(
						err,
						websocket.CloseNormalClosure,
						websocket.CloseGoingAway,
					) {
						l.Println("camera: association: readJSON: ", err)
					}
					break
				}
				if s, err = StitchingFromString(a.Stitching); err != nil {
					l.Println("camera: association: stitching: ", err)
					break
				}
				if len(a.Cameras) != s.ExpectedCams() {
					l.Println("camera: association: stitching length: ", err)
					break
				}

				reg.enqueue(ShutterbugAssociations{
					Cameras:    a.Cameras,
					Shutterbug: shutterbug,
					Stitching:  s,
				})
			}
		}()

		go func() {
			defer close(wDone)
			rec, err := reg.addRecipient(
				wDone,
				shutterbug,
				func(ra RegAsc) error {
					return sendRegAsc(c, l, ra)
				},
			)
			if err != nil {
				return
			}

			for {
				select {
				case <-rDone:
					return
				case ra, ok := <-rec.rac:
					if !ok {
						return
					} else if err := sendRegAsc(c, l, ra); err != nil {
						return
					}
				}
			}
		}()

		select {
		case <-rDone:
		case <-wDone:
		}
	}
}

type RegAsc struct {
	Associated []string `json:"associated"`
	Registered []string `json:"registered"`
	Stitching  string   `json:"stitching"`
}

var empty = []string{} // WriteJSON doesn't like a nil argument!

func sendRegAsc(c *websocket.Conn, l *log.Logger, ra RegAsc) error {
	if len(ra.Associated) == 0 {
		ra.Associated = empty
	}
	if len(ra.Registered) == 0 {
		ra.Registered = empty
	}
	if err := c.WriteJSON(ra); err != nil {
		if err != websocket.ErrCloseSent {
			l.Println("camera: update: writeJSON: ", err)
		}
		return err
	}
	return nil
}
