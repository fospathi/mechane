package mechcamera_test

import (
	"bytes"
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/mechane/mechserver/mechcamera"
	"gitlab.com/fospathi/mechane/mechserver/mechshutterbug"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/universal/watchable"
)

func TestRegistry(t *testing.T) {
	logBuf := &bytes.Buffer{}
	l := log.New(logBuf, "mechane ", log.LstdFlags)
	reg := mechcamera.NewRegistry(l)

	// Test some registry methods on an empty registry.

	{
		for i, cams := range [][]string{
			reg.AllAssociatedCameras(), reg.Associated(""), reg.Registered(),
			reg.AssociatedShutterbugs(""),
		} {
			want := 0
			got := len(cams)
			if want != got {
				t.Errorf("\ni:\n%v\nWant:\n%#v\nGot:\n%#v\n", i, want, got)
			}
		}

		reg.Register("")

		for i, cams := range [][]string{
			reg.AllAssociatedCameras(), reg.Associated(""), reg.Registered(),
			reg.AssociatedShutterbugs(""),
		} {
			want := 0
			got := len(cams)
			if want != got {
				t.Errorf("\ni:\n%v\nWant:\n%#v\nGot:\n%#v\n", i, want, got)
			}
		}
	}

	{
		want := false
		got := reg.Associate(mechcamera.ShutterbugAssociations{
			Shutterbug: "shutterbug1",
			Cameras:    []string{"cam1"},
		}) || reg.Contains("")
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		for i, cams := range [][]string{
			reg.AllAssociatedCameras(), reg.Associated(""), reg.Registered(),
			reg.AssociatedShutterbugs(""),
		} {
			want := 0
			got := len(cams)
			if want != got {
				t.Errorf("\ni:\n%v\nWant:\n%#v\nGot:\n%#v\n", i, want, got)
			}
		}
	}

	{
		_, err := reg.Pop()
		want := true
		got := watchable.IsEmptyError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Set up a test server.

	shutterbugs := eqset.New[string]()
	u := mechshutterbug.NewTestServer(
		mech.CameraPath, mechcamera.NewCameraHandleFunc(reg, shutterbugs))

	// Do the standard tests for a shutterbug verifying websocket.

	mechshutterbug.TestUpgradeAndVerify(t, u, l, logBuf, "shutterbug1")

	// Add a shutterbug.

	shutterbugs.Add("shutterbug2")

	// Test the initial response of the empty registry.

	c, _, _ := websocket.DefaultDialer.Dial(u.String(), nil)
	c.WriteJSON("shutterbug2")
	ra := mechcamera.RegAsc{}
	err := c.ReadJSON(&ra)

	{
		want := true
		got := err == nil &&
			len(ra.Registered) == 0 &&
			len(ra.Associated) == 0 &&
			ra.Stitching == mechcamera.Uni.String()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Add a camera.

	reg.Register("cameraA")

	// Test the list of cameras has one entry.

	err = c.ReadJSON(&ra)

	{
		want := true
		got := err == nil &&
			len(ra.Registered) == 1 &&
			ra.Registered[0] == "cameraA"
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test the camera and shutterbug are not yet associated.

	{
		want := true
		got := len(reg.AllAssociatedCameras()) == 0
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test an association request.

	c.WriteJSON(&mechcamera.AssociationsSchema{
		Cameras:   []string{"cameraA"},
		Stitching: mechcamera.Uni.String(),
	})
	time.Sleep(100 * time.Millisecond)

	var a mechcamera.ShutterbugAssociations
	a, err = reg.Pop()
	reg.Associate(a)

	{
		associatedCams := reg.AllAssociatedCameras()
		want := true
		got := err == nil && len(associatedCams) == 1 &&
			associatedCams[0] == "cameraA" &&
			reg.Associated("shutterbug2")[0] == "cameraA"
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		sBugs := reg.AssociatedShutterbugs("cameraA")
		got = len(sBugs) == 1 && sBugs[0] == "shutterbug2"
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	err = c.ReadJSON(&ra)
	{
		want := true
		got := err == nil &&
			len(ra.Registered) == 1 &&
			len(ra.Associated) == 1 &&
			ra.Registered[0] == "cameraA" &&
			ra.Associated[0] == "cameraA" &&
			ra.Stitching == mechcamera.Uni.String()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test deregistering an associated camera.

	reg.Deregister("cameraA")
	err = c.ReadJSON(&ra)

	{
		want := true
		got := err == nil &&
			len(ra.Registered) == 0 &&
			len(reg.Registered()) == 0
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test two clients.

	reg.Register("cameraA", "cameraB", "cameraC")
	shutterbugs.Add("shutterbug3")

	var c2 *websocket.Conn
	c2, _, _ = websocket.DefaultDialer.Dial(u.String(), nil)
	c2.WriteJSON("shutterbug3")
	time.Sleep(100 * time.Millisecond)

	c.WriteJSON(&mechcamera.AssociationsSchema{
		Cameras:   []string{"cameraA", "cameraB"},
		Stitching: "bi",
	})

	c2.WriteJSON(&mechcamera.AssociationsSchema{
		Cameras:   []string{"cameraB", "cameraC"},
		Stitching: "bi",
	})

	time.Sleep(100 * time.Millisecond)

	a, _ = reg.Pop()
	reg.Associate(a)
	a, _ = reg.Pop()
	reg.Associate(a)

	setAB := eqset.New("cameraA", "cameraB")
	setBC := eqset.New("cameraB", "cameraC")
	setABC := eqset.New("cameraA", "cameraB", "cameraC")

	c.ReadJSON(&ra)
	{
		want := true
		got := eqset.New(ra.Registered...).Equals(setABC) &&
			eqset.New(reg.Associated("shutterbug2")...).Equals(setAB)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	c2.ReadJSON(&ra)
	{
		want := true
		got := eqset.New(ra.Registered...).Equals(setABC) &&
			eqset.New(reg.Associated("shutterbug3")...).Equals(setBC) &&
			eqset.New(ra.Associated...).Equals(eqset.New[string]())
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	c2.ReadJSON(&ra)
	{
		want := true
		got := eqset.New(ra.Registered...).Equals(setABC) &&
			eqset.New(ra.Associated...).Equals(setBC) &&
			ra.Stitching == mechcamera.Bi.String()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test shutterbug deletion.

	reg.RemoveShutterbug("shutterbug3")
	time.Sleep(100 * time.Millisecond)

	{
		want := true
		got := eqset.New(reg.Associated("shutterbug2")...).Equals(setAB) &&
			len(reg.Associated("shutterbug3")) == 0
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	err = c2.ReadJSON(&ra)
	fmt.Printf("ra: %v\n", ra)
	{
		want := websocket.CloseAbnormalClosure
		got := err.(*websocket.CloseError).Code
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
