package mechcamera

import (
	"fmt"
	"slices"
	"strings"

	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/wiregl"
)

// ShutterbugAssociations between a shutterbug and some cameras.
type ShutterbugAssociations struct {
	Cameras    []string // In farthest to closest stacking order.
	Shutterbug string
	Stitching  Stitching
}

type Associations struct {
	Cameras   []string // In farthest to closest stacking order.
	Stitching Stitching
}

// IsPartial reports whether the given camera has a partial camera status in
// this association.
func (a Associations) IsPartial(camera string) bool {
	switch a.Stitching {
	case QuadPartial:
		fallthrough
	case TriPartial:
		fallthrough
	case BiPartial:
		return slices.Contains(a.Cameras[1:], camera)
	}
	return false
}

// Key can be used as a unique key in, for example, a cache of stitched images
// for this exact combination of cameras and stitching.
func (a Associations) Key() string {
	return strings.Join(a.Cameras, " ") + a.Stitching.String()
}

// StitchWithCPU the given images where each image corresponds to the view from
// the camera with the same index in the associations.
//
// In the case of a single camera just return the camera's pixel data
// unmodified.
func (a Associations) StitchWithCPU(
	s *wiregl.CPUStitcher, vp ...wiregl.Pixels,
) wiregl.PixelData {
	var major wiregl.PixelData
	switch len(a.Cameras) {
	case 4:
		fallthrough
	case 3:
		fallthrough
	case 2:
		major = vp[0].Full.Copy()
	case 0:
		return wiregl.PixelData{}
	default:
		return vp[0].Full // Nothing to stitch into it.
	}

	const ( // Alpha values.
		fA = 1.0                   // Applied to the fully overlapping images.
		pA = mech.SmallScreenAlpha // Applied to the partially overlapping images.
	)
	var b, l, m, r = 0, 0, (major.Width / 3), 2 * (major.Width / 3)
	switch len(a.Cameras) {
	case 4:
		switch a.Stitching {
		case QuadPartial:
			s.Stitch(major, vp[1].Small, pA, b, l)
			s.Stitch(major, vp[2].Small, pA, b, m)
			s.Stitch(major, vp[3].Small, pA, b, r)

		default:
			s.Stitch(major, vp[1].Full, fA, b, l)
			s.Stitch(major, vp[2].Full, fA, b, l)
			s.Stitch(major, vp[3].Full, fA, b, l)
		}
	case 3:
		switch a.Stitching {
		case TriPartial:
			s.Stitch(major, vp[1].Small, pA, b, l)
			s.Stitch(major, vp[2].Small, pA, b, r)
		default:
			s.Stitch(major, vp[1].Full, fA, b, l)
			s.Stitch(major, vp[2].Full, fA, b, l)
		}
	case 2:
		switch a.Stitching {
		case BiPartial:
			s.Stitch(major, vp[1].Small, pA, b, r)
		default:
			s.Stitch(major, vp[1].Full, fA, b, l)
		}
	}

	return major
}

type AssociationsSchema struct {
	Cameras   []string `json:"cameras"`
	Stitching string   `json:"stitching"`
}

const (
	// Uni implies no stitching. A single camera occupies the whole view.
	Uni = Stitching(iota)

	// Bi is two overlapping cameras. Both cameras occupy the whole view.
	Bi

	// BiPartial is a partial overlapping of one camera by another. The rear
	// camera occupies the whole view. The closest occupies a smaller rectangle.
	BiPartial

	// Tri is three overlapping cameras. All three cameras occupy the whole
	// view.
	Tri

	// TriPartial is a partial overlapping of one camera by two others. The rear
	// camera occupies the whole view. The two closer cameras occupy mutually
	// exclusive smaller rectangles.
	TriPartial

	// Quad is four overlapping cameras. All four cameras occupy the whole view.
	Quad

	// QuadPartial is a partial overlapping of one camera by three others. The
	// rear camera occupies the whole view. The three closer cameras occupy
	// mutually exclusive smaller rectangles.
	QuadPartial
)

type Stitching uint

func (s Stitching) ExpectedCams() int {
	switch s {
	case Bi:
		return 2
	case BiPartial:
		return 2
	case Tri:
		return 3
	case TriPartial:
		return 3
	case Quad:
		return 4
	case QuadPartial:
		return 4
	default:
		return 1
	}
}

func (s Stitching) String() string {
	switch s {
	case Bi:
		return "bi"
	case BiPartial:
		return "biPartial"
	case Tri:
		return "tri"
	case TriPartial:
		return "triPartial"
	case Quad:
		return "quad"
	case QuadPartial:
		return "quadPartial"
	default:
		return "uni"
	}
}

func StitchingFromString(s string) (Stitching, error) {
	switch s {
	case "uni":
		return Uni, nil
	case "bi":
		return Bi, nil
	case "biPartial":
		return BiPartial, nil
	case "tri":
		return Tri, nil
	case "triPartial":
		return TriPartial, nil
	case "quad":
		return Quad, nil
	case "quadPartial":
		return QuadPartial, nil
	}

	return 0, fmt.Errorf("unknown camera stitching: %v", s)
}
