package mechpixel_test

import (
	"bytes"
	"log"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/mechane/mechserver/mechpixel"
	"gitlab.com/fospathi/mechane/mechserver/mechshutterbug"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/universal/tezd"
	"gitlab.com/fospathi/wire/wiregl"
)

func TestDispenser(t *testing.T) {
	logBuf := &bytes.Buffer{}
	l := log.New(logBuf, "mechane ", log.LstdFlags)
	dis := mechpixel.NewDispenser(l)

	// Test some dispenser methods on an empty dispenser.

	dis.Dispense("", wiregl.BinaryPixelData{})

	dis.Dispense("shutterbug1", wiregl.BinaryPixelData{0, 1, 2})

	// Set up a test server.

	shutterbugs := eqset.New[string]()
	u := mechshutterbug.NewTestServer(
		mech.PixelPath, mechpixel.NewPixelHandleFunc(dis, shutterbugs))

	// Do the standard tests for a shutterbug verifying websocket.

	mechshutterbug.TestUpgradeAndVerify(t, u, l, logBuf, "shutterbug1")

	// Test dispensing pixels.

	shutterbugs.Add("shutterbug1")
	c, _, _ := websocket.DefaultDialer.Dial(u.String(), nil)
	c.WriteJSON("shutterbug1")
	time.Sleep(100 * time.Millisecond)

	dis.Dispense("shutterbug1", wiregl.BinaryPixelData{0, 1, 2, 3, 4, 5})
	msgType, pix, err := c.ReadMessage()

	{
		want := true
		got := err == nil && msgType == websocket.BinaryMessage &&
			tezd.CompareBytes(pix, []byte{0, 1, 2, 3, 4, 5})
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test shutterbug removal.

	dis.RemoveShutterbug("shutterbug1")

	_, _, err = c.ReadMessage()
	{
		want := websocket.CloseAbnormalClosure
		got := err.(*websocket.CloseError).Code
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
