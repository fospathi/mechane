/*
Package mechpixel dispenses drawings of scenes associated with a particular
shutterbug.
*/
package mechpixel

import (
	"log"
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/mechane/mechserver/mechshutterbug"
	"gitlab.com/fospathi/wire/wiregl"
)

// NewDispenser constructs a new Dispenser.
func NewDispenser(l *log.Logger) Dispenser {
	return Dispenser{
		Logger:  l,
		m:       &sync.Mutex{},
		viewers: map[string][]viewer{},
	}
}

// A Dispenser of pixels.
type Dispenser struct {
	Logger  *log.Logger
	m       *sync.Mutex
	viewers map[string][]viewer // Maps from a shutterbug to a slice of viewers.
}

// A viewer of a shutterbug's pixels.
type viewer struct {
	c    chan []byte     // Closed by the dispenser end on, for example, a shutterbug removal.
	done <-chan struct{} // Closed by the web handler end on handler exit.
}

// Dispense the given pixels to the viewers of the shutterbug.
func (d Dispenser) Dispense(shutterbug string, pix wiregl.BinaryPixelData) {
	d.m.Lock()
	defer d.m.Unlock()

	viewers, ok := d.viewers[shutterbug]
	if !ok {
		return
	}
	for i := 0; i < len(viewers); {
		v := viewers[i]
		select {
		case <-v.done:
			close(v.c)
			viewers = viewers[:i+copy(viewers[i:], viewers[i+1:])]
			continue
		default:
			select {
			case v.c <- pix:
			default:
			}
		}
		i++
	}
	d.viewers[shutterbug] = viewers
}

// RemoveShutterbug from the dispenser and close the associated HTTP request
// handlers using that shutterbug.
func (d Dispenser) RemoveShutterbug(shutterbug string) {
	d.m.Lock()
	defer d.m.Unlock()

	viewers, ok := d.viewers[shutterbug]
	if !ok {
		return
	}
	for _, v := range viewers {
		close(v.c)
	}
	delete(d.viewers, shutterbug)
}

// addViewer to the dispenser and return the new viewer which receives the given
// shutterbug's pixels on each dispensement hereafter.
//
// After the argument channel is closed no more pixels are sent to the viewer
// and it is eventually removed from the dispenser.
func (d Dispenser) addViewer(done <-chan struct{}, shutterbug string) viewer {
	d.m.Lock()
	defer d.m.Unlock()

	v := viewer{c: make(chan []byte), done: done}
	d.viewers[shutterbug] = append(d.viewers[shutterbug], v)
	return v
}

// NewPixelHandleFunc creates a HTTP request handler which allows clients to
// interface with the dispenser.
//
// The handler upgrades the HTTP request to a websocket. The websocket's first
// ever incoming message should be a valid shutterbug.
func NewPixelHandleFunc(
	d Dispenser,
	shutterbugs mechshutterbug.Containser,
) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		c, shutterbug, err := mechshutterbug.UpgradeAndVerify(
			w, r, d.Logger, "pixel", shutterbugs)
		if err != nil {
			return
		}
		defer c.Close()

		done := make(chan struct{})

		go func() {
			if _, _, err := c.ReadMessage(); err != nil {
				if !websocket.IsCloseError(
					err,
					websocket.CloseNormalClosure,
					websocket.CloseGoingAway) {
					d.Logger.Println("pixel: ReadMessage: ", err)
				}
			} else {
				d.Logger.Println(
					"pixel: ReadMessage: received unexpected message")
			}
			close(done)
		}()

		v := d.addViewer(done, shutterbug)
		for {
			pix, ok := <-v.c
			if !ok {
				break
			}
			if err = c.WriteMessage(websocket.BinaryMessage, pix); err != nil {
				if err != websocket.ErrCloseSent {
					d.Logger.Println("pixel: WriteMessage: ", err)
				}
				break
			}
		}
	}
}
