/*
Package mechserver provides a Mechane server which performs various essential
low level tasks for a Mechane world.
*/
package mechserver

import (
	"encoding/json"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"sync"
	"time"

	"gitlab.com/fospathi/mechane/about"
	"gitlab.com/fospathi/mechane/mechserver/mechcamera"
	"gitlab.com/fospathi/mechane/mechserver/mechdevoir"
	"gitlab.com/fospathi/mechane/mechserver/mechoverlayer"
	"gitlab.com/fospathi/mechane/mechserver/mechpixel"
	"gitlab.com/fospathi/mechane/mechserver/mechraster"
	"gitlab.com/fospathi/mechane/mechserver/mechshutterbug"
	"gitlab.com/fospathi/mechane/mechserver/mechuievent"
	"gitlab.com/fospathi/mechane/motif"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/wiregl"
	"gitlab.com/fospathi/wire/wireserver"
)

// Server is a Mechane server.
//
// A server is required for a Mechane world to run. It shall only ever run one
// world.
type Server struct {
	mech.Addr
	Logger *log.Logger

	begin    chan struct{} // Closed when a shutdown of the server is started.
	finalise chan struct{} // Closed when the world has tidied up after a shutdown.
	m        *sync.Mutex

	CameraRegistry    mechcamera.Registry
	Devoir            *mechdevoir.Devoir
	PixelDispenser    mechpixel.Dispenser
	PixelOverlayer    mechoverlayer.Overlayer
	SceneRasteriser   mechraster.Rasteriser
	ShutterbugRoll    mechshutterbug.Roll
	UIEventsCollector *mechuievent.Collector
}

// BeginShutdown of the server and provide the given reason to clients.
//
// Sleep for a small duration to give time for the UI events websocket to close.
func (svr Server) BeginShutdown(reason string) {
	svr.m.Lock()
	defer svr.m.Unlock()

	select {
	case <-svr.begin:
		return
	default:
	}

	close(svr.begin)
	svr.UIEventsCollector.Close(reason)
	time.Sleep(50 * time.Millisecond)
}

// FinaliseShutdown of the server to signify that the world has finished tidying
// up after shutdown was begun.
//
// Nothing happens if shutdown has not already begun.
func (svr Server) FinaliseShutdown() {
	svr.m.Lock()
	defer svr.m.Unlock()

	select {
	case <-svr.begin:
	case <-svr.finalise:
		return
	default:
		return
	}

	close(svr.finalise)
}

// ShutdownBegan channel is closed when a shutdown of the server is started.
func (svr Server) ShutdownBegan() <-chan struct{} {
	return svr.begin
}

// ShutdownFinalised channel is closed when a world has finished tidying up
// after a shutdown is started.
func (svr Server) ShutdownFinalised() <-chan struct{} {
	return svr.finalise
}

// Host info for a new Mechane server.
type Host struct {
	Rasteriser string // Either an embedded signifier or the URL of a rasteriser.
	Server     string // The hostname and optional port number.
}

// ServerSpec specifies the properties of a new Mechane Server.
type ServerSpec struct {
	Host Host

	// Static data available by GET request.

	About    about.Software
	Motif    motif.Motif
	UIWeb    http.FileSystem // Filesystem with the UI files.
	WorldWeb http.FileSystem // Filesystem with the world's own worldweb directory.
}

// New constructs and starts a new Mechane server at the given host which serves
// static files from the given filesystem.
//
// If the host does not specify a port an ephemeral port is used.
func New(spec ServerSpec) (Server, wiregl.MainThread, <-chan error, error) {

	var (
		err                      error
		abtJSON, elmJSON, tsJSON []byte
		listener                 net.Listener
		mux                      = http.NewServeMux()
		portPattern              = regexp.MustCompile(`:[0-9]+$`)
		svr                      = Server{
			m:        &sync.Mutex{},
			begin:    make(chan struct{}),
			finalise: make(chan struct{}),
		}
		svrErrC = make(chan error, 1)
	)

	if abtJSON, err = json.Marshal(spec.About); err != nil {
		return Server{}, nil, nil, err
	}
	if elmJSON, err = json.Marshal(spec.Motif.ToElmMotif()); err != nil {
		return Server{}, nil, nil, err
	}
	if tsJSON, err = json.Marshal(spec.Motif.ToTypescriptMotif()); err != nil {
		return Server{}, nil, nil, err
	}

	if !portPattern.Match([]byte(spec.Host.Server)) {
		spec.Host.Server += ":0"
	}

	if listener, err = net.Listen("tcp", spec.Host.Server); err != nil {
		return Server{}, nil, nil, err
	}

	l := log.New(os.Stderr, "mechane ", log.LstdFlags)
	svr.Addr = mech.NewAddr(listener)
	svr.Logger = l

	svr.CameraRegistry = mechcamera.NewRegistry(l)
	svr.Devoir = mechdevoir.NewDevoir(l, mechdevoir.InitialState{})
	svr.PixelDispenser = mechpixel.NewDispenser(l)
	svr.PixelOverlayer = mechoverlayer.New(l)
	svr.ShutterbugRoll = mechshutterbug.NewRoll(l)
	svr.UIEventsCollector = mechuievent.NewCollector(l)

	// Static data.

	mux.HandleFunc(
		mech.AboutPath,
		newStaticGetHandlerFunc("about", abtJSON),
	)
	mux.HandleFunc(
		mech.ElmMotifPath,
		newStaticGetHandlerFunc("elm", elmJSON),
	)
	mux.HandleFunc(
		mech.TypescriptMotifPath,
		newStaticGetHandlerFunc("typescript", tsJSON),
	)

	// Dynamic data.

	mux.HandleFunc(
		mech.CameraPath,
		mechcamera.NewCameraHandleFunc(svr.CameraRegistry, svr.ShutterbugRoll),
	)
	mux.HandleFunc(
		mech.DevoirPath,
		mechdevoir.NewDevoirHandleFunc(svr.Devoir, svr.ShutterbugRoll),
	)
	mux.HandleFunc(
		mech.PixelPath,
		mechpixel.NewPixelHandleFunc(svr.PixelDispenser, svr.ShutterbugRoll),
	)
	mux.HandleFunc(
		mech.OverlayerPath,
		mechoverlayer.NewOverlayerHandleFunc(
			svr.PixelOverlayer, svr.ShutterbugRoll,
		),
	)
	mux.HandleFunc(
		mech.ShutterbugPath,
		mechshutterbug.NewShutterbugHandleFunc(svr.ShutterbugRoll),
	)
	mux.HandleFunc(
		mech.UIEventPath,
		mechuievent.NewEventHandleFunc(
			svr.UIEventsCollector, svr.ShutterbugRoll,
		),
	)

	// File systems.

	const wwPrefix = mech.WorldWebPath + "/"
	mux.Handle(
		wwPrefix,
		http.StripPrefix(wwPrefix, http.FileServer(spec.WorldWeb)),
	)
	mux.Handle("/", http.FileServer(spec.UIWeb))

	var httpSvr http.Server
	httpSvr.Addr = listener.Addr().String()
	httpSvr.Handler = mux

	go func() {
		if err = httpSvr.Serve(listener); err != nil {
			svrErrC <- err
		}
	}()

	var (
		client  wireserver.WSGobClient
		rasErrC <-chan error
		mc      wiregl.MainThread
	)
	switch spec.Host.Rasteriser {
	case mech.EmbeddedFlag:
		svr.SceneRasteriser, mc, rasErrC = mechraster.NewEmbedded()
	default:
		u, _ := url.ParseRequestURI(spec.Host.Rasteriser)
		client, err = wireserver.NewWSGobClient(u.Host)
		if err != nil {
			return Server{}, nil, nil, err
		}
		svr.SceneRasteriser = client
	}

	select {
	case err = <-svrErrC:
		return Server{}, nil, nil, err
	case err = <-rasErrC:
		return Server{}, nil, nil, err
	default:
	}

	errC := make(chan error, 1)
	go func() {
		select {
		case err = <-svrErrC:
		case err = <-rasErrC:
		}
		errC <- err
	}()

	return svr, mc, errC, nil
}

func newStaticGetHandlerFunc(name string, vb []byte) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			w.Header().Set("Allow", "GET")
			http.Error(w, name+" handler: method error",
				http.StatusMethodNotAllowed)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(vb)
	}
}
