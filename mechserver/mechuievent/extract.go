package mechuievent

import (
	"math"
	"slices"
	"time"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/universal/mech"
)

// ButtonClick event on a mechane-button element from an input source associated
// with a particular shutterbug.
type ButtonClick struct {
	MechaneButtonEvent
	Shutterbug string
}

// ButtonClicks is a sequence of click events coming from Mechane buttons.
type ButtonClicks struct {
	*dryad.SyncAwaitSeq[ButtonClick]
}

// Extract the Mechane button click events from the given fresh collections.
//
// Of the filtered click events, append those that pass temporal hysteresis
// checks to the receiver sequence in order from oldest to most recent.
//
// The all collection shall contain and be a superset of the fresh collection.
func (seq ButtonClicks) Extract(fresh Collections, all Collections) {
	type bEvent struct {
		e   Event
		j   int // A left direction index relative to the last position.
		sb  string
		src string
	}

	// Get and remove the fresh button events.
	var vbe []bEvent
	for src, col := range fresh {
		ve, last := col.Events, len(col.Events)-1
		for i := last; i >= 0; i-- {
			e := ve[i]
			if _, ok := e.E.(MechaneButtonEvent); ok {
				vbe = append(vbe, bEvent{
					e:   e,
					j:   last - i,
					sb:  col.Shutterbug,
					src: src,
				})
				ve = append(ve[:i], ve[i+1:]...) // Remove button event.
			}
		}
		col.Events = ve
		fresh[src] = col
	}

	l := len(vbe)
	if l == 0 {
		return
	}

	hysteresisCheck := func(be bEvent) bool {
		const (
			farthest = mech.MouseMoveFarthest
			longest  = mech.ButtonClickLongest
		)
		ve := all[be.src].Events
		last := len(ve) - 1
		edge := be.e.T.Add(-longest)

		// Check that the duration since the most recent mousedown event before
		// the click is small.
		var k int
		for i := last - be.j; i >= 0; i-- {
			e := ve[i]
			if e.T.Before(edge) {
				return false
			}
			mbe, ok := e.E.(MouseButtonEvent)
			if ok && mbe.MouseDown {
				k = i
				break
			}
			if i == 0 {
				return false
			}
		}

		// Check that the scalar distance moved since the mousedown event is
		// small.
		var d float64
		for i := k + 1; i <= last-be.j; i++ {
			e := ve[i]
			mme, ok := e.E.(MouseMoveEvent)
			if !ok {
				continue
			}
			d += math.Hypot(mme.MovementX, mme.MovementY)
		}

		return d <= farthest
	}

	for i := l - 1; i >= 0; i-- {
		if !hysteresisCheck(vbe[i]) {
			vbe = slices.Delete(vbe, i, i+1)
		}
	}

	if len(vbe) == 0 {
		return
	}

	slices.SortFunc(vbe, func(a, b bEvent) int {
		return a.e.T.Compare(b.e.T) // Sort events by time.
	})

	res := make([]ButtonClick, 0, len(vbe))
	for _, be := range vbe {
		res = append(res, ButtonClick{
			be.e.E.(MechaneButtonEvent),
			be.sb,
		})
	}
	seq.AppendAll(res)
}

// NewButtonClicks constructs a new ButtonClicks.
func NewButtonClicks() ButtonClicks {
	return ButtonClicks{
		dryad.NewSyncAwaitSeq[ButtonClick](),
	}
}

// TextfieldKeys is a sequence of key events coming from Mechane textfields.
type TextfieldKeys struct {
	*dryad.SyncAwaitSeq[InOrderEvent[MechaneTextfieldKeyUpOrDownEvent]]
}

// Extract the Mechane textfield key events from the given fresh collections and
// append them to the receiver sequence in order from oldest to most recent.
func (seq TextfieldKeys) Extract(fresh Collections) {
	vcv := extractFresh[MechaneTextfieldKeyUpOrDownEvent](fresh)
	l := len(vcv)
	if l == 0 {
		return
	}
	sortChronoSlice(vcv)
	seq.AppendAll(chronoSliceValues(vcv))
}

// NewTextfieldKeys constructs a new TextfieldKeys.
func NewTextfieldKeys() TextfieldKeys {
	return TextfieldKeys{
		dryad.NewSyncAwaitSeq[InOrderEvent[MechaneTextfieldKeyUpOrDownEvent]](),
	}
}

// WheelMoves is a sequence of wheel move events.
type WheelMoves struct {
	*dryad.SyncAwaitSeq[InOrderEvent[MouseWheelEvent]]
}

// Extract the Mechane mouse wheel move events from the given fresh collections
// and append them to the receiver sequence in order from oldest to most recent.
func (seq WheelMoves) Extract(fresh Collections) {
	vcv := extractFresh[MouseWheelEvent](fresh)
	l := len(vcv)
	if l == 0 {
		return
	}
	sortChronoSlice(vcv)
	seq.AppendAll(chronoSliceValues(vcv))
}

// NewWheelMoves constructs a new WheelMoves.
func NewWheelMoves() WheelMoves {
	return WheelMoves{
		dryad.NewSyncAwaitSeq[InOrderEvent[MouseWheelEvent]](),
	}
}

type InOrderEvent[E any] struct {
	E          E
	Shutterbug string
}

func extractFresh[E any](fresh Collections) []chronoValue[InOrderEvent[E]] {
	var vcv []chronoValue[InOrderEvent[E]]
	for src, col := range fresh {
		ve := col.Events
		for i := len(col.Events) - 1; i >= 0; i-- {
			e := ve[i]
			if data, ok := e.E.(E); ok {
				vcv = append(vcv, chronoValue[InOrderEvent[E]]{
					t: e.T,
					v: InOrderEvent[E]{
						E:          data,
						Shutterbug: col.Shutterbug,
					},
				})
				ve = slices.Delete(ve, i, i+1)
			}
		}
		col.Events = ve
		fresh[src] = col
	}
	return vcv
}

type chronoValue[T any] struct {
	t time.Time
	v T
}

func sortChronoSlice[T any](vcv []chronoValue[T]) {
	slices.SortFunc(vcv, func(a, b chronoValue[T]) int {
		return a.t.Compare(b.t)
	})
}

func chronoSliceValues[T any](vcv []chronoValue[T]) []T {
	res := make([]T, 0, len(vcv))
	for _, cv := range vcv {
		res = append(res, cv.v)
	}
	return res
}
