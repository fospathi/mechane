/*
Package mechuievent collects certain UI events from clients such as keyboard key
events, mouse move events, and ShutterbugPixels' overlayer clicks. The semantic
meaning of these kinds of events is not known by the client and is determined by
the server.

Events from sidebar buttons and sidebar popup panels are dealt with by a
different package.
*/
package mechuievent

import (
	"errors"
	"log"
	"net"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/mechane/mechserver/mechshutterbug"
	"gitlab.com/fospathi/universal/mech"
)

// NewCollector constructs a new Collector.
func NewCollector(l *log.Logger) *Collector {
	return &Collector{
		Logger: l,
		m:      sync.Mutex{},
		cols:   map[string]collection{},
	}
}

// A Collector of UI events from various sources.
type Collector struct {
	Logger *log.Logger

	m      sync.Mutex
	cols   map[string]collection // Sources are the map keys.
	done   bool
	reason string // Closure reason.
}

// Close the collector disconnecting and deleting all current sources and
// rejecting any future attempts to add a new source.
func (clr *Collector) Close(reason string) {
	clr.m.Lock()
	defer clr.m.Unlock()

	if clr.done {
		return
	}
	clr.done = true
	clr.reason = reason

	for src, col := range clr.cols {
		col.done <- clr.reason
		delete(clr.cols, src)
	}
}

// ClearShutterbug UI events for the given shutterbug and return the sources
// whose events were cleared.
func (clr *Collector) ClearShutterbug(shutterbug string) dryad.Set[string] {
	res := eqset.New[string]()
	for src, c := range clr.cols {
		if c.Shutterbug != shutterbug {
			continue
		}
		res.Add(src)
		c.Events = nil
		clr.cols[src] = c
	}
	return res
}

// Collections of the collector.
//
// Map keys are UI event input sources.
func (clr *Collector) Collections() Collections {
	clr.m.Lock()
	defer clr.m.Unlock()

	res := map[string]Collection{}
	for src, col := range clr.cols {
		res[src] = col.Copy()
	}
	return res
}

// RemoveShutterbug from the collector and close the associated HTTP request
// handlers using that shutterbug.
//
// The given reason is sent to clients as the close reason message.
func (clr *Collector) RemoveShutterbug(shutterbug, reason string) {
	clr.m.Lock()
	defer clr.m.Unlock()

	if clr.done {
		return
	}

	// Shutterbugs can have multiple sources.
	for src, col := range clr.cols {
		if col.Shutterbug != shutterbug {
			continue
		}
		col.done <- reason
		delete(clr.cols, src)
	}
}

// RemoveSource from the collector and close the associated HTTP request
// handler.
//
// The given reason is sent to the client as the close reason message.
func (clr *Collector) RemoveSource(src string, reason string) {
	clr.m.Lock()
	defer clr.m.Unlock()

	if clr.done {
		return
	}

	col, ok := clr.cols[src]
	if !ok {
		return
	}
	col.done <- reason
	delete(clr.cols, src)
}

// Sources is a set containing all the UI event input sources in the collector.
func (clr *Collector) Sources() dryad.Set[string] {
	clr.m.Lock()
	defer clr.m.Unlock()

	res := eqset.New[string]()
	for src := range clr.cols {
		res.Add(src)
	}
	return res
}

// addEvent to the end of the given source's events list and set the event's
// time stamp to now.
//
// If the collector is closed nothing happens.
func (clr *Collector) addEvent(src string, e Event) {
	clr.m.Lock()
	defer clr.m.Unlock()

	if clr.done {
		return
	}

	col, ok := clr.cols[src]
	if !ok {
		return
	}
	e.T = time.Now().UTC()
	col.Events = append(col.Events, e)
	clr.cols[src] = col
}

func (clr *Collector) addSource(src, shutterbug string) <-chan string {
	clr.m.Lock()
	defer clr.m.Unlock()

	if clr.done {
		done := make(chan string, 1)
		done <- clr.reason
		return done
	}

	col := collection{
		Collection: Collection{
			Shutterbug: shutterbug,
		},
		done: make(chan string),
	}
	clr.cols[src] = col
	return col.done
}

func (clr *Collector) removeSource(src string) {
	clr.m.Lock()
	defer clr.m.Unlock()

	if clr.done {
		return
	}

	col, ok := clr.cols[src]
	if !ok {
		return
	}
	close(col.done)
	delete(clr.cols, src)
}

// A Collection of ordered UI events from one source.
type Collection struct {
	Events     []Event // UI events ordered by time, that is from oldest to youngest.
	Shutterbug string  // The target shutterbug of the UI events.
}

// Copy the collection.
//
// The slice of events is independent but the events themselves are the same.
func (col Collection) Copy() Collection {
	cpy := Collection{Shutterbug: col.Shutterbug}
	cpy.Events = append(cpy.Events, col.Events...)
	return cpy
}

type collection struct {
	Collection
	// Closed by the collector end thus alerting the HTTP request handler to
	// quit.
	done chan string
}

// Collections is a snapshot of the UI events collected in a collector at a
// particular instant.
//
// Map keys are UI event input sources.
type Collections map[string]Collection

// ClearShutterbug deletes the sources associated with the given shutterbug.
func (cols Collections) ClearShutterbug(shutterbug string) {
	for src, c := range cols {
		if c.Shutterbug == shutterbug {
			delete(cols, src)
		}
	}
}

// Fresh collected events present in the receiver collections which are not
// present in the argument older collections.
func (cols Collections) Fresh(old Collections) Collections {
	res := map[string]Collection{}
	for src, col := range cols {
		older, ok := old[src]
		if !ok {
			res[src] = col.Copy()
			continue
		}
		var (
			edge time.Time
			l    int
		)
		if l = len(older.Events); l > 0 {
			edge = older.Events[l-1].T
		}
		var n int
		l = len(col.Events)
		for i := l - 1; i >= 0; i-- {
			if !col.Events[i].T.After(edge) {
				break
			}
			n++
		}
		if n == 0 {
			continue
		}
		ve := make([]Event, n)
		copy(ve, col.Events[l-n:])
		res[src] = Collection{
			Shutterbug: col.Shutterbug,
			Events:     ve,
		}
	}
	return res
}

// Sources is a set containing all the UI event input sources in the
// collections.
func (cols Collections) Sources() dryad.Set[string] {
	res := eqset.New[string]()
	for src := range cols {
		res.Add(src)
	}
	return res
}

// NewEventHandleFunc creates a HTTP request handler which allows clients to
// interface with the collector.
//
// The handler upgrades the HTTP request to a websocket and accepts UI events,
// one per websocket message. The websocket's first ever message should be a
// valid shutterbug.
func NewEventHandleFunc(
	clr *Collector,
	shutterbugs mechshutterbug.Containser,
) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		l := clr.Logger
		c, shutterbug, err := mechshutterbug.UpgradeAndVerify(
			w, r, l, "ui event", shutterbugs)
		if err != nil {
			return
		}
		defer c.Close()

		var source string
		if source, err = createSource(); err != nil {
			clr.Logger.Println("ui event: create source: ", err)
			mech.InternalServerError(c,
				"internal server error for creating a source ID")
			return
		}
		done := clr.addSource(source, shutterbug)

		go func() {
			reason, ok := <-done
			if !ok {
				return
			}
			if err := mech.NormalClosure(c, reason); err != nil {
				clr.Logger.Println("ui event: websocket closure: ", err)
			}
			c.Close()
		}()

		var e Event
		for {
			var ie interimEvent
			if err = c.ReadJSON(&ie); err != nil {
				clr.removeSource(source)

				if !websocket.IsCloseError(
					err,
					websocket.CloseNormalClosure,
					websocket.CloseGoingAway,
				) && !errors.Is(err, net.ErrClosed) {
					// Check for net.ErrClosed for when the socket is closed
					// from this side. For details on the net.ErrClosed type of
					// error see
					// https://github.com/golang/go/commit/e9ad52e46dee4b4f9c73ff44f44e1e234815800f
					clr.Logger.Println("ui event: readJSON: ", err)
				}
				break
			}
			if e, err = ie.toEvent(); err != nil {
				clr.removeSource(source)

				clr.Logger.Println("ui event: newEvent: ", err)
				mech.PolicyViolation(c, err.Error())
				break
			}
			clr.addEvent(source, e)
		}
	}
}

func createSource() (string, error) {
	return mech.RandBase64String(mech.ShutterbugSourceBits)
}

// interimEvent is a UI event. It is the target type when decoding a JSON string
// which contains a UI event.
type interimEvent struct {
	Keyboard                    *KeyboardEvent
	MechaneButton               *MechaneButtonEvent
	MechaneTextfieldKeyUpOrDown *MechaneTextfieldKeyUpOrDownEvent
	MouseButton                 *MouseButtonEvent
	MouseMove                   *MouseMoveEvent
	MouseWheel                  *MouseWheelEvent
}

func (ie interimEvent) toEvent() (Event, error) {
	switch {
	case ie.Keyboard != nil:
		return Event{E: *ie.Keyboard}, nil
	case ie.MechaneButton != nil:
		return Event{E: *ie.MechaneButton}, nil
	case ie.MechaneTextfieldKeyUpOrDown != nil:
		return Event{E: *ie.MechaneTextfieldKeyUpOrDown}, nil
	case ie.MouseButton != nil:
		return Event{E: *ie.MouseButton}, nil
	case ie.MouseMove != nil:
		return Event{E: *ie.MouseMove}, nil
	case ie.MouseWheel != nil:
		return Event{E: *ie.MouseWheel}, nil
	}
	return Event{}, errors.New("unrecognised ui event")
}
