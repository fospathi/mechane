package mechuievent

import (
	"time"

	"gitlab.com/fospathi/universal/pridi"
)

// An Event collected at a certain time.
type Event struct {
	E interface{} // The keyboard, mouse move, mouse button, and so on UI event.
	T time.Time   // The UTC time of addition to the collection.
}

func (e Event) String() string {
	return pridi.Struct(e)
}

// A KeyboardEvent from a keyboard.
type KeyboardEvent struct {
	AltKey      bool
	CtrlKey     bool
	IsComposing bool
	Key         string // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key
	KeyDown     bool   // True for a key down event.
	MetaKey     bool
	Pressed     []string // All the pressed keyboard keys (logically not physically).
	Repeat      bool
	ShiftKey    bool
}

// Modified reports whether any modifier key was pressed when the event was
// created.
func (e KeyboardEvent) Modified() bool {
	return e.AltKey || e.CtrlKey || e.MetaKey || e.ShiftKey
}

// A MechaneButtonEvent from an overlay element.
type MechaneButtonEvent struct {
	AltKey           bool
	Button           float64
	ClientX, ClientY float64
	CtrlKey          bool
	Detail           int
	EffectName       string
	MetaKey          bool
	ShiftKey         bool
	WindowW, WindowH float64
}

// Modified reports whether any modifier key was pressed when the event was
// created.
func (e MechaneButtonEvent) Modified() bool {
	return e.AltKey || e.CtrlKey || e.MetaKey || e.ShiftKey
}

// A MechaneTextfieldKeyUpOrDownEvent from an overlay element.
type MechaneTextfieldKeyUpOrDownEvent struct {
	AltKey      bool
	CtrlKey     bool
	EffectName  string
	IsComposing bool
	Key         string // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key
	KeyDown     bool
	MetaKey     bool
	ShiftKey    bool
	Value       string
}

// Modified reports whether any modifier key was pressed when the event was
// created.
func (e MechaneTextfieldKeyUpOrDownEvent) Modified() bool {
	return e.AltKey || e.CtrlKey || e.MetaKey || e.ShiftKey
}

// A MouseButtonEvent from a mouse is a mousedown or mouseup event.
type MouseButtonEvent struct {
	AltKey           bool
	Button           float64 // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
	ClientX, ClientY float64 // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/clientX
	CtrlKey          bool
	Detail           int // https://developer.mozilla.org/en-US/docs/Web/API/UIEvent/detail
	MetaKey          bool
	MouseDown        bool
	ShiftKey         bool
	WindowW, WindowH float64 // https://developer.mozilla.org/en-US/docs/Web/API/Window/innerHeight
}

// Modified reports whether any modifier key was pressed when the event was
// created.
func (e MouseButtonEvent) Modified() bool {
	return e.AltKey || e.CtrlKey || e.MetaKey || e.ShiftKey
}

// A MouseMoveEvent from a mouse.
type MouseMoveEvent struct {
	ActiveEditable       bool // True if the hover is on a focused text editing area or similar.
	AltKey               bool
	Buttons              float64 // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/buttons
	ClientX, ClientY     float64 // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/clientX
	CtrlKey              bool
	MetaKey              bool
	MovementX, MovementY float64 // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/movementX
	// The dynamic dimensions of the canvas element/view area, inclusive of
	// scaling, in units of pixels.
	PixelsW, PixelsH             float64
	PixelsOffsetX, PixelsOffsetY float64 // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/offsetX
	PointerLock                  bool
	ShiftKey                     bool
	WindowW, WindowH             float64
}

// Modified reports whether any modifier key was pressed when the event was
// created.
func (e MouseMoveEvent) Modified() bool {
	return e.AltKey || e.CtrlKey || e.MetaKey || e.ShiftKey
}

// A MouseWheelEvent from a mouse wheel.
//
// https://developer.mozilla.org/en-US/docs/Web/API/Element/wheel_event
type MouseWheelEvent struct {
	AltKey           bool
	ClientX, ClientY float64 // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/clientX
	CtrlKey          bool
	DeltaX, DeltaY   float64 // https://developer.mozilla.org/en-US/docs/Web/API/Element/wheel_event#event_properties
	EffectName       string
	MetaKey          bool
	ShiftKey         bool
	WindowW, WindowH float64
}
