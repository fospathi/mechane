package mechuievent_test

import (
	"bytes"
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/mechane/mechserver/mechshutterbug"
	"gitlab.com/fospathi/mechane/mechserver/mechuievent"
	"gitlab.com/fospathi/universal/mech"
)

func TestCollector(t *testing.T) {
	logBuf := &bytes.Buffer{}
	l := log.New(logBuf, "mechane ", log.LstdFlags)
	clr := mechuievent.NewCollector(l)

	// Set up a test server.

	shutterbugs := eqset.New[string]()
	u := mechshutterbug.NewTestServer(
		mech.UIEventPath, mechuievent.NewEventHandleFunc(clr, shutterbugs))

	// Do the standard tests for a shutterbug verifying websocket.

	mechshutterbug.TestUpgradeAndVerify(t, u, l, logBuf, "shutterbug1")

	// Test sending a shutterbug followed by input events.

	shutterbugs.Add("shutterbug1")
	c, _, _ := websocket.DefaultDialer.Dial(u.String(), nil)
	c.WriteJSON("shutterbug1")
	c.WriteMessage(websocket.TextMessage, []byte(`{"keyboard":{"key":"X"}}`))
	time.Sleep(100 * time.Millisecond)
	collected1 := clr.Collections()

	{
		want := true
		got := len(collected1) == 1
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
	for _, col := range collected1 {
		for _, event := range col.Events {
			want := true
			got := (event.E.(mechuievent.KeyboardEvent)).Key == "X"
			if want != got {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}
	}

	type KeyboardEvent struct {
		Key string
	}

	type MousebuttonEvent struct {
		WindowW float64
	}

	type event struct {
		Keyboard    *KeyboardEvent
		Mousebutton *MousebuttonEvent
	}

	c.WriteJSON(event{
		Keyboard: &KeyboardEvent{Key: "Y"},
	})
	time.Sleep(100 * time.Millisecond)
	collected2 := clr.Collections()
	keys := eqset.New[string]()
	for _, col := range collected2 {
		for _, event := range col.Events {
			keys.Add(event.E.(mechuievent.KeyboardEvent).Key)
		}
	}

	{
		want := true
		got := keys.ContainsAll("X", "Y")
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test getting fresh events.

	fresh := clr.Collections().Fresh(collected1)

	keys = eqset.New[string]()
	for _, col := range fresh {
		for _, event := range col.Events {
			keys.Add(event.E.(mechuievent.KeyboardEvent).Key)
		}
	}

	{
		want := true
		got := keys.Equals(eqset.New("Y"))
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test sending an unknown UI event type.

	shutterbugs.Add("shutterbug2")
	c2, _, _ := websocket.DefaultDialer.Dial(u.String(), nil)
	c2.WriteJSON("shutterbug2")
	time.Sleep(100 * time.Millisecond)

	{
		want := 2
		got := len(clr.Collections())
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	c2.WriteMessage(websocket.TextMessage, []byte(`{"keeboard":{"key":"Z"}}`))
	time.Sleep(100 * time.Millisecond)
	_, _, err := c2.ReadMessage()

	{
		want := websocket.ClosePolicyViolation
		got := err.(*websocket.CloseError).Code
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	c2, _, _ = websocket.DefaultDialer.Dial(u.String(), nil)
	c2.WriteJSON("shutterbug2")
	c2.WriteMessage(websocket.TextMessage, []byte{3})
	time.Sleep(100 * time.Millisecond)
	_, _, err = c2.ReadMessage()

	{
		want := websocket.CloseAbnormalClosure
		got := err.(*websocket.CloseError).Code
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := 1
		got := len(clr.Collections())
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test fresh events from a new source.

	c2, _, _ = websocket.DefaultDialer.Dial(u.String(), nil)
	c2.WriteJSON("shutterbug2")
	c2.WriteJSON(event{
		Mousebutton: &MousebuttonEvent{WindowW: 3.14},
	})
	time.Sleep(100 * time.Millisecond)

	{
		want := 2
		got := len(clr.Collections())
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	collected3 := clr.Collections()
	keys = eqset.New[string]()
	for _, col := range collected3 {
		for _, event := range col.Events {
			switch e := event.E.(type) {
			case mechuievent.KeyboardEvent:
				keys.Add(e.Key)
			case mechuievent.MouseButtonEvent:
				keys.Add(fmt.Sprintf("%.2f", e.WindowW))
			}
		}
	}

	{
		want := true
		got := keys.ContainsAll("X", "Y", "3.14")
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	fresh = clr.Collections().Fresh(collected2)

	keys = eqset.New[string]()
	for _, col := range fresh {
		for _, event := range col.Events {
			switch e := event.E.(type) {
			case mechuievent.KeyboardEvent:
				keys.Add(e.Key)
			case mechuievent.MouseButtonEvent:
				keys.Add(fmt.Sprintf("%.2f", e.WindowW))
			}
		}
	}

	{
		want := true
		got := keys.ContainsAll("3.14") && keys.Len() == 1
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test removing a shutterbug.

	clr.RemoveShutterbug("shutterbug1", "reason")

	{
		want := 1
		got := len(clr.Collections())
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	collected4 := clr.Collections()
	keys = eqset.New[string]()
	for _, col := range collected4 {
		for _, event := range col.Events {
			switch e := event.E.(type) {
			case mechuievent.KeyboardEvent:
				keys.Add(e.Key)
			case mechuievent.MouseButtonEvent:
				keys.Add(fmt.Sprintf("%.2f", e.WindowW))
			}
		}
	}

	{
		want := true
		got := keys.ContainsAll("3.14") && keys.Len() == 1
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
