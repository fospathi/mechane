package mechoverlayer_test

import (
	"bytes"
	"log"
	"slices"
	"testing"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/mechane/mechserver/mechoverlayer"
	"gitlab.com/fospathi/mechane/mechserver/mechshutterbug"
	"gitlab.com/fospathi/universal/mech"
)

type uiElement struct {
	Name      string `json:"name"`
	InnerHTML string `json:"innerHTML"`
	Left      string `json:"left"`
	Top       string `json:"top"`
	ZIndex    int    `json:"zIndex"`
}

type element struct {
	uiElement
}

// The overlay update sent to clients in JSON format.
type overlayUpdate struct {
	// Newly created elements.
	Create []string `json:"create"`
	// Names of elements to delete.
	Delete []string `json:"delete"`
	// Names of elements to hide.
	Hide []string `json:"hide"`
	// Elements with changes to be shown.
	Update []element `json:"update"`
}

func TestOverlayer(t *testing.T) {
	logBuf := &bytes.Buffer{}
	l := log.New(logBuf, "mechane ", log.LstdFlags)
	ovr := mechoverlayer.New(l)

	// Set up a test server.

	shutterbugs := eqset.New[string]()
	u := mechshutterbug.NewTestServer(
		mech.OverlayerPath,
		mechoverlayer.NewOverlayerHandleFunc(ovr, shutterbugs))

	// Do the standard tests for a shutterbug verifying websocket.

	mechshutterbug.TestUpgradeAndVerify(t, u, l, logBuf, "shutterbug1")

	// Add a shutterbug.

	shutterbugs.Add("shutterbug2")

	// Add overlay elements associated with that shutterbug.

	main := mechoverlayer.Elements{
		Ahead: map[string]mechoverlayer.Element{
			"ahead1": {Z: 4},
			"ahead2": {Z: 3},
		},
		Behind: eqset.New("behind1"),
	}
	veneer := mechoverlayer.Elements{
		Ahead: map[string]mechoverlayer.Element{
			"ahead3": {Z: 5},
			"ahead4": {Z: 6},
		},
		Behind: eqset.New("behind2"),
	}

	ovr.Synchronise("shutterbug2", main, veneer)

	// Test the initial 'create' instruction upon connecting.

	c, _, _ := websocket.DefaultDialer.Dial(u.String(), nil)
	c.WriteJSON("shutterbug2")
	var update overlayUpdate
	if err := c.ReadJSON(&update); err != nil {
		t.Error(err)
	}

	{
		want := eqset.New("ahead1", "ahead2", "ahead3", "ahead4")
		got := eqset.New(update.Create...)
		if !want.Equals(got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := true
		got := len(update.Delete) == 0 &&
			len(update.Hide) == 0 &&
			len(update.Update) == 0
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test the next synchronise() call in the game loop by testing the
	// subsequent 'update' instruction.

	ovr.Synchronise("shutterbug2", main, veneer)

	update = overlayUpdate{}
	if err := c.ReadJSON(&update); err != nil {
		t.Error(err)
	}

	var names []string
	for _, u := range update.Update {
		names = append(names, u.Name)
	}

	{
		want := []string{"ahead1", "ahead2", "ahead4", "ahead3"}
		got := names
		if !slices.Equal(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := true
		got := len(update.Create) == 0 &&
			len(update.Delete) == 0 &&
			len(update.Hide) == 0
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
