/*
Package mechoverlayer dispenses HTML elements which overlay the scene associated
with a particular shutterbug.
*/
package mechoverlayer

import (
	"cmp"
	"log"
	"net/http"
	"slices"
	"strconv"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/mechane/mechserver/mechshutterbug"
	"gitlab.com/fospathi/universal/mech"
	"golang.org/x/exp/maps"
)

// New constructs a new Overlayer.
func New(l *log.Logger) Overlayer {
	return Overlayer{
		Logger: l,

		m:        &sync.Mutex{},
		overlays: map[string]*overlay{},
	}
}

// An Overlayer makes and updates HTML elements in overlays.
//
// Each shutterbug is associated with an overlay: all clients using that
// shutterbug see the same overlay. In the browser the overlay's elements appear
// above the shutterbug's pixels screen.
type Overlayer struct {
	Logger *log.Logger

	m        *sync.Mutex
	overlays map[string]*overlay // Maps from a shutterbug to an overlay.
}

// RemoveShutterbug from the overlayer and close the associated HTTP request
// handlers using that shutterbug.
//
// If the shutterbug does not exist in the overlayer nothing happens.
func (ovr Overlayer) RemoveShutterbug(shutterbug string) {
	ovr.m.Lock()
	defer ovr.m.Unlock()

	ov, ok := ovr.overlays[shutterbug]
	if !ok {
		return
	}
	for v := range ov.viewers {
		close(v.uc)
	}
	delete(ovr.overlays, shutterbug)
}

// Synchronise the receiver overlayer's overlay elements with the given elements
// for the given shutterbug.
//
// The given elements are the source of truth, and based on that the receiver
// creates, deletes, updates, and hides its elements as necessary.
func (ovr Overlayer) Synchronise(
	shutterbug string,
	main Elements,
	veneer Elements,
) {

	ovr.m.Lock()
	defer ovr.m.Unlock()

	ov, ok := ovr.overlays[shutterbug]
	if !ok {
		ov = newOverlay()
		ovr.overlays[shutterbug] = ov
	}

	for name, el := range main.Ahead {
		el.layer = 0
		main.Ahead[name] = el
	}
	for name, el := range veneer.Ahead {
		el.layer = 1
		veneer.Ahead[name] = el
	}

	synchronise(ov, main, veneer)
}

// An Element in a HTML document overlaying a shutterbug's pixels screen.
type Element struct {
	// The element's HTML as a string.
	InnerHTML string
	// Hash of the innerHTML.
	InnerHTMLHash string
	// Orientation of the element.
	Transform, TransformOrigin string
	// (0.5 * screen ratio) is a point at the right edge of the screen, (-0.5 *
	// screen ratio) is the left edge of the screen.
	X float64
	// 0.5 is a point at the top edge of the screen, -0.5 is the bottom edge of
	// the screen.
	Y float64
	// Perpendicular distance from the camera's Z=0 plane, used to determine the
	// element's zIndex in the HTML document.
	Z float64

	layer int
}

func (el Element) toUIElement(name string) uiElement {
	return uiElement{
		Name:            name,
		InnerHTML:       el.InnerHTML,
		InnerHTMLHash:   el.InnerHTMLHash,
		Left:            XToLeft(el.X),
		Transform:       el.Transform,
		TransformOrigin: el.TransformOrigin,
		Top:             YToTop(el.Y),
	}
}

// Elements accessible by element name.
type Elements struct {
	Ahead  map[string]Element
	Behind dryad.Set[string]
}

// Contains reports whether an overlay element with the given name exists in the
// receiver elements.
func (els Elements) Contains(name string) bool {
	_, ok := els.Ahead[name]
	return ok || els.Behind.Contains(name)
}

// Shows reports whether the overlay element with the given name is possibly
// showing.
func (els Elements) Shows(name string) bool {
	_, ok := els.Ahead[name]
	return ok
}

func newOverlay() *overlay {
	return &overlay{
		shown:    eqset.New[string](),
		elements: map[string]Element{},
		viewers:  map[viewer]struct{}{},
	}
}

// An overlay associated with a particular shutterbug.
type overlay struct {
	// Maps unique overlay element name to element, including hidden ones.
	elements map[string]Element
	// The recipients/clients of overlay updates for the shutterbug.
	viewers map[viewer]struct{}

	// A set of names of shown overlay elements.
	shown dryad.Set[string]
}

func (ov overlay) names() []string {
	var vn []string
	for name := range ov.elements {
		vn = append(vn, name)
	}
	return vn
}

func (ov overlay) sortedShown() []element {
	var ve []element
	for name := range ov.shown {
		el := ov.elements[name]
		ve = append(ve, element{
			uiElement: el.toUIElement(name),
			z:         el.Z,
			layer:     el.layer,
		})
	}
	slices.SortFunc(ve, func(a, b element) int {
		if a.layer != b.layer {
			return cmp.Compare(a.layer, b.layer)
		}
		if a.z == b.z {
			return -cmp.Compare(a.Name, b.Name)
		}
		// Z's are positive here. Sort in farthest is first order.
		return -cmp.Compare(a.z, b.z)
	})
	for i := 0; i < len(ve); i++ {
		ve[i].ZIndex = i
	}
	return ve
}

func synchronise(ov *overlay, main Elements, veneer Elements) {

	// Delete elements that no longer exist.

	var toDelete []string
	for name := range ov.elements {
		if !main.Contains(name) && !veneer.Contains(name) {
			toDelete = append(toDelete, name)
			delete(ov.elements, name)
			ov.shown.Delete(name)
		}
	}
	if len(toDelete) > 0 {
		ov.updateViewers(overlayUpdate{Delete: toDelete})
	}

	// Hide elements that are no longer shown.

	var toHide []string
	for name := range ov.shown {
		if !main.Shows(name) && !veneer.Shows(name) {
			toHide = append(toHide, name)
			ov.shown.Delete(name)
		}
	}
	if len(toHide) > 0 {
		ov.updateViewers(overlayUpdate{Hide: toHide})
	}

	// Create elements that are to be shown but don't yet exist.

	var (
		toCreate []string
		ahead    = map[string]Element{}
	)
	maps.Copy(ahead, main.Ahead)
	maps.Copy(ahead, veneer.Ahead)
	for name, el := range ahead {

		if _, ok := ov.elements[name]; !ok {
			// Just creating a placeholder element. Created are hidden by
			// default.
			toCreate = append(toCreate, name)
		}

		ov.elements[name] = el
		ov.shown.Add(name)
	}
	if len(toCreate) > 0 {
		ov.updateViewers(overlayUpdate{Create: toCreate})
	}

	// Update and show the shown elements.

	if toUpdate := ov.sortedShown(); len(toUpdate) > 0 {
		ov.updateViewers(overlayUpdate{Update: toUpdate})
	}
}

// updateViewers with the given update on their update channels.
//
// Remove any viewers whose HTTP connections have closed and close their update
// channel.
func (ov overlay) updateViewers(u overlayUpdate) {
	for v := range ov.viewers {
		select {
		case v.uc <- u:
		case <-v.done:
			close(v.uc)
			delete(ov.viewers, v)
		}
	}
}

// A viewer of an overlay.
type viewer struct {
	uc   chan overlayUpdate // Closed by the overlayer end.
	done <-chan struct{}    // Closed by the web handler end on handler exit.
}

type uiElement struct {
	Name            string `json:"name"`
	InnerHTML       string `json:"innerHTML"`
	InnerHTMLHash   string `json:"innerHTMLHash"`
	Left            string `json:"left"`
	Top             string `json:"top"`
	Transform       string `json:"transform"`
	TransformOrigin string `json:"transformOrigin"`
	ZIndex          int    `json:"zIndex"`
}

type element struct {
	uiElement

	layer int
	z     float64
}

// The overlay update sent to clients in JSON format.
//
// Only one field is used at a time.
type overlayUpdate struct {
	// Names of newly created elements.
	Create []string `json:"create"`
	// Names of elements to delete.
	Delete []string `json:"delete"`
	// Names of elements to hide.
	Hide []string `json:"hide"`
	// An update with the state of the shown elements.
	Update []element `json:"update"`
}

// XToLeft maps a screen space X coordinate to a CSS percentage value of the
// width of the screen's HTML element starting from the left edge of the screen.
//
// The visible screen space spans X values given in the range
//
//	(-0.5 * screen ratio) ... (0.5 * screen ratio)
func XToLeft(x float64) string {
	xpc := 100 * (x + (0.5 * mech.WToHPixelsRatio)) / mech.WToHPixelsRatio
	return strconv.FormatFloat(xpc, 'f', -1, 64) + "%"
}

// YToTop maps a screen space Y coordinate to a CSS percentage value of the
// height of the screen's HTML element starting from the top edge of the screen.
//
// The visible screen space spans Y values given in the range -0.5 ... 0.5 where
// 0.5 corresponds to the top edge of the screen.
func YToTop(y float64) string {
	ypc := 100 * (-y + 0.5)
	return strconv.FormatFloat(ypc, 'f', -1, 64) + "%"
}

// addViewer to the overlayer and return the added viewer which gets updated
// with the given shutterbug's overlay elements any time there is a change.
//
// After the argument channel is closed no more updates are sent to the viewer
// and it is eventually removed from the overlayer.
func (ovr Overlayer) addViewer(
	done <-chan struct{},
	shutterbug string,
	send func(toCreate overlayUpdate) error,
) (viewer, error) {

	ovr.m.Lock()
	defer ovr.m.Unlock()

	var (
		ok bool
		ov *overlay
	)
	ov, ok = ovr.overlays[shutterbug]
	if !ok {
		ov = newOverlay()
		ovr.overlays[shutterbug] = ov
	}

	v := viewer{done: done, uc: make(chan overlayUpdate)}
	ov.viewers[v] = struct{}{}

	toCreate := ov.names()
	if len(toCreate) > 0 {
		return v, send(overlayUpdate{Create: toCreate})
	}

	return v, nil
}

// NewOverlayerHandleFunc creates a HTTP request handler which allows clients to
// interface with the overlayer.
//
// The handler upgrades the HTTP request to a websocket. The websocket's first
// ever incoming message should be a valid shutterbug.
func NewOverlayerHandleFunc(
	ovr Overlayer,
	shutterbugs mechshutterbug.Containser,
) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		c, shutterbug, err := mechshutterbug.UpgradeAndVerify(
			w, r, ovr.Logger, "overlayer", shutterbugs)
		if err != nil {
			return
		}
		defer c.Close()

		done := make(chan struct{})

		go func() {
			if _, _, err := c.ReadMessage(); err != nil {
				if !websocket.IsCloseError(
					err,
					websocket.CloseNormalClosure,
					websocket.CloseGoingAway) {
					ovr.Logger.Println("overlayer: ReadMessage: ", err)
				}
			} else {
				ovr.Logger.Println(
					"overlayer: ReadMessage: received unexpected message")
			}
			close(done)
		}()

		v, err := ovr.addViewer(
			done,
			shutterbug,
			func(init overlayUpdate) error {
				return c.WriteJSON(init)
			},
		)
		if err != nil {
			if err != websocket.ErrCloseSent {
				ovr.Logger.Println("overlayer: initial WriteJSON: ", err)
			}
			return
		}

		// Keys are element names, values are innerHTML hashes.
		current := map[string]string{}

		for {
			update, ok := <-v.uc
			if !ok {
				break
			}

			// Don't resend innerHTML currently displayed by the client.
			if len(update.Update) > 0 {
				for i, u := range update.Update {
					if current[u.Name] == u.InnerHTMLHash {
						update.Update[i].InnerHTML = ""
						continue
					}
					current[u.Name] = u.InnerHTMLHash
				}
			} else if len(update.Delete) > 0 {
				for _, name := range update.Delete {
					delete(current, name)
				}
			}

			if err = c.WriteJSON(update); err != nil {
				if err != websocket.ErrCloseSent {
					ovr.Logger.Println("overlayer: WriteJSON: ", err)
				}
				break
			}
		}
	}
}
