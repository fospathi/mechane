/*
Package mechraster provides a rasteriser that can draw scenes.
*/
package mechraster

import (
	"sync"

	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/wiregl"
)

// A Rasteriser accepts scenes and then returns their pixels.
type Rasteriser interface {
	Rasterise(wiregl.Scene) (wiregl.Pixels, error)
}

// NewEmbedded constructs a new Embedded rasteriser. It is a part of the same
// process as the caller.
func NewEmbedded() (*Embedded, wiregl.MainThread, <-chan error) {
	var (
		ec = make(chan error, 1)
		mc wiregl.MainThread
	)
	mc, rc := wiregl.NewContext(wiregl.ContextSpec{
		Samples: mech.MSAA,
		//Debug:      true,
	})
	return &Embedded{initRes: rc, ec: ec}, mc, ec
}

// An Embedded rasteriser that implements the rasteriser interface.
type Embedded struct {
	cx  *wiregl.Context
	err error
	m   sync.Mutex

	gotInitRes bool
	ec         chan<- error
	initRes    <-chan wiregl.InitResult
}

// Rasterise the given scene.
//
// To work the main thread needs to be concurrently executing functions sent
// over the receiver rasteriser's channel-of-functions returned at construction.
func (ras *Embedded) Rasterise(
	sc wiregl.Scene,
) (wiregl.Pixels, error) {

	ras.m.Lock()
	defer ras.m.Unlock()

	if !ras.gotInitRes {
		initRes := <-ras.initRes
		ras.gotInitRes = true
		if initRes.Err != nil {
			ras.err = initRes.Err
			ras.ec <- initRes.Err
			return wiregl.Pixels{}, initRes.Err
		}
		ras.cx = initRes.Context
	}
	if ras.err != nil {
		return wiregl.Pixels{}, ras.err
	}
	// TODO verify the rasterise result is sensible.
	return ras.cx.Rasterise(sc), nil
}
