/* Package style contains some commonly used colours and colour gradients. */
package style

import (
	"gitlab.com/fospathi/wire/colour"
	"gitlab.com/fospathi/wire/colour/rgb"
)

const Thickness = 0.02

var (
	Spectrum, _ = colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())

	SymmetricalSpectrum = func() *colour.Join {
		gradPart1, _ := colour.NewLChMajorGrad(
			rgb.Red.LCh(), rgb.Yellow.LCh(),
		)
		gradPart2, _ := colour.NewLChMajorGrad(
			rgb.Yellow.LCh(), rgb.Red.LCh(),
		)
		j := colour.NewJoin()
		j.Append(gradPart1, 1)
		j.Append(gradPart2, 1)
		return j
	}()
)

var (
	Black       = colour.NewMonoGrad(rgb.Black.LCh())
	Cyan        = colour.NewMonoGrad(rgb.Cyan.LCh())
	DarkOrange  = colour.NewMonoGrad(rgb.Darkorange.LCh())
	DeepSkyBlue = colour.NewMonoGrad(rgb.Deepskyblue.LCh())
	Pink        = colour.NewMonoGrad(rgb.Pink.LCh())
	Purple      = colour.NewMonoGrad(rgb.Purple.LCh())
)
