/*
Package txtinput provides textfield inputs.
*/
package txtinput

import (
	"time"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mico"
	"gitlab.com/fospathi/wire/colour/rgb"
)

// TextfieldModel is a model with a text field.
type TextfieldModel struct {
	FormNode       *mechtree.Node
	Textfield1     world.Textfield
	Textfield2     world.Textfield
	Textfield1Node *mechtree.Node
	Textfield2Node *mechtree.Node
}

func NewTextfieldModel(orient ...maf.Vec) TextfieldModel {
	tfNode := mechtree.NewNode()
	tfNode.Orient(maf.NewFrame(orient...))
	tf1Node := mechtree.NewNode()
	tf2Node := mechtree.NewNode()
	tfNode.AddChild(tf1Node)
	tfNode.AddChild(tf2Node)
	tf2Node.Orient(maf.NewFrame(maf.Vec{X: 0, Y: -1.5, Z: 0}))

	tf1 := world.Textfield{
		EffectName:  "test-input",
		ElementName: "test-input-textfield-1",

		Cloggable:   world.Cloggable{ClogDuration: 4 * time.Second},
		Icon1:       mico.Round.Search,
		Icon2:       mico.Round.Done,
		Icon2Colour: rgb.Mediumseagreen.String(),
		Label:       "Hi, world!",
		Mask:        false,
		Placeholder: "...Enter message here.",
		Size:        0,
		Spellcheck:  false,
		Title:       "Input some random text!",
	}

	tf2 := world.Textfield{
		EffectName:  "test-input-2",
		ElementName: "test-input-textfield-2",

		Cloggable:   world.Cloggable{ClogDuration: 120 * time.Second},
		Icon1:       mico.Round.Search,
		Icon2:       mico.Round.Done,
		Icon2Colour: rgb.Mediumseagreen.String(),
		Label:       "Hi, world!",
		Mask:        false,
		Placeholder: "...Enter more text here.",
		Size:        0,
		Spellcheck:  false,
		Title:       "Input some more random text!",

		Button: world.Button{
			EffectName: "test-input-2",

			Cloggable: world.Cloggable{ClogDuration: 120 * time.Second},
			Icon:      mico.Round.Warehouse,
			Title:     "A button that warehouses something.",
		},
	}

	tf1.Overlay(tf1Node)
	tf2.Overlay(tf2Node)

	return TextfieldModel{
		FormNode:       tfNode,
		Textfield1:     tf1,
		Textfield2:     tf2,
		Textfield1Node: tf1Node,
		Textfield2Node: tf2Node,
	}
}
