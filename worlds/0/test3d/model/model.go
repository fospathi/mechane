/* Package model aggregates the various sub models into one model. */
package model

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/cams"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/shape"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/txtinput"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/veneer"
)

type Model struct {
	IsPaused bool
	T        float64
	Cam      *mechtree.Node

	AnimationRoot *mechtree.Node

	NavPanel     veneer.NavigationPanelModel
	CamHomeFrame maf.Frame

	Textfield txtinput.TextfieldModel

	BlinkingCam     *cams.BlinkingCamModel
	OscillatingCams *cams.OscillatingCamsModel

	BezCircTanLoc shape.BezCircTanLocModel
	Cos           shape.CosModel
	Cycloid       shape.CycloidModel
	Grids         shape.GridsModel
	Sinusoid      shape.SinusoidModel
	Spiral        shape.SpiralModel
	Standing      shape.StandingWaveModel
}
