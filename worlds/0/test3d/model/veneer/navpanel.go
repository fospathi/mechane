/*
Package veneer implements a navigation panel that uses the veneer to stay
visually above the rest of the world.
*/
package veneer

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mico"
)

type NavigationPanelModel struct {
	HomeButton     world.Button
	HomeButtonNode *mechtree.Node
}

func NewNavigationPanelModel() NavigationPanelModel {
	homeButton := world.Button{
		EffectName:     "home button",
		ElementName:    "home goto button",
		Icon:           mico.Round.Home,
		OriginPosition: mechtree.IconCenter,
		Title:          "Goto home.",
	}
	homeButtonNode := mechtree.NewNode()
	homeButtonFrame := maf.StdFrame
	homeButtonFrame.O.Z = -0.01
	homeButtonNode.Orient(homeButtonFrame)
	homeButton.Overlay(homeButtonNode)

	return NavigationPanelModel{
		HomeButton:     homeButton,
		HomeButtonNode: homeButtonNode,
	}
}
