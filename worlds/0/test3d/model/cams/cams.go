/*
Package cams provides models which simulate cameras being added and removed,
sometimes in an erratic manner.
*/
package cams

import (
	"fmt"
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/world/dolly"
)

type StandardCamModel struct {
	cam  world.Camera
	Node *mechtree.Node
}

func (mdl *StandardCamModel) Create(wld world.Standard) {
	wld.CameraAdjustments.Create(mdl.cam)
}

func NewStandardCamModel(
	name string, position maf.Vec, lookAt maf.Vec,
) StandardCamModel {
	if position == lookAt {
		lookAt = position.Add(maf.Vec{X: 1, Y: 0, Z: 0})
	}

	node := mechtree.NewNode()
	veneerNode := mechtree.NewNode()
	cam := world.Camera{
		BackgroundAlpha:  0.0,
		BackgroundColour: world.DefaultBackgroundColour,
		Camera: mechtree.Camera{
			Lens:    world.DefaultPerspectiveLens,
			Inverse: world.DefaultPerspectiveLensInverse,
			Name:    name,
			Node:    node,
		},
		Veneer: mechtree.Camera{
			Lens:    world.DefaultVeneerLens,
			Inverse: world.DefaultVeneerLensInverse,
			Name:    name + "Veneer",
			Node:    veneerNode,
		},
		Dolly: dolly.SpatialJetPack,
	}
	cam.Node.Orient(maf.Frame{O: position}.View(lookAt))
	return StandardCamModel{
		cam:  cam,
		Node: node,
	}
}

type BlinkingCamModel struct {
	cam     world.Camera
	delay   float64
	enabled bool
	period  float64
	periods int

	Node *mechtree.Node
}

func (mdl *BlinkingCamModel) Enable() {
	mdl.enabled = true
}

func (mdl *BlinkingCamModel) Update(wld world.Standard, t float64) {
	if !mdl.enabled {
		return
	}

	t -= mdl.delay
	if t < 0 {
		return
	}

	// Only update at the start of a new period.
	i := int(math.Floor(t / mdl.period))
	if i == mdl.periods {
		return
	}
	defer func() {
		mdl.periods = i
	}()

	if i%2 == 0 {
		wld.CameraAdjustments.Create(mdl.cam)
	} else {
		wld.CameraAdjustments.Delete(mdl.cam.Name)
	}
}

func NewBlinkingCamModel(
	name string, delay float64, period float64,
) *BlinkingCamModel {

	node := mechtree.NewNode()
	veneerNode := mechtree.NewNode()
	cam := world.Camera{
		BackgroundAlpha:  0,
		BackgroundColour: world.DefaultBackgroundColour,
		Camera: mechtree.Camera{
			Lens:    world.DefaultPerspectiveLens,
			Inverse: world.DefaultPerspectiveLensInverse,
			Name:    name,
			Node:    node,
		},
		Veneer: mechtree.Camera{
			Lens:    world.DefaultVeneerLens,
			Inverse: world.DefaultVeneerLensInverse,
			Name:    name + "Veneer",
			Node:    veneerNode,
		},
		Dolly: dolly.SpatialJetPack,
	}

	return &BlinkingCamModel{
		cam:     cam,
		delay:   delay,
		enabled: false,
		period:  period,
		periods: -1,

		Node: node,
	}
}

type OscillatingCamsModel struct {
	delay   float64
	enabled bool
	period  float64 // The duration between camera changes.
	periods int     // The number of periods progressed through so far.
	vCam    []world.Camera

	Node *mechtree.Node
}

func (mdl *OscillatingCamsModel) Enable() {
	mdl.enabled = true
}

func (mdl *OscillatingCamsModel) Update(wld world.Standard, t float64) {
	if !mdl.enabled {
		return
	}

	t -= mdl.delay
	if t < 0 {
		return
	}

	// Only update at the start of a new period.
	i := int(math.Floor(t / mdl.period))
	if i == mdl.periods {
		return
	}
	defer func() {
		mdl.periods = i
	}()

	n := len(mdl.vCam)
	creating := (i/n)%2 == 0 // Is it the creating or deleting phase?
	bulk := (i/(2*n))%2 == 0 // Is it the single or bulk phase?
	if bulk {
		switch i % (2 * n) {
		case 0:
			for _, c := range mdl.vCam {
				wld.CameraAdjustments.Create(c)
			}
		case n - 1:
			for _, c := range mdl.vCam {
				wld.CameraAdjustments.Delete(c.Name)
			}
		}
	} else {
		if creating {
			wld.CameraAdjustments.Create(mdl.vCam[i%n])
		} else {
			wld.CameraAdjustments.Delete(mdl.vCam[(n-1)-i%n].Name)
		}
	}
}

func NewOscillatingCamsModel(
	n int, namePrefix string, delay float64, period float64,
) *OscillatingCamsModel {
	veneerNode := mechtree.NewNode()
	node := mechtree.NewNode()
	var vCam []world.Camera

	for i := 0; i < n; i++ {
		vCam = append(vCam, world.Camera{
			BackgroundAlpha:  0,
			BackgroundColour: world.DefaultBackgroundColour,
			Camera: mechtree.Camera{
				Lens:    world.DefaultPerspectiveLens,
				Inverse: world.DefaultPerspectiveLensInverse,
				Name:    namePrefix + fmt.Sprintf("-%d", i),
				Node:    node,
			},
			Veneer: mechtree.Camera{
				Lens:    world.DefaultVeneerLens,
				Inverse: world.DefaultVeneerLensInverse,
				Name:    namePrefix + fmt.Sprintf("-veneer-%d", i),
				Node:    veneerNode,
			},
			Dolly: dolly.SpatialJetPack,
		})
	}

	return &OscillatingCamsModel{
		delay:   delay,
		enabled: false,
		period:  period,
		periods: -1,
		vCam:    vCam,

		Node: node,
	}
}
