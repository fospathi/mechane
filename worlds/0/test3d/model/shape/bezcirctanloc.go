package shape

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

// BezCircTanLocModel is in essence the tangency locus of the middle control
// point of a quadratic Bezier curve which is a tangent to a circle.
type BezCircTanLocModel struct {
	Depictioner     *wire.BezCircTangency
	TangencyPortion wire.BezCircTangencyPortion
}

func NewBezCircTanLoc() BezCircTanLocModel {

	bezCircTangencyFiguration := wire.BezCircTangencyFiguration{
		Bez: maf.Bez{
			A: maf.Vec{X: -15, Y: 0, Z: -20},
			B: maf.Vec{X: 0, Y: 11, Z: -15},
			C: maf.Vec{X: -10, Y: -10, Z: -10},
		},
		Sphere: maf.Sphere{
			C: maf.Vec{X: 0, Y: 10, Z: -15},
			R: 4,
		},

		CloserBranchT:  0,
		FartherBranchT: 0,

		CloserBranchControlCircleR:  1,
		FartherBranchControlCircleR: 1,

		CloserBranchTangentCircleR:  1,
		FartherBranchTangentCircleR: 1,

		CloserBranchZOffset:  4,
		FartherBranchZOffset: -4,
	}
	unitPortion := []mech.Portion{
		{
			In:       interval.In{0, 1},
			Colourer: style.Spectrum,
			Th:       [2]float64{style.Thickness, style.Thickness},
			Qual:     10,
		},
	}
	fullCircPortion := []mech.Portion{
		{
			In:       interval.In{0, maf.TwoPi},
			Colourer: style.Spectrum,
			Th:       [2]float64{style.Thickness, style.Thickness},
			Qual:     0,
		},
	}
	bezCircTangencyPortion := wire.BezCircTangencyPortion{
		Circle: fullCircPortion,

		AC: unitPortion,

		CloserBranchAB: unitPortion,
		CloserBranchCB: unitPortion,

		FartherBranchAB: unitPortion,
		FartherBranchCB: unitPortion,

		CloserBranchLocus: []mech.Portion{
			{
				In:       interval.In{0.05, 0.95},
				Colourer: style.Spectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     30,
			},
		},
		FartherBranchLocus: []mech.Portion{
			{
				In:       interval.In{0.05, 0.95},
				Colourer: style.Spectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     30,
			},
		},

		CloserBranchArc:           unitPortion,
		CloserBranchBezier:        unitPortion,
		CloserBranchControlCircle: fullCircPortion,
		CloserBranchTangentCircle: fullCircPortion,

		FartherBranchArc:           unitPortion,
		FartherBranchBezier:        unitPortion,
		FartherBranchControlCircle: fullCircPortion,
		FartherBranchTangentCircle: fullCircPortion,
	}
	bezCircTangency := wire.NewBezCircTangency(bezCircTangencyFiguration)
	bezCircTangency.Orientation.O.X = 0
	bezCircTangency.Orientation.O.Y = 0
	bezCircTangency.Orientation.O.Z = -3.0
	bezCircTangency.SetName("bezCircTangency")
	bezCircTangency.Depict(bezCircTangencyPortion)

	return BezCircTanLocModel{
		Depictioner:     bezCircTangency,
		TangencyPortion: bezCircTangencyPortion,
	}
}
