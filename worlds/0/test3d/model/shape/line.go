package shape

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

type LineModel struct {
	Depictioner mech.Depictioner
}

func NewLineModel() LineModel {
	line := wire.NewLineSeg(maf.LineSeg{
		P1: maf.Vec{X: 1, Y: 0, Z: 0},
		P2: maf.Vec{X: 4, Y: 3, Z: 0},
	})
	line.Orientation.O.X = 3
	line.SetName("line")
	line.Depict(mech.Portion{
		In:       interval.In{0, 1},
		Colourer: style.Black,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})

	return LineModel{
		Depictioner: line,
	}
}
