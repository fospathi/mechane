package shape

import (
	"time"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"

	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
)

type CosModel struct {
	Button      world.ToggleButton
	ButtonNode  *mechtree.Node
	Depictioner mech.Depictioner
	Node        *mechtree.Node
}

func NewCosModel(orient ...maf.Vec) CosModel {
	cos := wire.NewCos()
	cos.SetName("cos")
	cos.Depict(mech.Portion{
		In:       interval.PiIn,
		Colourer: style.Spectrum,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})
	cosNode := mechtree.NewNode()
	cosNode.AddDepictioner(cos)

	cosButton := world.ToggleButton{
		Button: world.Button{
			EffectName:     "cos button",
			Cloggable:      world.Cloggable{ClogDuration: 5000 * time.Millisecond},
			ElementName:    "cos button 1",
			Orientation:    mechtree.XYFront,
			OriginPosition: mechtree.BottomRight,
			Title:          "Disappear me!",
			Text:           "Cos Button",
		},
		OffTitle: "Appear me!",
	}
	cosButtonNode := mechtree.NewNode()
	cosNode.AddChild(cosButtonNode)
	cosButtonNode.Orient(maf.NewFrame(orient...))
	cosButton.Overlay(cosButtonNode)

	return CosModel{
		Button:      cosButton,
		ButtonNode:  cosButtonNode,
		Depictioner: cos,
		Node:        cosNode,
	}
}
