package shape

import (
	_ "embed"
	"fmt"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

//go:embed wave.html
var standingDoc string

type StandingWaveModel struct {
	Depictioner *wire.StandingWave
	Node        *mechtree.Node
	Portion     wire.StandingWavePortion
}

func NewStandingWaveModel(orient ...maf.Vec) StandingWaveModel {
	portion := wire.StandingWavePortion{
		Standing: []mech.Portion{
			{
				In:       interval.In{-20, 20},
				Colourer: style.Spectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
		Rightwards: []mech.Portion{
			{
				In:       interval.In{-20, 20},
				Colourer: style.Spectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
		Leftwards: []mech.Portion{
			{
				In:       interval.In{-20, 20},
				Colourer: style.Spectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
	}
	standing := wire.NewStandingWave(wire.StandingWaveFiguration{
		Standing: kirv.Standing{
			A: 15,
			L: 10,
			W: maf.QuartPi,
		},
		ExpDecay:          kirv.ExpDecay{},
		T:                 0,
		RightwardsZOffset: 0,
		LeftwardsZOffset:  0,
	})
	standing.SetName("standing")
	standing.Depict(portion)

	standingAscii := &world.Asciidoc{
		ElementName: "standingDoc",

		AsciiDocHTML: standingDoc,
		Title:        "A description of a standing wave.",

		AsciidoctorDefaultStyle: false,
	}

	standingNode := mechtree.NewNode()
	standingNode.AddDepictioner(standing)
	standingNode.Orient(maf.NewFrame(orient...))
	err := standingAscii.Overlay(standingNode)
	if err != nil {
		fmt.Printf("err: %v\n", err)
	}

	return StandingWaveModel{
		Depictioner: standing,
		Node:        standingNode,
		Portion:     portion,
	}
}
