package shape

import (
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

type CircleModel struct {
	Depictioner mech.Depictioner
}

func NewCircleModel() CircleModel {
	circle := wire.NewCircle(2)
	circle.Orientation.O.X = 4
	circle.SetName("circle")
	circle.Depict(mech.Portion{
		In:       interval.PiIn,
		Colourer: style.DeepSkyBlue,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})

	return CircleModel{
		Depictioner: circle,
	}
}
