package shape

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

type BullseyeModel struct {
	Node *mechtree.Node
}

func NewBullseyeModel() BullseyeModel {
	const (
		lineZ = -0.001
	)

	verticalLine := wire.NewLineSeg(maf.LineSeg{
		P1: maf.Vec{X: 0, Y: -0.3, Z: lineZ},
		P2: maf.Vec{X: 0, Y: 0.3, Z: lineZ},
	})
	verticalLine.SetName("bullseyeVerticalLine")
	verticalLine.Depict(mech.Portion{
		In:       interval.In{0, 1},
		Colourer: style.DeepSkyBlue,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})

	horizontalLine := wire.NewLineSeg(maf.LineSeg{
		P1: maf.Vec{X: -0.3, Y: 0, Z: lineZ},
		P2: maf.Vec{X: 0.3, Y: 0, Z: lineZ},
	})
	horizontalLine.SetName("bullseyeHorizontalLine")
	horizontalLine.Depict(mech.Portion{
		In:       interval.In{0, 1},
		Colourer: style.DeepSkyBlue,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})

	circle := wire.NewCircle(0.2)
	circle.SetName("bullseyeCircle")
	circle.Depict(mech.Portion{
		In:       interval.PiIn,
		Colourer: style.Purple,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})

	bullseyeNode := mechtree.NewNode()
	bullseyeFrame := maf.StdFrame
	bullseyeFrame.O.Z = -0.5
	bullseyeNode.Orient(bullseyeFrame)
	bullseyeNode.AddDepictioners(circle, horizontalLine, verticalLine)

	return BullseyeModel{
		Node: bullseyeNode,
	}
}
