package shape

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
	"gitlab.com/fospathi/wire/colour"
	"gitlab.com/fospathi/wire/colour/rgb"
)

type SinModel struct {
	Depictioner mech.Depictioner
}

func NewSinModel() SinModel {
	sin := wire.NewSin()
	sin.SetName("sin")
	sin.Orientation = sin.Orientation.LocalRotPosY(maf.HalfPi / 2)
	sin.Depict(mech.Portion{
		In:       interval.PiIn,
		Colourer: colour.NewMonoGrad(rgb.Black.LCh()),
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})

	return SinModel{
		Depictioner: sin,
	}
}

type SinPlotModel struct {
	Depictioner mech.Depictioner
}

func NewSinPlotModel() SinPlotModel {
	sinPlot := wire.NewPlot(func(t float64) maf.Vec {
		return maf.Vec{X: t, Y: math.Sin(t)}
	})
	sinPlot.Orientation.O.X = 0
	sinPlot.Orientation.O.Y = 0
	sinPlot.Orientation.O.Z = 2
	sinPlot.SetName("sinPlot")
	sinPlot.Depict(mech.Portion{
		In:       interval.In{0, maf.TwoPi},
		Colourer: style.Spectrum,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     10,
	})

	return SinPlotModel{
		Depictioner: sinPlot,
	}
}

type TimeDecaying struct {
	Depictioner *wire.Sinusoid
	Portion     mech.Portion
}

type SinusoidModel struct {
	Node         *mechtree.Node
	TimeDecaying TimeDecaying
}

func NewSinusoidModel() SinusoidModel {

	sinusoid1 := wire.NewSinusoid(wire.SinusoidFiguration{
		Sinusoid: kirv.Sinusoid{
			A: 4,
			L: 2,
			V: 1,
		},
		ExpDecay: kirv.ExpDecay{},
		T:        0,
	})
	sinusoid1.Orientation.O.X = 10
	sinusoid1.Orientation.O.Y = 5
	sinusoid1.Orientation.O.Z = -7
	sinusoid1.SetName("sinusoid")
	sinusoid1.Depict(mech.Portion{
		In:       interval.In{0, 4},
		Colourer: style.Pink,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})

	sinusoid2 := wire.NewSinusoid(wire.SinusoidFiguration{
		Sinusoid: kirv.Sinusoid{
			A: 4,
			L: 1,
			V: 1,
		},
		ExpDecay: kirv.ExpDecay{},
		T:        0,
	})
	sinusoid2.Orientation.O.X = 15
	sinusoid2.Orientation.O.Y = 4
	sinusoid2.Orientation.O.Z = -6
	sinusoid2.SetName("sinusoid2")
	sinusoid2.Depict(mech.Portion{
		In:       interval.In{0, 4},
		Colourer: style.DeepSkyBlue,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})

	portion := mech.Portion{
		In:       interval.In{0, 8},
		Colourer: style.Spectrum,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	}

	spaceDecayingSinusoid := wire.NewSinusoid(wire.SinusoidFiguration{
		Sinusoid: kirv.Sinusoid{
			A: 3,
			L: 2,
			V: 1,
		},
		ExpDecay: kirv.ExpDecay{
			Space: kirv.HalfLife(2).Decay(),
		},
		T: 0,
	})
	spaceDecayingSinusoid.Orientation.O.X = 13
	spaceDecayingSinusoid.Orientation.O.Y = -2
	spaceDecayingSinusoid.Orientation.O.Z = -5
	spaceDecayingSinusoid.SetName("spaceDecayingSinusoid")
	spaceDecayingSinusoid.Depict(portion)

	timeDecayingSinusoidFiguration := wire.SinusoidFiguration{
		Sinusoid: kirv.Sinusoid{
			A: 3,
			L: 2,
			V: 1,
		},
		ExpDecay: kirv.ExpDecay{
			Time:  kirv.HalfLife(3).Decay(),
			Space: kirv.HalfLife(2).Decay(),
		},
		T: 0,
	}
	timeDecayingSinusoid := wire.NewSinusoid(timeDecayingSinusoidFiguration)
	timeDecayingSinusoid.Orientation.O.X = 14
	timeDecayingSinusoid.Orientation.O.Y = -3
	timeDecayingSinusoid.Orientation.O.Z = -4
	timeDecayingSinusoid.SetName("timeDecayingSinusoid")
	timeDecayingSinusoid.Depict(portion)

	root := mechtree.NewNode()
	root.AddDepictioners(sinusoid1, sinusoid2, spaceDecayingSinusoid,
		timeDecayingSinusoid)

	return SinusoidModel{
		Node: root,
		TimeDecaying: TimeDecaying{
			Depictioner: timeDecayingSinusoid,
			Portion:     portion,
		},
	}
}
