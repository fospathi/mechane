package shape

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

type ConeModel struct {
	Depictioner mech.Depictioner
}

func NewConeModel() ConeModel {
	cone := wire.NewCone(wire.ConeFiguration{
		R:  4,
		H:  4,
		LC: 5,
		CC: 4,
	})
	cone.Orientation.O.X = -4
	cone.SetName("cone")
	cone.Depict(wire.ConePortion{
		Concentric: []mech.Portion{
			{
				In:       interval.In{maf.QuartPi, maf.TwoPi - maf.QuartPi},
				Colourer: style.DarkOrange,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
		Directrix: []mech.Portion{
			{
				In:       interval.In{maf.QuartPi, maf.TwoPi - maf.QuartPi},
				Colourer: style.Spectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
		Lateral: []mech.Portion{
			{
				In:       interval.In{maf.QuartPi, maf.TwoPi - maf.QuartPi},
				Colourer: style.Spectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
	})

	return ConeModel{
		Depictioner: cone,
	}
}
