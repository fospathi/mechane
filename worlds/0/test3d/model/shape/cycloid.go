package shape

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

type CycloidModel struct {
	Circle        *wire.Circle
	CirclePortion func(float64) mech.Portion
	Node          *mechtree.Node
	Parametric    kirv.Parametric
}

func NewCycloidModel() CycloidModel {

	cycloidRad := 2.0
	cycloidParametric := kirv.NewCycloid(cycloidRad)
	cycloid := wire.NewCycloid(cycloidRad)
	cycloid.SetName("cycloid")
	cycloid.Depict(mech.Portion{
		In:       interval.In{0, maf.TwoPi},
		Colourer: style.Spectrum,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})

	osc := cycloidParametric.Osculating(0)
	cycloidCirc := wire.NewCircle(osc.R)
	cycloidCirc.SetName("cycloidCirc")
	cycloidCirc.Depict(mech.Portion{
		In:       interval.In{0, maf.TwoPi},
		Colourer: style.Spectrum,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})

	cycloidNode := mechtree.NewNode()
	cycloidOrigin := maf.Vec{X: -cycloidRad * maf.TwoPi / 2, Y: -8, Z: -5}
	cycloidNode.Orient(
		maf.Frame{
			O:    cycloidOrigin,
			Axes: maf.StdBasis.Local(maf.NewXRot(maf.HalfPi)),
		})
	cycloidNode.AddDepictioners(cycloid, cycloidCirc)

	return CycloidModel{
		Circle: cycloidCirc,
		CirclePortion: func(ang float64) mech.Portion {
			return mech.Portion{
				In:       interval.In{0, maf.TwoPi},
				Colourer: style.Spectrum.Colours(ang/maf.TwoPi, ang/maf.TwoPi),
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			}
		},
		Node:       cycloidNode,
		Parametric: cycloidParametric,
	}
}
