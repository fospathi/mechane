package shape

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

type HelixModel struct {
	Depictioner mech.Depictioner
}

func NewHelixModel() HelixModel {
	helix := wire.NewHelix(kirv.Helix{
		P: 1,
		R: 2,
	})
	helix.Orientation.O.X = 3
	helix.Orientation.O.Y = -3
	helix.Orientation.O.Z = -5
	helix.SetName("helix")
	helix.Depict(mech.Portion{
		In:       interval.In{0, maf.TwoPi * 3},
		Colourer: style.Spectrum,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})

	return HelixModel{
		Depictioner: helix,
	}
}
