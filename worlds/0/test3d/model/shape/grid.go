package shape

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

type Disk struct {
	ButtonName  string
	ButtonNode  *mechtree.Node
	Depictioner mech.Depictioner
}

type Rect struct {
	ButtonName  string
	ButtonNode  *mechtree.Node
	Depictioner mech.Depictioner
}

type GridsModel struct {
	Disk Disk
	Rect Rect

	Node             *mechtree.Node
	RadioButtonGroup world.RadioButtonGroup
}

func NewGridsModel() GridsModel {

	disk := wire.NewGridDisk(wire.GridDiskFiguration{
		GridFiguration: wire.GridFiguration{
			GX: 2.5, GY: 2.5,
			CX: 0.6, CY: 0.6,
			OX: 0.3, OY: 0.3,
			ZX: 0.05,
		},
		R: 1.25,
	})
	disk.Orientation.O.X = 3
	disk.Orientation.O.Y = -3
	disk.SetName("gridDisk")
	disk.Depict(wire.GridPortion{
		X: []mech.Portion{
			{
				In:       interval.In{0, 1},
				Colourer: style.Spectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
		Y: []mech.Portion{
			{
				In:       interval.In{0, 1},
				Colourer: style.Spectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
	})

	rect := wire.NewGrid(wire.GridFiguration{
		GX: 2.5, GY: 2.5,
		CX: 0.6, CY: 0.6,
		//OX: 0.45, OY: -0.45,
		ZX: 0.05,
	})
	rect.Orientation.O.X = 0
	rect.Orientation.O.Y = -3
	rect.SetName("gridRect")
	rect.Depict(wire.GridPortion{
		X: mech.Portion{
			Caps:     [2]mech.Linecap{mech.Butt, mech.Butt},
			In:       interval.In{0, 1},
			Colourer: style.Spectrum,
			Th:       [2]float64{style.Thickness / 3, style.Thickness / 3},
			Qual:     0,
		}.DashArray(0.04, 0.02, 0.06),
		Y: []mech.Portion{
			{
				In:       interval.In{0, 1},
				Colourer: style.Spectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
	})

	diskGridButtonName := "diskGridButton"
	rectGridButtonName := "rectGridButton"
	gridRadioGroup := world.NewRadioButtonGroup(world.RadioButtonGroupSpec{
		Buttons: map[string]world.RadioButtonSpec{
			diskGridButtonName: {
				ElementName: diskGridButtonName + "Element",
				OffTitle:    "Enable disk grid",
				Text:        "disk grid",
				Title:       "Disable disk grid",
			},
			rectGridButtonName: {
				ElementName: rectGridButtonName + "Element",
				OffTitle:    "Enable rect grid",
				Text:        "square grid",
				Title:       "Disable rect grid",
			},
		},
		ClogDuration: mech.ClogDuration,
	})
	diskGridButtonNode := mechtree.NewNode()
	rectGridButtonNode := mechtree.NewNode()
	root := mechtree.NewNode()
	root.AddChild(diskGridButtonNode)
	root.AddChild(rectGridButtonNode)
	diskGridButtonNode.Orient(maf.NewFrame(maf.Vec{X: 3, Y: -3, Z: 0}))
	rectGridButtonNode.Orient(maf.NewFrame(maf.Vec{X: 0, Y: -3, Z: 0}))
	gridRadioGroup.Overlay(map[string]*mechtree.Node{
		diskGridButtonName: diskGridButtonNode,
		rectGridButtonName: rectGridButtonNode,
	})

	return GridsModel{
		Disk: Disk{
			ButtonName:  diskGridButtonName,
			ButtonNode:  diskGridButtonNode,
			Depictioner: disk,
		},
		Rect: Rect{
			ButtonName:  rectGridButtonName,
			ButtonNode:  rectGridButtonNode,
			Depictioner: rect,
		},

		Node:             root,
		RadioButtonGroup: gridRadioGroup,
	}
}
