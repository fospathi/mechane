package shape

import (
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

type CubeModel struct {
	Depictioner mech.Depictioner
}

func NewCubeModel() CubeModel {
	cube := wire.NewCube(5)
	cube.Orientation.O.X = 4
	cube.SetName("cube")
	cube.Depict(wire.CubePortion{
		Vertical: []mech.Portion{
			{
				In:       interval.In{0, 1},
				Colourer: style.SymmetricalSpectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
		Bottom: []mech.Portion{
			{
				In:       interval.In{0, 1},
				Colourer: style.SymmetricalSpectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
		Top: []mech.Portion{
			{
				In:       interval.In{0, 1},
				Colourer: style.SymmetricalSpectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
	})

	return CubeModel{
		Depictioner: cube,
	}
}
