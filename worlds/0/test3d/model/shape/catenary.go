package shape

import (
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

type CatenaryModel struct {
	Depictioner mech.Depictioner
}

func NewCatenaryModel() CatenaryModel {
	catenary := wire.NewCatenary(2)
	catenary.Orientation.O.X = -10
	catenary.SetName("catenary")
	catenary.Depict(mech.Portion{
		In:       interval.In{-4, 4},
		Colourer: style.Pink,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})
	return CatenaryModel{
		Depictioner: catenary,
	}
}
