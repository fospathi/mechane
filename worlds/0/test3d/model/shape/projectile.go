package shape

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

type ProjectileModel struct {
	Depictioner mech.Depictioner
}

func NewProjectileModel() ProjectileModel {
	horizontalSpeed := 3.0
	verticalSpeed := 15.0
	projectile := wire.NewTimeDepProjectile(kinem.Projectile{
		A: kinem.G,
		U: maf.Vec{X: horizontalSpeed, Y: verticalSpeed, Z: 0},
	})
	peak := projectile.TurningPoint()
	timeOfFlight := (peak.X * 2) / horizontalSpeed
	projectile.Orientation.O.X = 3
	projectile.Orientation.O.Y = 3
	projectile.Orientation.O.Z = -7
	projectile.SetName("projectile")
	projectile.Depict(mech.Portion{
		In:       interval.In{0, timeOfFlight},
		Colourer: style.Purple,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})

	return ProjectileModel{
		Depictioner: projectile,
	}
}
