package shape

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/mico"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

type SpiralModel struct {
	Button      world.Button
	ButtonNode  *mechtree.Node
	Depictioner mech.Depictioner
	Node        *mechtree.Node
}

func NewSpiralModel() SpiralModel {
	spiral := wire.NewTimeDepArchimedean(kinem.Archimedean{
		D: 1,
		V: 3,
		W: maf.TwoPi,
	})
	spiral.Orientation.O.X = 3
	spiral.Orientation.O.Y = 3
	spiral.Orientation.O.Z = -9
	spiral.SetName("spiral")
	spiral.Depict(mech.Portion{
		In:       interval.In{0, 3},
		Colourer: style.Spectrum,
		Th:       [2]float64{style.Thickness, style.Thickness},
		Qual:     0,
	})
	spiralNode := mechtree.NewNode()
	spiralNode.AddDepictioner(spiral)

	spiralButton := world.Button{
		EffectName:     "spiral button",
		ElementName:    "spiral goto button",
		Icon:           mico.Round.Place,
		OriginPosition: mechtree.IconCenter,
		Title:          "Goto spiral center!",
	}
	spiralButtonNode := mechtree.NewNode()
	spiralNode.AddChild(spiralButtonNode)
	spiralButtonNode.Orient(spiral.Orientation)
	spiralButton.Overlay(spiralButtonNode)

	return SpiralModel{
		Button:      spiralButton,
		ButtonNode:  spiralButtonNode,
		Depictioner: spiral,
		Node:        spiralNode,
	}
}
