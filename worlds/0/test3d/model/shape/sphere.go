package shape

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
)

type SphereModel struct {
	Depictioner mech.Depictioner
}

func NewSphereModel() SphereModel {
	sphere := wire.NewSphere(wire.SphereFiguration{
		R:  1,
		NX: 3, NY: 3, NZ: 3,
	})
	sphere.Orientation.O.X = 0
	sphere.Orientation.O.Y = 2
	sphere.SetName("sphere")
	sphere.Depict(wire.SpherePortion{
		X: []mech.Portion{
			{
				In:       interval.In{0, maf.TwoPi},
				Colourer: style.Cyan,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
		Y: []mech.Portion{
			{
				In:       interval.In{0, maf.TwoPi},
				Colourer: style.Cyan,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
		Z: []mech.Portion{
			{
				In:       interval.In{0, maf.TwoPi},
				Colourer: style.Cyan,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		},
	})

	return SphereModel{
		Depictioner: sphere,
	}
}
