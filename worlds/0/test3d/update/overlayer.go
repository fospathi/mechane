package update

import (
	"fmt"
	"time"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model"
)

func OverlayerButtons(wld world.Standard, mdl *model.Model, now time.Time) {

	w, _ := wld.ButtonClicks.Await(nil)
loop:
	for {
		select {
		case <-w:
			ev, _ := wld.ButtonClicks.Delete(w)
			w, _ = wld.ButtonClicks.Await(nil)

			name := ev.EffectName
			switch name {

			case mdl.Textfield.Textfield2.EffectName:
				if mdl.Textfield.Textfield2.Clogged() {
					break
				}
				mdl.Textfield.Textfield2.Clog(now)
				mdl.Textfield.Textfield2.Button.Clog(now)
				mdl.Textfield.Textfield2.Overlay(mdl.Textfield.Textfield2Node)

			case mdl.NavPanel.HomeButton.EffectName:
				wld.CameraRegistry.Associated(ev.Shutterbug) // Currently ignored.
				mdl.Cam.Orient(mdl.CamHomeFrame)
				mdl.AnimationRoot.AddChild(mdl.Cam)

			case mdl.Spiral.Button.EffectName:
				wld.CameraRegistry.Associated(ev.Shutterbug) // Currently ignored.
				mdl.Cam.Orient(maf.StdFrame)
				mdl.Spiral.ButtonNode.AddChild(mdl.Cam)

			case mdl.Cos.Button.EffectName:
				if mdl.Cos.Button.Clogged() {
					break
				}
				mdl.Cos.Button.Toggle(now)
				if mdl.Cos.Button.Off {
					mdl.Cos.Node.RemoveDepictioner(mdl.Cos.Depictioner)
				} else {
					mdl.Cos.Node.AddDepictioner(mdl.Cos.Depictioner)
				}
				mdl.Cos.Button.Overlay(mdl.Cos.ButtonNode)

			case mdl.Grids.Disk.ButtonName:
				if mdl.Grids.RadioButtonGroup.Clogged() {
					break
				}
				mdl.Grids.RadioButtonGroup.Click(name, now)
				if mdl.Grids.RadioButtonGroup[name].Off {
					mdl.Grids.Node.RemoveDepictioner(mdl.Grids.Disk.Depictioner)
				} else {
					mdl.Grids.Node.AddDepictioner(mdl.Grids.Disk.Depictioner)
					mdl.Grids.Node.RemoveDepictioner(mdl.Grids.Rect.Depictioner)
				}
				mdl.Grids.RadioButtonGroup.Overlay(map[string]*mechtree.Node{
					name:                      mdl.Grids.Disk.ButtonNode,
					mdl.Grids.Rect.ButtonName: mdl.Grids.Rect.ButtonNode,
				})

			case mdl.Grids.Rect.ButtonName:
				if mdl.Grids.RadioButtonGroup.Clogged() {
					break
				}
				mdl.Grids.RadioButtonGroup.Click(name, now)
				if mdl.Grids.RadioButtonGroup[name].Off {
					mdl.Grids.Node.RemoveDepictioner(mdl.Grids.Rect.Depictioner)
				} else {
					mdl.Grids.Node.AddDepictioner(mdl.Grids.Rect.Depictioner)
					mdl.Grids.Node.RemoveDepictioner(mdl.Grids.Disk.Depictioner)
				}
				mdl.Grids.RadioButtonGroup.Overlay(map[string]*mechtree.Node{
					mdl.Grids.Disk.ButtonName: mdl.Grids.Disk.ButtonNode,
					name:                      mdl.Grids.Rect.ButtonNode,
				})

			}

		default:
			break loop
		}
	}

	if mdl.Textfield.Textfield2.Button.Decloggable(now) {
		mdl.Textfield.Textfield2.Button.Declog()
	}

	if mdl.Cos.Button.Decloggable(now) {
		mdl.Cos.Button.Declog()
		mdl.Cos.Button.Overlay(mdl.Cos.ButtonNode)
	}

	if mdl.Grids.RadioButtonGroup.Decloggable(now) {
		mdl.Grids.RadioButtonGroup.Declog()
		mdl.Grids.RadioButtonGroup.Overlay(map[string]*mechtree.Node{
			mdl.Grids.Disk.ButtonName: mdl.Grids.Disk.ButtonNode,
			mdl.Grids.Rect.ButtonName: mdl.Grids.Rect.ButtonNode,
		})
	}
}

func OverlayerTextfields(wld world.Standard, mdl *model.Model, now time.Time) {

	w, _ := wld.TextfieldKeys.Await(nil)
loop:
	for {
		select {
		default:
			break loop

		case <-w:
			ev, _ := wld.TextfieldKeys.Delete(w)
			w, _ = wld.TextfieldKeys.Await(nil)

			switch ev.E.EffectName {

			case mdl.Textfield.Textfield1.EffectName:
				if ev.E.KeyDown {
					switch ev.E.Key {
					case "Enter":
						if mdl.Textfield.Textfield1.Clogged() {
							break
						}
						mdl.Textfield.Textfield1.Clog(now)
						mdl.Textfield.Textfield1.InputValue = ev.E.Value
						mdl.Textfield.Textfield1.Overlay(mdl.Textfield.Textfield1Node)
					}
				}

			}
		}
	}

	if mdl.Textfield.Textfield1.Decloggable(now) {
		mdl.Textfield.Textfield1.Declog()
		mdl.Textfield.Textfield1.Overlay(mdl.Textfield.Textfield1Node)
	}

	if mdl.Textfield.Textfield2.Decloggable(now) {
		mdl.Textfield.Textfield2.Declog()
		mdl.Textfield.Textfield2.Overlay(mdl.Textfield.Textfield2Node)
	}
}

func OverlayerWheelMoves(wld world.Standard, mdl *model.Model, now time.Time) {
	seq := wld.WheelMoves
	w, _ := seq.Await(nil)
loop:
	for {
		select {
		default:
			break loop

		case <-w:
			ev, _ := seq.Delete(w)
			w, _ = seq.Await(nil)

			fmt.Printf("effect name: %20s, deltaY: %v\n", ev.E.EffectName, ev.E.DeltaY)
		}
	}
}
