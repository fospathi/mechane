/*
Package update modifies the model in response to UI events and the relentless
passage of time.
*/
package update
