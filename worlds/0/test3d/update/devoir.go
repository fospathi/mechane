package update

import (
	"fmt"

	"gitlab.com/fospathi/mechane/mechserver/mechdevoir"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model"
	"gitlab.com/fospathi/universal/mech"
)

func Devoir(wld world.Standard, mdl *model.Model) {
	for {
		uic, err := wld.Devoir.Pop()
		if err != nil {
			break
		}
		switch uic.Value.(type) {
		case mechdevoir.PauseUIC:
			fmt.Printf("processing pause: %v\n", true)
			mdl.IsPaused = true
			wld.Devoir.Paused()
		case mechdevoir.PlayUIC:
			fmt.Printf("processing play: %v\n", true)
			mdl.IsPaused = false
			wld.Devoir.Playing()
		case mechdevoir.ShutdownUIC:
			wld.BeginShutdown(mech.ShutdownByClient)
			fmt.Printf("Received shutdown request from client.\n")
			fmt.Printf("Shutting down...\n")
		}
	}
}
