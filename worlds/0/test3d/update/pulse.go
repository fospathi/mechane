package update

import (
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model"
	"gitlab.com/fospathi/universal/watchable"
)

func Pulse(
	wld world.Standard,
	t watchable.ChronoShift,
	arrhythmic bool,
	mdl *model.Model,
) bool {

	select {
	case <-wld.ShutdownBegan():
		// Do shutdowny things here.
		wld.FinaliseShutdown()
		return false
	default:
	}

	if !mdl.IsPaused {
		mdl.T += t.DeltaT()
	}

	OverlayerButtons(wld, mdl, t.Time)
	OverlayerTextfields(wld, mdl, t.Time)
	OverlayerWheelMoves(wld, mdl, t.Time)
	Devoir(wld, mdl)

	if !mdl.IsPaused {
		Model(wld, mdl)
	}

	return true
}
