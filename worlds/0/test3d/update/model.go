package update

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model"
)

func Model(wld world.Standard, mdl *model.Model) {
	// fmt.Printf("update: T: %v\n", t.T())
	// fmt.Printf("update: t.Time: %v\n", t.Time)

	mdl.BlinkingCam.Update(wld, mdl.T)

	mdl.OscillatingCams.Update(wld, mdl.T)

	mdl.Sinusoid.TimeDecaying.Depictioner.T = math.Mod(mdl.T, 12.0)
	mdl.Sinusoid.TimeDecaying.Depictioner.Depict(
		mdl.Sinusoid.TimeDecaying.Portion)

	ang := math.Mod(mdl.T, maf.TwoPi)
	cycloidCirc := mdl.Cycloid.Parametric.Osculating(ang)
	mdl.Cycloid.Circle.Orientation.O = cycloidCirc.C.Add(maf.Vec{Z: -0.5})
	mdl.Cycloid.Circle.R = cycloidCirc.R
	mdl.Cycloid.Circle.Depict(mdl.Cycloid.CirclePortion(ang))

	bezCircTangencyPeriod := 10.0
	bezCircTangencyT :=
		math.Mod(mdl.T, bezCircTangencyPeriod) / bezCircTangencyPeriod
	mdl.BezCircTanLoc.Depictioner.CloserBranchT = bezCircTangencyT
	mdl.BezCircTanLoc.Depictioner.FartherBranchT = 1 - bezCircTangencyT
	mdl.BezCircTanLoc.Depictioner.Depict(mdl.BezCircTanLoc.TangencyPortion)

	mdl.Standing.Depictioner.T = mdl.T
	mdl.Standing.Depictioner.Depict(mdl.Standing.Portion)

}
