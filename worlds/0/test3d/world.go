/*
Package test3d is for the Mechane developer to manually test Mechane. Ordinary
Mechane users have no use for this world.
*/
package test3d

import (
	"embed"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/about"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/world/dolly"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/cams"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/shape"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/txtinput"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/veneer"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/update"
)

// WorldName is used by Mechane to identify this world.
const WorldName = "test3d"

//go:embed worldweb
var worldWeb embed.FS

// LaunchResources function returns the world's launch resources.
var LaunchResources = world.NewLaunchResources(
	world.LaunchResourcesSpec[*model.Model]{
		About: about.World{
			Author: []about.Author{about.Fospathi},
			Blurb:  []string{"A test world for the Mechane dev."},
			Link:   "https://gitlab.com/fospathi/mechane",
			Name:   WorldName,

			License:     about.CreativeCommons.PublicDomainDedication,
			Alternative: about.MITNoAttribution(2023, about.Fospathi.Name),
		},
		Init:      initWorld,
		PulseRate: world.Slow,
		Update:    update.Pulse,
		WorldWeb:  worldWeb,
	},
)

func initWorld(wld world.Standard) *model.Model {

	animationRoot := mechtree.NewNode()
	cam := mechtree.NewNode()
	camVeneer := mechtree.NewNode()
	animationRoot.AddChild(cam)
	camHomeFrame := maf.NewFrame(maf.Vec{X: 0, Y: 0, Z: 10})
	cam.Orient(camHomeFrame)
	wld.CameraAdjustments.Create(world.Camera{
		BackgroundAlpha:  0.0,
		BackgroundColour: world.DefaultBackgroundColour,
		Camera: mechtree.Camera{
			Lens:    world.DefaultPerspectiveLens,
			Inverse: world.DefaultPerspectiveLensInverse,
			Name:    "camera",
			Node:    cam,
		},
		Veneer: mechtree.Camera{
			Lens:    world.DefaultVeneerLens,
			Inverse: world.DefaultVeneerLensInverse,
			Name:    "cameraVeneer",
			Node:    camVeneer,
		},
		Dolly: dolly.SpatialJetPack,
	})

	camF := cam.Get()
	camF.O.Z = 10
	camF.O.Y = 3
	camF = camF.View(maf.Zero)
	cam.Orient(camF)

	rightCam := cams.NewStandardCamModel(
		"rightCam", maf.Vec{X: 10, Y: 3, Z: 0}, maf.Zero)
	rightCam.Create(wld)
	animationRoot.AddChild(rightCam.Node)

	leftCam := cams.NewStandardCamModel(
		"leftCam", maf.Vec{X: -10, Y: 3, Z: 0}, maf.Zero)
	leftCam.Create(wld)
	animationRoot.AddChild(leftCam.Node)

	rearCam := cams.NewStandardCamModel(
		"rearCam", maf.Vec{X: 0, Y: 3, Z: -10}, maf.Zero)
	rearCam.Create(wld)
	animationRoot.AddChild(rearCam.Node)

	// wld.CameraRegistry.Register(
	// 	"zoo A", "zoo B", "zoo C", "zoo D", "zoo E", "zoo F", "zoo G", "zoo H", "zoo I")

	// Blinking camera.
	blinkingCamModel := cams.NewBlinkingCamModel("Blinker", 10, 0.8)
	//blinkingCamModel.Enable()
	animationRoot.AddChild(blinkingCamModel.Node)

	// Oscillating camera.
	oscillatingCamsModel := cams.NewOscillatingCamsModel(4, "Cam", 5, 3)
	//oscillatingCamsModel.Enable()
	animationRoot.AddChild(oscillatingCamsModel.Node)

	// Veneer.

	navPanel := veneer.NewNavigationPanelModel()

	camVeneer.AddChild(navPanel.HomeButtonNode)

	// Input elements.

	textfield := txtinput.NewTextfieldModel(maf.Vec{X: 0, Y: 0.5, Z: 0})
	animationRoot.AddChild(textfield.FormNode)

	// Shapes.

	bezCircTanLocModel := shape.NewBezCircTanLoc()

	bullseyeModel := shape.NewBullseyeModel()
	camVeneer.AddChild(bullseyeModel.Node)

	catenaryModel := shape.NewCatenaryModel()

	cosModel := shape.NewCosModel(maf.Vec{X: 0, Y: 1, Z: 0})
	animationRoot.AddChild(cosModel.Node)

	coneModel := shape.NewConeModel()

	cubeModel := shape.NewCubeModel()

	cycloidModel := shape.NewCycloidModel()
	animationRoot.AddChild(cycloidModel.Node)

	gridsModel := shape.NewGridsModel()
	animationRoot.AddChild(gridsModel.Node)

	helixModel := shape.NewHelixModel()

	projectileModel := shape.NewProjectileModel()

	sinModel := shape.NewSinModel()

	sinPlotModel := shape.NewSinPlotModel()

	sinusoidModel := shape.NewSinusoidModel()
	animationRoot.AddChild(sinusoidModel.Node)

	sphereModel := shape.NewSphereModel()

	spiralModel := shape.NewSpiralModel()
	animationRoot.AddChild(spiralModel.Node)

	standingWaveModel :=
		shape.NewStandingWaveModel(maf.Vec{X: -15, Y: 15, Z: -15})
	animationRoot.AddChild(standingWaveModel.Node)

	circleModel := shape.NewCircleModel()
	lineModel := shape.NewLineModel()

	animationRoot.AddDepictioners(
		bezCircTanLocModel.Depictioner,
		catenaryModel.Depictioner,
		coneModel.Depictioner,
		cubeModel.Depictioner,
		helixModel.Depictioner,
		projectileModel.Depictioner,
		sinModel.Depictioner,
		sinPlotModel.Depictioner,
		sphereModel.Depictioner,
		standingWaveModel.Depictioner,

		lineModel.Depictioner,
		circleModel.Depictioner,
	)

	return &model.Model{
		Cam: cam,

		AnimationRoot: animationRoot,

		NavPanel:     navPanel,
		CamHomeFrame: camHomeFrame,

		Textfield: textfield,

		BlinkingCam:     blinkingCamModel,
		OscillatingCams: oscillatingCamsModel,

		BezCircTanLoc: bezCircTanLocModel,
		Cos:           cosModel,
		Cycloid:       cycloidModel,
		Grids:         gridsModel,
		Sinusoid:      sinusoidModel,
		Spiral:        spiralModel,
		Standing:      standingWaveModel,
	}
}
