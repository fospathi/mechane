//go:build ignore

package main // Convert AsciiDoc files to HTML.

import (
	"log"

	"gitlab.com/fospathi/mechane/mechui/asciidoc"
)

const rootDir = "../" // Convert AsciiDoc files in the filesystem rooted here.

func main() {
	if cnv, err := asciidoc.NewConverter(log.Default()); err != nil {
		log.Fatalln(err)
	} else if err := asciidoc.Convert(cnv, rootDir); err != nil {
		log.Fatalln(err)
	}
}
