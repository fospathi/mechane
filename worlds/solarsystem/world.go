package solarsystem

import (
	"embed"

	"gitlab.com/fospathi/mechane/about"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/solarsystem/model"
	"gitlab.com/fospathi/mechane/worlds/solarsystem/model/realm"
	"gitlab.com/fospathi/mechane/worlds/solarsystem/model/realm/lookup"
	"gitlab.com/fospathi/mechane/worlds/solarsystem/model/realm/observe"
	"gitlab.com/fospathi/mechane/worlds/solarsystem/update"
	"gitlab.com/fospathi/phyz/celestial"
	"gitlab.com/fospathi/phyz/celestial/solsys"
)

// WorldName is used by Mechane to identify this world.
const WorldName = "solarsystem"

//go:embed worldweb
var worldWeb embed.FS

// LaunchResources function returns the world's launch resources.
var LaunchResources = world.NewLaunchResources(
	world.LaunchResourcesSpec[*model.Model]{
		About: about.World{
			Author: []about.Author{about.Fospathi},
			Blurb:  []string{"A 3D model of the Solar System."},
			Link:   "https://gitlab.com/fospathi/mechane",
			Name:   WorldName,

			License:     about.CreativeCommons.PublicDomainDedication,
			Alternative: about.MITNoAttribution(2023, about.Fospathi.Name),
		},
		Init:      initWorld,
		PulseRate: world.Medium,
		Update:    update.Pulse,
		WorldWeb:  worldWeb,
	},
)

func initWorld(wld world.Standard) *model.Model {

	// Default devoir.
	wld.Devoir.Paused()
	isPaused := true

	// Default animation period.
	now := celestial.Now()
	validInterval := solsys.Y250
	const secsPerYear = 3.0
	tA := celestial.Gregorian{Y: solsys.Y250Start, M: 1, D: 1}.JD()
	tB := celestial.Gregorian{Y: solsys.Y250End, M: 1, D: 1}.JD()
	tD := (solsys.Y250End - solsys.Y250Start) * secsPerYear

	// Realms.
	anim := observe.NewModel(now, validInterval)
	look := lookup.NewModel()

	// Add the cameras to the world making them available for clients to select.
	wld.CameraAdjustments.Create(anim.Camera)
	wld.CameraAdjustments.Create(look.Camera)

	return &model.Model{
		IsPaused: isPaused,

		ValidInterval:     validInterval,
		T:                 celestial.Now(),
		TA:                tA,
		TB:                tB,
		AnimationDuration: tD,

		Observe: anim,
		Lookup:  look,

		Cams: map[realm.Realm]world.Camera{
			realm.Observe: anim.Camera,
			realm.Lookup:  look.Camera,
		},

		Roots: map[realm.Realm]*mechtree.Node{
			realm.Observe: anim.Root,
			realm.Lookup:  look.Root,
		},
	}
}
