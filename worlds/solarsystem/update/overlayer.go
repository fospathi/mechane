package update

import (
	"math"
	"time"

	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/solarsystem/model"
)

func OverlayerTextfields(wld world.Standard, mdl *model.Model, now time.Time) {
	w, _ := wld.TextfieldKeys.Await(nil)
loop:
	for {
		select {
		default:
			break loop

		case <-w:
			ev, _ := wld.TextfieldKeys.Delete(w)
			w, _ = wld.TextfieldKeys.Await(nil)

			name := ev.E.EffectName

			for n, li := range mdl.Lookup.Lookups {
				if name == n && ev.E.KeyDown {
					switch ev.E.Key {
					case "Enter":
						if li.Textfield.Clogged() {
							break
						}
						li.Textfield.Clog(now)
						li.Textfield.InputValue = ev.E.Value
						li.Textfield.Overlay(li.Results.Node)
						if ev.E.Value != "" {
							li.Lookup(ev.E.Value)
						}
					}
				}
			}
		}
	}

	for _, li := range mdl.Lookup.Lookups {
		if li.Textfield.Decloggable(now) {
			li.Textfield.Declog()
			li.Textfield.Overlay(li.Results.Node)
		}
	}
}

func OverlayerWheelMoves(wld world.Standard, mdl *model.Model, now time.Time) {
	seq := wld.WheelMoves
	w, _ := seq.Await(nil)
loop:
	for {
		select {
		default:
			break loop

		case <-w:
			ev, _ := seq.Delete(w)
			w, _ = seq.Await(nil)

			li, ok := mdl.Lookup.Lookups[ev.E.EffectName]
			if !ok {
				continue
			}
			if li.Results.Len() <= 1 {
				continue
			}

			li.Results.ShiftByOne(math.Signbit(ev.E.DeltaY))
		}
	}
}
