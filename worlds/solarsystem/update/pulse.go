package update

import (
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/solarsystem/model"
	"gitlab.com/fospathi/universal/watchable"
)

func Pulse(
	wld world.Standard,
	t watchable.ChronoShift,
	arrhythmic bool,
	mdl *model.Model,
) bool {

	select {
	case <-wld.ShutdownBegan():
		// Do shutdowny things here.
		wld.FinaliseShutdown()
		return false
	default:
	}

	if t.IsFirstPeriod {
		Observe(mdl)
		return true
	}

	OverlayerTextfields(wld, mdl, t.Time)
	OverlayerWheelMoves(wld, mdl, t.Time)
	Devoir(wld, mdl)

	if !mdl.IsPaused {
		mdl.IncrementT(t.DeltaT())
		Observe(mdl)
		if mdl.T == mdl.TB {
			mdl.IsPaused = true
			wld.Devoir.Paused()
		}
	}

	return true
}
