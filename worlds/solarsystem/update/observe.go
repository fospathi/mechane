package update

import (
	"gitlab.com/fospathi/mechane/worlds/solarsystem/model"
	"gitlab.com/fospathi/mechane/worlds/solarsystem/model/realm"
)

// Observe updates the observe part of the model where it is assumed that the
// date value has already been incremented in the model beforehand.
func Observe(mdl *model.Model) {
	mdl.Observe.Inbuilt.Update(mdl.T, mdl.ValidInterval)
	mdl.Roots[realm.Observe].AddDepictioners(mdl.Observe.Inbuilt.PlanetOrbits...)
	mdl.Roots[realm.Observe].AddDepictioners(mdl.Observe.Inbuilt.PlanetOutlines...)
}
