package model

import (
	"math"

	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/solarsystem/model/realm"
	"gitlab.com/fospathi/mechane/worlds/solarsystem/model/realm/lookup"
	"gitlab.com/fospathi/mechane/worlds/solarsystem/model/realm/observe"
	"gitlab.com/fospathi/phyz/celestial"
	"gitlab.com/fospathi/phyz/celestial/solsys"
)

// A Model of the solarsystem Mechane world.
type Model struct {
	IsPaused bool

	// The end user wall time that an animation takes to complete one cycle
	// measured in units of seconds.
	AnimationDuration float64
	ValidInterval     solsys.Interval
	T                 celestial.JD // Current calendar date.
	TA                celestial.JD // Start at this calendar date.
	TB                celestial.JD // End at this calendar date.

	Observe observe.Model
	Lookup  lookup.Model

	Cams  map[realm.Realm]world.Camera   // A camera for each realm.
	Roots map[realm.Realm]*mechtree.Node // Root nodes for each realm.
}

// IncrementT by the same proportion of the calendar span that delta t is of the
// animation duration.
//
// The given duration delta t is the period of time elapsed since the last loop
// and is measured in units of seconds.
func (mdl *Model) IncrementT(deltaT float64) {
	var jd celestial.JD
	{
		span := mdl.TB.Date() - mdl.TA.Date()
		if mdl.T == mdl.TB {
			jd = mdl.TA
		} else {
			proportion := deltaT / mdl.AnimationDuration
			jd = mdl.T.Add(proportion * span)
		}
		elapsed := jd.Date() - mdl.TA.Date()
		if math.Abs(elapsed) > math.Abs(span) {
			jd = mdl.TB
		}
	}

	mdl.T = jd
}
