/* Package ellipse computes properties of the ellipse. */
package ellipse

import "math"

// Eccentricity of the ellipse with semi-major axis length a and semi-minor axis
// length b.
func Eccentricity(a, b float64) float64 {
	return math.Sqrt(1 - (b*b)/(a*a))
}

// Focus is the positive distance from the centre of the ellipse, with
// semi-major axis length a and eccentricity e, to either focus.
func Focus(a, e float64) float64 {
	return a * e
}

// SemiMinorAxis length of the ellipse with semi-major axis length a and
// eccentricity e.
func SemiMinorAxis(a, e float64) float64 {
	return a * math.Sqrt(1-e*e)
}
