package ellipse

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/phyz/celestial/solsys"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
	"gitlab.com/fospathi/wire/colour"
	"gitlab.com/fospathi/wire/colour/rgb"
)

func PlanetOutlines(
	positions map[solsys.Planet]solsys.PlanetPosition,
) []mech.Depictioner {
	var outlines = make([]mech.Depictioner, 0, len(solsys.Planets))
	for _, planet := range solsys.Planets {

		position := positions[planet]
		posUnit, tan := position.Ecliptic.Unit(), position.EclipticTangent
		fr := maf.Frame{
			O:    position.Ecliptic,
			Axes: maf.Basis{X: posUnit, Y: tan, Z: posUnit.Cross(tan).Unit()},
		}

		outline := wire.NewSphere(wire.SphereFiguration{
			R:  1.0 / 40.0,
			NY: 5,
		})
		outline.FigName = solsys.PlanetToString(planet) + "outline"
		outline.Orientation = fr
		const thickness = 0.002
		outline.Depict(wire.SpherePortion{
			Y: []mech.Portion{
				{
					Caps:     [2]mech.Linecap{mech.Butt, mech.Butt},
					Colourer: colour.NewMonoGrad(rgb.Black.LCh()),
					In:       interval.In{0, maf.TwoPi},
					Th:       [2]float64{thickness, thickness},
					Qual:     0,
				},
			},
		})
		outlines = append(outlines, outline)
	}
	return outlines
}

func PlanetOrbits(
	positions map[solsys.Planet]solsys.PlanetPosition,
) []mech.Depictioner {

	var orbits = make([]mech.Depictioner, 0, len(solsys.Planets))
	for _, planet := range solsys.Planets {

		position := positions[planet]
		a, e := position.SemiMajorAxis, position.Eccentricity
		b := SemiMinorAxis(a, e)
		c := Focus(a, e)

		// The origin of the ecliptic coordinate system is the Sun, its positive
		// X-axis direction is that of the mean vernal equinox direction at
		// J2000.0, and its positive Z-axis direction is that of the ecliptic
		// North Pole direction.

		// Start with the soon to be orbit frame initially coinciding with the
		// ecliptic coordinate system.
		fr := maf.StdFrame
		// Align the X-axis with the ascending node position.
		fr = fr.LocalRotPosZ(float64(position.LongitudeOfTheAscendingNode))
		// Incline the plane.
		fr = fr.LocalRotPosX(float64(position.Inclination))
		// Align the X-axis with the perihelion.
		fr = fr.LocalRotPosZ(float64(position.ArgumentOfPerihelion))
		// Move the ellipse focus onto the Sun.
		fr = fr.LocalDisNegX(c)

		orbit := wire.NewEllipse(kirv.Ellipse{A: a, B: b})
		orbit.FigName = solsys.PlanetToString(planet) + "orbit"
		orbit.Orientation = fr
		orbit.Depict(NewEllipsePortions(maf.EllipsePerimeter(a, b), false)...)
		orbits = append(orbits, orbit)
	}
	return orbits
}

// NewEllipsePortions for an ellipse with the given perimeter length which is
// measured in astronomical units (AU).
func NewEllipsePortions(perimeter float64, dashed bool) []mech.Portion {
	const thickness = 0.00325

	// Approx dash length is the reciprocal of the density.
	const (
		innerDensity = 30.0
		outerDensity = 5.0
	)

	// Inner planets have perimeters well below the perimeter edge value.
	//  Mars    ~= 10 AU.
	//  Jupiter ~= 30 AU.
	const perimeterEdge = 20.0

	var (
		density float64
		qual    int
	)
	if perimeter < perimeterEdge {
		density = innerDensity
	} else {
		density = outerDensity
		qual = wire.TrigQual * 10
	}

	divs := int(math.Trunc(density * perimeter))
	if divs%2 != 0 { // An even number of divisions for a gap at the end.
		divs += 1
	}

	// Yellow/red at the closest approach (periapsis). Blueish at the furthest
	// approach (apoapsis).
	orbitGrad, _ := colour.NewLChMajorGrad(rgb.Yellow.LCh(), rgb.Red.LCh())
	portion := mech.Portion{
		Caps:     [2]mech.Linecap{mech.Butt, mech.Butt},
		In:       interval.In{0, maf.TwoPi},
		Colourer: orbitGrad,
		Th:       [2]float64{thickness, thickness},
		Qual:     qual,
	}

	if !dashed {
		return []mech.Portion{portion}
	}

	// Prevent occasional visual glitching at the end of the ellipse by
	// increasing the dash angle a small amount so that the last gap is just big
	// enough to reach past the interval beginning.
	k := 1e-7
	dashAngle := (maf.TwoPi / float64(divs)) + k

	return mech.Portion{
		Caps:     [2]mech.Linecap{mech.Butt, mech.Butt},
		In:       interval.In{0, maf.TwoPi},
		Colourer: orbitGrad,
		Th:       [2]float64{thickness, thickness},
		Qual:     qual,
	}.DashArray(dashAngle, dashAngle)
}

func SunOutline() mech.Depictioner {
	const thickness = 0.003
	fullCircle := []mech.Portion{
		{
			Caps:     [2]mech.Linecap{mech.Butt, mech.Butt},
			Colourer: colour.NewMonoGrad(rgb.Orange.LCh()),
			In:       interval.In{0, maf.TwoPi},
			Th:       [2]float64{thickness, thickness},
			Qual:     0,
		},
	}
	outlinePortion := wire.SpherePortion{
		X: fullCircle,
		Y: fullCircle,
		Z: fullCircle,
	}

	outline := wire.NewSphere(wire.SphereFiguration{
		R:  1.0 / 12.0,
		NX: 1,
		NY: 1,
		NZ: 1,
	})
	outline.FigName = "sun" + "outline"
	outline.Depict(outlinePortion)
	return outline
}
