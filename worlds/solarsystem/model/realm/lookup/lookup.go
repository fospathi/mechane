package lookup

import (
	"fmt"
	"math"
	"strings"
	"time"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/world/dolly"
	"gitlab.com/fospathi/mechane/worlds/0/test3d/model/style"
	"gitlab.com/fospathi/mico"
	"gitlab.com/fospathi/phyz/celestial/solsys/horizons"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/universal/tally"
	"gitlab.com/fospathi/wire"
	"gitlab.com/fospathi/wire/wiregl"
)

// Model for an interface with the Horizons Lookup API.
type Model struct {
	Camera  world.Camera
	Root    *mechtree.Node
	Lookups map[string]LookupInput
}

// NewModel constructs a new Model.
func NewModel() Model {

	// Surround the camera with a cube.

	cubeL := 10.0
	cube := wire.NewCube(cubeL)
	cube.SetName("lookupCube")
	cubePortion := func() []mech.Portion {
		return []mech.Portion{
			{
				In:       interval.In{0, 1},
				Colourer: style.SymmetricalSpectrum,
				Th:       [2]float64{style.Thickness, style.Thickness},
				Qual:     0,
			},
		}
	}
	cube.Depict(wire.CubePortion{
		Vertical: cubePortion(),
		Bottom:   cubePortion(),
		Top:      cubePortion(),
	})

	// Adorn the faces of the cube with an input at each face midpoint.

	var (
		mli   = map[string]LookupInput{}
		zHalf = maf.Vec{X: 0, Y: 0, Z: cubeL / 2}
	)
	for i, fr := range []maf.Frame{
		maf.StdFrame.LocalRotPosY(math.Pi).LocalDis(zHalf),     // Forward face.
		maf.StdFrame.LocalRotPosY(maf.HalfPi).LocalDis(zHalf),  // Rightward face.
		maf.StdFrame.LocalDis(zHalf),                           // Aft face.
		maf.StdFrame.LocalRotPosY(-maf.HalfPi).LocalDis(zHalf), // Leftward face.
		maf.StdFrame.LocalRotPosX(maf.HalfPi).
			LocalRotPosZ(math.Pi).LocalDis(zHalf), // Deck face.
		maf.StdFrame.LocalRotPosX(-maf.HalfPi).
			LocalRotPosZ(math.Pi).LocalDis(zHalf), // Overhead face.
	} {
		name := fmt.Sprintf("lookupInput%v", i)
		tf := world.Textfield{
			EffectName:     name,
			ElementName:    name,
			Icon1:          mico.Round.Search,
			Orientation:    mechtree.XYFront,
			OriginPosition: mechtree.Center,
			Placeholder:    fmt.Sprintf("Lookup #%v", i+1),
		}
		li := NewLookupInput(name+"Results", cubeL/3, &tf)
		li.Results.Node.Orient(fr)
		tf.Overlay(li.Results.Node)
		mli[name] = li
	}

	// Populate the root node.

	root := mechtree.NewNode()
	root.AddDepictioner(cube)
	camNode := mechtree.NewNode()
	camNode.Orient(maf.NewFrame(maf.Vec{X: 0, Y: 0, Z: -cubeL / 5}))
	root.AddChild(camNode)
	for _, li := range mli {
		root.AddChild(li.Results.Node)
	}
	veneerNode := mechtree.NewNode()

	// Camera.

	orthoH := 12.0
	ortho := wiregl.NewOrthographicProjection(
		wiregl.ViewingBox{
			H: orthoH,
			W: orthoH * mech.WToHPixelsRatio,
			F: 20,
			N: 0,
		},
	)
	cam := world.Camera{
		BackgroundAlpha:  1.0,
		BackgroundColour: world.DefaultBackgroundColour,
		Camera: mechtree.Camera{
			Lens:    ortho.Matrix,
			Inverse: ortho.Inverse,
			Name:    "lookup",
			Node:    camNode,
		},
		Veneer: mechtree.Camera{
			Lens:    world.DefaultVeneerLens,
			Inverse: world.DefaultVeneerLensInverse,
			Name:    "lookupVeneer",
			Node:    veneerNode,
		},
		Dolly: dolly.SphericalOutwardsSurface,
	}

	return Model{
		Camera:  cam,
		Lookups: mli,
		Root:    root,
	}
}

// LookupInput models a lookup textfield along with its results.
type LookupInput struct {
	Results   *world.HelixStand[struct{}]
	Textfield *world.Textfield
}

// addMatches of a lookup to the results.
func (li LookupInput) addMatches(res LookupResult) {
	standName := li.Results.ElementName()
	for i := len(res.Result) - 1; i >= 0; i-- {
		match := res.Result[i]
		node := li.Results.Add(struct{}{})
		name := standName + res.Name + "-" + world.HexStr32.String()
		sb := strings.Builder{}
		sb.WriteString(fmt.Sprintf("query: %v\n", res.Query))
		sb.WriteString(fmt.Sprintf("match: %v\n", match.HorizonsName()))
		if len(match.Aliases) > 0 {
			sb.WriteString(fmt.Sprintf("alias: %v\n",
				strings.Join(match.Aliases, ", ")))
		}
		if match.ObjectName != "" {
			sb.WriteString(fmt.Sprintf(" name: %v\n", match.ObjectName))
		}
		if match.PrimaryDesignation != "" {
			sb.WriteString(fmt.Sprintf(" pdes: %v\n", match.PrimaryDesignation))
		}
		if match.PrimarySPKID != "" {
			sb.WriteString(fmt.Sprintf("spkid: %v\n", match.PrimarySPKID))
		}
		info := world.Pre{
			ElementName:    name,
			Width:          30,
			MaxHeight:      15,
			Orientation:    mechtree.XYFront,
			OriginPosition: mechtree.Top,
			PreText:        sb.String(),
		}
		info.Overlay(node)
	}
}

// Lookup the entity(s) matching the given query in the Horizons Lookup API.
func (li LookupInput) Lookup(query string) {
	go func() {
		res, err := horizons.Lookup(query)
		if err != nil {
			return
		}
		li.addMatches(LookupResult{
			Name:   newLookupResultName(),
			Query:  query,
			Result: res,
		})
	}()
}

func NewLookupInput(
	elementName string,
	helixR float64,
	textfield *world.Textfield,
) LookupInput {

	stand := world.NewHelixStand[struct{}](world.HelixStandSpec{
		ElementName: elementName,
		A:           maf.QuartPi,
		P:           1,
		Phase:       maf.HalfPi,
		R:           helixR,
		T:           1 * time.Second,
	})
	return LookupInput{
		Results:   stand,
		Textfield: textfield,
	}
}

type LookupResult struct {
	Name   string
	Query  string // The textfield input value sent to the Horizons Lookup API.
	Result []horizons.LookupMatch
}

var lookupResultCounter = tally.Counter{}

func newLookupResultName() string {
	return "LookupResult" + lookupResultCounter.Next()
}
