package observe

import (
	"gitlab.com/fospathi/mechane/worlds/solarsystem/model/ellipse"
	"gitlab.com/fospathi/phyz/celestial"
	"gitlab.com/fospathi/phyz/celestial/solsys"
	"gitlab.com/fospathi/universal/mech"
)

// The Scale may change to that which is better for showing either the inner or
// outer planets.
type Scale uint

const (
	// A sphere of radius ~= 2 AU would suffice to enclose the inner planets.
	Inner = Scale(iota)
	// A sphere of radius ~= 50 AU would suffice to enclose the outer planets.
	Outer
)

// SolarSystemModel is a model of the Solar System including the orbits and
// outlines of the nine planets.
type SolarSystemModel struct {
	PlanetOrbits   []mech.Depictioner
	PlanetOutlines []mech.Depictioner
	SunOutline     mech.Depictioner
}

// Update the Solar System model.
func (mdl *SolarSystemModel) Update(t celestial.JD, in solsys.Interval) {
	positions := map[solsys.Planet]solsys.PlanetPosition{}
	for _, planet := range solsys.Planets {
		positions[planet] = solsys.NewPlanetPosition(planet, t, in)
	}
	mdl.PlanetOrbits = ellipse.PlanetOrbits(positions)
	mdl.PlanetOutlines = ellipse.PlanetOutlines(positions)
}

// NewSolarSystemModel constructs a new SolarSystemModel.
func NewSolarSystemModel(t celestial.JD, in solsys.Interval) *SolarSystemModel {
	mdl := &SolarSystemModel{SunOutline: ellipse.SunOutline()}
	mdl.Update(t, in)
	return mdl
}
