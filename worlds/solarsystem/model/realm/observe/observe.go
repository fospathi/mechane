package observe

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/world/dolly"
	"gitlab.com/fospathi/phyz/celestial"
	"gitlab.com/fospathi/phyz/celestial/solsys"
)

// Model of the Solar System.
type Model struct {
	Camera  world.Camera
	Root    *mechtree.Node
	Inbuilt SolarSystemModel // An inbuilt model that works without the internet.
	Scale
}

// NewModel constructs a new Solar System animation Model.
func NewModel(t celestial.JD, in solsys.Interval) Model {
	sol := NewSolarSystemModel(t, in)

	root := mechtree.NewNode()
	root.AddDepictioner(sol.SunOutline)
	root.AddDepictioners(sol.PlanetOrbits...)
	root.AddDepictioners(sol.PlanetOutlines...)

	camNode := mechtree.NewNode()
	root.AddChild(camNode)
	veneerNode := mechtree.NewNode()

	cam := world.Camera{
		BackgroundAlpha:  1.0,
		BackgroundColour: world.DefaultBackgroundColour,
		Camera: mechtree.Camera{
			Lens:    world.DefaultPerspectiveLens,
			Inverse: world.DefaultPerspectiveLensInverse,
			Name:    "animation",
			Node:    camNode,
		},
		Veneer: mechtree.Camera{
			Lens:    world.DefaultVeneerLens,
			Inverse: world.DefaultVeneerLensInverse,
			Name:    "animationVeneer",
			Node:    veneerNode,
		},
		Dolly: dolly.NewSpherical(dolly.SphericalSpec{
			SphericalMode: dolly.SphericalInnyMode,
			Up:            dolly.Z,
		}),
	}
	{
		fr := maf.StdFrame
		fr = fr.LocalRotPosX(maf.HalfPi)
		fr = fr.LocalRotPosY(maf.HalfPi)
		fr = fr.LocalRotPosX(-maf.QuartPi)
		fr = fr.LocalDisPosZ(30)
		camNode.Orient(fr)
	}

	return Model{
		Camera: cam,
		Root:   root,
		Scale:  Outer,
	}
}
