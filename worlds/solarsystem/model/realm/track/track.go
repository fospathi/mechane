package track

import (
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/phyz/celestial/solsys/horizons"
)

// Model of the tracker used to control which solar system objects are shown in
// the observatory.
type Model struct {
	Camera world.Camera
	Root   *mechtree.Node

	CurrentDate world.Textfield

	Tracked   []Tracked
	Untracked []Object
}

// Tracked solar system object.
type Tracked struct {
	horizons.EphemerisSpec
	Positions []horizons.DatedPosition
}

// Object in the solar system.
type Object struct {
	Name   string
	Object []horizons.LookupMatch
	Query  string // The textfield input value sent to the Horizons Lookup API.
}
