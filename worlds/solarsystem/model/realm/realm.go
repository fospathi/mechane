package realm

// A Realm is one of the top-level organisational components of the solarsystem
// Mechane world.
type Realm uint

const (
	Lookup = Realm(iota)
	Observe
	Track
)
