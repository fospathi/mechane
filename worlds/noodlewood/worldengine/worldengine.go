package worldengine

import (
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/noodlewood/principality"
)

// A WorldEngine which embodies the noodlewood Mechane world.
type WorldEngine struct {
	IsPaused bool

	SphereFreq float64 // Spins per second.
	SphereNode *mechtree.Node

	HilbertNode *mechtree.Node

	FloorHeight float64 // The elevation of the current floor level above the sea level.
	EyeHeight   float64 // The elevation of the camera above the current floor level.

	Cams  map[principality.Principality]world.Camera   // A camera for each principality.
	Roots map[principality.Principality]*mechtree.Node // Root nodes for each principality.
}
