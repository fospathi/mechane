package worldengine

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/world/dolly"
	"gitlab.com/fospathi/mechane/worlds/noodlewood/principality"
	"gitlab.com/fospathi/mechane/worlds/noodlewood/principality/animation"
)

func Init(wld world.Standard) *WorldEngine {
	wld.Devoir.Paused()
	isPaused := true

	animationRoot := mechtree.NewNode()

	animationCamNode := mechtree.NewNode()
	animationCamVeneerNode := mechtree.NewNode()
	animationRoot.AddChild(animationCamNode)
	animationCamera := world.Camera{
		BackgroundAlpha:  0.0,
		BackgroundColour: world.DefaultBackgroundColour,
		Camera: mechtree.Camera{
			Lens:    world.DefaultPerspectiveLens,
			Inverse: world.DefaultPerspectiveLensInverse,
			Name:    "animation",
			Node:    animationCamNode,
		},
		Veneer: mechtree.Camera{
			Lens:    world.DefaultVeneerLens,
			Inverse: world.DefaultVeneerLensInverse,
			Name:    "animationVeneer",
			Node:    animationCamVeneerNode,
		},
		Dolly: dolly.SpatialGame,
	}
	{
		fr := maf.StdFrame
		fr = fr.LocalDisPosZ(5)
		fr = fr.LocalDisPosY(0)
		animationCamNode.Orient(fr)
	}

	wld.CameraAdjustments.Create(animationCamera)

	sphereNode := mechtree.NewNode()
	animationRoot.AddChild(sphereNode)
	sphereNode.AddDepictioner(animation.Sphere())

	hilbertCurve := animation.NewMenagerie()
	animationRoot.AddChild(hilbertCurve.Node)

	circleNode := mechtree.NewNode()
	//circleNode.Frame.O.Z = 0
	testNode := mechtree.NewNode()
	testNode.AddChild(circleNode)
	animationCamVeneerNode.AddChild(testNode)
	circleNode.AddDepictioner(animation.Circle())

	return &WorldEngine{
		IsPaused: isPaused,

		SphereNode: sphereNode,
		SphereFreq: 0.5,

		Cams: map[principality.Principality]world.Camera{
			principality.Animation: animationCamera,
			// Settings:  settingsCamera,
		},

		Roots: map[principality.Principality]*mechtree.Node{
			principality.Animation: animationRoot,
			// Settings:  settingsRoot,
		},
	}
}
