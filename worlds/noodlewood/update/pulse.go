package update

import (
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/noodlewood/worldengine"
	"gitlab.com/fospathi/universal/watchable"
)

func Pulse(
	wld world.Standard,
	t watchable.ChronoShift,
	arrhythmic bool,
	eng *worldengine.WorldEngine,
) bool {

	select {
	case <-wld.ShutdownBegan():
		// Do shutdowny things here.
		wld.FinaliseShutdown()
		return false
	default:
	}

	if t.IsFirstPeriod {
		WorldEngine(wld, eng, 0)
		return true
	}

	OverlayerButtons(wld, eng)

	if !eng.IsPaused {
		WorldEngine(wld, eng, t.DeltaT())
	}

	Devoir(wld, eng)

	return true
}
