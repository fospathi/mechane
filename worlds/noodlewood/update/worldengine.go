package update

import (
	"math"

	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/noodlewood/worldengine"
)

func WorldEngine(
	wld world.Standard, eng *worldengine.WorldEngine, deltaT float64,
) {

	eng.SphereNode.Orient(
		eng.SphereNode.LocalRotPosY(eng.SphereFreq * 2 * math.Pi * deltaT))
}
