package update

import (
	"fmt"

	"gitlab.com/fospathi/mechane/mechserver/mechdevoir"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/noodlewood/worldengine"
	"gitlab.com/fospathi/universal/mech"
)

func Devoir(wld world.Standard, eng *worldengine.WorldEngine) {
	for {
		uic, err := wld.Devoir.Pop()
		if err != nil {
			// TODO handle error
			break
		}
		switch uic.Value.(type) {
		case mechdevoir.PauseUIC:
			eng.IsPaused = true
			wld.Devoir.Paused()
		case mechdevoir.PlayUIC:
			eng.IsPaused = false
			wld.Devoir.Playing()
		case mechdevoir.ShutdownUIC:
			wld.BeginShutdown(mech.ShutdownByClient)
			fmt.Printf("Received shutdown request from client.\n")
			fmt.Printf("Shutting down...\n")
		}
	}
}
