package animation

import (
	"math"
	"math/rand"
	"slices"
	"time"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/mechane/mechtree"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
	"gitlab.com/fospathi/wire/colour"
	"gitlab.com/fospathi/wire/colour/rgb"
	"gitlab.com/fospathi/wire/compgeom/bounding"
	"gitlab.com/fospathi/wire/compgeom/heighway"
	"gitlab.com/fospathi/wire/compgeom/hilbert"
	"gitlab.com/fospathi/wire/compgeom/hilbert/hilbert3d"
	"gitlab.com/fospathi/wire/compgeom/koch"
	"gitlab.com/fospathi/wire/compgeom/mcworter"
	"gitlab.com/fospathi/wire/compgeom/peano"
	"gitlab.com/fospathi/wire/compgeom/sierpinski"
)

// A Menagerie of pretty curves.
type Menagerie struct {
	Node *mechtree.Node
}

func NewMenagerie() Menagerie {
	const thickness = 0.003
	rdm := rand.New(rand.NewSource(time.Now().UnixNano()))
	node := mechtree.NewNode()

	// Pentadentrite.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: 25, Y: 0, Z: -12}))
		node.AddChild(subNode)

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		hCur := wire.NewPentadentrite(mcworter.PentadentritePlotSpec{
			Iterations: 5,
			Seg:        maf.LineSeg{P2: maf.Vec{X: 30}},
		})
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("pentadentriteCur2d")
		subNode.AddDepictioner(hCur)
	}

	// Golden dragon.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: 25, Y: -25, Z: -12}))
		node.AddChild(subNode)

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		hCur := wire.NewGoldenDragon(heighway.TerdragonPlotSpec{
			Iterations: 12,
			Seg:        maf.LineSeg{P2: maf.Vec{X: 30}},
		})
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("goldenDragonCur2d")
		subNode.AddDepictioner(hCur)

		{
			subNode := mechtree.NewNode()
			fr := maf.NewFrame(maf.Vec{X: 25, Y: -25, Z: -12})
			subNode.Orient(fr.LocalRotPosX(math.Pi))
			node.AddChild(subNode)

			col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
			hCur := wire.NewGoldenDragon(heighway.TerdragonPlotSpec{
				Iterations: 12,
				Seg:        maf.LineSeg{P2: maf.Vec{X: 30}},
			})
			hCur.Depict(mech.Portion{
				Colourer: col,
				In:       interval.In{0, 1},
				Th:       [2]float64{thickness, thickness},
				Qual:     1,
			})
			hCur.SetName("goldenDragonCur_2_2d")
			subNode.AddDepictioner(hCur)
		}
	}

	// Fudgeflake.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: 15, Y: -25, Z: -12}))
		node.AddChild(subNode)

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		s, c := math.Sincos(math.Pi / 6)
		tri := maf.Triangle{
			A: maf.Vec{X: 0, Y: 5, Z: 0},
			B: maf.Vec{X: 5 * c, Y: -5 * s, Z: 0},
			C: maf.Vec{X: -5 * c, Y: -5 * s, Z: 0},
		}
		hCur := wire.NewHeighwayFudgeflake(heighway.FudgeflakePlotSpec{
			Iterations:  5,
			Equilateral: tri,
		})
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("fudgeflakeCur2d")
		subNode.AddDepictioner(hCur)

		{ // Fudgeflake curves can tile the plane.
			subNode := mechtree.NewNode()
			subNode.Orient(maf.NewFrame(maf.Vec{X: 15, Y: -25, Z: -12}))
			node.AddChild(subNode)

			col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
			s, c := math.Sincos(math.Pi / 6)
			tri := maf.Triangle{
				A: maf.Vec{X: -10 * c, Y: 5, Z: 0},
				B: maf.Vec{X: 0, Y: 5, Z: 0},
				C: maf.Vec{X: -5 * c, Y: 5 + 5*(1+s), Z: 0},
			}
			hCur := wire.NewHeighwayFudgeflake(heighway.FudgeflakePlotSpec{
				Iterations:  5,
				Equilateral: tri,
			})
			hCur.Depict(mech.Portion{
				Colourer: col,
				In:       interval.In{0, 1},
				Th:       [2]float64{thickness, thickness},
				Qual:     1,
			})
			hCur.SetName("fudgeflakeCur_2_2d")
			subNode.AddDepictioner(hCur)
		}

		{ // Fudgeflake curves can tile the plane.
			subNode := mechtree.NewNode()
			subNode.Orient(maf.NewFrame(maf.Vec{X: 15, Y: -25, Z: -12}))
			node.AddChild(subNode)

			col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
			s, c := math.Sincos(math.Pi / 6)
			tri := maf.Triangle{
				A: maf.Vec{X: 0 - 10*c, Y: 5, Z: 0},
				B: maf.Vec{X: 5*c - 10*c, Y: -5 * s, Z: 0},
				C: maf.Vec{X: -5*c - 10*c, Y: -5 * s, Z: 0},
			}
			hCur := wire.NewHeighwayFudgeflake(heighway.FudgeflakePlotSpec{
				Iterations:  5,
				Equilateral: tri,
			})
			hCur.Depict(mech.Portion{
				Colourer: col,
				In:       interval.In{0, 1},
				Th:       [2]float64{thickness, thickness},
				Qual:     1,
			})
			hCur.SetName("fudgeflakeCur_3_2d")
			subNode.AddDepictioner(hCur)
		}
	}

	// Hex terdragon.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: -10, Y: -25, Z: -12}))
		node.AddChild(subNode)

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		hCur := wire.NewHexTerdragon(heighway.TerdragonPlotSpec{
			Iterations: 5,
			Seg:        maf.LineSeg{P2: maf.Vec{X: 10}},
		})
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("hexTerdragonCur2d")
		subNode.AddDepictioner(hCur)
	}

	// Terdragon.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: -20, Y: 5, Z: -12}))
		node.AddChild(subNode)

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		hCur := wire.NewTerdragon(heighway.TerdragonPlotSpec{
			Iterations: 5,
			Seg:        maf.LineSeg{P2: maf.Vec{X: 10}},
		})
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("terdragonCur2d")
		subNode.AddDepictioner(hCur)
	}

	// Dragon.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: -20, Y: -5, Z: -12}))
		node.AddChild(subNode)

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		hCur := wire.NewHeighwayTwinDragon(heighway.PlotSpec{
			Iterations: 10,
			ReflectX:   true,
			Seg:        maf.LineSeg{P2: maf.Vec{X: 10}},
		})
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("heighwayCur2d")
		subNode.AddDepictioner(hCur)

		var vp []maf.Vec
		for _, v := range hCur.Parts {
			vp = append(vp, v...)
		}
		vc := bounding.NewBox(vp).Expand(0.3).Corners()
		box := wire.NewClosedPolyLine(vc[:])
		box.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     5,
		})
		box.SetName("heighwayBoxCur2d")
		subNode.AddDepictioner(box)
	}

	// Koch snowflake.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: 10, Y: -5, Z: -12}))
		node.AddChild(subNode)

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		s, c := math.Sincos(math.Pi / 6)
		tri := maf.Triangle{
			A: maf.Vec{X: 0, Y: 5, Z: 0},
			B: maf.Vec{X: 5 * c, Y: -5 * s, Z: 0},
			C: maf.Vec{X: -5 * c, Y: -5 * s, Z: 0},
		}
		hCur := wire.NewKochSnowflake(koch.SnowflakePlotSpec{
			AntiSnowflake: true,
			Order:         4,
			Equilateral:   tri,
		})
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("kochCur2d")
		subNode.AddDepictioner(hCur)
	}

	// Koch.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: 10, Y: -5, Z: -10}))
		node.AddChild(subNode)

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		hCur := wire.NewKoch(koch.PlotSpec{
			Order: 3,
			Seg:   maf.LineSeg{P1: maf.Vec{X: 0, Y: 0}, P2: maf.Vec{X: 5, Y: 0}},
		})
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("kochCur2d")
		subNode.AddDepictioner(hCur)
	}

	// Peano 2D straight.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: 0, Y: -5, Z: -10}))
		node.AddChild(subNode)

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		hCur := wire.NewPeano(peano.PlotSpec{
			Centre: maf.Vec{X: 0, Y: 0},
			Order:  3,
			W:      10,
		})
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("straightPeanoCur2d")
		subNode.AddDepictioner(hCur)
	}

	// Sierpinski 2D straight.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: -10, Y: -10, Z: -5}))
		node.AddChild(subNode)

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		hCur := wire.NewSierpinskiTriangle(sierpinski.PlotSpec{
			Centre:     maf.Vec{X: 0, Y: 0},
			Iterations: 4,
			W:          10,
		})
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("straightSierpinskiCur2d")
		subNode.AddDepictioner(hCur)
	}

	// 2D straight.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: 5, Y: 0, Z: -5}))
		node.AddChild(subNode)

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		hCur := wire.NewHilbert(hilbert.PlotSpec{
			Centre: maf.Vec{X: 5, Y: 5},
			Order:  1,
			W:      10,
		})
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("straightHilbertCur2d")
		subNode.AddDepictioner(hCur)
	}

	// 3D straight.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: 0, Y: -10, Z: -30}))
		node.AddChild(subNode)

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		hCur := wire.NewHilbert3D(hilbert3d.PlotSpec{
			Centre: maf.Vec{X: 5, Y: 5},
			Order:  3,
			W:      10,
		})
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("straightHilbertCur3d")
		subNode.AddDepictioner(hCur)
	}

	// 2D random.
	{
		const n = 400
		points := make([]maf.Vec2, n)
		for i := 0; i < n; i++ {
			points[i] = maf.Vec2{X: rdm.Float64(), Y: rdm.Float64()}
		}
		slices.SortFunc(points, hilbert.Compare)
		for i := 0; i < n; i++ {
			points[i] = points[i].Scale(10)
		}

		points3d := make([]maf.Vec, n)
		for i := 0; i < n; i++ {
			points3d[i] = points[i].Higher(-10)
		}

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		hCur := wire.NewPolyLine(points3d)
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("randHilbertCur2d")
		node.AddDepictioner(hCur)
	}

	// 3D random.
	{
		subNode := mechtree.NewNode()
		subNode.Orient(maf.NewFrame(maf.Vec{X: -10, Y: 0, Z: -30}))
		node.AddChild(subNode)

		const n = 500
		points := make([]maf.Vec, n)
		for i := 0; i < n; i++ {
			points[i] = maf.Vec{
				X: rdm.Float64(), Y: rdm.Float64(), Z: rdm.Float64(),
			}
		}
		slices.SortFunc(points, hilbert3d.Compare)
		for i := 0; i < n; i++ {
			points[i] = points[i].Scale(10)
		}

		col, _ := colour.NewLChMajorGrad(rgb.Red.LCh(), rgb.Yellow.LCh())
		hCur := wire.NewPolyLine(points)
		hCur.Depict(mech.Portion{
			Colourer: col,
			In:       interval.In{0, 1},
			Th:       [2]float64{thickness, thickness},
			Qual:     1,
		})
		hCur.SetName("randHilbertCur3d")
		subNode.AddDepictioner(hCur)
	}

	return Menagerie{
		Node: node,
	}
}
