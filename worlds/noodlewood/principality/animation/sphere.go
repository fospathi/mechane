package animation

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire"
	"gitlab.com/fospathi/wire/colour"
	"gitlab.com/fospathi/wire/colour/rgb"
)

func Sphere() mech.Depictioner {
	const thickness = 0.003
	fullCircle := []mech.Portion{
		{
			Caps:     [2]mech.Linecap{mech.Butt, mech.Butt},
			Colourer: colour.NewMonoGrad(rgb.Orange.LCh()),
			In:       interval.In{0, maf.TwoPi},
			Th:       [2]float64{thickness, thickness},
			Qual:     0,
		},
	}
	outlinePortion := wire.SpherePortion{
		X: fullCircle,
		Y: fullCircle,
		Z: fullCircle,
	}

	outline := wire.NewSphere(wire.SphereFiguration{
		R:  0.9,
		NX: 3,
		NY: 2,
		NZ: 1,
	})
	outline.SetName("masterSphere")
	outline.Depict(outlinePortion)
	return outline
}

func Circle() mech.Depictioner {
	const thickness = 0.003
	fullCircle := []mech.Portion{
		{
			Caps:     [2]mech.Linecap{mech.Butt, mech.Butt},
			Colourer: colour.NewMonoGrad(rgb.Orange.LCh()),
			In:       interval.In{0, maf.TwoPi},
			Th:       [2]float64{thickness, thickness},
			Qual:     0,
		},
	}

	outline := wire.NewSphere(wire.SphereFiguration{
		R:  0.3,
		NZ: 1,
	})
	outline.SetName("masterCircle")
	outline.Depict(wire.SpherePortion{
		X: fullCircle,
		Y: fullCircle,
		Z: fullCircle,
	})
	return outline
}
