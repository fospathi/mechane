package principality

// A Principality is one of the top-level organisational components of the
// noodlewood Mechane world.
type Principality uint

const (
	Animation = Principality(iota)
	Settings
)
