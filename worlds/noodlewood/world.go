package noodlewood

import (
	"embed"

	"gitlab.com/fospathi/mechane/about"
	"gitlab.com/fospathi/mechane/world"
	"gitlab.com/fospathi/mechane/worlds/noodlewood/update"
	"gitlab.com/fospathi/mechane/worlds/noodlewood/worldengine"
)

// WorldName is used by Mechane to identify an instance of this world.
const WorldName = "noodlewood"

//go:embed worldweb
var worldWeb embed.FS

// LaunchResources function returns the world's launch resources.
var LaunchResources = world.NewLaunchResources(
	world.LaunchResourcesSpec[*worldengine.WorldEngine]{
		About: about.World{
			Author: []about.Author{about.Fospathi},
			Blurb:  []string{"A game set in a fantasy forest."},
			Link:   "https://gitlab.com/fospathi/mechane",
			Name:   WorldName,

			License:     about.CreativeCommons.PublicDomainDedication,
			Alternative: about.MITNoAttribution(2023, about.Fospathi.Name),
		},
		Init:      worldengine.Init,
		PulseRate: world.Medium,
		Update:    update.Pulse,
		WorldWeb:  worldWeb,
	},
)
