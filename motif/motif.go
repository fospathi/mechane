/* Package motif contains the Mechane themes. */
package motif

import (
	"log"

	"github.com/shopspring/decimal"

	"gitlab.com/fospathi/wire/colour"
)

// Default motif.
var Default = MetroOrange

// A Motif is a cohesive collection of stylistic properties used to style the
// UI.
type Motif struct {
	// A unique motif name.
	MotifName string

	// COLOURS.

	// Shared colours.
	Accent                 colour.RGB // Ephemeral button hover icon.
	BoxShadow              string
	ButtonBackground       colour.RGBA
	ButtonOpaqueBackground colour.RGB // Used when combining icons to obscure sections below.
	Error                  colour.RGB // Ephemeral background for anomalous shrinker.
	Primary                colour.RGB
	PrimaryGlow            colour.RGB
	PrimaryLight           colour.RGB
	Ready                  colour.RGB
	TextNormal             colour.RGB
	TextPassive            colour.RGB
	Warning                colour.RGB

	// Elm integrant colours.
	TextBigPassive colour.RGB

	// Typescript integrant/overlay item colours.
	BoxShadowGlow         string
	ButtonHoverBackground colour.RGBA
	Disabled              colour.RGB
	InputBorderFocus      colour.RGB
	InputBorderPrimary    colour.RGB
	PreBackground         colour.RGBA
	StitchingMajor        colour.RGB
	StitchingMinor1       colour.RGB
	StitchingMinor2       colour.RGB
	StitchingMinor3       colour.RGB

	// DIMENSIONS.
	//
	// String format decimal floats which represent dimensions in (implied) CSS
	// em units.

	ButtonBorder       string
	ButtonBorderRadius string
	ButtonIconFontSize string
	ButtonPadding      string
}

// An ElmMotif is a motif in a format the Elm language integrant's JSON decoder
// can parse.
type ElmMotif struct {
	MotifName string `json:"motifName"`

	Accent                 colour.RGBA `json:"accent"`
	BoxShadow              string      `json:"boxShadow"`
	ButtonBackground       colour.RGBA `json:"buttonBackground"`
	ButtonOpaqueBackground colour.RGBA `json:"buttonOpaqueBackground"`
	Error                  colour.RGBA `json:"error"`
	Primary                colour.RGBA `json:"primary"`
	PrimaryGlow            colour.RGBA `json:"primaryGlow"`
	PrimaryLight           colour.RGBA `json:"primaryLight"`
	Ready                  colour.RGBA `json:"ready"`
	TextNormal             colour.RGBA `json:"textNormal"`
	TextPassive            colour.RGBA `json:"textPassive"`
	Warning                colour.RGBA `json:"warning"`

	TextBigPassive colour.RGBA `json:"textBigPassive"`

	ButtonBorder       string `json:"buttonBorder"`
	ButtonBorderRadius string `json:"buttonBorderRadius"`
	ButtonIconFontSize string `json:"buttonIconFontSize"`
	ButtonPadding      string `json:"buttonPadding"`

	// Computed values.

	ButtonHeight float64 `json:"buttonHeight"`
}

// A TypescriptMotif is a motif in a format the Typescript language integrant
// can use.
type TypescriptMotif struct {
	MotifName string `json:"motifName"`

	Accent                 string `json:"accent"`
	BoxShadow              string `json:"boxShadow"`
	ButtonBackground       string `json:"buttonBackground"`
	ButtonOpaqueBackground string `json:"buttonOpaqueBackground"`
	Error                  string `json:"error"`
	Primary                string `json:"primary"`
	PrimaryGlow            string `json:"primaryGlow"`
	PrimaryLight           string `json:"primaryLight"`
	Ready                  string `json:"ready"`
	TextNormal             string `json:"textNormal"`
	TextPassive            string `json:"textPassive"`
	Warning                string `json:"warning"`

	BoxShadowGlow         string `json:"boxShadowGlow"`
	ButtonHoverBackground string `json:"buttonHoverBackground"`
	Disabled              string `json:"disabled"`
	InputBorderFocus      string `json:"inputBorderFocus"`
	InputBorderPrimary    string `json:"inputBorderPrimary"`
	PreBackground         string `json:"preBackground"`
	StitchingMajor        string `json:"stitchingMajor"`
	StitchingMinor1       string `json:"stitchingMinor1"`
	StitchingMinor2       string `json:"stitchingMinor2"`
	StitchingMinor3       string `json:"stitchingMinor3"`

	ButtonBorder       string `json:"buttonBorder"`
	ButtonBorderRadius string `json:"buttonBorderRadius"`
	ButtonIconFontSize string `json:"buttonIconFontSize"`
	ButtonPadding      string `json:"buttonPadding"`
}

// ToElmMotif suitable for decoding by the Elm language integrant.
func (m Motif) ToElmMotif() ElmMotif {
	cd := calcDims(m)
	return ElmMotif{
		MotifName: m.MotifName,

		Accent:                 m.Accent.Opaque(),
		BoxShadow:              m.BoxShadow,
		ButtonBackground:       m.ButtonBackground,
		ButtonOpaqueBackground: m.ButtonOpaqueBackground.Opaque(),
		Error:                  m.Error.Opaque(),
		Primary:                m.Primary.Opaque(),
		PrimaryGlow:            m.PrimaryGlow.Opaque(),
		PrimaryLight:           m.PrimaryLight.Opaque(),
		Ready:                  m.Ready.Opaque(),
		TextNormal:             m.TextNormal.Opaque(),
		TextPassive:            m.TextPassive.Opaque(),
		Warning:                m.Warning.Opaque(),

		TextBigPassive: m.TextBigPassive.Opaque(),

		ButtonBorder:       m.ButtonBorder,
		ButtonBorderRadius: m.ButtonBorderRadius,
		ButtonIconFontSize: m.ButtonIconFontSize,
		ButtonPadding:      m.ButtonPadding,

		ButtonHeight: cd.buttonHeight,
	}
}

// ToTypescriptMotif suitable for use by the Typescript language integrant.
func (m Motif) ToTypescriptMotif() TypescriptMotif {
	return TypescriptMotif{
		MotifName: m.MotifName,

		Accent:                 m.Accent.String(),
		BoxShadow:              m.BoxShadow,
		ButtonBackground:       m.ButtonBackground.String(),
		ButtonOpaqueBackground: m.ButtonOpaqueBackground.String(),
		Error:                  m.Error.String(),
		Primary:                m.Primary.String(),
		PrimaryGlow:            m.PrimaryGlow.String(),
		PrimaryLight:           m.PrimaryLight.String(),
		Ready:                  m.Ready.String(),
		TextNormal:             m.TextNormal.String(),
		TextPassive:            m.TextPassive.String(),
		Warning:                m.Warning.String(),

		BoxShadowGlow:         m.BoxShadowGlow,
		ButtonHoverBackground: m.ButtonHoverBackground.String(),
		Disabled:              m.Disabled.String(),
		InputBorderFocus:      m.InputBorderFocus.String(),
		InputBorderPrimary:    m.InputBorderPrimary.String(),
		PreBackground:         m.PreBackground.String(),
		StitchingMajor:        m.StitchingMajor.String(),
		StitchingMinor1:       m.StitchingMinor1.String(),
		StitchingMinor2:       m.StitchingMinor2.String(),
		StitchingMinor3:       m.StitchingMinor3.String(),

		ButtonBorder:       m.ButtonBorder + "em",
		ButtonBorderRadius: m.ButtonBorderRadius + "em",
		ButtonIconFontSize: m.ButtonIconFontSize + "em",
		ButtonPadding:      m.ButtonPadding + "em",
	}
}

type calcDim struct {
	buttonHeight float64
}

func calcDims(m Motif) calcDim {
	strToDecimal := func(s string) decimal.Decimal {
		d, err := decimal.NewFromString(s)
		if err != nil {
			log.Fatalf(
				"motif decimal error: motif name: %v, value: %v, err: %v\n",
				m.MotifName, s, err,
			)
		}
		return d
	}

	buttonIconFontSize := strToDecimal(m.ButtonIconFontSize)
	buttonBorder := strToDecimal(m.ButtonBorder)
	buttonPadding := strToDecimal(m.ButtonPadding)
	buttonHeight, _ := buttonIconFontSize.Add(
		decimal.NewFromInt(2).Mul(buttonPadding.Add(buttonBorder)),
	).Float64()

	return calcDim{
		buttonHeight: buttonHeight,
	}
}
