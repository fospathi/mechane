package motif

import (
	"fmt"

	"gitlab.com/fospathi/wire/colour"
	"gitlab.com/fospathi/wire/colour/rgb"
)

var metroGlow = colour.RGB{R: 255, G: 210, B: 0}

// MetroOrange is an orangey themed motif.
var MetroOrange = Motif{
	MotifName: "MetroOrange",

	Accent:                 rgb.Deepskyblue,
	BoxShadow:              "0px 0.1em 0.5em 1px DimGray",
	ButtonBackground:       rgb.White.ToRGBA(0.85),
	ButtonOpaqueBackground: rgb.White,
	Error:                  rgb.Red,
	Primary:                rgb.Darkorange,
	PrimaryGlow:            metroGlow,
	PrimaryLight:           rgb.Orange,
	Ready:                  rgb.PastelYellow,
	TextNormal:             rgb.Black,
	TextPassive:            rgb.Darkgrey, // Note: CSS dark gray is lighter than CSS gray!
	Warning:                rgb.PastelRed,

	TextBigPassive: rgb.Lightgray,

	BoxShadowGlow:         fmt.Sprintf("0px 0px 0.7em 0.2em %s", metroGlow),
	ButtonHoverBackground: rgb.White.ToRGBA(0.95),
	Disabled:              rgb.Lightgray,
	InputBorderFocus:      rgb.Dodgerblue,
	InputBorderPrimary:    rgb.Gold,
	PreBackground:         rgb.CosmicLatte.ToRGBA(0.85),
	StitchingMajor:        rgb.Orange,
	StitchingMinor1:       rgb.Deepskyblue,
	StitchingMinor2:       rgb.Gold,
	StitchingMinor3:       rgb.Silver,

	ButtonBorder:       "0.4",
	ButtonBorderRadius: "1.2",
	ButtonIconFontSize: "2",
	ButtonPadding:      "0.6",
}
