package motif

import (
	"fmt"

	"gitlab.com/fospathi/wire/colour"
	"gitlab.com/fospathi/wire/colour/rgb"
)

var schematicGlow = colour.RGB{R: 0, G: 150, B: 184}

// SchematicPrussian is a bluey themed motif.
var SchematicPrussian = Motif{
	MotifName: "SchematicPrussian",

	Accent:                 rgb.Violet,
	BoxShadow:              "0px 0.1em 0.5em 1px DimGray",
	ButtonBackground:       rgb.White.ToRGBA(0.85),
	ButtonOpaqueBackground: rgb.White,
	Error:                  rgb.Red,
	Primary:                rgb.PrussianBlue,
	PrimaryGlow:            colour.RGB{R: 0, G: 150, B: 184},
	PrimaryLight:           rgb.PrussianBlueLight,
	Ready:                  rgb.Lightseagreen,
	TextNormal:             rgb.Black,
	TextPassive:            rgb.Darkgrey, // Note: CSS dark gray is lighter than CSS gray!
	Warning:                rgb.Orange,

	TextBigPassive: rgb.Lightgray,

	BoxShadowGlow:         fmt.Sprintf("0px 0px 0.7em 0.2em %s", schematicGlow),
	ButtonHoverBackground: rgb.White.ToRGBA(0.95),
	Disabled:              rgb.Lightgray,
	InputBorderFocus:      rgb.Dodgerblue,
	InputBorderPrimary:    rgb.Gold,
	PreBackground:         rgb.CosmicLatte.ToRGBA(0.85),
	StitchingMajor:        rgb.PrussianBlueLight,
	StitchingMinor1:       rgb.Violet,
	StitchingMinor2:       rgb.Gold,
	StitchingMinor3:       rgb.Silver,

	ButtonBorder:       "0.4",
	ButtonBorderRadius: "1.2",
	ButtonIconFontSize: "2",
	ButtonPadding:      "0.6",
}
